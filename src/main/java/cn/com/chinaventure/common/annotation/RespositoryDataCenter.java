package cn.com.chinaventure.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

/**
 * Created by vitem on 2017/6/13.
 * <p>
 */
@Target({ElementType.TYPE})

public @interface RespositoryDataCenter {

}
