package cn.com.chinaventure.common.util;


import org.springframework.util.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 字符串工具类
 *
 * @author huangyong
 * @since 1.0.0
 */
public final class StringUtil {

    /**
     * 判断字符串是否为空
     */
    public static boolean isEmpty(String str) {
        if (str != null) {
            str = str.trim();
        }
        return StringUtils.isEmpty(str);
    }

    public static boolean isNotBlank(String str) {
        if (str != null) {
            str = str.trim();
        }
        return StringUtils.isEmpty(str);
    }

    /**
     * 判断字符串是否非空
     */
    public static boolean isNotEmpty(String str) {
        return !isEmpty(str);
    }

    public static boolean isNotEmpty(Long str) {
        return str!=null;
    }



    public static boolean validateName(String userName){

        if(userName ==null || userName.length()<1 || userName.length()>15)
            return false;

        int numFlag = 0;

        for(int index=0; index < userName.length(); index++)
            if(userName.charAt(index)>='0' && userName.charAt(index)<='9')
                numFlag ++;


        return numFlag>1;
    }

    /**
     * 邮箱验证（必须符合邮箱的格式，如xxxxxxxx@*.com）
     * @param email
     * @return
     */
    public static boolean validateEmail(String email){
        String check = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern regex = Pattern.compile(check);
        Matcher matcher = regex.matcher(email);
        boolean isMatched = matcher.matches();
        return isMatched;
    }

    /**
     * 手机号验证
     *
     * @return 验证通过返回true
     */
    public static boolean validateMobile(String mobile) {
        if(mobile==null||mobile.length()!=11){
            return false;
        }
        Pattern p = Pattern.compile("^[1][1,2,3,4,5,6,7,8,9][0-9]{9}$"); // 验证手机号
        Matcher m = p.matcher(mobile);
        boolean b = m.matches();
        return b;
    }

    public static String  getUUID32() {
        return  UUIDTool.getUUID();
    }



}
