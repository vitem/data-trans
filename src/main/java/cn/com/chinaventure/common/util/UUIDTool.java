package cn.com.chinaventure.common.util;

/**
 * Created by vitem on 2017/6/15.
 * <p>
 */
import java.util.UUID;

public class UUIDTool {

    public UUIDTool() {
    }

    public static String getUUID() {

        return UUID.randomUUID().toString().replace("-", "");
    }
}