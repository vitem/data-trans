package cn.com.chinaventure.common.enums;

/**
 * Created by vitem on 2017/6/13.
 * <p>
 */
public enum UserSexEnum {
    MAN, WOMAN
}
