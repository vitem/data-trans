package cn.com.chinaventure.common.error;


/**
 * @author Administrator
 *
 */
public interface CodeE {

	 String getName();

	 String getCode();

	 CodeE getCode(String code);
}
