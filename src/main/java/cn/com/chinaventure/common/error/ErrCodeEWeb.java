package cn.com.chinaventure.common.error;

/**
 * @author Administrator
 *
 */
public enum ErrCodeEWeb implements CodeE{
	

	/**公共模块*/
	SUCCESS("0", "成功"),
	NONE("w-404", "未知错误");
	
	private String code;
	private String name;

	private ErrCodeEWeb(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String getCode() {
		return this.code;
	}
	
	

	public void setCode(String code) {
		this.code = code;
	}

	public void setName(String name) {
		this.name = name;
	}

	public  ErrCodeEWeb getCode(String code) {
		ErrCodeEWeb[] opCodes = ErrCodeEWeb.values();
		for (ErrCodeEWeb opCode : opCodes) {
			if (opCode.code.equals(code))
				return opCode;
		}
		return SUCCESS;
	}
}
