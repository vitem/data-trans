package cn.com.chinaventure.common.error;


/**
 * @author Administrator
 * desc
 *
 */
public enum ErrCodeE implements CodeE {
	
	/**
	 *
	 *错误码规范：
	 *1XXXXX：公共错误
	 *2XXXXX：用户模块
	 *3XXXXX：IM模块
	 *4XXXXX：cms模块
	 *5XXXXX：文档管理模块
	 *6XXXXX：权限模块
	 *7XXXXX：商品模块
	 *8XXXXX：财务支付模块
	 *9XXXXX：工单模块
	 *AXXXXX：消息模块
	 *BXXXXX：流程定义模块
	 *CXXXXX：数据字典模块
	 */
	
	/**公共模块*/
	SUCCESS("0", "成功"),
	PARAM_INVALID("s-400", "参数无效"),
	DB_ERROR("s-3306", "数据库错误"),
	NONE("s-404", "未知错误");




	private String code;
	private String name;

	private ErrCodeE(String code, String name) {
		this.code = code;
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public String getCode() {
		return this.code;
	}

	public  ErrCodeE getCode(String code) {
		ErrCodeE[] opCodes = ErrCodeE.values();
		for (ErrCodeE opCode : opCodes) {
			if (opCode.code.equals(code))
				return opCode;
		}
		return SUCCESS;
	}
	
}
