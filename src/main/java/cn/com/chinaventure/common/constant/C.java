package cn.com.chinaventure.common.constant;

import java.math.BigDecimal;

/**
 * 全局常量
 * @author LC
 * @date:2015年11月17日 
 */
public interface C {
	
	/**
	 * 用户id标识：zgg【全局id（顾问查看用户信息用）】
	 */
	public static final String PRI_SYS = "zgg";
	
	/*=======================全局角色常量==========================*/
	/**
	 * session key
	 * 获取系统用户对象
	 */
	public static final String SYS_USER="sysUser";
	
	/**
	 * session key
	 * 获取系统SESSION 参数对象
	 * 包含用户id，session的id，角色id集合，按钮权限id集合
	 */
	public static final String SESSION_PARAM="sessionParam";
	
	/**
	 * 知果客户
	 */
	public static final Integer ZGUSER=1;
	/**
	 * 系统用户
	 */
	public static final Integer SYSUSER=2;
	/**
	 * 顾问
	 */
	public static final String GW = "gw";
	/**
	 * 律师
	 */
	public static final String LS = "ls";
	/**
	 * 客户
	 */
	public static final String KH = "kh";
	/**
	 * 服务商
	 */
	public static final String FWS = "fws";
	/**
	 * 客服
	 */
	public static final String KF = "kf";
	/**
	 * 收发文
	 */
	public static final String SFW = "sfw";
	/**
	 * 财务
	 */
	public static final String CW = "cw";
	/**
	 * 专利经理
	 */
	public static final String ZLJL = "zljl";
	/**
	 * 专利主管
	 */
	public static final String ZLZG = "zlzg";
	/**
	 * 流程主管
	 */
	public static final String LCZG = "lczg";
	/**
	 * 流程专员
	 */
	public static final String LCZY = "lczy";
	/**
	 * 申核专员
	 */
	public static final String SHZY = "shzy";
	/**
	 * 专利工程师
	 */
	public static final String ZLGCS = "zlgcs";
	/**
	 * 版权工程师
	 */
	public static final String BQGCS = "bqdlr";
	/**
	 * 加急律师
	 */
	public static final String WBZY = "wbzy";
	/**
	 * 自建角色
	 */
	public static final String ZJROLE = "qx";
	/**
	 * 商标分案人
	 */
	public static final String TRFAR = "trfar";
	/**
	 * 商标代理人
	 */
	public static final String TRDLR = "trdlr";
	/**
	 * 版权主管
	 */
	public static final String BQZG = "bqzg";
	
	/**
	 * 版权代理人
	 */
	public static final String BQDLR = "bqdlr";
	/*=======================全局角色常量==========================*/
	
	/*=================全局沟通记录类型常量==========================*/
	/**
	 * 合同记录
	 */
	public static final String CONTRACT_RECORD = "contract";
	/**
	 * 订单记录
	 */
	public static final String ORDER_RECORD = "order";
	/**
	 * 客户记录
	 */
	public static final String CUSTOMER_RECORD = "customer";
	
	/*====================全局沟通记录类型常量======================*/
	
	/**
	 * 流程节点间分割符(排他节点分割)
	 */
	public static final String SEPARATOR_STEP_EXC = "#";
	
	/**
	 * 流程节点间分割符(分支流程分割)
	 */
	public static final String SEPARATOR_STEP_BRA = "&";
	
	/**
	 * 流程节点处理结构和流程id分割符:,
	 */
	public static final String SEPARATOR_STEP_ID = ",";
	
	/**
	 * 是否含有分支流程：START（开始分支流程）
	 */
	public static final String HAS_BRANCH_START = "START";
	
	/**
	 * 是否含有分支流程：YES（是）
	 */
	public static final String HAS_BRANCH_YES = "YES";
	
	/**
	 * 是否含有分支流程：NO（是）
	 */
	public static final String HAS_BRANCH_NO = "NO";
	
	/**
	 * 是否含有分支流程：END（流程结束节点）
	 */
	public static final String HAS_BRANCH_END = "END";
	
	/**
	 * 订单流程激活参数：1 理想
	 */
	public static final String NEXTSTEP_SUCCESS = "1";
	
	/**
	 * 订单流程激活参数：2 异常
	 */
	public static final String NEXTSTEP_FAILURE = "2";
	
	/**
	 * 订单流程激活参数：3 特殊情况
	 */
	public static final String NEXTSTEP_OTHER = "3";
	/**
	 * 订单流程激活参数：j 加急情况
	 */
	public static final String NEXTSTEP_HURRY = "j";
	
	/**
	 * 默认业务文件类型：ft_1
	 */
	public static final String DEFAULT_FILE_TYPE = "ft_1";
	
	/**
	 * 默认业务文件存储路径：/upload/default
	 */
	public static final String DEFAULT_FILE_PATH = "/upload/default";
	
	/**
	 * 文件类型（基础文件）：base_
	 */
	public static final String FILE_LOGIC_TYPE_BASE = "base_";
	
	/**
	 * 文件类型（业务文件）：business_
	 */
	public static final String FILE_LOGIC_TYPE_BUSINESS = "business_";
	
	
	/**
	 * 版权类型（美术）pcode：cr_pro_1
	 */
	public static final String COPYRIGHT_ARTS = "cr_pro_1";
	/**
	 * 版权类型（文字）pcode：cr_pro_2
	 */
	public static final String COPYRIGHT_WORKS = "cr_pro_2";  
	/**
	 * 版权类型（其他）pcode：cr_pro_3
	 */
	public static final String COPYRIGHT_OTHER = "cr_pro_3";  
	/**
	 * 版权类型（软著）pcode：cr_soft_1 
	 */
	public static final String COPYRIGHT_SOFTWARE = "cr_soft_1"; 
	
	/**
	 * grid page size:25
	 */
	public static final int GRID_PAGE_SIZE = 25;
	
	/**
	 * 产品code（普通商标注册）：tr_reg_1
	 */
	public static final String PCODE_TR_REG_1 = "tr_reg_1";
	/**
	 * 产品code（商标驳回复审）：tr_refr_1
	 */
	public static final String PCODE_TR_REFR_1 = "tr_refr_1";
	/**
	 * 产品code（专利业务）：pt_sq_1
	 */
	public static final String PCODE_PT_SQ_1 = "pt_um_cra_1";
	
	/**
	 * 产品code（专利业务）：pt_sq_1
	 */
	public static final String PCODE_PT_SQ_OW = "pt_ow_cra_1";
	
	/**
	 * 产品 code (版权业务)：cr_sq_1
	 */
	public static final String PCODE_CR_SQ_1 = "cr_pro_1";
	
	/**
	 * 产品code（加急商标注册）：tr_reg_7
	 */
	public static final String PCODE_TR_REG_7 = "tr_reg_7";
	/**
	 * 产品code（果保加急商标注册）：tr_reg_8
	 */
	public static final String PCODE_TR_REG_8 = "tr_reg_8";

	/**
	 * 果时系列（快捷版）
	 */
	public static final String PCODE_TR_REG_12 = "tr_reg_12";

	/**
	 *果时系列（尊享版）
	 */
	public static final String PCODE_TR_REG_13 = "tr_reg_13";

	/**
	 * 产品code（高薪）：ht_ec_1
	 */
	public static final String PCODE_HT_EC_1 = "ht_ec_1";
    /**
     * 启动老流程的PCODE
     */
	public static final String OLD_FLOW_PCODE="tr_tf_1,tr_tf_2,tr_tf,tr_rl,tr_dis,tr_del,tr_es_1,tr_es,tr_doa_1,tr_doa_2,tr_doa_3,tr_mod_1,tr_mod_2,tr_mod_3,tr_mod_4,tr_mod_5,tr_rl_1,tr_rl_2,tr_rl_3,tr_dis_1,tr_dis_2,tr_dis_3,tr_del_1,tr_del_2,tr_del_3,tr_del_4,tr_es_3,tr_es_2";
	
	/**
	 * 文件类型（基础文件）：1
	 */
	public static final String TYPE_1 = "1";

	/**
	 * 文件类型（业务文件）：2
	 */
	public static final String TYPE_2 = "2";

	/**
	 * 文件操作类型：预览
	 */
	public static final String OPERA_TYPE_SEE = "see";

	/**
	 * 文档下载路径:base/getfile/download/
	 */
	public static final String FILE_GET_PATH = "/base/getfile/download/";
	
	/**
	 * 获取文件上传地址用KEY： base_upload_pic
	 */
	public static final String BASE_UPLOAD_PIC = "base_upload_pic";
	
	/**
	 * 获取文件上传地址用KEY： base_upload_file
	 */
	public static final String BASE_UPLOAD_FILE = "base_upload_file";
	
	/**
	 * 图片下载地址：/base/getfile/pic/
	 */
	public static final String BASE_GETFILE_PIC = "/base/getfile/pic/";
	
	/**
	 * 商标驳回申请模块：文件下载路径
	 */
	public static final String REBUT_FILE_PATH = "/zgOrderRebut/download/";
	
	/**
	 * 收发文上传文件下载地址用KEY：ft_sfw
	 */
	public static final String BASE_GETFILE_SFW = "ft_sfw";
	
	/**
	 * 受理通知书下载地址用KEY：ft_sfw_st
	 */
	public static final String BASE_GETFILE_SFW_ST = "ft_sfw_st";

	/*====================申报端口常量======================*/
	/**
	 * 申报端口：1
	 * 北京知果科技有限公司
	 */
	public static final Integer APPLY_CHANNEL_ZGG = 1;
	/**
	 * 申报端口：2
	 * 扶友（北京）知识产权代理有限公司
	 */
	public static final Integer APPLY_CHANNEL_FY = 2;
	/**
	 * 申报端口：3
	 * 南京知果科技有限公司
	 */
	public static final Integer APPLY_CHANNEL_ZGG_NJ = 3;
	
	/**
	 * 主体状态（待审）：0
	 */
	public static final Integer MAIN_BODY_STATUS_0 = 0;
	
	/**
	 * 主体状态（无效）：-1
	 */
	public static final Integer MAIN_BODY_STATUS_INVALID = -1;
	
	/**
	 * 值班状态:未开启(0)
	 */
	public static final Integer USER_DUTY_STSTUS_READY = 0;
	
	/**
	 * 值班状态:值班中(2)
	 */
	public static final Integer USER_DUTY_STSTUS_ONDUTY = 2;
	
	/**
	 * 值班状态:已完成(1)
	 */
	public static final Integer USER_DUTY_STSTUS_DONE = 1;
	
	/**
	 * 字符串分割符：","
	 */
	public static final String STRING_SPLITER = ",";
	
	/**
	 * 手机号码正则
	 */
	public static final String TEL_PATTERN = "^[0-9]{11}$";
	
	/**
	 * 支付状态：已支付
	 */
	public static final Integer PAY_STATUS_DONE = 1;

	/**
	 * 支付状态：未支付
	 */
	public static final Integer PAY_STATUS_NO = 0;
	
	/**
	 * 支付状态：已退款
	 */
	public static final Integer PAY_STATUS_RETURN = -1;
	
	/**
	 * 是否委托：委托
	 */
	public static final String ENTRUST_FLAG_YES = "1";
	
	/**
	 * 是否委托：不委托
	 */
	public static final String ENTRUST_FLAG_NO = "0";
	
	/**
	 * 加急状态：加急
	 */
	public static final Integer URGENT_FLAG_YES = 1;
	
	/**
	 * 加急状态：不加急
	 */
	public static final Integer URGENT_FLAG_NO = 0;
	
	/**
	 * 是否开发票：开发票
	 */
	public static final Integer OPENBILL_YES = 1;
	
	/**
	 * 是否开发票：不发票
	 */
	public static final Integer OPENBILL_NO = 0;
	
	/**
	 * 支付方式：线下支付
	 */
	public static final Integer PAY_TYPE_OFFLINE = 21;
	
	/**
	 * 发票状态：已开发票
	 */
	public static final String BILL_STATUS_YES = "1";
	
	/**
	 * 发票状态：未开发票
	 */
	public static final String BILL_STATUS_NO = "0";
	
	
	/***********************************  开票合同批量导入常量 start ******************************************************************/
	/**
	 * 合同操作人：金美薇
	 */
	public static final String CZR_MAN = "金美薇";
	/**
	 *  发票类型：增值税普通发票
	 */
	public static final String FP_COMMON_TYPE = "普通";
	/**
	 *  开票方(文本)
	 */
	public static final String KPF_BEIJINGZHIGUO = "北京知果";
	public static final String KPF_NANJINGZHIGUO = "南京知果";
	public static final String KPF_FUYOU = "扶友";
	public static final String KPF_ZHUOTANG = "北京卓唐";
	/**
	 *  开票方（数值）
	 */
	public static final Integer KPF_BEIJINGZHIGUO_VALUE = 0;
	public static final Integer KPF_NANJINGZHIGUO_VALUE = 2;
	public static final Integer KPF_FUYOU_VALUE = 1;
	public static final Integer KPF_ZHUOTANG_VALUE = 3;
	
	
	public static final String KPHT_CONTRACT = "合同号";
	public static final String FP_NBR = "发票号";
	public static final String KP_TIME = "开票日期";
	public static final String CZ_INFO = "操作备注";
	public static final String FP_TYPE = "发票类型";
	public static final String KPF_BILL = "开票方";
	
	/***********************************  开票合同批量导入常量 end ******************************************************************/
	
	/***********************************  订单类型 start ******************************************************************/
	public static final String OTHER_PCODE = "other";
	public static final String OTHER_PNAME = "其它";
	public static final String NO_DONE = "未分配";
	public static final Integer TWO = 2;
	public static final Integer Three = 3;
	
	/***********************************  订单类型 end ******************************************************************/
	
	/***********************************  删除状态值  ******************************************************************/
	/**
	 *删除
	 */
	public static final Integer DEL_TRUE = 2;
	
	/**
	 *正常 
	 */
	public static final Integer DEL_FALSE = 1; 
   
	public static final String RETURN_SUCCESS = "0";
	public static final String RETURN_BUSINESS_FAILURE ="-1" ;//业务错误，业务逻辑错误，包含参数错误
	public static final  String RETURN_APPLICATION_FAILURE ="-2" ;//应用错误，包含runtime异常，网络，数据库异常
	
    /**
	 * 项目统一编码
	 */
	public static final String CHARSET = "UTF-8"; 
	
	/***********************************  订单启动流程  ******************************************************************/
	public static final Integer HCZL = 3057;//订单回传资料上一节点id
	public static final Integer ZCJD = 90;//订单终止节点标识
	public static final Integer WAIT_O_BJ = 6;//普通商标注册“等候报件”上一节点stepnum
	public static final Integer WAIT_BJ = 6;//加急商标注册“待候报件”上一节点stepnum
	

	/**
	 * 产品默认税点
	 */
	public static final BigDecimal P_DEFAULT_TAXATION=new BigDecimal(6);

	/***********************************  管理后台上传文件  ******************************************************************/
	/**
	 * 图片存储位置
	 */
	public static final String FILEUPLOAD_IMAGES="/fileupload/images";
	/**
	 * world,excel文件位置
	 */
	public static final String  FILEUPLOAD_OPPORTUNITY_ATTACHMENTS="/fileupload/opportunity/attachments";
	/**
	 * 其他文件位置
	 */
	public static final String  FILEUPLOAD_OTHERS="/fileupload/others";

	/**
	 * 默认图片（无图片时显示）
	 */
	public static final String  FILEUPLOAD_DEFAULTIMAGE="default.jpg";

}
