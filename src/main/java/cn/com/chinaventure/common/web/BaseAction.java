/**
 * @Title: BaseAction.java
 * @version V1.0
 */
package cn.com.chinaventure.common.web;

import cn.com.chinaventure.common.constant.C;
import cn.com.chinaventure.common.error.CodeE;
import cn.com.chinaventure.common.json.JSONOuter;
import cn.com.chinaventure.common.json.JsonResult;
import com.alibaba.fastjson.JSONException;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;



/**
 * 控制层基
 * @ClassName: BaseAction
 * @Description: BaseAction工具
 * @author
 *
 */

public  class BaseAction {

    private static ThreadLocal<HttpServletRequest> requestHolder = new ThreadLocal();
    private static ThreadLocal<HttpServletResponse> responseHolder = new ThreadLocal();


    /**
     * 初始化
     */
    public  void init(HttpServletRequest request, HttpServletResponse response) {
        requestHolder.set(request);
        responseHolder.set(response);
    }


    /**
     * 销毁
     */
    public void destroy() {
        requestHolder.remove();
        responseHolder.remove();

    }

/*    @Resource
    private DictPublicServicePretend dictPublicServicePretend;*/
    /**
     * @Title:getCurrentUser
     * @Description:获取当前用户
     * @author:
     * @param:无
     * @return:UserSession
     * @throws
     */



    /**得到request对象
     * @return
     */
    public HttpServletRequest getRequest() {
        return  requestHolder.get();
    }

    /**得到response对象
     * @return
     */
    public HttpServletResponse getResponse() {
        return  responseHolder.get();
    }


/*	@ModelAttribute
    public void setReqAndRes(HttpServletRequest request, HttpServletResponse response){
        this.request = request;
        this.response = response;
    } */

    public <T> JsonResult<T> buildJsonResult(T obj){
        return  new JsonResult<T>(obj,true, C.RETURN_SUCCESS,"成功！");
    }

    public <T> JsonResult<T> buildJsonResult(T obj,Map<String, Map<String, String>> dictMap){
        return  new JsonResult<T>(obj,true,C.RETURN_SUCCESS,"成功！",dictMap);
    }

    public <T> JsonResult<T> buildJsonResult(T obj,String[] pDicCodes){
        //取数据字典，组装map
        Map<String, Map<String, String>> dictMap=new HashMap<String,Map<String,String>>();
		/*for (String pcode : pDicCodes) {
			List<DictPublic> dicList=null;
			ServiceResult<List<DictPublic>>  sr=this.dictPublicServicePretend.getDictPublicByParentDicCode(pcode);
			if (C.RETURN_SUCCESS.equals(sr.getCode())) {
				dicList=sr.getResult();
            }
			if(CollectionUtil.isNotEmpty(dicList)){
				HashMap<String, String> map=new HashMap<String,String>();
				for (DictPublic dictPublic : dicList) {
					map.put(dictPublic.getDicValue(),dictPublic.getDicName());
				}
				dictMap.put(pcode, map);
			}
        } */

        if(dictMap.size()>0){
            return  new JsonResult<T>(obj,true,C.RETURN_SUCCESS,"成功！",dictMap);
        }
        return  new JsonResult<T>(obj,true,C.RETURN_SUCCESS,"成功！");

    }

    public <T> JsonResult<T> buildJsonResult(T obj, String code){
        return new JsonResult<T>(obj, code);
    }

    public <T> JsonResult<T> buildJsonResult(T obj, String code, String message){
        return new JsonResult<T>(obj, code, message);
    }

    public <T> JsonResult<T> buildJsonResult(T obj,Boolean success, String code, String message){
        return new JsonResult<T>(obj,success, code,message);
    }

    public <T> JsonResult<T> buildFailJsonResult(Boolean success,String code, String message){
        return new JsonResult<T>(success,code, message);
    }

    public <T> JsonResult<T> buildFailJsonResult(Boolean success,CodeE codeE){
        return new JsonResult<T>(success,codeE.getCode(), codeE.getName());
    }

    public <T> void buildJsonResultAndWrite(T obj){
        JsonResult<T> jsonResult = new JsonResult<T>(obj,true,"成功！");
        try {
            JSONOuter.write(this.getResponse().getWriter(), jsonResult);
        } catch (IOException e) {
            throw new JSONException(e.getMessage(), e);
        }
    }

    public <T> void buildJsonResultAndWrite(T obj, String code){
        JsonResult<T> jsonResult = new JsonResult<T>(obj, code);
        try {
            JSONOuter.write(this.getResponse().getWriter(), jsonResult);
        } catch (IOException e) {
            throw new JSONException(e.getMessage(), e);
        }
    }

    public <T> void buildJsonResultAndWrite(T obj, String code, String message){
        JsonResult<T> jsonResult = new JsonResult<T>(obj, code, message);
        try {
            JSONOuter.write(this.getResponse().getWriter(), jsonResult);
        } catch (IOException e) {
            throw new JSONException(e.getMessage(), e);
        }
    }

    public <T> void buildJsonResultAndWrite(T obj,Boolean success, String code, String message){
        JsonResult<T> jsonResult = new JsonResult<T>(obj,success, code, message);
        try {
            JSONOuter.write(this.getResponse().getWriter(), jsonResult);
        } catch (IOException e) {
            throw new JSONException(e.getMessage(), e);
        }
    }

    /**
     * 获取客户端真实IP地址
     *
     * @return IP
     */
    public String getClientIp() {
        String ip = this.getRequest().getHeader("x-forwarded-for");

        if (isUnAvailableIp(ip)) {
            ip = this.getRequest().getHeader("X-Real-IP");
        }
        if (isUnAvailableIp(ip)) {
            ip = this.getRequest().getHeader("Proxy-Client-IP");
        }
        if (isUnAvailableIp(ip)) {
            ip = this.getRequest().getHeader("WL-Proxy-Client-IP");
        }
        if (isUnAvailableIp(ip)) {
            ip = this.getRequest().getRemoteAddr();
        }
        return ip;
    }

    /**
     * 获取客户端真实IP地址
     *
     * @param request HttpServletRequest
     * @return IP
     */
    public String getClientIp(HttpServletRequest request) {
        String ip = request.getHeader("x-forwarded-for");
        if (isUnAvailableIp(ip)) {
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (isUnAvailableIp(ip)) {
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (isUnAvailableIp(ip)) {
            ip = request.getRemoteAddr();
        }
        return ip;
    }



    private static boolean isUnAvailableIp(String ip) {
        return (StringUtils.isEmpty(ip) || "unknown".equalsIgnoreCase(ip));
    }

    /**
     * 获取上下文域名地址
     */
    public String getWebDomain() {
        //读取资源文件获取域名地址
        ResourceBundle bundle = PropertyResourceBundle.getBundle("config");
        String webDomain = bundle.getString("web.domain");
        return webDomain;
    }


}
