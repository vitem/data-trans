package cn.com.chinaventure.common.page;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Iterator;
import java.util.List;

/**
 * 分页参数?
 * 
 */
@SuppressWarnings({ "serial", "rawtypes" })
public class PageParam implements Page,java.io.Serializable {

	/**
	 * 
	 */
	public static final int DEFAULT_PAGE_SIZE = 10;
	
	/**
	 * 页号, 不分页传-1。分页传页号，页号>0
	 */
	private int pageNo;

	private int offset;
	
	/**
	 * 每页显示条数
	 */
	private int pageSize;
	
	private int prePage;
	private int nextPage;
	private int totalPage;
	private int totalCount;

	public PageParam() {
		this.pageNo = 1;
		this.pageSize = DEFAULT_PAGE_SIZE;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public int getPrePage() {
		return prePage;
	}

	public void setPrePage(int prePage) {
		this.prePage = prePage;
	}

	public int getNextPage() {
		return nextPage;
	}

	public void setNextPage(int nextPage) {
		this.nextPage = nextPage;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public List getContent() {
		
		return null;
	}

	public int getNumber() {
		return pageNo;
	}

	public int getNumberOfElements() {
		
		return 0;
	}

	public int getSize() {
		return pageSize;
	}

	public Sort getSort() {
		
		return null;
	}

	public boolean hasContent() {
		
		return false;
	}

	public boolean hasNext() {
		
		return false;
	}

	public boolean hasPrevious() {
		
		return false;
	}

	public boolean isFirst() {
		
		return false;
	}

	public boolean isLast() {
		
		return false;
	}

	public Pageable nextPageable() {
		
		return null;
	}

	public Pageable previousPageable() {
		
		return null;
	}

	public Iterator iterator() {
		
		return null;
	}

	public long getTotalElements() {
		return totalCount;
	}

	public int getTotalPages() {
		return totalPage;
	}

	public boolean hasNextPage() {
		if (pageNo < totalPage) {
			return true;
		} else {
			return false;
		}
	}

	public boolean hasPreviousPage() {
		if (pageNo > 1) {
			return true;
		} else {
			return false;
		}
	}

	public boolean isFirstPage() {
		
		return false;
	}

	public boolean isLastPage() {
		
		return false;
	}

	@Override
	public Page map(Converter converter) {
		return null;
	}

	public int getOffset() {
		return offset=(pageNo>=1)?(pageNo-1) *pageSize:0;
	}


}
