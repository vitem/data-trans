package cn.com.chinaventure.common.page;

import org.apache.ibatis.session.RowBounds;

public class PageTools {
	 
	public static RowBounds createRowBounds(PageParam pageParam) {
		    int pageNo=pageParam.getPageNo();
		    if (-1==pageNo) {
		    	  return new RowBoundsExt(0, 0);
			}
		    int offset=(pageNo>=1)?(pageNo-1) *pageParam.getPageSize():0;
	        return new RowBoundsExt(offset, pageParam.getPageSize());
	}
	
	public static RowBounds createRowBounds(int pageNo, int pageSize) {
		 if (-1==pageNo) {
	    	  return new RowBoundsExt(0, 0);
		}
		 
        int offset = (pageNo - 1) * pageSize;
        return new RowBoundsExt(offset, pageSize);
	}

	/**
	 * @param pageParam
	 * @param customTotalCount 使用自定义的查询总条数的方法。true：自定义，false：采用自动生成的sql。默认false
	 * @return
	 */
	public static RowBounds createRowBounds(PageParam pageParam, Boolean customTotalCount) {
	    int pageNo=pageParam.getPageNo();
	    if (-1==pageNo) {
	    	  return new RowBoundsExt(0, 0);
		}
	    int offset=(pageNo>=1)?(pageNo-1) *pageParam.getPageSize():0;
        return new RowBoundsExt(offset, pageParam.getPageSize(),customTotalCount);
	}

}
