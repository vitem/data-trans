package cn.com.chinaventure.common.page;

import com.github.miemiedev.mybatis.paginator.domain.PageList;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/*
 * 往前台页面返回的分页对象。
 * 
 */
public class PageResult<T>  implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2620919373917939077L;
	// 返回当前页结果
	private List<T> list;
	
	/** 页大小（每页数据个数） */
	private int pageSize;

	/** 当前页号 */
	private int pageNo;

	/** 数据总个数 */
	private long totalCount = 0;
	
	/** 数据总页数数 */
	private long totalPage = 0;
	
	public PageResult() {
	}

	 /**
     * 包装Page对象，因为直接返回Page对象，在JSON处理以及其他情况下会被当成List来处理，
     * 而出现一些问题。
     * @param list          page结果
     * @param navigatePages 页码数量
     */
    public PageResult(List<T> list) {
        if (list instanceof PageList) {
        	PageList<T> page = (PageList<T>) list;
           /* this.pageNo = page.getPageNum();
            this.pageSize = page.getPageSize();
            this.totalCount = page.getTotal();
            this.totalPage = page.getPages();*/
            this.list = page;
        }
    }


	/**
	 * 把查询的List封装到PageResult中
	 * @param list
	 * @return
	 */
	public static <X> PageResult<X> toPage(List<X> list) {
		PageResult<X> pageResult = new PageResult<X>();
		pageResult.setList(list);
	/*	if( list instanceof Page ){
			Page<X> page = (Page<X>) list;
			pageResult.setPageNo(page.getPageNum());
			pageResult.setPageSize(page.getPageSize());
			pageResult.setTotalCount(page.getTotal());
			pageResult.setTotalPage(page.getPages());
		}else{
			pageResult.setTotalCount(list.size());
		}*/
		
		if( list instanceof PageList ){
			PageList<X> page = (PageList<X>) list;
			pageResult.setPageNo(page.getPaginator().getPage());
			pageResult.setPageSize(page.getPaginator().getLimit());
			pageResult.setTotalCount(page.getPaginator().getTotalCount());
			pageResult.setTotalPage(calculateTotalPage(page.getPaginator().getLimit(),page.getPaginator().getTotalCount()));
		}else{
			pageResult.setTotalCount(list.size());
		}
		
		return pageResult;
    }

	/**
	 * 把查询的List封装到PageResult中
	 * @param list
	 * @return
	 */
	public static <X> PageResult<X> toPage(List<X> list,PageParam pageParam) {
		PageResult<X> pageResult = new PageResult<X>();
		pageResult.setList(list);
		
		if( list instanceof ArrayList&& null!=pageParam){
			//PageList<X> page = (PageList<X>) list;
			pageResult.setPageNo(pageParam.getPageNo());
			pageResult.setPageSize(pageParam.getPageSize());
			int totalCount=0;
			try {
	             totalCount = TotalCountHolder.get();
	        } finally {
	            TotalCountHolder.remove();
	        }
			pageResult.setTotalCount(totalCount);
			pageResult.setTotalPage(calculateTotalPage(pageParam.getPageSize(),totalCount));
		}else{
			pageResult.setTotalCount(list.size());
		}
		
		return pageResult;
    }
	
	/**
	 * 把查询的List封装到PageResult中
	 * @param list
	 * @param pageParam
	 * @param totalCount 总条数
	 * @return
	 */
	public static <X> PageResult<X> toPage(List<X> list,PageParam pageParam,int totalCount) {
		PageResult<X> pageResult = new PageResult<X>();
		pageResult.setList(list);
		
		if( list instanceof ArrayList&& null!=pageParam){
			//PageList<X> page = (PageList<X>) list;
			pageResult.setPageNo(pageParam.getPageNo());
			pageResult.setPageSize(pageParam.getPageSize());
			pageResult.setTotalCount(totalCount);
			pageResult.setTotalPage(calculateTotalPage(pageParam.getPageSize(),totalCount));
		}else{
			pageResult.setTotalCount(list.size());
		}
		
		return pageResult;
    }

	public List<T> getList() {
		return list;
	}



	public void setList(List<T> list) {
		this.list = list;
	}



	public int getPageSize() {
		return pageSize;
	}



	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}



	public int getPageNo() {
		return pageNo;
	}



	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}



	public long getTotalCount() {
		return totalCount;
	}



	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}



	public long getTotalPage() {
		return totalPage;
	}



	public void setTotalPage(long totalPage) {
		this.totalPage = totalPage;
	}
	
	private static long calculateTotalPage(int pageSize, int totalCount){
		//return (totalCount % pageSize==0)?new Double(Math.floor(totalCount / pageSize)).intValue():(new Double(Math.floor(totalCount / pageSize)).intValue()+1);
		return (totalCount % pageSize==0)?totalCount / pageSize:(totalCount / pageSize)+1;
	}


	
	
}
