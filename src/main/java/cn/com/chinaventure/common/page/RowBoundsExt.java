package cn.com.chinaventure.common.page;

import org.apache.ibatis.session.RowBounds;

public class RowBoundsExt extends RowBounds {
	private int offset;
	private int limit;
	/**
	 * 计算总条数方法采用自定义方式，而不是默认系统生成的sql语句
	 */
	private Boolean customTotalCount=false;
	
	public RowBoundsExt() {
	    this.offset = NO_ROW_OFFSET;
	    this.limit = NO_ROW_LIMIT;
	  }
 
	public RowBoundsExt(int offset, int limit) {
		this.offset = offset;
		this.limit = limit;
	}
	
	public RowBoundsExt(int offset, int limit, Boolean customTotalCount) {
		this.offset = offset;
		this.limit = limit;
		this.customTotalCount = customTotalCount;
	}
   
	
	public int getOffset() {
		return offset;
	}

	public int getLimit() {
		return limit;
	}

	public Boolean getCustomTotalCount() {
		return customTotalCount;
	}

	public void setCustomTotalCount(Boolean customTotalCount) {
		this.customTotalCount = customTotalCount;
	}



	
}
