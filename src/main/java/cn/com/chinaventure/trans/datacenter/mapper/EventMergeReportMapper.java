package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventMergeReport;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeReportExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventMergeReportMapper {
    int countByExample(EventMergeReportExample example);

    int deleteByExample(EventMergeReportExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventMergeReport record);

    int insertSelective(EventMergeReport record);

    List<EventMergeReport> selectByExample(EventMergeReportExample example);

    EventMergeReport selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventMergeReport record, @Param("example") EventMergeReportExample example);

    int updateByExample(@Param("record") EventMergeReport record, @Param("example") EventMergeReportExample example);

    int updateByPrimaryKeySelective(EventMergeReport record);

    int updateByPrimaryKey(EventMergeReport record);
}