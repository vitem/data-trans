package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FundCollectExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public FundCollectExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdIsNull() {
            addCriterion("fund_strategy_id is null");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdIsNotNull() {
            addCriterion("fund_strategy_id is not null");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdEqualTo(String value) {
            addCriterion("fund_strategy_id =", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdNotEqualTo(String value) {
            addCriterion("fund_strategy_id <>", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdGreaterThan(String value) {
            addCriterion("fund_strategy_id >", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdGreaterThanOrEqualTo(String value) {
            addCriterion("fund_strategy_id >=", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdLessThan(String value) {
            addCriterion("fund_strategy_id <", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdLessThanOrEqualTo(String value) {
            addCriterion("fund_strategy_id <=", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdLike(String value) {
            addCriterion("fund_strategy_id like", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdNotLike(String value) {
            addCriterion("fund_strategy_id not like", value, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdIn(List<String> values) {
            addCriterion("fund_strategy_id in", values, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdNotIn(List<String> values) {
            addCriterion("fund_strategy_id not in", values, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdBetween(String value1, String value2) {
            addCriterion("fund_strategy_id between", value1, value2, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andFundStrategyIdNotBetween(String value1, String value2) {
            addCriterion("fund_strategy_id not between", value1, value2, "fundStrategyId");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartScaleIsNull() {
            addCriterion("start_scale is null");
            return (Criteria) this;
        }

        public Criteria andStartScaleIsNotNull() {
            addCriterion("start_scale is not null");
            return (Criteria) this;
        }

        public Criteria andStartScaleEqualTo(BigDecimal value) {
            addCriterion("start_scale =", value, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleNotEqualTo(BigDecimal value) {
            addCriterion("start_scale <>", value, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleGreaterThan(BigDecimal value) {
            addCriterion("start_scale >", value, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("start_scale >=", value, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleLessThan(BigDecimal value) {
            addCriterion("start_scale <", value, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("start_scale <=", value, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleIn(List<BigDecimal> values) {
            addCriterion("start_scale in", values, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleNotIn(List<BigDecimal> values) {
            addCriterion("start_scale not in", values, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("start_scale between", value1, value2, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartScaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("start_scale not between", value1, value2, "startScale");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeIsNull() {
            addCriterion("start_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeIsNotNull() {
            addCriterion("start_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeEqualTo(Byte value) {
            addCriterion("start_currency_type =", value, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("start_currency_type <>", value, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeGreaterThan(Byte value) {
            addCriterion("start_currency_type >", value, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("start_currency_type >=", value, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeLessThan(Byte value) {
            addCriterion("start_currency_type <", value, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("start_currency_type <=", value, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeIn(List<Byte> values) {
            addCriterion("start_currency_type in", values, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("start_currency_type not in", values, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("start_currency_type between", value1, value2, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("start_currency_type not between", value1, value2, "startCurrencyType");
            return (Criteria) this;
        }

        public Criteria andStartCnDescIsNull() {
            addCriterion("start_cn_desc is null");
            return (Criteria) this;
        }

        public Criteria andStartCnDescIsNotNull() {
            addCriterion("start_cn_desc is not null");
            return (Criteria) this;
        }

        public Criteria andStartCnDescEqualTo(String value) {
            addCriterion("start_cn_desc =", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescNotEqualTo(String value) {
            addCriterion("start_cn_desc <>", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescGreaterThan(String value) {
            addCriterion("start_cn_desc >", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescGreaterThanOrEqualTo(String value) {
            addCriterion("start_cn_desc >=", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescLessThan(String value) {
            addCriterion("start_cn_desc <", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescLessThanOrEqualTo(String value) {
            addCriterion("start_cn_desc <=", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescLike(String value) {
            addCriterion("start_cn_desc like", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescNotLike(String value) {
            addCriterion("start_cn_desc not like", value, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescIn(List<String> values) {
            addCriterion("start_cn_desc in", values, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescNotIn(List<String> values) {
            addCriterion("start_cn_desc not in", values, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescBetween(String value1, String value2) {
            addCriterion("start_cn_desc between", value1, value2, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartCnDescNotBetween(String value1, String value2) {
            addCriterion("start_cn_desc not between", value1, value2, "startCnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescIsNull() {
            addCriterion("start_en_desc is null");
            return (Criteria) this;
        }

        public Criteria andStartEnDescIsNotNull() {
            addCriterion("start_en_desc is not null");
            return (Criteria) this;
        }

        public Criteria andStartEnDescEqualTo(String value) {
            addCriterion("start_en_desc =", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescNotEqualTo(String value) {
            addCriterion("start_en_desc <>", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescGreaterThan(String value) {
            addCriterion("start_en_desc >", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescGreaterThanOrEqualTo(String value) {
            addCriterion("start_en_desc >=", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescLessThan(String value) {
            addCriterion("start_en_desc <", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescLessThanOrEqualTo(String value) {
            addCriterion("start_en_desc <=", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescLike(String value) {
            addCriterion("start_en_desc like", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescNotLike(String value) {
            addCriterion("start_en_desc not like", value, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescIn(List<String> values) {
            addCriterion("start_en_desc in", values, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescNotIn(List<String> values) {
            addCriterion("start_en_desc not in", values, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescBetween(String value1, String value2) {
            addCriterion("start_en_desc between", value1, value2, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andStartEnDescNotBetween(String value1, String value2) {
            addCriterion("start_en_desc not between", value1, value2, "startEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstTimeIsNull() {
            addCriterion("first_time is null");
            return (Criteria) this;
        }

        public Criteria andFirstTimeIsNotNull() {
            addCriterion("first_time is not null");
            return (Criteria) this;
        }

        public Criteria andFirstTimeEqualTo(Date value) {
            addCriterion("first_time =", value, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeNotEqualTo(Date value) {
            addCriterion("first_time <>", value, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeGreaterThan(Date value) {
            addCriterion("first_time >", value, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("first_time >=", value, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeLessThan(Date value) {
            addCriterion("first_time <", value, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeLessThanOrEqualTo(Date value) {
            addCriterion("first_time <=", value, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeIn(List<Date> values) {
            addCriterion("first_time in", values, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeNotIn(List<Date> values) {
            addCriterion("first_time not in", values, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeBetween(Date value1, Date value2) {
            addCriterion("first_time between", value1, value2, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstTimeNotBetween(Date value1, Date value2) {
            addCriterion("first_time not between", value1, value2, "firstTime");
            return (Criteria) this;
        }

        public Criteria andFirstScaleIsNull() {
            addCriterion("first_scale is null");
            return (Criteria) this;
        }

        public Criteria andFirstScaleIsNotNull() {
            addCriterion("first_scale is not null");
            return (Criteria) this;
        }

        public Criteria andFirstScaleEqualTo(BigDecimal value) {
            addCriterion("first_scale =", value, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleNotEqualTo(BigDecimal value) {
            addCriterion("first_scale <>", value, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleGreaterThan(BigDecimal value) {
            addCriterion("first_scale >", value, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("first_scale >=", value, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleLessThan(BigDecimal value) {
            addCriterion("first_scale <", value, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("first_scale <=", value, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleIn(List<BigDecimal> values) {
            addCriterion("first_scale in", values, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleNotIn(List<BigDecimal> values) {
            addCriterion("first_scale not in", values, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("first_scale between", value1, value2, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstScaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("first_scale not between", value1, value2, "firstScale");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeIsNull() {
            addCriterion("first_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeIsNotNull() {
            addCriterion("first_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeEqualTo(Byte value) {
            addCriterion("first_currency_type =", value, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("first_currency_type <>", value, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeGreaterThan(Byte value) {
            addCriterion("first_currency_type >", value, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("first_currency_type >=", value, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeLessThan(Byte value) {
            addCriterion("first_currency_type <", value, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("first_currency_type <=", value, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeIn(List<Byte> values) {
            addCriterion("first_currency_type in", values, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("first_currency_type not in", values, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("first_currency_type between", value1, value2, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("first_currency_type not between", value1, value2, "firstCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescIsNull() {
            addCriterion("first_cn_desc is null");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescIsNotNull() {
            addCriterion("first_cn_desc is not null");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescEqualTo(String value) {
            addCriterion("first_cn_desc =", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescNotEqualTo(String value) {
            addCriterion("first_cn_desc <>", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescGreaterThan(String value) {
            addCriterion("first_cn_desc >", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescGreaterThanOrEqualTo(String value) {
            addCriterion("first_cn_desc >=", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescLessThan(String value) {
            addCriterion("first_cn_desc <", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescLessThanOrEqualTo(String value) {
            addCriterion("first_cn_desc <=", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescLike(String value) {
            addCriterion("first_cn_desc like", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescNotLike(String value) {
            addCriterion("first_cn_desc not like", value, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescIn(List<String> values) {
            addCriterion("first_cn_desc in", values, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescNotIn(List<String> values) {
            addCriterion("first_cn_desc not in", values, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescBetween(String value1, String value2) {
            addCriterion("first_cn_desc between", value1, value2, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstCnDescNotBetween(String value1, String value2) {
            addCriterion("first_cn_desc not between", value1, value2, "firstCnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescIsNull() {
            addCriterion("first_en_desc is null");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescIsNotNull() {
            addCriterion("first_en_desc is not null");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescEqualTo(String value) {
            addCriterion("first_en_desc =", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescNotEqualTo(String value) {
            addCriterion("first_en_desc <>", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescGreaterThan(String value) {
            addCriterion("first_en_desc >", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescGreaterThanOrEqualTo(String value) {
            addCriterion("first_en_desc >=", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescLessThan(String value) {
            addCriterion("first_en_desc <", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescLessThanOrEqualTo(String value) {
            addCriterion("first_en_desc <=", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescLike(String value) {
            addCriterion("first_en_desc like", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescNotLike(String value) {
            addCriterion("first_en_desc not like", value, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescIn(List<String> values) {
            addCriterion("first_en_desc in", values, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescNotIn(List<String> values) {
            addCriterion("first_en_desc not in", values, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescBetween(String value1, String value2) {
            addCriterion("first_en_desc between", value1, value2, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFirstEnDescNotBetween(String value1, String value2) {
            addCriterion("first_en_desc not between", value1, value2, "firstEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeIsNull() {
            addCriterion("finally_time is null");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeIsNotNull() {
            addCriterion("finally_time is not null");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeEqualTo(Date value) {
            addCriterion("finally_time =", value, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeNotEqualTo(Date value) {
            addCriterion("finally_time <>", value, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeGreaterThan(Date value) {
            addCriterion("finally_time >", value, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("finally_time >=", value, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeLessThan(Date value) {
            addCriterion("finally_time <", value, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeLessThanOrEqualTo(Date value) {
            addCriterion("finally_time <=", value, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeIn(List<Date> values) {
            addCriterion("finally_time in", values, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeNotIn(List<Date> values) {
            addCriterion("finally_time not in", values, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeBetween(Date value1, Date value2) {
            addCriterion("finally_time between", value1, value2, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyTimeNotBetween(Date value1, Date value2) {
            addCriterion("finally_time not between", value1, value2, "finallyTime");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleIsNull() {
            addCriterion("finally_scale is null");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleIsNotNull() {
            addCriterion("finally_scale is not null");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleEqualTo(BigDecimal value) {
            addCriterion("finally_scale =", value, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleNotEqualTo(BigDecimal value) {
            addCriterion("finally_scale <>", value, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleGreaterThan(BigDecimal value) {
            addCriterion("finally_scale >", value, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("finally_scale >=", value, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleLessThan(BigDecimal value) {
            addCriterion("finally_scale <", value, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("finally_scale <=", value, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleIn(List<BigDecimal> values) {
            addCriterion("finally_scale in", values, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleNotIn(List<BigDecimal> values) {
            addCriterion("finally_scale not in", values, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("finally_scale between", value1, value2, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyScaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("finally_scale not between", value1, value2, "finallyScale");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeIsNull() {
            addCriterion("finally_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeIsNotNull() {
            addCriterion("finally_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeEqualTo(Byte value) {
            addCriterion("finally_currency_type =", value, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("finally_currency_type <>", value, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeGreaterThan(Byte value) {
            addCriterion("finally_currency_type >", value, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("finally_currency_type >=", value, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeLessThan(Byte value) {
            addCriterion("finally_currency_type <", value, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("finally_currency_type <=", value, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeIn(List<Byte> values) {
            addCriterion("finally_currency_type in", values, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("finally_currency_type not in", values, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("finally_currency_type between", value1, value2, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("finally_currency_type not between", value1, value2, "finallyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescIsNull() {
            addCriterion("finally_cn_desc is null");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescIsNotNull() {
            addCriterion("finally_cn_desc is not null");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescEqualTo(String value) {
            addCriterion("finally_cn_desc =", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescNotEqualTo(String value) {
            addCriterion("finally_cn_desc <>", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescGreaterThan(String value) {
            addCriterion("finally_cn_desc >", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescGreaterThanOrEqualTo(String value) {
            addCriterion("finally_cn_desc >=", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescLessThan(String value) {
            addCriterion("finally_cn_desc <", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescLessThanOrEqualTo(String value) {
            addCriterion("finally_cn_desc <=", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescLike(String value) {
            addCriterion("finally_cn_desc like", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescNotLike(String value) {
            addCriterion("finally_cn_desc not like", value, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescIn(List<String> values) {
            addCriterion("finally_cn_desc in", values, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescNotIn(List<String> values) {
            addCriterion("finally_cn_desc not in", values, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescBetween(String value1, String value2) {
            addCriterion("finally_cn_desc between", value1, value2, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyCnDescNotBetween(String value1, String value2) {
            addCriterion("finally_cn_desc not between", value1, value2, "finallyCnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescIsNull() {
            addCriterion("finally_en_desc is null");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescIsNotNull() {
            addCriterion("finally_en_desc is not null");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescEqualTo(String value) {
            addCriterion("finally_en_desc =", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescNotEqualTo(String value) {
            addCriterion("finally_en_desc <>", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescGreaterThan(String value) {
            addCriterion("finally_en_desc >", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescGreaterThanOrEqualTo(String value) {
            addCriterion("finally_en_desc >=", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescLessThan(String value) {
            addCriterion("finally_en_desc <", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescLessThanOrEqualTo(String value) {
            addCriterion("finally_en_desc <=", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescLike(String value) {
            addCriterion("finally_en_desc like", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescNotLike(String value) {
            addCriterion("finally_en_desc not like", value, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescIn(List<String> values) {
            addCriterion("finally_en_desc in", values, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescNotIn(List<String> values) {
            addCriterion("finally_en_desc not in", values, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescBetween(String value1, String value2) {
            addCriterion("finally_en_desc between", value1, value2, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andFinallyEnDescNotBetween(String value1, String value2) {
            addCriterion("finally_en_desc not between", value1, value2, "finallyEnDesc");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}