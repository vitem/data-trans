package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.PersonageService;
import cn.com.chinaventure.trans.datacenter.entity.Personage;
import cn.com.chinaventure.trans.datacenter.entity.PersonageExample;
import cn.com.chinaventure.trans.datacenter.entity.PersonageExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.PersonageMapper;


@Service("personageService")
public class PersonageServiceImpl implements PersonageService {

	private final static Logger logger = LoggerFactory.getLogger(PersonageServiceImpl.class);

	@Resource
	private PersonageMapper personageMapper;


	@Override
	public void savePersonage(Personage personage) {
		if (null == personage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.personageMapper.insertSelective(personage);
	}

	@Override
	public void updatePersonage(Personage personage) {
		if (null == personage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.personageMapper.updateByPrimaryKeySelective(personage);
	}

	@Override
	public Personage getPersonageById(String id) {
		return personageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Personage> getPersonageList(Personage personage) {
		return this.getPersonageListByParam(personage, null);
	}

	@Override
	public List<Personage> getPersonageListByParam(Personage personage, String orderByStr) {
		PersonageExample example = new PersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Personage> personageList = this.personageMapper.selectByExample(example);
		return personageList;
	}

	@Override
	public PageResult<Personage> findPersonagePageByParam(Personage personage, PageParam pageParam) {
		return this.findPersonagePageByParam(personage, pageParam, null);
	}

	@Override
	public PageResult<Personage> findPersonagePageByParam(Personage personage, PageParam pageParam, String orderByStr) {
		PageResult<Personage> pageResult = new PageResult<Personage>();

		PersonageExample example = new PersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Personage> list = null;// personageMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deletePersonageById(String id) {
		this.personageMapper.deleteByPrimaryKey(id);
	}

}
