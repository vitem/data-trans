package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.OrgFundService;
import cn.com.chinaventure.trans.datacenter.entity.OrgFund;
import cn.com.chinaventure.trans.datacenter.entity.OrgFundExample;
import cn.com.chinaventure.trans.datacenter.entity.OrgFundExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.OrgFundMapper;


@Service("orgFundService")
public class OrgFundServiceImpl implements OrgFundService {

	private final static Logger logger = LoggerFactory.getLogger(OrgFundServiceImpl.class);

	@Resource
	private OrgFundMapper orgFundMapper;


	@Override
	public void saveOrgFund(OrgFund orgFund) {
		if (null == orgFund) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.orgFundMapper.insertSelective(orgFund);
	}

	@Override
	public void updateOrgFund(OrgFund orgFund) {
		if (null == orgFund) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.orgFundMapper.updateByPrimaryKeySelective(orgFund);
	}

	@Override
	public OrgFund getOrgFundById(String id) {
		return orgFundMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<OrgFund> getOrgFundList(OrgFund orgFund) {
		return this.getOrgFundListByParam(orgFund, null);
	}

	@Override
	public List<OrgFund> getOrgFundListByParam(OrgFund orgFund, String orderByStr) {
		OrgFundExample example = new OrgFundExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<OrgFund> orgFundList = this.orgFundMapper.selectByExample(example);
		return orgFundList;
	}

	@Override
	public PageResult<OrgFund> findOrgFundPageByParam(OrgFund orgFund, PageParam pageParam) {
		return this.findOrgFundPageByParam(orgFund, pageParam, null);
	}

	@Override
	public PageResult<OrgFund> findOrgFundPageByParam(OrgFund orgFund, PageParam pageParam, String orderByStr) {
		PageResult<OrgFund> pageResult = new PageResult<OrgFund>();

		OrgFundExample example = new OrgFundExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<OrgFund> list = null;// orgFundMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteOrgFundById(String id) {
		this.orgFundMapper.deleteByPrimaryKey(id);
	}

}
