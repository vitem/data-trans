package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.ManagerService;
import cn.com.chinaventure.trans.datacenter.entity.Manager;
import cn.com.chinaventure.trans.datacenter.entity.ManagerExample;
import cn.com.chinaventure.trans.datacenter.entity.ManagerExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.ManagerMapper;


@Service("managerService")
public class ManagerServiceImpl implements ManagerService {

	private final static Logger logger = LoggerFactory.getLogger(ManagerServiceImpl.class);

	@Resource
	private ManagerMapper managerMapper;


	@Override
	public void saveManager(Manager manager) {
		if (null == manager) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.managerMapper.insertSelective(manager);
	}

	@Override
	public void updateManager(Manager manager) {
		if (null == manager) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.managerMapper.updateByPrimaryKeySelective(manager);
	}

	@Override
	public Manager getManagerById(String id) {
		return managerMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Manager> getManagerList(Manager manager) {
		return this.getManagerListByParam(manager, null);
	}

	@Override
	public List<Manager> getManagerListByParam(Manager manager, String orderByStr) {
		ManagerExample example = new ManagerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Manager> managerList = this.managerMapper.selectByExample(example);
		return managerList;
	}

	@Override
	public PageResult<Manager> findManagerPageByParam(Manager manager, PageParam pageParam) {
		return this.findManagerPageByParam(manager, pageParam, null);
	}

	@Override
	public PageResult<Manager> findManagerPageByParam(Manager manager, PageParam pageParam, String orderByStr) {
		PageResult<Manager> pageResult = new PageResult<Manager>();

		ManagerExample example = new ManagerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Manager> list = null;// managerMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteManagerById(String id) {
		this.managerMapper.deleteByPrimaryKey(id);
	}

}
