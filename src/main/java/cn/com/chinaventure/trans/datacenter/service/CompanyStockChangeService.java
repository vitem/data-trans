package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChange;

/**
 * @author 
 *
 */
public interface CompanyStockChangeService {
	
	/**
     * @api saveCompanyStockChange 新增companyStockChange
     * @apiGroup companyStockChange管理
     * @apiName  新增companyStockChange记录
     * @apiDescription 全量插入companyStockChange记录
     * @apiParam {CompanyStockChange} companyStockChange companyStockChange对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyStockChange(CompanyStockChange companyStockChange);
	
	/**
     * @api updateCompanyStockChange 修改companyStockChange
     * @apiGroup companyStockChange管理
     * @apiName  修改companyStockChange记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyStockChange} companyStockChange companyStockChange对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyStockChange(CompanyStockChange companyStockChange);
	
	/**
     * @api getCompanyStockChangeById 根据companyStockChangeid查询详情
     * @apiGroup companyStockChange管理
     * @apiName  查询companyStockChange详情
     * @apiDescription 根据主键id查询companyStockChange详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyStockChange companyStockChange实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyStockChange getCompanyStockChangeById(String id);
	
	/**
     * @api getCompanyStockChangeList 根据companyStockChange条件查询列表
     * @apiGroup companyStockChange管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyStockChange} companyStockChange  实体条件
     * @apiSuccess List<CompanyStockChange> companyStockChange实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyStockChange> getCompanyStockChangeList(CompanyStockChange companyStockChange);
	

	/**
     * @api getCompanyStockChangeListByParam 根据companyStockChange条件查询列表（含排序）
     * @apiGroup companyStockChange管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyStockChange} companyStockChange  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyStockChange> companyStockChange实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyStockChange> getCompanyStockChangeListByParam(CompanyStockChange companyStockChange, String orderByStr);
	
	
	/**
     * @api findCompanyStockChangePageByParam 根据companyStockChange条件查询列表（分页）
     * @apiGroup companyStockChange管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyStockChange} companyStockChange  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyStockChange> companyStockChange实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyStockChange> findCompanyStockChangePageByParam(CompanyStockChange companyStockChange, PageParam pageParam); 
	
	/**
     * @api findCompanyStockChangePageByParam 根据companyStockChange条件查询列表（分页，含排序）
     * @apiGroup companyStockChange管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyStockChange} companyStockChange  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyStockChange> companyStockChange实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyStockChange> findCompanyStockChangePageByParam(CompanyStockChange companyStockChange, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyStockChangeById 根据companyStockChange的id删除(物理删除)
     * @apiGroup companyStockChange管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyStockChangeById(String id);

}

