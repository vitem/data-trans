package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNotice;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNoticeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyCourtNoticeMapper {
    int countByExample(CompanyCourtNoticeExample example);

    int deleteByExample(CompanyCourtNoticeExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyCourtNotice record);

    int insertSelective(CompanyCourtNotice record);

    List<CompanyCourtNotice> selectByExample(CompanyCourtNoticeExample example);

    CompanyCourtNotice selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyCourtNotice record, @Param("example") CompanyCourtNoticeExample example);

    int updateByExample(@Param("record") CompanyCourtNotice record, @Param("example") CompanyCourtNoticeExample example);

    int updateByPrimaryKeySelective(CompanyCourtNotice record);

    int updateByPrimaryKey(CompanyCourtNotice record);
}