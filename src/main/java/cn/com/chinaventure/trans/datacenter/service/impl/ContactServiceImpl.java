package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.ContactService;
import cn.com.chinaventure.trans.datacenter.entity.Contact;
import cn.com.chinaventure.trans.datacenter.entity.ContactExample;
import cn.com.chinaventure.trans.datacenter.entity.ContactExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.ContactMapper;


@Service("contactService")
public class ContactServiceImpl implements ContactService {

	private final static Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);

	@Resource
	private ContactMapper contactMapper;


	@Override
	public void saveContact(Contact contact) {
		if (null == contact) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.contactMapper.insertSelective(contact);
	}

	@Override
	public void updateContact(Contact contact) {
		if (null == contact) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.contactMapper.updateByPrimaryKeySelective(contact);
	}

	@Override
	public Contact getContactById(String id) {
		return contactMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Contact> getContactList(Contact contact) {
		return this.getContactListByParam(contact, null);
	}

	@Override
	public List<Contact> getContactListByParam(Contact contact, String orderByStr) {
		ContactExample example = new ContactExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Contact> contactList = this.contactMapper.selectByExample(example);
		return contactList;
	}

	@Override
	public PageResult<Contact> findContactPageByParam(Contact contact, PageParam pageParam) {
		return this.findContactPageByParam(contact, pageParam, null);
	}

	@Override
	public PageResult<Contact> findContactPageByParam(Contact contact, PageParam pageParam, String orderByStr) {
		PageResult<Contact> pageResult = new PageResult<Contact>();

		ContactExample example = new ContactExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Contact> list = null;// contactMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteContactById(String id) {
		this.contactMapper.deleteByPrimaryKey(id);
	}

}
