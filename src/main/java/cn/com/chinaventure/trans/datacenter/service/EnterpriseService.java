package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Enterprise;

/**
 * @author 
 *
 */
public interface EnterpriseService {
	
	/**
     * @api saveEnterprise 新增enterprise
     * @apiGroup enterprise管理
     * @apiName  新增enterprise记录
     * @apiDescription 全量插入enterprise记录
     * @apiParam {Enterprise} enterprise enterprise对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise(Enterprise enterprise);
	
	/**
     * @api updateEnterprise 修改enterprise
     * @apiGroup enterprise管理
     * @apiName  修改enterprise记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise} enterprise enterprise对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise(Enterprise enterprise);
	
	/**
     * @api getEnterpriseById 根据enterpriseid查询详情
     * @apiGroup enterprise管理
     * @apiName  查询enterprise详情
     * @apiDescription 根据主键id查询enterprise详情
     * @apiParam {String} id 主键id
     * @apiSuccess Enterprise enterprise实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise getEnterpriseById(String id);
	
	/**
     * @api getEnterpriseList 根据enterprise条件查询列表
     * @apiGroup enterprise管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise} enterprise  实体条件
     * @apiSuccess List<Enterprise> enterprise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise> getEnterpriseList(Enterprise enterprise);
	

	/**
     * @api getEnterpriseListByParam 根据enterprise条件查询列表（含排序）
     * @apiGroup enterprise管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise} enterprise  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise> enterprise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise> getEnterpriseListByParam(Enterprise enterprise, String orderByStr);
	
	
	/**
     * @api findEnterprisePageByParam 根据enterprise条件查询列表（分页）
     * @apiGroup enterprise管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise} enterprise  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise> enterprise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise> findEnterprisePageByParam(Enterprise enterprise, PageParam pageParam); 
	
	/**
     * @api findEnterprisePageByParam 根据enterprise条件查询列表（分页，含排序）
     * @apiGroup enterprise管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise} enterprise  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise> enterprise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise> findEnterprisePageByParam(Enterprise enterprise, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterpriseById 根据enterprise的id删除(物理删除)
     * @apiGroup enterprise管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterpriseById(String id);

}

