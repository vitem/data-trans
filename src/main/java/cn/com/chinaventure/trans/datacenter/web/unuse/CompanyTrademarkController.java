package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyTrademarkService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademark;


@RestController
@RequestMapping("/companyTrademark")
public class CompanyTrademarkController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyTrademarkController.class);

    @Resource
    private CompanyTrademarkService companyTrademarkService;

    /**
	 * @api {post} / 新增companyTrademark
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyTrademark(@RequestBody CompanyTrademark companyTrademark) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyTrademarkService.saveCompanyTrademark(companyTrademark);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyTrademark
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyTrademark(@RequestBody CompanyTrademark companyTrademark) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyTrademarkService.updateCompanyTrademark(companyTrademark);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyTrademark详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyTrademark> getCompanyTrademarkById(@PathVariable String id) {
        JsonResult<CompanyTrademark> jr = new JsonResult<CompanyTrademark>();
        try {
            CompanyTrademark companyTrademark  = companyTrademarkService.getCompanyTrademarkById(id);
            jr = buildJsonResult(companyTrademark);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyTrademark/getCompanyTrademarkList 查询companyTrademark列表(不带分页)
	 * @apiGroup companyTrademark管理
	 * @apiName 查询companyTrademark列表(不带分页)
	 * @apiDescription 根据条件查询companyTrademark列表(不带分页)
	 * @apiParam {CompanyTrademark} companyTrademark companyTrademark对象
	 */
    @RequestMapping("/getCompanyTrademarkList")
    public JsonResult<List<CompanyTrademark>> getCompanyTrademarkList(CompanyTrademark companyTrademark) {
        JsonResult<List<CompanyTrademark>> jr = new JsonResult<List<CompanyTrademark>>();
        try {
            List<CompanyTrademark> list = this.companyTrademarkService.getCompanyTrademarkList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyTrademark/findCompanyTrademarkPageByParam 查询companyTrademark列表(带分页)
	 * @apiGroup companyTrademark管理
	 * @apiName 查询companyTrademark列表(带分页)
	 * @apiDescription 根据条件查询companyTrademark列表(带分页)
	 * @apiParam {CompanyTrademark} companyTrademark companyTrademark对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyTrademarkPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyTrademark>> findCompanyTrademarkPageByParam(@RequestBody CompanyTrademark companyTrademark, PageParam pageParam) {
        JsonResult<PageResult<CompanyTrademark>> jr = new JsonResult<PageResult<CompanyTrademark>>();
        try {
            PageResult<CompanyTrademark> page = this.companyTrademarkService.findCompanyTrademarkPageByParam(companyTrademark, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyTrademark/deleteCompanyTrademarkById 删除companyTrademark(物理删除)
	 * @apiGroup companyTrademark管理
	 * @apiName 删除companyTrademark记录(物理删除)
	 * @apiDescription 根据主键id删除companyTrademark记录(物理删除)
	 * @apiParam {String} id companyTrademark主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyTrademarkById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyTrademarkById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyTrademarkService.deleteCompanyTrademarkById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
