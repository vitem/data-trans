package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyStockChangeService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChange;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChangeExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChangeExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyStockChangeMapper;


@Service("companyStockChangeService")
public class CompanyStockChangeServiceImpl implements CompanyStockChangeService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyStockChangeServiceImpl.class);

	@Resource
	private CompanyStockChangeMapper companyStockChangeMapper;


	@Override
	public void saveCompanyStockChange(CompanyStockChange companyStockChange) {
		if (null == companyStockChange) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyStockChangeMapper.insertSelective(companyStockChange);
	}

	@Override
	public void updateCompanyStockChange(CompanyStockChange companyStockChange) {
		if (null == companyStockChange) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyStockChangeMapper.updateByPrimaryKeySelective(companyStockChange);
	}

	@Override
	public CompanyStockChange getCompanyStockChangeById(String id) {
		return companyStockChangeMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyStockChange> getCompanyStockChangeList(CompanyStockChange companyStockChange) {
		return this.getCompanyStockChangeListByParam(companyStockChange, null);
	}

	@Override
	public List<CompanyStockChange> getCompanyStockChangeListByParam(CompanyStockChange companyStockChange, String orderByStr) {
		CompanyStockChangeExample example = new CompanyStockChangeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyStockChange> companyStockChangeList = this.companyStockChangeMapper.selectByExample(example);
		return companyStockChangeList;
	}

	@Override
	public PageResult<CompanyStockChange> findCompanyStockChangePageByParam(CompanyStockChange companyStockChange, PageParam pageParam) {
		return this.findCompanyStockChangePageByParam(companyStockChange, pageParam, null);
	}

	@Override
	public PageResult<CompanyStockChange> findCompanyStockChangePageByParam(CompanyStockChange companyStockChange, PageParam pageParam, String orderByStr) {
		PageResult<CompanyStockChange> pageResult = new PageResult<CompanyStockChange>();

		CompanyStockChangeExample example = new CompanyStockChangeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyStockChange> list = null;// companyStockChangeMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyStockChangeById(String id) {
		this.companyStockChangeMapper.deleteByPrimaryKey(id);
	}

}
