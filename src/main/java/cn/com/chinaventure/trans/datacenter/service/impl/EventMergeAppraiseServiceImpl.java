package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeAppraiseService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraise;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraiseExample;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraiseExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventMergeAppraiseMapper;


@Service("eventMergeAppraiseService")
public class EventMergeAppraiseServiceImpl implements EventMergeAppraiseService {

	private final static Logger logger = LoggerFactory.getLogger(EventMergeAppraiseServiceImpl.class);

	@Resource
	private EventMergeAppraiseMapper eventMergeAppraiseMapper;


	@Override
	public void saveEventMergeAppraise(EventMergeAppraise eventMergeAppraise) {
		if (null == eventMergeAppraise) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeAppraiseMapper.insertSelective(eventMergeAppraise);
	}

	@Override
	public void updateEventMergeAppraise(EventMergeAppraise eventMergeAppraise) {
		if (null == eventMergeAppraise) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeAppraiseMapper.updateByPrimaryKeySelective(eventMergeAppraise);
	}

	@Override
	public EventMergeAppraise getEventMergeAppraiseById(String id) {
		return eventMergeAppraiseMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventMergeAppraise> getEventMergeAppraiseList(EventMergeAppraise eventMergeAppraise) {
		return this.getEventMergeAppraiseListByParam(eventMergeAppraise, null);
	}

	@Override
	public List<EventMergeAppraise> getEventMergeAppraiseListByParam(EventMergeAppraise eventMergeAppraise, String orderByStr) {
		EventMergeAppraiseExample example = new EventMergeAppraiseExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventMergeAppraise> eventMergeAppraiseList = this.eventMergeAppraiseMapper.selectByExample(example);
		return eventMergeAppraiseList;
	}

	@Override
	public PageResult<EventMergeAppraise> findEventMergeAppraisePageByParam(EventMergeAppraise eventMergeAppraise, PageParam pageParam) {
		return this.findEventMergeAppraisePageByParam(eventMergeAppraise, pageParam, null);
	}

	@Override
	public PageResult<EventMergeAppraise> findEventMergeAppraisePageByParam(EventMergeAppraise eventMergeAppraise, PageParam pageParam, String orderByStr) {
		PageResult<EventMergeAppraise> pageResult = new PageResult<EventMergeAppraise>();

		EventMergeAppraiseExample example = new EventMergeAppraiseExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventMergeAppraise> list = null;// eventMergeAppraiseMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventMergeAppraiseById(String id) {
		this.eventMergeAppraiseMapper.deleteByPrimaryKey(id);
	}

}
