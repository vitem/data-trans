package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.FundCollect;
import cn.com.chinaventure.trans.datacenter.entity.FundCollectExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FundCollectMapper {
    int countByExample(FundCollectExample example);

    int deleteByExample(FundCollectExample example);

    int deleteByPrimaryKey(String id);

    int insert(FundCollect record);

    int insertSelective(FundCollect record);

    List<FundCollect> selectByExample(FundCollectExample example);

    FundCollect selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") FundCollect record, @Param("example") FundCollectExample example);

    int updateByExample(@Param("record") FundCollect record, @Param("example") FundCollectExample example);

    int updateByPrimaryKeySelective(FundCollect record);

    int updateByPrimaryKey(FundCollect record);
}