package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventInvestOrgService;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrg;


@RestController
@RequestMapping("/eventInvestOrg")
public class EventInvestOrgController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventInvestOrgController.class);

    @Resource
    private EventInvestOrgService eventInvestOrgService;

    /**
	 * @api {post} / 新增eventInvestOrg
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventInvestOrg(@RequestBody EventInvestOrg eventInvestOrg) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventInvestOrgService.saveEventInvestOrg(eventInvestOrg);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventInvestOrg
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventInvestOrg(@RequestBody EventInvestOrg eventInvestOrg) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventInvestOrgService.updateEventInvestOrg(eventInvestOrg);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventInvestOrg详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventInvestOrg> getEventInvestOrgById(@PathVariable String id) {
        JsonResult<EventInvestOrg> jr = new JsonResult<EventInvestOrg>();
        try {
            EventInvestOrg eventInvestOrg  = eventInvestOrgService.getEventInvestOrgById(id);
            jr = buildJsonResult(eventInvestOrg);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventInvestOrg/getEventInvestOrgList 查询eventInvestOrg列表(不带分页)
	 * @apiGroup eventInvestOrg管理
	 * @apiName 查询eventInvestOrg列表(不带分页)
	 * @apiDescription 根据条件查询eventInvestOrg列表(不带分页)
	 * @apiParam {EventInvestOrg} eventInvestOrg eventInvestOrg对象
	 */
    @RequestMapping("/getEventInvestOrgList")
    public JsonResult<List<EventInvestOrg>> getEventInvestOrgList(EventInvestOrg eventInvestOrg) {
        JsonResult<List<EventInvestOrg>> jr = new JsonResult<List<EventInvestOrg>>();
        try {
            List<EventInvestOrg> list = this.eventInvestOrgService.getEventInvestOrgList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventInvestOrg/findEventInvestOrgPageByParam 查询eventInvestOrg列表(带分页)
	 * @apiGroup eventInvestOrg管理
	 * @apiName 查询eventInvestOrg列表(带分页)
	 * @apiDescription 根据条件查询eventInvestOrg列表(带分页)
	 * @apiParam {EventInvestOrg} eventInvestOrg eventInvestOrg对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventInvestOrgPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventInvestOrg>> findEventInvestOrgPageByParam(@RequestBody EventInvestOrg eventInvestOrg, PageParam pageParam) {
        JsonResult<PageResult<EventInvestOrg>> jr = new JsonResult<PageResult<EventInvestOrg>>();
        try {
            PageResult<EventInvestOrg> page = this.eventInvestOrgService.findEventInvestOrgPageByParam(eventInvestOrg, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventInvestOrg/deleteEventInvestOrgById 删除eventInvestOrg(物理删除)
	 * @apiGroup eventInvestOrg管理
	 * @apiName 删除eventInvestOrg记录(物理删除)
	 * @apiDescription 根据主键id删除eventInvestOrg记录(物理删除)
	 * @apiParam {String} id eventInvestOrg主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventInvestOrgById",method=RequestMethod.DELETE)
    public JsonResult deleteEventInvestOrgById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventInvestOrgService.deleteEventInvestOrgById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
