package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyTrademarkService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademark;
import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademarkExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademarkExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyTrademarkMapper;


@Service("companyTrademarkService")
public class CompanyTrademarkServiceImpl implements CompanyTrademarkService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyTrademarkServiceImpl.class);

	@Resource
	private CompanyTrademarkMapper companyTrademarkMapper;


	@Override
	public void saveCompanyTrademark(CompanyTrademark companyTrademark) {
		if (null == companyTrademark) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyTrademarkMapper.insertSelective(companyTrademark);
	}

	@Override
	public void updateCompanyTrademark(CompanyTrademark companyTrademark) {
		if (null == companyTrademark) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyTrademarkMapper.updateByPrimaryKeySelective(companyTrademark);
	}

	@Override
	public CompanyTrademark getCompanyTrademarkById(String id) {
		return companyTrademarkMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyTrademark> getCompanyTrademarkList(CompanyTrademark companyTrademark) {
		return this.getCompanyTrademarkListByParam(companyTrademark, null);
	}

	@Override
	public List<CompanyTrademark> getCompanyTrademarkListByParam(CompanyTrademark companyTrademark, String orderByStr) {
		CompanyTrademarkExample example = new CompanyTrademarkExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyTrademark> companyTrademarkList = this.companyTrademarkMapper.selectByExample(example);
		return companyTrademarkList;
	}

	@Override
	public PageResult<CompanyTrademark> findCompanyTrademarkPageByParam(CompanyTrademark companyTrademark, PageParam pageParam) {
		return this.findCompanyTrademarkPageByParam(companyTrademark, pageParam, null);
	}

	@Override
	public PageResult<CompanyTrademark> findCompanyTrademarkPageByParam(CompanyTrademark companyTrademark, PageParam pageParam, String orderByStr) {
		PageResult<CompanyTrademark> pageResult = new PageResult<CompanyTrademark>();

		CompanyTrademarkExample example = new CompanyTrademarkExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyTrademark> list = null;// companyTrademarkMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyTrademarkById(String id) {
		this.companyTrademarkMapper.deleteByPrimaryKey(id);
	}

}
