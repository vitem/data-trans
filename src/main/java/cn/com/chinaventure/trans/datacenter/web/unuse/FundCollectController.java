package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.FundCollectService;
import cn.com.chinaventure.trans.datacenter.entity.FundCollect;


@RestController
@RequestMapping("/fundCollect")
public class FundCollectController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(FundCollectController.class);

    @Resource
    private FundCollectService fundCollectService;

    /**
	 * @api {post} / 新增fundCollect
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveFundCollect(@RequestBody FundCollect fundCollect) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundCollectService.saveFundCollect(fundCollect);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改fundCollect
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateFundCollect(@RequestBody FundCollect fundCollect) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundCollectService.updateFundCollect(fundCollect);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询fundCollect详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<FundCollect> getFundCollectById(@PathVariable String id) {
        JsonResult<FundCollect> jr = new JsonResult<FundCollect>();
        try {
            FundCollect fundCollect  = fundCollectService.getFundCollectById(id);
            jr = buildJsonResult(fundCollect);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /fundCollect/getFundCollectList 查询fundCollect列表(不带分页)
	 * @apiGroup fundCollect管理
	 * @apiName 查询fundCollect列表(不带分页)
	 * @apiDescription 根据条件查询fundCollect列表(不带分页)
	 * @apiParam {FundCollect} fundCollect fundCollect对象
	 */
    @RequestMapping("/getFundCollectList")
    public JsonResult<List<FundCollect>> getFundCollectList(FundCollect fundCollect) {
        JsonResult<List<FundCollect>> jr = new JsonResult<List<FundCollect>>();
        try {
            List<FundCollect> list = this.fundCollectService.getFundCollectList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /fundCollect/findFundCollectPageByParam 查询fundCollect列表(带分页)
	 * @apiGroup fundCollect管理
	 * @apiName 查询fundCollect列表(带分页)
	 * @apiDescription 根据条件查询fundCollect列表(带分页)
	 * @apiParam {FundCollect} fundCollect fundCollect对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findFundCollectPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<FundCollect>> findFundCollectPageByParam(@RequestBody FundCollect fundCollect, PageParam pageParam) {
        JsonResult<PageResult<FundCollect>> jr = new JsonResult<PageResult<FundCollect>>();
        try {
            PageResult<FundCollect> page = this.fundCollectService.findFundCollectPageByParam(fundCollect, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /fundCollect/deleteFundCollectById 删除fundCollect(物理删除)
	 * @apiGroup fundCollect管理
	 * @apiName 删除fundCollect记录(物理删除)
	 * @apiDescription 根据主键id删除fundCollect记录(物理删除)
	 * @apiParam {String} id fundCollect主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteFundCollectById",method=RequestMethod.DELETE)
    public JsonResult deleteFundCollectById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundCollectService.deleteFundCollectById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
