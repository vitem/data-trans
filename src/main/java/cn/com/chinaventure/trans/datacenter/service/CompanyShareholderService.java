package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholder;

/**
 * @author 
 *
 */
public interface CompanyShareholderService {
	
	/**
     * @api saveCompanyShareholder 新增companyShareholder
     * @apiGroup companyShareholder管理
     * @apiName  新增companyShareholder记录
     * @apiDescription 全量插入companyShareholder记录
     * @apiParam {CompanyShareholder} companyShareholder companyShareholder对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyShareholder(CompanyShareholder companyShareholder);
	
	/**
     * @api updateCompanyShareholder 修改companyShareholder
     * @apiGroup companyShareholder管理
     * @apiName  修改companyShareholder记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyShareholder} companyShareholder companyShareholder对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyShareholder(CompanyShareholder companyShareholder);
	
	/**
     * @api getCompanyShareholderById 根据companyShareholderid查询详情
     * @apiGroup companyShareholder管理
     * @apiName  查询companyShareholder详情
     * @apiDescription 根据主键id查询companyShareholder详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyShareholder companyShareholder实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyShareholder getCompanyShareholderById(String id);
	
	/**
     * @api getCompanyShareholderList 根据companyShareholder条件查询列表
     * @apiGroup companyShareholder管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyShareholder} companyShareholder  实体条件
     * @apiSuccess List<CompanyShareholder> companyShareholder实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyShareholder> getCompanyShareholderList(CompanyShareholder companyShareholder);
	

	/**
     * @api getCompanyShareholderListByParam 根据companyShareholder条件查询列表（含排序）
     * @apiGroup companyShareholder管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyShareholder} companyShareholder  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyShareholder> companyShareholder实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyShareholder> getCompanyShareholderListByParam(CompanyShareholder companyShareholder, String orderByStr);
	
	
	/**
     * @api findCompanyShareholderPageByParam 根据companyShareholder条件查询列表（分页）
     * @apiGroup companyShareholder管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyShareholder} companyShareholder  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyShareholder> companyShareholder实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyShareholder> findCompanyShareholderPageByParam(CompanyShareholder companyShareholder, PageParam pageParam); 
	
	/**
     * @api findCompanyShareholderPageByParam 根据companyShareholder条件查询列表（分页，含排序）
     * @apiGroup companyShareholder管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyShareholder} companyShareholder  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyShareholder> companyShareholder实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyShareholder> findCompanyShareholderPageByParam(CompanyShareholder companyShareholder, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyShareholderById 根据companyShareholder的id删除(物理删除)
     * @apiGroup companyShareholder管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyShareholderById(String id);

}

