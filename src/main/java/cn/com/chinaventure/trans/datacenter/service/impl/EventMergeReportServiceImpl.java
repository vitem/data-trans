package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeReportService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeReport;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeReportExample;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeReportExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventMergeReportMapper;


@Service("eventMergeReportService")
public class EventMergeReportServiceImpl implements EventMergeReportService {

	private final static Logger logger = LoggerFactory.getLogger(EventMergeReportServiceImpl.class);

	@Resource
	private EventMergeReportMapper eventMergeReportMapper;


	@Override
	public void saveEventMergeReport(EventMergeReport eventMergeReport) {
		if (null == eventMergeReport) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeReportMapper.insertSelective(eventMergeReport);
	}

	@Override
	public void updateEventMergeReport(EventMergeReport eventMergeReport) {
		if (null == eventMergeReport) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeReportMapper.updateByPrimaryKeySelective(eventMergeReport);
	}

	@Override
	public EventMergeReport getEventMergeReportById(String id) {
		return eventMergeReportMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventMergeReport> getEventMergeReportList(EventMergeReport eventMergeReport) {
		return this.getEventMergeReportListByParam(eventMergeReport, null);
	}

	@Override
	public List<EventMergeReport> getEventMergeReportListByParam(EventMergeReport eventMergeReport, String orderByStr) {
		EventMergeReportExample example = new EventMergeReportExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventMergeReport> eventMergeReportList = this.eventMergeReportMapper.selectByExample(example);
		return eventMergeReportList;
	}

	@Override
	public PageResult<EventMergeReport> findEventMergeReportPageByParam(EventMergeReport eventMergeReport, PageParam pageParam) {
		return this.findEventMergeReportPageByParam(eventMergeReport, pageParam, null);
	}

	@Override
	public PageResult<EventMergeReport> findEventMergeReportPageByParam(EventMergeReport eventMergeReport, PageParam pageParam, String orderByStr) {
		PageResult<EventMergeReport> pageResult = new PageResult<EventMergeReport>();

		EventMergeReportExample example = new EventMergeReportExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventMergeReport> list = null;// eventMergeReportMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventMergeReportById(String id) {
		this.eventMergeReportMapper.deleteByPrimaryKey(id);
	}

}
