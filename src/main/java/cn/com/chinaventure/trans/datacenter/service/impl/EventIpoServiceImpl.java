package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventIpoService;
import cn.com.chinaventure.trans.datacenter.entity.EventIpo;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoExample;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventIpoMapper;


@Service("eventIpoService")
public class EventIpoServiceImpl implements EventIpoService {

	private final static Logger logger = LoggerFactory.getLogger(EventIpoServiceImpl.class);

	@Resource
	private EventIpoMapper eventIpoMapper;


	@Override
	public void saveEventIpo(EventIpo eventIpo) {
		if (null == eventIpo) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventIpoMapper.insertSelective(eventIpo);
	}

	@Override
	public void updateEventIpo(EventIpo eventIpo) {
		if (null == eventIpo) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventIpoMapper.updateByPrimaryKeySelective(eventIpo);
	}

	@Override
	public EventIpo getEventIpoById(String id) {
		return eventIpoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventIpo> getEventIpoList(EventIpo eventIpo) {
		return this.getEventIpoListByParam(eventIpo, null);
	}

	@Override
	public List<EventIpo> getEventIpoListByParam(EventIpo eventIpo, String orderByStr) {
		EventIpoExample example = new EventIpoExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventIpo> eventIpoList = this.eventIpoMapper.selectByExample(example);
		return eventIpoList;
	}

	@Override
	public PageResult<EventIpo> findEventIpoPageByParam(EventIpo eventIpo, PageParam pageParam) {
		return this.findEventIpoPageByParam(eventIpo, pageParam, null);
	}

	@Override
	public PageResult<EventIpo> findEventIpoPageByParam(EventIpo eventIpo, PageParam pageParam, String orderByStr) {
		PageResult<EventIpo> pageResult = new PageResult<EventIpo>();

		EventIpoExample example = new EventIpoExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventIpo> list = null;// eventIpoMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventIpoById(String id) {
		this.eventIpoMapper.deleteByPrimaryKey(id);
	}

}
