package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventIpo;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventIpoMapper {
    int countByExample(EventIpoExample example);

    int deleteByExample(EventIpoExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventIpo record);

    int insertSelective(EventIpo record);

    List<EventIpo> selectByExample(EventIpoExample example);

    EventIpo selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventIpo record, @Param("example") EventIpoExample example);

    int updateByExample(@Param("record") EventIpo record, @Param("example") EventIpoExample example);

    int updateByPrimaryKeySelective(EventIpo record);

    int updateByPrimaryKey(EventIpo record);
}