package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Event;

/**
 * @author 
 *
 */
public interface EventService {
	
	/**
     * @api saveEvent 新增event
     * @apiGroup event管理
     * @apiName  新增event记录
     * @apiDescription 全量插入event记录
     * @apiParam {Event} event event对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEvent(Event event);
	
	/**
     * @api updateEvent 修改event
     * @apiGroup event管理
     * @apiName  修改event记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Event} event event对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEvent(Event event);
	
	/**
     * @api getEventById 根据eventid查询详情
     * @apiGroup event管理
     * @apiName  查询event详情
     * @apiDescription 根据主键id查询event详情
     * @apiParam {String} id 主键id
     * @apiSuccess Event event实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Event getEventById(String id);
	
	/**
     * @api getEventList 根据event条件查询列表
     * @apiGroup event管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Event} event  实体条件
     * @apiSuccess List<Event> event实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Event> getEventList(Event event);
	

	/**
     * @api getEventListByParam 根据event条件查询列表（含排序）
     * @apiGroup event管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Event} event  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Event> event实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Event> getEventListByParam(Event event, String orderByStr);
	
	
	/**
     * @api findEventPageByParam 根据event条件查询列表（分页）
     * @apiGroup event管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Event} event  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Event> event实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Event> findEventPageByParam(Event event, PageParam pageParam); 
	
	/**
     * @api findEventPageByParam 根据event条件查询列表（分页，含排序）
     * @apiGroup event管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Event} event  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Event> event实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Event> findEventPageByParam(Event event, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventById 根据event的id删除(物理删除)
     * @apiGroup event管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventById(String id);

}

