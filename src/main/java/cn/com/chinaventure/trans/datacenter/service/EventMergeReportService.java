package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeReport;

/**
 * @author 
 *
 */
public interface EventMergeReportService {
	
	/**
     * @api saveEventMergeReport 新增eventMergeReport
     * @apiGroup eventMergeReport管理
     * @apiName  新增eventMergeReport记录
     * @apiDescription 全量插入eventMergeReport记录
     * @apiParam {EventMergeReport} eventMergeReport eventMergeReport对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventMergeReport(EventMergeReport eventMergeReport);
	
	/**
     * @api updateEventMergeReport 修改eventMergeReport
     * @apiGroup eventMergeReport管理
     * @apiName  修改eventMergeReport记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventMergeReport} eventMergeReport eventMergeReport对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventMergeReport(EventMergeReport eventMergeReport);
	
	/**
     * @api getEventMergeReportById 根据eventMergeReportid查询详情
     * @apiGroup eventMergeReport管理
     * @apiName  查询eventMergeReport详情
     * @apiDescription 根据主键id查询eventMergeReport详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventMergeReport eventMergeReport实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventMergeReport getEventMergeReportById(String id);
	
	/**
     * @api getEventMergeReportList 根据eventMergeReport条件查询列表
     * @apiGroup eventMergeReport管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventMergeReport} eventMergeReport  实体条件
     * @apiSuccess List<EventMergeReport> eventMergeReport实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeReport> getEventMergeReportList(EventMergeReport eventMergeReport);
	

	/**
     * @api getEventMergeReportListByParam 根据eventMergeReport条件查询列表（含排序）
     * @apiGroup eventMergeReport管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventMergeReport} eventMergeReport  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventMergeReport> eventMergeReport实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeReport> getEventMergeReportListByParam(EventMergeReport eventMergeReport, String orderByStr);
	
	
	/**
     * @api findEventMergeReportPageByParam 根据eventMergeReport条件查询列表（分页）
     * @apiGroup eventMergeReport管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventMergeReport} eventMergeReport  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventMergeReport> eventMergeReport实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeReport> findEventMergeReportPageByParam(EventMergeReport eventMergeReport, PageParam pageParam); 
	
	/**
     * @api findEventMergeReportPageByParam 根据eventMergeReport条件查询列表（分页，含排序）
     * @apiGroup eventMergeReport管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventMergeReport} eventMergeReport  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventMergeReport> eventMergeReport实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeReport> findEventMergeReportPageByParam(EventMergeReport eventMergeReport, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventMergeReportById 根据eventMergeReport的id删除(物理删除)
     * @apiGroup eventMergeReport管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventMergeReportById(String id);

}

