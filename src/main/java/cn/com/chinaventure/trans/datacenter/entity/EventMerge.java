package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.util.Date;

public class EventMerge implements Serializable {
    private String id;

    private String eventId;

    private Byte tradeStatus;

    /**
     * 开始时间
     */
    private Date startTime;

    /**
     * 并购结束时间  对应发生时间
     */
    private Date endTime;

    /**
     * 地域  1，全求 2，亚州 3，大中华 4，中国
     */
    private Byte regionType;

    private Byte mergeType;

    /**
     *  1 是 2否
     */
    private Byte tradeLinkStatus;

    /**
     * 是否有VC/PE支持  1是 2 否
     */
    private Byte vcpeStatus;

    /**
     * 投行
     */
    private String investOrgId;

    /**
     * 投行人物
     */
    private String investPersonageId;

    /**
     * 律师事务所
     */
    private String lawOrgId;

    /**
     * 律师事务所人物
     */
    private String lawPersonageId;

    /**
     * 会计事务所
     */
    private String accountingOrgId;

    /**
     * 会计事务所人物
     */
    private String accountingPersonageId;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId == null ? null : eventId.trim();
    }

    public Byte getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(Byte tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Byte getRegionType() {
        return regionType;
    }

    public void setRegionType(Byte regionType) {
        this.regionType = regionType;
    }

    public Byte getMergeType() {
        return mergeType;
    }

    public void setMergeType(Byte mergeType) {
        this.mergeType = mergeType;
    }

    public Byte getTradeLinkStatus() {
        return tradeLinkStatus;
    }

    public void setTradeLinkStatus(Byte tradeLinkStatus) {
        this.tradeLinkStatus = tradeLinkStatus;
    }

    public Byte getVcpeStatus() {
        return vcpeStatus;
    }

    public void setVcpeStatus(Byte vcpeStatus) {
        this.vcpeStatus = vcpeStatus;
    }

    public String getInvestOrgId() {
        return investOrgId;
    }

    public void setInvestOrgId(String investOrgId) {
        this.investOrgId = investOrgId == null ? null : investOrgId.trim();
    }

    public String getInvestPersonageId() {
        return investPersonageId;
    }

    public void setInvestPersonageId(String investPersonageId) {
        this.investPersonageId = investPersonageId == null ? null : investPersonageId.trim();
    }

    public String getLawOrgId() {
        return lawOrgId;
    }

    public void setLawOrgId(String lawOrgId) {
        this.lawOrgId = lawOrgId == null ? null : lawOrgId.trim();
    }

    public String getLawPersonageId() {
        return lawPersonageId;
    }

    public void setLawPersonageId(String lawPersonageId) {
        this.lawPersonageId = lawPersonageId == null ? null : lawPersonageId.trim();
    }

    public String getAccountingOrgId() {
        return accountingOrgId;
    }

    public void setAccountingOrgId(String accountingOrgId) {
        this.accountingOrgId = accountingOrgId == null ? null : accountingOrgId.trim();
    }

    public String getAccountingPersonageId() {
        return accountingPersonageId;
    }

    public void setAccountingPersonageId(String accountingPersonageId) {
        this.accountingPersonageId = accountingPersonageId == null ? null : accountingPersonageId.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventId=").append(eventId);
        sb.append(", tradeStatus=").append(tradeStatus);
        sb.append(", startTime=").append(startTime);
        sb.append(", endTime=").append(endTime);
        sb.append(", regionType=").append(regionType);
        sb.append(", mergeType=").append(mergeType);
        sb.append(", tradeLinkStatus=").append(tradeLinkStatus);
        sb.append(", vcpeStatus=").append(vcpeStatus);
        sb.append(", investOrgId=").append(investOrgId);
        sb.append(", investPersonageId=").append(investPersonageId);
        sb.append(", lawOrgId=").append(lawOrgId);
        sb.append(", lawPersonageId=").append(lawPersonageId);
        sb.append(", accountingOrgId=").append(accountingOrgId);
        sb.append(", accountingPersonageId=").append(accountingPersonageId);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}