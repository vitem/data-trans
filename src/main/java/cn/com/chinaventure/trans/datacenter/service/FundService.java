package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Fund;

/**
 * @author 
 *
 */
public interface FundService {
	
	/**
     * @api saveFund 新增fund
     * @apiGroup fund管理
     * @apiName  新增fund记录
     * @apiDescription 全量插入fund记录
     * @apiParam {Fund} fund fund对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveFund(Fund fund);
	
	/**
     * @api updateFund 修改fund
     * @apiGroup fund管理
     * @apiName  修改fund记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Fund} fund fund对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateFund(Fund fund);
	
	/**
     * @api getFundById 根据fundid查询详情
     * @apiGroup fund管理
     * @apiName  查询fund详情
     * @apiDescription 根据主键id查询fund详情
     * @apiParam {String} id 主键id
     * @apiSuccess Fund fund实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Fund getFundById(String id);
	
	/**
     * @api getFundList 根据fund条件查询列表
     * @apiGroup fund管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Fund} fund  实体条件
     * @apiSuccess List<Fund> fund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Fund> getFundList(Fund fund);
	

	/**
     * @api getFundListByParam 根据fund条件查询列表（含排序）
     * @apiGroup fund管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Fund} fund  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Fund> fund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Fund> getFundListByParam(Fund fund, String orderByStr);
	
	
	/**
     * @api findFundPageByParam 根据fund条件查询列表（分页）
     * @apiGroup fund管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Fund} fund  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Fund> fund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Fund> findFundPageByParam(Fund fund, PageParam pageParam); 
	
	/**
     * @api findFundPageByParam 根据fund条件查询列表（分页，含排序）
     * @apiGroup fund管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Fund} fund  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Fund> fund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Fund> findFundPageByParam(Fund fund, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteFundById 根据fund的id删除(物理删除)
     * @apiGroup fund管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteFundById(String id);

}

