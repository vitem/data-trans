package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrg;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrgExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventInvestOrgMapper {
    int countByExample(EventInvestOrgExample example);

    int deleteByExample(EventInvestOrgExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventInvestOrg record);

    int insertSelective(EventInvestOrg record);

    List<EventInvestOrg> selectByExample(EventInvestOrgExample example);

    EventInvestOrg selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventInvestOrg record, @Param("example") EventInvestOrgExample example);

    int updateByExample(@Param("record") EventInvestOrg record, @Param("example") EventInvestOrgExample example);

    int updateByPrimaryKeySelective(EventInvestOrg record);

    int updateByPrimaryKey(EventInvestOrg record);
}