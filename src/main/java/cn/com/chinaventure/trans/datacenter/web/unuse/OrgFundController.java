package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.OrgFundService;
import cn.com.chinaventure.trans.datacenter.entity.OrgFund;


@RestController
@RequestMapping("/orgFund")
public class OrgFundController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(OrgFundController.class);

    @Resource
    private OrgFundService orgFundService;

    /**
	 * @api {post} / 新增orgFund
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveOrgFund(@RequestBody OrgFund orgFund) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.orgFundService.saveOrgFund(orgFund);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改orgFund
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateOrgFund(@RequestBody OrgFund orgFund) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.orgFundService.updateOrgFund(orgFund);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询orgFund详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<OrgFund> getOrgFundById(@PathVariable String id) {
        JsonResult<OrgFund> jr = new JsonResult<OrgFund>();
        try {
            OrgFund orgFund  = orgFundService.getOrgFundById(id);
            jr = buildJsonResult(orgFund);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /orgFund/getOrgFundList 查询orgFund列表(不带分页)
	 * @apiGroup orgFund管理
	 * @apiName 查询orgFund列表(不带分页)
	 * @apiDescription 根据条件查询orgFund列表(不带分页)
	 * @apiParam {OrgFund} orgFund orgFund对象
	 */
    @RequestMapping("/getOrgFundList")
    public JsonResult<List<OrgFund>> getOrgFundList(OrgFund orgFund) {
        JsonResult<List<OrgFund>> jr = new JsonResult<List<OrgFund>>();
        try {
            List<OrgFund> list = this.orgFundService.getOrgFundList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /orgFund/findOrgFundPageByParam 查询orgFund列表(带分页)
	 * @apiGroup orgFund管理
	 * @apiName 查询orgFund列表(带分页)
	 * @apiDescription 根据条件查询orgFund列表(带分页)
	 * @apiParam {OrgFund} orgFund orgFund对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findOrgFundPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<OrgFund>> findOrgFundPageByParam(@RequestBody OrgFund orgFund, PageParam pageParam) {
        JsonResult<PageResult<OrgFund>> jr = new JsonResult<PageResult<OrgFund>>();
        try {
            PageResult<OrgFund> page = this.orgFundService.findOrgFundPageByParam(orgFund, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /orgFund/deleteOrgFundById 删除orgFund(物理删除)
	 * @apiGroup orgFund管理
	 * @apiName 删除orgFund记录(物理删除)
	 * @apiDescription 根据主键id删除orgFund记录(物理删除)
	 * @apiParam {String} id orgFund主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteOrgFundById",method=RequestMethod.DELETE)
    public JsonResult deleteOrgFundById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.orgFundService.deleteOrgFundById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
