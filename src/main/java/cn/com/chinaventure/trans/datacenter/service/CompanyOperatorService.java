package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyOperator;

/**
 * @author 
 *
 */
public interface CompanyOperatorService {
	
	/**
     * @api saveCompanyOperator 新增companyOperator
     * @apiGroup companyOperator管理
     * @apiName  新增companyOperator记录
     * @apiDescription 全量插入companyOperator记录
     * @apiParam {CompanyOperator} companyOperator companyOperator对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyOperator(CompanyOperator companyOperator);
	
	/**
     * @api updateCompanyOperator 修改companyOperator
     * @apiGroup companyOperator管理
     * @apiName  修改companyOperator记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyOperator} companyOperator companyOperator对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyOperator(CompanyOperator companyOperator);
	
	/**
     * @api getCompanyOperatorById 根据companyOperatorid查询详情
     * @apiGroup companyOperator管理
     * @apiName  查询companyOperator详情
     * @apiDescription 根据主键id查询companyOperator详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyOperator companyOperator实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyOperator getCompanyOperatorById(String id);
	
	/**
     * @api getCompanyOperatorList 根据companyOperator条件查询列表
     * @apiGroup companyOperator管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyOperator} companyOperator  实体条件
     * @apiSuccess List<CompanyOperator> companyOperator实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyOperator> getCompanyOperatorList(CompanyOperator companyOperator);
	

	/**
     * @api getCompanyOperatorListByParam 根据companyOperator条件查询列表（含排序）
     * @apiGroup companyOperator管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyOperator} companyOperator  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyOperator> companyOperator实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyOperator> getCompanyOperatorListByParam(CompanyOperator companyOperator, String orderByStr);
	
	
	/**
     * @api findCompanyOperatorPageByParam 根据companyOperator条件查询列表（分页）
     * @apiGroup companyOperator管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyOperator} companyOperator  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyOperator> companyOperator实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyOperator> findCompanyOperatorPageByParam(CompanyOperator companyOperator, PageParam pageParam); 
	
	/**
     * @api findCompanyOperatorPageByParam 根据companyOperator条件查询列表（分页，含排序）
     * @apiGroup companyOperator管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyOperator} companyOperator  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyOperator> companyOperator实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyOperator> findCompanyOperatorPageByParam(CompanyOperator companyOperator, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyOperatorById 根据companyOperator的id删除(物理删除)
     * @apiGroup companyOperator管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyOperatorById(String id);

}

