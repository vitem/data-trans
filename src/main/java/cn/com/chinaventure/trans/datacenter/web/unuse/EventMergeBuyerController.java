package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyer;
import cn.com.chinaventure.trans.datacenter.service.EventMergeBuyerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/eventMergeBuyer")
public class EventMergeBuyerController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventMergeBuyerController.class);

    @Resource
    private EventMergeBuyerService eventMergeBuyerService;

    /**
	 * @api {post} / 新增eventMergeBuyer
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventMergeBuyer(@RequestBody EventMergeBuyer eventMergeBuyer) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeBuyerService.saveEventMergeBuyer(eventMergeBuyer);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventMergeBuyer
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventMergeBuyer(@RequestBody EventMergeBuyer eventMergeBuyer) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeBuyerService.updateEventMergeBuyer(eventMergeBuyer);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventMergeBuyer详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventMergeBuyer> getEventMergeBuyerById(@PathVariable String id) {
        JsonResult<EventMergeBuyer> jr = new JsonResult<EventMergeBuyer>();
        try {
            EventMergeBuyer eventMergeBuyer  = eventMergeBuyerService.getEventMergeBuyerById(id);
            jr = buildJsonResult(eventMergeBuyer);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventMergeBuyer/getEventMergeBuyerList 查询eventMergeBuyer列表(不带分页)
	 * @apiGroup eventMergeBuyer管理
	 * @apiName 查询eventMergeBuyer列表(不带分页)
	 * @apiDescription 根据条件查询eventMergeBuyer列表(不带分页)
	 * @apiParam {EventMergeBuyer} eventMergeBuyer eventMergeBuyer对象
	 */
    @RequestMapping("/getEventMergeBuyerList")
    public JsonResult<List<EventMergeBuyer>> getEventMergeBuyerList(EventMergeBuyer eventMergeBuyer) {
        JsonResult<List<EventMergeBuyer>> jr = new JsonResult<List<EventMergeBuyer>>();
        try {
            List<EventMergeBuyer> list = this.eventMergeBuyerService.getEventMergeBuyerList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventMergeBuyer/findEventMergeBuyerPageByParam 查询eventMergeBuyer列表(带分页)
	 * @apiGroup eventMergeBuyer管理
	 * @apiName 查询eventMergeBuyer列表(带分页)
	 * @apiDescription 根据条件查询eventMergeBuyer列表(带分页)
	 * @apiParam {EventMergeBuyer} eventMergeBuyer eventMergeBuyer对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventMergeBuyerPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventMergeBuyer>> findEventMergeBuyerPageByParam(@RequestBody EventMergeBuyer eventMergeBuyer, PageParam pageParam) {
        JsonResult<PageResult<EventMergeBuyer>> jr = new JsonResult<PageResult<EventMergeBuyer>>();
        try {
            PageResult<EventMergeBuyer> page = this.eventMergeBuyerService.findEventMergeBuyerPageByParam(eventMergeBuyer, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventMergeBuyer/deleteEventMergeBuyerById 删除eventMergeBuyer(物理删除)
	 * @apiGroup eventMergeBuyer管理
	 * @apiName 删除eventMergeBuyer记录(物理删除)
	 * @apiDescription 根据主键id删除eventMergeBuyer记录(物理删除)
	 * @apiParam {String} id eventMergeBuyer主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventMergeBuyerById",method=RequestMethod.DELETE)
    public JsonResult deleteEventMergeBuyerById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeBuyerService.deleteEventMergeBuyerById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
