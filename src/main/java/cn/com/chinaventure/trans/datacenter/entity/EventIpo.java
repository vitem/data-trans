package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EventIpo implements Serializable {
    private String id;

    private String eventId;

    /**
     * 股票代码
     */
    private String stockCode;

    /**
     * 申请时间
     */
    private Date applyTime;

    /**
     * 上市交易所ID
     */
    private String exchangeId;

    /**
     * 上市时间 (对应发生时间)
     */
    private Date ipoTime;

    /**
     * 上市状态 1：已完成   2：已失败   3：进行中
     */
    private Byte ipoStatus;

    private Byte ipoType;

    /**
     * 发行价
     */
    private BigDecimal issuePrice;

    /**
     * 是否有vc/pe支持
     */
    private Byte vcpeType;

    /**
     * 股票发行数量
     */
    private Integer stockNo;

    /**
     * 募集资金总额
     */
    private BigDecimal sumMoney;

    /**
     * 货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte sumMoneyCurrencyType;

    /**
     * 募集资金净额
     */
    private BigDecimal netWorth;

    /**
     * 货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte netWorthCurrencyType;

    /**
     * 保荐机构
     */
    private String recommendFirmId;

    /**
     * 律师事务所
     */
    private String lawOrgId;

    /**
     * 律师事务所人物
     */
    private String lawPersonageId;

    /**
     * 会计事务所
     */
    private String accountingOrgId;

    /**
     * 会计事务所人物
     */
    private String accountingPersonageId;

    /**
     * 发行费用总计
     */
    private BigDecimal issueCostTotal;

    /**
     * 货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte issueCostCurrencyType;

    /**
     * 保荐及承销费用
     */
    private BigDecimal recommendShipmentCost;

    /**
     * 保荐费用
     */
    private BigDecimal recommendCost;

    /**
     * 承销费用
     */
    private BigDecimal shipmentInwardCost;

    /**
     * 律师费用
     */
    private BigDecimal lawyerCost;

    /**
     * 审计费用
     */
    private BigDecimal auditCost;

    private String remark;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId == null ? null : eventId.trim();
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode == null ? null : stockCode.trim();
    }

    public Date getApplyTime() {
        return applyTime;
    }

    public void setApplyTime(Date applyTime) {
        this.applyTime = applyTime;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId == null ? null : exchangeId.trim();
    }

    public Date getIpoTime() {
        return ipoTime;
    }

    public void setIpoTime(Date ipoTime) {
        this.ipoTime = ipoTime;
    }

    public Byte getIpoStatus() {
        return ipoStatus;
    }

    public void setIpoStatus(Byte ipoStatus) {
        this.ipoStatus = ipoStatus;
    }

    public Byte getIpoType() {
        return ipoType;
    }

    public void setIpoType(Byte ipoType) {
        this.ipoType = ipoType;
    }

    public BigDecimal getIssuePrice() {
        return issuePrice;
    }

    public void setIssuePrice(BigDecimal issuePrice) {
        this.issuePrice = issuePrice;
    }

    public Byte getVcpeType() {
        return vcpeType;
    }

    public void setVcpeType(Byte vcpeType) {
        this.vcpeType = vcpeType;
    }

    public Integer getStockNo() {
        return stockNo;
    }

    public void setStockNo(Integer stockNo) {
        this.stockNo = stockNo;
    }

    public BigDecimal getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(BigDecimal sumMoney) {
        this.sumMoney = sumMoney;
    }

    public Byte getSumMoneyCurrencyType() {
        return sumMoneyCurrencyType;
    }

    public void setSumMoneyCurrencyType(Byte sumMoneyCurrencyType) {
        this.sumMoneyCurrencyType = sumMoneyCurrencyType;
    }

    public BigDecimal getNetWorth() {
        return netWorth;
    }

    public void setNetWorth(BigDecimal netWorth) {
        this.netWorth = netWorth;
    }

    public Byte getNetWorthCurrencyType() {
        return netWorthCurrencyType;
    }

    public void setNetWorthCurrencyType(Byte netWorthCurrencyType) {
        this.netWorthCurrencyType = netWorthCurrencyType;
    }

    public String getRecommendFirmId() {
        return recommendFirmId;
    }

    public void setRecommendFirmId(String recommendFirmId) {
        this.recommendFirmId = recommendFirmId == null ? null : recommendFirmId.trim();
    }

    public String getLawOrgId() {
        return lawOrgId;
    }

    public void setLawOrgId(String lawOrgId) {
        this.lawOrgId = lawOrgId == null ? null : lawOrgId.trim();
    }

    public String getLawPersonageId() {
        return lawPersonageId;
    }

    public void setLawPersonageId(String lawPersonageId) {
        this.lawPersonageId = lawPersonageId == null ? null : lawPersonageId.trim();
    }

    public String getAccountingOrgId() {
        return accountingOrgId;
    }

    public void setAccountingOrgId(String accountingOrgId) {
        this.accountingOrgId = accountingOrgId == null ? null : accountingOrgId.trim();
    }

    public String getAccountingPersonageId() {
        return accountingPersonageId;
    }

    public void setAccountingPersonageId(String accountingPersonageId) {
        this.accountingPersonageId = accountingPersonageId == null ? null : accountingPersonageId.trim();
    }

    public BigDecimal getIssueCostTotal() {
        return issueCostTotal;
    }

    public void setIssueCostTotal(BigDecimal issueCostTotal) {
        this.issueCostTotal = issueCostTotal;
    }

    public Byte getIssueCostCurrencyType() {
        return issueCostCurrencyType;
    }

    public void setIssueCostCurrencyType(Byte issueCostCurrencyType) {
        this.issueCostCurrencyType = issueCostCurrencyType;
    }

    public BigDecimal getRecommendShipmentCost() {
        return recommendShipmentCost;
    }

    public void setRecommendShipmentCost(BigDecimal recommendShipmentCost) {
        this.recommendShipmentCost = recommendShipmentCost;
    }

    public BigDecimal getRecommendCost() {
        return recommendCost;
    }

    public void setRecommendCost(BigDecimal recommendCost) {
        this.recommendCost = recommendCost;
    }

    public BigDecimal getShipmentInwardCost() {
        return shipmentInwardCost;
    }

    public void setShipmentInwardCost(BigDecimal shipmentInwardCost) {
        this.shipmentInwardCost = shipmentInwardCost;
    }

    public BigDecimal getLawyerCost() {
        return lawyerCost;
    }

    public void setLawyerCost(BigDecimal lawyerCost) {
        this.lawyerCost = lawyerCost;
    }

    public BigDecimal getAuditCost() {
        return auditCost;
    }

    public void setAuditCost(BigDecimal auditCost) {
        this.auditCost = auditCost;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventId=").append(eventId);
        sb.append(", stockCode=").append(stockCode);
        sb.append(", applyTime=").append(applyTime);
        sb.append(", exchangeId=").append(exchangeId);
        sb.append(", ipoTime=").append(ipoTime);
        sb.append(", ipoStatus=").append(ipoStatus);
        sb.append(", ipoType=").append(ipoType);
        sb.append(", issuePrice=").append(issuePrice);
        sb.append(", vcpeType=").append(vcpeType);
        sb.append(", stockNo=").append(stockNo);
        sb.append(", sumMoney=").append(sumMoney);
        sb.append(", sumMoneyCurrencyType=").append(sumMoneyCurrencyType);
        sb.append(", netWorth=").append(netWorth);
        sb.append(", netWorthCurrencyType=").append(netWorthCurrencyType);
        sb.append(", recommendFirmId=").append(recommendFirmId);
        sb.append(", lawOrgId=").append(lawOrgId);
        sb.append(", lawPersonageId=").append(lawPersonageId);
        sb.append(", accountingOrgId=").append(accountingOrgId);
        sb.append(", accountingPersonageId=").append(accountingPersonageId);
        sb.append(", issueCostTotal=").append(issueCostTotal);
        sb.append(", issueCostCurrencyType=").append(issueCostCurrencyType);
        sb.append(", recommendShipmentCost=").append(recommendShipmentCost);
        sb.append(", recommendCost=").append(recommendCost);
        sb.append(", shipmentInwardCost=").append(shipmentInwardCost);
        sb.append(", lawyerCost=").append(lawyerCost);
        sb.append(", auditCost=").append(auditCost);
        sb.append(", remark=").append(remark);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}