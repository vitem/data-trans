package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraise;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraiseExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventMergeAppraiseMapper {
    int countByExample(EventMergeAppraiseExample example);

    int deleteByExample(EventMergeAppraiseExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventMergeAppraise record);

    int insertSelective(EventMergeAppraise record);

    List<EventMergeAppraise> selectByExample(EventMergeAppraiseExample example);

    EventMergeAppraise selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventMergeAppraise record, @Param("example") EventMergeAppraiseExample example);

    int updateByExample(@Param("record") EventMergeAppraise record, @Param("example") EventMergeAppraiseExample example);

    int updateByPrimaryKeySelective(EventMergeAppraise record);

    int updateByPrimaryKey(EventMergeAppraise record);
}