package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademark;

/**
 * @author 
 *
 */
public interface CompanyTrademarkService {
	
	/**
     * @api saveCompanyTrademark 新增companyTrademark
     * @apiGroup companyTrademark管理
     * @apiName  新增companyTrademark记录
     * @apiDescription 全量插入companyTrademark记录
     * @apiParam {CompanyTrademark} companyTrademark companyTrademark对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyTrademark(CompanyTrademark companyTrademark);
	
	/**
     * @api updateCompanyTrademark 修改companyTrademark
     * @apiGroup companyTrademark管理
     * @apiName  修改companyTrademark记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyTrademark} companyTrademark companyTrademark对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyTrademark(CompanyTrademark companyTrademark);
	
	/**
     * @api getCompanyTrademarkById 根据companyTrademarkid查询详情
     * @apiGroup companyTrademark管理
     * @apiName  查询companyTrademark详情
     * @apiDescription 根据主键id查询companyTrademark详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyTrademark companyTrademark实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyTrademark getCompanyTrademarkById(String id);
	
	/**
     * @api getCompanyTrademarkList 根据companyTrademark条件查询列表
     * @apiGroup companyTrademark管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyTrademark} companyTrademark  实体条件
     * @apiSuccess List<CompanyTrademark> companyTrademark实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyTrademark> getCompanyTrademarkList(CompanyTrademark companyTrademark);
	

	/**
     * @api getCompanyTrademarkListByParam 根据companyTrademark条件查询列表（含排序）
     * @apiGroup companyTrademark管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyTrademark} companyTrademark  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyTrademark> companyTrademark实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyTrademark> getCompanyTrademarkListByParam(CompanyTrademark companyTrademark, String orderByStr);
	
	
	/**
     * @api findCompanyTrademarkPageByParam 根据companyTrademark条件查询列表（分页）
     * @apiGroup companyTrademark管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyTrademark} companyTrademark  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyTrademark> companyTrademark实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyTrademark> findCompanyTrademarkPageByParam(CompanyTrademark companyTrademark, PageParam pageParam); 
	
	/**
     * @api findCompanyTrademarkPageByParam 根据companyTrademark条件查询列表（分页，含排序）
     * @apiGroup companyTrademark管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyTrademark} companyTrademark  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyTrademark> companyTrademark实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyTrademark> findCompanyTrademarkPageByParam(CompanyTrademark companyTrademark, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyTrademarkById 根据companyTrademark的id删除(物理删除)
     * @apiGroup companyTrademark管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyTrademarkById(String id);

}

