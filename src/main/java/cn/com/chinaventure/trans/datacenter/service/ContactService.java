package cn.com.chinaventure.trans.datacenter.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Contact;

import java.util.List;

/**
 * @author 
 *
 */
public interface ContactService {
	
	/**
     * @api saveContact 新增contact
     * @apiGroup contact管理
     * @apiName  新增contact记录
     * @apiDescription 全量插入contact记录
     * @apiParam {Contact} contact contact对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveContact(Contact contact);
	
	/**
     * @api updateContact 修改contact
     * @apiGroup contact管理
     * @apiName  修改contact记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Contact} contact contact对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateContact(Contact contact);
	
	/**
     * @api getContactById 根据contactid查询详情
     * @apiGroup contact管理
     * @apiName  查询contact详情
     * @apiDescription 根据主键id查询contact详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Contact contact实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Contact getContactById(String id);
	
	/**
     * @api getContactList 根据contact条件查询列表
     * @apiGroup contact管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Contact} contact  实体条件
     * @apiSuccess List<Contact> contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Contact> getContactList(Contact contact);
	

	/**
     * @api getContactListByParam 根据contact条件查询列表（含排序）
     * @apiGroup contact管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Contact} contact  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Contact> contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Contact> getContactListByParam(Contact contact, String orderByStr);
	
	
	/**
     * @api findContactPageByParam 根据contact条件查询列表（分页）
     * @apiGroup contact管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Contact} contact  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Contact> contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Contact> findContactPageByParam(Contact contact, PageParam pageParam); 
	
	/**
     * @api findContactPageByParam 根据contact条件查询列表（分页，含排序）
     * @apiGroup contact管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Contact} contact  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Contact> contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Contact> findContactPageByParam(Contact contact, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteContactById 根据contact的id删除(物理删除)
     * @apiGroup contact管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteContactById(String id);

}

