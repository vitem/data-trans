package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyStockChangeService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChange;


@RestController
@RequestMapping("/companyStockChange")
public class CompanyStockChangeController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyStockChangeController.class);

    @Resource
    private CompanyStockChangeService companyStockChangeService;

    /**
	 * @api {post} / 新增companyStockChange
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyStockChange(@RequestBody CompanyStockChange companyStockChange) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyStockChangeService.saveCompanyStockChange(companyStockChange);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyStockChange
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyStockChange(@RequestBody CompanyStockChange companyStockChange) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyStockChangeService.updateCompanyStockChange(companyStockChange);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyStockChange详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyStockChange> getCompanyStockChangeById(@PathVariable String id) {
        JsonResult<CompanyStockChange> jr = new JsonResult<CompanyStockChange>();
        try {
            CompanyStockChange companyStockChange  = companyStockChangeService.getCompanyStockChangeById(id);
            jr = buildJsonResult(companyStockChange);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyStockChange/getCompanyStockChangeList 查询companyStockChange列表(不带分页)
	 * @apiGroup companyStockChange管理
	 * @apiName 查询companyStockChange列表(不带分页)
	 * @apiDescription 根据条件查询companyStockChange列表(不带分页)
	 * @apiParam {CompanyStockChange} companyStockChange companyStockChange对象
	 */
    @RequestMapping("/getCompanyStockChangeList")
    public JsonResult<List<CompanyStockChange>> getCompanyStockChangeList(CompanyStockChange companyStockChange) {
        JsonResult<List<CompanyStockChange>> jr = new JsonResult<List<CompanyStockChange>>();
        try {
            List<CompanyStockChange> list = this.companyStockChangeService.getCompanyStockChangeList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyStockChange/findCompanyStockChangePageByParam 查询companyStockChange列表(带分页)
	 * @apiGroup companyStockChange管理
	 * @apiName 查询companyStockChange列表(带分页)
	 * @apiDescription 根据条件查询companyStockChange列表(带分页)
	 * @apiParam {CompanyStockChange} companyStockChange companyStockChange对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyStockChangePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyStockChange>> findCompanyStockChangePageByParam(@RequestBody CompanyStockChange companyStockChange, PageParam pageParam) {
        JsonResult<PageResult<CompanyStockChange>> jr = new JsonResult<PageResult<CompanyStockChange>>();
        try {
            PageResult<CompanyStockChange> page = this.companyStockChangeService.findCompanyStockChangePageByParam(companyStockChange, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyStockChange/deleteCompanyStockChangeById 删除companyStockChange(物理删除)
	 * @apiGroup companyStockChange管理
	 * @apiName 删除companyStockChange记录(物理删除)
	 * @apiDescription 根据主键id删除companyStockChange记录(物理删除)
	 * @apiParam {String} id companyStockChange主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyStockChangeById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyStockChangeById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyStockChangeService.deleteCompanyStockChangeById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
