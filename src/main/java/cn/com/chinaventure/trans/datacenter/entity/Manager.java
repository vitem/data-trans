package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Manager implements Serializable {
    private String id;

    /**
     * 机构则是机构ID 企业是企业ID 中介ID,:LP是LP的ID 
     */
    private String contextId;

    /**
     * 1 企业 2:机构 3:LP 4:中介
     */
    private Byte contextType;

    /**
     * 0：管理人员  1：董事会人员  2：监事会人员
     */
    private Byte manageType;

    private String personageId;

    /**
     * 职位
     */
    private String title;

    private Date entryTime;

    /**
     * 是否离职  1是 2未离职
     */
    private Byte dimissionType;

    private Date dimissionTime;

    /**
     * 持股总额
     */
    private BigDecimal holdingTotalMoney;

    /**
     * 持股数量
     */
    private Integer holdingAmount;

    /**
     * 年薪
     */
    private BigDecimal annualSalary;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    /**
     * 个人简介
     */
    private String selfInfo;

    private Personage personage;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId == null ? null : contextId.trim();
    }

    public Byte getContextType() {
        return contextType;
    }

    public void setContextType(Byte contextType) {
        this.contextType = contextType;
    }

    public Byte getManageType() {
        return manageType;
    }

    public void setManageType(Byte manageType) {
        this.manageType = manageType;
    }

    public String getPersonageId() {
        return personageId;
    }

    public void setPersonageId(String personageId) {
        this.personageId = personageId == null ? null : personageId.trim();
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getEntryTime() {
        return entryTime;
    }

    public void setEntryTime(Date entryTime) {
        this.entryTime = entryTime;
    }

    public Byte getDimissionType() {
        return dimissionType;
    }

    public void setDimissionType(Byte dimissionType) {
        this.dimissionType = dimissionType;
    }

    public Date getDimissionTime() {
        return dimissionTime;
    }

    public void setDimissionTime(Date dimissionTime) {
        this.dimissionTime = dimissionTime;
    }

    public BigDecimal getHoldingTotalMoney() {
        return holdingTotalMoney;
    }

    public void setHoldingTotalMoney(BigDecimal holdingTotalMoney) {
        this.holdingTotalMoney = holdingTotalMoney;
    }

    public Integer getHoldingAmount() {
        return holdingAmount;
    }

    public void setHoldingAmount(Integer holdingAmount) {
        this.holdingAmount = holdingAmount;
    }

    public BigDecimal getAnnualSalary() {
        return annualSalary;
    }

    public void setAnnualSalary(BigDecimal annualSalary) {
        this.annualSalary = annualSalary;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    public String getSelfInfo() {
        return selfInfo;
    }

    public void setSelfInfo(String selfInfo) {
        this.selfInfo = selfInfo == null ? null : selfInfo.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", contextId=").append(contextId);
        sb.append(", contextType=").append(contextType);
        sb.append(", manageType=").append(manageType);
        sb.append(", personageId=").append(personageId);
        sb.append(", title=").append(title);
        sb.append(", entryTime=").append(entryTime);
        sb.append(", dimissionType=").append(dimissionType);
        sb.append(", dimissionTime=").append(dimissionTime);
        sb.append(", holdingTotalMoney=").append(holdingTotalMoney);
        sb.append(", holdingAmount=").append(holdingAmount);
        sb.append(", annualSalary=").append(annualSalary);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", selfInfo=").append(selfInfo);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }

    public Personage getPersonage() {
        return personage;
    }

    public void setPersonage(Personage personage) {
        this.personage = personage;
    }
}