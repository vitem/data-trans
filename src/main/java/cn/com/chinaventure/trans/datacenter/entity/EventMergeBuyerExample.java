package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventMergeBuyerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EventMergeBuyerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdIsNull() {
            addCriterion("event_merge_id is null");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdIsNotNull() {
            addCriterion("event_merge_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdEqualTo(String value) {
            addCriterion("event_merge_id =", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotEqualTo(String value) {
            addCriterion("event_merge_id <>", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdGreaterThan(String value) {
            addCriterion("event_merge_id >", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_merge_id >=", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdLessThan(String value) {
            addCriterion("event_merge_id <", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdLessThanOrEqualTo(String value) {
            addCriterion("event_merge_id <=", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdLike(String value) {
            addCriterion("event_merge_id like", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotLike(String value) {
            addCriterion("event_merge_id not like", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdIn(List<String> values) {
            addCriterion("event_merge_id in", values, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotIn(List<String> values) {
            addCriterion("event_merge_id not in", values, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdBetween(String value1, String value2) {
            addCriterion("event_merge_id between", value1, value2, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotBetween(String value1, String value2) {
            addCriterion("event_merge_id not between", value1, value2, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("enterprise_id is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("enterprise_id is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(String value) {
            addCriterion("enterprise_id =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(String value) {
            addCriterion("enterprise_id <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(String value) {
            addCriterion("enterprise_id >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(String value) {
            addCriterion("enterprise_id >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(String value) {
            addCriterion("enterprise_id <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(String value) {
            addCriterion("enterprise_id <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLike(String value) {
            addCriterion("enterprise_id like", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotLike(String value) {
            addCriterion("enterprise_id not like", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<String> values) {
            addCriterion("enterprise_id in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<String> values) {
            addCriterion("enterprise_id not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(String value1, String value2) {
            addCriterion("enterprise_id between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(String value1, String value2) {
            addCriterion("enterprise_id not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdIsNull() {
            addCriterion("natural_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdIsNotNull() {
            addCriterion("natural_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdEqualTo(String value) {
            addCriterion("natural_personage_id =", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdNotEqualTo(String value) {
            addCriterion("natural_personage_id <>", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdGreaterThan(String value) {
            addCriterion("natural_personage_id >", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("natural_personage_id >=", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdLessThan(String value) {
            addCriterion("natural_personage_id <", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("natural_personage_id <=", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdLike(String value) {
            addCriterion("natural_personage_id like", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdNotLike(String value) {
            addCriterion("natural_personage_id not like", value, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdIn(List<String> values) {
            addCriterion("natural_personage_id in", values, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdNotIn(List<String> values) {
            addCriterion("natural_personage_id not in", values, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdBetween(String value1, String value2) {
            addCriterion("natural_personage_id between", value1, value2, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andNaturalPersonageIdNotBetween(String value1, String value2) {
            addCriterion("natural_personage_id not between", value1, value2, "naturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateIsNull() {
            addCriterion("before_stock_rate is null");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateIsNotNull() {
            addCriterion("before_stock_rate is not null");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateEqualTo(BigDecimal value) {
            addCriterion("before_stock_rate =", value, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateNotEqualTo(BigDecimal value) {
            addCriterion("before_stock_rate <>", value, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateGreaterThan(BigDecimal value) {
            addCriterion("before_stock_rate >", value, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("before_stock_rate >=", value, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateLessThan(BigDecimal value) {
            addCriterion("before_stock_rate <", value, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("before_stock_rate <=", value, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateIn(List<BigDecimal> values) {
            addCriterion("before_stock_rate in", values, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateNotIn(List<BigDecimal> values) {
            addCriterion("before_stock_rate not in", values, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("before_stock_rate between", value1, value2, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andBeforeStockRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("before_stock_rate not between", value1, value2, "beforeStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateIsNull() {
            addCriterion("after_stock_rate is null");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateIsNotNull() {
            addCriterion("after_stock_rate is not null");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateEqualTo(BigDecimal value) {
            addCriterion("after_stock_rate =", value, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateNotEqualTo(BigDecimal value) {
            addCriterion("after_stock_rate <>", value, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateGreaterThan(BigDecimal value) {
            addCriterion("after_stock_rate >", value, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("after_stock_rate >=", value, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateLessThan(BigDecimal value) {
            addCriterion("after_stock_rate <", value, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("after_stock_rate <=", value, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateIn(List<BigDecimal> values) {
            addCriterion("after_stock_rate in", values, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateNotIn(List<BigDecimal> values) {
            addCriterion("after_stock_rate not in", values, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("after_stock_rate between", value1, value2, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAfterStockRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("after_stock_rate not between", value1, value2, "afterStockRate");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceIsNull() {
            addCriterion("attorn_stock_price is null");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceIsNotNull() {
            addCriterion("attorn_stock_price is not null");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceEqualTo(BigDecimal value) {
            addCriterion("attorn_stock_price =", value, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceNotEqualTo(BigDecimal value) {
            addCriterion("attorn_stock_price <>", value, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceGreaterThan(BigDecimal value) {
            addCriterion("attorn_stock_price >", value, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("attorn_stock_price >=", value, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceLessThan(BigDecimal value) {
            addCriterion("attorn_stock_price <", value, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("attorn_stock_price <=", value, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceIn(List<BigDecimal> values) {
            addCriterion("attorn_stock_price in", values, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceNotIn(List<BigDecimal> values) {
            addCriterion("attorn_stock_price not in", values, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("attorn_stock_price between", value1, value2, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockPriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("attorn_stock_price not between", value1, value2, "attornStockPrice");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountIsNull() {
            addCriterion("attorn_stock_amount is null");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountIsNotNull() {
            addCriterion("attorn_stock_amount is not null");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountEqualTo(String value) {
            addCriterion("attorn_stock_amount =", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountNotEqualTo(String value) {
            addCriterion("attorn_stock_amount <>", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountGreaterThan(String value) {
            addCriterion("attorn_stock_amount >", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountGreaterThanOrEqualTo(String value) {
            addCriterion("attorn_stock_amount >=", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountLessThan(String value) {
            addCriterion("attorn_stock_amount <", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountLessThanOrEqualTo(String value) {
            addCriterion("attorn_stock_amount <=", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountLike(String value) {
            addCriterion("attorn_stock_amount like", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountNotLike(String value) {
            addCriterion("attorn_stock_amount not like", value, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountIn(List<String> values) {
            addCriterion("attorn_stock_amount in", values, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountNotIn(List<String> values) {
            addCriterion("attorn_stock_amount not in", values, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountBetween(String value1, String value2) {
            addCriterion("attorn_stock_amount between", value1, value2, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andAttornStockAmountNotBetween(String value1, String value2) {
            addCriterion("attorn_stock_amount not between", value1, value2, "attornStockAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountIsNull() {
            addCriterion("pay_amount is null");
            return (Criteria) this;
        }

        public Criteria andPayAmountIsNotNull() {
            addCriterion("pay_amount is not null");
            return (Criteria) this;
        }

        public Criteria andPayAmountEqualTo(BigDecimal value) {
            addCriterion("pay_amount =", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountNotEqualTo(BigDecimal value) {
            addCriterion("pay_amount <>", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountGreaterThan(BigDecimal value) {
            addCriterion("pay_amount >", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("pay_amount >=", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountLessThan(BigDecimal value) {
            addCriterion("pay_amount <", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountLessThanOrEqualTo(BigDecimal value) {
            addCriterion("pay_amount <=", value, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountIn(List<BigDecimal> values) {
            addCriterion("pay_amount in", values, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountNotIn(List<BigDecimal> values) {
            addCriterion("pay_amount not in", values, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pay_amount between", value1, value2, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayAmountNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("pay_amount not between", value1, value2, "payAmount");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNull() {
            addCriterion("pay_type is null");
            return (Criteria) this;
        }

        public Criteria andPayTypeIsNotNull() {
            addCriterion("pay_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayTypeEqualTo(Byte value) {
            addCriterion("pay_type =", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotEqualTo(Byte value) {
            addCriterion("pay_type <>", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThan(Byte value) {
            addCriterion("pay_type >", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("pay_type >=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThan(Byte value) {
            addCriterion("pay_type <", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeLessThanOrEqualTo(Byte value) {
            addCriterion("pay_type <=", value, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeIn(List<Byte> values) {
            addCriterion("pay_type in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotIn(List<Byte> values) {
            addCriterion("pay_type not in", values, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeBetween(Byte value1, Byte value2) {
            addCriterion("pay_type between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("pay_type not between", value1, value2, "payType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeIsNull() {
            addCriterion("pay_invest_type is null");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeIsNotNull() {
            addCriterion("pay_invest_type is not null");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeEqualTo(Byte value) {
            addCriterion("pay_invest_type =", value, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeNotEqualTo(Byte value) {
            addCriterion("pay_invest_type <>", value, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeGreaterThan(Byte value) {
            addCriterion("pay_invest_type >", value, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("pay_invest_type >=", value, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeLessThan(Byte value) {
            addCriterion("pay_invest_type <", value, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeLessThanOrEqualTo(Byte value) {
            addCriterion("pay_invest_type <=", value, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeIn(List<Byte> values) {
            addCriterion("pay_invest_type in", values, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeNotIn(List<Byte> values) {
            addCriterion("pay_invest_type not in", values, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeBetween(Byte value1, Byte value2) {
            addCriterion("pay_invest_type between", value1, value2, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andPayInvestTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("pay_invest_type not between", value1, value2, "payInvestType");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}