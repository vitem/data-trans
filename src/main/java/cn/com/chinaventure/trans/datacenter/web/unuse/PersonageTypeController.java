package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.PersonageTypeService;
import cn.com.chinaventure.trans.datacenter.entity.PersonageType;


@RestController
@RequestMapping("/personageType")
public class PersonageTypeController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(PersonageTypeController.class);

    @Resource
    private PersonageTypeService personageTypeService;

    /**
	 * @api {post} / 新增personageType
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> savePersonageType(@RequestBody PersonageType personageType) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.personageTypeService.savePersonageType(personageType);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改personageType
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updatePersonageType(@RequestBody PersonageType personageType) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.personageTypeService.updatePersonageType(personageType);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询personageType详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<PersonageType> getPersonageTypeById(@PathVariable String id) {
        JsonResult<PersonageType> jr = new JsonResult<PersonageType>();
        try {
            PersonageType personageType  = personageTypeService.getPersonageTypeById(id);
            jr = buildJsonResult(personageType);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /personageType/getPersonageTypeList 查询personageType列表(不带分页)
	 * @apiGroup personageType管理
	 * @apiName 查询personageType列表(不带分页)
	 * @apiDescription 根据条件查询personageType列表(不带分页)
	 * @apiParam {PersonageType} personageType personageType对象
	 */
    @RequestMapping("/getPersonageTypeList")
    public JsonResult<List<PersonageType>> getPersonageTypeList(PersonageType personageType) {
        JsonResult<List<PersonageType>> jr = new JsonResult<List<PersonageType>>();
        try {
            List<PersonageType> list = this.personageTypeService.getPersonageTypeList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /personageType/findPersonageTypePageByParam 查询personageType列表(带分页)
	 * @apiGroup personageType管理
	 * @apiName 查询personageType列表(带分页)
	 * @apiDescription 根据条件查询personageType列表(带分页)
	 * @apiParam {PersonageType} personageType personageType对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findPersonageTypePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<PersonageType>> findPersonageTypePageByParam(@RequestBody PersonageType personageType, PageParam pageParam) {
        JsonResult<PageResult<PersonageType>> jr = new JsonResult<PageResult<PersonageType>>();
        try {
            PageResult<PersonageType> page = this.personageTypeService.findPersonageTypePageByParam(personageType, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /personageType/deletePersonageTypeById 删除personageType(物理删除)
	 * @apiGroup personageType管理
	 * @apiName 删除personageType记录(物理删除)
	 * @apiDescription 根据主键id删除personageType记录(物理删除)
	 * @apiParam {String} id personageType主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deletePersonageTypeById",method=RequestMethod.DELETE)
    public JsonResult deletePersonageTypeById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.personageTypeService.deletePersonageTypeById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
