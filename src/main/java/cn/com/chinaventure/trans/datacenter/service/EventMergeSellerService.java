package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeSeller;

/**
 * @author 
 *
 */
public interface EventMergeSellerService {
	
	/**
     * @api saveEventMergeSeller 新增eventMergeSeller
     * @apiGroup eventMergeSeller管理
     * @apiName  新增eventMergeSeller记录
     * @apiDescription 全量插入eventMergeSeller记录
     * @apiParam {EventMergeSeller} eventMergeSeller eventMergeSeller对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventMergeSeller(EventMergeSeller eventMergeSeller);
	
	/**
     * @api updateEventMergeSeller 修改eventMergeSeller
     * @apiGroup eventMergeSeller管理
     * @apiName  修改eventMergeSeller记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventMergeSeller} eventMergeSeller eventMergeSeller对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventMergeSeller(EventMergeSeller eventMergeSeller);
	
	/**
     * @api getEventMergeSellerById 根据eventMergeSellerid查询详情
     * @apiGroup eventMergeSeller管理
     * @apiName  查询eventMergeSeller详情
     * @apiDescription 根据主键id查询eventMergeSeller详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventMergeSeller eventMergeSeller实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventMergeSeller getEventMergeSellerById(String id);
	
	/**
     * @api getEventMergeSellerList 根据eventMergeSeller条件查询列表
     * @apiGroup eventMergeSeller管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventMergeSeller} eventMergeSeller  实体条件
     * @apiSuccess List<EventMergeSeller> eventMergeSeller实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeSeller> getEventMergeSellerList(EventMergeSeller eventMergeSeller);
	

	/**
     * @api getEventMergeSellerListByParam 根据eventMergeSeller条件查询列表（含排序）
     * @apiGroup eventMergeSeller管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventMergeSeller} eventMergeSeller  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventMergeSeller> eventMergeSeller实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeSeller> getEventMergeSellerListByParam(EventMergeSeller eventMergeSeller, String orderByStr);
	
	
	/**
     * @api findEventMergeSellerPageByParam 根据eventMergeSeller条件查询列表（分页）
     * @apiGroup eventMergeSeller管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventMergeSeller} eventMergeSeller  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventMergeSeller> eventMergeSeller实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeSeller> findEventMergeSellerPageByParam(EventMergeSeller eventMergeSeller, PageParam pageParam); 
	
	/**
     * @api findEventMergeSellerPageByParam 根据eventMergeSeller条件查询列表（分页，含排序）
     * @apiGroup eventMergeSeller管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventMergeSeller} eventMergeSeller  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventMergeSeller> eventMergeSeller实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeSeller> findEventMergeSellerPageByParam(EventMergeSeller eventMergeSeller, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventMergeSellerById 根据eventMergeSeller的id删除(物理删除)
     * @apiGroup eventMergeSeller管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventMergeSellerById(String id);

}

