package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventTags;

/**
 * @author 
 *
 */
public interface EventTagsService {
	
	/**
     * @api saveEventTags 新增eventTags
     * @apiGroup eventTags管理
     * @apiName  新增eventTags记录
     * @apiDescription 全量插入eventTags记录
     * @apiParam {EventTags} eventTags eventTags对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventTags(EventTags eventTags);
	
	/**
     * @api updateEventTags 修改eventTags
     * @apiGroup eventTags管理
     * @apiName  修改eventTags记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventTags} eventTags eventTags对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventTags(EventTags eventTags);
	
	/**
     * @api getEventTagsById 根据eventTagsid查询详情
     * @apiGroup eventTags管理
     * @apiName  查询eventTags详情
     * @apiDescription 根据主键id查询eventTags详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventTags eventTags实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventTags getEventTagsById(String id);
	
	/**
     * @api getEventTagsList 根据eventTags条件查询列表
     * @apiGroup eventTags管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventTags} eventTags  实体条件
     * @apiSuccess List<EventTags> eventTags实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventTags> getEventTagsList(EventTags eventTags);
	

	/**
     * @api getEventTagsListByParam 根据eventTags条件查询列表（含排序）
     * @apiGroup eventTags管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventTags} eventTags  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventTags> eventTags实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventTags> getEventTagsListByParam(EventTags eventTags, String orderByStr);
	
	
	/**
     * @api findEventTagsPageByParam 根据eventTags条件查询列表（分页）
     * @apiGroup eventTags管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventTags} eventTags  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventTags> eventTags实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventTags> findEventTagsPageByParam(EventTags eventTags, PageParam pageParam); 
	
	/**
     * @api findEventTagsPageByParam 根据eventTags条件查询列表（分页，含排序）
     * @apiGroup eventTags管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventTags} eventTags  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventTags> eventTags实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventTags> findEventTagsPageByParam(EventTags eventTags, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventTagsById 根据eventTags的id删除(物理删除)
     * @apiGroup eventTags管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventTagsById(String id);

}

