package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.FundService;
import cn.com.chinaventure.trans.datacenter.entity.Fund;


@RestController
@RequestMapping("/fund")
public class FundController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(FundController.class);

    @Resource
    private FundService fundService;

    /**
	 * @api {post} / 新增fund
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveFund(@RequestBody Fund fund) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundService.saveFund(fund);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改fund
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateFund(@RequestBody Fund fund) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundService.updateFund(fund);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询fund详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Fund> getFundById(@PathVariable String id) {
        JsonResult<Fund> jr = new JsonResult<Fund>();
        try {
            Fund fund  = fundService.getFundById(id);
            jr = buildJsonResult(fund);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /fund/getFundList 查询fund列表(不带分页)
	 * @apiGroup fund管理
	 * @apiName 查询fund列表(不带分页)
	 * @apiDescription 根据条件查询fund列表(不带分页)
	 * @apiParam {Fund} fund fund对象
	 */
    @RequestMapping("/getFundList")
    public JsonResult<List<Fund>> getFundList(Fund fund) {
        JsonResult<List<Fund>> jr = new JsonResult<List<Fund>>();
        try {
            List<Fund> list = this.fundService.getFundList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /fund/findFundPageByParam 查询fund列表(带分页)
	 * @apiGroup fund管理
	 * @apiName 查询fund列表(带分页)
	 * @apiDescription 根据条件查询fund列表(带分页)
	 * @apiParam {Fund} fund fund对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findFundPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Fund>> findFundPageByParam(@RequestBody Fund fund, PageParam pageParam) {
        JsonResult<PageResult<Fund>> jr = new JsonResult<PageResult<Fund>>();
        try {
            PageResult<Fund> page = this.fundService.findFundPageByParam(fund, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /fund/deleteFundById 删除fund(物理删除)
	 * @apiGroup fund管理
	 * @apiName 删除fund记录(物理删除)
	 * @apiDescription 根据主键id删除fund记录(物理删除)
	 * @apiParam {String} id fund主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteFundById",method=RequestMethod.DELETE)
    public JsonResult deleteFundById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundService.deleteFundById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
