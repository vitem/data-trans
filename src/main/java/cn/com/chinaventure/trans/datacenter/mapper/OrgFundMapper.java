package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.OrgFund;
import cn.com.chinaventure.trans.datacenter.entity.OrgFundExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface OrgFundMapper {
    int countByExample(OrgFundExample example);

    int deleteByExample(OrgFundExample example);

    int deleteByPrimaryKey(String id);

    int insert(OrgFund record);

    int insertSelective(OrgFund record);

    List<OrgFund> selectByExample(OrgFundExample example);

    OrgFund selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OrgFund record, @Param("example") OrgFundExample example);

    int updateByExample(@Param("record") OrgFund record, @Param("example") OrgFundExample example);

    int updateByPrimaryKeySelective(OrgFund record);

    int updateByPrimaryKey(OrgFund record);
}