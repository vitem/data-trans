package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.FundStrategyService;
import cn.com.chinaventure.trans.datacenter.entity.FundStrategy;
import cn.com.chinaventure.trans.datacenter.entity.FundStrategyExample;
import cn.com.chinaventure.trans.datacenter.entity.FundStrategyExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.FundStrategyMapper;


@Service("fundStrategyService")
public class FundStrategyServiceImpl implements FundStrategyService {

	private final static Logger logger = LoggerFactory.getLogger(FundStrategyServiceImpl.class);

	@Resource
	private FundStrategyMapper fundStrategyMapper;


	@Override
	public void saveFundStrategy(FundStrategy fundStrategy) {
		if (null == fundStrategy) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.fundStrategyMapper.insertSelective(fundStrategy);
	}

	@Override
	public void updateFundStrategy(FundStrategy fundStrategy) {
		if (null == fundStrategy) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.fundStrategyMapper.updateByPrimaryKeySelective(fundStrategy);
	}

	@Override
	public FundStrategy getFundStrategyById(Long id) {
		return fundStrategyMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<FundStrategy> getFundStrategyList(FundStrategy fundStrategy) {
		return this.getFundStrategyListByParam(fundStrategy, null);
	}

	@Override
	public List<FundStrategy> getFundStrategyListByParam(FundStrategy fundStrategy, String orderByStr) {
		FundStrategyExample example = new FundStrategyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<FundStrategy> fundStrategyList = this.fundStrategyMapper.selectByExample(example);
		return fundStrategyList;
	}

	@Override
	public PageResult<FundStrategy> findFundStrategyPageByParam(FundStrategy fundStrategy, PageParam pageParam) {
		return this.findFundStrategyPageByParam(fundStrategy, pageParam, null);
	}

	@Override
	public PageResult<FundStrategy> findFundStrategyPageByParam(FundStrategy fundStrategy, PageParam pageParam, String orderByStr) {
		PageResult<FundStrategy> pageResult = new PageResult<FundStrategy>();

		FundStrategyExample example = new FundStrategyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<FundStrategy> list = null;// fundStrategyMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}



}
