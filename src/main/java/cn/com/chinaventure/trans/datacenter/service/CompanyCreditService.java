package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCredit;

/**
 * @author 
 *
 */
public interface CompanyCreditService {
	
	/**
     * @api saveCompanyCredit 新增companyCredit
     * @apiGroup companyCredit管理
     * @apiName  新增companyCredit记录
     * @apiDescription 全量插入companyCredit记录
     * @apiParam {CompanyCredit} companyCredit companyCredit对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyCredit(CompanyCredit companyCredit);
	
	/**
     * @api updateCompanyCredit 修改companyCredit
     * @apiGroup companyCredit管理
     * @apiName  修改companyCredit记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyCredit} companyCredit companyCredit对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyCredit(CompanyCredit companyCredit);
	
	/**
     * @api getCompanyCreditById 根据companyCreditid查询详情
     * @apiGroup companyCredit管理
     * @apiName  查询companyCredit详情
     * @apiDescription 根据主键id查询companyCredit详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyCredit companyCredit实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyCredit getCompanyCreditById(String id);
	
	/**
     * @api getCompanyCreditList 根据companyCredit条件查询列表
     * @apiGroup companyCredit管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyCredit} companyCredit  实体条件
     * @apiSuccess List<CompanyCredit> companyCredit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCredit> getCompanyCreditList(CompanyCredit companyCredit);
	

	/**
     * @api getCompanyCreditListByParam 根据companyCredit条件查询列表（含排序）
     * @apiGroup companyCredit管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyCredit} companyCredit  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyCredit> companyCredit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCredit> getCompanyCreditListByParam(CompanyCredit companyCredit, String orderByStr);
	
	
	/**
     * @api findCompanyCreditPageByParam 根据companyCredit条件查询列表（分页）
     * @apiGroup companyCredit管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyCredit} companyCredit  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyCredit> companyCredit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCredit> findCompanyCreditPageByParam(CompanyCredit companyCredit, PageParam pageParam); 
	
	/**
     * @api findCompanyCreditPageByParam 根据companyCredit条件查询列表（分页，含排序）
     * @apiGroup companyCredit管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyCredit} companyCredit  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyCredit> companyCredit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCredit> findCompanyCreditPageByParam(CompanyCredit companyCredit, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyCreditById 根据companyCredit的id删除(物理删除)
     * @apiGroup companyCredit管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyCreditById(String id);

}

