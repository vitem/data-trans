package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyright;

/**
 * @author 
 *
 */
public interface CompanyCopyrightService {
	
	/**
     * @api saveCompanyCopyright 新增companyCopyright
     * @apiGroup companyCopyright管理
     * @apiName  新增companyCopyright记录
     * @apiDescription 全量插入companyCopyright记录
     * @apiParam {CompanyCopyright} companyCopyright companyCopyright对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyCopyright(CompanyCopyright companyCopyright);
	
	/**
     * @api updateCompanyCopyright 修改companyCopyright
     * @apiGroup companyCopyright管理
     * @apiName  修改companyCopyright记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyCopyright} companyCopyright companyCopyright对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyCopyright(CompanyCopyright companyCopyright);
	
	/**
     * @api getCompanyCopyrightById 根据companyCopyrightid查询详情
     * @apiGroup companyCopyright管理
     * @apiName  查询companyCopyright详情
     * @apiDescription 根据主键id查询companyCopyright详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyCopyright companyCopyright实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyCopyright getCompanyCopyrightById(String id);
	
	/**
     * @api getCompanyCopyrightList 根据companyCopyright条件查询列表
     * @apiGroup companyCopyright管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyCopyright} companyCopyright  实体条件
     * @apiSuccess List<CompanyCopyright> companyCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCopyright> getCompanyCopyrightList(CompanyCopyright companyCopyright);
	

	/**
     * @api getCompanyCopyrightListByParam 根据companyCopyright条件查询列表（含排序）
     * @apiGroup companyCopyright管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyCopyright} companyCopyright  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyCopyright> companyCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCopyright> getCompanyCopyrightListByParam(CompanyCopyright companyCopyright, String orderByStr);
	
	
	/**
     * @api findCompanyCopyrightPageByParam 根据companyCopyright条件查询列表（分页）
     * @apiGroup companyCopyright管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyCopyright} companyCopyright  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyCopyright> companyCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCopyright> findCompanyCopyrightPageByParam(CompanyCopyright companyCopyright, PageParam pageParam); 
	
	/**
     * @api findCompanyCopyrightPageByParam 根据companyCopyright条件查询列表（分页，含排序）
     * @apiGroup companyCopyright管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyCopyright} companyCopyright  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyCopyright> companyCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCopyright> findCompanyCopyrightPageByParam(CompanyCopyright companyCopyright, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyCopyrightById 根据companyCopyright的id删除(物理删除)
     * @apiGroup companyCopyright管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyCopyrightById(String id);

}

