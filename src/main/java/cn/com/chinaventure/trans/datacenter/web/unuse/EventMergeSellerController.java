package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeSellerService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeSeller;


@RestController
@RequestMapping("/eventMergeSeller")
public class EventMergeSellerController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventMergeSellerController.class);

    @Resource
    private EventMergeSellerService eventMergeSellerService;

    /**
	 * @api {post} / 新增eventMergeSeller
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventMergeSeller(@RequestBody EventMergeSeller eventMergeSeller) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeSellerService.saveEventMergeSeller(eventMergeSeller);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventMergeSeller
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventMergeSeller(@RequestBody EventMergeSeller eventMergeSeller) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeSellerService.updateEventMergeSeller(eventMergeSeller);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventMergeSeller详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventMergeSeller> getEventMergeSellerById(@PathVariable String id) {
        JsonResult<EventMergeSeller> jr = new JsonResult<EventMergeSeller>();
        try {
            EventMergeSeller eventMergeSeller  = eventMergeSellerService.getEventMergeSellerById(id);
            jr = buildJsonResult(eventMergeSeller);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventMergeSeller/getEventMergeSellerList 查询eventMergeSeller列表(不带分页)
	 * @apiGroup eventMergeSeller管理
	 * @apiName 查询eventMergeSeller列表(不带分页)
	 * @apiDescription 根据条件查询eventMergeSeller列表(不带分页)
	 * @apiParam {EventMergeSeller} eventMergeSeller eventMergeSeller对象
	 */
    @RequestMapping("/getEventMergeSellerList")
    public JsonResult<List<EventMergeSeller>> getEventMergeSellerList(EventMergeSeller eventMergeSeller) {
        JsonResult<List<EventMergeSeller>> jr = new JsonResult<List<EventMergeSeller>>();
        try {
            List<EventMergeSeller> list = this.eventMergeSellerService.getEventMergeSellerList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventMergeSeller/findEventMergeSellerPageByParam 查询eventMergeSeller列表(带分页)
	 * @apiGroup eventMergeSeller管理
	 * @apiName 查询eventMergeSeller列表(带分页)
	 * @apiDescription 根据条件查询eventMergeSeller列表(带分页)
	 * @apiParam {EventMergeSeller} eventMergeSeller eventMergeSeller对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventMergeSellerPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventMergeSeller>> findEventMergeSellerPageByParam(@RequestBody EventMergeSeller eventMergeSeller, PageParam pageParam) {
        JsonResult<PageResult<EventMergeSeller>> jr = new JsonResult<PageResult<EventMergeSeller>>();
        try {
            PageResult<EventMergeSeller> page = this.eventMergeSellerService.findEventMergeSellerPageByParam(eventMergeSeller, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventMergeSeller/deleteEventMergeSellerById 删除eventMergeSeller(物理删除)
	 * @apiGroup eventMergeSeller管理
	 * @apiName 删除eventMergeSeller记录(物理删除)
	 * @apiDescription 根据主键id删除eventMergeSeller记录(物理删除)
	 * @apiParam {String} id eventMergeSeller主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventMergeSellerById",method=RequestMethod.DELETE)
    public JsonResult deleteEventMergeSellerById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeSellerService.deleteEventMergeSellerById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
