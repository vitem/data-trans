package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCopyrightService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyright;


@RestController
@RequestMapping("/companyCopyright")
public class CompanyCopyrightController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyCopyrightController.class);

    @Resource
    private CompanyCopyrightService companyCopyrightService;

    /**
	 * @api {post} / 新增companyCopyright
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyCopyright(@RequestBody CompanyCopyright companyCopyright) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCopyrightService.saveCompanyCopyright(companyCopyright);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyCopyright
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyCopyright(@RequestBody CompanyCopyright companyCopyright) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCopyrightService.updateCompanyCopyright(companyCopyright);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyCopyright详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyCopyright> getCompanyCopyrightById(@PathVariable String id) {
        JsonResult<CompanyCopyright> jr = new JsonResult<CompanyCopyright>();
        try {
            CompanyCopyright companyCopyright  = companyCopyrightService.getCompanyCopyrightById(id);
            jr = buildJsonResult(companyCopyright);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyCopyright/getCompanyCopyrightList 查询companyCopyright列表(不带分页)
	 * @apiGroup companyCopyright管理
	 * @apiName 查询companyCopyright列表(不带分页)
	 * @apiDescription 根据条件查询companyCopyright列表(不带分页)
	 * @apiParam {CompanyCopyright} companyCopyright companyCopyright对象
	 */
    @RequestMapping("/getCompanyCopyrightList")
    public JsonResult<List<CompanyCopyright>> getCompanyCopyrightList(CompanyCopyright companyCopyright) {
        JsonResult<List<CompanyCopyright>> jr = new JsonResult<List<CompanyCopyright>>();
        try {
            List<CompanyCopyright> list = this.companyCopyrightService.getCompanyCopyrightList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyCopyright/findCompanyCopyrightPageByParam 查询companyCopyright列表(带分页)
	 * @apiGroup companyCopyright管理
	 * @apiName 查询companyCopyright列表(带分页)
	 * @apiDescription 根据条件查询companyCopyright列表(带分页)
	 * @apiParam {CompanyCopyright} companyCopyright companyCopyright对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyCopyrightPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyCopyright>> findCompanyCopyrightPageByParam(@RequestBody CompanyCopyright companyCopyright, PageParam pageParam) {
        JsonResult<PageResult<CompanyCopyright>> jr = new JsonResult<PageResult<CompanyCopyright>>();
        try {
            PageResult<CompanyCopyright> page = this.companyCopyrightService.findCompanyCopyrightPageByParam(companyCopyright, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyCopyright/deleteCompanyCopyrightById 删除companyCopyright(物理删除)
	 * @apiGroup companyCopyright管理
	 * @apiName 删除companyCopyright记录(物理删除)
	 * @apiDescription 根据主键id删除companyCopyright记录(物理删除)
	 * @apiParam {String} id companyCopyright主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyCopyrightById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyCopyrightById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCopyrightService.deleteCompanyCopyrightById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
