package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.PersonageType;
import cn.com.chinaventure.trans.datacenter.entity.PersonageTypeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PersonageTypeMapper {
    int countByExample(PersonageTypeExample example);

    int deleteByExample(PersonageTypeExample example);

    int deleteByPrimaryKey(String id);

    int insert(PersonageType record);

    int insertSelective(PersonageType record);

    List<PersonageType> selectByExample(PersonageTypeExample example);

    PersonageType selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") PersonageType record, @Param("example") PersonageTypeExample example);

    int updateByExample(@Param("record") PersonageType record, @Param("example") PersonageTypeExample example);

    int updateByPrimaryKeySelective(PersonageType record);

    int updateByPrimaryKey(PersonageType record);
}