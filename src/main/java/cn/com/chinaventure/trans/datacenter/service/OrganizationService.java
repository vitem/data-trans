package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Organization;

/**
 * @author 
 *
 */
public interface OrganizationService {
	
	/**
     * @api saveOrganization 新增organization
     * @apiGroup organization管理
     * @apiName  新增organization记录
     * @apiDescription 全量插入organization记录
     * @apiParam {Organization} organization organization对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveOrganization(Organization organization);
	
	/**
     * @api updateOrganization 修改organization
     * @apiGroup organization管理
     * @apiName  修改organization记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Organization} organization organization对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateOrganization(Organization organization);
	
	/**
     * @api getOrganizationById 根据organizationid查询详情
     * @apiGroup organization管理
     * @apiName  查询organization详情
     * @apiDescription 根据主键id查询organization详情
     * @apiParam {String} id 主键id
     * @apiSuccess Organization organization实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Organization getOrganizationById(String id);
	
	/**
     * @api getOrganizationList 根据organization条件查询列表
     * @apiGroup organization管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Organization} organization  实体条件
     * @apiSuccess List<Organization> organization实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Organization> getOrganizationList(Organization organization);
	

	/**
     * @api getOrganizationListByParam 根据organization条件查询列表（含排序）
     * @apiGroup organization管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Organization} organization  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Organization> organization实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Organization> getOrganizationListByParam(Organization organization, String orderByStr);
	
	
	/**
     * @api findOrganizationPageByParam 根据organization条件查询列表（分页）
     * @apiGroup organization管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Organization} organization  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Organization> organization实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Organization> findOrganizationPageByParam(Organization organization, PageParam pageParam); 
	
	/**
     * @api findOrganizationPageByParam 根据organization条件查询列表（分页，含排序）
     * @apiGroup organization管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Organization} organization  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Organization> organization实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Organization> findOrganizationPageByParam(Organization organization, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteOrganizationById 根据organization的id删除(物理删除)
     * @apiGroup organization管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteOrganizationById(String id);

}

