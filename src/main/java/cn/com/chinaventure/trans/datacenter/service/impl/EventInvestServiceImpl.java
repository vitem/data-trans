package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventInvestService;
import cn.com.chinaventure.trans.datacenter.entity.EventInvest;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestExample;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventInvestMapper;


@Service("eventInvestService")
public class EventInvestServiceImpl implements EventInvestService {

	private final static Logger logger = LoggerFactory.getLogger(EventInvestServiceImpl.class);

	@Resource
	private EventInvestMapper eventInvestMapper;


	@Override
	public void saveEventInvest(EventInvest eventInvest) {
		if (null == eventInvest) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventInvestMapper.insertSelective(eventInvest);
	}

	@Override
	public void updateEventInvest(EventInvest eventInvest) {
		if (null == eventInvest) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventInvestMapper.updateByPrimaryKeySelective(eventInvest);
	}

	@Override
	public EventInvest getEventInvestById(String id) {
		return eventInvestMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventInvest> getEventInvestList(EventInvest eventInvest) {
		return this.getEventInvestListByParam(eventInvest, null);
	}

	@Override
	public List<EventInvest> getEventInvestListByParam(EventInvest eventInvest, String orderByStr) {
		EventInvestExample example = new EventInvestExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventInvest> eventInvestList = this.eventInvestMapper.selectByExample(example);
		return eventInvestList;
	}

	@Override
	public PageResult<EventInvest> findEventInvestPageByParam(EventInvest eventInvest, PageParam pageParam) {
		return this.findEventInvestPageByParam(eventInvest, pageParam, null);
	}

	@Override
	public PageResult<EventInvest> findEventInvestPageByParam(EventInvest eventInvest, PageParam pageParam, String orderByStr) {
		PageResult<EventInvest> pageResult = new PageResult<EventInvest>();

		EventInvestExample example = new EventInvestExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventInvest> list = null;// eventInvestMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventInvestById(String id) {
		this.eventInvestMapper.deleteByPrimaryKey(id);
	}

}
