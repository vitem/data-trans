package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.PersonageType;

/**
 * @author 
 *
 */
public interface PersonageTypeService {
	
	/**
     * @api savePersonageType 新增personageType
     * @apiGroup personageType管理
     * @apiName  新增personageType记录
     * @apiDescription 全量插入personageType记录
     * @apiParam {PersonageType} personageType personageType对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void savePersonageType(PersonageType personageType);
	
	/**
     * @api updatePersonageType 修改personageType
     * @apiGroup personageType管理
     * @apiName  修改personageType记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {PersonageType} personageType personageType对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updatePersonageType(PersonageType personageType);
	
	/**
     * @api getPersonageTypeById 根据personageTypeid查询详情
     * @apiGroup personageType管理
     * @apiName  查询personageType详情
     * @apiDescription 根据主键id查询personageType详情
     * @apiParam {String} id 主键id
     * @apiSuccess PersonageType personageType实体对象
     * @apiVersion 1.0.0
     * 
     */
	public PersonageType getPersonageTypeById(String id);
	
	/**
     * @api getPersonageTypeList 根据personageType条件查询列表
     * @apiGroup personageType管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {PersonageType} personageType  实体条件
     * @apiSuccess List<PersonageType> personageType实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<PersonageType> getPersonageTypeList(PersonageType personageType);
	

	/**
     * @api getPersonageTypeListByParam 根据personageType条件查询列表（含排序）
     * @apiGroup personageType管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {PersonageType} personageType  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<PersonageType> personageType实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<PersonageType> getPersonageTypeListByParam(PersonageType personageType, String orderByStr);
	
	
	/**
     * @api findPersonageTypePageByParam 根据personageType条件查询列表（分页）
     * @apiGroup personageType管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {PersonageType} personageType  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<PersonageType> personageType实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<PersonageType> findPersonageTypePageByParam(PersonageType personageType, PageParam pageParam); 
	
	/**
     * @api findPersonageTypePageByParam 根据personageType条件查询列表（分页，含排序）
     * @apiGroup personageType管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {PersonageType} personageType  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<PersonageType> personageType实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<PersonageType> findPersonageTypePageByParam(PersonageType personageType, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deletePersonageTypeById 根据personageType的id删除(物理删除)
     * @apiGroup personageType管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deletePersonageTypeById(String id);

}

