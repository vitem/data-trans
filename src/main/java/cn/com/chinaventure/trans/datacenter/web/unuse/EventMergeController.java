package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeService;
import cn.com.chinaventure.trans.datacenter.entity.EventMerge;


@RestController
@RequestMapping("/eventMerge")
public class EventMergeController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventMergeController.class);

    @Resource
    private EventMergeService eventMergeService;

    /**
	 * @api {post} / 新增eventMerge
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventMerge(@RequestBody EventMerge eventMerge) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeService.saveEventMerge(eventMerge);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventMerge
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventMerge(@RequestBody EventMerge eventMerge) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeService.updateEventMerge(eventMerge);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventMerge详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventMerge> getEventMergeById(@PathVariable String id) {
        JsonResult<EventMerge> jr = new JsonResult<EventMerge>();
        try {
            EventMerge eventMerge  = eventMergeService.getEventMergeById(id);
            jr = buildJsonResult(eventMerge);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventMerge/getEventMergeList 查询eventMerge列表(不带分页)
	 * @apiGroup eventMerge管理
	 * @apiName 查询eventMerge列表(不带分页)
	 * @apiDescription 根据条件查询eventMerge列表(不带分页)
	 * @apiParam {EventMerge} eventMerge eventMerge对象
	 */
    @RequestMapping("/getEventMergeList")
    public JsonResult<List<EventMerge>> getEventMergeList(EventMerge eventMerge) {
        JsonResult<List<EventMerge>> jr = new JsonResult<List<EventMerge>>();
        try {
            List<EventMerge> list = this.eventMergeService.getEventMergeList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventMerge/findEventMergePageByParam 查询eventMerge列表(带分页)
	 * @apiGroup eventMerge管理
	 * @apiName 查询eventMerge列表(带分页)
	 * @apiDescription 根据条件查询eventMerge列表(带分页)
	 * @apiParam {EventMerge} eventMerge eventMerge对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventMergePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventMerge>> findEventMergePageByParam(@RequestBody EventMerge eventMerge, PageParam pageParam) {
        JsonResult<PageResult<EventMerge>> jr = new JsonResult<PageResult<EventMerge>>();
        try {
            PageResult<EventMerge> page = this.eventMergeService.findEventMergePageByParam(eventMerge, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventMerge/deleteEventMergeById 删除eventMerge(物理删除)
	 * @apiGroup eventMerge管理
	 * @apiName 删除eventMerge记录(物理删除)
	 * @apiDescription 根据主键id删除eventMerge记录(物理删除)
	 * @apiParam {String} id eventMerge主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventMergeById",method=RequestMethod.DELETE)
    public JsonResult deleteEventMergeById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeService.deleteEventMergeById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
