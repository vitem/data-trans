package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventInvestOrgService;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrg;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrgExample;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrgExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventInvestOrgMapper;


@Service("eventInvestOrgService")
public class EventInvestOrgServiceImpl implements EventInvestOrgService {

	private final static Logger logger = LoggerFactory.getLogger(EventInvestOrgServiceImpl.class);

	@Resource
	private EventInvestOrgMapper eventInvestOrgMapper;


	@Override
	public void saveEventInvestOrg(EventInvestOrg eventInvestOrg) {
		if (null == eventInvestOrg) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventInvestOrgMapper.insertSelective(eventInvestOrg);
	}

	@Override
	public void updateEventInvestOrg(EventInvestOrg eventInvestOrg) {
		if (null == eventInvestOrg) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventInvestOrgMapper.updateByPrimaryKeySelective(eventInvestOrg);
	}

	@Override
	public EventInvestOrg getEventInvestOrgById(String id) {
		return eventInvestOrgMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventInvestOrg> getEventInvestOrgList(EventInvestOrg eventInvestOrg) {
		return this.getEventInvestOrgListByParam(eventInvestOrg, null);
	}

	@Override
	public List<EventInvestOrg> getEventInvestOrgListByParam(EventInvestOrg eventInvestOrg, String orderByStr) {
		EventInvestOrgExample example = new EventInvestOrgExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventInvestOrg> eventInvestOrgList = this.eventInvestOrgMapper.selectByExample(example);
		return eventInvestOrgList;
	}

	@Override
	public PageResult<EventInvestOrg> findEventInvestOrgPageByParam(EventInvestOrg eventInvestOrg, PageParam pageParam) {
		return this.findEventInvestOrgPageByParam(eventInvestOrg, pageParam, null);
	}

	@Override
	public PageResult<EventInvestOrg> findEventInvestOrgPageByParam(EventInvestOrg eventInvestOrg, PageParam pageParam, String orderByStr) {
		PageResult<EventInvestOrg> pageResult = new PageResult<EventInvestOrg>();

		EventInvestOrgExample example = new EventInvestOrgExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventInvestOrg> list = null;// eventInvestOrgMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventInvestOrgById(String id) {
		this.eventInvestOrgMapper.deleteByPrimaryKey(id);
	}

}
