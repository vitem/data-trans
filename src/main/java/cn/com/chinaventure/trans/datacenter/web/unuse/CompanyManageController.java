package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.trans.datacenter.entity.CompanyManage;
import cn.com.chinaventure.trans.datacenter.service.CompanyManageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/companyManage")
public class CompanyManageController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyManageController.class);

    @Resource
    private CompanyManageService companyManageService;

    /**
	 * @api {post} / 新增companyManage
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyManage(@RequestBody CompanyManage companyManage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyManageService.saveCompanyManage(companyManage);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyManage
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyManage(@RequestBody CompanyManage companyManage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyManageService.updateCompanyManage(companyManage);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyManage详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyManage> getCompanyManageById(@PathVariable String id) {
        JsonResult<CompanyManage> jr = new JsonResult<CompanyManage>();
        try {
            CompanyManage companyManage  = companyManageService.getCompanyManageById(id);
            jr = buildJsonResult(companyManage);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyManage/getCompanyManageList 查询companyManage列表(不带分页)
	 * @apiGroup companyManage管理
	 * @apiName 查询companyManage列表(不带分页)
	 * @apiDescription 根据条件查询companyManage列表(不带分页)
	 * @apiParam {CompanyManage} companyManage companyManage对象
	 */
    @RequestMapping("/getCompanyManageList")
    public JsonResult<List<CompanyManage>> getCompanyManageList(CompanyManage companyManage) {
        JsonResult<List<CompanyManage>> jr = new JsonResult<List<CompanyManage>>();
        try {
            List<CompanyManage> list = this.companyManageService.getCompanyManageList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyManage/findCompanyManagePageByParam 查询companyManage列表(带分页)
	 * @apiGroup companyManage管理
	 * @apiName 查询companyManage列表(带分页)
	 * @apiDescription 根据条件查询companyManage列表(带分页)
	 * @apiParam {CompanyManage} companyManage companyManage对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyManagePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyManage>> findCompanyManagePageByParam(@RequestBody CompanyManage companyManage, PageParam pageParam) {
        JsonResult<PageResult<CompanyManage>> jr = new JsonResult<PageResult<CompanyManage>>();
        try {
            PageResult<CompanyManage> page = this.companyManageService.findCompanyManagePageByParam(companyManage, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyManage/deleteCompanyManageById 删除companyManage(物理删除)
	 * @apiGroup companyManage管理
	 * @apiName 删除companyManage记录(物理删除)
	 * @apiDescription 根据主键id删除companyManage记录(物理删除)
	 * @apiParam {String} id companyManage主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyManageById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyManageById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyManageService.deleteCompanyManageById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
