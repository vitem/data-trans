package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventExitOrgPersonageService;
import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonage;


@RestController
@RequestMapping("/eventExitOrgPersonage")
public class EventExitOrgPersonageController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventExitOrgPersonageController.class);

    @Resource
    private EventExitOrgPersonageService eventExitOrgPersonageService;

    /**
	 * @api {post} / 新增eventExitOrgPersonage
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventExitOrgPersonage(@RequestBody EventExitOrgPersonage eventExitOrgPersonage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventExitOrgPersonageService.saveEventExitOrgPersonage(eventExitOrgPersonage);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventExitOrgPersonage
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventExitOrgPersonage(@RequestBody EventExitOrgPersonage eventExitOrgPersonage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventExitOrgPersonageService.updateEventExitOrgPersonage(eventExitOrgPersonage);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventExitOrgPersonage详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventExitOrgPersonage> getEventExitOrgPersonageById(@PathVariable String id) {
        JsonResult<EventExitOrgPersonage> jr = new JsonResult<EventExitOrgPersonage>();
        try {
            EventExitOrgPersonage eventExitOrgPersonage  = eventExitOrgPersonageService.getEventExitOrgPersonageById(id);
            jr = buildJsonResult(eventExitOrgPersonage);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventExitOrgPersonage/getEventExitOrgPersonageList 查询eventExitOrgPersonage列表(不带分页)
	 * @apiGroup eventExitOrgPersonage管理
	 * @apiName 查询eventExitOrgPersonage列表(不带分页)
	 * @apiDescription 根据条件查询eventExitOrgPersonage列表(不带分页)
	 * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage eventExitOrgPersonage对象
	 */
    @RequestMapping("/getEventExitOrgPersonageList")
    public JsonResult<List<EventExitOrgPersonage>> getEventExitOrgPersonageList(EventExitOrgPersonage eventExitOrgPersonage) {
        JsonResult<List<EventExitOrgPersonage>> jr = new JsonResult<List<EventExitOrgPersonage>>();
        try {
            List<EventExitOrgPersonage> list = this.eventExitOrgPersonageService.getEventExitOrgPersonageList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventExitOrgPersonage/findEventExitOrgPersonagePageByParam 查询eventExitOrgPersonage列表(带分页)
	 * @apiGroup eventExitOrgPersonage管理
	 * @apiName 查询eventExitOrgPersonage列表(带分页)
	 * @apiDescription 根据条件查询eventExitOrgPersonage列表(带分页)
	 * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage eventExitOrgPersonage对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventExitOrgPersonagePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventExitOrgPersonage>> findEventExitOrgPersonagePageByParam(@RequestBody EventExitOrgPersonage eventExitOrgPersonage, PageParam pageParam) {
        JsonResult<PageResult<EventExitOrgPersonage>> jr = new JsonResult<PageResult<EventExitOrgPersonage>>();
        try {
            PageResult<EventExitOrgPersonage> page = this.eventExitOrgPersonageService.findEventExitOrgPersonagePageByParam(eventExitOrgPersonage, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventExitOrgPersonage/deleteEventExitOrgPersonageById 删除eventExitOrgPersonage(物理删除)
	 * @apiGroup eventExitOrgPersonage管理
	 * @apiName 删除eventExitOrgPersonage记录(物理删除)
	 * @apiDescription 根据主键id删除eventExitOrgPersonage记录(物理删除)
	 * @apiParam {String} id eventExitOrgPersonage主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventExitOrgPersonageById",method=RequestMethod.DELETE)
    public JsonResult deleteEventExitOrgPersonageById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventExitOrgPersonageService.deleteEventExitOrgPersonageById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
