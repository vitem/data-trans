package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructure;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructureExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyStockStructureMapper {
    int countByExample(CompanyStockStructureExample example);

    int deleteByExample(CompanyStockStructureExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyStockStructure record);

    int insertSelective(CompanyStockStructure record);

    List<CompanyStockStructure> selectByExample(CompanyStockStructureExample example);

    CompanyStockStructure selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyStockStructure record, @Param("example") CompanyStockStructureExample example);

    int updateByExample(@Param("record") CompanyStockStructure record, @Param("example") CompanyStockStructureExample example);

    int updateByPrimaryKeySelective(CompanyStockStructure record);

    int updateByPrimaryKey(CompanyStockStructure record);
}