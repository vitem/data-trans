package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeAppraiseService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraise;


@RestController
@RequestMapping("/eventMergeAppraise")
public class EventMergeAppraiseController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventMergeAppraiseController.class);

    @Resource
    private EventMergeAppraiseService eventMergeAppraiseService;

    /**
	 * @api {post} / 新增eventMergeAppraise
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventMergeAppraise(@RequestBody EventMergeAppraise eventMergeAppraise) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeAppraiseService.saveEventMergeAppraise(eventMergeAppraise);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventMergeAppraise
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventMergeAppraise(@RequestBody EventMergeAppraise eventMergeAppraise) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeAppraiseService.updateEventMergeAppraise(eventMergeAppraise);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventMergeAppraise详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventMergeAppraise> getEventMergeAppraiseById(@PathVariable String id) {
        JsonResult<EventMergeAppraise> jr = new JsonResult<EventMergeAppraise>();
        try {
            EventMergeAppraise eventMergeAppraise  = eventMergeAppraiseService.getEventMergeAppraiseById(id);
            jr = buildJsonResult(eventMergeAppraise);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventMergeAppraise/getEventMergeAppraiseList 查询eventMergeAppraise列表(不带分页)
	 * @apiGroup eventMergeAppraise管理
	 * @apiName 查询eventMergeAppraise列表(不带分页)
	 * @apiDescription 根据条件查询eventMergeAppraise列表(不带分页)
	 * @apiParam {EventMergeAppraise} eventMergeAppraise eventMergeAppraise对象
	 */
    @RequestMapping("/getEventMergeAppraiseList")
    public JsonResult<List<EventMergeAppraise>> getEventMergeAppraiseList(EventMergeAppraise eventMergeAppraise) {
        JsonResult<List<EventMergeAppraise>> jr = new JsonResult<List<EventMergeAppraise>>();
        try {
            List<EventMergeAppraise> list = this.eventMergeAppraiseService.getEventMergeAppraiseList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventMergeAppraise/findEventMergeAppraisePageByParam 查询eventMergeAppraise列表(带分页)
	 * @apiGroup eventMergeAppraise管理
	 * @apiName 查询eventMergeAppraise列表(带分页)
	 * @apiDescription 根据条件查询eventMergeAppraise列表(带分页)
	 * @apiParam {EventMergeAppraise} eventMergeAppraise eventMergeAppraise对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventMergeAppraisePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventMergeAppraise>> findEventMergeAppraisePageByParam(@RequestBody EventMergeAppraise eventMergeAppraise, PageParam pageParam) {
        JsonResult<PageResult<EventMergeAppraise>> jr = new JsonResult<PageResult<EventMergeAppraise>>();
        try {
            PageResult<EventMergeAppraise> page = this.eventMergeAppraiseService.findEventMergeAppraisePageByParam(eventMergeAppraise, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventMergeAppraise/deleteEventMergeAppraiseById 删除eventMergeAppraise(物理删除)
	 * @apiGroup eventMergeAppraise管理
	 * @apiName 删除eventMergeAppraise记录(物理删除)
	 * @apiDescription 根据主键id删除eventMergeAppraise记录(物理删除)
	 * @apiParam {String} id eventMergeAppraise主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventMergeAppraiseById",method=RequestMethod.DELETE)
    public JsonResult deleteEventMergeAppraiseById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeAppraiseService.deleteEventMergeAppraiseById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
