package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrg;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrgExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EnterpriseOrgMapper {
    int countByExample(EnterpriseOrgExample example);

    int deleteByExample(EnterpriseOrgExample example);

    int deleteByPrimaryKey(String id);

    int insert(EnterpriseOrg record);

    int insertSelective(EnterpriseOrg record);

    List<EnterpriseOrg> selectByExample(EnterpriseOrgExample example);

    EnterpriseOrg selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EnterpriseOrg record, @Param("example") EnterpriseOrgExample example);

    int updateByExample(@Param("record") EnterpriseOrg record, @Param("example") EnterpriseOrgExample example);

    int updateByPrimaryKeySelective(EnterpriseOrg record);

    int updateByPrimaryKey(EnterpriseOrg record);
}