package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventIpoService;
import cn.com.chinaventure.trans.datacenter.entity.EventIpo;


@RestController
@RequestMapping("/eventIpo")
public class EventIpoController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventIpoController.class);

    @Resource
    private EventIpoService eventIpoService;

    /**
	 * @api {post} / 新增eventIpo
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventIpo(@RequestBody EventIpo eventIpo) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventIpoService.saveEventIpo(eventIpo);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventIpo
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventIpo(@RequestBody EventIpo eventIpo) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventIpoService.updateEventIpo(eventIpo);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventIpo详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventIpo> getEventIpoById(@PathVariable String id) {
        JsonResult<EventIpo> jr = new JsonResult<EventIpo>();
        try {
            EventIpo eventIpo  = eventIpoService.getEventIpoById(id);
            jr = buildJsonResult(eventIpo);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventIpo/getEventIpoList 查询eventIpo列表(不带分页)
	 * @apiGroup eventIpo管理
	 * @apiName 查询eventIpo列表(不带分页)
	 * @apiDescription 根据条件查询eventIpo列表(不带分页)
	 * @apiParam {EventIpo} eventIpo eventIpo对象
	 */
    @RequestMapping("/getEventIpoList")
    public JsonResult<List<EventIpo>> getEventIpoList(EventIpo eventIpo) {
        JsonResult<List<EventIpo>> jr = new JsonResult<List<EventIpo>>();
        try {
            List<EventIpo> list = this.eventIpoService.getEventIpoList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventIpo/findEventIpoPageByParam 查询eventIpo列表(带分页)
	 * @apiGroup eventIpo管理
	 * @apiName 查询eventIpo列表(带分页)
	 * @apiDescription 根据条件查询eventIpo列表(带分页)
	 * @apiParam {EventIpo} eventIpo eventIpo对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventIpoPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventIpo>> findEventIpoPageByParam(@RequestBody EventIpo eventIpo, PageParam pageParam) {
        JsonResult<PageResult<EventIpo>> jr = new JsonResult<PageResult<EventIpo>>();
        try {
            PageResult<EventIpo> page = this.eventIpoService.findEventIpoPageByParam(eventIpo, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventIpo/deleteEventIpoById 删除eventIpo(物理删除)
	 * @apiGroup eventIpo管理
	 * @apiName 删除eventIpo记录(物理删除)
	 * @apiDescription 根据主键id删除eventIpo记录(物理删除)
	 * @apiParam {String} id eventIpo主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventIpoById",method=RequestMethod.DELETE)
    public JsonResult deleteEventIpoById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventIpoService.deleteEventIpoById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
