package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCourtJudgeService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudge;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudgeExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudgeExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyCourtJudgeMapper;


@Service("companyCourtJudgeService")
public class CompanyCourtJudgeServiceImpl implements CompanyCourtJudgeService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyCourtJudgeServiceImpl.class);

	@Resource
	private CompanyCourtJudgeMapper companyCourtJudgeMapper;


	@Override
	public void saveCompanyCourtJudge(CompanyCourtJudge companyCourtJudge) {
		if (null == companyCourtJudge) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCourtJudgeMapper.insertSelective(companyCourtJudge);
	}

	@Override
	public void updateCompanyCourtJudge(CompanyCourtJudge companyCourtJudge) {
		if (null == companyCourtJudge) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCourtJudgeMapper.updateByPrimaryKeySelective(companyCourtJudge);
	}

	@Override
	public CompanyCourtJudge getCompanyCourtJudgeById(String id) {
		return companyCourtJudgeMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyCourtJudge> getCompanyCourtJudgeList(CompanyCourtJudge companyCourtJudge) {
		return this.getCompanyCourtJudgeListByParam(companyCourtJudge, null);
	}

	@Override
	public List<CompanyCourtJudge> getCompanyCourtJudgeListByParam(CompanyCourtJudge companyCourtJudge, String orderByStr) {
		CompanyCourtJudgeExample example = new CompanyCourtJudgeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyCourtJudge> companyCourtJudgeList = this.companyCourtJudgeMapper.selectByExample(example);
		return companyCourtJudgeList;
	}

	@Override
	public PageResult<CompanyCourtJudge> findCompanyCourtJudgePageByParam(CompanyCourtJudge companyCourtJudge, PageParam pageParam) {
		return this.findCompanyCourtJudgePageByParam(companyCourtJudge, pageParam, null);
	}

	@Override
	public PageResult<CompanyCourtJudge> findCompanyCourtJudgePageByParam(CompanyCourtJudge companyCourtJudge, PageParam pageParam, String orderByStr) {
		PageResult<CompanyCourtJudge> pageResult = new PageResult<CompanyCourtJudge>();

		CompanyCourtJudgeExample example = new CompanyCourtJudgeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyCourtJudge> list = null;// companyCourtJudgeMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyCourtJudgeById(String id) {
		this.companyCourtJudgeMapper.deleteByPrimaryKey(id);
	}

}
