package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventExitOrgPersonageService;
import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonage;
import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonageExample;
import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonageExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventExitOrgPersonageMapper;


@Service("eventExitOrgPersonageService")
public class EventExitOrgPersonageServiceImpl implements EventExitOrgPersonageService {

	private final static Logger logger = LoggerFactory.getLogger(EventExitOrgPersonageServiceImpl.class);

	@Resource
	private EventExitOrgPersonageMapper eventExitOrgPersonageMapper;


	@Override
	public void saveEventExitOrgPersonage(EventExitOrgPersonage eventExitOrgPersonage) {
		if (null == eventExitOrgPersonage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventExitOrgPersonageMapper.insertSelective(eventExitOrgPersonage);
	}

	@Override
	public void updateEventExitOrgPersonage(EventExitOrgPersonage eventExitOrgPersonage) {
		if (null == eventExitOrgPersonage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventExitOrgPersonageMapper.updateByPrimaryKeySelective(eventExitOrgPersonage);
	}

	@Override
	public EventExitOrgPersonage getEventExitOrgPersonageById(String id) {
		return eventExitOrgPersonageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventExitOrgPersonage> getEventExitOrgPersonageList(EventExitOrgPersonage eventExitOrgPersonage) {
		return this.getEventExitOrgPersonageListByParam(eventExitOrgPersonage, null);
	}

	@Override
	public List<EventExitOrgPersonage> getEventExitOrgPersonageListByParam(EventExitOrgPersonage eventExitOrgPersonage, String orderByStr) {
		EventExitOrgPersonageExample example = new EventExitOrgPersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventExitOrgPersonage> eventExitOrgPersonageList = this.eventExitOrgPersonageMapper.selectByExample(example);
		return eventExitOrgPersonageList;
	}

	@Override
	public PageResult<EventExitOrgPersonage> findEventExitOrgPersonagePageByParam(EventExitOrgPersonage eventExitOrgPersonage, PageParam pageParam) {
		return this.findEventExitOrgPersonagePageByParam(eventExitOrgPersonage, pageParam, null);
	}

	@Override
	public PageResult<EventExitOrgPersonage> findEventExitOrgPersonagePageByParam(EventExitOrgPersonage eventExitOrgPersonage, PageParam pageParam, String orderByStr) {
		PageResult<EventExitOrgPersonage> pageResult = new PageResult<EventExitOrgPersonage>();

		EventExitOrgPersonageExample example = new EventExitOrgPersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventExitOrgPersonage> list = null;// eventExitOrgPersonageMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventExitOrgPersonageById(String id) {
		this.eventExitOrgPersonageMapper.deleteByPrimaryKey(id);
	}

}
