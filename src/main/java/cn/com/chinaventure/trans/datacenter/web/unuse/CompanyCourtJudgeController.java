package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCourtJudgeService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudge;


@RestController
@RequestMapping("/companyCourtJudge")
public class CompanyCourtJudgeController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyCourtJudgeController.class);

    @Resource
    private CompanyCourtJudgeService companyCourtJudgeService;

    /**
	 * @api {post} / 新增companyCourtJudge
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyCourtJudge(@RequestBody CompanyCourtJudge companyCourtJudge) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCourtJudgeService.saveCompanyCourtJudge(companyCourtJudge);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyCourtJudge
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyCourtJudge(@RequestBody CompanyCourtJudge companyCourtJudge) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCourtJudgeService.updateCompanyCourtJudge(companyCourtJudge);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyCourtJudge详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyCourtJudge> getCompanyCourtJudgeById(@PathVariable String id) {
        JsonResult<CompanyCourtJudge> jr = new JsonResult<CompanyCourtJudge>();
        try {
            CompanyCourtJudge companyCourtJudge  = companyCourtJudgeService.getCompanyCourtJudgeById(id);
            jr = buildJsonResult(companyCourtJudge);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyCourtJudge/getCompanyCourtJudgeList 查询companyCourtJudge列表(不带分页)
	 * @apiGroup companyCourtJudge管理
	 * @apiName 查询companyCourtJudge列表(不带分页)
	 * @apiDescription 根据条件查询companyCourtJudge列表(不带分页)
	 * @apiParam {CompanyCourtJudge} companyCourtJudge companyCourtJudge对象
	 */
    @RequestMapping("/getCompanyCourtJudgeList")
    public JsonResult<List<CompanyCourtJudge>> getCompanyCourtJudgeList(CompanyCourtJudge companyCourtJudge) {
        JsonResult<List<CompanyCourtJudge>> jr = new JsonResult<List<CompanyCourtJudge>>();
        try {
            List<CompanyCourtJudge> list = this.companyCourtJudgeService.getCompanyCourtJudgeList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyCourtJudge/findCompanyCourtJudgePageByParam 查询companyCourtJudge列表(带分页)
	 * @apiGroup companyCourtJudge管理
	 * @apiName 查询companyCourtJudge列表(带分页)
	 * @apiDescription 根据条件查询companyCourtJudge列表(带分页)
	 * @apiParam {CompanyCourtJudge} companyCourtJudge companyCourtJudge对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyCourtJudgePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyCourtJudge>> findCompanyCourtJudgePageByParam(@RequestBody CompanyCourtJudge companyCourtJudge, PageParam pageParam) {
        JsonResult<PageResult<CompanyCourtJudge>> jr = new JsonResult<PageResult<CompanyCourtJudge>>();
        try {
            PageResult<CompanyCourtJudge> page = this.companyCourtJudgeService.findCompanyCourtJudgePageByParam(companyCourtJudge, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyCourtJudge/deleteCompanyCourtJudgeById 删除companyCourtJudge(物理删除)
	 * @apiGroup companyCourtJudge管理
	 * @apiName 删除companyCourtJudge记录(物理删除)
	 * @apiDescription 根据主键id删除companyCourtJudge记录(物理删除)
	 * @apiParam {String} id companyCourtJudge主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyCourtJudgeById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyCourtJudgeById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCourtJudgeService.deleteCompanyCourtJudgeById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
