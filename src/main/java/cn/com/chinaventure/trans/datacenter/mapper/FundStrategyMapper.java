package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.FundStrategy;
import cn.com.chinaventure.trans.datacenter.entity.FundStrategyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface FundStrategyMapper {
    int countByExample(FundStrategyExample example);

    int deleteByExample(FundStrategyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(FundStrategy record);

    int insertSelective(FundStrategy record);

    List<FundStrategy> selectByExample(FundStrategyExample example);

    FundStrategy selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") FundStrategy record, @Param("example") FundStrategyExample example);

    int updateByExample(@Param("record") FundStrategy record, @Param("example") FundStrategyExample example);

    int updateByPrimaryKeySelective(FundStrategy record);

    int updateByPrimaryKey(FundStrategy record);
}