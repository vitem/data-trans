package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyright;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyrightExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyCopyrightMapper {
    int countByExample(CompanyCopyrightExample example);

    int deleteByExample(CompanyCopyrightExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyCopyright record);

    int insertSelective(CompanyCopyright record);

    List<CompanyCopyright> selectByExample(CompanyCopyrightExample example);

    CompanyCopyright selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyCopyright record, @Param("example") CompanyCopyrightExample example);

    int updateByExample(@Param("record") CompanyCopyright record, @Param("example") CompanyCopyrightExample example);

    int updateByPrimaryKeySelective(CompanyCopyright record);

    int updateByPrimaryKey(CompanyCopyright record);
}