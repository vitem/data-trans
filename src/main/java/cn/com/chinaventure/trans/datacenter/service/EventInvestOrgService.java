package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestOrg;

/**
 * @author 
 *
 */
public interface EventInvestOrgService {
	
	/**
     * @api saveEventInvestOrg 新增eventInvestOrg
     * @apiGroup eventInvestOrg管理
     * @apiName  新增eventInvestOrg记录
     * @apiDescription 全量插入eventInvestOrg记录
     * @apiParam {EventInvestOrg} eventInvestOrg eventInvestOrg对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventInvestOrg(EventInvestOrg eventInvestOrg);
	
	/**
     * @api updateEventInvestOrg 修改eventInvestOrg
     * @apiGroup eventInvestOrg管理
     * @apiName  修改eventInvestOrg记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventInvestOrg} eventInvestOrg eventInvestOrg对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventInvestOrg(EventInvestOrg eventInvestOrg);
	
	/**
     * @api getEventInvestOrgById 根据eventInvestOrgid查询详情
     * @apiGroup eventInvestOrg管理
     * @apiName  查询eventInvestOrg详情
     * @apiDescription 根据主键id查询eventInvestOrg详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventInvestOrg eventInvestOrg实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventInvestOrg getEventInvestOrgById(String id);
	
	/**
     * @api getEventInvestOrgList 根据eventInvestOrg条件查询列表
     * @apiGroup eventInvestOrg管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventInvestOrg} eventInvestOrg  实体条件
     * @apiSuccess List<EventInvestOrg> eventInvestOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventInvestOrg> getEventInvestOrgList(EventInvestOrg eventInvestOrg);
	

	/**
     * @api getEventInvestOrgListByParam 根据eventInvestOrg条件查询列表（含排序）
     * @apiGroup eventInvestOrg管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventInvestOrg} eventInvestOrg  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventInvestOrg> eventInvestOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventInvestOrg> getEventInvestOrgListByParam(EventInvestOrg eventInvestOrg, String orderByStr);
	
	
	/**
     * @api findEventInvestOrgPageByParam 根据eventInvestOrg条件查询列表（分页）
     * @apiGroup eventInvestOrg管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventInvestOrg} eventInvestOrg  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventInvestOrg> eventInvestOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventInvestOrg> findEventInvestOrgPageByParam(EventInvestOrg eventInvestOrg, PageParam pageParam); 
	
	/**
     * @api findEventInvestOrgPageByParam 根据eventInvestOrg条件查询列表（分页，含排序）
     * @apiGroup eventInvestOrg管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventInvestOrg} eventInvestOrg  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventInvestOrg> eventInvestOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventInvestOrg> findEventInvestOrgPageByParam(EventInvestOrg eventInvestOrg, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventInvestOrgById 根据eventInvestOrg的id删除(物理删除)
     * @apiGroup eventInvestOrg管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventInvestOrgById(String id);

}

