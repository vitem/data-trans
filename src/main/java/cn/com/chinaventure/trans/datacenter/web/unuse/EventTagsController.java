package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventTagsService;
import cn.com.chinaventure.trans.datacenter.entity.EventTags;


@RestController
@RequestMapping("/eventTags")
public class EventTagsController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventTagsController.class);

    @Resource
    private EventTagsService eventTagsService;

    /**
	 * @api {post} / 新增eventTags
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventTags(@RequestBody EventTags eventTags) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventTagsService.saveEventTags(eventTags);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventTags
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventTags(@RequestBody EventTags eventTags) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventTagsService.updateEventTags(eventTags);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventTags详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventTags> getEventTagsById(@PathVariable String id) {
        JsonResult<EventTags> jr = new JsonResult<EventTags>();
        try {
            EventTags eventTags  = eventTagsService.getEventTagsById(id);
            jr = buildJsonResult(eventTags);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventTags/getEventTagsList 查询eventTags列表(不带分页)
	 * @apiGroup eventTags管理
	 * @apiName 查询eventTags列表(不带分页)
	 * @apiDescription 根据条件查询eventTags列表(不带分页)
	 * @apiParam {EventTags} eventTags eventTags对象
	 */
    @RequestMapping("/getEventTagsList")
    public JsonResult<List<EventTags>> getEventTagsList(EventTags eventTags) {
        JsonResult<List<EventTags>> jr = new JsonResult<List<EventTags>>();
        try {
            List<EventTags> list = this.eventTagsService.getEventTagsList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventTags/findEventTagsPageByParam 查询eventTags列表(带分页)
	 * @apiGroup eventTags管理
	 * @apiName 查询eventTags列表(带分页)
	 * @apiDescription 根据条件查询eventTags列表(带分页)
	 * @apiParam {EventTags} eventTags eventTags对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventTagsPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventTags>> findEventTagsPageByParam(@RequestBody EventTags eventTags, PageParam pageParam) {
        JsonResult<PageResult<EventTags>> jr = new JsonResult<PageResult<EventTags>>();
        try {
            PageResult<EventTags> page = this.eventTagsService.findEventTagsPageByParam(eventTags, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventTags/deleteEventTagsById 删除eventTags(物理删除)
	 * @apiGroup eventTags管理
	 * @apiName 删除eventTags记录(物理删除)
	 * @apiDescription 根据主键id删除eventTags记录(物理删除)
	 * @apiParam {String} id eventTags主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventTagsById",method=RequestMethod.DELETE)
    public JsonResult deleteEventTagsById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventTagsService.deleteEventTagsById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
