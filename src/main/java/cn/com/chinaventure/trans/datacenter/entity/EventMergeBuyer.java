package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EventMergeBuyer implements Serializable {
    private String id;

    private String eventMergeId;

    /**
     * 企业  （企业自然人二选一）
     */
    private String enterpriseId;

    /**
     * 自然人 （企业自然人二选一）
     */
    private String naturalPersonageId;

    /**
     * 交易前持有股权
     */
    private BigDecimal beforeStockRate;

    /**
     * 交易后持有股权
     */
    private BigDecimal afterStockRate;

    /**
     * 转让股票价格
     */
    private BigDecimal attornStockPrice;

    /**
     * 转让股票数量
     */
    private String attornStockAmount;

    /**
     * 支付金额
     */
    private BigDecimal payAmount;

    /**
     * 支付方式 (暂未定义)
     */
    private Byte payType;

    /**
     * 支付融资方式 (暂未定义)
     */
    private Byte payInvestType;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventMergeId() {
        return eventMergeId;
    }

    public void setEventMergeId(String eventMergeId) {
        this.eventMergeId = eventMergeId == null ? null : eventMergeId.trim();
    }

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(String enterpriseId) {
        this.enterpriseId = enterpriseId == null ? null : enterpriseId.trim();
    }

    public String getNaturalPersonageId() {
        return naturalPersonageId;
    }

    public void setNaturalPersonageId(String naturalPersonageId) {
        this.naturalPersonageId = naturalPersonageId == null ? null : naturalPersonageId.trim();
    }

    public BigDecimal getBeforeStockRate() {
        return beforeStockRate;
    }

    public void setBeforeStockRate(BigDecimal beforeStockRate) {
        this.beforeStockRate = beforeStockRate;
    }

    public BigDecimal getAfterStockRate() {
        return afterStockRate;
    }

    public void setAfterStockRate(BigDecimal afterStockRate) {
        this.afterStockRate = afterStockRate;
    }

    public BigDecimal getAttornStockPrice() {
        return attornStockPrice;
    }

    public void setAttornStockPrice(BigDecimal attornStockPrice) {
        this.attornStockPrice = attornStockPrice;
    }

    public String getAttornStockAmount() {
        return attornStockAmount;
    }

    public void setAttornStockAmount(String attornStockAmount) {
        this.attornStockAmount = attornStockAmount == null ? null : attornStockAmount.trim();
    }

    public BigDecimal getPayAmount() {
        return payAmount;
    }

    public void setPayAmount(BigDecimal payAmount) {
        this.payAmount = payAmount;
    }

    public Byte getPayType() {
        return payType;
    }

    public void setPayType(Byte payType) {
        this.payType = payType;
    }

    public Byte getPayInvestType() {
        return payInvestType;
    }

    public void setPayInvestType(Byte payInvestType) {
        this.payInvestType = payInvestType;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventMergeId=").append(eventMergeId);
        sb.append(", enterpriseId=").append(enterpriseId);
        sb.append(", naturalPersonageId=").append(naturalPersonageId);
        sb.append(", beforeStockRate=").append(beforeStockRate);
        sb.append(", afterStockRate=").append(afterStockRate);
        sb.append(", attornStockPrice=").append(attornStockPrice);
        sb.append(", attornStockAmount=").append(attornStockAmount);
        sb.append(", payAmount=").append(payAmount);
        sb.append(", payType=").append(payType);
        sb.append(", payInvestType=").append(payInvestType);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}