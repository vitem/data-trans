package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.util.Date;

public class CompanySoftwareCopyright implements Serializable {
    private String id;

    private String companyId;

    /**
     * 软件名称
     */
    private String softCopyrightName;

    /**
     * 登记号
     */
    private String enregisterNumber;

    /**
     * 版本号
     */
    private String versionNumber;

    /**
     * 分类号
     */
    private String categoryNumber;

    /**
     * 登记批准日期
     */
    private Date enregisterApprovalDate;

    /**
     * 软件简称
     */
    private String abbreviation;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getSoftCopyrightName() {
        return softCopyrightName;
    }

    public void setSoftCopyrightName(String softCopyrightName) {
        this.softCopyrightName = softCopyrightName == null ? null : softCopyrightName.trim();
    }

    public String getEnregisterNumber() {
        return enregisterNumber;
    }

    public void setEnregisterNumber(String enregisterNumber) {
        this.enregisterNumber = enregisterNumber == null ? null : enregisterNumber.trim();
    }

    public String getVersionNumber() {
        return versionNumber;
    }

    public void setVersionNumber(String versionNumber) {
        this.versionNumber = versionNumber == null ? null : versionNumber.trim();
    }

    public String getCategoryNumber() {
        return categoryNumber;
    }

    public void setCategoryNumber(String categoryNumber) {
        this.categoryNumber = categoryNumber == null ? null : categoryNumber.trim();
    }

    public Date getEnregisterApprovalDate() {
        return enregisterApprovalDate;
    }

    public void setEnregisterApprovalDate(Date enregisterApprovalDate) {
        this.enregisterApprovalDate = enregisterApprovalDate;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation == null ? null : abbreviation.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", companyId=").append(companyId);
        sb.append(", softCopyrightName=").append(softCopyrightName);
        sb.append(", enregisterNumber=").append(enregisterNumber);
        sb.append(", versionNumber=").append(versionNumber);
        sb.append(", categoryNumber=").append(categoryNumber);
        sb.append(", enregisterApprovalDate=").append(enregisterApprovalDate);
        sb.append(", abbreviation=").append(abbreviation);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}