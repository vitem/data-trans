package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.OrganizationStrategyService;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategy;


@RestController
@RequestMapping("/organizationStrategy")
public class OrganizationStrategyController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(OrganizationStrategyController.class);

    @Resource
    private OrganizationStrategyService organizationStrategyService;

    /**
	 * @api {post} / 新增organizationStrategy
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveOrganizationStrategy(@RequestBody OrganizationStrategy organizationStrategy) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.organizationStrategyService.saveOrganizationStrategy(organizationStrategy);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改organizationStrategy
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateOrganizationStrategy(@RequestBody OrganizationStrategy organizationStrategy) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.organizationStrategyService.updateOrganizationStrategy(organizationStrategy);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询organizationStrategy详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<OrganizationStrategy> getOrganizationStrategyById(@PathVariable String id) {
        JsonResult<OrganizationStrategy> jr = new JsonResult<OrganizationStrategy>();
        try {
            OrganizationStrategy organizationStrategy  = organizationStrategyService.getOrganizationStrategyById(id);
            jr = buildJsonResult(organizationStrategy);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /organizationStrategy/getOrganizationStrategyList 查询organizationStrategy列表(不带分页)
	 * @apiGroup organizationStrategy管理
	 * @apiName 查询organizationStrategy列表(不带分页)
	 * @apiDescription 根据条件查询organizationStrategy列表(不带分页)
	 * @apiParam {OrganizationStrategy} organizationStrategy organizationStrategy对象
	 */
    @RequestMapping("/getOrganizationStrategyList")
    public JsonResult<List<OrganizationStrategy>> getOrganizationStrategyList(OrganizationStrategy organizationStrategy) {
        JsonResult<List<OrganizationStrategy>> jr = new JsonResult<List<OrganizationStrategy>>();
        try {
            List<OrganizationStrategy> list = this.organizationStrategyService.getOrganizationStrategyList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /organizationStrategy/findOrganizationStrategyPageByParam 查询organizationStrategy列表(带分页)
	 * @apiGroup organizationStrategy管理
	 * @apiName 查询organizationStrategy列表(带分页)
	 * @apiDescription 根据条件查询organizationStrategy列表(带分页)
	 * @apiParam {OrganizationStrategy} organizationStrategy organizationStrategy对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findOrganizationStrategyPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<OrganizationStrategy>> findOrganizationStrategyPageByParam(@RequestBody OrganizationStrategy organizationStrategy, PageParam pageParam) {
        JsonResult<PageResult<OrganizationStrategy>> jr = new JsonResult<PageResult<OrganizationStrategy>>();
        try {
            PageResult<OrganizationStrategy> page = this.organizationStrategyService.findOrganizationStrategyPageByParam(organizationStrategy, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /organizationStrategy/deleteOrganizationStrategyById 删除organizationStrategy(物理删除)
	 * @apiGroup organizationStrategy管理
	 * @apiName 删除organizationStrategy记录(物理删除)
	 * @apiDescription 根据主键id删除organizationStrategy记录(物理删除)
	 * @apiParam {String} id organizationStrategy主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteOrganizationStrategyById",method=RequestMethod.DELETE)
    public JsonResult deleteOrganizationStrategyById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.organizationStrategyService.deleteOrganizationStrategyById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
