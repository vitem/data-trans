package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventExitService;
import cn.com.chinaventure.trans.datacenter.entity.EventExit;
import cn.com.chinaventure.trans.datacenter.entity.EventExitExample;
import cn.com.chinaventure.trans.datacenter.entity.EventExitExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventExitMapper;


@Service("eventExitService")
public class EventExitServiceImpl implements EventExitService {

	private final static Logger logger = LoggerFactory.getLogger(EventExitServiceImpl.class);

	@Resource
	private EventExitMapper eventExitMapper;


	@Override
	public void saveEventExit(EventExit eventExit) {
		if (null == eventExit) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventExitMapper.insertSelective(eventExit);
	}

	@Override
	public void updateEventExit(EventExit eventExit) {
		if (null == eventExit) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventExitMapper.updateByPrimaryKeySelective(eventExit);
	}

	@Override
	public EventExit getEventExitById(String id) {
		return eventExitMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventExit> getEventExitList(EventExit eventExit) {
		return this.getEventExitListByParam(eventExit, null);
	}

	@Override
	public List<EventExit> getEventExitListByParam(EventExit eventExit, String orderByStr) {
		EventExitExample example = new EventExitExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventExit> eventExitList = this.eventExitMapper.selectByExample(example);
		return eventExitList;
	}

	@Override
	public PageResult<EventExit> findEventExitPageByParam(EventExit eventExit, PageParam pageParam) {
		return this.findEventExitPageByParam(eventExit, pageParam, null);
	}

	@Override
	public PageResult<EventExit> findEventExitPageByParam(EventExit eventExit, PageParam pageParam, String orderByStr) {
		PageResult<EventExit> pageResult = new PageResult<EventExit>();

		EventExitExample example = new EventExitExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventExit> list = null;// eventExitMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventExitById(String id) {
		this.eventExitMapper.deleteByPrimaryKey(id);
	}

}
