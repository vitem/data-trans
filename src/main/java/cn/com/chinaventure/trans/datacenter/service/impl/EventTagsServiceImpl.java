package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventTagsService;
import cn.com.chinaventure.trans.datacenter.entity.EventTags;
import cn.com.chinaventure.trans.datacenter.entity.EventTagsExample;
import cn.com.chinaventure.trans.datacenter.entity.EventTagsExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventTagsMapper;


@Service("eventTagsService")
public class EventTagsServiceImpl implements EventTagsService {

	private final static Logger logger = LoggerFactory.getLogger(EventTagsServiceImpl.class);

	@Resource
	private EventTagsMapper eventTagsMapper;


	@Override
	public void saveEventTags(EventTags eventTags) {
		if (null == eventTags) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventTagsMapper.insertSelective(eventTags);
	}

	@Override
	public void updateEventTags(EventTags eventTags) {
		if (null == eventTags) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventTagsMapper.updateByPrimaryKeySelective(eventTags);
	}

	@Override
	public EventTags getEventTagsById(String id) {
		return eventTagsMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventTags> getEventTagsList(EventTags eventTags) {
		return this.getEventTagsListByParam(eventTags, null);
	}

	@Override
	public List<EventTags> getEventTagsListByParam(EventTags eventTags, String orderByStr) {
		EventTagsExample example = new EventTagsExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventTags> eventTagsList = this.eventTagsMapper.selectByExample(example);
		return eventTagsList;
	}

	@Override
	public PageResult<EventTags> findEventTagsPageByParam(EventTags eventTags, PageParam pageParam) {
		return this.findEventTagsPageByParam(eventTags, pageParam, null);
	}

	@Override
	public PageResult<EventTags> findEventTagsPageByParam(EventTags eventTags, PageParam pageParam, String orderByStr) {
		PageResult<EventTags> pageResult = new PageResult<EventTags>();

		EventTagsExample example = new EventTagsExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventTags> list = null;// eventTagsMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventTagsById(String id) {
		this.eventTagsMapper.deleteByPrimaryKey(id);
	}

}
