package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventInvest;
import cn.com.chinaventure.trans.datacenter.entity.EventInvestExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventInvestMapper {
    int countByExample(EventInvestExample example);

    int deleteByExample(EventInvestExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventInvest record);

    int insertSelective(EventInvest record);

    List<EventInvest> selectByExample(EventInvestExample example);

    EventInvest selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventInvest record, @Param("example") EventInvestExample example);

    int updateByExample(@Param("record") EventInvest record, @Param("example") EventInvestExample example);

    int updateByPrimaryKeySelective(EventInvest record);

    int updateByPrimaryKey(EventInvest record);
}