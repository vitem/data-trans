package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.PersonageService;
import cn.com.chinaventure.trans.datacenter.entity.Personage;


@RestController
@RequestMapping("/personage")
public class PersonageController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(PersonageController.class);

    @Resource
    private PersonageService personageService;

    /**
	 * @api {post} / 新增personage
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> savePersonage(@RequestBody Personage personage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.personageService.savePersonage(personage);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改personage
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updatePersonage(@RequestBody Personage personage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.personageService.updatePersonage(personage);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询personage详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Personage> getPersonageById(@PathVariable String id) {
        JsonResult<Personage> jr = new JsonResult<Personage>();
        try {
            Personage personage  = personageService.getPersonageById(id);
            jr = buildJsonResult(personage);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /personage/getPersonageList 查询personage列表(不带分页)
	 * @apiGroup personage管理
	 * @apiName 查询personage列表(不带分页)
	 * @apiDescription 根据条件查询personage列表(不带分页)
	 * @apiParam {Personage} personage personage对象
	 */
    @RequestMapping("/getPersonageList")
    public JsonResult<List<Personage>> getPersonageList(Personage personage) {
        JsonResult<List<Personage>> jr = new JsonResult<List<Personage>>();
        try {
            List<Personage> list = this.personageService.getPersonageList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /personage/findPersonagePageByParam 查询personage列表(带分页)
	 * @apiGroup personage管理
	 * @apiName 查询personage列表(带分页)
	 * @apiDescription 根据条件查询personage列表(带分页)
	 * @apiParam {Personage} personage personage对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findPersonagePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Personage>> findPersonagePageByParam(@RequestBody Personage personage, PageParam pageParam) {
        JsonResult<PageResult<Personage>> jr = new JsonResult<PageResult<Personage>>();
        try {
            PageResult<Personage> page = this.personageService.findPersonagePageByParam(personage, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /personage/deletePersonageById 删除personage(物理删除)
	 * @apiGroup personage管理
	 * @apiName 删除personage记录(物理删除)
	 * @apiDescription 根据主键id删除personage记录(物理删除)
	 * @apiParam {String} id personage主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deletePersonageById",method=RequestMethod.DELETE)
    public JsonResult deletePersonageById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.personageService.deletePersonageById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
