package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanySoftwareCopyrightService;
import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyright;


@RestController
@RequestMapping("/companySoftwareCopyright")
public class CompanySoftwareCopyrightController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanySoftwareCopyrightController.class);

    @Resource
    private CompanySoftwareCopyrightService companySoftwareCopyrightService;

    /**
	 * @api {post} / 新增companySoftwareCopyright
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanySoftwareCopyright(@RequestBody CompanySoftwareCopyright companySoftwareCopyright) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companySoftwareCopyrightService.saveCompanySoftwareCopyright(companySoftwareCopyright);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companySoftwareCopyright
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanySoftwareCopyright(@RequestBody CompanySoftwareCopyright companySoftwareCopyright) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companySoftwareCopyrightService.updateCompanySoftwareCopyright(companySoftwareCopyright);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companySoftwareCopyright详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanySoftwareCopyright> getCompanySoftwareCopyrightById(@PathVariable String id) {
        JsonResult<CompanySoftwareCopyright> jr = new JsonResult<CompanySoftwareCopyright>();
        try {
            CompanySoftwareCopyright companySoftwareCopyright  = companySoftwareCopyrightService.getCompanySoftwareCopyrightById(id);
            jr = buildJsonResult(companySoftwareCopyright);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companySoftwareCopyright/getCompanySoftwareCopyrightList 查询companySoftwareCopyright列表(不带分页)
	 * @apiGroup companySoftwareCopyright管理
	 * @apiName 查询companySoftwareCopyright列表(不带分页)
	 * @apiDescription 根据条件查询companySoftwareCopyright列表(不带分页)
	 * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright companySoftwareCopyright对象
	 */
    @RequestMapping("/getCompanySoftwareCopyrightList")
    public JsonResult<List<CompanySoftwareCopyright>> getCompanySoftwareCopyrightList(CompanySoftwareCopyright companySoftwareCopyright) {
        JsonResult<List<CompanySoftwareCopyright>> jr = new JsonResult<List<CompanySoftwareCopyright>>();
        try {
            List<CompanySoftwareCopyright> list = this.companySoftwareCopyrightService.getCompanySoftwareCopyrightList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companySoftwareCopyright/findCompanySoftwareCopyrightPageByParam 查询companySoftwareCopyright列表(带分页)
	 * @apiGroup companySoftwareCopyright管理
	 * @apiName 查询companySoftwareCopyright列表(带分页)
	 * @apiDescription 根据条件查询companySoftwareCopyright列表(带分页)
	 * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright companySoftwareCopyright对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanySoftwareCopyrightPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanySoftwareCopyright>> findCompanySoftwareCopyrightPageByParam(@RequestBody CompanySoftwareCopyright companySoftwareCopyright, PageParam pageParam) {
        JsonResult<PageResult<CompanySoftwareCopyright>> jr = new JsonResult<PageResult<CompanySoftwareCopyright>>();
        try {
            PageResult<CompanySoftwareCopyright> page = this.companySoftwareCopyrightService.findCompanySoftwareCopyrightPageByParam(companySoftwareCopyright, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companySoftwareCopyright/deleteCompanySoftwareCopyrightById 删除companySoftwareCopyright(物理删除)
	 * @apiGroup companySoftwareCopyright管理
	 * @apiName 删除companySoftwareCopyright记录(物理删除)
	 * @apiDescription 根据主键id删除companySoftwareCopyright记录(物理删除)
	 * @apiParam {String} id companySoftwareCopyright主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanySoftwareCopyrightById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanySoftwareCopyrightById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companySoftwareCopyrightService.deleteCompanySoftwareCopyrightById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
