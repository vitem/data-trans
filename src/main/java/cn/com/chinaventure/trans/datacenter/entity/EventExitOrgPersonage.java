package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EventExitOrgPersonage implements Serializable {
    private String id;

    private String eventExitId;

    /**
     * 退出方机构
     */
    private String orgId;

    /**
     * 退出方基金
     */
    private String fundId;

    /**
     * 退出方基金人物
     */
    private String fundPersonageId;

    /**
     * 自然人
     */
    private String personageId;

    /**
     * 退出时持有股权
     */
    private BigDecimal holdingEquity;

    /**
     * 退出时股权价值
     */
    private BigDecimal holdingValue;

    /**
     * 退出时股权价值金额类型 货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte holdingCurrencyType;

    /**
     * 实际退出金额
     */
    private BigDecimal effectiveMoney;

    /**
     * 货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte effectiveCurrencyType;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private BigDecimal computeIncome;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventExitId() {
        return eventExitId;
    }

    public void setEventExitId(String eventExitId) {
        this.eventExitId = eventExitId == null ? null : eventExitId.trim();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId == null ? null : fundId.trim();
    }

    public String getFundPersonageId() {
        return fundPersonageId;
    }

    public void setFundPersonageId(String fundPersonageId) {
        this.fundPersonageId = fundPersonageId == null ? null : fundPersonageId.trim();
    }

    public String getPersonageId() {
        return personageId;
    }

    public void setPersonageId(String personageId) {
        this.personageId = personageId == null ? null : personageId.trim();
    }

    public BigDecimal getHoldingEquity() {
        return holdingEquity;
    }

    public void setHoldingEquity(BigDecimal holdingEquity) {
        this.holdingEquity = holdingEquity;
    }

    public BigDecimal getHoldingValue() {
        return holdingValue;
    }

    public void setHoldingValue(BigDecimal holdingValue) {
        this.holdingValue = holdingValue;
    }

    public Byte getHoldingCurrencyType() {
        return holdingCurrencyType;
    }

    public void setHoldingCurrencyType(Byte holdingCurrencyType) {
        this.holdingCurrencyType = holdingCurrencyType;
    }

    public BigDecimal getEffectiveMoney() {
        return effectiveMoney;
    }

    public void setEffectiveMoney(BigDecimal effectiveMoney) {
        this.effectiveMoney = effectiveMoney;
    }

    public Byte getEffectiveCurrencyType() {
        return effectiveCurrencyType;
    }

    public void setEffectiveCurrencyType(Byte effectiveCurrencyType) {
        this.effectiveCurrencyType = effectiveCurrencyType;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public BigDecimal getComputeIncome() {
        return computeIncome;
    }

    public void setComputeIncome(BigDecimal computeIncome) {
        this.computeIncome = computeIncome;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventExitId=").append(eventExitId);
        sb.append(", orgId=").append(orgId);
        sb.append(", fundId=").append(fundId);
        sb.append(", fundPersonageId=").append(fundPersonageId);
        sb.append(", personageId=").append(personageId);
        sb.append(", holdingEquity=").append(holdingEquity);
        sb.append(", holdingValue=").append(holdingValue);
        sb.append(", holdingCurrencyType=").append(holdingCurrencyType);
        sb.append(", effectiveMoney=").append(effectiveMoney);
        sb.append(", effectiveCurrencyType=").append(effectiveCurrencyType);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", computeIncome=").append(computeIncome);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}