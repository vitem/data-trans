package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventIpoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EventIpoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNull() {
            addCriterion("event_id is null");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNotNull() {
            addCriterion("event_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventIdEqualTo(String value) {
            addCriterion("event_id =", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotEqualTo(String value) {
            addCriterion("event_id <>", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThan(String value) {
            addCriterion("event_id >", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_id >=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThan(String value) {
            addCriterion("event_id <", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThanOrEqualTo(String value) {
            addCriterion("event_id <=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLike(String value) {
            addCriterion("event_id like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotLike(String value) {
            addCriterion("event_id not like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdIn(List<String> values) {
            addCriterion("event_id in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotIn(List<String> values) {
            addCriterion("event_id not in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdBetween(String value1, String value2) {
            addCriterion("event_id between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotBetween(String value1, String value2) {
            addCriterion("event_id not between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNull() {
            addCriterion("stock_code is null");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNotNull() {
            addCriterion("stock_code is not null");
            return (Criteria) this;
        }

        public Criteria andStockCodeEqualTo(String value) {
            addCriterion("stock_code =", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotEqualTo(String value) {
            addCriterion("stock_code <>", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThan(String value) {
            addCriterion("stock_code >", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThanOrEqualTo(String value) {
            addCriterion("stock_code >=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThan(String value) {
            addCriterion("stock_code <", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThanOrEqualTo(String value) {
            addCriterion("stock_code <=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLike(String value) {
            addCriterion("stock_code like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotLike(String value) {
            addCriterion("stock_code not like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIn(List<String> values) {
            addCriterion("stock_code in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotIn(List<String> values) {
            addCriterion("stock_code not in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeBetween(String value1, String value2) {
            addCriterion("stock_code between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotBetween(String value1, String value2) {
            addCriterion("stock_code not between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andApplyTimeIsNull() {
            addCriterion("apply_time is null");
            return (Criteria) this;
        }

        public Criteria andApplyTimeIsNotNull() {
            addCriterion("apply_time is not null");
            return (Criteria) this;
        }

        public Criteria andApplyTimeEqualTo(Date value) {
            addCriterion("apply_time =", value, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeNotEqualTo(Date value) {
            addCriterion("apply_time <>", value, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeGreaterThan(Date value) {
            addCriterion("apply_time >", value, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("apply_time >=", value, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeLessThan(Date value) {
            addCriterion("apply_time <", value, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeLessThanOrEqualTo(Date value) {
            addCriterion("apply_time <=", value, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeIn(List<Date> values) {
            addCriterion("apply_time in", values, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeNotIn(List<Date> values) {
            addCriterion("apply_time not in", values, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeBetween(Date value1, Date value2) {
            addCriterion("apply_time between", value1, value2, "applyTime");
            return (Criteria) this;
        }

        public Criteria andApplyTimeNotBetween(Date value1, Date value2) {
            addCriterion("apply_time not between", value1, value2, "applyTime");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIsNull() {
            addCriterion("exchange_id is null");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIsNotNull() {
            addCriterion("exchange_id is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeIdEqualTo(String value) {
            addCriterion("exchange_id =", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotEqualTo(String value) {
            addCriterion("exchange_id <>", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdGreaterThan(String value) {
            addCriterion("exchange_id >", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_id >=", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLessThan(String value) {
            addCriterion("exchange_id <", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLessThanOrEqualTo(String value) {
            addCriterion("exchange_id <=", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLike(String value) {
            addCriterion("exchange_id like", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotLike(String value) {
            addCriterion("exchange_id not like", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIn(List<String> values) {
            addCriterion("exchange_id in", values, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotIn(List<String> values) {
            addCriterion("exchange_id not in", values, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdBetween(String value1, String value2) {
            addCriterion("exchange_id between", value1, value2, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotBetween(String value1, String value2) {
            addCriterion("exchange_id not between", value1, value2, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andIpoTimeIsNull() {
            addCriterion("ipo_time is null");
            return (Criteria) this;
        }

        public Criteria andIpoTimeIsNotNull() {
            addCriterion("ipo_time is not null");
            return (Criteria) this;
        }

        public Criteria andIpoTimeEqualTo(Date value) {
            addCriterion("ipo_time =", value, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeNotEqualTo(Date value) {
            addCriterion("ipo_time <>", value, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeGreaterThan(Date value) {
            addCriterion("ipo_time >", value, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("ipo_time >=", value, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeLessThan(Date value) {
            addCriterion("ipo_time <", value, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeLessThanOrEqualTo(Date value) {
            addCriterion("ipo_time <=", value, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeIn(List<Date> values) {
            addCriterion("ipo_time in", values, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeNotIn(List<Date> values) {
            addCriterion("ipo_time not in", values, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeBetween(Date value1, Date value2) {
            addCriterion("ipo_time between", value1, value2, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoTimeNotBetween(Date value1, Date value2) {
            addCriterion("ipo_time not between", value1, value2, "ipoTime");
            return (Criteria) this;
        }

        public Criteria andIpoStatusIsNull() {
            addCriterion("ipo_status is null");
            return (Criteria) this;
        }

        public Criteria andIpoStatusIsNotNull() {
            addCriterion("ipo_status is not null");
            return (Criteria) this;
        }

        public Criteria andIpoStatusEqualTo(Byte value) {
            addCriterion("ipo_status =", value, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusNotEqualTo(Byte value) {
            addCriterion("ipo_status <>", value, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusGreaterThan(Byte value) {
            addCriterion("ipo_status >", value, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("ipo_status >=", value, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusLessThan(Byte value) {
            addCriterion("ipo_status <", value, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusLessThanOrEqualTo(Byte value) {
            addCriterion("ipo_status <=", value, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusIn(List<Byte> values) {
            addCriterion("ipo_status in", values, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusNotIn(List<Byte> values) {
            addCriterion("ipo_status not in", values, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusBetween(Byte value1, Byte value2) {
            addCriterion("ipo_status between", value1, value2, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("ipo_status not between", value1, value2, "ipoStatus");
            return (Criteria) this;
        }

        public Criteria andIpoTypeIsNull() {
            addCriterion("ipo_type is null");
            return (Criteria) this;
        }

        public Criteria andIpoTypeIsNotNull() {
            addCriterion("ipo_type is not null");
            return (Criteria) this;
        }

        public Criteria andIpoTypeEqualTo(Byte value) {
            addCriterion("ipo_type =", value, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeNotEqualTo(Byte value) {
            addCriterion("ipo_type <>", value, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeGreaterThan(Byte value) {
            addCriterion("ipo_type >", value, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("ipo_type >=", value, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeLessThan(Byte value) {
            addCriterion("ipo_type <", value, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeLessThanOrEqualTo(Byte value) {
            addCriterion("ipo_type <=", value, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeIn(List<Byte> values) {
            addCriterion("ipo_type in", values, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeNotIn(List<Byte> values) {
            addCriterion("ipo_type not in", values, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeBetween(Byte value1, Byte value2) {
            addCriterion("ipo_type between", value1, value2, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIpoTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("ipo_type not between", value1, value2, "ipoType");
            return (Criteria) this;
        }

        public Criteria andIssuePriceIsNull() {
            addCriterion("issue_price is null");
            return (Criteria) this;
        }

        public Criteria andIssuePriceIsNotNull() {
            addCriterion("issue_price is not null");
            return (Criteria) this;
        }

        public Criteria andIssuePriceEqualTo(BigDecimal value) {
            addCriterion("issue_price =", value, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceNotEqualTo(BigDecimal value) {
            addCriterion("issue_price <>", value, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceGreaterThan(BigDecimal value) {
            addCriterion("issue_price >", value, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("issue_price >=", value, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceLessThan(BigDecimal value) {
            addCriterion("issue_price <", value, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceLessThanOrEqualTo(BigDecimal value) {
            addCriterion("issue_price <=", value, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceIn(List<BigDecimal> values) {
            addCriterion("issue_price in", values, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceNotIn(List<BigDecimal> values) {
            addCriterion("issue_price not in", values, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("issue_price between", value1, value2, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andIssuePriceNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("issue_price not between", value1, value2, "issuePrice");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeIsNull() {
            addCriterion("vcpe_type is null");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeIsNotNull() {
            addCriterion("vcpe_type is not null");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeEqualTo(Byte value) {
            addCriterion("vcpe_type =", value, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeNotEqualTo(Byte value) {
            addCriterion("vcpe_type <>", value, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeGreaterThan(Byte value) {
            addCriterion("vcpe_type >", value, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("vcpe_type >=", value, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeLessThan(Byte value) {
            addCriterion("vcpe_type <", value, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeLessThanOrEqualTo(Byte value) {
            addCriterion("vcpe_type <=", value, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeIn(List<Byte> values) {
            addCriterion("vcpe_type in", values, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeNotIn(List<Byte> values) {
            addCriterion("vcpe_type not in", values, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeBetween(Byte value1, Byte value2) {
            addCriterion("vcpe_type between", value1, value2, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andVcpeTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("vcpe_type not between", value1, value2, "vcpeType");
            return (Criteria) this;
        }

        public Criteria andStockNoIsNull() {
            addCriterion("stock_no is null");
            return (Criteria) this;
        }

        public Criteria andStockNoIsNotNull() {
            addCriterion("stock_no is not null");
            return (Criteria) this;
        }

        public Criteria andStockNoEqualTo(Integer value) {
            addCriterion("stock_no =", value, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoNotEqualTo(Integer value) {
            addCriterion("stock_no <>", value, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoGreaterThan(Integer value) {
            addCriterion("stock_no >", value, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoGreaterThanOrEqualTo(Integer value) {
            addCriterion("stock_no >=", value, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoLessThan(Integer value) {
            addCriterion("stock_no <", value, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoLessThanOrEqualTo(Integer value) {
            addCriterion("stock_no <=", value, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoIn(List<Integer> values) {
            addCriterion("stock_no in", values, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoNotIn(List<Integer> values) {
            addCriterion("stock_no not in", values, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoBetween(Integer value1, Integer value2) {
            addCriterion("stock_no between", value1, value2, "stockNo");
            return (Criteria) this;
        }

        public Criteria andStockNoNotBetween(Integer value1, Integer value2) {
            addCriterion("stock_no not between", value1, value2, "stockNo");
            return (Criteria) this;
        }

        public Criteria andSumMoneyIsNull() {
            addCriterion("sum_money is null");
            return (Criteria) this;
        }

        public Criteria andSumMoneyIsNotNull() {
            addCriterion("sum_money is not null");
            return (Criteria) this;
        }

        public Criteria andSumMoneyEqualTo(BigDecimal value) {
            addCriterion("sum_money =", value, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyNotEqualTo(BigDecimal value) {
            addCriterion("sum_money <>", value, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyGreaterThan(BigDecimal value) {
            addCriterion("sum_money >", value, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("sum_money >=", value, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyLessThan(BigDecimal value) {
            addCriterion("sum_money <", value, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("sum_money <=", value, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyIn(List<BigDecimal> values) {
            addCriterion("sum_money in", values, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyNotIn(List<BigDecimal> values) {
            addCriterion("sum_money not in", values, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sum_money between", value1, value2, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("sum_money not between", value1, value2, "sumMoney");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeIsNull() {
            addCriterion("sum_money_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeIsNotNull() {
            addCriterion("sum_money_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeEqualTo(Byte value) {
            addCriterion("sum_money_currency_type =", value, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("sum_money_currency_type <>", value, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeGreaterThan(Byte value) {
            addCriterion("sum_money_currency_type >", value, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("sum_money_currency_type >=", value, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeLessThan(Byte value) {
            addCriterion("sum_money_currency_type <", value, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("sum_money_currency_type <=", value, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeIn(List<Byte> values) {
            addCriterion("sum_money_currency_type in", values, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("sum_money_currency_type not in", values, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("sum_money_currency_type between", value1, value2, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andSumMoneyCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("sum_money_currency_type not between", value1, value2, "sumMoneyCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthIsNull() {
            addCriterion("net_worth is null");
            return (Criteria) this;
        }

        public Criteria andNetWorthIsNotNull() {
            addCriterion("net_worth is not null");
            return (Criteria) this;
        }

        public Criteria andNetWorthEqualTo(BigDecimal value) {
            addCriterion("net_worth =", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthNotEqualTo(BigDecimal value) {
            addCriterion("net_worth <>", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthGreaterThan(BigDecimal value) {
            addCriterion("net_worth >", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("net_worth >=", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthLessThan(BigDecimal value) {
            addCriterion("net_worth <", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("net_worth <=", value, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthIn(List<BigDecimal> values) {
            addCriterion("net_worth in", values, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthNotIn(List<BigDecimal> values) {
            addCriterion("net_worth not in", values, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("net_worth between", value1, value2, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("net_worth not between", value1, value2, "netWorth");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeIsNull() {
            addCriterion("net_worth_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeIsNotNull() {
            addCriterion("net_worth_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeEqualTo(Byte value) {
            addCriterion("net_worth_currency_type =", value, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("net_worth_currency_type <>", value, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeGreaterThan(Byte value) {
            addCriterion("net_worth_currency_type >", value, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("net_worth_currency_type >=", value, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeLessThan(Byte value) {
            addCriterion("net_worth_currency_type <", value, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("net_worth_currency_type <=", value, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeIn(List<Byte> values) {
            addCriterion("net_worth_currency_type in", values, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("net_worth_currency_type not in", values, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("net_worth_currency_type between", value1, value2, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andNetWorthCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("net_worth_currency_type not between", value1, value2, "netWorthCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdIsNull() {
            addCriterion("recommend_firm_id is null");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdIsNotNull() {
            addCriterion("recommend_firm_id is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdEqualTo(String value) {
            addCriterion("recommend_firm_id =", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdNotEqualTo(String value) {
            addCriterion("recommend_firm_id <>", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdGreaterThan(String value) {
            addCriterion("recommend_firm_id >", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdGreaterThanOrEqualTo(String value) {
            addCriterion("recommend_firm_id >=", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdLessThan(String value) {
            addCriterion("recommend_firm_id <", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdLessThanOrEqualTo(String value) {
            addCriterion("recommend_firm_id <=", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdLike(String value) {
            addCriterion("recommend_firm_id like", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdNotLike(String value) {
            addCriterion("recommend_firm_id not like", value, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdIn(List<String> values) {
            addCriterion("recommend_firm_id in", values, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdNotIn(List<String> values) {
            addCriterion("recommend_firm_id not in", values, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdBetween(String value1, String value2) {
            addCriterion("recommend_firm_id between", value1, value2, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andRecommendFirmIdNotBetween(String value1, String value2) {
            addCriterion("recommend_firm_id not between", value1, value2, "recommendFirmId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIsNull() {
            addCriterion("law_org_id is null");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIsNotNull() {
            addCriterion("law_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdEqualTo(String value) {
            addCriterion("law_org_id =", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotEqualTo(String value) {
            addCriterion("law_org_id <>", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdGreaterThan(String value) {
            addCriterion("law_org_id >", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("law_org_id >=", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLessThan(String value) {
            addCriterion("law_org_id <", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLessThanOrEqualTo(String value) {
            addCriterion("law_org_id <=", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLike(String value) {
            addCriterion("law_org_id like", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotLike(String value) {
            addCriterion("law_org_id not like", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIn(List<String> values) {
            addCriterion("law_org_id in", values, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotIn(List<String> values) {
            addCriterion("law_org_id not in", values, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdBetween(String value1, String value2) {
            addCriterion("law_org_id between", value1, value2, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotBetween(String value1, String value2) {
            addCriterion("law_org_id not between", value1, value2, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIsNull() {
            addCriterion("law_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIsNotNull() {
            addCriterion("law_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdEqualTo(String value) {
            addCriterion("law_personage_id =", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotEqualTo(String value) {
            addCriterion("law_personage_id <>", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdGreaterThan(String value) {
            addCriterion("law_personage_id >", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("law_personage_id >=", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLessThan(String value) {
            addCriterion("law_personage_id <", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("law_personage_id <=", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLike(String value) {
            addCriterion("law_personage_id like", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotLike(String value) {
            addCriterion("law_personage_id not like", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIn(List<String> values) {
            addCriterion("law_personage_id in", values, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotIn(List<String> values) {
            addCriterion("law_personage_id not in", values, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdBetween(String value1, String value2) {
            addCriterion("law_personage_id between", value1, value2, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotBetween(String value1, String value2) {
            addCriterion("law_personage_id not between", value1, value2, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIsNull() {
            addCriterion("accounting_org_id is null");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIsNotNull() {
            addCriterion("accounting_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdEqualTo(String value) {
            addCriterion("accounting_org_id =", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotEqualTo(String value) {
            addCriterion("accounting_org_id <>", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdGreaterThan(String value) {
            addCriterion("accounting_org_id >", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_org_id >=", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLessThan(String value) {
            addCriterion("accounting_org_id <", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLessThanOrEqualTo(String value) {
            addCriterion("accounting_org_id <=", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLike(String value) {
            addCriterion("accounting_org_id like", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotLike(String value) {
            addCriterion("accounting_org_id not like", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIn(List<String> values) {
            addCriterion("accounting_org_id in", values, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotIn(List<String> values) {
            addCriterion("accounting_org_id not in", values, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdBetween(String value1, String value2) {
            addCriterion("accounting_org_id between", value1, value2, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotBetween(String value1, String value2) {
            addCriterion("accounting_org_id not between", value1, value2, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIsNull() {
            addCriterion("accounting_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIsNotNull() {
            addCriterion("accounting_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdEqualTo(String value) {
            addCriterion("accounting_personage_id =", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotEqualTo(String value) {
            addCriterion("accounting_personage_id <>", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdGreaterThan(String value) {
            addCriterion("accounting_personage_id >", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_personage_id >=", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLessThan(String value) {
            addCriterion("accounting_personage_id <", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("accounting_personage_id <=", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLike(String value) {
            addCriterion("accounting_personage_id like", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotLike(String value) {
            addCriterion("accounting_personage_id not like", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIn(List<String> values) {
            addCriterion("accounting_personage_id in", values, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotIn(List<String> values) {
            addCriterion("accounting_personage_id not in", values, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdBetween(String value1, String value2) {
            addCriterion("accounting_personage_id between", value1, value2, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotBetween(String value1, String value2) {
            addCriterion("accounting_personage_id not between", value1, value2, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalIsNull() {
            addCriterion("issue_cost_total is null");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalIsNotNull() {
            addCriterion("issue_cost_total is not null");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalEqualTo(BigDecimal value) {
            addCriterion("issue_cost_total =", value, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalNotEqualTo(BigDecimal value) {
            addCriterion("issue_cost_total <>", value, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalGreaterThan(BigDecimal value) {
            addCriterion("issue_cost_total >", value, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("issue_cost_total >=", value, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalLessThan(BigDecimal value) {
            addCriterion("issue_cost_total <", value, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalLessThanOrEqualTo(BigDecimal value) {
            addCriterion("issue_cost_total <=", value, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalIn(List<BigDecimal> values) {
            addCriterion("issue_cost_total in", values, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalNotIn(List<BigDecimal> values) {
            addCriterion("issue_cost_total not in", values, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("issue_cost_total between", value1, value2, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostTotalNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("issue_cost_total not between", value1, value2, "issueCostTotal");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeIsNull() {
            addCriterion("issue_cost_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeIsNotNull() {
            addCriterion("issue_cost_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeEqualTo(Byte value) {
            addCriterion("issue_cost_currency_type =", value, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("issue_cost_currency_type <>", value, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeGreaterThan(Byte value) {
            addCriterion("issue_cost_currency_type >", value, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("issue_cost_currency_type >=", value, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeLessThan(Byte value) {
            addCriterion("issue_cost_currency_type <", value, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("issue_cost_currency_type <=", value, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeIn(List<Byte> values) {
            addCriterion("issue_cost_currency_type in", values, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("issue_cost_currency_type not in", values, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("issue_cost_currency_type between", value1, value2, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andIssueCostCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("issue_cost_currency_type not between", value1, value2, "issueCostCurrencyType");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostIsNull() {
            addCriterion("recommend_shipment_cost is null");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostIsNotNull() {
            addCriterion("recommend_shipment_cost is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostEqualTo(BigDecimal value) {
            addCriterion("recommend_shipment_cost =", value, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostNotEqualTo(BigDecimal value) {
            addCriterion("recommend_shipment_cost <>", value, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostGreaterThan(BigDecimal value) {
            addCriterion("recommend_shipment_cost >", value, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("recommend_shipment_cost >=", value, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostLessThan(BigDecimal value) {
            addCriterion("recommend_shipment_cost <", value, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("recommend_shipment_cost <=", value, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostIn(List<BigDecimal> values) {
            addCriterion("recommend_shipment_cost in", values, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostNotIn(List<BigDecimal> values) {
            addCriterion("recommend_shipment_cost not in", values, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("recommend_shipment_cost between", value1, value2, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendShipmentCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("recommend_shipment_cost not between", value1, value2, "recommendShipmentCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostIsNull() {
            addCriterion("recommend_cost is null");
            return (Criteria) this;
        }

        public Criteria andRecommendCostIsNotNull() {
            addCriterion("recommend_cost is not null");
            return (Criteria) this;
        }

        public Criteria andRecommendCostEqualTo(BigDecimal value) {
            addCriterion("recommend_cost =", value, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostNotEqualTo(BigDecimal value) {
            addCriterion("recommend_cost <>", value, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostGreaterThan(BigDecimal value) {
            addCriterion("recommend_cost >", value, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("recommend_cost >=", value, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostLessThan(BigDecimal value) {
            addCriterion("recommend_cost <", value, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("recommend_cost <=", value, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostIn(List<BigDecimal> values) {
            addCriterion("recommend_cost in", values, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostNotIn(List<BigDecimal> values) {
            addCriterion("recommend_cost not in", values, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("recommend_cost between", value1, value2, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andRecommendCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("recommend_cost not between", value1, value2, "recommendCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostIsNull() {
            addCriterion("shipment_inward_cost is null");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostIsNotNull() {
            addCriterion("shipment_inward_cost is not null");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostEqualTo(BigDecimal value) {
            addCriterion("shipment_inward_cost =", value, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostNotEqualTo(BigDecimal value) {
            addCriterion("shipment_inward_cost <>", value, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostGreaterThan(BigDecimal value) {
            addCriterion("shipment_inward_cost >", value, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("shipment_inward_cost >=", value, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostLessThan(BigDecimal value) {
            addCriterion("shipment_inward_cost <", value, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("shipment_inward_cost <=", value, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostIn(List<BigDecimal> values) {
            addCriterion("shipment_inward_cost in", values, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostNotIn(List<BigDecimal> values) {
            addCriterion("shipment_inward_cost not in", values, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("shipment_inward_cost between", value1, value2, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andShipmentInwardCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("shipment_inward_cost not between", value1, value2, "shipmentInwardCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostIsNull() {
            addCriterion("lawyer_cost is null");
            return (Criteria) this;
        }

        public Criteria andLawyerCostIsNotNull() {
            addCriterion("lawyer_cost is not null");
            return (Criteria) this;
        }

        public Criteria andLawyerCostEqualTo(BigDecimal value) {
            addCriterion("lawyer_cost =", value, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostNotEqualTo(BigDecimal value) {
            addCriterion("lawyer_cost <>", value, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostGreaterThan(BigDecimal value) {
            addCriterion("lawyer_cost >", value, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("lawyer_cost >=", value, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostLessThan(BigDecimal value) {
            addCriterion("lawyer_cost <", value, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("lawyer_cost <=", value, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostIn(List<BigDecimal> values) {
            addCriterion("lawyer_cost in", values, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostNotIn(List<BigDecimal> values) {
            addCriterion("lawyer_cost not in", values, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("lawyer_cost between", value1, value2, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andLawyerCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("lawyer_cost not between", value1, value2, "lawyerCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostIsNull() {
            addCriterion("audit_cost is null");
            return (Criteria) this;
        }

        public Criteria andAuditCostIsNotNull() {
            addCriterion("audit_cost is not null");
            return (Criteria) this;
        }

        public Criteria andAuditCostEqualTo(BigDecimal value) {
            addCriterion("audit_cost =", value, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostNotEqualTo(BigDecimal value) {
            addCriterion("audit_cost <>", value, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostGreaterThan(BigDecimal value) {
            addCriterion("audit_cost >", value, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("audit_cost >=", value, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostLessThan(BigDecimal value) {
            addCriterion("audit_cost <", value, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostLessThanOrEqualTo(BigDecimal value) {
            addCriterion("audit_cost <=", value, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostIn(List<BigDecimal> values) {
            addCriterion("audit_cost in", values, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostNotIn(List<BigDecimal> values) {
            addCriterion("audit_cost not in", values, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("audit_cost between", value1, value2, "auditCost");
            return (Criteria) this;
        }

        public Criteria andAuditCostNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("audit_cost not between", value1, value2, "auditCost");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNull() {
            addCriterion("remark is null");
            return (Criteria) this;
        }

        public Criteria andRemarkIsNotNull() {
            addCriterion("remark is not null");
            return (Criteria) this;
        }

        public Criteria andRemarkEqualTo(String value) {
            addCriterion("remark =", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotEqualTo(String value) {
            addCriterion("remark <>", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThan(String value) {
            addCriterion("remark >", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("remark >=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThan(String value) {
            addCriterion("remark <", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLessThanOrEqualTo(String value) {
            addCriterion("remark <=", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkLike(String value) {
            addCriterion("remark like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotLike(String value) {
            addCriterion("remark not like", value, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkIn(List<String> values) {
            addCriterion("remark in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotIn(List<String> values) {
            addCriterion("remark not in", values, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkBetween(String value1, String value2) {
            addCriterion("remark between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andRemarkNotBetween(String value1, String value2) {
            addCriterion("remark not between", value1, value2, "remark");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}