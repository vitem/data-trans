package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademark;
import cn.com.chinaventure.trans.datacenter.entity.CompanyTrademarkExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyTrademarkMapper {
    int countByExample(CompanyTrademarkExample example);

    int deleteByExample(CompanyTrademarkExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyTrademark record);

    int insertSelective(CompanyTrademark record);

    List<CompanyTrademark> selectByExample(CompanyTrademarkExample example);

    CompanyTrademark selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyTrademark record, @Param("example") CompanyTrademarkExample example);

    int updateByExample(@Param("record") CompanyTrademark record, @Param("example") CompanyTrademarkExample example);

    int updateByPrimaryKeySelective(CompanyTrademark record);

    int updateByPrimaryKey(CompanyTrademark record);
}