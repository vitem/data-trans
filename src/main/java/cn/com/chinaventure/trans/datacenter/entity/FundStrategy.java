package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FundStrategy implements Serializable {
    private Long id;

    private Long fundId;

    /**
     * 投资时间
     */
    private Date strategyTime;

    /**
     * 投资上限
     */
    private BigDecimal uppMoney;

    /**
     * 投资下限
     */
    private BigDecimal lowMoney;

    /**
     * currency_type 投资上限货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte currencyType;

    /**
     * 投资中国的比例
     */
    private BigDecimal vcScale;

    /**
     * 投资描述
     */
    private String strategyCnDesc;

    /**
     * 投资英文描述
     */
    private String strategyEnDesc;

    /**
     * 投资备注
     */
    private String strategyRemark;

    /**
     * 地域  1，全求 2，亚州 3，大中华 4，中国
     */
    private Byte regionType;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFundId() {
        return fundId;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }

    public Date getStrategyTime() {
        return strategyTime;
    }

    public void setStrategyTime(Date strategyTime) {
        this.strategyTime = strategyTime;
    }

    public BigDecimal getUppMoney() {
        return uppMoney;
    }

    public void setUppMoney(BigDecimal uppMoney) {
        this.uppMoney = uppMoney;
    }

    public BigDecimal getLowMoney() {
        return lowMoney;
    }

    public void setLowMoney(BigDecimal lowMoney) {
        this.lowMoney = lowMoney;
    }

    public Byte getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(Byte currencyType) {
        this.currencyType = currencyType;
    }

    public BigDecimal getVcScale() {
        return vcScale;
    }

    public void setVcScale(BigDecimal vcScale) {
        this.vcScale = vcScale;
    }

    public String getStrategyCnDesc() {
        return strategyCnDesc;
    }

    public void setStrategyCnDesc(String strategyCnDesc) {
        this.strategyCnDesc = strategyCnDesc == null ? null : strategyCnDesc.trim();
    }

    public String getStrategyEnDesc() {
        return strategyEnDesc;
    }

    public void setStrategyEnDesc(String strategyEnDesc) {
        this.strategyEnDesc = strategyEnDesc == null ? null : strategyEnDesc.trim();
    }

    public String getStrategyRemark() {
        return strategyRemark;
    }

    public void setStrategyRemark(String strategyRemark) {
        this.strategyRemark = strategyRemark == null ? null : strategyRemark.trim();
    }

    public Byte getRegionType() {
        return regionType;
    }

    public void setRegionType(Byte regionType) {
        this.regionType = regionType;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", fundId=").append(fundId);
        sb.append(", strategyTime=").append(strategyTime);
        sb.append(", uppMoney=").append(uppMoney);
        sb.append(", lowMoney=").append(lowMoney);
        sb.append(", currencyType=").append(currencyType);
        sb.append(", vcScale=").append(vcScale);
        sb.append(", strategyCnDesc=").append(strategyCnDesc);
        sb.append(", strategyEnDesc=").append(strategyEnDesc);
        sb.append(", strategyRemark=").append(strategyRemark);
        sb.append(", regionType=").append(regionType);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}