package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.ManagerService;
import cn.com.chinaventure.trans.datacenter.entity.Manager;


@RestController
@RequestMapping("/manager")
public class ManagerController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(ManagerController.class);

    @Resource
    private ManagerService managerService;

    /**
	 * @api {post} / 新增manager
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveManager(@RequestBody Manager manager) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.managerService.saveManager(manager);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改manager
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateManager(@RequestBody Manager manager) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.managerService.updateManager(manager);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询manager详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Manager> getManagerById(@PathVariable String id) {
        JsonResult<Manager> jr = new JsonResult<Manager>();
        try {
            Manager manager  = managerService.getManagerById(id);
            jr = buildJsonResult(manager);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /manager/getManagerList 查询manager列表(不带分页)
	 * @apiGroup manager管理
	 * @apiName 查询manager列表(不带分页)
	 * @apiDescription 根据条件查询manager列表(不带分页)
	 * @apiParam {Manager} manager manager对象
	 */
    @RequestMapping("/getManagerList")
    public JsonResult<List<Manager>> getManagerList(Manager manager) {
        JsonResult<List<Manager>> jr = new JsonResult<List<Manager>>();
        try {
            List<Manager> list = this.managerService.getManagerList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /manager/findManagerPageByParam 查询manager列表(带分页)
	 * @apiGroup manager管理
	 * @apiName 查询manager列表(带分页)
	 * @apiDescription 根据条件查询manager列表(带分页)
	 * @apiParam {Manager} manager manager对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findManagerPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Manager>> findManagerPageByParam(@RequestBody Manager manager, PageParam pageParam) {
        JsonResult<PageResult<Manager>> jr = new JsonResult<PageResult<Manager>>();
        try {
            PageResult<Manager> page = this.managerService.findManagerPageByParam(manager, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /manager/deleteManagerById 删除manager(物理删除)
	 * @apiGroup manager管理
	 * @apiName 删除manager记录(物理删除)
	 * @apiDescription 根据主键id删除manager记录(物理删除)
	 * @apiParam {String} id manager主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteManagerById",method=RequestMethod.DELETE)
    public JsonResult deleteManagerById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.managerService.deleteManagerById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
