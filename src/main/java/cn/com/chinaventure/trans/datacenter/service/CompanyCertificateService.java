package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificate;

/**
 * @author 
 *
 */
public interface CompanyCertificateService {
	
	/**
     * @api saveCompanyCertificate 新增companyCertificate
     * @apiGroup companyCertificate管理
     * @apiName  新增companyCertificate记录
     * @apiDescription 全量插入companyCertificate记录
     * @apiParam {CompanyCertificate} companyCertificate companyCertificate对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyCertificate(CompanyCertificate companyCertificate);
	
	/**
     * @api updateCompanyCertificate 修改companyCertificate
     * @apiGroup companyCertificate管理
     * @apiName  修改companyCertificate记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyCertificate} companyCertificate companyCertificate对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyCertificate(CompanyCertificate companyCertificate);
	
	/**
     * @api getCompanyCertificateById 根据companyCertificateid查询详情
     * @apiGroup companyCertificate管理
     * @apiName  查询companyCertificate详情
     * @apiDescription 根据主键id查询companyCertificate详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyCertificate companyCertificate实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyCertificate getCompanyCertificateById(String id);
	
	/**
     * @api getCompanyCertificateList 根据companyCertificate条件查询列表
     * @apiGroup companyCertificate管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyCertificate} companyCertificate  实体条件
     * @apiSuccess List<CompanyCertificate> companyCertificate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCertificate> getCompanyCertificateList(CompanyCertificate companyCertificate);
	

	/**
     * @api getCompanyCertificateListByParam 根据companyCertificate条件查询列表（含排序）
     * @apiGroup companyCertificate管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyCertificate} companyCertificate  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyCertificate> companyCertificate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCertificate> getCompanyCertificateListByParam(CompanyCertificate companyCertificate, String orderByStr);
	
	
	/**
     * @api findCompanyCertificatePageByParam 根据companyCertificate条件查询列表（分页）
     * @apiGroup companyCertificate管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyCertificate} companyCertificate  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyCertificate> companyCertificate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCertificate> findCompanyCertificatePageByParam(CompanyCertificate companyCertificate, PageParam pageParam); 
	
	/**
     * @api findCompanyCertificatePageByParam 根据companyCertificate条件查询列表（分页，含排序）
     * @apiGroup companyCertificate管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyCertificate} companyCertificate  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyCertificate> companyCertificate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCertificate> findCompanyCertificatePageByParam(CompanyCertificate companyCertificate, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyCertificateById 根据companyCertificate的id删除(物理删除)
     * @apiGroup companyCertificate管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyCertificateById(String id);

}

