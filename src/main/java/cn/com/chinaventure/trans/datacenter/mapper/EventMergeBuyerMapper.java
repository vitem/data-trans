package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyer;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventMergeBuyerMapper {
    int countByExample(EventMergeBuyerExample example);

    int deleteByExample(EventMergeBuyerExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventMergeBuyer record);

    int insertSelective(EventMergeBuyer record);

    List<EventMergeBuyer> selectByExample(EventMergeBuyerExample example);

    EventMergeBuyer selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventMergeBuyer record, @Param("example") EventMergeBuyerExample example);

    int updateByExample(@Param("record") EventMergeBuyer record, @Param("example") EventMergeBuyerExample example);

    int updateByPrimaryKeySelective(EventMergeBuyer record);

    int updateByPrimaryKey(EventMergeBuyer record);
}