package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyPatent;

/**
 * @author 
 *
 */
public interface CompanyPatentService {
	
	/**
     * @api saveCompanyPatent 新增companyPatent
     * @apiGroup companyPatent管理
     * @apiName  新增companyPatent记录
     * @apiDescription 全量插入companyPatent记录
     * @apiParam {CompanyPatent} companyPatent companyPatent对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyPatent(CompanyPatent companyPatent);
	
	/**
     * @api updateCompanyPatent 修改companyPatent
     * @apiGroup companyPatent管理
     * @apiName  修改companyPatent记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyPatent} companyPatent companyPatent对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyPatent(CompanyPatent companyPatent);
	
	/**
     * @api getCompanyPatentById 根据companyPatentid查询详情
     * @apiGroup companyPatent管理
     * @apiName  查询companyPatent详情
     * @apiDescription 根据主键id查询companyPatent详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyPatent companyPatent实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyPatent getCompanyPatentById(String id);
	
	/**
     * @api getCompanyPatentList 根据companyPatent条件查询列表
     * @apiGroup companyPatent管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyPatent} companyPatent  实体条件
     * @apiSuccess List<CompanyPatent> companyPatent实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyPatent> getCompanyPatentList(CompanyPatent companyPatent);
	

	/**
     * @api getCompanyPatentListByParam 根据companyPatent条件查询列表（含排序）
     * @apiGroup companyPatent管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyPatent} companyPatent  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyPatent> companyPatent实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyPatent> getCompanyPatentListByParam(CompanyPatent companyPatent, String orderByStr);
	
	
	/**
     * @api findCompanyPatentPageByParam 根据companyPatent条件查询列表（分页）
     * @apiGroup companyPatent管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyPatent} companyPatent  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyPatent> companyPatent实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyPatent> findCompanyPatentPageByParam(CompanyPatent companyPatent, PageParam pageParam); 
	
	/**
     * @api findCompanyPatentPageByParam 根据companyPatent条件查询列表（分页，含排序）
     * @apiGroup companyPatent管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyPatent} companyPatent  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyPatent> companyPatent实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyPatent> findCompanyPatentPageByParam(CompanyPatent companyPatent, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyPatentById 根据companyPatent的id删除(物理删除)
     * @apiGroup companyPatent管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyPatentById(String id);

}

