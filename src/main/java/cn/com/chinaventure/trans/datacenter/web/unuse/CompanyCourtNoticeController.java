package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCourtNoticeService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNotice;


@RestController
@RequestMapping("/companyCourtNotice")
public class CompanyCourtNoticeController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyCourtNoticeController.class);

    @Resource
    private CompanyCourtNoticeService companyCourtNoticeService;

    /**
	 * @api {post} / 新增companyCourtNotice
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyCourtNotice(@RequestBody CompanyCourtNotice companyCourtNotice) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCourtNoticeService.saveCompanyCourtNotice(companyCourtNotice);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyCourtNotice
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyCourtNotice(@RequestBody CompanyCourtNotice companyCourtNotice) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCourtNoticeService.updateCompanyCourtNotice(companyCourtNotice);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyCourtNotice详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyCourtNotice> getCompanyCourtNoticeById(@PathVariable String id) {
        JsonResult<CompanyCourtNotice> jr = new JsonResult<CompanyCourtNotice>();
        try {
            CompanyCourtNotice companyCourtNotice  = companyCourtNoticeService.getCompanyCourtNoticeById(id);
            jr = buildJsonResult(companyCourtNotice);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyCourtNotice/getCompanyCourtNoticeList 查询companyCourtNotice列表(不带分页)
	 * @apiGroup companyCourtNotice管理
	 * @apiName 查询companyCourtNotice列表(不带分页)
	 * @apiDescription 根据条件查询companyCourtNotice列表(不带分页)
	 * @apiParam {CompanyCourtNotice} companyCourtNotice companyCourtNotice对象
	 */
    @RequestMapping("/getCompanyCourtNoticeList")
    public JsonResult<List<CompanyCourtNotice>> getCompanyCourtNoticeList(CompanyCourtNotice companyCourtNotice) {
        JsonResult<List<CompanyCourtNotice>> jr = new JsonResult<List<CompanyCourtNotice>>();
        try {
            List<CompanyCourtNotice> list = this.companyCourtNoticeService.getCompanyCourtNoticeList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyCourtNotice/findCompanyCourtNoticePageByParam 查询companyCourtNotice列表(带分页)
	 * @apiGroup companyCourtNotice管理
	 * @apiName 查询companyCourtNotice列表(带分页)
	 * @apiDescription 根据条件查询companyCourtNotice列表(带分页)
	 * @apiParam {CompanyCourtNotice} companyCourtNotice companyCourtNotice对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyCourtNoticePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyCourtNotice>> findCompanyCourtNoticePageByParam(@RequestBody CompanyCourtNotice companyCourtNotice, PageParam pageParam) {
        JsonResult<PageResult<CompanyCourtNotice>> jr = new JsonResult<PageResult<CompanyCourtNotice>>();
        try {
            PageResult<CompanyCourtNotice> page = this.companyCourtNoticeService.findCompanyCourtNoticePageByParam(companyCourtNotice, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyCourtNotice/deleteCompanyCourtNoticeById 删除companyCourtNotice(物理删除)
	 * @apiGroup companyCourtNotice管理
	 * @apiName 删除companyCourtNotice记录(物理删除)
	 * @apiDescription 根据主键id删除companyCourtNotice记录(物理删除)
	 * @apiParam {String} id companyCourtNotice主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyCourtNoticeById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyCourtNoticeById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCourtNoticeService.deleteCompanyCourtNoticeById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
