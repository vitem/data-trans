package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonage;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventIpoOrgPersonageMapper {
    int countByExample(EventIpoOrgPersonageExample example);

    int deleteByExample(EventIpoOrgPersonageExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventIpoOrgPersonage record);

    int insertSelective(EventIpoOrgPersonage record);

    List<EventIpoOrgPersonage> selectByExample(EventIpoOrgPersonageExample example);

    EventIpoOrgPersonage selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventIpoOrgPersonage record, @Param("example") EventIpoOrgPersonageExample example);

    int updateByExample(@Param("record") EventIpoOrgPersonage record, @Param("example") EventIpoOrgPersonageExample example);

    int updateByPrimaryKeySelective(EventIpoOrgPersonage record);

    int updateByPrimaryKey(EventIpoOrgPersonage record);
}