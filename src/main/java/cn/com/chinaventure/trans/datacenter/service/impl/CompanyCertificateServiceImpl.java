package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCertificateService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificate;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificateExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificateExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyCertificateMapper;


@Service("companyCertificateService")
public class CompanyCertificateServiceImpl implements CompanyCertificateService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyCertificateServiceImpl.class);

	@Resource
	private CompanyCertificateMapper companyCertificateMapper;


	@Override
	public void saveCompanyCertificate(CompanyCertificate companyCertificate) {
		if (null == companyCertificate) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCertificateMapper.insertSelective(companyCertificate);
	}

	@Override
	public void updateCompanyCertificate(CompanyCertificate companyCertificate) {
		if (null == companyCertificate) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCertificateMapper.updateByPrimaryKeySelective(companyCertificate);
	}

	@Override
	public CompanyCertificate getCompanyCertificateById(String id) {
		return companyCertificateMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyCertificate> getCompanyCertificateList(CompanyCertificate companyCertificate) {
		return this.getCompanyCertificateListByParam(companyCertificate, null);
	}

	@Override
	public List<CompanyCertificate> getCompanyCertificateListByParam(CompanyCertificate companyCertificate, String orderByStr) {
		CompanyCertificateExample example = new CompanyCertificateExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyCertificate> companyCertificateList = this.companyCertificateMapper.selectByExample(example);
		return companyCertificateList;
	}

	@Override
	public PageResult<CompanyCertificate> findCompanyCertificatePageByParam(CompanyCertificate companyCertificate, PageParam pageParam) {
		return this.findCompanyCertificatePageByParam(companyCertificate, pageParam, null);
	}

	@Override
	public PageResult<CompanyCertificate> findCompanyCertificatePageByParam(CompanyCertificate companyCertificate, PageParam pageParam, String orderByStr) {
		PageResult<CompanyCertificate> pageResult = new PageResult<CompanyCertificate>();

		CompanyCertificateExample example = new CompanyCertificateExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyCertificate> list = null;// companyCertificateMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyCertificateById(String id) {
		this.companyCertificateMapper.deleteByPrimaryKey(id);
	}

}
