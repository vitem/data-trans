package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonage;

/**
 * @author 
 *
 */
public interface EventIpoOrgPersonageService {
	
	/**
     * @api saveEventIpoOrgPersonage 新增eventIpoOrgPersonage
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  新增eventIpoOrgPersonage记录
     * @apiDescription 全量插入eventIpoOrgPersonage记录
     * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage eventIpoOrgPersonage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventIpoOrgPersonage(EventIpoOrgPersonage eventIpoOrgPersonage);
	
	/**
     * @api updateEventIpoOrgPersonage 修改eventIpoOrgPersonage
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  修改eventIpoOrgPersonage记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage eventIpoOrgPersonage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventIpoOrgPersonage(EventIpoOrgPersonage eventIpoOrgPersonage);
	
	/**
     * @api getEventIpoOrgPersonageById 根据eventIpoOrgPersonageid查询详情
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  查询eventIpoOrgPersonage详情
     * @apiDescription 根据主键id查询eventIpoOrgPersonage详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventIpoOrgPersonage eventIpoOrgPersonage实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventIpoOrgPersonage getEventIpoOrgPersonageById(String id);
	
	/**
     * @api getEventIpoOrgPersonageList 根据eventIpoOrgPersonage条件查询列表
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage  实体条件
     * @apiSuccess List<EventIpoOrgPersonage> eventIpoOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventIpoOrgPersonage> getEventIpoOrgPersonageList(EventIpoOrgPersonage eventIpoOrgPersonage);
	

	/**
     * @api getEventIpoOrgPersonageListByParam 根据eventIpoOrgPersonage条件查询列表（含排序）
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventIpoOrgPersonage> eventIpoOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventIpoOrgPersonage> getEventIpoOrgPersonageListByParam(EventIpoOrgPersonage eventIpoOrgPersonage, String orderByStr);
	
	
	/**
     * @api findEventIpoOrgPersonagePageByParam 根据eventIpoOrgPersonage条件查询列表（分页）
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventIpoOrgPersonage> eventIpoOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventIpoOrgPersonage> findEventIpoOrgPersonagePageByParam(EventIpoOrgPersonage eventIpoOrgPersonage, PageParam pageParam); 
	
	/**
     * @api findEventIpoOrgPersonagePageByParam 根据eventIpoOrgPersonage条件查询列表（分页，含排序）
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventIpoOrgPersonage> eventIpoOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventIpoOrgPersonage> findEventIpoOrgPersonagePageByParam(EventIpoOrgPersonage eventIpoOrgPersonage, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventIpoOrgPersonageById 根据eventIpoOrgPersonage的id删除(物理删除)
     * @apiGroup eventIpoOrgPersonage管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventIpoOrgPersonageById(String id);

}

