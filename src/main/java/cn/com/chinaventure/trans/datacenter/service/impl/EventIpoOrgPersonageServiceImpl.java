package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventIpoOrgPersonageService;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonage;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonageExample;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonageExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventIpoOrgPersonageMapper;


@Service("eventIpoOrgPersonageService")
public class EventIpoOrgPersonageServiceImpl implements EventIpoOrgPersonageService {

	private final static Logger logger = LoggerFactory.getLogger(EventIpoOrgPersonageServiceImpl.class);

	@Resource
	private EventIpoOrgPersonageMapper eventIpoOrgPersonageMapper;


	@Override
	public void saveEventIpoOrgPersonage(EventIpoOrgPersonage eventIpoOrgPersonage) {
		if (null == eventIpoOrgPersonage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventIpoOrgPersonageMapper.insertSelective(eventIpoOrgPersonage);
	}

	@Override
	public void updateEventIpoOrgPersonage(EventIpoOrgPersonage eventIpoOrgPersonage) {
		if (null == eventIpoOrgPersonage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventIpoOrgPersonageMapper.updateByPrimaryKeySelective(eventIpoOrgPersonage);
	}

	@Override
	public EventIpoOrgPersonage getEventIpoOrgPersonageById(String id) {
		return eventIpoOrgPersonageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventIpoOrgPersonage> getEventIpoOrgPersonageList(EventIpoOrgPersonage eventIpoOrgPersonage) {
		return this.getEventIpoOrgPersonageListByParam(eventIpoOrgPersonage, null);
	}

	@Override
	public List<EventIpoOrgPersonage> getEventIpoOrgPersonageListByParam(EventIpoOrgPersonage eventIpoOrgPersonage, String orderByStr) {
		EventIpoOrgPersonageExample example = new EventIpoOrgPersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventIpoOrgPersonage> eventIpoOrgPersonageList = this.eventIpoOrgPersonageMapper.selectByExample(example);
		return eventIpoOrgPersonageList;
	}

	@Override
	public PageResult<EventIpoOrgPersonage> findEventIpoOrgPersonagePageByParam(EventIpoOrgPersonage eventIpoOrgPersonage, PageParam pageParam) {
		return this.findEventIpoOrgPersonagePageByParam(eventIpoOrgPersonage, pageParam, null);
	}

	@Override
	public PageResult<EventIpoOrgPersonage> findEventIpoOrgPersonagePageByParam(EventIpoOrgPersonage eventIpoOrgPersonage, PageParam pageParam, String orderByStr) {
		PageResult<EventIpoOrgPersonage> pageResult = new PageResult<EventIpoOrgPersonage>();

		EventIpoOrgPersonageExample example = new EventIpoOrgPersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventIpoOrgPersonage> list = null;// eventIpoOrgPersonageMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventIpoOrgPersonageById(String id) {
		this.eventIpoOrgPersonageMapper.deleteByPrimaryKey(id);
	}

}
