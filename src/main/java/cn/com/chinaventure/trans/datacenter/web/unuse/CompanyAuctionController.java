package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyAuctionService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyAuction;


@RestController
@RequestMapping("/companyAuction")
public class CompanyAuctionController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyAuctionController.class);

    @Resource
    private CompanyAuctionService companyAuctionService;

    /**
	 * @api {post} / 新增companyAuction
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyAuction(@RequestBody CompanyAuction companyAuction) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyAuctionService.saveCompanyAuction(companyAuction);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyAuction
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyAuction(@RequestBody CompanyAuction companyAuction) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyAuctionService.updateCompanyAuction(companyAuction);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyAuction详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyAuction> getCompanyAuctionById(@PathVariable String id) {
        JsonResult<CompanyAuction> jr = new JsonResult<CompanyAuction>();
        try {
            CompanyAuction companyAuction  = companyAuctionService.getCompanyAuctionById(id);
            jr = buildJsonResult(companyAuction);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyAuction/getCompanyAuctionList 查询companyAuction列表(不带分页)
	 * @apiGroup companyAuction管理
	 * @apiName 查询companyAuction列表(不带分页)
	 * @apiDescription 根据条件查询companyAuction列表(不带分页)
	 * @apiParam {CompanyAuction} companyAuction companyAuction对象
	 */
    @RequestMapping("/getCompanyAuctionList")
    public JsonResult<List<CompanyAuction>> getCompanyAuctionList(CompanyAuction companyAuction) {
        JsonResult<List<CompanyAuction>> jr = new JsonResult<List<CompanyAuction>>();
        try {
            List<CompanyAuction> list = this.companyAuctionService.getCompanyAuctionList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyAuction/findCompanyAuctionPageByParam 查询companyAuction列表(带分页)
	 * @apiGroup companyAuction管理
	 * @apiName 查询companyAuction列表(带分页)
	 * @apiDescription 根据条件查询companyAuction列表(带分页)
	 * @apiParam {CompanyAuction} companyAuction companyAuction对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyAuctionPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyAuction>> findCompanyAuctionPageByParam(@RequestBody CompanyAuction companyAuction, PageParam pageParam) {
        JsonResult<PageResult<CompanyAuction>> jr = new JsonResult<PageResult<CompanyAuction>>();
        try {
            PageResult<CompanyAuction> page = this.companyAuctionService.findCompanyAuctionPageByParam(companyAuction, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyAuction/deleteCompanyAuctionById 删除companyAuction(物理删除)
	 * @apiGroup companyAuction管理
	 * @apiName 删除companyAuction记录(物理删除)
	 * @apiDescription 根据主键id删除companyAuction记录(物理删除)
	 * @apiParam {String} id companyAuction主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyAuctionById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyAuctionById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyAuctionService.deleteCompanyAuctionById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
