package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyShareholderService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholder;


@RestController
@RequestMapping("/companyShareholder")
public class CompanyShareholderController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyShareholderController.class);

    @Resource
    private CompanyShareholderService companyShareholderService;

    /**
	 * @api {post} / 新增companyShareholder
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyShareholder(@RequestBody CompanyShareholder companyShareholder) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyShareholderService.saveCompanyShareholder(companyShareholder);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyShareholder
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyShareholder(@RequestBody CompanyShareholder companyShareholder) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyShareholderService.updateCompanyShareholder(companyShareholder);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyShareholder详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyShareholder> getCompanyShareholderById(@PathVariable String id) {
        JsonResult<CompanyShareholder> jr = new JsonResult<CompanyShareholder>();
        try {
            CompanyShareholder companyShareholder  = companyShareholderService.getCompanyShareholderById(id);
            jr = buildJsonResult(companyShareholder);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyShareholder/getCompanyShareholderList 查询companyShareholder列表(不带分页)
	 * @apiGroup companyShareholder管理
	 * @apiName 查询companyShareholder列表(不带分页)
	 * @apiDescription 根据条件查询companyShareholder列表(不带分页)
	 * @apiParam {CompanyShareholder} companyShareholder companyShareholder对象
	 */
    @RequestMapping("/getCompanyShareholderList")
    public JsonResult<List<CompanyShareholder>> getCompanyShareholderList(CompanyShareholder companyShareholder) {
        JsonResult<List<CompanyShareholder>> jr = new JsonResult<List<CompanyShareholder>>();
        try {
            List<CompanyShareholder> list = this.companyShareholderService.getCompanyShareholderList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyShareholder/findCompanyShareholderPageByParam 查询companyShareholder列表(带分页)
	 * @apiGroup companyShareholder管理
	 * @apiName 查询companyShareholder列表(带分页)
	 * @apiDescription 根据条件查询companyShareholder列表(带分页)
	 * @apiParam {CompanyShareholder} companyShareholder companyShareholder对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyShareholderPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyShareholder>> findCompanyShareholderPageByParam(@RequestBody CompanyShareholder companyShareholder, PageParam pageParam) {
        JsonResult<PageResult<CompanyShareholder>> jr = new JsonResult<PageResult<CompanyShareholder>>();
        try {
            PageResult<CompanyShareholder> page = this.companyShareholderService.findCompanyShareholderPageByParam(companyShareholder, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyShareholder/deleteCompanyShareholderById 删除companyShareholder(物理删除)
	 * @apiGroup companyShareholder管理
	 * @apiName 删除companyShareholder记录(物理删除)
	 * @apiDescription 根据主键id删除companyShareholder记录(物理删除)
	 * @apiParam {String} id companyShareholder主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyShareholderById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyShareholderById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyShareholderService.deleteCompanyShareholderById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
