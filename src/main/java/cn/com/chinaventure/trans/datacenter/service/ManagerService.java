package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Manager;

/**
 * @author 
 *
 */
public interface ManagerService {
	
	/**
     * @api saveManager 新增manager
     * @apiGroup manager管理
     * @apiName  新增manager记录
     * @apiDescription 全量插入manager记录
     * @apiParam {Manager} manager manager对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveManager(Manager manager);
	
	/**
     * @api updateManager 修改manager
     * @apiGroup manager管理
     * @apiName  修改manager记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Manager} manager manager对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateManager(Manager manager);
	
	/**
     * @api getManagerById 根据managerid查询详情
     * @apiGroup manager管理
     * @apiName  查询manager详情
     * @apiDescription 根据主键id查询manager详情
     * @apiParam {String} id 主键id
     * @apiSuccess Manager manager实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Manager getManagerById(String id);
	
	/**
     * @api getManagerList 根据manager条件查询列表
     * @apiGroup manager管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Manager} manager  实体条件
     * @apiSuccess List<Manager> manager实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Manager> getManagerList(Manager manager);
	

	/**
     * @api getManagerListByParam 根据manager条件查询列表（含排序）
     * @apiGroup manager管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Manager} manager  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Manager> manager实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Manager> getManagerListByParam(Manager manager, String orderByStr);
	
	
	/**
     * @api findManagerPageByParam 根据manager条件查询列表（分页）
     * @apiGroup manager管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Manager} manager  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Manager> manager实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Manager> findManagerPageByParam(Manager manager, PageParam pageParam); 
	
	/**
     * @api findManagerPageByParam 根据manager条件查询列表（分页，含排序）
     * @apiGroup manager管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Manager} manager  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Manager> manager实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Manager> findManagerPageByParam(Manager manager, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteManagerById 根据manager的id删除(物理删除)
     * @apiGroup manager管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteManagerById(String id);

}

