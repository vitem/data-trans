package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.OrganizationService;
import cn.com.chinaventure.trans.datacenter.entity.Organization;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationExample;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.OrganizationMapper;


@Service("organizationService")
public class OrganizationServiceImpl implements OrganizationService {

	private final static Logger logger = LoggerFactory.getLogger(OrganizationServiceImpl.class);

	@Resource
	private OrganizationMapper organizationMapper;


	@Override
	public void saveOrganization(Organization organization) {
		if (null == organization) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.organizationMapper.insertSelective(organization);
	}

	@Override
	public void updateOrganization(Organization organization) {
		if (null == organization) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.organizationMapper.updateByPrimaryKeySelective(organization);
	}

	@Override
	public Organization getOrganizationById(String id) {
		return organizationMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Organization> getOrganizationList(Organization organization) {
		return this.getOrganizationListByParam(organization, null);
	}

	@Override
	public List<Organization> getOrganizationListByParam(Organization organization, String orderByStr) {
		OrganizationExample example = new OrganizationExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Organization> organizationList = this.organizationMapper.selectByExample(example);
		return organizationList;
	}

	@Override
	public PageResult<Organization> findOrganizationPageByParam(Organization organization, PageParam pageParam) {
		return this.findOrganizationPageByParam(organization, pageParam, null);
	}

	@Override
	public PageResult<Organization> findOrganizationPageByParam(Organization organization, PageParam pageParam, String orderByStr) {
		PageResult<Organization> pageResult = new PageResult<Organization>();

		OrganizationExample example = new OrganizationExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Organization> list = null;// organizationMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteOrganizationById(String id) {
		this.organizationMapper.deleteByPrimaryKey(id);
	}

}
