package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyManageService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyManage;
import cn.com.chinaventure.trans.datacenter.entity.CompanyManageExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyManageExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyManageMapper;


@Service("companyManageService")
public class CompanyManageServiceImpl implements CompanyManageService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyManageServiceImpl.class);

	@Resource
	private CompanyManageMapper companyManageMapper;


	@Override
	public void saveCompanyManage(CompanyManage companyManage) {
		if (null == companyManage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyManageMapper.insertSelective(companyManage);
	}

	@Override
	public void updateCompanyManage(CompanyManage companyManage) {
		if (null == companyManage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyManageMapper.updateByPrimaryKeySelective(companyManage);
	}

	@Override
	public CompanyManage getCompanyManageById(String id) {
		return companyManageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyManage> getCompanyManageList(CompanyManage companyManage) {
		return this.getCompanyManageListByParam(companyManage, null);
	}

	@Override
	public List<CompanyManage> getCompanyManageListByParam(CompanyManage companyManage, String orderByStr) {
		CompanyManageExample example = new CompanyManageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyManage> companyManageList = this.companyManageMapper.selectByExample(example);
		return companyManageList;
	}

	@Override
	public PageResult<CompanyManage> findCompanyManagePageByParam(CompanyManage companyManage, PageParam pageParam) {
		return this.findCompanyManagePageByParam(companyManage, pageParam, null);
	}

	@Override
	public PageResult<CompanyManage> findCompanyManagePageByParam(CompanyManage companyManage, PageParam pageParam, String orderByStr) {
		PageResult<CompanyManage> pageResult = new PageResult<CompanyManage>();

		CompanyManageExample example = new CompanyManageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyManage> list = null;// companyManageMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyManageById(String id) {
		this.companyManageMapper.deleteByPrimaryKey(id);
	}

}
