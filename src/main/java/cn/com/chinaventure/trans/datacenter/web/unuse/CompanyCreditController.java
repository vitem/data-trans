package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCreditService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCredit;


@RestController
@RequestMapping("/companyCredit")
public class CompanyCreditController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyCreditController.class);

    @Resource
    private CompanyCreditService companyCreditService;

    /**
	 * @api {post} / 新增companyCredit
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyCredit(@RequestBody CompanyCredit companyCredit) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCreditService.saveCompanyCredit(companyCredit);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyCredit
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyCredit(@RequestBody CompanyCredit companyCredit) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCreditService.updateCompanyCredit(companyCredit);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyCredit详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyCredit> getCompanyCreditById(@PathVariable String id) {
        JsonResult<CompanyCredit> jr = new JsonResult<CompanyCredit>();
        try {
            CompanyCredit companyCredit  = companyCreditService.getCompanyCreditById(id);
            jr = buildJsonResult(companyCredit);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyCredit/getCompanyCreditList 查询companyCredit列表(不带分页)
	 * @apiGroup companyCredit管理
	 * @apiName 查询companyCredit列表(不带分页)
	 * @apiDescription 根据条件查询companyCredit列表(不带分页)
	 * @apiParam {CompanyCredit} companyCredit companyCredit对象
	 */
    @RequestMapping("/getCompanyCreditList")
    public JsonResult<List<CompanyCredit>> getCompanyCreditList(CompanyCredit companyCredit) {
        JsonResult<List<CompanyCredit>> jr = new JsonResult<List<CompanyCredit>>();
        try {
            List<CompanyCredit> list = this.companyCreditService.getCompanyCreditList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyCredit/findCompanyCreditPageByParam 查询companyCredit列表(带分页)
	 * @apiGroup companyCredit管理
	 * @apiName 查询companyCredit列表(带分页)
	 * @apiDescription 根据条件查询companyCredit列表(带分页)
	 * @apiParam {CompanyCredit} companyCredit companyCredit对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyCreditPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyCredit>> findCompanyCreditPageByParam(@RequestBody CompanyCredit companyCredit, PageParam pageParam) {
        JsonResult<PageResult<CompanyCredit>> jr = new JsonResult<PageResult<CompanyCredit>>();
        try {
            PageResult<CompanyCredit> page = this.companyCreditService.findCompanyCreditPageByParam(companyCredit, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyCredit/deleteCompanyCreditById 删除companyCredit(物理删除)
	 * @apiGroup companyCredit管理
	 * @apiName 删除companyCredit记录(物理删除)
	 * @apiDescription 根据主键id删除companyCredit记录(物理删除)
	 * @apiParam {String} id companyCredit主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyCreditById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyCreditById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCreditService.deleteCompanyCreditById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
