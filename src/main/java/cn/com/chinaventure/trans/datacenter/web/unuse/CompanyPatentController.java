package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyPatentService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyPatent;


@RestController
@RequestMapping("/companyPatent")
public class CompanyPatentController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyPatentController.class);

    @Resource
    private CompanyPatentService companyPatentService;

    /**
	 * @api {post} / 新增companyPatent
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyPatent(@RequestBody CompanyPatent companyPatent) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyPatentService.saveCompanyPatent(companyPatent);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyPatent
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyPatent(@RequestBody CompanyPatent companyPatent) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyPatentService.updateCompanyPatent(companyPatent);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyPatent详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyPatent> getCompanyPatentById(@PathVariable String id) {
        JsonResult<CompanyPatent> jr = new JsonResult<CompanyPatent>();
        try {
            CompanyPatent companyPatent  = companyPatentService.getCompanyPatentById(id);
            jr = buildJsonResult(companyPatent);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyPatent/getCompanyPatentList 查询companyPatent列表(不带分页)
	 * @apiGroup companyPatent管理
	 * @apiName 查询companyPatent列表(不带分页)
	 * @apiDescription 根据条件查询companyPatent列表(不带分页)
	 * @apiParam {CompanyPatent} companyPatent companyPatent对象
	 */
    @RequestMapping("/getCompanyPatentList")
    public JsonResult<List<CompanyPatent>> getCompanyPatentList(CompanyPatent companyPatent) {
        JsonResult<List<CompanyPatent>> jr = new JsonResult<List<CompanyPatent>>();
        try {
            List<CompanyPatent> list = this.companyPatentService.getCompanyPatentList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyPatent/findCompanyPatentPageByParam 查询companyPatent列表(带分页)
	 * @apiGroup companyPatent管理
	 * @apiName 查询companyPatent列表(带分页)
	 * @apiDescription 根据条件查询companyPatent列表(带分页)
	 * @apiParam {CompanyPatent} companyPatent companyPatent对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyPatentPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyPatent>> findCompanyPatentPageByParam(@RequestBody CompanyPatent companyPatent, PageParam pageParam) {
        JsonResult<PageResult<CompanyPatent>> jr = new JsonResult<PageResult<CompanyPatent>>();
        try {
            PageResult<CompanyPatent> page = this.companyPatentService.findCompanyPatentPageByParam(companyPatent, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyPatent/deleteCompanyPatentById 删除companyPatent(物理删除)
	 * @apiGroup companyPatent管理
	 * @apiName 删除companyPatent记录(物理删除)
	 * @apiDescription 根据主键id删除companyPatent记录(物理删除)
	 * @apiParam {String} id companyPatent主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyPatentById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyPatentById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyPatentService.deleteCompanyPatentById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
