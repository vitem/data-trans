package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.ContactService;
import cn.com.chinaventure.trans.datacenter.entity.Contact;


@RestController
@RequestMapping("/contact")
public class ContactController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(ContactController.class);

    @Resource
    private ContactService contactService;

    /**
	 * @api {post} / 新增contact
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveContact(@RequestBody Contact contact) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.contactService.saveContact(contact);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改contact
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateContact(@RequestBody Contact contact) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.contactService.updateContact(contact);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询contact详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Contact> getContactById(@PathVariable String id) {
        JsonResult<Contact> jr = new JsonResult<Contact>();
        try {
            Contact contact  = contactService.getContactById(id);
            jr = buildJsonResult(contact);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /contact/getContactList 查询contact列表(不带分页)
	 * @apiGroup contact管理
	 * @apiName 查询contact列表(不带分页)
	 * @apiDescription 根据条件查询contact列表(不带分页)
	 * @apiParam {Contact} contact contact对象
	 */
    @RequestMapping("/getContactList")
    public JsonResult<List<Contact>> getContactList(Contact contact) {
        JsonResult<List<Contact>> jr = new JsonResult<List<Contact>>();
        try {
            List<Contact> list = this.contactService.getContactList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /contact/findContactPageByParam 查询contact列表(带分页)
	 * @apiGroup contact管理
	 * @apiName 查询contact列表(带分页)
	 * @apiDescription 根据条件查询contact列表(带分页)
	 * @apiParam {Contact} contact contact对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findContactPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Contact>> findContactPageByParam(@RequestBody Contact contact, PageParam pageParam) {
        JsonResult<PageResult<Contact>> jr = new JsonResult<PageResult<Contact>>();
        try {
            PageResult<Contact> page = this.contactService.findContactPageByParam(contact, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /contact/deleteContactById 删除contact(物理删除)
	 * @apiGroup contact管理
	 * @apiName 删除contact记录(物理删除)
	 * @apiDescription 根据主键id删除contact记录(物理删除)
	 * @apiParam {String} id contact主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteContactById",method=RequestMethod.DELETE)
    public JsonResult deleteContactById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.contactService.deleteContactById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
