package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyer;

/**
 * @author 
 *
 */
public interface EventMergeBuyerService {
	
	/**
     * @api saveEventMergeBuyer 新增eventMergeBuyer
     * @apiGroup eventMergeBuyer管理
     * @apiName  新增eventMergeBuyer记录
     * @apiDescription 全量插入eventMergeBuyer记录
     * @apiParam {EventMergeBuyer} eventMergeBuyer eventMergeBuyer对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventMergeBuyer(EventMergeBuyer eventMergeBuyer);
	
	/**
     * @api updateEventMergeBuyer 修改eventMergeBuyer
     * @apiGroup eventMergeBuyer管理
     * @apiName  修改eventMergeBuyer记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventMergeBuyer} eventMergeBuyer eventMergeBuyer对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventMergeBuyer(EventMergeBuyer eventMergeBuyer);
	
	/**
     * @api getEventMergeBuyerById 根据eventMergeBuyerid查询详情
     * @apiGroup eventMergeBuyer管理
     * @apiName  查询eventMergeBuyer详情
     * @apiDescription 根据主键id查询eventMergeBuyer详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventMergeBuyer eventMergeBuyer实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventMergeBuyer getEventMergeBuyerById(String id);
	
	/**
     * @api getEventMergeBuyerList 根据eventMergeBuyer条件查询列表
     * @apiGroup eventMergeBuyer管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventMergeBuyer} eventMergeBuyer  实体条件
     * @apiSuccess List<EventMergeBuyer> eventMergeBuyer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeBuyer> getEventMergeBuyerList(EventMergeBuyer eventMergeBuyer);
	

	/**
     * @api getEventMergeBuyerListByParam 根据eventMergeBuyer条件查询列表（含排序）
     * @apiGroup eventMergeBuyer管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventMergeBuyer} eventMergeBuyer  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventMergeBuyer> eventMergeBuyer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeBuyer> getEventMergeBuyerListByParam(EventMergeBuyer eventMergeBuyer, String orderByStr);
	
	
	/**
     * @api findEventMergeBuyerPageByParam 根据eventMergeBuyer条件查询列表（分页）
     * @apiGroup eventMergeBuyer管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventMergeBuyer} eventMergeBuyer  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventMergeBuyer> eventMergeBuyer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeBuyer> findEventMergeBuyerPageByParam(EventMergeBuyer eventMergeBuyer, PageParam pageParam); 
	
	/**
     * @api findEventMergeBuyerPageByParam 根据eventMergeBuyer条件查询列表（分页，含排序）
     * @apiGroup eventMergeBuyer管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventMergeBuyer} eventMergeBuyer  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventMergeBuyer> eventMergeBuyer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeBuyer> findEventMergeBuyerPageByParam(EventMergeBuyer eventMergeBuyer, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventMergeBuyerById 根据eventMergeBuyer的id删除(物理删除)
     * @apiGroup eventMergeBuyer管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventMergeBuyerById(String id);

}

