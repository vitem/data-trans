package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.Lp;
import cn.com.chinaventure.trans.datacenter.entity.LpExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface LpMapper {
    int countByExample(LpExample example);

    int deleteByExample(LpExample example);

    int deleteByPrimaryKey(String id);

    int insert(Lp record);

    int insertSelective(Lp record);

    List<Lp> selectByExample(LpExample example);

    Lp selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Lp record, @Param("example") LpExample example);

    int updateByExample(@Param("record") Lp record, @Param("example") LpExample example);

    int updateByPrimaryKeySelective(Lp record);

    int updateByPrimaryKey(Lp record);
}