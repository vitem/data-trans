package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventMerge;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventMergeMapper {
    int countByExample(EventMergeExample example);

    int deleteByExample(EventMergeExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventMerge record);

    int insertSelective(EventMerge record);

    List<EventMerge> selectByExample(EventMergeExample example);

    EventMerge selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventMerge record, @Param("example") EventMergeExample example);

    int updateByExample(@Param("record") EventMerge record, @Param("example") EventMergeExample example);

    int updateByPrimaryKeySelective(EventMerge record);

    int updateByPrimaryKey(EventMerge record);
}