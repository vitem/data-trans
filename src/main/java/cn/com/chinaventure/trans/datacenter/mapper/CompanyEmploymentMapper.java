package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyEmployment;
import cn.com.chinaventure.trans.datacenter.entity.CompanyEmploymentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyEmploymentMapper {
    int countByExample(CompanyEmploymentExample example);

    int deleteByExample(CompanyEmploymentExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyEmployment record);

    int insertSelective(CompanyEmployment record);

    List<CompanyEmployment> selectByExample(CompanyEmploymentExample example);

    CompanyEmployment selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyEmployment record, @Param("example") CompanyEmploymentExample example);

    int updateByExample(@Param("record") CompanyEmployment record, @Param("example") CompanyEmploymentExample example);

    int updateByPrimaryKeySelective(CompanyEmployment record);

    int updateByPrimaryKey(CompanyEmployment record);
}