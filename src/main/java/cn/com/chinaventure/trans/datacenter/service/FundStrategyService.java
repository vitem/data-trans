package cn.com.chinaventure.trans.datacenter.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.FundStrategy;

import java.util.List;

/**
 * @author 
 *
 */
public interface FundStrategyService {
	
	/**
     * @api saveFundStrategy 新增fundStrategy
     * @apiGroup fundStrategy管理
     * @apiName  新增fundStrategy记录
     * @apiDescription 全量插入fundStrategy记录
     * @apiParam {FundStrategy} fundStrategy fundStrategy对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveFundStrategy(FundStrategy fundStrategy);
	
	/**
     * @api updateFundStrategy 修改fundStrategy
     * @apiGroup fundStrategy管理
     * @apiName  修改fundStrategy记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {FundStrategy} fundStrategy fundStrategy对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateFundStrategy(FundStrategy fundStrategy);
	
	/**
     * @api getFundStrategyById 根据fundStrategyid查询详情
     * @apiGroup fundStrategy管理
     * @apiName  查询fundStrategy详情
     * @apiDescription 根据主键id查询fundStrategy详情
     * @apiParam {String} id 主键id
     * @apiSuccess FundStrategy fundStrategy实体对象
     * @apiVersion 1.0.0
     * 
     */
	public FundStrategy getFundStrategyById(Long id);
	
	/**
     * @api getFundStrategyList 根据fundStrategy条件查询列表
     * @apiGroup fundStrategy管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {FundStrategy} fundStrategy  实体条件
     * @apiSuccess List<FundStrategy> fundStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<FundStrategy> getFundStrategyList(FundStrategy fundStrategy);
	

	/**
     * @api getFundStrategyListByParam 根据fundStrategy条件查询列表（含排序）
     * @apiGroup fundStrategy管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {FundStrategy} fundStrategy  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<FundStrategy> fundStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<FundStrategy> getFundStrategyListByParam(FundStrategy fundStrategy, String orderByStr);
	
	
	/**
     * @api findFundStrategyPageByParam 根据fundStrategy条件查询列表（分页）
     * @apiGroup fundStrategy管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {FundStrategy} fundStrategy  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<FundStrategy> fundStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<FundStrategy> findFundStrategyPageByParam(FundStrategy fundStrategy, PageParam pageParam); 
	
	/**
     * @api findFundStrategyPageByParam 根据fundStrategy条件查询列表（分页，含排序）
     * @apiGroup fundStrategy管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {FundStrategy} fundStrategy  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<FundStrategy> fundStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<FundStrategy> findFundStrategyPageByParam(FundStrategy fundStrategy, PageParam pageParam, String orderByStr);
	


}

