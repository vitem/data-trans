package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyService;
import cn.com.chinaventure.trans.datacenter.entity.Company;
import cn.com.chinaventure.trans.datacenter.entity.CompanyExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyMapper;


@Service("companyService")
public class CompanyServiceImpl implements CompanyService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyServiceImpl.class);

	@Resource
	private CompanyMapper companyMapper;


	@Override
	public void saveCompany(Company company) {
		if (null == company) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyMapper.insertSelective(company);
	}

	@Override
	public void updateCompany(Company company) {
		if (null == company) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyMapper.updateByPrimaryKeySelective(company);
	}

	@Override
	public Company getCompanyById(String id) {
		return companyMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Company> getCompanyList(Company company) {
		return this.getCompanyListByParam(company, null);
	}

	@Override
	public List<Company> getCompanyListByParam(Company company, String orderByStr) {
		CompanyExample example = new CompanyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Company> companyList = this.companyMapper.selectByExample(example);
		return companyList;
	}

	@Override
	public PageResult<Company> findCompanyPageByParam(Company company, PageParam pageParam) {
		return this.findCompanyPageByParam(company, pageParam, null);
	}

	@Override
	public PageResult<Company> findCompanyPageByParam(Company company, PageParam pageParam, String orderByStr) {
		PageResult<Company> pageResult = new PageResult<Company>();

		CompanyExample example = new CompanyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Company> list = null;// companyMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyById(String id) {
		this.companyMapper.deleteByPrimaryKey(id);
	}

}
