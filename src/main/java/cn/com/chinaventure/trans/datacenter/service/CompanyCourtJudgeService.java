package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudge;

/**
 * @author 
 *
 */
public interface CompanyCourtJudgeService {
	
	/**
     * @api saveCompanyCourtJudge 新增companyCourtJudge
     * @apiGroup companyCourtJudge管理
     * @apiName  新增companyCourtJudge记录
     * @apiDescription 全量插入companyCourtJudge记录
     * @apiParam {CompanyCourtJudge} companyCourtJudge companyCourtJudge对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyCourtJudge(CompanyCourtJudge companyCourtJudge);
	
	/**
     * @api updateCompanyCourtJudge 修改companyCourtJudge
     * @apiGroup companyCourtJudge管理
     * @apiName  修改companyCourtJudge记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyCourtJudge} companyCourtJudge companyCourtJudge对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyCourtJudge(CompanyCourtJudge companyCourtJudge);
	
	/**
     * @api getCompanyCourtJudgeById 根据companyCourtJudgeid查询详情
     * @apiGroup companyCourtJudge管理
     * @apiName  查询companyCourtJudge详情
     * @apiDescription 根据主键id查询companyCourtJudge详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyCourtJudge companyCourtJudge实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyCourtJudge getCompanyCourtJudgeById(String id);
	
	/**
     * @api getCompanyCourtJudgeList 根据companyCourtJudge条件查询列表
     * @apiGroup companyCourtJudge管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyCourtJudge} companyCourtJudge  实体条件
     * @apiSuccess List<CompanyCourtJudge> companyCourtJudge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCourtJudge> getCompanyCourtJudgeList(CompanyCourtJudge companyCourtJudge);
	

	/**
     * @api getCompanyCourtJudgeListByParam 根据companyCourtJudge条件查询列表（含排序）
     * @apiGroup companyCourtJudge管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyCourtJudge} companyCourtJudge  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyCourtJudge> companyCourtJudge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCourtJudge> getCompanyCourtJudgeListByParam(CompanyCourtJudge companyCourtJudge, String orderByStr);
	
	
	/**
     * @api findCompanyCourtJudgePageByParam 根据companyCourtJudge条件查询列表（分页）
     * @apiGroup companyCourtJudge管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyCourtJudge} companyCourtJudge  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyCourtJudge> companyCourtJudge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCourtJudge> findCompanyCourtJudgePageByParam(CompanyCourtJudge companyCourtJudge, PageParam pageParam); 
	
	/**
     * @api findCompanyCourtJudgePageByParam 根据companyCourtJudge条件查询列表（分页，含排序）
     * @apiGroup companyCourtJudge管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyCourtJudge} companyCourtJudge  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyCourtJudge> companyCourtJudge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCourtJudge> findCompanyCourtJudgePageByParam(CompanyCourtJudge companyCourtJudge, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyCourtJudgeById 根据companyCourtJudge的id删除(物理删除)
     * @apiGroup companyCourtJudge管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyCourtJudgeById(String id);

}

