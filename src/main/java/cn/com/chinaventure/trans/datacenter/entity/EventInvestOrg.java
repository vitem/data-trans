package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EventInvestOrg implements Serializable {
    private String id;

    /**
     * 融资事件
     */
    private String eventInvestId;

    /**
     * 机构
     */
    private String orgId;

    /**
     * 机构人物
     */
    private String orgPersonageId;

    /**
     * 基金
     */
    private String fundId;

    /**
     * 基金人物
     */
    private String fundPersonageId;

    /**
     * 自然人
     */
    private String personageId;

    /**
     * 投资金额
     */
    private BigDecimal investAmount;

    /**
     * 交易后持有的股权
     */
    private BigDecimal stockRightRate;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventInvestId() {
        return eventInvestId;
    }

    public void setEventInvestId(String eventInvestId) {
        this.eventInvestId = eventInvestId == null ? null : eventInvestId.trim();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getOrgPersonageId() {
        return orgPersonageId;
    }

    public void setOrgPersonageId(String orgPersonageId) {
        this.orgPersonageId = orgPersonageId == null ? null : orgPersonageId.trim();
    }

    public String getFundId() {
        return fundId;
    }

    public void setFundId(String fundId) {
        this.fundId = fundId == null ? null : fundId.trim();
    }

    public String getFundPersonageId() {
        return fundPersonageId;
    }

    public void setFundPersonageId(String fundPersonageId) {
        this.fundPersonageId = fundPersonageId == null ? null : fundPersonageId.trim();
    }

    public String getPersonageId() {
        return personageId;
    }

    public void setPersonageId(String personageId) {
        this.personageId = personageId == null ? null : personageId.trim();
    }

    public BigDecimal getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(BigDecimal investAmount) {
        this.investAmount = investAmount;
    }

    public BigDecimal getStockRightRate() {
        return stockRightRate;
    }

    public void setStockRightRate(BigDecimal stockRightRate) {
        this.stockRightRate = stockRightRate;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventInvestId=").append(eventInvestId);
        sb.append(", orgId=").append(orgId);
        sb.append(", orgPersonageId=").append(orgPersonageId);
        sb.append(", fundId=").append(fundId);
        sb.append(", fundPersonageId=").append(fundPersonageId);
        sb.append(", personageId=").append(personageId);
        sb.append(", investAmount=").append(investAmount);
        sb.append(", stockRightRate=").append(stockRightRate);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}