package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyAuction;
import cn.com.chinaventure.trans.datacenter.entity.CompanyAuctionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyAuctionMapper {
    int countByExample(CompanyAuctionExample example);

    int deleteByExample(CompanyAuctionExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyAuction record);

    int insertSelective(CompanyAuction record);

    List<CompanyAuction> selectByExample(CompanyAuctionExample example);

    CompanyAuction selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyAuction record, @Param("example") CompanyAuctionExample example);

    int updateByExample(@Param("record") CompanyAuction record, @Param("example") CompanyAuctionExample example);

    int updateByPrimaryKeySelective(CompanyAuction record);

    int updateByPrimaryKey(CompanyAuction record);
}