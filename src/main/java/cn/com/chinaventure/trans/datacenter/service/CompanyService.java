package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Company;

/**
 * @author 
 *
 */
public interface CompanyService {
	
	/**
     * @api saveCompany 新增company
     * @apiGroup company管理
     * @apiName  新增company记录
     * @apiDescription 全量插入company记录
     * @apiParam {Company} company company对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompany(Company company);
	
	/**
     * @api updateCompany 修改company
     * @apiGroup company管理
     * @apiName  修改company记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Company} company company对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompany(Company company);
	
	/**
     * @api getCompanyById 根据companyid查询详情
     * @apiGroup company管理
     * @apiName  查询company详情
     * @apiDescription 根据主键id查询company详情
     * @apiParam {String} id 主键id
     * @apiSuccess Company company实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Company getCompanyById(String id);
	
	/**
     * @api getCompanyList 根据company条件查询列表
     * @apiGroup company管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Company} company  实体条件
     * @apiSuccess List<Company> company实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Company> getCompanyList(Company company);
	

	/**
     * @api getCompanyListByParam 根据company条件查询列表（含排序）
     * @apiGroup company管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Company} company  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Company> company实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Company> getCompanyListByParam(Company company, String orderByStr);
	
	
	/**
     * @api findCompanyPageByParam 根据company条件查询列表（分页）
     * @apiGroup company管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Company} company  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Company> company实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Company> findCompanyPageByParam(Company company, PageParam pageParam); 
	
	/**
     * @api findCompanyPageByParam 根据company条件查询列表（分页，含排序）
     * @apiGroup company管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Company} company  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Company> company实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Company> findCompanyPageByParam(Company company, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyById 根据company的id删除(物理删除)
     * @apiGroup company管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyById(String id);

}

