package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Company implements Serializable {
    private String id;

    /**
     * 中文名
     */
    private String cnName;

    /**
     * 中文简称
     */
    private String cnShort;

    /**
     * 英文名
     */
    private String enName;

    /**
     * 英文简称
     */
    private String enShort;

    /**
     * 统一信用代码
     */
    private String creditCode;

    /**
     * 登记机关
     */
    private String regInstitute;

    /**
     * 公司规模
     */
    private String scale;

    /**
     * organize_code
     */
    private String organizeCode;

    /**
     * 工商注册号
     */
    private String registrationNumber;

    /**
     * 法人
     */
    private String legalPerson;

    /**
     * 公司类型 如:有限责任公司(自然人投资或控股)
     */
    private String companyType;

    /**
     * 营业期限开始
     */
    private Date estiblishTime;

    /**
     * 营业期限截止
     */
    private Date toTime;

    /**
     * 发照日期
     */
    private Date checkDate;

    /**
     * 国家/省/市
     */
    private Byte areaType;

    /**
     * 企业地址 
     */
    private String regLocation;

    /**
     * 经营范围
     */
    private String businessScope;

    /**
     * 标签
     */
    private Byte tags;

    /**
     * 网址
     */
    private String webUrl;

    /**
     * logo
     */
    private String companyLogo;

    /**
     * 中文简介
     */
    private String cnDesc;

    /**
     * 成立时间
     */
    private Date beginTime;

    /**
     * 停业时间
     */
    private Date stopTime;

    /**
     * 注册资本
     */
    private BigDecimal registeredCapital;

    /**
     * 注册资本货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte currencyType;

    /**
     * 商业状态
2：已上市
3：停业
4：非上市
     */
    private Byte commerceStatus;

    private String remark;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName == null ? null : cnName.trim();
    }

    public String getCnShort() {
        return cnShort;
    }

    public void setCnShort(String cnShort) {
        this.cnShort = cnShort == null ? null : cnShort.trim();
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName == null ? null : enName.trim();
    }

    public String getEnShort() {
        return enShort;
    }

    public void setEnShort(String enShort) {
        this.enShort = enShort == null ? null : enShort.trim();
    }

    public String getCreditCode() {
        return creditCode;
    }

    public void setCreditCode(String creditCode) {
        this.creditCode = creditCode == null ? null : creditCode.trim();
    }

    public String getRegInstitute() {
        return regInstitute;
    }

    public void setRegInstitute(String regInstitute) {
        this.regInstitute = regInstitute == null ? null : regInstitute.trim();
    }

    public String getScale() {
        return scale;
    }

    public void setScale(String scale) {
        this.scale = scale == null ? null : scale.trim();
    }

    public String getOrganizeCode() {
        return organizeCode;
    }

    public void setOrganizeCode(String organizeCode) {
        this.organizeCode = organizeCode == null ? null : organizeCode.trim();
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber == null ? null : registrationNumber.trim();
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson == null ? null : legalPerson.trim();
    }

    public String getCompanyType() {
        return companyType;
    }

    public void setCompanyType(String companyType) {
        this.companyType = companyType == null ? null : companyType.trim();
    }

    public Date getEstiblishTime() {
        return estiblishTime;
    }

    public void setEstiblishTime(Date estiblishTime) {
        this.estiblishTime = estiblishTime;
    }

    public Date getToTime() {
        return toTime;
    }

    public void setToTime(Date toTime) {
        this.toTime = toTime;
    }

    public Date getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(Date checkDate) {
        this.checkDate = checkDate;
    }

    public Byte getAreaType() {
        return areaType;
    }

    public void setAreaType(Byte areaType) {
        this.areaType = areaType;
    }

    public String getRegLocation() {
        return regLocation;
    }

    public void setRegLocation(String regLocation) {
        this.regLocation = regLocation == null ? null : regLocation.trim();
    }

    public String getBusinessScope() {
        return businessScope;
    }

    public void setBusinessScope(String businessScope) {
        this.businessScope = businessScope == null ? null : businessScope.trim();
    }

    public Byte getTags() {
        return tags;
    }

    public void setTags(Byte tags) {
        this.tags = tags;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl == null ? null : webUrl.trim();
    }

    public String getCompanyLogo() {
        return companyLogo;
    }

    public void setCompanyLogo(String companyLogo) {
        this.companyLogo = companyLogo == null ? null : companyLogo.trim();
    }

    public String getCnDesc() {
        return cnDesc;
    }

    public void setCnDesc(String cnDesc) {
        this.cnDesc = cnDesc == null ? null : cnDesc.trim();
    }

    public Date getBeginTime() {
        return beginTime;
    }

    public void setBeginTime(Date beginTime) {
        this.beginTime = beginTime;
    }

    public Date getStopTime() {
        return stopTime;
    }

    public void setStopTime(Date stopTime) {
        this.stopTime = stopTime;
    }

    public BigDecimal getRegisteredCapital() {
        return registeredCapital;
    }

    public void setRegisteredCapital(BigDecimal registeredCapital) {
        this.registeredCapital = registeredCapital;
    }

    public Byte getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(Byte currencyType) {
        this.currencyType = currencyType;
    }

    public Byte getCommerceStatus() {
        return commerceStatus;
    }

    public void setCommerceStatus(Byte commerceStatus) {
        this.commerceStatus = commerceStatus;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", cnName=").append(cnName);
        sb.append(", cnShort=").append(cnShort);
        sb.append(", enName=").append(enName);
        sb.append(", enShort=").append(enShort);
        sb.append(", creditCode=").append(creditCode);
        sb.append(", regInstitute=").append(regInstitute);
        sb.append(", scale=").append(scale);
        sb.append(", organizeCode=").append(organizeCode);
        sb.append(", registrationNumber=").append(registrationNumber);
        sb.append(", legalPerson=").append(legalPerson);
        sb.append(", companyType=").append(companyType);
        sb.append(", estiblishTime=").append(estiblishTime);
        sb.append(", toTime=").append(toTime);
        sb.append(", checkDate=").append(checkDate);
        sb.append(", areaType=").append(areaType);
        sb.append(", regLocation=").append(regLocation);
        sb.append(", businessScope=").append(businessScope);
        sb.append(", tags=").append(tags);
        sb.append(", webUrl=").append(webUrl);
        sb.append(", companyLogo=").append(companyLogo);
        sb.append(", cnDesc=").append(cnDesc);
        sb.append(", beginTime=").append(beginTime);
        sb.append(", stopTime=").append(stopTime);
        sb.append(", registeredCapital=").append(registeredCapital);
        sb.append(", currencyType=").append(currencyType);
        sb.append(", commerceStatus=").append(commerceStatus);
        sb.append(", remark=").append(remark);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", status=").append(status);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}