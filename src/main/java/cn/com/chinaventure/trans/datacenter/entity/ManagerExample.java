package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ManagerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public ManagerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andContextIdIsNull() {
            addCriterion("context_id is null");
            return (Criteria) this;
        }

        public Criteria andContextIdIsNotNull() {
            addCriterion("context_id is not null");
            return (Criteria) this;
        }

        public Criteria andContextIdEqualTo(String value) {
            addCriterion("context_id =", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdNotEqualTo(String value) {
            addCriterion("context_id <>", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdGreaterThan(String value) {
            addCriterion("context_id >", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdGreaterThanOrEqualTo(String value) {
            addCriterion("context_id >=", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdLessThan(String value) {
            addCriterion("context_id <", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdLessThanOrEqualTo(String value) {
            addCriterion("context_id <=", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdLike(String value) {
            addCriterion("context_id like", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdNotLike(String value) {
            addCriterion("context_id not like", value, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdIn(List<String> values) {
            addCriterion("context_id in", values, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdNotIn(List<String> values) {
            addCriterion("context_id not in", values, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdBetween(String value1, String value2) {
            addCriterion("context_id between", value1, value2, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextIdNotBetween(String value1, String value2) {
            addCriterion("context_id not between", value1, value2, "contextId");
            return (Criteria) this;
        }

        public Criteria andContextTypeIsNull() {
            addCriterion("context_type is null");
            return (Criteria) this;
        }

        public Criteria andContextTypeIsNotNull() {
            addCriterion("context_type is not null");
            return (Criteria) this;
        }

        public Criteria andContextTypeEqualTo(Byte value) {
            addCriterion("context_type =", value, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeNotEqualTo(Byte value) {
            addCriterion("context_type <>", value, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeGreaterThan(Byte value) {
            addCriterion("context_type >", value, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("context_type >=", value, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeLessThan(Byte value) {
            addCriterion("context_type <", value, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeLessThanOrEqualTo(Byte value) {
            addCriterion("context_type <=", value, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeIn(List<Byte> values) {
            addCriterion("context_type in", values, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeNotIn(List<Byte> values) {
            addCriterion("context_type not in", values, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeBetween(Byte value1, Byte value2) {
            addCriterion("context_type between", value1, value2, "contextType");
            return (Criteria) this;
        }

        public Criteria andContextTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("context_type not between", value1, value2, "contextType");
            return (Criteria) this;
        }

        public Criteria andManageTypeIsNull() {
            addCriterion("manage_type is null");
            return (Criteria) this;
        }

        public Criteria andManageTypeIsNotNull() {
            addCriterion("manage_type is not null");
            return (Criteria) this;
        }

        public Criteria andManageTypeEqualTo(Byte value) {
            addCriterion("manage_type =", value, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeNotEqualTo(Byte value) {
            addCriterion("manage_type <>", value, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeGreaterThan(Byte value) {
            addCriterion("manage_type >", value, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("manage_type >=", value, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeLessThan(Byte value) {
            addCriterion("manage_type <", value, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeLessThanOrEqualTo(Byte value) {
            addCriterion("manage_type <=", value, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeIn(List<Byte> values) {
            addCriterion("manage_type in", values, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeNotIn(List<Byte> values) {
            addCriterion("manage_type not in", values, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeBetween(Byte value1, Byte value2) {
            addCriterion("manage_type between", value1, value2, "manageType");
            return (Criteria) this;
        }

        public Criteria andManageTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("manage_type not between", value1, value2, "manageType");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("personage_id is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(String value) {
            addCriterion("personage_id =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(String value) {
            addCriterion("personage_id <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(String value) {
            addCriterion("personage_id >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("personage_id >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(String value) {
            addCriterion("personage_id <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("personage_id <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLike(String value) {
            addCriterion("personage_id like", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotLike(String value) {
            addCriterion("personage_id not like", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<String> values) {
            addCriterion("personage_id in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<String> values) {
            addCriterion("personage_id not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(String value1, String value2) {
            addCriterion("personage_id between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(String value1, String value2) {
            addCriterion("personage_id not between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andTitleIsNull() {
            addCriterion("title is null");
            return (Criteria) this;
        }

        public Criteria andTitleIsNotNull() {
            addCriterion("title is not null");
            return (Criteria) this;
        }

        public Criteria andTitleEqualTo(Byte value) {
            addCriterion("title =", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotEqualTo(Byte value) {
            addCriterion("title <>", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThan(Byte value) {
            addCriterion("title >", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleGreaterThanOrEqualTo(Byte value) {
            addCriterion("title >=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThan(Byte value) {
            addCriterion("title <", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleLessThanOrEqualTo(Byte value) {
            addCriterion("title <=", value, "title");
            return (Criteria) this;
        }

        public Criteria andTitleIn(List<Byte> values) {
            addCriterion("title in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotIn(List<Byte> values) {
            addCriterion("title not in", values, "title");
            return (Criteria) this;
        }

        public Criteria andTitleBetween(Byte value1, Byte value2) {
            addCriterion("title between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andTitleNotBetween(Byte value1, Byte value2) {
            addCriterion("title not between", value1, value2, "title");
            return (Criteria) this;
        }

        public Criteria andEntryTimeIsNull() {
            addCriterion("entry_time is null");
            return (Criteria) this;
        }

        public Criteria andEntryTimeIsNotNull() {
            addCriterion("entry_time is not null");
            return (Criteria) this;
        }

        public Criteria andEntryTimeEqualTo(Date value) {
            addCriterion("entry_time =", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeNotEqualTo(Date value) {
            addCriterion("entry_time <>", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeGreaterThan(Date value) {
            addCriterion("entry_time >", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("entry_time >=", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeLessThan(Date value) {
            addCriterion("entry_time <", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeLessThanOrEqualTo(Date value) {
            addCriterion("entry_time <=", value, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeIn(List<Date> values) {
            addCriterion("entry_time in", values, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeNotIn(List<Date> values) {
            addCriterion("entry_time not in", values, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeBetween(Date value1, Date value2) {
            addCriterion("entry_time between", value1, value2, "entryTime");
            return (Criteria) this;
        }

        public Criteria andEntryTimeNotBetween(Date value1, Date value2) {
            addCriterion("entry_time not between", value1, value2, "entryTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeIsNull() {
            addCriterion("dimission_type is null");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeIsNotNull() {
            addCriterion("dimission_type is not null");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeEqualTo(Byte value) {
            addCriterion("dimission_type =", value, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeNotEqualTo(Byte value) {
            addCriterion("dimission_type <>", value, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeGreaterThan(Byte value) {
            addCriterion("dimission_type >", value, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("dimission_type >=", value, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeLessThan(Byte value) {
            addCriterion("dimission_type <", value, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeLessThanOrEqualTo(Byte value) {
            addCriterion("dimission_type <=", value, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeIn(List<Byte> values) {
            addCriterion("dimission_type in", values, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeNotIn(List<Byte> values) {
            addCriterion("dimission_type not in", values, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeBetween(Byte value1, Byte value2) {
            addCriterion("dimission_type between", value1, value2, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("dimission_type not between", value1, value2, "dimissionType");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeIsNull() {
            addCriterion("dimission_time is null");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeIsNotNull() {
            addCriterion("dimission_time is not null");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeEqualTo(Date value) {
            addCriterion("dimission_time =", value, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeNotEqualTo(Date value) {
            addCriterion("dimission_time <>", value, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeGreaterThan(Date value) {
            addCriterion("dimission_time >", value, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("dimission_time >=", value, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeLessThan(Date value) {
            addCriterion("dimission_time <", value, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeLessThanOrEqualTo(Date value) {
            addCriterion("dimission_time <=", value, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeIn(List<Date> values) {
            addCriterion("dimission_time in", values, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeNotIn(List<Date> values) {
            addCriterion("dimission_time not in", values, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeBetween(Date value1, Date value2) {
            addCriterion("dimission_time between", value1, value2, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andDimissionTimeNotBetween(Date value1, Date value2) {
            addCriterion("dimission_time not between", value1, value2, "dimissionTime");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyIsNull() {
            addCriterion("holding_total_money is null");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyIsNotNull() {
            addCriterion("holding_total_money is not null");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyEqualTo(BigDecimal value) {
            addCriterion("holding_total_money =", value, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyNotEqualTo(BigDecimal value) {
            addCriterion("holding_total_money <>", value, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyGreaterThan(BigDecimal value) {
            addCriterion("holding_total_money >", value, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("holding_total_money >=", value, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyLessThan(BigDecimal value) {
            addCriterion("holding_total_money <", value, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("holding_total_money <=", value, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyIn(List<BigDecimal> values) {
            addCriterion("holding_total_money in", values, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyNotIn(List<BigDecimal> values) {
            addCriterion("holding_total_money not in", values, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("holding_total_money between", value1, value2, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingTotalMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("holding_total_money not between", value1, value2, "holdingTotalMoney");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountIsNull() {
            addCriterion("holding_amount is null");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountIsNotNull() {
            addCriterion("holding_amount is not null");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountEqualTo(Integer value) {
            addCriterion("holding_amount =", value, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountNotEqualTo(Integer value) {
            addCriterion("holding_amount <>", value, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountGreaterThan(Integer value) {
            addCriterion("holding_amount >", value, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountGreaterThanOrEqualTo(Integer value) {
            addCriterion("holding_amount >=", value, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountLessThan(Integer value) {
            addCriterion("holding_amount <", value, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountLessThanOrEqualTo(Integer value) {
            addCriterion("holding_amount <=", value, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountIn(List<Integer> values) {
            addCriterion("holding_amount in", values, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountNotIn(List<Integer> values) {
            addCriterion("holding_amount not in", values, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountBetween(Integer value1, Integer value2) {
            addCriterion("holding_amount between", value1, value2, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andHoldingAmountNotBetween(Integer value1, Integer value2) {
            addCriterion("holding_amount not between", value1, value2, "holdingAmount");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryIsNull() {
            addCriterion("annual_salary is null");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryIsNotNull() {
            addCriterion("annual_salary is not null");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryEqualTo(BigDecimal value) {
            addCriterion("annual_salary =", value, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryNotEqualTo(BigDecimal value) {
            addCriterion("annual_salary <>", value, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryGreaterThan(BigDecimal value) {
            addCriterion("annual_salary >", value, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("annual_salary >=", value, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryLessThan(BigDecimal value) {
            addCriterion("annual_salary <", value, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryLessThanOrEqualTo(BigDecimal value) {
            addCriterion("annual_salary <=", value, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryIn(List<BigDecimal> values) {
            addCriterion("annual_salary in", values, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryNotIn(List<BigDecimal> values) {
            addCriterion("annual_salary not in", values, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("annual_salary between", value1, value2, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andAnnualSalaryNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("annual_salary not between", value1, value2, "annualSalary");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}