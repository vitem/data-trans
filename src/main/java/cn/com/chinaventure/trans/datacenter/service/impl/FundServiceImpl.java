package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.FundService;
import cn.com.chinaventure.trans.datacenter.entity.Fund;
import cn.com.chinaventure.trans.datacenter.entity.FundExample;
import cn.com.chinaventure.trans.datacenter.entity.FundExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.FundMapper;


@Service("fundService")
public class FundServiceImpl implements FundService {

	private final static Logger logger = LoggerFactory.getLogger(FundServiceImpl.class);

	@Resource
	private FundMapper fundMapper;


	@Override
	public void saveFund(Fund fund) {
		if (null == fund) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.fundMapper.insertSelective(fund);
	}

	@Override
	public void updateFund(Fund fund) {
		if (null == fund) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.fundMapper.updateByPrimaryKeySelective(fund);
	}

	@Override
	public Fund getFundById(String id) {
		return fundMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Fund> getFundList(Fund fund) {
		return this.getFundListByParam(fund, null);
	}

	@Override
	public List<Fund> getFundListByParam(Fund fund, String orderByStr) {
		FundExample example = new FundExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Fund> fundList = this.fundMapper.selectByExample(example);
		return fundList;
	}

	@Override
	public PageResult<Fund> findFundPageByParam(Fund fund, PageParam pageParam) {
		return this.findFundPageByParam(fund, pageParam, null);
	}

	@Override
	public PageResult<Fund> findFundPageByParam(Fund fund, PageParam pageParam, String orderByStr) {
		PageResult<Fund> pageResult = new PageResult<Fund>();

		FundExample example = new FundExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Fund> list = null;// fundMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteFundById(String id) {
		this.fundMapper.deleteByPrimaryKey(id);
	}

}
