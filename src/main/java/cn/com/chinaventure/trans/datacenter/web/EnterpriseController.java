package cn.com.chinaventure.trans.datacenter.web;

import cn.com.chinaventure.common.constant.CommonStatic;
import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.util.DateTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCV;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_contact;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_info;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteam;
import cn.com.chinaventure.trans.cvent.service.*;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonage;
import cn.com.chinaventure.trans.cvsource.entity.SourcePosition;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageService;
import cn.com.chinaventure.trans.cvsource.service.SourcePositionService;
import cn.com.chinaventure.trans.datacenter.entity.*;
import cn.com.chinaventure.trans.datacenter.service.*;
import com.alibaba.fastjson.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@RestController
@RequestMapping("/enterprise")
public class EnterpriseController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EnterpriseController.class);

    @Resource
    private EnterpriseService enterpriseService;

    @Resource
    private EnterpriseCVService enterpriseCVService;

    @Resource
    private CompanyService companyService;

    @Resource
    private ContactService contactService;

    @Resource
    private ManagerService managerService;

    @Resource
    private Enterprise_manageteamService enterpriseManageteamService;

    @Resource
    private Enterprise_credit_infoService enterpriseCreditInfoService;

    @Resource
    private Enterprise_contactService enterpriseContactService;

    @Resource
    private SourcePositionService sourcePositionService;

    @Resource
    private SourcePersonageService sourcePersonageService;

    @Resource
    private Enterprise_credit_investorService enterpriseCreditInvestorService;

    @Resource
    private PersonageService personageService;

    private final int pageSize = 100;

    private final long index = 0;



    @RequestMapping(value="/init" ,method=RequestMethod.POST)
    public JsonResult<Object> init(@RequestBody JSONObject jsonObject) {
        JsonResult<Object> jr = new JsonResult<Object>();
        logger.info("time begin -- > " + DateTools.getCurrentDateTimeString());
        try {
            if(null != jsonObject && null !=jsonObject.getInteger("id")){
                EnterpriseCV enterpriseCV = enterpriseCVService.getEnterpriseCVById(jsonObject.getInteger("id"));
                transEnterpriseRelation(enterpriseCV);
            }else{
                int count = enterpriseCVService.countEnterpriseCV();

//                ExecutorService executor = Executors.newFixedThreadPool(100);
//                executor.submit((index) -> {
//                    String threadName = Thread.currentThread().getName();
//
//                });
//
//                Runnable<> task = (enterpriseCV) -> {
//                    try {
//                        TimeUnit.SECONDS.sleep(1);
//
//                        Integer enterpriseId = enterpriseCV.get();
//                        Date date = new Date();
//                        logger.info("id = {} , time begin -- > " + DateTools.getDateTimeString(date),enterpriseId);
//                        transEnterpriseRelation(enterpriseCV);
//                        logger.info("======== time end {} , expire = {} " , DateTools.getCurrentDateTimeString(),DateTools.getCurrentDateTimeDiffString(date));
//                        logger.info(" -------------------------------------------------------");
//                    }
//                    catch (InterruptedException e) {
//                        throw new IllegalStateException("task interrupted", e);
//                    }
//                };


                int size = count % pageSize==0? count/pageSize:count/pageSize+1;
                for (int pageNo = 1; pageNo < size ; pageNo++) {

                    PageParam pageParam = new PageParam();
                    pageParam.setPageNo(pageNo);
                    pageParam.setPageSize(pageSize);
                    PageResult<EnterpriseCV> pageResult = enterpriseCVService.findEnterpriseCVPageByParam(new EnterpriseCV(),pageParam);

                    for (EnterpriseCV enterpriseCV : pageResult.getList()) {
                        Integer enterpriseId = enterpriseCV.getEnterpriseId();
                        Date date = new Date();
                        logger.info("id = {} , time begin -- > " + DateTools.getDateTimeString(date),enterpriseId);
                        transEnterpriseRelation(enterpriseCV);
                        logger.info("======== time end {} , expire = {} " , DateTools.getCurrentDateTimeString(),DateTools.getCurrentDateTimeDiffString(date));
                        logger.info(" -------------------------------------------------------");
                    }
                }
            }

            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        logger.info("time ent -- > " + DateTools.getCurrentDateTimeString());
        return jr;
    }

    private void transEnterpriseRelation( EnterpriseCV enterpriseCV) {



        //EnterpriseCVWithBLOBs enterpriseCV = enterpriseCVService.getEnterpriseCVById(Integer.valueOf(id));
        Integer cvEnterpriseId = enterpriseCV.getEnterpriseId();


        Company company = new Company();

        Enterprise enterprise = new Enterprise();
        String enterpriseId = StringUtil.getUUID32();
        enterprise.setId(enterpriseId);

        Enterprise_credit_info enterpriseCreditInfo = enterpriseCreditInfoService.getByEnterpriseId((long)cvEnterpriseId);

        if(enterpriseCreditInfo!=null){
            company = transCompanyByCredit(enterpriseCreditInfo, company);
            enterprise = transEnpByCredit(enterpriseCreditInfo, enterprise);
        }
        company = transCompanyByCVEnp(enterpriseCV, company);

        enterprise.setCompanyId(company.getId());

        enterprise = transEnpByCVEnp(enterpriseCV, enterprise);

        //联系人
        Enterprise_contact enterpriseContact = new Enterprise_contact();
        enterpriseContact.setEnterpriseid((long)cvEnterpriseId);
        List<Enterprise_contact> cvEnterpriseContactList = enterpriseContactService.getEnterprise_contactListByParam(enterpriseContact,null);
        List<Contact> contacts = new LinkedList<>();
        if(cvEnterpriseContactList.size()>0){
            contacts = transContactByCVContact(enterpriseId,cvEnterpriseContactList);
        }

        //管理人员
        List<Manager> managerList = transCVManagerTeam(enterpriseId,cvEnterpriseId);

        try{
            companyService.saveCompany(company);
            enterpriseService.saveEnterprise(enterprise);
            if(contacts.size()>0){
                contacts.parallelStream().forEach((contact) -> contactService.saveContact(contact));
            }
            for (Manager manager : managerList) {
                Personage personage = manager.getPersonage();
                personageService.savePersonage(personage);
                managerService.saveManager(manager);
            }
        }catch (Exception e){
            e.printStackTrace();
            logger.error("message " ,e.getStackTrace());
        }


    }


    /**
     * 管理团队
     * @param enterpriseId
     * @param cvEnterpriseId
     * @return
     */
    private List<Manager> transCVManagerTeam(String enterpriseId, Integer cvEnterpriseId) {
        Enterprise_manageteam enterpriseManageteam  = new Enterprise_manageteam();
        enterpriseManageteam.setEnterpriseId((long)cvEnterpriseId);
        List<Manager> managers = new LinkedList<>();
        //enterpriseManageteam.setEnterpriseId((long)cvEnterpriseId);
        List<Enterprise_manageteam> cvEnterpriseManageteamList = enterpriseManageteamService.getEnterprise_manageteamList(enterpriseManageteam);

        for (Enterprise_manageteam cvManageTeam : cvEnterpriseManageteamList) {

            Long cvPersonageId = cvManageTeam.getEnterpriseMtPersonageid();

            if(cvPersonageId==null){
                continue;
            }

            Manager manager = new Manager();
            manager.setId(StringUtil.getUUID32());

            if(CommonStatic.POSITION_MAP_ID==null){
                List<SourcePosition> positionList = sourcePositionService.getSourcePositionList(null);
                Map<Long,Object> idMap = new HashMap<Long, Object>();
                Map<String,Object> nameMap = new HashMap<String, Object>();

                for (SourcePosition sourcePosition : positionList) {
                    idMap.put(sourcePosition.getPositionId(),sourcePosition);
                    nameMap.put(sourcePosition.getPositionCnName(),sourcePosition);
                }
                CommonStatic.POSITION_MAP_ID = idMap;
                CommonStatic.POSITION_MAP_NAME = nameMap;
            }

            manager.setContextType(CommonStatic.BYTE_1);
            manager.setContextId(enterpriseId);
            manager.setCreateTime(cvManageTeam.getCreateTime());
            Long dimissionType = cvManageTeam.getEnterpriseMtLeaveoffice()==null?2:cvManageTeam.getEnterpriseMtLeaveoffice();
            manager.setDimissionType(dimissionType.byteValue());
            manager.setDimissionTime(cvManageTeam.getEnterpriseMtLeaveofficedate());
            manager.setModifyTime(cvManageTeam.getUpdateTime());
            manager.setTitle(cvManageTeam.getEnterpriseMtPosition());
            manager.setEntryTime(cvManageTeam.getEnterpriseMtEnterdutydate());

            manager.setExt1("enterprise_manageteam_id:"+cvManageTeam.getEnterpriseMtId());
            manager.setExt2("enp_id:"+cvManageTeam.getEnterpriseId());

            manager.setStatus(CommonStatic.STATUS);

            Personage personage = transPersonage(cvPersonageId);
            manager.setPersonageId(personage.getId());
            manager.setPersonage(personage);

            managers.add(manager);

        }

        return managers;


    }

    /**
     * 个人
     * @param cvPersonageId
     * @return
     */
    private Personage transPersonage(Long cvPersonageId) {

        SourcePersonage sourcePersonage = sourcePersonageService.getSourcePersonageById(cvPersonageId.longValue());
        Personage personage = new Personage();
        personage.setId(StringUtil.getUUID32());
        Long personageStatus = sourcePersonage.getPersonageStatus();
        personageStatus = personageStatus==null? CommonStatic.LONG_1:personageStatus;
        personage.setStatus(personageStatus.byteValue());
        personage.setTelephone(sourcePersonage.getPersonageTelephone());
        personage.setEmail(sourcePersonage.getPersonageMail());
        personage.setCnDescription(sourcePersonage.getPersonageCnDesc());
        personage.setEnDescription(sourcePersonage.getPersonageEnDesc());
        personage.setCnName(sourcePersonage.getPersonageCnName());
        personage.setEnName(sourcePersonage.getPersonageEnName());
        personage.setBirthday(sourcePersonage.getPersonageBirthdate());
        personage.setCreateTime(sourcePersonage.getCreateTime());
        personage.setModifyTime(sourcePersonage.getUpdateTime());
        personage.setGlory(sourcePersonage.getPersonageGlory());
        //personage.setCreaterId();
        personage.setCountry(sourcePersonage.getPersonageAreaCountry()==null?1:sourcePersonage.getPersonageAreaCountry().byteValue());
        //personage.setProvince(sourcePersonage.getPersonageAreaCountry()==null?1:sourcePersonage.getPersonageAreaCountry().byteValue());
        personage.setExt1("cvsource.tbl_personage:"+sourcePersonage.getPersonageId());
        personage.setSex(sourcePersonage.getPersonageSex()==null?null:sourcePersonage.getPersonageSex().byteValue());
        return personage;
    }

    /**
     * 联系人
     * @param enterPriseId
     * @param cvEnterpriseContactList
     * @return
     */
    private List<Contact> transContactByCVContact(String enterPriseId,List<Enterprise_contact> cvEnterpriseContactList) {

        List<Contact> contacts = new LinkedList<>();

        for (Enterprise_contact cvEnterpriseContact : cvEnterpriseContactList) {

            Contact contact = new Contact();
            contact.setId(StringUtil.getUUID32());

            contact.setStatus(CommonStatic.STATUS);
            contact.setContextId(enterPriseId);
            contact.setContextType(CommonStatic.BYTE_1);

            contact.setCnAddress(cvEnterpriseContact.getEpCnAddress());
            contact.setEnAddress(cvEnterpriseContact.getEpEnAddress());
            contact.setCnPerson(cvEnterpriseContact.getEpCnPerson());
            contact.setEnPerson(cvEnterpriseContact.getEpEnPerson());
            contact.setPostCode(cvEnterpriseContact.getEpFax());
            contact.setTelephone(cvEnterpriseContact.getEpTelephone());
            contact.setFax(cvEnterpriseContact.getEpFax());
            contact.setEmail(cvEnterpriseContact.getEpMail());
            contact.setOrdered(cvEnterpriseContact.getOrdered()==null?1:cvEnterpriseContact.getOrdered().intValue());

            contact.setCreaterId(cvEnterpriseContact.getBehavior());
            contact.setCreateTime(cvEnterpriseContact.getCreateTime());
            contact.setModifyTime(cvEnterpriseContact.getUpdateTime());
            contact.setModifierId(cvEnterpriseContact.getBehavior());

            contact.setExt1(cvEnterpriseContact.getEpContactId()+"");
            contacts.add(contact);
        }

        return contacts;
    }


    /**
     * 旧企业到新企业数据
     * @param enterpriseCV
     * @param enterprise
     * @return
     */
    private Enterprise transEnpByCVEnp(EnterpriseCV enterpriseCV, Enterprise enterprise) {


        enterprise.setName(enterpriseCV.getEnterpriseCnName());
        enterprise.setCreateTime(enterpriseCV.getCreateTime());

        String products = enterpriseCV.getMainProductOneNew()+" "+enterpriseCV.getMainProductTwoNew()+" "+enterpriseCV.getMainProductThreeNew();
        enterprise.setMainProduct(products);
        //enterprise.setIndustryType();
        enterpriseCV.getEnterprisestageStatus();//------------
        enterpriseCV.getEnterprisestageId();//------------
        enterprise.setStatus(CommonStatic.STATUS);
        enterprise.setCreaterId(CommonStatic.CREATOR_ID);
        enterprise.setExt1(enterpriseCV.getEnterpriseId()+"");

        return  enterprise;

    }

    /**
     * 旧的工商数据表抽取
     * @param enterpriseCreditInfo
     * @param enterprise
     * @return
     */
    private Enterprise transEnpByCredit(Enterprise_credit_info enterpriseCreditInfo, Enterprise enterprise) {
        //注册类型
        String registerType = enterpriseCreditInfo.getRegisterType();
        enterprise.setEnpRegisterType(CommonStatic.dictRegisterMap.get(registerType));
        enterprise.setMainBusiness(enterpriseCreditInfo.getMainBusiness());
        return enterprise;
    }

    /**
     * 企业中数据抽取成工商数据
     * @param enterpriseCV
     * @param company
     * @return
     */
    private Company transCompanyByCVEnp(EnterpriseCV enterpriseCV, Company company) {

        company.setId(StringUtil.getUUID32());
        company.setCnName(enterpriseCV.getEnterpriseCnName());
        company.setEnName(enterpriseCV.getEnterpriseEnName());
        company.setCnShort(enterpriseCV.getEnterpriseCnShort());
        company.setEnShort(enterpriseCV.getEnterpriseEnShort());
        company.setCnDesc(enterpriseCV.getEnterpriseCnDesc());

        company.setWebUrl(enterpriseCV.getEnterpriseWeb());
        //logo
        company.setCompanyLogo(enterpriseCV.getEnterpriseLogo());
        //开始时间
        company.setBeginTime(company.getBeginTime()==null?enterpriseCV.getEnterpriseSetuptime():company.getBeginTime());
        //停业时间
        company.setStopTime(enterpriseCV.getEnterpriseStoptime());
        //法人
        company.setLegalPerson(company.getLegalPerson()==null?enterpriseCV.getLegalPerson():company.getLegalPerson());
        //机构代码
        company.setOrganizeCode(enterpriseCV.getOrganizeCode());
        //商业状态
        company.setCommerceStatus(enterpriseCV.getCommercestatusId());

        company.setCreateTime(enterpriseCV.getCreateTime());
        company.setStatus(CommonStatic.STATUS);
        company.setCreaterId(CommonStatic.CREATOR_ID);
        company.setCreateTime(enterpriseCV.getCreateTime());
        company.setExt2(enterpriseCV.getEnterpriseId()+"");

        return company;

    }


    /**
     * 转换company
     * @param enterpriseCreditInfo
     * @param company
     * @return
     */
    private Company  transCompanyByCredit(Enterprise_credit_info enterpriseCreditInfo, Company company) {
        //注册号码
        company.setRegistrationNumber(enterpriseCreditInfo.getRegisterNum());
        //注册名称
        company.setCnName(enterpriseCreditInfo.getRegisterName());

        //成立时间
        company.setBeginTime(enterpriseCreditInfo.getSetupdate());
        //法人
        company.setLegalPerson(enterpriseCreditInfo.getLegalperson());
        //注册资本
        Double registermoney = enterpriseCreditInfo.getRegistermoney();
        registermoney = registermoney==null?0:registermoney;
        registermoney = registermoney.intValue()==0?0:registermoney*10000;
        company.setRegisteredCapital(new BigDecimal(registermoney));
        company.setCurrencyType(CommonStatic.currencyTypeMap.get(enterpriseCreditInfo.getRegistermoneyCurrency()));
        //注册地址
        company.setRegLocation(enterpriseCreditInfo.getRegisterAddress());
        //主营
        company.setBusinessScope(enterpriseCreditInfo.getMainBusiness());
        //营业开始结束
        company.setEstiblishTime(enterpriseCreditInfo.getOpenstartDate());
        company.setToTime(enterpriseCreditInfo.getOpenendDate());
        //登记机关
        company.setRegInstitute(enterpriseCreditInfo.getRegisterOrgan());
        //发照时间
        company.setCheckDate(enterpriseCreditInfo.getLicensedate());
        company.setBusinessScope(enterpriseCreditInfo.getMainBusiness());

        company.setCreateTime(enterpriseCreditInfo.getCreateTime());
        company.setStatus(CommonStatic.STATUS);

        company.setExt1(enterpriseCreditInfo.getCreditId()+"");

        return company;
    }

    public static void main(String[] strings){
        ExecutorService executor = Executors.newFixedThreadPool(100);
        executor.submit(() -> {
            String threadName = Thread.currentThread().getName();

        });
    }


}
