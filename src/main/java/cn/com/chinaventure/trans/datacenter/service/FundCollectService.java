package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.FundCollect;

/**
 * @author 
 *
 */
public interface FundCollectService {
	
	/**
     * @api saveFundCollect 新增fundCollect
     * @apiGroup fundCollect管理
     * @apiName  新增fundCollect记录
     * @apiDescription 全量插入fundCollect记录
     * @apiParam {FundCollect} fundCollect fundCollect对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveFundCollect(FundCollect fundCollect);
	
	/**
     * @api updateFundCollect 修改fundCollect
     * @apiGroup fundCollect管理
     * @apiName  修改fundCollect记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {FundCollect} fundCollect fundCollect对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateFundCollect(FundCollect fundCollect);
	
	/**
     * @api getFundCollectById 根据fundCollectid查询详情
     * @apiGroup fundCollect管理
     * @apiName  查询fundCollect详情
     * @apiDescription 根据主键id查询fundCollect详情
     * @apiParam {String} id 主键id
     * @apiSuccess FundCollect fundCollect实体对象
     * @apiVersion 1.0.0
     * 
     */
	public FundCollect getFundCollectById(String id);
	
	/**
     * @api getFundCollectList 根据fundCollect条件查询列表
     * @apiGroup fundCollect管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {FundCollect} fundCollect  实体条件
     * @apiSuccess List<FundCollect> fundCollect实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<FundCollect> getFundCollectList(FundCollect fundCollect);
	

	/**
     * @api getFundCollectListByParam 根据fundCollect条件查询列表（含排序）
     * @apiGroup fundCollect管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {FundCollect} fundCollect  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<FundCollect> fundCollect实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<FundCollect> getFundCollectListByParam(FundCollect fundCollect, String orderByStr);
	
	
	/**
     * @api findFundCollectPageByParam 根据fundCollect条件查询列表（分页）
     * @apiGroup fundCollect管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {FundCollect} fundCollect  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<FundCollect> fundCollect实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<FundCollect> findFundCollectPageByParam(FundCollect fundCollect, PageParam pageParam); 
	
	/**
     * @api findFundCollectPageByParam 根据fundCollect条件查询列表（分页，含排序）
     * @apiGroup fundCollect管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {FundCollect} fundCollect  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<FundCollect> fundCollect实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<FundCollect> findFundCollectPageByParam(FundCollect fundCollect, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteFundCollectById 根据fundCollect的id删除(物理删除)
     * @apiGroup fundCollect管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteFundCollectById(String id);

}

