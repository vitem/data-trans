package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyShareholderService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholder;
import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholderExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholderExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyShareholderMapper;


@Service("companyShareholderService")
public class CompanyShareholderServiceImpl implements CompanyShareholderService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyShareholderServiceImpl.class);

	@Resource
	private CompanyShareholderMapper companyShareholderMapper;


	@Override
	public void saveCompanyShareholder(CompanyShareholder companyShareholder) {
		if (null == companyShareholder) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyShareholderMapper.insertSelective(companyShareholder);
	}

	@Override
	public void updateCompanyShareholder(CompanyShareholder companyShareholder) {
		if (null == companyShareholder) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyShareholderMapper.updateByPrimaryKeySelective(companyShareholder);
	}

	@Override
	public CompanyShareholder getCompanyShareholderById(String id) {
		return companyShareholderMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyShareholder> getCompanyShareholderList(CompanyShareholder companyShareholder) {
		return this.getCompanyShareholderListByParam(companyShareholder, null);
	}

	@Override
	public List<CompanyShareholder> getCompanyShareholderListByParam(CompanyShareholder companyShareholder, String orderByStr) {
		CompanyShareholderExample example = new CompanyShareholderExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyShareholder> companyShareholderList = this.companyShareholderMapper.selectByExample(example);
		return companyShareholderList;
	}

	@Override
	public PageResult<CompanyShareholder> findCompanyShareholderPageByParam(CompanyShareholder companyShareholder, PageParam pageParam) {
		return this.findCompanyShareholderPageByParam(companyShareholder, pageParam, null);
	}

	@Override
	public PageResult<CompanyShareholder> findCompanyShareholderPageByParam(CompanyShareholder companyShareholder, PageParam pageParam, String orderByStr) {
		PageResult<CompanyShareholder> pageResult = new PageResult<CompanyShareholder>();

		CompanyShareholderExample example = new CompanyShareholderExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyShareholder> list = null;// companyShareholderMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyShareholderById(String id) {
		this.companyShareholderMapper.deleteByPrimaryKey(id);
	}

}
