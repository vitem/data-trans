package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChange;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockChangeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyStockChangeMapper {
    int countByExample(CompanyStockChangeExample example);

    int deleteByExample(CompanyStockChangeExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyStockChange record);

    int insertSelective(CompanyStockChange record);

    List<CompanyStockChange> selectByExample(CompanyStockChangeExample example);

    CompanyStockChange selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyStockChange record, @Param("example") CompanyStockChangeExample example);

    int updateByExample(@Param("record") CompanyStockChange record, @Param("example") CompanyStockChangeExample example);

    int updateByPrimaryKeySelective(CompanyStockChange record);

    int updateByPrimaryKey(CompanyStockChange record);
}