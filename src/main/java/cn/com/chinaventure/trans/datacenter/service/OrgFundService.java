package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.OrgFund;

/**
 * @author 
 *
 */
public interface OrgFundService {
	
	/**
     * @api saveOrgFund 新增orgFund
     * @apiGroup orgFund管理
     * @apiName  新增orgFund记录
     * @apiDescription 全量插入orgFund记录
     * @apiParam {OrgFund} orgFund orgFund对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveOrgFund(OrgFund orgFund);
	
	/**
     * @api updateOrgFund 修改orgFund
     * @apiGroup orgFund管理
     * @apiName  修改orgFund记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {OrgFund} orgFund orgFund对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateOrgFund(OrgFund orgFund);
	
	/**
     * @api getOrgFundById 根据orgFundid查询详情
     * @apiGroup orgFund管理
     * @apiName  查询orgFund详情
     * @apiDescription 根据主键id查询orgFund详情
     * @apiParam {String} id 主键id
     * @apiSuccess OrgFund orgFund实体对象
     * @apiVersion 1.0.0
     * 
     */
	public OrgFund getOrgFundById(String id);
	
	/**
     * @api getOrgFundList 根据orgFund条件查询列表
     * @apiGroup orgFund管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {OrgFund} orgFund  实体条件
     * @apiSuccess List<OrgFund> orgFund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<OrgFund> getOrgFundList(OrgFund orgFund);
	

	/**
     * @api getOrgFundListByParam 根据orgFund条件查询列表（含排序）
     * @apiGroup orgFund管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {OrgFund} orgFund  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<OrgFund> orgFund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<OrgFund> getOrgFundListByParam(OrgFund orgFund, String orderByStr);
	
	
	/**
     * @api findOrgFundPageByParam 根据orgFund条件查询列表（分页）
     * @apiGroup orgFund管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {OrgFund} orgFund  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<OrgFund> orgFund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<OrgFund> findOrgFundPageByParam(OrgFund orgFund, PageParam pageParam); 
	
	/**
     * @api findOrgFundPageByParam 根据orgFund条件查询列表（分页，含排序）
     * @apiGroup orgFund管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {OrgFund} orgFund  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<OrgFund> orgFund实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<OrgFund> findOrgFundPageByParam(OrgFund orgFund, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteOrgFundById 根据orgFund的id删除(物理删除)
     * @apiGroup orgFund管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteOrgFundById(String id);

}

