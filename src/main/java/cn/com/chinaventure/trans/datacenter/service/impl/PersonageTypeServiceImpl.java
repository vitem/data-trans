package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.PersonageTypeService;
import cn.com.chinaventure.trans.datacenter.entity.PersonageType;
import cn.com.chinaventure.trans.datacenter.entity.PersonageTypeExample;
import cn.com.chinaventure.trans.datacenter.entity.PersonageTypeExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.PersonageTypeMapper;


@Service("personageTypeService")
public class PersonageTypeServiceImpl implements PersonageTypeService {

	private final static Logger logger = LoggerFactory.getLogger(PersonageTypeServiceImpl.class);

	@Resource
	private PersonageTypeMapper personageTypeMapper;


	@Override
	public void savePersonageType(PersonageType personageType) {
		if (null == personageType) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.personageTypeMapper.insertSelective(personageType);
	}

	@Override
	public void updatePersonageType(PersonageType personageType) {
		if (null == personageType) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.personageTypeMapper.updateByPrimaryKeySelective(personageType);
	}

	@Override
	public PersonageType getPersonageTypeById(String id) {
		return personageTypeMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<PersonageType> getPersonageTypeList(PersonageType personageType) {
		return this.getPersonageTypeListByParam(personageType, null);
	}

	@Override
	public List<PersonageType> getPersonageTypeListByParam(PersonageType personageType, String orderByStr) {
		PersonageTypeExample example = new PersonageTypeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<PersonageType> personageTypeList = this.personageTypeMapper.selectByExample(example);
		return personageTypeList;
	}

	@Override
	public PageResult<PersonageType> findPersonageTypePageByParam(PersonageType personageType, PageParam pageParam) {
		return this.findPersonageTypePageByParam(personageType, pageParam, null);
	}

	@Override
	public PageResult<PersonageType> findPersonageTypePageByParam(PersonageType personageType, PageParam pageParam, String orderByStr) {
		PageResult<PersonageType> pageResult = new PageResult<PersonageType>();

		PersonageTypeExample example = new PersonageTypeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<PersonageType> list = null;// personageTypeMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deletePersonageTypeById(String id) {
		this.personageTypeMapper.deleteByPrimaryKey(id);
	}

}
