package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyStockStructureService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructure;


@RestController
@RequestMapping("/companyStockStructure")
public class CompanyStockStructureController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyStockStructureController.class);

    @Resource
    private CompanyStockStructureService companyStockStructureService;

    /**
	 * @api {post} / 新增companyStockStructure
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyStockStructure(@RequestBody CompanyStockStructure companyStockStructure) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyStockStructureService.saveCompanyStockStructure(companyStockStructure);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyStockStructure
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyStockStructure(@RequestBody CompanyStockStructure companyStockStructure) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyStockStructureService.updateCompanyStockStructure(companyStockStructure);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyStockStructure详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyStockStructure> getCompanyStockStructureById(@PathVariable String id) {
        JsonResult<CompanyStockStructure> jr = new JsonResult<CompanyStockStructure>();
        try {
            CompanyStockStructure companyStockStructure  = companyStockStructureService.getCompanyStockStructureById(id);
            jr = buildJsonResult(companyStockStructure);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyStockStructure/getCompanyStockStructureList 查询companyStockStructure列表(不带分页)
	 * @apiGroup companyStockStructure管理
	 * @apiName 查询companyStockStructure列表(不带分页)
	 * @apiDescription 根据条件查询companyStockStructure列表(不带分页)
	 * @apiParam {CompanyStockStructure} companyStockStructure companyStockStructure对象
	 */
    @RequestMapping("/getCompanyStockStructureList")
    public JsonResult<List<CompanyStockStructure>> getCompanyStockStructureList(CompanyStockStructure companyStockStructure) {
        JsonResult<List<CompanyStockStructure>> jr = new JsonResult<List<CompanyStockStructure>>();
        try {
            List<CompanyStockStructure> list = this.companyStockStructureService.getCompanyStockStructureList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyStockStructure/findCompanyStockStructurePageByParam 查询companyStockStructure列表(带分页)
	 * @apiGroup companyStockStructure管理
	 * @apiName 查询companyStockStructure列表(带分页)
	 * @apiDescription 根据条件查询companyStockStructure列表(带分页)
	 * @apiParam {CompanyStockStructure} companyStockStructure companyStockStructure对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyStockStructurePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyStockStructure>> findCompanyStockStructurePageByParam(@RequestBody CompanyStockStructure companyStockStructure, PageParam pageParam) {
        JsonResult<PageResult<CompanyStockStructure>> jr = new JsonResult<PageResult<CompanyStockStructure>>();
        try {
            PageResult<CompanyStockStructure> page = this.companyStockStructureService.findCompanyStockStructurePageByParam(companyStockStructure, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyStockStructure/deleteCompanyStockStructureById 删除companyStockStructure(物理删除)
	 * @apiGroup companyStockStructure管理
	 * @apiName 删除companyStockStructure记录(物理删除)
	 * @apiDescription 根据主键id删除companyStockStructure记录(物理删除)
	 * @apiParam {String} id companyStockStructure主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyStockStructureById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyStockStructureById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyStockStructureService.deleteCompanyStockStructureById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
