package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.Personage;
import cn.com.chinaventure.trans.datacenter.entity.PersonageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PersonageMapper {
    int countByExample(PersonageExample example);

    int deleteByExample(PersonageExample example);

    int deleteByPrimaryKey(String id);

    int insert(Personage record);

    int insertSelective(Personage record);

    List<Personage> selectByExample(PersonageExample example);

    Personage selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") Personage record, @Param("example") PersonageExample example);

    int updateByExample(@Param("record") Personage record, @Param("example") PersonageExample example);

    int updateByPrimaryKeySelective(Personage record);

    int updateByPrimaryKey(Personage record);
}