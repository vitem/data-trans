package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonage;

/**
 * @author 
 *
 */
public interface EventExitOrgPersonageService {
	
	/**
     * @api saveEventExitOrgPersonage 新增eventExitOrgPersonage
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  新增eventExitOrgPersonage记录
     * @apiDescription 全量插入eventExitOrgPersonage记录
     * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage eventExitOrgPersonage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventExitOrgPersonage(EventExitOrgPersonage eventExitOrgPersonage);
	
	/**
     * @api updateEventExitOrgPersonage 修改eventExitOrgPersonage
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  修改eventExitOrgPersonage记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage eventExitOrgPersonage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventExitOrgPersonage(EventExitOrgPersonage eventExitOrgPersonage);
	
	/**
     * @api getEventExitOrgPersonageById 根据eventExitOrgPersonageid查询详情
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  查询eventExitOrgPersonage详情
     * @apiDescription 根据主键id查询eventExitOrgPersonage详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventExitOrgPersonage eventExitOrgPersonage实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventExitOrgPersonage getEventExitOrgPersonageById(String id);
	
	/**
     * @api getEventExitOrgPersonageList 根据eventExitOrgPersonage条件查询列表
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage  实体条件
     * @apiSuccess List<EventExitOrgPersonage> eventExitOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventExitOrgPersonage> getEventExitOrgPersonageList(EventExitOrgPersonage eventExitOrgPersonage);
	

	/**
     * @api getEventExitOrgPersonageListByParam 根据eventExitOrgPersonage条件查询列表（含排序）
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventExitOrgPersonage> eventExitOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventExitOrgPersonage> getEventExitOrgPersonageListByParam(EventExitOrgPersonage eventExitOrgPersonage, String orderByStr);
	
	
	/**
     * @api findEventExitOrgPersonagePageByParam 根据eventExitOrgPersonage条件查询列表（分页）
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventExitOrgPersonage> eventExitOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventExitOrgPersonage> findEventExitOrgPersonagePageByParam(EventExitOrgPersonage eventExitOrgPersonage, PageParam pageParam); 
	
	/**
     * @api findEventExitOrgPersonagePageByParam 根据eventExitOrgPersonage条件查询列表（分页，含排序）
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventExitOrgPersonage} eventExitOrgPersonage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventExitOrgPersonage> eventExitOrgPersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventExitOrgPersonage> findEventExitOrgPersonagePageByParam(EventExitOrgPersonage eventExitOrgPersonage, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventExitOrgPersonageById 根据eventExitOrgPersonage的id删除(物理删除)
     * @apiGroup eventExitOrgPersonage管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventExitOrgPersonageById(String id);

}

