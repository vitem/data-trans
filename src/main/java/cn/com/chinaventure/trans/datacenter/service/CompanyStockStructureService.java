package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructure;

/**
 * @author 
 *
 */
public interface CompanyStockStructureService {
	
	/**
     * @api saveCompanyStockStructure 新增companyStockStructure
     * @apiGroup companyStockStructure管理
     * @apiName  新增companyStockStructure记录
     * @apiDescription 全量插入companyStockStructure记录
     * @apiParam {CompanyStockStructure} companyStockStructure companyStockStructure对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyStockStructure(CompanyStockStructure companyStockStructure);
	
	/**
     * @api updateCompanyStockStructure 修改companyStockStructure
     * @apiGroup companyStockStructure管理
     * @apiName  修改companyStockStructure记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyStockStructure} companyStockStructure companyStockStructure对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyStockStructure(CompanyStockStructure companyStockStructure);
	
	/**
     * @api getCompanyStockStructureById 根据companyStockStructureid查询详情
     * @apiGroup companyStockStructure管理
     * @apiName  查询companyStockStructure详情
     * @apiDescription 根据主键id查询companyStockStructure详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyStockStructure companyStockStructure实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyStockStructure getCompanyStockStructureById(String id);
	
	/**
     * @api getCompanyStockStructureList 根据companyStockStructure条件查询列表
     * @apiGroup companyStockStructure管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyStockStructure} companyStockStructure  实体条件
     * @apiSuccess List<CompanyStockStructure> companyStockStructure实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyStockStructure> getCompanyStockStructureList(CompanyStockStructure companyStockStructure);
	

	/**
     * @api getCompanyStockStructureListByParam 根据companyStockStructure条件查询列表（含排序）
     * @apiGroup companyStockStructure管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyStockStructure} companyStockStructure  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyStockStructure> companyStockStructure实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyStockStructure> getCompanyStockStructureListByParam(CompanyStockStructure companyStockStructure, String orderByStr);
	
	
	/**
     * @api findCompanyStockStructurePageByParam 根据companyStockStructure条件查询列表（分页）
     * @apiGroup companyStockStructure管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyStockStructure} companyStockStructure  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyStockStructure> companyStockStructure实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyStockStructure> findCompanyStockStructurePageByParam(CompanyStockStructure companyStockStructure, PageParam pageParam); 
	
	/**
     * @api findCompanyStockStructurePageByParam 根据companyStockStructure条件查询列表（分页，含排序）
     * @apiGroup companyStockStructure管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyStockStructure} companyStockStructure  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyStockStructure> companyStockStructure实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyStockStructure> findCompanyStockStructurePageByParam(CompanyStockStructure companyStockStructure, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyStockStructureById 根据companyStockStructure的id删除(物理删除)
     * @apiGroup companyStockStructure管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyStockStructureById(String id);

}

