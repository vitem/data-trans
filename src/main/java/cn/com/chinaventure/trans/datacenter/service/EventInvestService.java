package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventInvest;

/**
 * @author 
 *
 */
public interface EventInvestService {
	
	/**
     * @api saveEventInvest 新增eventInvest
     * @apiGroup eventInvest管理
     * @apiName  新增eventInvest记录
     * @apiDescription 全量插入eventInvest记录
     * @apiParam {EventInvest} eventInvest eventInvest对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventInvest(EventInvest eventInvest);
	
	/**
     * @api updateEventInvest 修改eventInvest
     * @apiGroup eventInvest管理
     * @apiName  修改eventInvest记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventInvest} eventInvest eventInvest对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventInvest(EventInvest eventInvest);
	
	/**
     * @api getEventInvestById 根据eventInvestid查询详情
     * @apiGroup eventInvest管理
     * @apiName  查询eventInvest详情
     * @apiDescription 根据主键id查询eventInvest详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventInvest eventInvest实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventInvest getEventInvestById(String id);
	
	/**
     * @api getEventInvestList 根据eventInvest条件查询列表
     * @apiGroup eventInvest管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventInvest} eventInvest  实体条件
     * @apiSuccess List<EventInvest> eventInvest实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventInvest> getEventInvestList(EventInvest eventInvest);
	

	/**
     * @api getEventInvestListByParam 根据eventInvest条件查询列表（含排序）
     * @apiGroup eventInvest管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventInvest} eventInvest  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventInvest> eventInvest实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventInvest> getEventInvestListByParam(EventInvest eventInvest, String orderByStr);
	
	
	/**
     * @api findEventInvestPageByParam 根据eventInvest条件查询列表（分页）
     * @apiGroup eventInvest管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventInvest} eventInvest  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventInvest> eventInvest实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventInvest> findEventInvestPageByParam(EventInvest eventInvest, PageParam pageParam); 
	
	/**
     * @api findEventInvestPageByParam 根据eventInvest条件查询列表（分页，含排序）
     * @apiGroup eventInvest管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventInvest} eventInvest  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventInvest> eventInvest实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventInvest> findEventInvestPageByParam(EventInvest eventInvest, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventInvestById 根据eventInvest的id删除(物理删除)
     * @apiGroup eventInvest管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventInvestById(String id);

}

