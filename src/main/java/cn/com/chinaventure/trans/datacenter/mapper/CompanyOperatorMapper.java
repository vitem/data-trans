package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyOperator;
import cn.com.chinaventure.trans.datacenter.entity.CompanyOperatorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyOperatorMapper {
    int countByExample(CompanyOperatorExample example);

    int deleteByExample(CompanyOperatorExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyOperator record);

    int insertSelective(CompanyOperator record);

    List<CompanyOperator> selectByExample(CompanyOperatorExample example);

    CompanyOperator selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyOperator record, @Param("example") CompanyOperatorExample example);

    int updateByExample(@Param("record") CompanyOperator record, @Param("example") CompanyOperatorExample example);

    int updateByPrimaryKeySelective(CompanyOperator record);

    int updateByPrimaryKey(CompanyOperator record);
}