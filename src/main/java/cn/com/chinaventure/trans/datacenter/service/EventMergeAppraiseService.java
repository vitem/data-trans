package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeAppraise;

/**
 * @author 
 *
 */
public interface EventMergeAppraiseService {
	
	/**
     * @api saveEventMergeAppraise 新增eventMergeAppraise
     * @apiGroup eventMergeAppraise管理
     * @apiName  新增eventMergeAppraise记录
     * @apiDescription 全量插入eventMergeAppraise记录
     * @apiParam {EventMergeAppraise} eventMergeAppraise eventMergeAppraise对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventMergeAppraise(EventMergeAppraise eventMergeAppraise);
	
	/**
     * @api updateEventMergeAppraise 修改eventMergeAppraise
     * @apiGroup eventMergeAppraise管理
     * @apiName  修改eventMergeAppraise记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventMergeAppraise} eventMergeAppraise eventMergeAppraise对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventMergeAppraise(EventMergeAppraise eventMergeAppraise);
	
	/**
     * @api getEventMergeAppraiseById 根据eventMergeAppraiseid查询详情
     * @apiGroup eventMergeAppraise管理
     * @apiName  查询eventMergeAppraise详情
     * @apiDescription 根据主键id查询eventMergeAppraise详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventMergeAppraise eventMergeAppraise实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventMergeAppraise getEventMergeAppraiseById(String id);
	
	/**
     * @api getEventMergeAppraiseList 根据eventMergeAppraise条件查询列表
     * @apiGroup eventMergeAppraise管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventMergeAppraise} eventMergeAppraise  实体条件
     * @apiSuccess List<EventMergeAppraise> eventMergeAppraise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeAppraise> getEventMergeAppraiseList(EventMergeAppraise eventMergeAppraise);
	

	/**
     * @api getEventMergeAppraiseListByParam 根据eventMergeAppraise条件查询列表（含排序）
     * @apiGroup eventMergeAppraise管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventMergeAppraise} eventMergeAppraise  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventMergeAppraise> eventMergeAppraise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMergeAppraise> getEventMergeAppraiseListByParam(EventMergeAppraise eventMergeAppraise, String orderByStr);
	
	
	/**
     * @api findEventMergeAppraisePageByParam 根据eventMergeAppraise条件查询列表（分页）
     * @apiGroup eventMergeAppraise管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventMergeAppraise} eventMergeAppraise  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventMergeAppraise> eventMergeAppraise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeAppraise> findEventMergeAppraisePageByParam(EventMergeAppraise eventMergeAppraise, PageParam pageParam); 
	
	/**
     * @api findEventMergeAppraisePageByParam 根据eventMergeAppraise条件查询列表（分页，含排序）
     * @apiGroup eventMergeAppraise管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventMergeAppraise} eventMergeAppraise  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventMergeAppraise> eventMergeAppraise实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMergeAppraise> findEventMergeAppraisePageByParam(EventMergeAppraise eventMergeAppraise, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventMergeAppraiseById 根据eventMergeAppraise的id删除(物理删除)
     * @apiGroup eventMergeAppraise管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventMergeAppraiseById(String id);

}

