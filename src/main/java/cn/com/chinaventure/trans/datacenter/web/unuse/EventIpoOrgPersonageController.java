package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventIpoOrgPersonageService;
import cn.com.chinaventure.trans.datacenter.entity.EventIpoOrgPersonage;


@RestController
@RequestMapping("/eventIpoOrgPersonage")
public class EventIpoOrgPersonageController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventIpoOrgPersonageController.class);

    @Resource
    private EventIpoOrgPersonageService eventIpoOrgPersonageService;

    /**
	 * @api {post} / 新增eventIpoOrgPersonage
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventIpoOrgPersonage(@RequestBody EventIpoOrgPersonage eventIpoOrgPersonage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventIpoOrgPersonageService.saveEventIpoOrgPersonage(eventIpoOrgPersonage);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventIpoOrgPersonage
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventIpoOrgPersonage(@RequestBody EventIpoOrgPersonage eventIpoOrgPersonage) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventIpoOrgPersonageService.updateEventIpoOrgPersonage(eventIpoOrgPersonage);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventIpoOrgPersonage详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventIpoOrgPersonage> getEventIpoOrgPersonageById(@PathVariable String id) {
        JsonResult<EventIpoOrgPersonage> jr = new JsonResult<EventIpoOrgPersonage>();
        try {
            EventIpoOrgPersonage eventIpoOrgPersonage  = eventIpoOrgPersonageService.getEventIpoOrgPersonageById(id);
            jr = buildJsonResult(eventIpoOrgPersonage);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventIpoOrgPersonage/getEventIpoOrgPersonageList 查询eventIpoOrgPersonage列表(不带分页)
	 * @apiGroup eventIpoOrgPersonage管理
	 * @apiName 查询eventIpoOrgPersonage列表(不带分页)
	 * @apiDescription 根据条件查询eventIpoOrgPersonage列表(不带分页)
	 * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage eventIpoOrgPersonage对象
	 */
    @RequestMapping("/getEventIpoOrgPersonageList")
    public JsonResult<List<EventIpoOrgPersonage>> getEventIpoOrgPersonageList(EventIpoOrgPersonage eventIpoOrgPersonage) {
        JsonResult<List<EventIpoOrgPersonage>> jr = new JsonResult<List<EventIpoOrgPersonage>>();
        try {
            List<EventIpoOrgPersonage> list = this.eventIpoOrgPersonageService.getEventIpoOrgPersonageList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventIpoOrgPersonage/findEventIpoOrgPersonagePageByParam 查询eventIpoOrgPersonage列表(带分页)
	 * @apiGroup eventIpoOrgPersonage管理
	 * @apiName 查询eventIpoOrgPersonage列表(带分页)
	 * @apiDescription 根据条件查询eventIpoOrgPersonage列表(带分页)
	 * @apiParam {EventIpoOrgPersonage} eventIpoOrgPersonage eventIpoOrgPersonage对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventIpoOrgPersonagePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventIpoOrgPersonage>> findEventIpoOrgPersonagePageByParam(@RequestBody EventIpoOrgPersonage eventIpoOrgPersonage, PageParam pageParam) {
        JsonResult<PageResult<EventIpoOrgPersonage>> jr = new JsonResult<PageResult<EventIpoOrgPersonage>>();
        try {
            PageResult<EventIpoOrgPersonage> page = this.eventIpoOrgPersonageService.findEventIpoOrgPersonagePageByParam(eventIpoOrgPersonage, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventIpoOrgPersonage/deleteEventIpoOrgPersonageById 删除eventIpoOrgPersonage(物理删除)
	 * @apiGroup eventIpoOrgPersonage管理
	 * @apiName 删除eventIpoOrgPersonage记录(物理删除)
	 * @apiDescription 根据主键id删除eventIpoOrgPersonage记录(物理删除)
	 * @apiParam {String} id eventIpoOrgPersonage主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventIpoOrgPersonageById",method=RequestMethod.DELETE)
    public JsonResult deleteEventIpoOrgPersonageById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventIpoOrgPersonageService.deleteEventIpoOrgPersonageById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
