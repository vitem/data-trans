package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;

public class OrganizationStrategyWithBLOBs extends OrganizationStrategy implements Serializable {
    /**
     * 投资描述
     */
    private String strategyCnDesc;

    /**
     * 投资英文描述
     */
    private String strategyEnDesc;

    /**
     * 投资备注
     */
    private String strategyRemark;

    private static final long serialVersionUID = 1L;

    public String getStrategyCnDesc() {
        return strategyCnDesc;
    }

    public void setStrategyCnDesc(String strategyCnDesc) {
        this.strategyCnDesc = strategyCnDesc == null ? null : strategyCnDesc.trim();
    }

    public String getStrategyEnDesc() {
        return strategyEnDesc;
    }

    public void setStrategyEnDesc(String strategyEnDesc) {
        this.strategyEnDesc = strategyEnDesc == null ? null : strategyEnDesc.trim();
    }

    public String getStrategyRemark() {
        return strategyRemark;
    }

    public void setStrategyRemark(String strategyRemark) {
        this.strategyRemark = strategyRemark == null ? null : strategyRemark.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", strategyCnDesc=").append(strategyCnDesc);
        sb.append(", strategyEnDesc=").append(strategyEnDesc);
        sb.append(", strategyRemark=").append(strategyRemark);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}