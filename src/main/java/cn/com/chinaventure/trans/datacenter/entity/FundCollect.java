package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class FundCollect implements Serializable {
    private String id;

    private String fundStrategyId;

    private Date startTime;

    /**
     * 开始募集规模
     */
    private BigDecimal startScale;

    /**
     * 货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte startCurrencyType;

    /**
     * 开始募集中文描述
     */
    private String startCnDesc;

    /**
     * 开始募集英文描述
     */
    private String startEnDesc;

    /**
     * 首轮募集时间
     */
    private Date firstTime;

    /**
     * 首轮募集规模
     */
    private BigDecimal firstScale;

    /**
     * 首轮募集货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte firstCurrencyType;

    /**
     * 首轮募集中文描述
     */
    private String firstCnDesc;

    /**
     * 首轮募集英文描述
     */
    private String firstEnDesc;

    /**
     * 募集完成时间
     */
    private Date finallyTime;

    /**
     * 募集完成规模
     */
    private BigDecimal finallyScale;

    /**
     * 募集完成规模货币类型 1,美元 2,元 3,日元 4,英镑 5,欧元 6,新元 7,港元 8,澳元 9,加元 10,台币 11,韩元 12,新谢克尔 13,新西兰元 14,瑞士法郎 15,马来西亚林吉特 16,瑞典克朗
     */
    private Byte finallyCurrencyType;

    /**
     * 募集完成中文描述
     */
    private String finallyCnDesc;

    /**
     * 募集完成英文描述
     */
    private String finallyEnDesc;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getFundStrategyId() {
        return fundStrategyId;
    }

    public void setFundStrategyId(String fundStrategyId) {
        this.fundStrategyId = fundStrategyId == null ? null : fundStrategyId.trim();
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public BigDecimal getStartScale() {
        return startScale;
    }

    public void setStartScale(BigDecimal startScale) {
        this.startScale = startScale;
    }

    public Byte getStartCurrencyType() {
        return startCurrencyType;
    }

    public void setStartCurrencyType(Byte startCurrencyType) {
        this.startCurrencyType = startCurrencyType;
    }

    public String getStartCnDesc() {
        return startCnDesc;
    }

    public void setStartCnDesc(String startCnDesc) {
        this.startCnDesc = startCnDesc == null ? null : startCnDesc.trim();
    }

    public String getStartEnDesc() {
        return startEnDesc;
    }

    public void setStartEnDesc(String startEnDesc) {
        this.startEnDesc = startEnDesc == null ? null : startEnDesc.trim();
    }

    public Date getFirstTime() {
        return firstTime;
    }

    public void setFirstTime(Date firstTime) {
        this.firstTime = firstTime;
    }

    public BigDecimal getFirstScale() {
        return firstScale;
    }

    public void setFirstScale(BigDecimal firstScale) {
        this.firstScale = firstScale;
    }

    public Byte getFirstCurrencyType() {
        return firstCurrencyType;
    }

    public void setFirstCurrencyType(Byte firstCurrencyType) {
        this.firstCurrencyType = firstCurrencyType;
    }

    public String getFirstCnDesc() {
        return firstCnDesc;
    }

    public void setFirstCnDesc(String firstCnDesc) {
        this.firstCnDesc = firstCnDesc == null ? null : firstCnDesc.trim();
    }

    public String getFirstEnDesc() {
        return firstEnDesc;
    }

    public void setFirstEnDesc(String firstEnDesc) {
        this.firstEnDesc = firstEnDesc == null ? null : firstEnDesc.trim();
    }

    public Date getFinallyTime() {
        return finallyTime;
    }

    public void setFinallyTime(Date finallyTime) {
        this.finallyTime = finallyTime;
    }

    public BigDecimal getFinallyScale() {
        return finallyScale;
    }

    public void setFinallyScale(BigDecimal finallyScale) {
        this.finallyScale = finallyScale;
    }

    public Byte getFinallyCurrencyType() {
        return finallyCurrencyType;
    }

    public void setFinallyCurrencyType(Byte finallyCurrencyType) {
        this.finallyCurrencyType = finallyCurrencyType;
    }

    public String getFinallyCnDesc() {
        return finallyCnDesc;
    }

    public void setFinallyCnDesc(String finallyCnDesc) {
        this.finallyCnDesc = finallyCnDesc == null ? null : finallyCnDesc.trim();
    }

    public String getFinallyEnDesc() {
        return finallyEnDesc;
    }

    public void setFinallyEnDesc(String finallyEnDesc) {
        this.finallyEnDesc = finallyEnDesc == null ? null : finallyEnDesc.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", fundStrategyId=").append(fundStrategyId);
        sb.append(", startTime=").append(startTime);
        sb.append(", startScale=").append(startScale);
        sb.append(", startCurrencyType=").append(startCurrencyType);
        sb.append(", startCnDesc=").append(startCnDesc);
        sb.append(", startEnDesc=").append(startEnDesc);
        sb.append(", firstTime=").append(firstTime);
        sb.append(", firstScale=").append(firstScale);
        sb.append(", firstCurrencyType=").append(firstCurrencyType);
        sb.append(", firstCnDesc=").append(firstCnDesc);
        sb.append(", firstEnDesc=").append(firstEnDesc);
        sb.append(", finallyTime=").append(finallyTime);
        sb.append(", finallyScale=").append(finallyScale);
        sb.append(", finallyCurrencyType=").append(finallyCurrencyType);
        sb.append(", finallyCnDesc=").append(finallyCnDesc);
        sb.append(", finallyEnDesc=").append(finallyEnDesc);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}