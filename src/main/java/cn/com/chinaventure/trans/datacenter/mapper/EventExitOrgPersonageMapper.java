package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonage;
import cn.com.chinaventure.trans.datacenter.entity.EventExitOrgPersonageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventExitOrgPersonageMapper {
    int countByExample(EventExitOrgPersonageExample example);

    int deleteByExample(EventExitOrgPersonageExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventExitOrgPersonage record);

    int insertSelective(EventExitOrgPersonage record);

    List<EventExitOrgPersonage> selectByExample(EventExitOrgPersonageExample example);

    EventExitOrgPersonage selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventExitOrgPersonage record, @Param("example") EventExitOrgPersonageExample example);

    int updateByExample(@Param("record") EventExitOrgPersonage record, @Param("example") EventExitOrgPersonageExample example);

    int updateByPrimaryKeySelective(EventExitOrgPersonage record);

    int updateByPrimaryKey(EventExitOrgPersonage record);
}