package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventService;
import cn.com.chinaventure.trans.datacenter.entity.Event;


@RestController
@RequestMapping("/event")
public class EventController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventController.class);

    @Resource
    private EventService eventService;

    /**
	 * @api {post} / 新增event
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEvent(@RequestBody Event event) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventService.saveEvent(event);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改event
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEvent(@RequestBody Event event) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventService.updateEvent(event);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询event详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Event> getEventById(@PathVariable String id) {
        JsonResult<Event> jr = new JsonResult<Event>();
        try {
            Event event  = eventService.getEventById(id);
            jr = buildJsonResult(event);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /event/getEventList 查询event列表(不带分页)
	 * @apiGroup event管理
	 * @apiName 查询event列表(不带分页)
	 * @apiDescription 根据条件查询event列表(不带分页)
	 * @apiParam {Event} event event对象
	 */
    @RequestMapping("/getEventList")
    public JsonResult<List<Event>> getEventList(Event event) {
        JsonResult<List<Event>> jr = new JsonResult<List<Event>>();
        try {
            List<Event> list = this.eventService.getEventList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /event/findEventPageByParam 查询event列表(带分页)
	 * @apiGroup event管理
	 * @apiName 查询event列表(带分页)
	 * @apiDescription 根据条件查询event列表(带分页)
	 * @apiParam {Event} event event对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Event>> findEventPageByParam(@RequestBody Event event, PageParam pageParam) {
        JsonResult<PageResult<Event>> jr = new JsonResult<PageResult<Event>>();
        try {
            PageResult<Event> page = this.eventService.findEventPageByParam(event, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /event/deleteEventById 删除event(物理删除)
	 * @apiGroup event管理
	 * @apiName 删除event记录(物理删除)
	 * @apiDescription 根据主键id删除event记录(物理删除)
	 * @apiParam {String} id event主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventById",method=RequestMethod.DELETE)
    public JsonResult deleteEventById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventService.deleteEventById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
