package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyStockStructureService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructure;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructureExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyStockStructureExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyStockStructureMapper;


@Service("companyStockStructureService")
public class CompanyStockStructureServiceImpl implements CompanyStockStructureService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyStockStructureServiceImpl.class);

	@Resource
	private CompanyStockStructureMapper companyStockStructureMapper;


	@Override
	public void saveCompanyStockStructure(CompanyStockStructure companyStockStructure) {
		if (null == companyStockStructure) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyStockStructureMapper.insertSelective(companyStockStructure);
	}

	@Override
	public void updateCompanyStockStructure(CompanyStockStructure companyStockStructure) {
		if (null == companyStockStructure) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyStockStructureMapper.updateByPrimaryKeySelective(companyStockStructure);
	}

	@Override
	public CompanyStockStructure getCompanyStockStructureById(String id) {
		return companyStockStructureMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyStockStructure> getCompanyStockStructureList(CompanyStockStructure companyStockStructure) {
		return this.getCompanyStockStructureListByParam(companyStockStructure, null);
	}

	@Override
	public List<CompanyStockStructure> getCompanyStockStructureListByParam(CompanyStockStructure companyStockStructure, String orderByStr) {
		CompanyStockStructureExample example = new CompanyStockStructureExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyStockStructure> companyStockStructureList = this.companyStockStructureMapper.selectByExample(example);
		return companyStockStructureList;
	}

	@Override
	public PageResult<CompanyStockStructure> findCompanyStockStructurePageByParam(CompanyStockStructure companyStockStructure, PageParam pageParam) {
		return this.findCompanyStockStructurePageByParam(companyStockStructure, pageParam, null);
	}

	@Override
	public PageResult<CompanyStockStructure> findCompanyStockStructurePageByParam(CompanyStockStructure companyStockStructure, PageParam pageParam, String orderByStr) {
		PageResult<CompanyStockStructure> pageResult = new PageResult<CompanyStockStructure>();

		CompanyStockStructureExample example = new CompanyStockStructureExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyStockStructure> list = null;// companyStockStructureMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyStockStructureById(String id) {
		this.companyStockStructureMapper.deleteByPrimaryKey(id);
	}

}
