package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyCredit;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCreditExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyCreditMapper {
    int countByExample(CompanyCreditExample example);

    int deleteByExample(CompanyCreditExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyCredit record);

    int insertSelective(CompanyCredit record);

    List<CompanyCredit> selectByExample(CompanyCreditExample example);

    CompanyCredit selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyCredit record, @Param("example") CompanyCreditExample example);

    int updateByExample(@Param("record") CompanyCredit record, @Param("example") CompanyCreditExample example);

    int updateByPrimaryKeySelective(CompanyCredit record);

    int updateByPrimaryKey(CompanyCredit record);
}