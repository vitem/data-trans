package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.datacenter.entity.Lp;
import cn.com.chinaventure.trans.datacenter.entity.LpExample;
import cn.com.chinaventure.trans.datacenter.entity.LpExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.LpMapper;
import cn.com.chinaventure.trans.datacenter.service.LpService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("lpService")
public class LpServiceImpl implements LpService {

	private final static Logger logger = LoggerFactory.getLogger(LpServiceImpl.class);

	@Resource
	private LpMapper lpMapper;


	@Override
	public void saveLp(Lp lp) {
		if (null == lp) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.lpMapper.insertSelective(lp);
	}

	@Override
	public void updateLp(Lp lp) {
		if (null == lp) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.lpMapper.updateByPrimaryKeySelective(lp);
	}

	@Override
	public Lp getLpById(String id) {
		return lpMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Lp> getLpList(Lp lp) {
		return this.getLpListByParam(lp, null);
	}

	@Override
	public List<Lp> getLpListByParam(Lp lp, String orderByStr) {
		LpExample example = new LpExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Lp> lpList = this.lpMapper.selectByExample(example);
		return lpList;
	}

	@Override
	public PageResult<Lp> findLpPageByParam(Lp lp, PageParam pageParam) {
		return this.findLpPageByParam(lp, pageParam, null);
	}

	@Override
	public PageResult<Lp> findLpPageByParam(Lp lp, PageParam pageParam, String orderByStr) {
		PageResult<Lp> pageResult = new PageResult<Lp>();

		LpExample example = new LpExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Lp> list = null;// lpMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteLpById(String id) {
		this.lpMapper.deleteByPrimaryKey(id);
	}

}
