package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrganizationStrategyExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public OrganizationStrategyExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNull() {
            addCriterion("organization_id is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNotNull() {
            addCriterion("organization_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdEqualTo(String value) {
            addCriterion("organization_id =", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotEqualTo(String value) {
            addCriterion("organization_id <>", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThan(String value) {
            addCriterion("organization_id >", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThanOrEqualTo(String value) {
            addCriterion("organization_id >=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThan(String value) {
            addCriterion("organization_id <", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThanOrEqualTo(String value) {
            addCriterion("organization_id <=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLike(String value) {
            addCriterion("organization_id like", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotLike(String value) {
            addCriterion("organization_id not like", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIn(List<String> values) {
            addCriterion("organization_id in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotIn(List<String> values) {
            addCriterion("organization_id not in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdBetween(String value1, String value2) {
            addCriterion("organization_id between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotBetween(String value1, String value2) {
            addCriterion("organization_id not between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeIsNull() {
            addCriterion("strategy_time is null");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeIsNotNull() {
            addCriterion("strategy_time is not null");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeEqualTo(Date value) {
            addCriterion("strategy_time =", value, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeNotEqualTo(Date value) {
            addCriterion("strategy_time <>", value, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeGreaterThan(Date value) {
            addCriterion("strategy_time >", value, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("strategy_time >=", value, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeLessThan(Date value) {
            addCriterion("strategy_time <", value, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeLessThanOrEqualTo(Date value) {
            addCriterion("strategy_time <=", value, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeIn(List<Date> values) {
            addCriterion("strategy_time in", values, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeNotIn(List<Date> values) {
            addCriterion("strategy_time not in", values, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeBetween(Date value1, Date value2) {
            addCriterion("strategy_time between", value1, value2, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andStrategyTimeNotBetween(Date value1, Date value2) {
            addCriterion("strategy_time not between", value1, value2, "strategyTime");
            return (Criteria) this;
        }

        public Criteria andUppMoneyIsNull() {
            addCriterion("upp_money is null");
            return (Criteria) this;
        }

        public Criteria andUppMoneyIsNotNull() {
            addCriterion("upp_money is not null");
            return (Criteria) this;
        }

        public Criteria andUppMoneyEqualTo(BigDecimal value) {
            addCriterion("upp_money =", value, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyNotEqualTo(BigDecimal value) {
            addCriterion("upp_money <>", value, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyGreaterThan(BigDecimal value) {
            addCriterion("upp_money >", value, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("upp_money >=", value, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyLessThan(BigDecimal value) {
            addCriterion("upp_money <", value, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("upp_money <=", value, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyIn(List<BigDecimal> values) {
            addCriterion("upp_money in", values, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyNotIn(List<BigDecimal> values) {
            addCriterion("upp_money not in", values, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("upp_money between", value1, value2, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andUppMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("upp_money not between", value1, value2, "uppMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyIsNull() {
            addCriterion("low_money is null");
            return (Criteria) this;
        }

        public Criteria andLowMoneyIsNotNull() {
            addCriterion("low_money is not null");
            return (Criteria) this;
        }

        public Criteria andLowMoneyEqualTo(BigDecimal value) {
            addCriterion("low_money =", value, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyNotEqualTo(BigDecimal value) {
            addCriterion("low_money <>", value, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyGreaterThan(BigDecimal value) {
            addCriterion("low_money >", value, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("low_money >=", value, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyLessThan(BigDecimal value) {
            addCriterion("low_money <", value, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("low_money <=", value, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyIn(List<BigDecimal> values) {
            addCriterion("low_money in", values, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyNotIn(List<BigDecimal> values) {
            addCriterion("low_money not in", values, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_money between", value1, value2, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andLowMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("low_money not between", value1, value2, "lowMoney");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeIsNull() {
            addCriterion("currency_type is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeIsNotNull() {
            addCriterion("currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeEqualTo(Byte value) {
            addCriterion("currency_type =", value, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("currency_type <>", value, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeGreaterThan(Byte value) {
            addCriterion("currency_type >", value, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("currency_type >=", value, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeLessThan(Byte value) {
            addCriterion("currency_type <", value, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("currency_type <=", value, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeIn(List<Byte> values) {
            addCriterion("currency_type in", values, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("currency_type not in", values, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("currency_type between", value1, value2, "currencyType");
            return (Criteria) this;
        }

        public Criteria andCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("currency_type not between", value1, value2, "currencyType");
            return (Criteria) this;
        }

        public Criteria andVcScaleIsNull() {
            addCriterion("vc_scale is null");
            return (Criteria) this;
        }

        public Criteria andVcScaleIsNotNull() {
            addCriterion("vc_scale is not null");
            return (Criteria) this;
        }

        public Criteria andVcScaleEqualTo(BigDecimal value) {
            addCriterion("vc_scale =", value, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleNotEqualTo(BigDecimal value) {
            addCriterion("vc_scale <>", value, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleGreaterThan(BigDecimal value) {
            addCriterion("vc_scale >", value, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("vc_scale >=", value, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleLessThan(BigDecimal value) {
            addCriterion("vc_scale <", value, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleLessThanOrEqualTo(BigDecimal value) {
            addCriterion("vc_scale <=", value, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleIn(List<BigDecimal> values) {
            addCriterion("vc_scale in", values, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleNotIn(List<BigDecimal> values) {
            addCriterion("vc_scale not in", values, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("vc_scale between", value1, value2, "vcScale");
            return (Criteria) this;
        }

        public Criteria andVcScaleNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("vc_scale not between", value1, value2, "vcScale");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescIsNull() {
            addCriterion("strategy_cn_desc is null");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescIsNotNull() {
            addCriterion("strategy_cn_desc is not null");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescEqualTo(String value) {
            addCriterion("strategy_cn_desc =", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescNotEqualTo(String value) {
            addCriterion("strategy_cn_desc <>", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescGreaterThan(String value) {
            addCriterion("strategy_cn_desc >", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescGreaterThanOrEqualTo(String value) {
            addCriterion("strategy_cn_desc >=", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescLessThan(String value) {
            addCriterion("strategy_cn_desc <", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescLessThanOrEqualTo(String value) {
            addCriterion("strategy_cn_desc <=", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescLike(String value) {
            addCriterion("strategy_cn_desc like", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescNotLike(String value) {
            addCriterion("strategy_cn_desc not like", value, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescIn(List<String> values) {
            addCriterion("strategy_cn_desc in", values, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescNotIn(List<String> values) {
            addCriterion("strategy_cn_desc not in", values, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescBetween(String value1, String value2) {
            addCriterion("strategy_cn_desc between", value1, value2, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyCnDescNotBetween(String value1, String value2) {
            addCriterion("strategy_cn_desc not between", value1, value2, "strategyCnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescIsNull() {
            addCriterion("strategy_en_desc is null");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescIsNotNull() {
            addCriterion("strategy_en_desc is not null");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescEqualTo(String value) {
            addCriterion("strategy_en_desc =", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescNotEqualTo(String value) {
            addCriterion("strategy_en_desc <>", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescGreaterThan(String value) {
            addCriterion("strategy_en_desc >", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescGreaterThanOrEqualTo(String value) {
            addCriterion("strategy_en_desc >=", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescLessThan(String value) {
            addCriterion("strategy_en_desc <", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescLessThanOrEqualTo(String value) {
            addCriterion("strategy_en_desc <=", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescLike(String value) {
            addCriterion("strategy_en_desc like", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescNotLike(String value) {
            addCriterion("strategy_en_desc not like", value, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescIn(List<String> values) {
            addCriterion("strategy_en_desc in", values, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescNotIn(List<String> values) {
            addCriterion("strategy_en_desc not in", values, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescBetween(String value1, String value2) {
            addCriterion("strategy_en_desc between", value1, value2, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyEnDescNotBetween(String value1, String value2) {
            addCriterion("strategy_en_desc not between", value1, value2, "strategyEnDesc");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkIsNull() {
            addCriterion("strategy_remark is null");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkIsNotNull() {
            addCriterion("strategy_remark is not null");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkEqualTo(String value) {
            addCriterion("strategy_remark =", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkNotEqualTo(String value) {
            addCriterion("strategy_remark <>", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkGreaterThan(String value) {
            addCriterion("strategy_remark >", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkGreaterThanOrEqualTo(String value) {
            addCriterion("strategy_remark >=", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkLessThan(String value) {
            addCriterion("strategy_remark <", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkLessThanOrEqualTo(String value) {
            addCriterion("strategy_remark <=", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkLike(String value) {
            addCriterion("strategy_remark like", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkNotLike(String value) {
            addCriterion("strategy_remark not like", value, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkIn(List<String> values) {
            addCriterion("strategy_remark in", values, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkNotIn(List<String> values) {
            addCriterion("strategy_remark not in", values, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkBetween(String value1, String value2) {
            addCriterion("strategy_remark between", value1, value2, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andStrategyRemarkNotBetween(String value1, String value2) {
            addCriterion("strategy_remark not between", value1, value2, "strategyRemark");
            return (Criteria) this;
        }

        public Criteria andRegionTypeIsNull() {
            addCriterion("region_type is null");
            return (Criteria) this;
        }

        public Criteria andRegionTypeIsNotNull() {
            addCriterion("region_type is not null");
            return (Criteria) this;
        }

        public Criteria andRegionTypeEqualTo(Byte value) {
            addCriterion("region_type =", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeNotEqualTo(Byte value) {
            addCriterion("region_type <>", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeGreaterThan(Byte value) {
            addCriterion("region_type >", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("region_type >=", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeLessThan(Byte value) {
            addCriterion("region_type <", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeLessThanOrEqualTo(Byte value) {
            addCriterion("region_type <=", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeIn(List<Byte> values) {
            addCriterion("region_type in", values, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeNotIn(List<Byte> values) {
            addCriterion("region_type not in", values, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeBetween(Byte value1, Byte value2) {
            addCriterion("region_type between", value1, value2, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("region_type not between", value1, value2, "regionType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeIsNull() {
            addCriterion("industry_type is null");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeIsNotNull() {
            addCriterion("industry_type is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeEqualTo(Byte value) {
            addCriterion("industry_type =", value, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeNotEqualTo(Byte value) {
            addCriterion("industry_type <>", value, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeGreaterThan(Byte value) {
            addCriterion("industry_type >", value, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("industry_type >=", value, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeLessThan(Byte value) {
            addCriterion("industry_type <", value, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeLessThanOrEqualTo(Byte value) {
            addCriterion("industry_type <=", value, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeIn(List<Byte> values) {
            addCriterion("industry_type in", values, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeNotIn(List<Byte> values) {
            addCriterion("industry_type not in", values, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeBetween(Byte value1, Byte value2) {
            addCriterion("industry_type between", value1, value2, "industryType");
            return (Criteria) this;
        }

        public Criteria andIndustryTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("industry_type not between", value1, value2, "industryType");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}