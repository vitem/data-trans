package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyOperatorService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyOperator;
import cn.com.chinaventure.trans.datacenter.entity.CompanyOperatorExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyOperatorExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyOperatorMapper;


@Service("companyOperatorService")
public class CompanyOperatorServiceImpl implements CompanyOperatorService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyOperatorServiceImpl.class);

	@Resource
	private CompanyOperatorMapper companyOperatorMapper;


	@Override
	public void saveCompanyOperator(CompanyOperator companyOperator) {
		if (null == companyOperator) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyOperatorMapper.insertSelective(companyOperator);
	}

	@Override
	public void updateCompanyOperator(CompanyOperator companyOperator) {
		if (null == companyOperator) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyOperatorMapper.updateByPrimaryKeySelective(companyOperator);
	}

	@Override
	public CompanyOperator getCompanyOperatorById(String id) {
		return companyOperatorMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyOperator> getCompanyOperatorList(CompanyOperator companyOperator) {
		return this.getCompanyOperatorListByParam(companyOperator, null);
	}

	@Override
	public List<CompanyOperator> getCompanyOperatorListByParam(CompanyOperator companyOperator, String orderByStr) {
		CompanyOperatorExample example = new CompanyOperatorExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyOperator> companyOperatorList = this.companyOperatorMapper.selectByExample(example);
		return companyOperatorList;
	}

	@Override
	public PageResult<CompanyOperator> findCompanyOperatorPageByParam(CompanyOperator companyOperator, PageParam pageParam) {
		return this.findCompanyOperatorPageByParam(companyOperator, pageParam, null);
	}

	@Override
	public PageResult<CompanyOperator> findCompanyOperatorPageByParam(CompanyOperator companyOperator, PageParam pageParam, String orderByStr) {
		PageResult<CompanyOperator> pageResult = new PageResult<CompanyOperator>();

		CompanyOperatorExample example = new CompanyOperatorExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyOperator> list = null;// companyOperatorMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyOperatorById(String id) {
		this.companyOperatorMapper.deleteByPrimaryKey(id);
	}

}
