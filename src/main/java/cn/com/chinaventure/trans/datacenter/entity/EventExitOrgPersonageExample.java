package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventExitOrgPersonageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EventExitOrgPersonageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEventExitIdIsNull() {
            addCriterion("event_exit_id is null");
            return (Criteria) this;
        }

        public Criteria andEventExitIdIsNotNull() {
            addCriterion("event_exit_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventExitIdEqualTo(String value) {
            addCriterion("event_exit_id =", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdNotEqualTo(String value) {
            addCriterion("event_exit_id <>", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdGreaterThan(String value) {
            addCriterion("event_exit_id >", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_exit_id >=", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdLessThan(String value) {
            addCriterion("event_exit_id <", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdLessThanOrEqualTo(String value) {
            addCriterion("event_exit_id <=", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdLike(String value) {
            addCriterion("event_exit_id like", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdNotLike(String value) {
            addCriterion("event_exit_id not like", value, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdIn(List<String> values) {
            addCriterion("event_exit_id in", values, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdNotIn(List<String> values) {
            addCriterion("event_exit_id not in", values, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdBetween(String value1, String value2) {
            addCriterion("event_exit_id between", value1, value2, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andEventExitIdNotBetween(String value1, String value2) {
            addCriterion("event_exit_id not between", value1, value2, "eventExitId");
            return (Criteria) this;
        }

        public Criteria andOrgIdIsNull() {
            addCriterion("org_id is null");
            return (Criteria) this;
        }

        public Criteria andOrgIdIsNotNull() {
            addCriterion("org_id is not null");
            return (Criteria) this;
        }

        public Criteria andOrgIdEqualTo(String value) {
            addCriterion("org_id =", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdNotEqualTo(String value) {
            addCriterion("org_id <>", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdGreaterThan(String value) {
            addCriterion("org_id >", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("org_id >=", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdLessThan(String value) {
            addCriterion("org_id <", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdLessThanOrEqualTo(String value) {
            addCriterion("org_id <=", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdLike(String value) {
            addCriterion("org_id like", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdNotLike(String value) {
            addCriterion("org_id not like", value, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdIn(List<String> values) {
            addCriterion("org_id in", values, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdNotIn(List<String> values) {
            addCriterion("org_id not in", values, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdBetween(String value1, String value2) {
            addCriterion("org_id between", value1, value2, "orgId");
            return (Criteria) this;
        }

        public Criteria andOrgIdNotBetween(String value1, String value2) {
            addCriterion("org_id not between", value1, value2, "orgId");
            return (Criteria) this;
        }

        public Criteria andFundIdIsNull() {
            addCriterion("fund_id is null");
            return (Criteria) this;
        }

        public Criteria andFundIdIsNotNull() {
            addCriterion("fund_id is not null");
            return (Criteria) this;
        }

        public Criteria andFundIdEqualTo(String value) {
            addCriterion("fund_id =", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdNotEqualTo(String value) {
            addCriterion("fund_id <>", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdGreaterThan(String value) {
            addCriterion("fund_id >", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdGreaterThanOrEqualTo(String value) {
            addCriterion("fund_id >=", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdLessThan(String value) {
            addCriterion("fund_id <", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdLessThanOrEqualTo(String value) {
            addCriterion("fund_id <=", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdLike(String value) {
            addCriterion("fund_id like", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdNotLike(String value) {
            addCriterion("fund_id not like", value, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdIn(List<String> values) {
            addCriterion("fund_id in", values, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdNotIn(List<String> values) {
            addCriterion("fund_id not in", values, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdBetween(String value1, String value2) {
            addCriterion("fund_id between", value1, value2, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundIdNotBetween(String value1, String value2) {
            addCriterion("fund_id not between", value1, value2, "fundId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdIsNull() {
            addCriterion("fund_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdIsNotNull() {
            addCriterion("fund_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdEqualTo(String value) {
            addCriterion("fund_personage_id =", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdNotEqualTo(String value) {
            addCriterion("fund_personage_id <>", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdGreaterThan(String value) {
            addCriterion("fund_personage_id >", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("fund_personage_id >=", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdLessThan(String value) {
            addCriterion("fund_personage_id <", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("fund_personage_id <=", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdLike(String value) {
            addCriterion("fund_personage_id like", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdNotLike(String value) {
            addCriterion("fund_personage_id not like", value, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdIn(List<String> values) {
            addCriterion("fund_personage_id in", values, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdNotIn(List<String> values) {
            addCriterion("fund_personage_id not in", values, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdBetween(String value1, String value2) {
            addCriterion("fund_personage_id between", value1, value2, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andFundPersonageIdNotBetween(String value1, String value2) {
            addCriterion("fund_personage_id not between", value1, value2, "fundPersonageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("personage_id is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(String value) {
            addCriterion("personage_id =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(String value) {
            addCriterion("personage_id <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(String value) {
            addCriterion("personage_id >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("personage_id >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(String value) {
            addCriterion("personage_id <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("personage_id <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLike(String value) {
            addCriterion("personage_id like", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotLike(String value) {
            addCriterion("personage_id not like", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<String> values) {
            addCriterion("personage_id in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<String> values) {
            addCriterion("personage_id not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(String value1, String value2) {
            addCriterion("personage_id between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(String value1, String value2) {
            addCriterion("personage_id not between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityIsNull() {
            addCriterion("holding_equity is null");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityIsNotNull() {
            addCriterion("holding_equity is not null");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityEqualTo(BigDecimal value) {
            addCriterion("holding_equity =", value, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityNotEqualTo(BigDecimal value) {
            addCriterion("holding_equity <>", value, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityGreaterThan(BigDecimal value) {
            addCriterion("holding_equity >", value, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("holding_equity >=", value, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityLessThan(BigDecimal value) {
            addCriterion("holding_equity <", value, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityLessThanOrEqualTo(BigDecimal value) {
            addCriterion("holding_equity <=", value, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityIn(List<BigDecimal> values) {
            addCriterion("holding_equity in", values, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityNotIn(List<BigDecimal> values) {
            addCriterion("holding_equity not in", values, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("holding_equity between", value1, value2, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingEquityNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("holding_equity not between", value1, value2, "holdingEquity");
            return (Criteria) this;
        }

        public Criteria andHoldingValueIsNull() {
            addCriterion("holding_value is null");
            return (Criteria) this;
        }

        public Criteria andHoldingValueIsNotNull() {
            addCriterion("holding_value is not null");
            return (Criteria) this;
        }

        public Criteria andHoldingValueEqualTo(BigDecimal value) {
            addCriterion("holding_value =", value, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueNotEqualTo(BigDecimal value) {
            addCriterion("holding_value <>", value, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueGreaterThan(BigDecimal value) {
            addCriterion("holding_value >", value, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("holding_value >=", value, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueLessThan(BigDecimal value) {
            addCriterion("holding_value <", value, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("holding_value <=", value, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueIn(List<BigDecimal> values) {
            addCriterion("holding_value in", values, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueNotIn(List<BigDecimal> values) {
            addCriterion("holding_value not in", values, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("holding_value between", value1, value2, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingValueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("holding_value not between", value1, value2, "holdingValue");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeIsNull() {
            addCriterion("holding_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeIsNotNull() {
            addCriterion("holding_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeEqualTo(Byte value) {
            addCriterion("holding_currency_type =", value, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("holding_currency_type <>", value, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeGreaterThan(Byte value) {
            addCriterion("holding_currency_type >", value, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("holding_currency_type >=", value, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeLessThan(Byte value) {
            addCriterion("holding_currency_type <", value, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("holding_currency_type <=", value, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeIn(List<Byte> values) {
            addCriterion("holding_currency_type in", values, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("holding_currency_type not in", values, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("holding_currency_type between", value1, value2, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andHoldingCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("holding_currency_type not between", value1, value2, "holdingCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyIsNull() {
            addCriterion("effective_money is null");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyIsNotNull() {
            addCriterion("effective_money is not null");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyEqualTo(BigDecimal value) {
            addCriterion("effective_money =", value, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyNotEqualTo(BigDecimal value) {
            addCriterion("effective_money <>", value, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyGreaterThan(BigDecimal value) {
            addCriterion("effective_money >", value, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("effective_money >=", value, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyLessThan(BigDecimal value) {
            addCriterion("effective_money <", value, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyLessThanOrEqualTo(BigDecimal value) {
            addCriterion("effective_money <=", value, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyIn(List<BigDecimal> values) {
            addCriterion("effective_money in", values, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyNotIn(List<BigDecimal> values) {
            addCriterion("effective_money not in", values, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("effective_money between", value1, value2, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveMoneyNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("effective_money not between", value1, value2, "effectiveMoney");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeIsNull() {
            addCriterion("effective_currency_type is null");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeIsNotNull() {
            addCriterion("effective_currency_type is not null");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeEqualTo(Byte value) {
            addCriterion("effective_currency_type =", value, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeNotEqualTo(Byte value) {
            addCriterion("effective_currency_type <>", value, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeGreaterThan(Byte value) {
            addCriterion("effective_currency_type >", value, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("effective_currency_type >=", value, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeLessThan(Byte value) {
            addCriterion("effective_currency_type <", value, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeLessThanOrEqualTo(Byte value) {
            addCriterion("effective_currency_type <=", value, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeIn(List<Byte> values) {
            addCriterion("effective_currency_type in", values, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeNotIn(List<Byte> values) {
            addCriterion("effective_currency_type not in", values, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeBetween(Byte value1, Byte value2) {
            addCriterion("effective_currency_type between", value1, value2, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andEffectiveCurrencyTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("effective_currency_type not between", value1, value2, "effectiveCurrencyType");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeIsNull() {
            addCriterion("compute_income is null");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeIsNotNull() {
            addCriterion("compute_income is not null");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeEqualTo(BigDecimal value) {
            addCriterion("compute_income =", value, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeNotEqualTo(BigDecimal value) {
            addCriterion("compute_income <>", value, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeGreaterThan(BigDecimal value) {
            addCriterion("compute_income >", value, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("compute_income >=", value, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeLessThan(BigDecimal value) {
            addCriterion("compute_income <", value, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("compute_income <=", value, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeIn(List<BigDecimal> values) {
            addCriterion("compute_income in", values, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeNotIn(List<BigDecimal> values) {
            addCriterion("compute_income not in", values, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("compute_income between", value1, value2, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andComputeIncomeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("compute_income not between", value1, value2, "computeIncome");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}