package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCertificateService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificate;


@RestController
@RequestMapping("/companyCertificate")
public class CompanyCertificateController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyCertificateController.class);

    @Resource
    private CompanyCertificateService companyCertificateService;

    /**
	 * @api {post} / 新增companyCertificate
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyCertificate(@RequestBody CompanyCertificate companyCertificate) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCertificateService.saveCompanyCertificate(companyCertificate);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyCertificate
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyCertificate(@RequestBody CompanyCertificate companyCertificate) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCertificateService.updateCompanyCertificate(companyCertificate);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyCertificate详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyCertificate> getCompanyCertificateById(@PathVariable String id) {
        JsonResult<CompanyCertificate> jr = new JsonResult<CompanyCertificate>();
        try {
            CompanyCertificate companyCertificate  = companyCertificateService.getCompanyCertificateById(id);
            jr = buildJsonResult(companyCertificate);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyCertificate/getCompanyCertificateList 查询companyCertificate列表(不带分页)
	 * @apiGroup companyCertificate管理
	 * @apiName 查询companyCertificate列表(不带分页)
	 * @apiDescription 根据条件查询companyCertificate列表(不带分页)
	 * @apiParam {CompanyCertificate} companyCertificate companyCertificate对象
	 */
    @RequestMapping("/getCompanyCertificateList")
    public JsonResult<List<CompanyCertificate>> getCompanyCertificateList(CompanyCertificate companyCertificate) {
        JsonResult<List<CompanyCertificate>> jr = new JsonResult<List<CompanyCertificate>>();
        try {
            List<CompanyCertificate> list = this.companyCertificateService.getCompanyCertificateList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyCertificate/findCompanyCertificatePageByParam 查询companyCertificate列表(带分页)
	 * @apiGroup companyCertificate管理
	 * @apiName 查询companyCertificate列表(带分页)
	 * @apiDescription 根据条件查询companyCertificate列表(带分页)
	 * @apiParam {CompanyCertificate} companyCertificate companyCertificate对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyCertificatePageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyCertificate>> findCompanyCertificatePageByParam(@RequestBody CompanyCertificate companyCertificate, PageParam pageParam) {
        JsonResult<PageResult<CompanyCertificate>> jr = new JsonResult<PageResult<CompanyCertificate>>();
        try {
            PageResult<CompanyCertificate> page = this.companyCertificateService.findCompanyCertificatePageByParam(companyCertificate, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyCertificate/deleteCompanyCertificateById 删除companyCertificate(物理删除)
	 * @apiGroup companyCertificate管理
	 * @apiName 删除companyCertificate记录(物理删除)
	 * @apiDescription 根据主键id删除companyCertificate记录(物理删除)
	 * @apiParam {String} id companyCertificate主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyCertificateById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyCertificateById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyCertificateService.deleteCompanyCertificateById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
