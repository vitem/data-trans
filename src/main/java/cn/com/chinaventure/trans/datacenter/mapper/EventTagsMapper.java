package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventTags;
import cn.com.chinaventure.trans.datacenter.entity.EventTagsExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventTagsMapper {
    int countByExample(EventTagsExample example);

    int deleteByExample(EventTagsExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventTags record);

    int insertSelective(EventTags record);

    List<EventTags> selectByExample(EventTagsExample example);

    EventTags selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventTags record, @Param("example") EventTagsExample example);

    int updateByExample(@Param("record") EventTags record, @Param("example") EventTagsExample example);

    int updateByPrimaryKeySelective(EventTags record);

    int updateByPrimaryKey(EventTags record);
}