package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventExit;

/**
 * @author 
 *
 */
public interface EventExitService {
	
	/**
     * @api saveEventExit 新增eventExit
     * @apiGroup eventExit管理
     * @apiName  新增eventExit记录
     * @apiDescription 全量插入eventExit记录
     * @apiParam {EventExit} eventExit eventExit对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventExit(EventExit eventExit);
	
	/**
     * @api updateEventExit 修改eventExit
     * @apiGroup eventExit管理
     * @apiName  修改eventExit记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventExit} eventExit eventExit对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventExit(EventExit eventExit);
	
	/**
     * @api getEventExitById 根据eventExitid查询详情
     * @apiGroup eventExit管理
     * @apiName  查询eventExit详情
     * @apiDescription 根据主键id查询eventExit详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventExit eventExit实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventExit getEventExitById(String id);
	
	/**
     * @api getEventExitList 根据eventExit条件查询列表
     * @apiGroup eventExit管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventExit} eventExit  实体条件
     * @apiSuccess List<EventExit> eventExit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventExit> getEventExitList(EventExit eventExit);
	

	/**
     * @api getEventExitListByParam 根据eventExit条件查询列表（含排序）
     * @apiGroup eventExit管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventExit} eventExit  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventExit> eventExit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventExit> getEventExitListByParam(EventExit eventExit, String orderByStr);
	
	
	/**
     * @api findEventExitPageByParam 根据eventExit条件查询列表（分页）
     * @apiGroup eventExit管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventExit} eventExit  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventExit> eventExit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventExit> findEventExitPageByParam(EventExit eventExit, PageParam pageParam); 
	
	/**
     * @api findEventExitPageByParam 根据eventExit条件查询列表（分页，含排序）
     * @apiGroup eventExit管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventExit} eventExit  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventExit> eventExit实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventExit> findEventExitPageByParam(EventExit eventExit, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventExitById 根据eventExit的id删除(物理删除)
     * @apiGroup eventExit管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventExitById(String id);

}

