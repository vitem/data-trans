package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNotice;

/**
 * @author 
 *
 */
public interface CompanyCourtNoticeService {
	
	/**
     * @api saveCompanyCourtNotice 新增companyCourtNotice
     * @apiGroup companyCourtNotice管理
     * @apiName  新增companyCourtNotice记录
     * @apiDescription 全量插入companyCourtNotice记录
     * @apiParam {CompanyCourtNotice} companyCourtNotice companyCourtNotice对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyCourtNotice(CompanyCourtNotice companyCourtNotice);
	
	/**
     * @api updateCompanyCourtNotice 修改companyCourtNotice
     * @apiGroup companyCourtNotice管理
     * @apiName  修改companyCourtNotice记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyCourtNotice} companyCourtNotice companyCourtNotice对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyCourtNotice(CompanyCourtNotice companyCourtNotice);
	
	/**
     * @api getCompanyCourtNoticeById 根据companyCourtNoticeid查询详情
     * @apiGroup companyCourtNotice管理
     * @apiName  查询companyCourtNotice详情
     * @apiDescription 根据主键id查询companyCourtNotice详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyCourtNotice companyCourtNotice实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyCourtNotice getCompanyCourtNoticeById(String id);
	
	/**
     * @api getCompanyCourtNoticeList 根据companyCourtNotice条件查询列表
     * @apiGroup companyCourtNotice管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyCourtNotice} companyCourtNotice  实体条件
     * @apiSuccess List<CompanyCourtNotice> companyCourtNotice实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCourtNotice> getCompanyCourtNoticeList(CompanyCourtNotice companyCourtNotice);
	

	/**
     * @api getCompanyCourtNoticeListByParam 根据companyCourtNotice条件查询列表（含排序）
     * @apiGroup companyCourtNotice管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyCourtNotice} companyCourtNotice  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyCourtNotice> companyCourtNotice实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyCourtNotice> getCompanyCourtNoticeListByParam(CompanyCourtNotice companyCourtNotice, String orderByStr);
	
	
	/**
     * @api findCompanyCourtNoticePageByParam 根据companyCourtNotice条件查询列表（分页）
     * @apiGroup companyCourtNotice管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyCourtNotice} companyCourtNotice  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyCourtNotice> companyCourtNotice实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCourtNotice> findCompanyCourtNoticePageByParam(CompanyCourtNotice companyCourtNotice, PageParam pageParam); 
	
	/**
     * @api findCompanyCourtNoticePageByParam 根据companyCourtNotice条件查询列表（分页，含排序）
     * @apiGroup companyCourtNotice管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyCourtNotice} companyCourtNotice  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyCourtNotice> companyCourtNotice实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyCourtNotice> findCompanyCourtNoticePageByParam(CompanyCourtNotice companyCourtNotice, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyCourtNoticeById 根据companyCourtNotice的id删除(物理删除)
     * @apiGroup companyCourtNotice管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyCourtNoticeById(String id);

}

