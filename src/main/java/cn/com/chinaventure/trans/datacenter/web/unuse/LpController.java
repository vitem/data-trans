package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.LpService;
import cn.com.chinaventure.trans.datacenter.entity.Lp;


@RestController
@RequestMapping("/lp")
public class LpController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(LpController.class);

    @Resource
    private LpService lpService;

    /**
	 * @api {post} / 新增lp
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveLp(@RequestBody Lp lp) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.lpService.saveLp(lp);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改lp
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateLp(@RequestBody Lp lp) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.lpService.updateLp(lp);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询lp详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Lp> getLpById(@PathVariable String id) {
        JsonResult<Lp> jr = new JsonResult<Lp>();
        try {
            Lp lp  = lpService.getLpById(id);
            jr = buildJsonResult(lp);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /lp/getLpList 查询lp列表(不带分页)
	 * @apiGroup lp管理
	 * @apiName 查询lp列表(不带分页)
	 * @apiDescription 根据条件查询lp列表(不带分页)
	 * @apiParam {Lp} lp lp对象
	 */
    @RequestMapping("/getLpList")
    public JsonResult<List<Lp>> getLpList(Lp lp) {
        JsonResult<List<Lp>> jr = new JsonResult<List<Lp>>();
        try {
            List<Lp> list = this.lpService.getLpList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /lp/findLpPageByParam 查询lp列表(带分页)
	 * @apiGroup lp管理
	 * @apiName 查询lp列表(带分页)
	 * @apiDescription 根据条件查询lp列表(带分页)
	 * @apiParam {Lp} lp lp对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findLpPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Lp>> findLpPageByParam(@RequestBody Lp lp, PageParam pageParam) {
        JsonResult<PageResult<Lp>> jr = new JsonResult<PageResult<Lp>>();
        try {
            PageResult<Lp> page = this.lpService.findLpPageByParam(lp, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /lp/deleteLpById 删除lp(物理删除)
	 * @apiGroup lp管理
	 * @apiName 删除lp记录(物理删除)
	 * @apiDescription 根据主键id删除lp记录(物理删除)
	 * @apiParam {String} id lp主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteLpById",method=RequestMethod.DELETE)
    public JsonResult deleteLpById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.lpService.deleteLpById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
