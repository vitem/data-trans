package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EnterpriseOrgService;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrg;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrgExample;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrgExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EnterpriseOrgMapper;


@Service("enterpriseOrgService")
public class EnterpriseOrgServiceImpl implements EnterpriseOrgService {

	private final static Logger logger = LoggerFactory.getLogger(EnterpriseOrgServiceImpl.class);

	@Resource
	private EnterpriseOrgMapper enterpriseOrgMapper;


	@Override
	public void saveEnterpriseOrg(EnterpriseOrg enterpriseOrg) {
		if (null == enterpriseOrg) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseOrgMapper.insertSelective(enterpriseOrg);
	}

	@Override
	public void updateEnterpriseOrg(EnterpriseOrg enterpriseOrg) {
		if (null == enterpriseOrg) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseOrgMapper.updateByPrimaryKeySelective(enterpriseOrg);
	}

	@Override
	public EnterpriseOrg getEnterpriseOrgById(String id) {
		return enterpriseOrgMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EnterpriseOrg> getEnterpriseOrgList(EnterpriseOrg enterpriseOrg) {
		return this.getEnterpriseOrgListByParam(enterpriseOrg, null);
	}

	@Override
	public List<EnterpriseOrg> getEnterpriseOrgListByParam(EnterpriseOrg enterpriseOrg, String orderByStr) {
		EnterpriseOrgExample example = new EnterpriseOrgExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EnterpriseOrg> enterpriseOrgList = this.enterpriseOrgMapper.selectByExample(example);
		return enterpriseOrgList;
	}

	@Override
	public PageResult<EnterpriseOrg> findEnterpriseOrgPageByParam(EnterpriseOrg enterpriseOrg, PageParam pageParam) {
		return this.findEnterpriseOrgPageByParam(enterpriseOrg, pageParam, null);
	}

	@Override
	public PageResult<EnterpriseOrg> findEnterpriseOrgPageByParam(EnterpriseOrg enterpriseOrg, PageParam pageParam, String orderByStr) {
		PageResult<EnterpriseOrg> pageResult = new PageResult<EnterpriseOrg>();

		EnterpriseOrgExample example = new EnterpriseOrgExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EnterpriseOrg> list = null;// enterpriseOrgMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterpriseOrgById(String id) {
		this.enterpriseOrgMapper.deleteByPrimaryKey(id);
	}

}
