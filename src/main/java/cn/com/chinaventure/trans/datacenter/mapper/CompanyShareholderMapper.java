package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholder;
import cn.com.chinaventure.trans.datacenter.entity.CompanyShareholderExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyShareholderMapper {
    int countByExample(CompanyShareholderExample example);

    int deleteByExample(CompanyShareholderExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyShareholder record);

    int insertSelective(CompanyShareholder record);

    List<CompanyShareholder> selectByExample(CompanyShareholderExample example);

    CompanyShareholder selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyShareholder record, @Param("example") CompanyShareholderExample example);

    int updateByExample(@Param("record") CompanyShareholder record, @Param("example") CompanyShareholderExample example);

    int updateByPrimaryKeySelective(CompanyShareholder record);

    int updateByPrimaryKey(CompanyShareholder record);
}