package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Lp;

/**
 * @author 
 *
 */
public interface LpService {
	
	/**
     * @api saveLp 新增lp
     * @apiGroup lp管理
     * @apiName  新增lp记录
     * @apiDescription 全量插入lp记录
     * @apiParam {Lp} lp lp对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveLp(Lp lp);
	
	/**
     * @api updateLp 修改lp
     * @apiGroup lp管理
     * @apiName  修改lp记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Lp} lp lp对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateLp(Lp lp);
	
	/**
     * @api getLpById 根据lpid查询详情
     * @apiGroup lp管理
     * @apiName  查询lp详情
     * @apiDescription 根据主键id查询lp详情
     * @apiParam {String} id 主键id
     * @apiSuccess Lp lp实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Lp getLpById(String id);
	
	/**
     * @api getLpList 根据lp条件查询列表
     * @apiGroup lp管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Lp} lp  实体条件
     * @apiSuccess List<Lp> lp实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Lp> getLpList(Lp lp);
	

	/**
     * @api getLpListByParam 根据lp条件查询列表（含排序）
     * @apiGroup lp管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Lp} lp  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Lp> lp实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Lp> getLpListByParam(Lp lp, String orderByStr);
	
	
	/**
     * @api findLpPageByParam 根据lp条件查询列表（分页）
     * @apiGroup lp管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Lp} lp  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Lp> lp实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Lp> findLpPageByParam(Lp lp, PageParam pageParam); 
	
	/**
     * @api findLpPageByParam 根据lp条件查询列表（分页，含排序）
     * @apiGroup lp管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Lp} lp  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Lp> lp实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Lp> findLpPageByParam(Lp lp, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteLpById 根据lp的id删除(物理删除)
     * @apiGroup lp管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteLpById(String id);

}

