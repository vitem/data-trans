package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudge;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtJudgeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyCourtJudgeMapper {
    int countByExample(CompanyCourtJudgeExample example);

    int deleteByExample(CompanyCourtJudgeExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyCourtJudge record);

    int insertSelective(CompanyCourtJudge record);

    List<CompanyCourtJudge> selectByExample(CompanyCourtJudgeExample example);

    CompanyCourtJudge selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyCourtJudge record, @Param("example") CompanyCourtJudgeExample example);

    int updateByExample(@Param("record") CompanyCourtJudge record, @Param("example") CompanyCourtJudgeExample example);

    int updateByPrimaryKeySelective(CompanyCourtJudge record);

    int updateByPrimaryKey(CompanyCourtJudge record);
}