package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategy;

/**
 * @author 
 *
 */
public interface OrganizationStrategyService {
	
	/**
     * @api saveOrganizationStrategy 新增organizationStrategy
     * @apiGroup organizationStrategy管理
     * @apiName  新增organizationStrategy记录
     * @apiDescription 全量插入organizationStrategy记录
     * @apiParam {OrganizationStrategy} organizationStrategy organizationStrategy对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveOrganizationStrategy(OrganizationStrategy organizationStrategy);
	
	/**
     * @api updateOrganizationStrategy 修改organizationStrategy
     * @apiGroup organizationStrategy管理
     * @apiName  修改organizationStrategy记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {OrganizationStrategy} organizationStrategy organizationStrategy对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateOrganizationStrategy(OrganizationStrategy organizationStrategy);
	
	/**
     * @api getOrganizationStrategyById 根据organizationStrategyid查询详情
     * @apiGroup organizationStrategy管理
     * @apiName  查询organizationStrategy详情
     * @apiDescription 根据主键id查询organizationStrategy详情
     * @apiParam {String} id 主键id
     * @apiSuccess OrganizationStrategy organizationStrategy实体对象
     * @apiVersion 1.0.0
     * 
     */
	public OrganizationStrategy getOrganizationStrategyById(String id);
	
	/**
     * @api getOrganizationStrategyList 根据organizationStrategy条件查询列表
     * @apiGroup organizationStrategy管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {OrganizationStrategy} organizationStrategy  实体条件
     * @apiSuccess List<OrganizationStrategy> organizationStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<OrganizationStrategy> getOrganizationStrategyList(OrganizationStrategy organizationStrategy);
	

	/**
     * @api getOrganizationStrategyListByParam 根据organizationStrategy条件查询列表（含排序）
     * @apiGroup organizationStrategy管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {OrganizationStrategy} organizationStrategy  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<OrganizationStrategy> organizationStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<OrganizationStrategy> getOrganizationStrategyListByParam(OrganizationStrategy organizationStrategy, String orderByStr);
	
	
	/**
     * @api findOrganizationStrategyPageByParam 根据organizationStrategy条件查询列表（分页）
     * @apiGroup organizationStrategy管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {OrganizationStrategy} organizationStrategy  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<OrganizationStrategy> organizationStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<OrganizationStrategy> findOrganizationStrategyPageByParam(OrganizationStrategy organizationStrategy, PageParam pageParam); 
	
	/**
     * @api findOrganizationStrategyPageByParam 根据organizationStrategy条件查询列表（分页，含排序）
     * @apiGroup organizationStrategy管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {OrganizationStrategy} organizationStrategy  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<OrganizationStrategy> organizationStrategy实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<OrganizationStrategy> findOrganizationStrategyPageByParam(OrganizationStrategy organizationStrategy, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteOrganizationStrategyById 根据organizationStrategy的id删除(物理删除)
     * @apiGroup organizationStrategy管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteOrganizationStrategyById(String id);

}

