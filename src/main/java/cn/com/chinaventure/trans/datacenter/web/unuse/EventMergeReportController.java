package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeReportService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeReport;


@RestController
@RequestMapping("/eventMergeReport")
public class EventMergeReportController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventMergeReportController.class);

    @Resource
    private EventMergeReportService eventMergeReportService;

    /**
	 * @api {post} / 新增eventMergeReport
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventMergeReport(@RequestBody EventMergeReport eventMergeReport) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeReportService.saveEventMergeReport(eventMergeReport);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventMergeReport
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventMergeReport(@RequestBody EventMergeReport eventMergeReport) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeReportService.updateEventMergeReport(eventMergeReport);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventMergeReport详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventMergeReport> getEventMergeReportById(@PathVariable String id) {
        JsonResult<EventMergeReport> jr = new JsonResult<EventMergeReport>();
        try {
            EventMergeReport eventMergeReport  = eventMergeReportService.getEventMergeReportById(id);
            jr = buildJsonResult(eventMergeReport);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventMergeReport/getEventMergeReportList 查询eventMergeReport列表(不带分页)
	 * @apiGroup eventMergeReport管理
	 * @apiName 查询eventMergeReport列表(不带分页)
	 * @apiDescription 根据条件查询eventMergeReport列表(不带分页)
	 * @apiParam {EventMergeReport} eventMergeReport eventMergeReport对象
	 */
    @RequestMapping("/getEventMergeReportList")
    public JsonResult<List<EventMergeReport>> getEventMergeReportList(EventMergeReport eventMergeReport) {
        JsonResult<List<EventMergeReport>> jr = new JsonResult<List<EventMergeReport>>();
        try {
            List<EventMergeReport> list = this.eventMergeReportService.getEventMergeReportList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventMergeReport/findEventMergeReportPageByParam 查询eventMergeReport列表(带分页)
	 * @apiGroup eventMergeReport管理
	 * @apiName 查询eventMergeReport列表(带分页)
	 * @apiDescription 根据条件查询eventMergeReport列表(带分页)
	 * @apiParam {EventMergeReport} eventMergeReport eventMergeReport对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventMergeReportPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventMergeReport>> findEventMergeReportPageByParam(@RequestBody EventMergeReport eventMergeReport, PageParam pageParam) {
        JsonResult<PageResult<EventMergeReport>> jr = new JsonResult<PageResult<EventMergeReport>>();
        try {
            PageResult<EventMergeReport> page = this.eventMergeReportService.findEventMergeReportPageByParam(eventMergeReport, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventMergeReport/deleteEventMergeReportById 删除eventMergeReport(物理删除)
	 * @apiGroup eventMergeReport管理
	 * @apiName 删除eventMergeReport记录(物理删除)
	 * @apiDescription 根据主键id删除eventMergeReport记录(物理删除)
	 * @apiParam {String} id eventMergeReport主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventMergeReportById",method=RequestMethod.DELETE)
    public JsonResult deleteEventMergeReportById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventMergeReportService.deleteEventMergeReportById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
