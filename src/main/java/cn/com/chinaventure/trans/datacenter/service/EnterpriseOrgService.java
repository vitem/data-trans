package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrg;

/**
 * @author 
 *
 */
public interface EnterpriseOrgService {
	
	/**
     * @api saveEnterpriseOrg 新增enterpriseOrg
     * @apiGroup enterpriseOrg管理
     * @apiName  新增enterpriseOrg记录
     * @apiDescription 全量插入enterpriseOrg记录
     * @apiParam {EnterpriseOrg} enterpriseOrg enterpriseOrg对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterpriseOrg(EnterpriseOrg enterpriseOrg);
	
	/**
     * @api updateEnterpriseOrg 修改enterpriseOrg
     * @apiGroup enterpriseOrg管理
     * @apiName  修改enterpriseOrg记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EnterpriseOrg} enterpriseOrg enterpriseOrg对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterpriseOrg(EnterpriseOrg enterpriseOrg);
	
	/**
     * @api getEnterpriseOrgById 根据enterpriseOrgid查询详情
     * @apiGroup enterpriseOrg管理
     * @apiName  查询enterpriseOrg详情
     * @apiDescription 根据主键id查询enterpriseOrg详情
     * @apiParam {String} id 主键id
     * @apiSuccess EnterpriseOrg enterpriseOrg实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EnterpriseOrg getEnterpriseOrgById(String id);
	
	/**
     * @api getEnterpriseOrgList 根据enterpriseOrg条件查询列表
     * @apiGroup enterpriseOrg管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EnterpriseOrg} enterpriseOrg  实体条件
     * @apiSuccess List<EnterpriseOrg> enterpriseOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EnterpriseOrg> getEnterpriseOrgList(EnterpriseOrg enterpriseOrg);
	

	/**
     * @api getEnterpriseOrgListByParam 根据enterpriseOrg条件查询列表（含排序）
     * @apiGroup enterpriseOrg管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EnterpriseOrg} enterpriseOrg  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EnterpriseOrg> enterpriseOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EnterpriseOrg> getEnterpriseOrgListByParam(EnterpriseOrg enterpriseOrg, String orderByStr);
	
	
	/**
     * @api findEnterpriseOrgPageByParam 根据enterpriseOrg条件查询列表（分页）
     * @apiGroup enterpriseOrg管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EnterpriseOrg} enterpriseOrg  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EnterpriseOrg> enterpriseOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EnterpriseOrg> findEnterpriseOrgPageByParam(EnterpriseOrg enterpriseOrg, PageParam pageParam); 
	
	/**
     * @api findEnterpriseOrgPageByParam 根据enterpriseOrg条件查询列表（分页，含排序）
     * @apiGroup enterpriseOrg管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EnterpriseOrg} enterpriseOrg  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EnterpriseOrg> enterpriseOrg实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EnterpriseOrg> findEnterpriseOrgPageByParam(EnterpriseOrg enterpriseOrg, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterpriseOrgById 根据enterpriseOrg的id删除(物理删除)
     * @apiGroup enterpriseOrg管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterpriseOrgById(String id);

}

