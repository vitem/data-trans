package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeSellerService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeSeller;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeSellerExample;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeSellerExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventMergeSellerMapper;


@Service("eventMergeSellerService")
public class EventMergeSellerServiceImpl implements EventMergeSellerService {

	private final static Logger logger = LoggerFactory.getLogger(EventMergeSellerServiceImpl.class);

	@Resource
	private EventMergeSellerMapper eventMergeSellerMapper;


	@Override
	public void saveEventMergeSeller(EventMergeSeller eventMergeSeller) {
		if (null == eventMergeSeller) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeSellerMapper.insertSelective(eventMergeSeller);
	}

	@Override
	public void updateEventMergeSeller(EventMergeSeller eventMergeSeller) {
		if (null == eventMergeSeller) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeSellerMapper.updateByPrimaryKeySelective(eventMergeSeller);
	}

	@Override
	public EventMergeSeller getEventMergeSellerById(String id) {
		return eventMergeSellerMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventMergeSeller> getEventMergeSellerList(EventMergeSeller eventMergeSeller) {
		return this.getEventMergeSellerListByParam(eventMergeSeller, null);
	}

	@Override
	public List<EventMergeSeller> getEventMergeSellerListByParam(EventMergeSeller eventMergeSeller, String orderByStr) {
		EventMergeSellerExample example = new EventMergeSellerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventMergeSeller> eventMergeSellerList = this.eventMergeSellerMapper.selectByExample(example);
		return eventMergeSellerList;
	}

	@Override
	public PageResult<EventMergeSeller> findEventMergeSellerPageByParam(EventMergeSeller eventMergeSeller, PageParam pageParam) {
		return this.findEventMergeSellerPageByParam(eventMergeSeller, pageParam, null);
	}

	@Override
	public PageResult<EventMergeSeller> findEventMergeSellerPageByParam(EventMergeSeller eventMergeSeller, PageParam pageParam, String orderByStr) {
		PageResult<EventMergeSeller> pageResult = new PageResult<EventMergeSeller>();

		EventMergeSellerExample example = new EventMergeSellerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventMergeSeller> list = null;// eventMergeSellerMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventMergeSellerById(String id) {
		this.eventMergeSellerMapper.deleteByPrimaryKey(id);
	}

}
