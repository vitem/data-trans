package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCreditService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCredit;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCreditExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCreditExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyCreditMapper;


@Service("companyCreditService")
public class CompanyCreditServiceImpl implements CompanyCreditService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyCreditServiceImpl.class);

	@Resource
	private CompanyCreditMapper companyCreditMapper;


	@Override
	public void saveCompanyCredit(CompanyCredit companyCredit) {
		if (null == companyCredit) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCreditMapper.insertSelective(companyCredit);
	}

	@Override
	public void updateCompanyCredit(CompanyCredit companyCredit) {
		if (null == companyCredit) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCreditMapper.updateByPrimaryKeySelective(companyCredit);
	}

	@Override
	public CompanyCredit getCompanyCreditById(String id) {
		return companyCreditMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyCredit> getCompanyCreditList(CompanyCredit companyCredit) {
		return this.getCompanyCreditListByParam(companyCredit, null);
	}

	@Override
	public List<CompanyCredit> getCompanyCreditListByParam(CompanyCredit companyCredit, String orderByStr) {
		CompanyCreditExample example = new CompanyCreditExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyCredit> companyCreditList = this.companyCreditMapper.selectByExample(example);
		return companyCreditList;
	}

	@Override
	public PageResult<CompanyCredit> findCompanyCreditPageByParam(CompanyCredit companyCredit, PageParam pageParam) {
		return this.findCompanyCreditPageByParam(companyCredit, pageParam, null);
	}

	@Override
	public PageResult<CompanyCredit> findCompanyCreditPageByParam(CompanyCredit companyCredit, PageParam pageParam, String orderByStr) {
		PageResult<CompanyCredit> pageResult = new PageResult<CompanyCredit>();

		CompanyCreditExample example = new CompanyCreditExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyCredit> list = null;// companyCreditMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyCreditById(String id) {
		this.companyCreditMapper.deleteByPrimaryKey(id);
	}

}
