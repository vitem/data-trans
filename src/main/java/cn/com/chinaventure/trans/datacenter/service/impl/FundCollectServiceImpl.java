package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.datacenter.entity.FundCollect;
import cn.com.chinaventure.trans.datacenter.entity.FundCollectExample;
import cn.com.chinaventure.trans.datacenter.entity.FundCollectExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.FundCollectMapper;
import cn.com.chinaventure.trans.datacenter.service.FundCollectService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("fundCollectService")
public class FundCollectServiceImpl implements FundCollectService {

	private final static Logger logger = LoggerFactory.getLogger(FundCollectServiceImpl.class);

	@Resource
	private FundCollectMapper fundCollectMapper;


	@Override
	public void saveFundCollect(FundCollect fundCollect) {
		if (null == fundCollect) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.fundCollectMapper.insertSelective(fundCollect);
	}

	@Override
	public void updateFundCollect(FundCollect fundCollect) {
		if (null == fundCollect) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.fundCollectMapper.updateByPrimaryKeySelective(fundCollect);
	}

	@Override
	public FundCollect getFundCollectById(String id) {
		return fundCollectMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<FundCollect> getFundCollectList(FundCollect fundCollect) {
		return this.getFundCollectListByParam(fundCollect, null);
	}

	@Override
	public List<FundCollect> getFundCollectListByParam(FundCollect fundCollect, String orderByStr) {
		FundCollectExample example = new FundCollectExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<FundCollect> fundCollectList = this.fundCollectMapper.selectByExample(example);
		return fundCollectList;
	}

	@Override
	public PageResult<FundCollect> findFundCollectPageByParam(FundCollect fundCollect, PageParam pageParam) {
		return this.findFundCollectPageByParam(fundCollect, pageParam, null);
	}

	@Override
	public PageResult<FundCollect> findFundCollectPageByParam(FundCollect fundCollect, PageParam pageParam, String orderByStr) {
		PageResult<FundCollect> pageResult = new PageResult<FundCollect>();

		FundCollectExample example = new FundCollectExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<FundCollect> list = null;// fundCollectMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteFundCollectById(String id) {
		this.fundCollectMapper.deleteByPrimaryKey(id);
	}

}
