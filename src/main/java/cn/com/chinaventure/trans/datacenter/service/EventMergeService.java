package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventMerge;

/**
 * @author 
 *
 */
public interface EventMergeService {
	
	/**
     * @api saveEventMerge 新增eventMerge
     * @apiGroup eventMerge管理
     * @apiName  新增eventMerge记录
     * @apiDescription 全量插入eventMerge记录
     * @apiParam {EventMerge} eventMerge eventMerge对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventMerge(EventMerge eventMerge);
	
	/**
     * @api updateEventMerge 修改eventMerge
     * @apiGroup eventMerge管理
     * @apiName  修改eventMerge记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventMerge} eventMerge eventMerge对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventMerge(EventMerge eventMerge);
	
	/**
     * @api getEventMergeById 根据eventMergeid查询详情
     * @apiGroup eventMerge管理
     * @apiName  查询eventMerge详情
     * @apiDescription 根据主键id查询eventMerge详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventMerge eventMerge实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventMerge getEventMergeById(String id);
	
	/**
     * @api getEventMergeList 根据eventMerge条件查询列表
     * @apiGroup eventMerge管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventMerge} eventMerge  实体条件
     * @apiSuccess List<EventMerge> eventMerge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMerge> getEventMergeList(EventMerge eventMerge);
	

	/**
     * @api getEventMergeListByParam 根据eventMerge条件查询列表（含排序）
     * @apiGroup eventMerge管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventMerge} eventMerge  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventMerge> eventMerge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventMerge> getEventMergeListByParam(EventMerge eventMerge, String orderByStr);
	
	
	/**
     * @api findEventMergePageByParam 根据eventMerge条件查询列表（分页）
     * @apiGroup eventMerge管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventMerge} eventMerge  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventMerge> eventMerge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMerge> findEventMergePageByParam(EventMerge eventMerge, PageParam pageParam); 
	
	/**
     * @api findEventMergePageByParam 根据eventMerge条件查询列表（分页，含排序）
     * @apiGroup eventMerge管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventMerge} eventMerge  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventMerge> eventMerge实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventMerge> findEventMergePageByParam(EventMerge eventMerge, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventMergeById 根据eventMerge的id删除(物理删除)
     * @apiGroup eventMerge管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventMergeById(String id);

}

