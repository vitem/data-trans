package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyOperatorService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyOperator;


@RestController
@RequestMapping("/companyOperator")
public class CompanyOperatorController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyOperatorController.class);

    @Resource
    private CompanyOperatorService companyOperatorService;

    /**
	 * @api {post} / 新增companyOperator
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyOperator(@RequestBody CompanyOperator companyOperator) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyOperatorService.saveCompanyOperator(companyOperator);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyOperator
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyOperator(@RequestBody CompanyOperator companyOperator) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyOperatorService.updateCompanyOperator(companyOperator);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyOperator详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyOperator> getCompanyOperatorById(@PathVariable String id) {
        JsonResult<CompanyOperator> jr = new JsonResult<CompanyOperator>();
        try {
            CompanyOperator companyOperator  = companyOperatorService.getCompanyOperatorById(id);
            jr = buildJsonResult(companyOperator);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyOperator/getCompanyOperatorList 查询companyOperator列表(不带分页)
	 * @apiGroup companyOperator管理
	 * @apiName 查询companyOperator列表(不带分页)
	 * @apiDescription 根据条件查询companyOperator列表(不带分页)
	 * @apiParam {CompanyOperator} companyOperator companyOperator对象
	 */
    @RequestMapping("/getCompanyOperatorList")
    public JsonResult<List<CompanyOperator>> getCompanyOperatorList(CompanyOperator companyOperator) {
        JsonResult<List<CompanyOperator>> jr = new JsonResult<List<CompanyOperator>>();
        try {
            List<CompanyOperator> list = this.companyOperatorService.getCompanyOperatorList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyOperator/findCompanyOperatorPageByParam 查询companyOperator列表(带分页)
	 * @apiGroup companyOperator管理
	 * @apiName 查询companyOperator列表(带分页)
	 * @apiDescription 根据条件查询companyOperator列表(带分页)
	 * @apiParam {CompanyOperator} companyOperator companyOperator对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyOperatorPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyOperator>> findCompanyOperatorPageByParam(@RequestBody CompanyOperator companyOperator, PageParam pageParam) {
        JsonResult<PageResult<CompanyOperator>> jr = new JsonResult<PageResult<CompanyOperator>>();
        try {
            PageResult<CompanyOperator> page = this.companyOperatorService.findCompanyOperatorPageByParam(companyOperator, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyOperator/deleteCompanyOperatorById 删除companyOperator(物理删除)
	 * @apiGroup companyOperator管理
	 * @apiName 删除companyOperator记录(物理删除)
	 * @apiDescription 根据主键id删除companyOperator记录(物理删除)
	 * @apiParam {String} id companyOperator主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyOperatorById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyOperatorById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyOperatorService.deleteCompanyOperatorById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
