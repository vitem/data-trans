package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyright;

/**
 * @author 
 *
 */
public interface CompanySoftwareCopyrightService {
	
	/**
     * @api saveCompanySoftwareCopyright 新增companySoftwareCopyright
     * @apiGroup companySoftwareCopyright管理
     * @apiName  新增companySoftwareCopyright记录
     * @apiDescription 全量插入companySoftwareCopyright记录
     * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright companySoftwareCopyright对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanySoftwareCopyright(CompanySoftwareCopyright companySoftwareCopyright);
	
	/**
     * @api updateCompanySoftwareCopyright 修改companySoftwareCopyright
     * @apiGroup companySoftwareCopyright管理
     * @apiName  修改companySoftwareCopyright记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright companySoftwareCopyright对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanySoftwareCopyright(CompanySoftwareCopyright companySoftwareCopyright);
	
	/**
     * @api getCompanySoftwareCopyrightById 根据companySoftwareCopyrightid查询详情
     * @apiGroup companySoftwareCopyright管理
     * @apiName  查询companySoftwareCopyright详情
     * @apiDescription 根据主键id查询companySoftwareCopyright详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanySoftwareCopyright companySoftwareCopyright实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanySoftwareCopyright getCompanySoftwareCopyrightById(String id);
	
	/**
     * @api getCompanySoftwareCopyrightList 根据companySoftwareCopyright条件查询列表
     * @apiGroup companySoftwareCopyright管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright  实体条件
     * @apiSuccess List<CompanySoftwareCopyright> companySoftwareCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanySoftwareCopyright> getCompanySoftwareCopyrightList(CompanySoftwareCopyright companySoftwareCopyright);
	

	/**
     * @api getCompanySoftwareCopyrightListByParam 根据companySoftwareCopyright条件查询列表（含排序）
     * @apiGroup companySoftwareCopyright管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanySoftwareCopyright> companySoftwareCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanySoftwareCopyright> getCompanySoftwareCopyrightListByParam(CompanySoftwareCopyright companySoftwareCopyright, String orderByStr);
	
	
	/**
     * @api findCompanySoftwareCopyrightPageByParam 根据companySoftwareCopyright条件查询列表（分页）
     * @apiGroup companySoftwareCopyright管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanySoftwareCopyright> companySoftwareCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanySoftwareCopyright> findCompanySoftwareCopyrightPageByParam(CompanySoftwareCopyright companySoftwareCopyright, PageParam pageParam); 
	
	/**
     * @api findCompanySoftwareCopyrightPageByParam 根据companySoftwareCopyright条件查询列表（分页，含排序）
     * @apiGroup companySoftwareCopyright管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanySoftwareCopyright} companySoftwareCopyright  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanySoftwareCopyright> companySoftwareCopyright实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanySoftwareCopyright> findCompanySoftwareCopyrightPageByParam(CompanySoftwareCopyright companySoftwareCopyright, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanySoftwareCopyrightById 根据companySoftwareCopyright的id删除(物理删除)
     * @apiGroup companySoftwareCopyright管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanySoftwareCopyrightById(String id);

}

