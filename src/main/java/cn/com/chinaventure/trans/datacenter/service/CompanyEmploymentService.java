package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyEmployment;

/**
 * @author 
 *
 */
public interface CompanyEmploymentService {
	
	/**
     * @api saveCompanyEmployment 新增companyEmployment
     * @apiGroup companyEmployment管理
     * @apiName  新增companyEmployment记录
     * @apiDescription 全量插入companyEmployment记录
     * @apiParam {CompanyEmployment} companyEmployment companyEmployment对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyEmployment(CompanyEmployment companyEmployment);
	
	/**
     * @api updateCompanyEmployment 修改companyEmployment
     * @apiGroup companyEmployment管理
     * @apiName  修改companyEmployment记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyEmployment} companyEmployment companyEmployment对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyEmployment(CompanyEmployment companyEmployment);
	
	/**
     * @api getCompanyEmploymentById 根据companyEmploymentid查询详情
     * @apiGroup companyEmployment管理
     * @apiName  查询companyEmployment详情
     * @apiDescription 根据主键id查询companyEmployment详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyEmployment companyEmployment实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyEmployment getCompanyEmploymentById(String id);
	
	/**
     * @api getCompanyEmploymentList 根据companyEmployment条件查询列表
     * @apiGroup companyEmployment管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyEmployment} companyEmployment  实体条件
     * @apiSuccess List<CompanyEmployment> companyEmployment实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyEmployment> getCompanyEmploymentList(CompanyEmployment companyEmployment);
	

	/**
     * @api getCompanyEmploymentListByParam 根据companyEmployment条件查询列表（含排序）
     * @apiGroup companyEmployment管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyEmployment} companyEmployment  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyEmployment> companyEmployment实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyEmployment> getCompanyEmploymentListByParam(CompanyEmployment companyEmployment, String orderByStr);
	
	
	/**
     * @api findCompanyEmploymentPageByParam 根据companyEmployment条件查询列表（分页）
     * @apiGroup companyEmployment管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyEmployment} companyEmployment  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyEmployment> companyEmployment实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyEmployment> findCompanyEmploymentPageByParam(CompanyEmployment companyEmployment, PageParam pageParam); 
	
	/**
     * @api findCompanyEmploymentPageByParam 根据companyEmployment条件查询列表（分页，含排序）
     * @apiGroup companyEmployment管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyEmployment} companyEmployment  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyEmployment> companyEmployment实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyEmployment> findCompanyEmploymentPageByParam(CompanyEmployment companyEmployment, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyEmploymentById 根据companyEmployment的id删除(物理删除)
     * @apiGroup companyEmployment管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyEmploymentById(String id);

}

