package cn.com.chinaventure.trans.datacenter.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventInvestExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EventInvestExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNull() {
            addCriterion("event_id is null");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNotNull() {
            addCriterion("event_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventIdEqualTo(String value) {
            addCriterion("event_id =", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotEqualTo(String value) {
            addCriterion("event_id <>", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThan(String value) {
            addCriterion("event_id >", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_id >=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThan(String value) {
            addCriterion("event_id <", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThanOrEqualTo(String value) {
            addCriterion("event_id <=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLike(String value) {
            addCriterion("event_id like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotLike(String value) {
            addCriterion("event_id not like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdIn(List<String> values) {
            addCriterion("event_id in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotIn(List<String> values) {
            addCriterion("event_id not in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdBetween(String value1, String value2) {
            addCriterion("event_id between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotBetween(String value1, String value2) {
            addCriterion("event_id not between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andInvesTimeIsNull() {
            addCriterion("Inves_time is null");
            return (Criteria) this;
        }

        public Criteria andInvesTimeIsNotNull() {
            addCriterion("Inves_time is not null");
            return (Criteria) this;
        }

        public Criteria andInvesTimeEqualTo(Date value) {
            addCriterion("Inves_time =", value, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeNotEqualTo(Date value) {
            addCriterion("Inves_time <>", value, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeGreaterThan(Date value) {
            addCriterion("Inves_time >", value, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Inves_time >=", value, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeLessThan(Date value) {
            addCriterion("Inves_time <", value, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeLessThanOrEqualTo(Date value) {
            addCriterion("Inves_time <=", value, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeIn(List<Date> values) {
            addCriterion("Inves_time in", values, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeNotIn(List<Date> values) {
            addCriterion("Inves_time not in", values, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeBetween(Date value1, Date value2) {
            addCriterion("Inves_time between", value1, value2, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTimeNotBetween(Date value1, Date value2) {
            addCriterion("Inves_time not between", value1, value2, "invesTime");
            return (Criteria) this;
        }

        public Criteria andInvesTypeIsNull() {
            addCriterion("Inves_type is null");
            return (Criteria) this;
        }

        public Criteria andInvesTypeIsNotNull() {
            addCriterion("Inves_type is not null");
            return (Criteria) this;
        }

        public Criteria andInvesTypeEqualTo(Byte value) {
            addCriterion("Inves_type =", value, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeNotEqualTo(Byte value) {
            addCriterion("Inves_type <>", value, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeGreaterThan(Byte value) {
            addCriterion("Inves_type >", value, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("Inves_type >=", value, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeLessThan(Byte value) {
            addCriterion("Inves_type <", value, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeLessThanOrEqualTo(Byte value) {
            addCriterion("Inves_type <=", value, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeIn(List<Byte> values) {
            addCriterion("Inves_type in", values, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeNotIn(List<Byte> values) {
            addCriterion("Inves_type not in", values, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeBetween(Byte value1, Byte value2) {
            addCriterion("Inves_type between", value1, value2, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvesTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("Inves_type not between", value1, value2, "invesType");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdIsNull() {
            addCriterion("invest_bank_org_id is null");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdIsNotNull() {
            addCriterion("invest_bank_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdEqualTo(String value) {
            addCriterion("invest_bank_org_id =", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdNotEqualTo(String value) {
            addCriterion("invest_bank_org_id <>", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdGreaterThan(String value) {
            addCriterion("invest_bank_org_id >", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("invest_bank_org_id >=", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdLessThan(String value) {
            addCriterion("invest_bank_org_id <", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdLessThanOrEqualTo(String value) {
            addCriterion("invest_bank_org_id <=", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdLike(String value) {
            addCriterion("invest_bank_org_id like", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdNotLike(String value) {
            addCriterion("invest_bank_org_id not like", value, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdIn(List<String> values) {
            addCriterion("invest_bank_org_id in", values, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdNotIn(List<String> values) {
            addCriterion("invest_bank_org_id not in", values, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdBetween(String value1, String value2) {
            addCriterion("invest_bank_org_id between", value1, value2, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankOrgIdNotBetween(String value1, String value2) {
            addCriterion("invest_bank_org_id not between", value1, value2, "investBankOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdIsNull() {
            addCriterion("invest_bank_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdIsNotNull() {
            addCriterion("invest_bank_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdEqualTo(String value) {
            addCriterion("invest_bank_personage_id =", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdNotEqualTo(String value) {
            addCriterion("invest_bank_personage_id <>", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdGreaterThan(String value) {
            addCriterion("invest_bank_personage_id >", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("invest_bank_personage_id >=", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdLessThan(String value) {
            addCriterion("invest_bank_personage_id <", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("invest_bank_personage_id <=", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdLike(String value) {
            addCriterion("invest_bank_personage_id like", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdNotLike(String value) {
            addCriterion("invest_bank_personage_id not like", value, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdIn(List<String> values) {
            addCriterion("invest_bank_personage_id in", values, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdNotIn(List<String> values) {
            addCriterion("invest_bank_personage_id not in", values, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdBetween(String value1, String value2) {
            addCriterion("invest_bank_personage_id between", value1, value2, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestBankPersonageIdNotBetween(String value1, String value2) {
            addCriterion("invest_bank_personage_id not between", value1, value2, "investBankPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdIsNull() {
            addCriterion("exit_org_id is null");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdIsNotNull() {
            addCriterion("exit_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdEqualTo(String value) {
            addCriterion("exit_org_id =", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdNotEqualTo(String value) {
            addCriterion("exit_org_id <>", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdGreaterThan(String value) {
            addCriterion("exit_org_id >", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("exit_org_id >=", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdLessThan(String value) {
            addCriterion("exit_org_id <", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdLessThanOrEqualTo(String value) {
            addCriterion("exit_org_id <=", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdLike(String value) {
            addCriterion("exit_org_id like", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdNotLike(String value) {
            addCriterion("exit_org_id not like", value, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdIn(List<String> values) {
            addCriterion("exit_org_id in", values, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdNotIn(List<String> values) {
            addCriterion("exit_org_id not in", values, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdBetween(String value1, String value2) {
            addCriterion("exit_org_id between", value1, value2, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitOrgIdNotBetween(String value1, String value2) {
            addCriterion("exit_org_id not between", value1, value2, "exitOrgId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdIsNull() {
            addCriterion("exit_fund_id is null");
            return (Criteria) this;
        }

        public Criteria andExitFundIdIsNotNull() {
            addCriterion("exit_fund_id is not null");
            return (Criteria) this;
        }

        public Criteria andExitFundIdEqualTo(String value) {
            addCriterion("exit_fund_id =", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdNotEqualTo(String value) {
            addCriterion("exit_fund_id <>", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdGreaterThan(String value) {
            addCriterion("exit_fund_id >", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdGreaterThanOrEqualTo(String value) {
            addCriterion("exit_fund_id >=", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdLessThan(String value) {
            addCriterion("exit_fund_id <", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdLessThanOrEqualTo(String value) {
            addCriterion("exit_fund_id <=", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdLike(String value) {
            addCriterion("exit_fund_id like", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdNotLike(String value) {
            addCriterion("exit_fund_id not like", value, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdIn(List<String> values) {
            addCriterion("exit_fund_id in", values, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdNotIn(List<String> values) {
            addCriterion("exit_fund_id not in", values, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdBetween(String value1, String value2) {
            addCriterion("exit_fund_id between", value1, value2, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitFundIdNotBetween(String value1, String value2) {
            addCriterion("exit_fund_id not between", value1, value2, "exitFundId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdIsNull() {
            addCriterion("exit_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdIsNotNull() {
            addCriterion("exit_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdEqualTo(String value) {
            addCriterion("exit_personage_id =", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdNotEqualTo(String value) {
            addCriterion("exit_personage_id <>", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdGreaterThan(String value) {
            addCriterion("exit_personage_id >", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("exit_personage_id >=", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdLessThan(String value) {
            addCriterion("exit_personage_id <", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("exit_personage_id <=", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdLike(String value) {
            addCriterion("exit_personage_id like", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdNotLike(String value) {
            addCriterion("exit_personage_id not like", value, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdIn(List<String> values) {
            addCriterion("exit_personage_id in", values, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdNotIn(List<String> values) {
            addCriterion("exit_personage_id not in", values, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdBetween(String value1, String value2) {
            addCriterion("exit_personage_id between", value1, value2, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitPersonageIdNotBetween(String value1, String value2) {
            addCriterion("exit_personage_id not between", value1, value2, "exitPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdIsNull() {
            addCriterion("exit_natural_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdIsNotNull() {
            addCriterion("exit_natural_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdEqualTo(String value) {
            addCriterion("exit_natural_personage_id =", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdNotEqualTo(String value) {
            addCriterion("exit_natural_personage_id <>", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdGreaterThan(String value) {
            addCriterion("exit_natural_personage_id >", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("exit_natural_personage_id >=", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdLessThan(String value) {
            addCriterion("exit_natural_personage_id <", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("exit_natural_personage_id <=", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdLike(String value) {
            addCriterion("exit_natural_personage_id like", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdNotLike(String value) {
            addCriterion("exit_natural_personage_id not like", value, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdIn(List<String> values) {
            addCriterion("exit_natural_personage_id in", values, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdNotIn(List<String> values) {
            addCriterion("exit_natural_personage_id not in", values, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdBetween(String value1, String value2) {
            addCriterion("exit_natural_personage_id between", value1, value2, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andExitNaturalPersonageIdNotBetween(String value1, String value2) {
            addCriterion("exit_natural_personage_id not between", value1, value2, "exitNaturalPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIsNull() {
            addCriterion("law_org_id is null");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIsNotNull() {
            addCriterion("law_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdEqualTo(String value) {
            addCriterion("law_org_id =", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotEqualTo(String value) {
            addCriterion("law_org_id <>", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdGreaterThan(String value) {
            addCriterion("law_org_id >", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("law_org_id >=", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLessThan(String value) {
            addCriterion("law_org_id <", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLessThanOrEqualTo(String value) {
            addCriterion("law_org_id <=", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLike(String value) {
            addCriterion("law_org_id like", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotLike(String value) {
            addCriterion("law_org_id not like", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIn(List<String> values) {
            addCriterion("law_org_id in", values, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotIn(List<String> values) {
            addCriterion("law_org_id not in", values, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdBetween(String value1, String value2) {
            addCriterion("law_org_id between", value1, value2, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotBetween(String value1, String value2) {
            addCriterion("law_org_id not between", value1, value2, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIsNull() {
            addCriterion("law_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIsNotNull() {
            addCriterion("law_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdEqualTo(String value) {
            addCriterion("law_personage_id =", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotEqualTo(String value) {
            addCriterion("law_personage_id <>", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdGreaterThan(String value) {
            addCriterion("law_personage_id >", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("law_personage_id >=", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLessThan(String value) {
            addCriterion("law_personage_id <", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("law_personage_id <=", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLike(String value) {
            addCriterion("law_personage_id like", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotLike(String value) {
            addCriterion("law_personage_id not like", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIn(List<String> values) {
            addCriterion("law_personage_id in", values, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotIn(List<String> values) {
            addCriterion("law_personage_id not in", values, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdBetween(String value1, String value2) {
            addCriterion("law_personage_id between", value1, value2, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotBetween(String value1, String value2) {
            addCriterion("law_personage_id not between", value1, value2, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIsNull() {
            addCriterion("accounting_org_id is null");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIsNotNull() {
            addCriterion("accounting_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdEqualTo(String value) {
            addCriterion("accounting_org_id =", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotEqualTo(String value) {
            addCriterion("accounting_org_id <>", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdGreaterThan(String value) {
            addCriterion("accounting_org_id >", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_org_id >=", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLessThan(String value) {
            addCriterion("accounting_org_id <", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLessThanOrEqualTo(String value) {
            addCriterion("accounting_org_id <=", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLike(String value) {
            addCriterion("accounting_org_id like", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotLike(String value) {
            addCriterion("accounting_org_id not like", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIn(List<String> values) {
            addCriterion("accounting_org_id in", values, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotIn(List<String> values) {
            addCriterion("accounting_org_id not in", values, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdBetween(String value1, String value2) {
            addCriterion("accounting_org_id between", value1, value2, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotBetween(String value1, String value2) {
            addCriterion("accounting_org_id not between", value1, value2, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIsNull() {
            addCriterion("accounting_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIsNotNull() {
            addCriterion("accounting_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdEqualTo(String value) {
            addCriterion("accounting_personage_id =", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotEqualTo(String value) {
            addCriterion("accounting_personage_id <>", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdGreaterThan(String value) {
            addCriterion("accounting_personage_id >", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_personage_id >=", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLessThan(String value) {
            addCriterion("accounting_personage_id <", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("accounting_personage_id <=", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLike(String value) {
            addCriterion("accounting_personage_id like", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotLike(String value) {
            addCriterion("accounting_personage_id not like", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIn(List<String> values) {
            addCriterion("accounting_personage_id in", values, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotIn(List<String> values) {
            addCriterion("accounting_personage_id not in", values, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdBetween(String value1, String value2) {
            addCriterion("accounting_personage_id between", value1, value2, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotBetween(String value1, String value2) {
            addCriterion("accounting_personage_id not between", value1, value2, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}