package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyAuction;

/**
 * @author 
 *
 */
public interface CompanyAuctionService {
	
	/**
     * @api saveCompanyAuction 新增companyAuction
     * @apiGroup companyAuction管理
     * @apiName  新增companyAuction记录
     * @apiDescription 全量插入companyAuction记录
     * @apiParam {CompanyAuction} companyAuction companyAuction对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyAuction(CompanyAuction companyAuction);
	
	/**
     * @api updateCompanyAuction 修改companyAuction
     * @apiGroup companyAuction管理
     * @apiName  修改companyAuction记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyAuction} companyAuction companyAuction对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyAuction(CompanyAuction companyAuction);
	
	/**
     * @api getCompanyAuctionById 根据companyAuctionid查询详情
     * @apiGroup companyAuction管理
     * @apiName  查询companyAuction详情
     * @apiDescription 根据主键id查询companyAuction详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyAuction companyAuction实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyAuction getCompanyAuctionById(String id);
	
	/**
     * @api getCompanyAuctionList 根据companyAuction条件查询列表
     * @apiGroup companyAuction管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyAuction} companyAuction  实体条件
     * @apiSuccess List<CompanyAuction> companyAuction实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyAuction> getCompanyAuctionList(CompanyAuction companyAuction);
	

	/**
     * @api getCompanyAuctionListByParam 根据companyAuction条件查询列表（含排序）
     * @apiGroup companyAuction管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyAuction} companyAuction  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyAuction> companyAuction实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyAuction> getCompanyAuctionListByParam(CompanyAuction companyAuction, String orderByStr);
	
	
	/**
     * @api findCompanyAuctionPageByParam 根据companyAuction条件查询列表（分页）
     * @apiGroup companyAuction管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyAuction} companyAuction  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyAuction> companyAuction实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyAuction> findCompanyAuctionPageByParam(CompanyAuction companyAuction, PageParam pageParam); 
	
	/**
     * @api findCompanyAuctionPageByParam 根据companyAuction条件查询列表（分页，含排序）
     * @apiGroup companyAuction管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyAuction} companyAuction  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyAuction> companyAuction实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyAuction> findCompanyAuctionPageByParam(CompanyAuction companyAuction, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyAuctionById 根据companyAuction的id删除(物理删除)
     * @apiGroup companyAuction管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyAuctionById(String id);

}

