package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategy;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategyExample;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategyExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.OrganizationStrategyMapper;
import cn.com.chinaventure.trans.datacenter.service.OrganizationStrategyService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("organizationStrategyService")
public class OrganizationStrategyServiceImpl implements OrganizationStrategyService {

	private final static Logger logger = LoggerFactory.getLogger(OrganizationStrategyServiceImpl.class);

	@Resource
	private OrganizationStrategyMapper organizationStrategyMapper;


	@Override
	public void saveOrganizationStrategy(OrganizationStrategy organizationStrategy) {
		if (null == organizationStrategy) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.organizationStrategyMapper.insertSelective(organizationStrategy);
	}

	@Override
	public void updateOrganizationStrategy(OrganizationStrategy organizationStrategy) {
		if (null == organizationStrategy) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.organizationStrategyMapper.updateByPrimaryKeySelective(organizationStrategy);
	}

	@Override
	public OrganizationStrategy getOrganizationStrategyById(String id) {
		return organizationStrategyMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<OrganizationStrategy> getOrganizationStrategyList(OrganizationStrategy organizationStrategy) {
		return this.getOrganizationStrategyListByParam(organizationStrategy, null);
	}

	@Override
	public List<OrganizationStrategy> getOrganizationStrategyListByParam(OrganizationStrategy organizationStrategy, String orderByStr) {
		OrganizationStrategyExample example = new OrganizationStrategyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<OrganizationStrategy> organizationStrategyList = this.organizationStrategyMapper.selectByExample(example);
		return organizationStrategyList;
	}

	@Override
	public PageResult<OrganizationStrategy> findOrganizationStrategyPageByParam(OrganizationStrategy organizationStrategy, PageParam pageParam) {
		return this.findOrganizationStrategyPageByParam(organizationStrategy, pageParam, null);
	}

	@Override
	public PageResult<OrganizationStrategy> findOrganizationStrategyPageByParam(OrganizationStrategy organizationStrategy, PageParam pageParam, String orderByStr) {
		PageResult<OrganizationStrategy> pageResult = new PageResult<OrganizationStrategy>();

		OrganizationStrategyExample example = new OrganizationStrategyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<OrganizationStrategy> list = null;// organizationStrategyMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteOrganizationStrategyById(String id) {
		this.organizationStrategyMapper.deleteByPrimaryKey(id);
	}

}
