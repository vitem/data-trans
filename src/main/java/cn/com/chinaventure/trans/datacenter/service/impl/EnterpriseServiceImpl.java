package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EnterpriseService;
import cn.com.chinaventure.trans.datacenter.entity.Enterprise;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseExample;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EnterpriseMapper;


@Service("enterpriseService")
public class EnterpriseServiceImpl implements EnterpriseService {

	private final static Logger logger = LoggerFactory.getLogger(EnterpriseServiceImpl.class);

	@Resource
	private EnterpriseMapper enterpriseMapper;


	@Override
	public void saveEnterprise(Enterprise enterprise) {
		if (null == enterprise) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseMapper.insertSelective(enterprise);
	}

	@Override
	public void updateEnterprise(Enterprise enterprise) {
		if (null == enterprise) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseMapper.updateByPrimaryKeySelective(enterprise);
	}

	@Override
	public Enterprise getEnterpriseById(String id) {
		return enterpriseMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise> getEnterpriseList(Enterprise enterprise) {
		return this.getEnterpriseListByParam(enterprise, null);
	}

	@Override
	public List<Enterprise> getEnterpriseListByParam(Enterprise enterprise, String orderByStr) {
		EnterpriseExample example = new EnterpriseExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise> enterpriseList = this.enterpriseMapper.selectByExample(example);
		return enterpriseList;
	}

	@Override
	public PageResult<Enterprise> findEnterprisePageByParam(Enterprise enterprise, PageParam pageParam) {
		return this.findEnterprisePageByParam(enterprise, pageParam, null);
	}

	@Override
	public PageResult<Enterprise> findEnterprisePageByParam(Enterprise enterprise, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise> pageResult = new PageResult<Enterprise>();

		EnterpriseExample example = new EnterpriseExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise> list = null;// enterpriseMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterpriseById(String id) {
		this.enterpriseMapper.deleteByPrimaryKey(id);
	}

}
