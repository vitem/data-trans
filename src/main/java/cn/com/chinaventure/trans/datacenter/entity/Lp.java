package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class Lp implements Serializable {
    private String id;

    private String companyId;

    private String name;

    /**
     * 管理的资金
     */
    private BigDecimal managementMoney;

    private Byte currencyType;

    /**
     * 是否开放投资 1是 2否
     */
    private Byte openInvestType;

    /**
     * 基本来源 1, 中资 2, 外资 3, 中/外资
     */
    private Byte capitalFromType;

    /**
     * 审核状态 1，已审核  2，未审核
     */
    private Byte verifyStatus;

    /**
     * 审核时间
     */
    private Date verifyTime;

    /**
     * 审核人
     */
    private String verifyUserId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 是否在发改委备案  1是 2否   (ndrc=National Development and Reform Commission)
     */
    private Byte ndrcRecordType;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public BigDecimal getManagementMoney() {
        return managementMoney;
    }

    public void setManagementMoney(BigDecimal managementMoney) {
        this.managementMoney = managementMoney;
    }

    public Byte getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(Byte currencyType) {
        this.currencyType = currencyType;
    }

    public Byte getOpenInvestType() {
        return openInvestType;
    }

    public void setOpenInvestType(Byte openInvestType) {
        this.openInvestType = openInvestType;
    }

    public Byte getCapitalFromType() {
        return capitalFromType;
    }

    public void setCapitalFromType(Byte capitalFromType) {
        this.capitalFromType = capitalFromType;
    }

    public Byte getVerifyStatus() {
        return verifyStatus;
    }

    public void setVerifyStatus(Byte verifyStatus) {
        this.verifyStatus = verifyStatus;
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public String getVerifyUserId() {
        return verifyUserId;
    }

    public void setVerifyUserId(String verifyUserId) {
        this.verifyUserId = verifyUserId == null ? null : verifyUserId.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public Byte getNdrcRecordType() {
        return ndrcRecordType;
    }

    public void setNdrcRecordType(Byte ndrcRecordType) {
        this.ndrcRecordType = ndrcRecordType;
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", companyId=").append(companyId);
        sb.append(", name=").append(name);
        sb.append(", managementMoney=").append(managementMoney);
        sb.append(", currencyType=").append(currencyType);
        sb.append(", openInvestType=").append(openInvestType);
        sb.append(", capitalFromType=").append(capitalFromType);
        sb.append(", verifyStatus=").append(verifyStatus);
        sb.append(", verifyTime=").append(verifyTime);
        sb.append(", verifyUserId=").append(verifyUserId);
        sb.append(", remark=").append(remark);
        sb.append(", ndrcRecordType=").append(ndrcRecordType);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}