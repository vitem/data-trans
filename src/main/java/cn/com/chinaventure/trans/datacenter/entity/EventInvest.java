package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.util.Date;

public class EventInvest implements Serializable {
    private String id;

    private String eventId;

    /**
     * 融资时间
     */
    private Date invesTime;

    /**
     * 融资类型 还未定义
     */
    private Byte invesType;

    /**
     * 投行
     */
    private String investBankOrgId;

    /**
     * 投行人物
     */
    private String investBankPersonageId;

    /**
     * 退出方
     */
    private String exitOrgId;

    /**
     * 退出方基金
     */
    private String exitFundId;

    /**
     * 退出方人物
     */
    private String exitPersonageId;

    /**
     * 退出方自然人
     */
    private String exitNaturalPersonageId;

    /**
     * 律师事务所
     */
    private String lawOrgId;

    /**
     * 律师事务所人物
     */
    private String lawPersonageId;

    /**
     * 会计事务所
     */
    private String accountingOrgId;

    /**
     * 会计事务所人物
     */
    private String accountingPersonageId;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId == null ? null : eventId.trim();
    }

    public Date getInvesTime() {
        return invesTime;
    }

    public void setInvesTime(Date invesTime) {
        this.invesTime = invesTime;
    }

    public Byte getInvesType() {
        return invesType;
    }

    public void setInvesType(Byte invesType) {
        this.invesType = invesType;
    }

    public String getInvestBankOrgId() {
        return investBankOrgId;
    }

    public void setInvestBankOrgId(String investBankOrgId) {
        this.investBankOrgId = investBankOrgId == null ? null : investBankOrgId.trim();
    }

    public String getInvestBankPersonageId() {
        return investBankPersonageId;
    }

    public void setInvestBankPersonageId(String investBankPersonageId) {
        this.investBankPersonageId = investBankPersonageId == null ? null : investBankPersonageId.trim();
    }

    public String getExitOrgId() {
        return exitOrgId;
    }

    public void setExitOrgId(String exitOrgId) {
        this.exitOrgId = exitOrgId == null ? null : exitOrgId.trim();
    }

    public String getExitFundId() {
        return exitFundId;
    }

    public void setExitFundId(String exitFundId) {
        this.exitFundId = exitFundId == null ? null : exitFundId.trim();
    }

    public String getExitPersonageId() {
        return exitPersonageId;
    }

    public void setExitPersonageId(String exitPersonageId) {
        this.exitPersonageId = exitPersonageId == null ? null : exitPersonageId.trim();
    }

    public String getExitNaturalPersonageId() {
        return exitNaturalPersonageId;
    }

    public void setExitNaturalPersonageId(String exitNaturalPersonageId) {
        this.exitNaturalPersonageId = exitNaturalPersonageId == null ? null : exitNaturalPersonageId.trim();
    }

    public String getLawOrgId() {
        return lawOrgId;
    }

    public void setLawOrgId(String lawOrgId) {
        this.lawOrgId = lawOrgId == null ? null : lawOrgId.trim();
    }

    public String getLawPersonageId() {
        return lawPersonageId;
    }

    public void setLawPersonageId(String lawPersonageId) {
        this.lawPersonageId = lawPersonageId == null ? null : lawPersonageId.trim();
    }

    public String getAccountingOrgId() {
        return accountingOrgId;
    }

    public void setAccountingOrgId(String accountingOrgId) {
        this.accountingOrgId = accountingOrgId == null ? null : accountingOrgId.trim();
    }

    public String getAccountingPersonageId() {
        return accountingPersonageId;
    }

    public void setAccountingPersonageId(String accountingPersonageId) {
        this.accountingPersonageId = accountingPersonageId == null ? null : accountingPersonageId.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventId=").append(eventId);
        sb.append(", invesTime=").append(invesTime);
        sb.append(", invesType=").append(invesType);
        sb.append(", investBankOrgId=").append(investBankOrgId);
        sb.append(", investBankPersonageId=").append(investBankPersonageId);
        sb.append(", exitOrgId=").append(exitOrgId);
        sb.append(", exitFundId=").append(exitFundId);
        sb.append(", exitPersonageId=").append(exitPersonageId);
        sb.append(", exitNaturalPersonageId=").append(exitNaturalPersonageId);
        sb.append(", lawOrgId=").append(lawOrgId);
        sb.append(", lawPersonageId=").append(lawPersonageId);
        sb.append(", accountingOrgId=").append(accountingOrgId);
        sb.append(", accountingPersonageId=").append(accountingPersonageId);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}