package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EnterpriseOrgService;
import cn.com.chinaventure.trans.datacenter.entity.EnterpriseOrg;


@RestController
@RequestMapping("/enterpriseOrg")
public class EnterpriseOrgController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EnterpriseOrgController.class);

    @Resource
    private EnterpriseOrgService enterpriseOrgService;

    /**
	 * @api {post} / 新增enterpriseOrg
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEnterpriseOrg(@RequestBody EnterpriseOrg enterpriseOrg) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.enterpriseOrgService.saveEnterpriseOrg(enterpriseOrg);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改enterpriseOrg
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEnterpriseOrg(@RequestBody EnterpriseOrg enterpriseOrg) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.enterpriseOrgService.updateEnterpriseOrg(enterpriseOrg);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询enterpriseOrg详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EnterpriseOrg> getEnterpriseOrgById(@PathVariable String id) {
        JsonResult<EnterpriseOrg> jr = new JsonResult<EnterpriseOrg>();
        try {
            EnterpriseOrg enterpriseOrg  = enterpriseOrgService.getEnterpriseOrgById(id);
            jr = buildJsonResult(enterpriseOrg);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /enterpriseOrg/getEnterpriseOrgList 查询enterpriseOrg列表(不带分页)
	 * @apiGroup enterpriseOrg管理
	 * @apiName 查询enterpriseOrg列表(不带分页)
	 * @apiDescription 根据条件查询enterpriseOrg列表(不带分页)
	 * @apiParam {EnterpriseOrg} enterpriseOrg enterpriseOrg对象
	 */
    @RequestMapping("/getEnterpriseOrgList")
    public JsonResult<List<EnterpriseOrg>> getEnterpriseOrgList(EnterpriseOrg enterpriseOrg) {
        JsonResult<List<EnterpriseOrg>> jr = new JsonResult<List<EnterpriseOrg>>();
        try {
            List<EnterpriseOrg> list = this.enterpriseOrgService.getEnterpriseOrgList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /enterpriseOrg/findEnterpriseOrgPageByParam 查询enterpriseOrg列表(带分页)
	 * @apiGroup enterpriseOrg管理
	 * @apiName 查询enterpriseOrg列表(带分页)
	 * @apiDescription 根据条件查询enterpriseOrg列表(带分页)
	 * @apiParam {EnterpriseOrg} enterpriseOrg enterpriseOrg对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEnterpriseOrgPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EnterpriseOrg>> findEnterpriseOrgPageByParam(@RequestBody EnterpriseOrg enterpriseOrg, PageParam pageParam) {
        JsonResult<PageResult<EnterpriseOrg>> jr = new JsonResult<PageResult<EnterpriseOrg>>();
        try {
            PageResult<EnterpriseOrg> page = this.enterpriseOrgService.findEnterpriseOrgPageByParam(enterpriseOrg, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /enterpriseOrg/deleteEnterpriseOrgById 删除enterpriseOrg(物理删除)
	 * @apiGroup enterpriseOrg管理
	 * @apiName 删除enterpriseOrg记录(物理删除)
	 * @apiDescription 根据主键id删除enterpriseOrg记录(物理删除)
	 * @apiParam {String} id enterpriseOrg主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEnterpriseOrgById",method=RequestMethod.DELETE)
    public JsonResult deleteEnterpriseOrgById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.enterpriseOrgService.deleteEnterpriseOrgById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
