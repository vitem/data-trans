package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyPatentService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyPatent;
import cn.com.chinaventure.trans.datacenter.entity.CompanyPatentExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyPatentExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyPatentMapper;


@Service("companyPatentService")
public class CompanyPatentServiceImpl implements CompanyPatentService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyPatentServiceImpl.class);

	@Resource
	private CompanyPatentMapper companyPatentMapper;


	@Override
	public void saveCompanyPatent(CompanyPatent companyPatent) {
		if (null == companyPatent) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyPatentMapper.insertSelective(companyPatent);
	}

	@Override
	public void updateCompanyPatent(CompanyPatent companyPatent) {
		if (null == companyPatent) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyPatentMapper.updateByPrimaryKeySelective(companyPatent);
	}

	@Override
	public CompanyPatent getCompanyPatentById(String id) {
		return companyPatentMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyPatent> getCompanyPatentList(CompanyPatent companyPatent) {
		return this.getCompanyPatentListByParam(companyPatent, null);
	}

	@Override
	public List<CompanyPatent> getCompanyPatentListByParam(CompanyPatent companyPatent, String orderByStr) {
		CompanyPatentExample example = new CompanyPatentExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyPatent> companyPatentList = this.companyPatentMapper.selectByExample(example);
		return companyPatentList;
	}

	@Override
	public PageResult<CompanyPatent> findCompanyPatentPageByParam(CompanyPatent companyPatent, PageParam pageParam) {
		return this.findCompanyPatentPageByParam(companyPatent, pageParam, null);
	}

	@Override
	public PageResult<CompanyPatent> findCompanyPatentPageByParam(CompanyPatent companyPatent, PageParam pageParam, String orderByStr) {
		PageResult<CompanyPatent> pageResult = new PageResult<CompanyPatent>();

		CompanyPatentExample example = new CompanyPatentExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyPatent> list = null;// companyPatentMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyPatentById(String id) {
		this.companyPatentMapper.deleteByPrimaryKey(id);
	}

}
