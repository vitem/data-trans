package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeService;
import cn.com.chinaventure.trans.datacenter.entity.EventMerge;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeExample;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventMergeMapper;


@Service("eventMergeService")
public class EventMergeServiceImpl implements EventMergeService {

	private final static Logger logger = LoggerFactory.getLogger(EventMergeServiceImpl.class);

	@Resource
	private EventMergeMapper eventMergeMapper;


	@Override
	public void saveEventMerge(EventMerge eventMerge) {
		if (null == eventMerge) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeMapper.insertSelective(eventMerge);
	}

	@Override
	public void updateEventMerge(EventMerge eventMerge) {
		if (null == eventMerge) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeMapper.updateByPrimaryKeySelective(eventMerge);
	}

	@Override
	public EventMerge getEventMergeById(String id) {
		return eventMergeMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventMerge> getEventMergeList(EventMerge eventMerge) {
		return this.getEventMergeListByParam(eventMerge, null);
	}

	@Override
	public List<EventMerge> getEventMergeListByParam(EventMerge eventMerge, String orderByStr) {
		EventMergeExample example = new EventMergeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventMerge> eventMergeList = this.eventMergeMapper.selectByExample(example);
		return eventMergeList;
	}

	@Override
	public PageResult<EventMerge> findEventMergePageByParam(EventMerge eventMerge, PageParam pageParam) {
		return this.findEventMergePageByParam(eventMerge, pageParam, null);
	}

	@Override
	public PageResult<EventMerge> findEventMergePageByParam(EventMerge eventMerge, PageParam pageParam, String orderByStr) {
		PageResult<EventMerge> pageResult = new PageResult<EventMerge>();

		EventMergeExample example = new EventMergeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventMerge> list = null;// eventMergeMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventMergeById(String id) {
		this.eventMergeMapper.deleteByPrimaryKey(id);
	}

}
