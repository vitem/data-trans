package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;

public class PersonageWithBLOBs extends Personage implements Serializable {
    private String cnDescription;

    private String enDescription;

    private String glory;

    private static final long serialVersionUID = 1L;

    public String getCnDescription() {
        return cnDescription;
    }

    public void setCnDescription(String cnDescription) {
        this.cnDescription = cnDescription == null ? null : cnDescription.trim();
    }

    public String getEnDescription() {
        return enDescription;
    }

    public void setEnDescription(String enDescription) {
        this.enDescription = enDescription == null ? null : enDescription.trim();
    }

    public String getGlory() {
        return glory;
    }

    public void setGlory(String glory) {
        this.glory = glory == null ? null : glory.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", cnDescription=").append(cnDescription);
        sb.append(", enDescription=").append(enDescription);
        sb.append(", glory=").append(glory);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}