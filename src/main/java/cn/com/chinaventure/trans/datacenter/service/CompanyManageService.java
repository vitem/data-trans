package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.CompanyManage;

/**
 * @author 
 *
 */
public interface CompanyManageService {
	
	/**
     * @api saveCompanyManage 新增companyManage
     * @apiGroup companyManage管理
     * @apiName  新增companyManage记录
     * @apiDescription 全量插入companyManage记录
     * @apiParam {CompanyManage} companyManage companyManage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveCompanyManage(CompanyManage companyManage);
	
	/**
     * @api updateCompanyManage 修改companyManage
     * @apiGroup companyManage管理
     * @apiName  修改companyManage记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {CompanyManage} companyManage companyManage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateCompanyManage(CompanyManage companyManage);
	
	/**
     * @api getCompanyManageById 根据companyManageid查询详情
     * @apiGroup companyManage管理
     * @apiName  查询companyManage详情
     * @apiDescription 根据主键id查询companyManage详情
     * @apiParam {String} id 主键id
     * @apiSuccess CompanyManage companyManage实体对象
     * @apiVersion 1.0.0
     * 
     */
	public CompanyManage getCompanyManageById(String id);
	
	/**
     * @api getCompanyManageList 根据companyManage条件查询列表
     * @apiGroup companyManage管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {CompanyManage} companyManage  实体条件
     * @apiSuccess List<CompanyManage> companyManage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyManage> getCompanyManageList(CompanyManage companyManage);
	

	/**
     * @api getCompanyManageListByParam 根据companyManage条件查询列表（含排序）
     * @apiGroup companyManage管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {CompanyManage} companyManage  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<CompanyManage> companyManage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<CompanyManage> getCompanyManageListByParam(CompanyManage companyManage, String orderByStr);
	
	
	/**
     * @api findCompanyManagePageByParam 根据companyManage条件查询列表（分页）
     * @apiGroup companyManage管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {CompanyManage} companyManage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<CompanyManage> companyManage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyManage> findCompanyManagePageByParam(CompanyManage companyManage, PageParam pageParam); 
	
	/**
     * @api findCompanyManagePageByParam 根据companyManage条件查询列表（分页，含排序）
     * @apiGroup companyManage管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {CompanyManage} companyManage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<CompanyManage> companyManage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<CompanyManage> findCompanyManagePageByParam(CompanyManage companyManage, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteCompanyManageById 根据companyManage的id删除(物理删除)
     * @apiGroup companyManage管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteCompanyManageById(String id);

}

