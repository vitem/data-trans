package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventMergeBuyerService;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyer;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyerExample;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeBuyerExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventMergeBuyerMapper;


@Service("eventMergeBuyerService")
public class EventMergeBuyerServiceImpl implements EventMergeBuyerService {

	private final static Logger logger = LoggerFactory.getLogger(EventMergeBuyerServiceImpl.class);

	@Resource
	private EventMergeBuyerMapper eventMergeBuyerMapper;


	@Override
	public void saveEventMergeBuyer(EventMergeBuyer eventMergeBuyer) {
		if (null == eventMergeBuyer) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeBuyerMapper.insertSelective(eventMergeBuyer);
	}

	@Override
	public void updateEventMergeBuyer(EventMergeBuyer eventMergeBuyer) {
		if (null == eventMergeBuyer) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMergeBuyerMapper.updateByPrimaryKeySelective(eventMergeBuyer);
	}

	@Override
	public EventMergeBuyer getEventMergeBuyerById(String id) {
		return eventMergeBuyerMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EventMergeBuyer> getEventMergeBuyerList(EventMergeBuyer eventMergeBuyer) {
		return this.getEventMergeBuyerListByParam(eventMergeBuyer, null);
	}

	@Override
	public List<EventMergeBuyer> getEventMergeBuyerListByParam(EventMergeBuyer eventMergeBuyer, String orderByStr) {
		EventMergeBuyerExample example = new EventMergeBuyerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EventMergeBuyer> eventMergeBuyerList = this.eventMergeBuyerMapper.selectByExample(example);
		return eventMergeBuyerList;
	}

	@Override
	public PageResult<EventMergeBuyer> findEventMergeBuyerPageByParam(EventMergeBuyer eventMergeBuyer, PageParam pageParam) {
		return this.findEventMergeBuyerPageByParam(eventMergeBuyer, pageParam, null);
	}

	@Override
	public PageResult<EventMergeBuyer> findEventMergeBuyerPageByParam(EventMergeBuyer eventMergeBuyer, PageParam pageParam, String orderByStr) {
		PageResult<EventMergeBuyer> pageResult = new PageResult<EventMergeBuyer>();

		EventMergeBuyerExample example = new EventMergeBuyerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EventMergeBuyer> list = null;// eventMergeBuyerMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventMergeBuyerById(String id) {
		this.eventMergeBuyerMapper.deleteByPrimaryKey(id);
	}

}
