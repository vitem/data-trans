package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCopyrightService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyright;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyrightExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCopyrightExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyCopyrightMapper;


@Service("companyCopyrightService")
public class CompanyCopyrightServiceImpl implements CompanyCopyrightService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyCopyrightServiceImpl.class);

	@Resource
	private CompanyCopyrightMapper companyCopyrightMapper;


	@Override
	public void saveCompanyCopyright(CompanyCopyright companyCopyright) {
		if (null == companyCopyright) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCopyrightMapper.insertSelective(companyCopyright);
	}

	@Override
	public void updateCompanyCopyright(CompanyCopyright companyCopyright) {
		if (null == companyCopyright) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCopyrightMapper.updateByPrimaryKeySelective(companyCopyright);
	}

	@Override
	public CompanyCopyright getCompanyCopyrightById(String id) {
		return companyCopyrightMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyCopyright> getCompanyCopyrightList(CompanyCopyright companyCopyright) {
		return this.getCompanyCopyrightListByParam(companyCopyright, null);
	}

	@Override
	public List<CompanyCopyright> getCompanyCopyrightListByParam(CompanyCopyright companyCopyright, String orderByStr) {
		CompanyCopyrightExample example = new CompanyCopyrightExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyCopyright> companyCopyrightList = this.companyCopyrightMapper.selectByExample(example);
		return companyCopyrightList;
	}

	@Override
	public PageResult<CompanyCopyright> findCompanyCopyrightPageByParam(CompanyCopyright companyCopyright, PageParam pageParam) {
		return this.findCompanyCopyrightPageByParam(companyCopyright, pageParam, null);
	}

	@Override
	public PageResult<CompanyCopyright> findCompanyCopyrightPageByParam(CompanyCopyright companyCopyright, PageParam pageParam, String orderByStr) {
		PageResult<CompanyCopyright> pageResult = new PageResult<CompanyCopyright>();

		CompanyCopyrightExample example = new CompanyCopyrightExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyCopyright> list = null;// companyCopyrightMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyCopyrightById(String id) {
		this.companyCopyrightMapper.deleteByPrimaryKey(id);
	}

}
