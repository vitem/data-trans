package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

public class EventMergeAppraise implements Serializable {
    private String id;

    private String eventId;

    private String eventMergeId;

    /**
     * 评估公司
     */
    private String appraiseId;

    /**
     * 被评估公司
     */
    private String appraisedId;

    /**
     * 评估基准日
     */
    private Date referenceDate;

    /**
     * 评估方法  1市场法，2收益法，3成本法
     */
    private Byte appraiseWay;

    /**
     * 评估对象
     */
    private String appraiseTarget;

    /**
     * 价值类型 : 1市场价值 ，2在用价值 ，3投资价值，4持续经营价值，5清算价值，6保险价值，7课税价值，8重置价值，9其他
     */
    private Byte worthType;

    /**
     * 评估值
     */
    private BigDecimal appraiseValue;

    /**
     * 增值率
     */
    private BigDecimal appreciationRate;

    private String operatorId;

    /**
     * 修改人
     */
    private String createrId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改人
     */
    private String modifierId;

    /**
     * 修改时间
     */
    private Date modifyTime;

    /**
     * 1：正常  2删除 3...
     */
    private Byte status;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId == null ? null : eventId.trim();
    }

    public String getEventMergeId() {
        return eventMergeId;
    }

    public void setEventMergeId(String eventMergeId) {
        this.eventMergeId = eventMergeId == null ? null : eventMergeId.trim();
    }

    public String getAppraiseId() {
        return appraiseId;
    }

    public void setAppraiseId(String appraiseId) {
        this.appraiseId = appraiseId == null ? null : appraiseId.trim();
    }

    public String getAppraisedId() {
        return appraisedId;
    }

    public void setAppraisedId(String appraisedId) {
        this.appraisedId = appraisedId == null ? null : appraisedId.trim();
    }

    public Date getReferenceDate() {
        return referenceDate;
    }

    public void setReferenceDate(Date referenceDate) {
        this.referenceDate = referenceDate;
    }

    public Byte getAppraiseWay() {
        return appraiseWay;
    }

    public void setAppraiseWay(Byte appraiseWay) {
        this.appraiseWay = appraiseWay;
    }

    public String getAppraiseTarget() {
        return appraiseTarget;
    }

    public void setAppraiseTarget(String appraiseTarget) {
        this.appraiseTarget = appraiseTarget == null ? null : appraiseTarget.trim();
    }

    public Byte getWorthType() {
        return worthType;
    }

    public void setWorthType(Byte worthType) {
        this.worthType = worthType;
    }

    public BigDecimal getAppraiseValue() {
        return appraiseValue;
    }

    public void setAppraiseValue(BigDecimal appraiseValue) {
        this.appraiseValue = appraiseValue;
    }

    public BigDecimal getAppreciationRate() {
        return appreciationRate;
    }

    public void setAppreciationRate(BigDecimal appreciationRate) {
        this.appreciationRate = appreciationRate;
    }

    public String getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId == null ? null : operatorId.trim();
    }

    public String getCreaterId() {
        return createrId;
    }

    public void setCreaterId(String createrId) {
        this.createrId = createrId == null ? null : createrId.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getModifierId() {
        return modifierId;
    }

    public void setModifierId(String modifierId) {
        this.modifierId = modifierId == null ? null : modifierId.trim();
    }

    public Date getModifyTime() {
        return modifyTime;
    }

    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    public Byte getStatus() {
        return status;
    }

    public void setStatus(Byte status) {
        this.status = status;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", eventId=").append(eventId);
        sb.append(", eventMergeId=").append(eventMergeId);
        sb.append(", appraiseId=").append(appraiseId);
        sb.append(", appraisedId=").append(appraisedId);
        sb.append(", referenceDate=").append(referenceDate);
        sb.append(", appraiseWay=").append(appraiseWay);
        sb.append(", appraiseTarget=").append(appraiseTarget);
        sb.append(", worthType=").append(worthType);
        sb.append(", appraiseValue=").append(appraiseValue);
        sb.append(", appreciationRate=").append(appreciationRate);
        sb.append(", operatorId=").append(operatorId);
        sb.append(", createrId=").append(createrId);
        sb.append(", createTime=").append(createTime);
        sb.append(", modifierId=").append(modifierId);
        sb.append(", modifyTime=").append(modifyTime);
        sb.append(", status=").append(status);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}