package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;

public class FundCollectWithBLOBs extends FundCollect implements Serializable {
    /**
     * 开始募集中文描述
     */
    private String startCnDesc;

    /**
     * 开始募集英文描述
     */
    private String startEnDesc;

    /**
     * 首轮募集中文描述
     */
    private String firstCnDesc;

    /**
     * 首轮募集英文描述
     */
    private String firstEnDesc;

    /**
     * 募集完成中文描述
     */
    private String finallyCnDesc;

    /**
     * 募集完成英文描述
     */
    private String finallyEnDesc;

    private static final long serialVersionUID = 1L;

    public String getStartCnDesc() {
        return startCnDesc;
    }

    public void setStartCnDesc(String startCnDesc) {
        this.startCnDesc = startCnDesc == null ? null : startCnDesc.trim();
    }

    public String getStartEnDesc() {
        return startEnDesc;
    }

    public void setStartEnDesc(String startEnDesc) {
        this.startEnDesc = startEnDesc == null ? null : startEnDesc.trim();
    }

    public String getFirstCnDesc() {
        return firstCnDesc;
    }

    public void setFirstCnDesc(String firstCnDesc) {
        this.firstCnDesc = firstCnDesc == null ? null : firstCnDesc.trim();
    }

    public String getFirstEnDesc() {
        return firstEnDesc;
    }

    public void setFirstEnDesc(String firstEnDesc) {
        this.firstEnDesc = firstEnDesc == null ? null : firstEnDesc.trim();
    }

    public String getFinallyCnDesc() {
        return finallyCnDesc;
    }

    public void setFinallyCnDesc(String finallyCnDesc) {
        this.finallyCnDesc = finallyCnDesc == null ? null : finallyCnDesc.trim();
    }

    public String getFinallyEnDesc() {
        return finallyEnDesc;
    }

    public void setFinallyEnDesc(String finallyEnDesc) {
        this.finallyEnDesc = finallyEnDesc == null ? null : finallyEnDesc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", startCnDesc=").append(startCnDesc);
        sb.append(", startEnDesc=").append(startEnDesc);
        sb.append(", firstCnDesc=").append(firstCnDesc);
        sb.append(", firstEnDesc=").append(firstEnDesc);
        sb.append(", finallyCnDesc=").append(finallyCnDesc);
        sb.append(", finallyEnDesc=").append(finallyEnDesc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}