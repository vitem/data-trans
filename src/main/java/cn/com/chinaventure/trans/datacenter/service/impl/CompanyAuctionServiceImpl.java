package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyAuctionService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyAuction;
import cn.com.chinaventure.trans.datacenter.entity.CompanyAuctionExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyAuctionExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyAuctionMapper;


@Service("companyAuctionService")
public class CompanyAuctionServiceImpl implements CompanyAuctionService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyAuctionServiceImpl.class);

	@Resource
	private CompanyAuctionMapper companyAuctionMapper;


	@Override
	public void saveCompanyAuction(CompanyAuction companyAuction) {
		if (null == companyAuction) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyAuctionMapper.insertSelective(companyAuction);
	}

	@Override
	public void updateCompanyAuction(CompanyAuction companyAuction) {
		if (null == companyAuction) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyAuctionMapper.updateByPrimaryKeySelective(companyAuction);
	}

	@Override
	public CompanyAuction getCompanyAuctionById(String id) {
		return companyAuctionMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyAuction> getCompanyAuctionList(CompanyAuction companyAuction) {
		return this.getCompanyAuctionListByParam(companyAuction, null);
	}

	@Override
	public List<CompanyAuction> getCompanyAuctionListByParam(CompanyAuction companyAuction, String orderByStr) {
		CompanyAuctionExample example = new CompanyAuctionExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyAuction> companyAuctionList = this.companyAuctionMapper.selectByExample(example);
		return companyAuctionList;
	}

	@Override
	public PageResult<CompanyAuction> findCompanyAuctionPageByParam(CompanyAuction companyAuction, PageParam pageParam) {
		return this.findCompanyAuctionPageByParam(companyAuction, pageParam, null);
	}

	@Override
	public PageResult<CompanyAuction> findCompanyAuctionPageByParam(CompanyAuction companyAuction, PageParam pageParam, String orderByStr) {
		PageResult<CompanyAuction> pageResult = new PageResult<CompanyAuction>();

		CompanyAuctionExample example = new CompanyAuctionExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyAuction> list = null;// companyAuctionMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyAuctionById(String id) {
		this.companyAuctionMapper.deleteByPrimaryKey(id);
	}

}
