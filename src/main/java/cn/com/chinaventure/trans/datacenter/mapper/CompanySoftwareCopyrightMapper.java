package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyright;
import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyrightExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanySoftwareCopyrightMapper {
    int countByExample(CompanySoftwareCopyrightExample example);

    int deleteByExample(CompanySoftwareCopyrightExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanySoftwareCopyright record);

    int insertSelective(CompanySoftwareCopyright record);

    List<CompanySoftwareCopyright> selectByExample(CompanySoftwareCopyrightExample example);

    CompanySoftwareCopyright selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanySoftwareCopyright record, @Param("example") CompanySoftwareCopyrightExample example);

    int updateByExample(@Param("record") CompanySoftwareCopyright record, @Param("example") CompanySoftwareCopyrightExample example);

    int updateByPrimaryKeySelective(CompanySoftwareCopyright record);

    int updateByPrimaryKey(CompanySoftwareCopyright record);
}