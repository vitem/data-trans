package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.EventIpo;

/**
 * @author 
 *
 */
public interface EventIpoService {
	
	/**
     * @api saveEventIpo 新增eventIpo
     * @apiGroup eventIpo管理
     * @apiName  新增eventIpo记录
     * @apiDescription 全量插入eventIpo记录
     * @apiParam {EventIpo} eventIpo eventIpo对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEventIpo(EventIpo eventIpo);
	
	/**
     * @api updateEventIpo 修改eventIpo
     * @apiGroup eventIpo管理
     * @apiName  修改eventIpo记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EventIpo} eventIpo eventIpo对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEventIpo(EventIpo eventIpo);
	
	/**
     * @api getEventIpoById 根据eventIpoid查询详情
     * @apiGroup eventIpo管理
     * @apiName  查询eventIpo详情
     * @apiDescription 根据主键id查询eventIpo详情
     * @apiParam {String} id 主键id
     * @apiSuccess EventIpo eventIpo实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EventIpo getEventIpoById(String id);
	
	/**
     * @api getEventIpoList 根据eventIpo条件查询列表
     * @apiGroup eventIpo管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EventIpo} eventIpo  实体条件
     * @apiSuccess List<EventIpo> eventIpo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventIpo> getEventIpoList(EventIpo eventIpo);
	

	/**
     * @api getEventIpoListByParam 根据eventIpo条件查询列表（含排序）
     * @apiGroup eventIpo管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EventIpo} eventIpo  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EventIpo> eventIpo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EventIpo> getEventIpoListByParam(EventIpo eventIpo, String orderByStr);
	
	
	/**
     * @api findEventIpoPageByParam 根据eventIpo条件查询列表（分页）
     * @apiGroup eventIpo管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EventIpo} eventIpo  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EventIpo> eventIpo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventIpo> findEventIpoPageByParam(EventIpo eventIpo, PageParam pageParam); 
	
	/**
     * @api findEventIpoPageByParam 根据eventIpo条件查询列表（分页，含排序）
     * @apiGroup eventIpo管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EventIpo} eventIpo  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EventIpo> eventIpo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EventIpo> findEventIpoPageByParam(EventIpo eventIpo, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEventIpoById 根据eventIpo的id删除(物理删除)
     * @apiGroup eventIpo管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEventIpoById(String id);

}

