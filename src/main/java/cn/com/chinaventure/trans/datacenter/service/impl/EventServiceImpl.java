package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventService;
import cn.com.chinaventure.trans.datacenter.entity.Event;
import cn.com.chinaventure.trans.datacenter.entity.EventExample;
import cn.com.chinaventure.trans.datacenter.entity.EventExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.EventMapper;


@Service("eventService")
public class EventServiceImpl implements EventService {

	private final static Logger logger = LoggerFactory.getLogger(EventServiceImpl.class);

	@Resource
	private EventMapper eventMapper;


	@Override
	public void saveEvent(Event event) {
		if (null == event) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMapper.insertSelective(event);
	}

	@Override
	public void updateEvent(Event event) {
		if (null == event) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.eventMapper.updateByPrimaryKeySelective(event);
	}

	@Override
	public Event getEventById(String id) {
		return eventMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Event> getEventList(Event event) {
		return this.getEventListByParam(event, null);
	}

	@Override
	public List<Event> getEventListByParam(Event event, String orderByStr) {
		EventExample example = new EventExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Event> eventList = this.eventMapper.selectByExample(example);
		return eventList;
	}

	@Override
	public PageResult<Event> findEventPageByParam(Event event, PageParam pageParam) {
		return this.findEventPageByParam(event, pageParam, null);
	}

	@Override
	public PageResult<Event> findEventPageByParam(Event event, PageParam pageParam, String orderByStr) {
		PageResult<Event> pageResult = new PageResult<Event>();

		EventExample example = new EventExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Event> list = null;// eventMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEventById(String id) {
		this.eventMapper.deleteByPrimaryKey(id);
	}

}
