package cn.com.chinaventure.trans.datacenter.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventMergeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EventMergeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNull() {
            addCriterion("event_id is null");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNotNull() {
            addCriterion("event_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventIdEqualTo(String value) {
            addCriterion("event_id =", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotEqualTo(String value) {
            addCriterion("event_id <>", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThan(String value) {
            addCriterion("event_id >", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_id >=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThan(String value) {
            addCriterion("event_id <", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThanOrEqualTo(String value) {
            addCriterion("event_id <=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLike(String value) {
            addCriterion("event_id like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotLike(String value) {
            addCriterion("event_id not like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdIn(List<String> values) {
            addCriterion("event_id in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotIn(List<String> values) {
            addCriterion("event_id not in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdBetween(String value1, String value2) {
            addCriterion("event_id between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotBetween(String value1, String value2) {
            addCriterion("event_id not between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andTradeStatusIsNull() {
            addCriterion("trade_status is null");
            return (Criteria) this;
        }

        public Criteria andTradeStatusIsNotNull() {
            addCriterion("trade_status is not null");
            return (Criteria) this;
        }

        public Criteria andTradeStatusEqualTo(Byte value) {
            addCriterion("trade_status =", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotEqualTo(Byte value) {
            addCriterion("trade_status <>", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusGreaterThan(Byte value) {
            addCriterion("trade_status >", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("trade_status >=", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusLessThan(Byte value) {
            addCriterion("trade_status <", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusLessThanOrEqualTo(Byte value) {
            addCriterion("trade_status <=", value, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusIn(List<Byte> values) {
            addCriterion("trade_status in", values, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotIn(List<Byte> values) {
            addCriterion("trade_status not in", values, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusBetween(Byte value1, Byte value2) {
            addCriterion("trade_status between", value1, value2, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andTradeStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("trade_status not between", value1, value2, "tradeStatus");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNull() {
            addCriterion("start_time is null");
            return (Criteria) this;
        }

        public Criteria andStartTimeIsNotNull() {
            addCriterion("start_time is not null");
            return (Criteria) this;
        }

        public Criteria andStartTimeEqualTo(Date value) {
            addCriterion("start_time =", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotEqualTo(Date value) {
            addCriterion("start_time <>", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThan(Date value) {
            addCriterion("start_time >", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("start_time >=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThan(Date value) {
            addCriterion("start_time <", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeLessThanOrEqualTo(Date value) {
            addCriterion("start_time <=", value, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeIn(List<Date> values) {
            addCriterion("start_time in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotIn(List<Date> values) {
            addCriterion("start_time not in", values, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeBetween(Date value1, Date value2) {
            addCriterion("start_time between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andStartTimeNotBetween(Date value1, Date value2) {
            addCriterion("start_time not between", value1, value2, "startTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNull() {
            addCriterion("end_time is null");
            return (Criteria) this;
        }

        public Criteria andEndTimeIsNotNull() {
            addCriterion("end_time is not null");
            return (Criteria) this;
        }

        public Criteria andEndTimeEqualTo(Date value) {
            addCriterion("end_time =", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotEqualTo(Date value) {
            addCriterion("end_time <>", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThan(Date value) {
            addCriterion("end_time >", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("end_time >=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThan(Date value) {
            addCriterion("end_time <", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeLessThanOrEqualTo(Date value) {
            addCriterion("end_time <=", value, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeIn(List<Date> values) {
            addCriterion("end_time in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotIn(List<Date> values) {
            addCriterion("end_time not in", values, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeBetween(Date value1, Date value2) {
            addCriterion("end_time between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andEndTimeNotBetween(Date value1, Date value2) {
            addCriterion("end_time not between", value1, value2, "endTime");
            return (Criteria) this;
        }

        public Criteria andRegionTypeIsNull() {
            addCriterion("region_type is null");
            return (Criteria) this;
        }

        public Criteria andRegionTypeIsNotNull() {
            addCriterion("region_type is not null");
            return (Criteria) this;
        }

        public Criteria andRegionTypeEqualTo(Byte value) {
            addCriterion("region_type =", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeNotEqualTo(Byte value) {
            addCriterion("region_type <>", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeGreaterThan(Byte value) {
            addCriterion("region_type >", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("region_type >=", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeLessThan(Byte value) {
            addCriterion("region_type <", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeLessThanOrEqualTo(Byte value) {
            addCriterion("region_type <=", value, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeIn(List<Byte> values) {
            addCriterion("region_type in", values, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeNotIn(List<Byte> values) {
            addCriterion("region_type not in", values, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeBetween(Byte value1, Byte value2) {
            addCriterion("region_type between", value1, value2, "regionType");
            return (Criteria) this;
        }

        public Criteria andRegionTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("region_type not between", value1, value2, "regionType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeIsNull() {
            addCriterion("merge_type is null");
            return (Criteria) this;
        }

        public Criteria andMergeTypeIsNotNull() {
            addCriterion("merge_type is not null");
            return (Criteria) this;
        }

        public Criteria andMergeTypeEqualTo(Byte value) {
            addCriterion("merge_type =", value, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeNotEqualTo(Byte value) {
            addCriterion("merge_type <>", value, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeGreaterThan(Byte value) {
            addCriterion("merge_type >", value, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("merge_type >=", value, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeLessThan(Byte value) {
            addCriterion("merge_type <", value, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeLessThanOrEqualTo(Byte value) {
            addCriterion("merge_type <=", value, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeIn(List<Byte> values) {
            addCriterion("merge_type in", values, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeNotIn(List<Byte> values) {
            addCriterion("merge_type not in", values, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeBetween(Byte value1, Byte value2) {
            addCriterion("merge_type between", value1, value2, "mergeType");
            return (Criteria) this;
        }

        public Criteria andMergeTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("merge_type not between", value1, value2, "mergeType");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusIsNull() {
            addCriterion("trade_link_status is null");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusIsNotNull() {
            addCriterion("trade_link_status is not null");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusEqualTo(Byte value) {
            addCriterion("trade_link_status =", value, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusNotEqualTo(Byte value) {
            addCriterion("trade_link_status <>", value, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusGreaterThan(Byte value) {
            addCriterion("trade_link_status >", value, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("trade_link_status >=", value, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusLessThan(Byte value) {
            addCriterion("trade_link_status <", value, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusLessThanOrEqualTo(Byte value) {
            addCriterion("trade_link_status <=", value, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusIn(List<Byte> values) {
            addCriterion("trade_link_status in", values, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusNotIn(List<Byte> values) {
            addCriterion("trade_link_status not in", values, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusBetween(Byte value1, Byte value2) {
            addCriterion("trade_link_status between", value1, value2, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andTradeLinkStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("trade_link_status not between", value1, value2, "tradeLinkStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusIsNull() {
            addCriterion("vcpe_status is null");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusIsNotNull() {
            addCriterion("vcpe_status is not null");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusEqualTo(Byte value) {
            addCriterion("vcpe_status =", value, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusNotEqualTo(Byte value) {
            addCriterion("vcpe_status <>", value, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusGreaterThan(Byte value) {
            addCriterion("vcpe_status >", value, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("vcpe_status >=", value, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusLessThan(Byte value) {
            addCriterion("vcpe_status <", value, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusLessThanOrEqualTo(Byte value) {
            addCriterion("vcpe_status <=", value, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusIn(List<Byte> values) {
            addCriterion("vcpe_status in", values, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusNotIn(List<Byte> values) {
            addCriterion("vcpe_status not in", values, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusBetween(Byte value1, Byte value2) {
            addCriterion("vcpe_status between", value1, value2, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andVcpeStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("vcpe_status not between", value1, value2, "vcpeStatus");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdIsNull() {
            addCriterion("invest_org_id is null");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdIsNotNull() {
            addCriterion("invest_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdEqualTo(String value) {
            addCriterion("invest_org_id =", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdNotEqualTo(String value) {
            addCriterion("invest_org_id <>", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdGreaterThan(String value) {
            addCriterion("invest_org_id >", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("invest_org_id >=", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdLessThan(String value) {
            addCriterion("invest_org_id <", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdLessThanOrEqualTo(String value) {
            addCriterion("invest_org_id <=", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdLike(String value) {
            addCriterion("invest_org_id like", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdNotLike(String value) {
            addCriterion("invest_org_id not like", value, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdIn(List<String> values) {
            addCriterion("invest_org_id in", values, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdNotIn(List<String> values) {
            addCriterion("invest_org_id not in", values, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdBetween(String value1, String value2) {
            addCriterion("invest_org_id between", value1, value2, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestOrgIdNotBetween(String value1, String value2) {
            addCriterion("invest_org_id not between", value1, value2, "investOrgId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdIsNull() {
            addCriterion("invest_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdIsNotNull() {
            addCriterion("invest_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdEqualTo(String value) {
            addCriterion("invest_personage_id =", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdNotEqualTo(String value) {
            addCriterion("invest_personage_id <>", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdGreaterThan(String value) {
            addCriterion("invest_personage_id >", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("invest_personage_id >=", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdLessThan(String value) {
            addCriterion("invest_personage_id <", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("invest_personage_id <=", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdLike(String value) {
            addCriterion("invest_personage_id like", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdNotLike(String value) {
            addCriterion("invest_personage_id not like", value, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdIn(List<String> values) {
            addCriterion("invest_personage_id in", values, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdNotIn(List<String> values) {
            addCriterion("invest_personage_id not in", values, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdBetween(String value1, String value2) {
            addCriterion("invest_personage_id between", value1, value2, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andInvestPersonageIdNotBetween(String value1, String value2) {
            addCriterion("invest_personage_id not between", value1, value2, "investPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIsNull() {
            addCriterion("law_org_id is null");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIsNotNull() {
            addCriterion("law_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdEqualTo(String value) {
            addCriterion("law_org_id =", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotEqualTo(String value) {
            addCriterion("law_org_id <>", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdGreaterThan(String value) {
            addCriterion("law_org_id >", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("law_org_id >=", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLessThan(String value) {
            addCriterion("law_org_id <", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLessThanOrEqualTo(String value) {
            addCriterion("law_org_id <=", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdLike(String value) {
            addCriterion("law_org_id like", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotLike(String value) {
            addCriterion("law_org_id not like", value, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdIn(List<String> values) {
            addCriterion("law_org_id in", values, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotIn(List<String> values) {
            addCriterion("law_org_id not in", values, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdBetween(String value1, String value2) {
            addCriterion("law_org_id between", value1, value2, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawOrgIdNotBetween(String value1, String value2) {
            addCriterion("law_org_id not between", value1, value2, "lawOrgId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIsNull() {
            addCriterion("law_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIsNotNull() {
            addCriterion("law_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdEqualTo(String value) {
            addCriterion("law_personage_id =", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotEqualTo(String value) {
            addCriterion("law_personage_id <>", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdGreaterThan(String value) {
            addCriterion("law_personage_id >", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("law_personage_id >=", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLessThan(String value) {
            addCriterion("law_personage_id <", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("law_personage_id <=", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdLike(String value) {
            addCriterion("law_personage_id like", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotLike(String value) {
            addCriterion("law_personage_id not like", value, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdIn(List<String> values) {
            addCriterion("law_personage_id in", values, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotIn(List<String> values) {
            addCriterion("law_personage_id not in", values, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdBetween(String value1, String value2) {
            addCriterion("law_personage_id between", value1, value2, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andLawPersonageIdNotBetween(String value1, String value2) {
            addCriterion("law_personage_id not between", value1, value2, "lawPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIsNull() {
            addCriterion("accounting_org_id is null");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIsNotNull() {
            addCriterion("accounting_org_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdEqualTo(String value) {
            addCriterion("accounting_org_id =", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotEqualTo(String value) {
            addCriterion("accounting_org_id <>", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdGreaterThan(String value) {
            addCriterion("accounting_org_id >", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_org_id >=", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLessThan(String value) {
            addCriterion("accounting_org_id <", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLessThanOrEqualTo(String value) {
            addCriterion("accounting_org_id <=", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdLike(String value) {
            addCriterion("accounting_org_id like", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotLike(String value) {
            addCriterion("accounting_org_id not like", value, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdIn(List<String> values) {
            addCriterion("accounting_org_id in", values, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotIn(List<String> values) {
            addCriterion("accounting_org_id not in", values, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdBetween(String value1, String value2) {
            addCriterion("accounting_org_id between", value1, value2, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingOrgIdNotBetween(String value1, String value2) {
            addCriterion("accounting_org_id not between", value1, value2, "accountingOrgId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIsNull() {
            addCriterion("accounting_personage_id is null");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIsNotNull() {
            addCriterion("accounting_personage_id is not null");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdEqualTo(String value) {
            addCriterion("accounting_personage_id =", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotEqualTo(String value) {
            addCriterion("accounting_personage_id <>", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdGreaterThan(String value) {
            addCriterion("accounting_personage_id >", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdGreaterThanOrEqualTo(String value) {
            addCriterion("accounting_personage_id >=", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLessThan(String value) {
            addCriterion("accounting_personage_id <", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLessThanOrEqualTo(String value) {
            addCriterion("accounting_personage_id <=", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdLike(String value) {
            addCriterion("accounting_personage_id like", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotLike(String value) {
            addCriterion("accounting_personage_id not like", value, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdIn(List<String> values) {
            addCriterion("accounting_personage_id in", values, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotIn(List<String> values) {
            addCriterion("accounting_personage_id not in", values, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdBetween(String value1, String value2) {
            addCriterion("accounting_personage_id between", value1, value2, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andAccountingPersonageIdNotBetween(String value1, String value2) {
            addCriterion("accounting_personage_id not between", value1, value2, "accountingPersonageId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}