package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategy;
import cn.com.chinaventure.trans.datacenter.entity.OrganizationStrategyExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrganizationStrategyMapper {
    int countByExample(OrganizationStrategyExample example);

    int deleteByExample(OrganizationStrategyExample example);

    int deleteByPrimaryKey(String id);

    int insert(OrganizationStrategy record);

    int insertSelective(OrganizationStrategy record);

    List<OrganizationStrategy> selectByExample(OrganizationStrategyExample example);

    OrganizationStrategy selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") OrganizationStrategy record, @Param("example") OrganizationStrategyExample example);

    int updateByExample(@Param("record") OrganizationStrategy record, @Param("example") OrganizationStrategyExample example);

    int updateByPrimaryKeySelective(OrganizationStrategy record);

    int updateByPrimaryKey(OrganizationStrategy record);
}