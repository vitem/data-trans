package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyEmploymentService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyEmployment;


@RestController
@RequestMapping("/companyEmployment")
public class CompanyEmploymentController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyEmploymentController.class);

    @Resource
    private CompanyEmploymentService companyEmploymentService;

    /**
	 * @api {post} / 新增companyEmployment
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompanyEmployment(@RequestBody CompanyEmployment companyEmployment) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyEmploymentService.saveCompanyEmployment(companyEmployment);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改companyEmployment
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompanyEmployment(@RequestBody CompanyEmployment companyEmployment) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyEmploymentService.updateCompanyEmployment(companyEmployment);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询companyEmployment详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<CompanyEmployment> getCompanyEmploymentById(@PathVariable String id) {
        JsonResult<CompanyEmployment> jr = new JsonResult<CompanyEmployment>();
        try {
            CompanyEmployment companyEmployment  = companyEmploymentService.getCompanyEmploymentById(id);
            jr = buildJsonResult(companyEmployment);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /companyEmployment/getCompanyEmploymentList 查询companyEmployment列表(不带分页)
	 * @apiGroup companyEmployment管理
	 * @apiName 查询companyEmployment列表(不带分页)
	 * @apiDescription 根据条件查询companyEmployment列表(不带分页)
	 * @apiParam {CompanyEmployment} companyEmployment companyEmployment对象
	 */
    @RequestMapping("/getCompanyEmploymentList")
    public JsonResult<List<CompanyEmployment>> getCompanyEmploymentList(CompanyEmployment companyEmployment) {
        JsonResult<List<CompanyEmployment>> jr = new JsonResult<List<CompanyEmployment>>();
        try {
            List<CompanyEmployment> list = this.companyEmploymentService.getCompanyEmploymentList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /companyEmployment/findCompanyEmploymentPageByParam 查询companyEmployment列表(带分页)
	 * @apiGroup companyEmployment管理
	 * @apiName 查询companyEmployment列表(带分页)
	 * @apiDescription 根据条件查询companyEmployment列表(带分页)
	 * @apiParam {CompanyEmployment} companyEmployment companyEmployment对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyEmploymentPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<CompanyEmployment>> findCompanyEmploymentPageByParam(@RequestBody CompanyEmployment companyEmployment, PageParam pageParam) {
        JsonResult<PageResult<CompanyEmployment>> jr = new JsonResult<PageResult<CompanyEmployment>>();
        try {
            PageResult<CompanyEmployment> page = this.companyEmploymentService.findCompanyEmploymentPageByParam(companyEmployment, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /companyEmployment/deleteCompanyEmploymentById 删除companyEmployment(物理删除)
	 * @apiGroup companyEmployment管理
	 * @apiName 删除companyEmployment记录(物理删除)
	 * @apiDescription 根据主键id删除companyEmployment记录(物理删除)
	 * @apiParam {String} id companyEmployment主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyEmploymentById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyEmploymentById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyEmploymentService.deleteCompanyEmploymentById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
