package cn.com.chinaventure.trans.datacenter.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventMergeAppraiseExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EventMergeAppraiseExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(String value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(String value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(String value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(String value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(String value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(String value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLike(String value) {
            addCriterion("id like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotLike(String value) {
            addCriterion("id not like", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<String> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<String> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(String value1, String value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(String value1, String value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNull() {
            addCriterion("event_id is null");
            return (Criteria) this;
        }

        public Criteria andEventIdIsNotNull() {
            addCriterion("event_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventIdEqualTo(String value) {
            addCriterion("event_id =", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotEqualTo(String value) {
            addCriterion("event_id <>", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThan(String value) {
            addCriterion("event_id >", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_id >=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThan(String value) {
            addCriterion("event_id <", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLessThanOrEqualTo(String value) {
            addCriterion("event_id <=", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdLike(String value) {
            addCriterion("event_id like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotLike(String value) {
            addCriterion("event_id not like", value, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdIn(List<String> values) {
            addCriterion("event_id in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotIn(List<String> values) {
            addCriterion("event_id not in", values, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdBetween(String value1, String value2) {
            addCriterion("event_id between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventIdNotBetween(String value1, String value2) {
            addCriterion("event_id not between", value1, value2, "eventId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdIsNull() {
            addCriterion("event_merge_id is null");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdIsNotNull() {
            addCriterion("event_merge_id is not null");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdEqualTo(String value) {
            addCriterion("event_merge_id =", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotEqualTo(String value) {
            addCriterion("event_merge_id <>", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdGreaterThan(String value) {
            addCriterion("event_merge_id >", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdGreaterThanOrEqualTo(String value) {
            addCriterion("event_merge_id >=", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdLessThan(String value) {
            addCriterion("event_merge_id <", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdLessThanOrEqualTo(String value) {
            addCriterion("event_merge_id <=", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdLike(String value) {
            addCriterion("event_merge_id like", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotLike(String value) {
            addCriterion("event_merge_id not like", value, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdIn(List<String> values) {
            addCriterion("event_merge_id in", values, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotIn(List<String> values) {
            addCriterion("event_merge_id not in", values, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdBetween(String value1, String value2) {
            addCriterion("event_merge_id between", value1, value2, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andEventMergeIdNotBetween(String value1, String value2) {
            addCriterion("event_merge_id not between", value1, value2, "eventMergeId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdIsNull() {
            addCriterion("appraise_id is null");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdIsNotNull() {
            addCriterion("appraise_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdEqualTo(String value) {
            addCriterion("appraise_id =", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdNotEqualTo(String value) {
            addCriterion("appraise_id <>", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdGreaterThan(String value) {
            addCriterion("appraise_id >", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdGreaterThanOrEqualTo(String value) {
            addCriterion("appraise_id >=", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdLessThan(String value) {
            addCriterion("appraise_id <", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdLessThanOrEqualTo(String value) {
            addCriterion("appraise_id <=", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdLike(String value) {
            addCriterion("appraise_id like", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdNotLike(String value) {
            addCriterion("appraise_id not like", value, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdIn(List<String> values) {
            addCriterion("appraise_id in", values, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdNotIn(List<String> values) {
            addCriterion("appraise_id not in", values, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdBetween(String value1, String value2) {
            addCriterion("appraise_id between", value1, value2, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraiseIdNotBetween(String value1, String value2) {
            addCriterion("appraise_id not between", value1, value2, "appraiseId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdIsNull() {
            addCriterion("appraised_id is null");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdIsNotNull() {
            addCriterion("appraised_id is not null");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdEqualTo(String value) {
            addCriterion("appraised_id =", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdNotEqualTo(String value) {
            addCriterion("appraised_id <>", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdGreaterThan(String value) {
            addCriterion("appraised_id >", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdGreaterThanOrEqualTo(String value) {
            addCriterion("appraised_id >=", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdLessThan(String value) {
            addCriterion("appraised_id <", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdLessThanOrEqualTo(String value) {
            addCriterion("appraised_id <=", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdLike(String value) {
            addCriterion("appraised_id like", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdNotLike(String value) {
            addCriterion("appraised_id not like", value, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdIn(List<String> values) {
            addCriterion("appraised_id in", values, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdNotIn(List<String> values) {
            addCriterion("appraised_id not in", values, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdBetween(String value1, String value2) {
            addCriterion("appraised_id between", value1, value2, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andAppraisedIdNotBetween(String value1, String value2) {
            addCriterion("appraised_id not between", value1, value2, "appraisedId");
            return (Criteria) this;
        }

        public Criteria andReferenceDateIsNull() {
            addCriterion("reference_date is null");
            return (Criteria) this;
        }

        public Criteria andReferenceDateIsNotNull() {
            addCriterion("reference_date is not null");
            return (Criteria) this;
        }

        public Criteria andReferenceDateEqualTo(Date value) {
            addCriterion("reference_date =", value, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateNotEqualTo(Date value) {
            addCriterion("reference_date <>", value, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateGreaterThan(Date value) {
            addCriterion("reference_date >", value, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateGreaterThanOrEqualTo(Date value) {
            addCriterion("reference_date >=", value, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateLessThan(Date value) {
            addCriterion("reference_date <", value, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateLessThanOrEqualTo(Date value) {
            addCriterion("reference_date <=", value, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateIn(List<Date> values) {
            addCriterion("reference_date in", values, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateNotIn(List<Date> values) {
            addCriterion("reference_date not in", values, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateBetween(Date value1, Date value2) {
            addCriterion("reference_date between", value1, value2, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andReferenceDateNotBetween(Date value1, Date value2) {
            addCriterion("reference_date not between", value1, value2, "referenceDate");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayIsNull() {
            addCriterion("appraise_way is null");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayIsNotNull() {
            addCriterion("appraise_way is not null");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayEqualTo(Byte value) {
            addCriterion("appraise_way =", value, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayNotEqualTo(Byte value) {
            addCriterion("appraise_way <>", value, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayGreaterThan(Byte value) {
            addCriterion("appraise_way >", value, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayGreaterThanOrEqualTo(Byte value) {
            addCriterion("appraise_way >=", value, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayLessThan(Byte value) {
            addCriterion("appraise_way <", value, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayLessThanOrEqualTo(Byte value) {
            addCriterion("appraise_way <=", value, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayIn(List<Byte> values) {
            addCriterion("appraise_way in", values, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayNotIn(List<Byte> values) {
            addCriterion("appraise_way not in", values, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayBetween(Byte value1, Byte value2) {
            addCriterion("appraise_way between", value1, value2, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseWayNotBetween(Byte value1, Byte value2) {
            addCriterion("appraise_way not between", value1, value2, "appraiseWay");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetIsNull() {
            addCriterion("appraise_target is null");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetIsNotNull() {
            addCriterion("appraise_target is not null");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetEqualTo(String value) {
            addCriterion("appraise_target =", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetNotEqualTo(String value) {
            addCriterion("appraise_target <>", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetGreaterThan(String value) {
            addCriterion("appraise_target >", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetGreaterThanOrEqualTo(String value) {
            addCriterion("appraise_target >=", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetLessThan(String value) {
            addCriterion("appraise_target <", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetLessThanOrEqualTo(String value) {
            addCriterion("appraise_target <=", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetLike(String value) {
            addCriterion("appraise_target like", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetNotLike(String value) {
            addCriterion("appraise_target not like", value, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetIn(List<String> values) {
            addCriterion("appraise_target in", values, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetNotIn(List<String> values) {
            addCriterion("appraise_target not in", values, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetBetween(String value1, String value2) {
            addCriterion("appraise_target between", value1, value2, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andAppraiseTargetNotBetween(String value1, String value2) {
            addCriterion("appraise_target not between", value1, value2, "appraiseTarget");
            return (Criteria) this;
        }

        public Criteria andWorthTypeIsNull() {
            addCriterion("worth_type is null");
            return (Criteria) this;
        }

        public Criteria andWorthTypeIsNotNull() {
            addCriterion("worth_type is not null");
            return (Criteria) this;
        }

        public Criteria andWorthTypeEqualTo(Byte value) {
            addCriterion("worth_type =", value, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeNotEqualTo(Byte value) {
            addCriterion("worth_type <>", value, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeGreaterThan(Byte value) {
            addCriterion("worth_type >", value, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("worth_type >=", value, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeLessThan(Byte value) {
            addCriterion("worth_type <", value, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeLessThanOrEqualTo(Byte value) {
            addCriterion("worth_type <=", value, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeIn(List<Byte> values) {
            addCriterion("worth_type in", values, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeNotIn(List<Byte> values) {
            addCriterion("worth_type not in", values, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeBetween(Byte value1, Byte value2) {
            addCriterion("worth_type between", value1, value2, "worthType");
            return (Criteria) this;
        }

        public Criteria andWorthTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("worth_type not between", value1, value2, "worthType");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueIsNull() {
            addCriterion("appraise_value is null");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueIsNotNull() {
            addCriterion("appraise_value is not null");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueEqualTo(BigDecimal value) {
            addCriterion("appraise_value =", value, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueNotEqualTo(BigDecimal value) {
            addCriterion("appraise_value <>", value, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueGreaterThan(BigDecimal value) {
            addCriterion("appraise_value >", value, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("appraise_value >=", value, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueLessThan(BigDecimal value) {
            addCriterion("appraise_value <", value, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("appraise_value <=", value, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueIn(List<BigDecimal> values) {
            addCriterion("appraise_value in", values, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueNotIn(List<BigDecimal> values) {
            addCriterion("appraise_value not in", values, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("appraise_value between", value1, value2, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppraiseValueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("appraise_value not between", value1, value2, "appraiseValue");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateIsNull() {
            addCriterion("appreciation_rate is null");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateIsNotNull() {
            addCriterion("appreciation_rate is not null");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateEqualTo(BigDecimal value) {
            addCriterion("appreciation_rate =", value, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateNotEqualTo(BigDecimal value) {
            addCriterion("appreciation_rate <>", value, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateGreaterThan(BigDecimal value) {
            addCriterion("appreciation_rate >", value, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("appreciation_rate >=", value, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateLessThan(BigDecimal value) {
            addCriterion("appreciation_rate <", value, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("appreciation_rate <=", value, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateIn(List<BigDecimal> values) {
            addCriterion("appreciation_rate in", values, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateNotIn(List<BigDecimal> values) {
            addCriterion("appreciation_rate not in", values, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("appreciation_rate between", value1, value2, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andAppreciationRateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("appreciation_rate not between", value1, value2, "appreciationRate");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNull() {
            addCriterion("operator_id is null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIsNotNull() {
            addCriterion("operator_id is not null");
            return (Criteria) this;
        }

        public Criteria andOperatorIdEqualTo(String value) {
            addCriterion("operator_id =", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotEqualTo(String value) {
            addCriterion("operator_id <>", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThan(String value) {
            addCriterion("operator_id >", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdGreaterThanOrEqualTo(String value) {
            addCriterion("operator_id >=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThan(String value) {
            addCriterion("operator_id <", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLessThanOrEqualTo(String value) {
            addCriterion("operator_id <=", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdLike(String value) {
            addCriterion("operator_id like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotLike(String value) {
            addCriterion("operator_id not like", value, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdIn(List<String> values) {
            addCriterion("operator_id in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotIn(List<String> values) {
            addCriterion("operator_id not in", values, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdBetween(String value1, String value2) {
            addCriterion("operator_id between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andOperatorIdNotBetween(String value1, String value2) {
            addCriterion("operator_id not between", value1, value2, "operatorId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNull() {
            addCriterion("creater_id is null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIsNotNull() {
            addCriterion("creater_id is not null");
            return (Criteria) this;
        }

        public Criteria andCreaterIdEqualTo(String value) {
            addCriterion("creater_id =", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotEqualTo(String value) {
            addCriterion("creater_id <>", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThan(String value) {
            addCriterion("creater_id >", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdGreaterThanOrEqualTo(String value) {
            addCriterion("creater_id >=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThan(String value) {
            addCriterion("creater_id <", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLessThanOrEqualTo(String value) {
            addCriterion("creater_id <=", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdLike(String value) {
            addCriterion("creater_id like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotLike(String value) {
            addCriterion("creater_id not like", value, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdIn(List<String> values) {
            addCriterion("creater_id in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotIn(List<String> values) {
            addCriterion("creater_id not in", values, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdBetween(String value1, String value2) {
            addCriterion("creater_id between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreaterIdNotBetween(String value1, String value2) {
            addCriterion("creater_id not between", value1, value2, "createrId");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNull() {
            addCriterion("modifier_id is null");
            return (Criteria) this;
        }

        public Criteria andModifierIdIsNotNull() {
            addCriterion("modifier_id is not null");
            return (Criteria) this;
        }

        public Criteria andModifierIdEqualTo(String value) {
            addCriterion("modifier_id =", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotEqualTo(String value) {
            addCriterion("modifier_id <>", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThan(String value) {
            addCriterion("modifier_id >", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdGreaterThanOrEqualTo(String value) {
            addCriterion("modifier_id >=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThan(String value) {
            addCriterion("modifier_id <", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLessThanOrEqualTo(String value) {
            addCriterion("modifier_id <=", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdLike(String value) {
            addCriterion("modifier_id like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotLike(String value) {
            addCriterion("modifier_id not like", value, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdIn(List<String> values) {
            addCriterion("modifier_id in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotIn(List<String> values) {
            addCriterion("modifier_id not in", values, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdBetween(String value1, String value2) {
            addCriterion("modifier_id between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifierIdNotBetween(String value1, String value2) {
            addCriterion("modifier_id not between", value1, value2, "modifierId");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(Byte value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(Byte value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(Byte value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(Byte value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(Byte value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<Byte> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<Byte> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(Byte value1, Byte value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andExt1IsNull() {
            addCriterion("ext1 is null");
            return (Criteria) this;
        }

        public Criteria andExt1IsNotNull() {
            addCriterion("ext1 is not null");
            return (Criteria) this;
        }

        public Criteria andExt1EqualTo(String value) {
            addCriterion("ext1 =", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotEqualTo(String value) {
            addCriterion("ext1 <>", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThan(String value) {
            addCriterion("ext1 >", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1GreaterThanOrEqualTo(String value) {
            addCriterion("ext1 >=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThan(String value) {
            addCriterion("ext1 <", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1LessThanOrEqualTo(String value) {
            addCriterion("ext1 <=", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Like(String value) {
            addCriterion("ext1 like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotLike(String value) {
            addCriterion("ext1 not like", value, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1In(List<String> values) {
            addCriterion("ext1 in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotIn(List<String> values) {
            addCriterion("ext1 not in", values, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1Between(String value1, String value2) {
            addCriterion("ext1 between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt1NotBetween(String value1, String value2) {
            addCriterion("ext1 not between", value1, value2, "ext1");
            return (Criteria) this;
        }

        public Criteria andExt2IsNull() {
            addCriterion("ext2 is null");
            return (Criteria) this;
        }

        public Criteria andExt2IsNotNull() {
            addCriterion("ext2 is not null");
            return (Criteria) this;
        }

        public Criteria andExt2EqualTo(String value) {
            addCriterion("ext2 =", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotEqualTo(String value) {
            addCriterion("ext2 <>", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThan(String value) {
            addCriterion("ext2 >", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2GreaterThanOrEqualTo(String value) {
            addCriterion("ext2 >=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThan(String value) {
            addCriterion("ext2 <", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2LessThanOrEqualTo(String value) {
            addCriterion("ext2 <=", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Like(String value) {
            addCriterion("ext2 like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotLike(String value) {
            addCriterion("ext2 not like", value, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2In(List<String> values) {
            addCriterion("ext2 in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotIn(List<String> values) {
            addCriterion("ext2 not in", values, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2Between(String value1, String value2) {
            addCriterion("ext2 between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt2NotBetween(String value1, String value2) {
            addCriterion("ext2 not between", value1, value2, "ext2");
            return (Criteria) this;
        }

        public Criteria andExt3IsNull() {
            addCriterion("ext3 is null");
            return (Criteria) this;
        }

        public Criteria andExt3IsNotNull() {
            addCriterion("ext3 is not null");
            return (Criteria) this;
        }

        public Criteria andExt3EqualTo(String value) {
            addCriterion("ext3 =", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotEqualTo(String value) {
            addCriterion("ext3 <>", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThan(String value) {
            addCriterion("ext3 >", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3GreaterThanOrEqualTo(String value) {
            addCriterion("ext3 >=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThan(String value) {
            addCriterion("ext3 <", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3LessThanOrEqualTo(String value) {
            addCriterion("ext3 <=", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Like(String value) {
            addCriterion("ext3 like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotLike(String value) {
            addCriterion("ext3 not like", value, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3In(List<String> values) {
            addCriterion("ext3 in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotIn(List<String> values) {
            addCriterion("ext3 not in", values, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3Between(String value1, String value2) {
            addCriterion("ext3 between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt3NotBetween(String value1, String value2) {
            addCriterion("ext3 not between", value1, value2, "ext3");
            return (Criteria) this;
        }

        public Criteria andExt4IsNull() {
            addCriterion("ext4 is null");
            return (Criteria) this;
        }

        public Criteria andExt4IsNotNull() {
            addCriterion("ext4 is not null");
            return (Criteria) this;
        }

        public Criteria andExt4EqualTo(String value) {
            addCriterion("ext4 =", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotEqualTo(String value) {
            addCriterion("ext4 <>", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThan(String value) {
            addCriterion("ext4 >", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4GreaterThanOrEqualTo(String value) {
            addCriterion("ext4 >=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThan(String value) {
            addCriterion("ext4 <", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4LessThanOrEqualTo(String value) {
            addCriterion("ext4 <=", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Like(String value) {
            addCriterion("ext4 like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotLike(String value) {
            addCriterion("ext4 not like", value, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4In(List<String> values) {
            addCriterion("ext4 in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotIn(List<String> values) {
            addCriterion("ext4 not in", values, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4Between(String value1, String value2) {
            addCriterion("ext4 between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt4NotBetween(String value1, String value2) {
            addCriterion("ext4 not between", value1, value2, "ext4");
            return (Criteria) this;
        }

        public Criteria andExt5IsNull() {
            addCriterion("ext5 is null");
            return (Criteria) this;
        }

        public Criteria andExt5IsNotNull() {
            addCriterion("ext5 is not null");
            return (Criteria) this;
        }

        public Criteria andExt5EqualTo(String value) {
            addCriterion("ext5 =", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotEqualTo(String value) {
            addCriterion("ext5 <>", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThan(String value) {
            addCriterion("ext5 >", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5GreaterThanOrEqualTo(String value) {
            addCriterion("ext5 >=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThan(String value) {
            addCriterion("ext5 <", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5LessThanOrEqualTo(String value) {
            addCriterion("ext5 <=", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Like(String value) {
            addCriterion("ext5 like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotLike(String value) {
            addCriterion("ext5 not like", value, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5In(List<String> values) {
            addCriterion("ext5 in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotIn(List<String> values) {
            addCriterion("ext5 not in", values, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5Between(String value1, String value2) {
            addCriterion("ext5 between", value1, value2, "ext5");
            return (Criteria) this;
        }

        public Criteria andExt5NotBetween(String value1, String value2) {
            addCriterion("ext5 not between", value1, value2, "ext5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}