package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyCourtNoticeService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNotice;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNoticeExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCourtNoticeExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyCourtNoticeMapper;


@Service("companyCourtNoticeService")
public class CompanyCourtNoticeServiceImpl implements CompanyCourtNoticeService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyCourtNoticeServiceImpl.class);

	@Resource
	private CompanyCourtNoticeMapper companyCourtNoticeMapper;


	@Override
	public void saveCompanyCourtNotice(CompanyCourtNotice companyCourtNotice) {
		if (null == companyCourtNotice) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCourtNoticeMapper.insertSelective(companyCourtNotice);
	}

	@Override
	public void updateCompanyCourtNotice(CompanyCourtNotice companyCourtNotice) {
		if (null == companyCourtNotice) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyCourtNoticeMapper.updateByPrimaryKeySelective(companyCourtNotice);
	}

	@Override
	public CompanyCourtNotice getCompanyCourtNoticeById(String id) {
		return companyCourtNoticeMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyCourtNotice> getCompanyCourtNoticeList(CompanyCourtNotice companyCourtNotice) {
		return this.getCompanyCourtNoticeListByParam(companyCourtNotice, null);
	}

	@Override
	public List<CompanyCourtNotice> getCompanyCourtNoticeListByParam(CompanyCourtNotice companyCourtNotice, String orderByStr) {
		CompanyCourtNoticeExample example = new CompanyCourtNoticeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyCourtNotice> companyCourtNoticeList = this.companyCourtNoticeMapper.selectByExample(example);
		return companyCourtNoticeList;
	}

	@Override
	public PageResult<CompanyCourtNotice> findCompanyCourtNoticePageByParam(CompanyCourtNotice companyCourtNotice, PageParam pageParam) {
		return this.findCompanyCourtNoticePageByParam(companyCourtNotice, pageParam, null);
	}

	@Override
	public PageResult<CompanyCourtNotice> findCompanyCourtNoticePageByParam(CompanyCourtNotice companyCourtNotice, PageParam pageParam, String orderByStr) {
		PageResult<CompanyCourtNotice> pageResult = new PageResult<CompanyCourtNotice>();

		CompanyCourtNoticeExample example = new CompanyCourtNoticeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyCourtNotice> list = null;// companyCourtNoticeMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyCourtNoticeById(String id) {
		this.companyCourtNoticeMapper.deleteByPrimaryKey(id);
	}

}
