package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventExit;
import cn.com.chinaventure.trans.datacenter.entity.EventExitExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventExitMapper {
    int countByExample(EventExitExample example);

    int deleteByExample(EventExitExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventExit record);

    int insertSelective(EventExit record);

    List<EventExit> selectByExample(EventExitExample example);

    EventExit selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventExit record, @Param("example") EventExitExample example);

    int updateByExample(@Param("record") EventExit record, @Param("example") EventExitExample example);

    int updateByPrimaryKeySelective(EventExit record);

    int updateByPrimaryKey(EventExit record);
}