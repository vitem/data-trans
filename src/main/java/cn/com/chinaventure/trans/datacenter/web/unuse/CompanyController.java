package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyService;
import cn.com.chinaventure.trans.datacenter.entity.Company;


@RestController
@RequestMapping("/company")
public class CompanyController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(CompanyController.class);

    @Resource
    private CompanyService companyService;

    /**
	 * @api {post} / 新增company
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveCompany(@RequestBody Company company) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyService.saveCompany(company);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改company
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateCompany(@RequestBody Company company) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyService.updateCompany(company);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询company详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<Company> getCompanyById(@PathVariable String id) {
        JsonResult<Company> jr = new JsonResult<Company>();
        try {
            Company company  = companyService.getCompanyById(id);
            jr = buildJsonResult(company);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /company/getCompanyList 查询company列表(不带分页)
	 * @apiGroup company管理
	 * @apiName 查询company列表(不带分页)
	 * @apiDescription 根据条件查询company列表(不带分页)
	 * @apiParam {Company} company company对象
	 */
    @RequestMapping("/getCompanyList")
    public JsonResult<List<Company>> getCompanyList(Company company) {
        JsonResult<List<Company>> jr = new JsonResult<List<Company>>();
        try {
            List<Company> list = this.companyService.getCompanyList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /company/findCompanyPageByParam 查询company列表(带分页)
	 * @apiGroup company管理
	 * @apiName 查询company列表(带分页)
	 * @apiDescription 根据条件查询company列表(带分页)
	 * @apiParam {Company} company company对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findCompanyPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<Company>> findCompanyPageByParam(@RequestBody Company company, PageParam pageParam) {
        JsonResult<PageResult<Company>> jr = new JsonResult<PageResult<Company>>();
        try {
            PageResult<Company> page = this.companyService.findCompanyPageByParam(company, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /company/deleteCompanyById 删除company(物理删除)
	 * @apiGroup company管理
	 * @apiName 删除company记录(物理删除)
	 * @apiDescription 根据主键id删除company记录(物理删除)
	 * @apiParam {String} id company主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteCompanyById",method=RequestMethod.DELETE)
    public JsonResult deleteCompanyById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.companyService.deleteCompanyById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
