package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificate;
import cn.com.chinaventure.trans.datacenter.entity.CompanyCertificateExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyCertificateMapper {
    int countByExample(CompanyCertificateExample example);

    int deleteByExample(CompanyCertificateExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyCertificate record);

    int insertSelective(CompanyCertificate record);

    List<CompanyCertificate> selectByExample(CompanyCertificateExample example);

    CompanyCertificate selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyCertificate record, @Param("example") CompanyCertificateExample example);

    int updateByExample(@Param("record") CompanyCertificate record, @Param("example") CompanyCertificateExample example);

    int updateByPrimaryKeySelective(CompanyCertificate record);

    int updateByPrimaryKey(CompanyCertificate record);
}