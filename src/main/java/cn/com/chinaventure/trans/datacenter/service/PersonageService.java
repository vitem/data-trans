package cn.com.chinaventure.trans.datacenter.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.datacenter.entity.Personage;

/**
 * @author 
 *
 */
public interface PersonageService {
	
	/**
     * @api savePersonage 新增personage
     * @apiGroup personage管理
     * @apiName  新增personage记录
     * @apiDescription 全量插入personage记录
     * @apiParam {Personage} personage personage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void savePersonage(Personage personage);
	
	/**
     * @api updatePersonage 修改personage
     * @apiGroup personage管理
     * @apiName  修改personage记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Personage} personage personage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updatePersonage(Personage personage);
	
	/**
     * @api getPersonageById 根据personageid查询详情
     * @apiGroup personage管理
     * @apiName  查询personage详情
     * @apiDescription 根据主键id查询personage详情
     * @apiParam {String} id 主键id
     * @apiSuccess Personage personage实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Personage getPersonageById(String id);
	
	/**
     * @api getPersonageList 根据personage条件查询列表
     * @apiGroup personage管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Personage} personage  实体条件
     * @apiSuccess List<Personage> personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Personage> getPersonageList(Personage personage);
	

	/**
     * @api getPersonageListByParam 根据personage条件查询列表（含排序）
     * @apiGroup personage管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Personage} personage  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Personage> personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Personage> getPersonageListByParam(Personage personage, String orderByStr);
	
	
	/**
     * @api findPersonagePageByParam 根据personage条件查询列表（分页）
     * @apiGroup personage管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Personage} personage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Personage> personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Personage> findPersonagePageByParam(Personage personage, PageParam pageParam); 
	
	/**
     * @api findPersonagePageByParam 根据personage条件查询列表（分页，含排序）
     * @apiGroup personage管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Personage} personage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Personage> personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Personage> findPersonagePageByParam(Personage personage, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deletePersonageById 根据personage的id删除(物理删除)
     * @apiGroup personage管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {String} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deletePersonageById(String id);

}

