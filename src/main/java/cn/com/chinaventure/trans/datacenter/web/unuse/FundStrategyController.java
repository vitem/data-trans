package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.trans.datacenter.entity.FundStrategy;
import cn.com.chinaventure.trans.datacenter.service.FundStrategyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/fundStrategy")
public class FundStrategyController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(FundStrategyController.class);

    @Resource
    private FundStrategyService fundStrategyService;

    /**
	 * @api {post} / 新增fundStrategy
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveFundStrategy(@RequestBody FundStrategy fundStrategy) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundStrategyService.saveFundStrategy(fundStrategy);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改fundStrategy
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateFundStrategy(@RequestBody FundStrategy fundStrategy) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.fundStrategyService.updateFundStrategy(fundStrategy);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询fundStrategy详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<FundStrategy> getFundStrategyById(@PathVariable Long id) {
        JsonResult<FundStrategy> jr = new JsonResult<FundStrategy>();
        try {
            FundStrategy fundStrategy  = fundStrategyService.getFundStrategyById(id);
            jr = buildJsonResult(fundStrategy);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /fundStrategy/getFundStrategyList 查询fundStrategy列表(不带分页)
	 * @apiGroup fundStrategy管理
	 * @apiName 查询fundStrategy列表(不带分页)
	 * @apiDescription 根据条件查询fundStrategy列表(不带分页)
	 * @apiParam {FundStrategy} fundStrategy fundStrategy对象
	 */
    @RequestMapping("/getFundStrategyList")
    public JsonResult<List<FundStrategy>> getFundStrategyList(FundStrategy fundStrategy) {
        JsonResult<List<FundStrategy>> jr = new JsonResult<List<FundStrategy>>();
        try {
            List<FundStrategy> list = this.fundStrategyService.getFundStrategyList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /fundStrategy/findFundStrategyPageByParam 查询fundStrategy列表(带分页)
	 * @apiGroup fundStrategy管理
	 * @apiName 查询fundStrategy列表(带分页)
	 * @apiDescription 根据条件查询fundStrategy列表(带分页)
	 * @apiParam {FundStrategy} fundStrategy fundStrategy对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findFundStrategyPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<FundStrategy>> findFundStrategyPageByParam(@RequestBody FundStrategy fundStrategy, PageParam pageParam) {
        JsonResult<PageResult<FundStrategy>> jr = new JsonResult<PageResult<FundStrategy>>();
        try {
            PageResult<FundStrategy> page = this.fundStrategyService.findFundStrategyPageByParam(fundStrategy, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }




}
