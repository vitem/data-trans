package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanySoftwareCopyrightService;
import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyright;
import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyrightExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanySoftwareCopyrightExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanySoftwareCopyrightMapper;


@Service("companySoftwareCopyrightService")
public class CompanySoftwareCopyrightServiceImpl implements CompanySoftwareCopyrightService {

	private final static Logger logger = LoggerFactory.getLogger(CompanySoftwareCopyrightServiceImpl.class);

	@Resource
	private CompanySoftwareCopyrightMapper companySoftwareCopyrightMapper;


	@Override
	public void saveCompanySoftwareCopyright(CompanySoftwareCopyright companySoftwareCopyright) {
		if (null == companySoftwareCopyright) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companySoftwareCopyrightMapper.insertSelective(companySoftwareCopyright);
	}

	@Override
	public void updateCompanySoftwareCopyright(CompanySoftwareCopyright companySoftwareCopyright) {
		if (null == companySoftwareCopyright) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companySoftwareCopyrightMapper.updateByPrimaryKeySelective(companySoftwareCopyright);
	}

	@Override
	public CompanySoftwareCopyright getCompanySoftwareCopyrightById(String id) {
		return companySoftwareCopyrightMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanySoftwareCopyright> getCompanySoftwareCopyrightList(CompanySoftwareCopyright companySoftwareCopyright) {
		return this.getCompanySoftwareCopyrightListByParam(companySoftwareCopyright, null);
	}

	@Override
	public List<CompanySoftwareCopyright> getCompanySoftwareCopyrightListByParam(CompanySoftwareCopyright companySoftwareCopyright, String orderByStr) {
		CompanySoftwareCopyrightExample example = new CompanySoftwareCopyrightExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanySoftwareCopyright> companySoftwareCopyrightList = this.companySoftwareCopyrightMapper.selectByExample(example);
		return companySoftwareCopyrightList;
	}

	@Override
	public PageResult<CompanySoftwareCopyright> findCompanySoftwareCopyrightPageByParam(CompanySoftwareCopyright companySoftwareCopyright, PageParam pageParam) {
		return this.findCompanySoftwareCopyrightPageByParam(companySoftwareCopyright, pageParam, null);
	}

	@Override
	public PageResult<CompanySoftwareCopyright> findCompanySoftwareCopyrightPageByParam(CompanySoftwareCopyright companySoftwareCopyright, PageParam pageParam, String orderByStr) {
		PageResult<CompanySoftwareCopyright> pageResult = new PageResult<CompanySoftwareCopyright>();

		CompanySoftwareCopyrightExample example = new CompanySoftwareCopyrightExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanySoftwareCopyright> list = null;// companySoftwareCopyrightMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanySoftwareCopyrightById(String id) {
		this.companySoftwareCopyrightMapper.deleteByPrimaryKey(id);
	}

}
