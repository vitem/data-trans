package cn.com.chinaventure.trans.datacenter.web;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.web.BaseAction;
import cn.com.chinaventure.trans.datacenter.service.OrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


@RestController
@RequestMapping("/org")
public class OrganizationController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(OrganizationController.class);

    @Resource
    private OrganizationService organizationService;

    /**
	 * @api {post} / trans organization
	 */
    @RequestMapping(value="/init/{id}",method=RequestMethod.POST)
    public JsonResult<Object> transOrganization(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {

            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }



}
