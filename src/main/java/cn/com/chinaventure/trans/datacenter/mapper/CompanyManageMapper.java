package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyManage;
import cn.com.chinaventure.trans.datacenter.entity.CompanyManageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyManageMapper {
    int countByExample(CompanyManageExample example);

    int deleteByExample(CompanyManageExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyManage record);

    int insertSelective(CompanyManage record);

    List<CompanyManage> selectByExample(CompanyManageExample example);

    CompanyManage selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyManage record, @Param("example") CompanyManageExample example);

    int updateByExample(@Param("record") CompanyManage record, @Param("example") CompanyManageExample example);

    int updateByPrimaryKeySelective(CompanyManage record);

    int updateByPrimaryKey(CompanyManage record);
}