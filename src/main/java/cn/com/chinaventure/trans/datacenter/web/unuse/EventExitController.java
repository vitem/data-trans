package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventExitService;
import cn.com.chinaventure.trans.datacenter.entity.EventExit;


@RestController
@RequestMapping("/eventExit")
public class EventExitController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventExitController.class);

    @Resource
    private EventExitService eventExitService;

    /**
	 * @api {post} / 新增eventExit
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventExit(@RequestBody EventExit eventExit) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventExitService.saveEventExit(eventExit);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventExit
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventExit(@RequestBody EventExit eventExit) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventExitService.updateEventExit(eventExit);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventExit详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventExit> getEventExitById(@PathVariable String id) {
        JsonResult<EventExit> jr = new JsonResult<EventExit>();
        try {
            EventExit eventExit  = eventExitService.getEventExitById(id);
            jr = buildJsonResult(eventExit);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventExit/getEventExitList 查询eventExit列表(不带分页)
	 * @apiGroup eventExit管理
	 * @apiName 查询eventExit列表(不带分页)
	 * @apiDescription 根据条件查询eventExit列表(不带分页)
	 * @apiParam {EventExit} eventExit eventExit对象
	 */
    @RequestMapping("/getEventExitList")
    public JsonResult<List<EventExit>> getEventExitList(EventExit eventExit) {
        JsonResult<List<EventExit>> jr = new JsonResult<List<EventExit>>();
        try {
            List<EventExit> list = this.eventExitService.getEventExitList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventExit/findEventExitPageByParam 查询eventExit列表(带分页)
	 * @apiGroup eventExit管理
	 * @apiName 查询eventExit列表(带分页)
	 * @apiDescription 根据条件查询eventExit列表(带分页)
	 * @apiParam {EventExit} eventExit eventExit对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventExitPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventExit>> findEventExitPageByParam(@RequestBody EventExit eventExit, PageParam pageParam) {
        JsonResult<PageResult<EventExit>> jr = new JsonResult<PageResult<EventExit>>();
        try {
            PageResult<EventExit> page = this.eventExitService.findEventExitPageByParam(eventExit, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventExit/deleteEventExitById 删除eventExit(物理删除)
	 * @apiGroup eventExit管理
	 * @apiName 删除eventExit记录(物理删除)
	 * @apiDescription 根据主键id删除eventExit记录(物理删除)
	 * @apiParam {String} id eventExit主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventExitById",method=RequestMethod.DELETE)
    public JsonResult deleteEventExitById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventExitService.deleteEventExitById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
