package cn.com.chinaventure.trans.datacenter.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.CompanyEmploymentService;
import cn.com.chinaventure.trans.datacenter.entity.CompanyEmployment;
import cn.com.chinaventure.trans.datacenter.entity.CompanyEmploymentExample;
import cn.com.chinaventure.trans.datacenter.entity.CompanyEmploymentExample.Criteria;
import cn.com.chinaventure.trans.datacenter.mapper.CompanyEmploymentMapper;


@Service("companyEmploymentService")
public class CompanyEmploymentServiceImpl implements CompanyEmploymentService {

	private final static Logger logger = LoggerFactory.getLogger(CompanyEmploymentServiceImpl.class);

	@Resource
	private CompanyEmploymentMapper companyEmploymentMapper;


	@Override
	public void saveCompanyEmployment(CompanyEmployment companyEmployment) {
		if (null == companyEmployment) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyEmploymentMapper.insertSelective(companyEmployment);
	}

	@Override
	public void updateCompanyEmployment(CompanyEmployment companyEmployment) {
		if (null == companyEmployment) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.companyEmploymentMapper.updateByPrimaryKeySelective(companyEmployment);
	}

	@Override
	public CompanyEmployment getCompanyEmploymentById(String id) {
		return companyEmploymentMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<CompanyEmployment> getCompanyEmploymentList(CompanyEmployment companyEmployment) {
		return this.getCompanyEmploymentListByParam(companyEmployment, null);
	}

	@Override
	public List<CompanyEmployment> getCompanyEmploymentListByParam(CompanyEmployment companyEmployment, String orderByStr) {
		CompanyEmploymentExample example = new CompanyEmploymentExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<CompanyEmployment> companyEmploymentList = this.companyEmploymentMapper.selectByExample(example);
		return companyEmploymentList;
	}

	@Override
	public PageResult<CompanyEmployment> findCompanyEmploymentPageByParam(CompanyEmployment companyEmployment, PageParam pageParam) {
		return this.findCompanyEmploymentPageByParam(companyEmployment, pageParam, null);
	}

	@Override
	public PageResult<CompanyEmployment> findCompanyEmploymentPageByParam(CompanyEmployment companyEmployment, PageParam pageParam, String orderByStr) {
		PageResult<CompanyEmployment> pageResult = new PageResult<CompanyEmployment>();

		CompanyEmploymentExample example = new CompanyEmploymentExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<CompanyEmployment> list = null;// companyEmploymentMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteCompanyEmploymentById(String id) {
		this.companyEmploymentMapper.deleteByPrimaryKey(id);
	}

}
