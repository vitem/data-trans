package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.CompanyPatent;
import cn.com.chinaventure.trans.datacenter.entity.CompanyPatentExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CompanyPatentMapper {
    int countByExample(CompanyPatentExample example);

    int deleteByExample(CompanyPatentExample example);

    int deleteByPrimaryKey(String id);

    int insert(CompanyPatent record);

    int insertSelective(CompanyPatent record);

    List<CompanyPatent> selectByExample(CompanyPatentExample example);

    CompanyPatent selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") CompanyPatent record, @Param("example") CompanyPatentExample example);

    int updateByExample(@Param("record") CompanyPatent record, @Param("example") CompanyPatentExample example);

    int updateByPrimaryKeySelective(CompanyPatent record);

    int updateByPrimaryKey(CompanyPatent record);
}