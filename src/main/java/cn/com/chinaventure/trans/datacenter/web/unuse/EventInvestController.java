package cn.com.chinaventure.trans.datacenter.web.unuse;

import cn.com.chinaventure.common.error.ErrCodeEWeb;
import cn.com.chinaventure.common.json.JsonResult;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.web.BaseAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.datacenter.service.EventInvestService;
import cn.com.chinaventure.trans.datacenter.entity.EventInvest;


@RestController
@RequestMapping("/eventInvest")
public class EventInvestController extends BaseAction {

    private final static Logger logger = LoggerFactory.getLogger(EventInvestController.class);

    @Resource
    private EventInvestService eventInvestService;

    /**
	 * @api {post} / 新增eventInvest
	 */
    @RequestMapping(value="/",method=RequestMethod.POST)
    public JsonResult<Object> saveEventInvest(@RequestBody EventInvest eventInvest) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventInvestService.saveEventInvest(eventInvest);
            jr = buildJsonResult(null);

        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {post} / 修改eventInvest
	 */
    @RequestMapping(value="/",method=RequestMethod.PUT)
    public JsonResult updateEventInvest(@RequestBody EventInvest eventInvest) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventInvestService.updateEventInvest(eventInvest);
            jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }


	/**
	 * @api {get} /{id} 查询eventInvest详情
	 */
    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public JsonResult<EventInvest> getEventInvestById(@PathVariable String id) {
        JsonResult<EventInvest> jr = new JsonResult<EventInvest>();
        try {
            EventInvest eventInvest  = eventInvestService.getEventInvestById(id);
            jr = buildJsonResult(eventInvest);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

    /**
	 * @api {get} /eventInvest/getEventInvestList 查询eventInvest列表(不带分页)
	 * @apiGroup eventInvest管理
	 * @apiName 查询eventInvest列表(不带分页)
	 * @apiDescription 根据条件查询eventInvest列表(不带分页)
	 * @apiParam {EventInvest} eventInvest eventInvest对象
	 */
    @RequestMapping("/getEventInvestList")
    public JsonResult<List<EventInvest>> getEventInvestList(EventInvest eventInvest) {
        JsonResult<List<EventInvest>> jr = new JsonResult<List<EventInvest>>();
        try {
            List<EventInvest> list = this.eventInvestService.getEventInvestList(null);
			jr = buildJsonResult(list);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }
        return jr;
    }

	/**
	 * @api {get} /eventInvest/findEventInvestPageByParam 查询eventInvest列表(带分页)
	 * @apiGroup eventInvest管理
	 * @apiName 查询eventInvest列表(带分页)
	 * @apiDescription 根据条件查询eventInvest列表(带分页)
	 * @apiParam {EventInvest} eventInvest eventInvest对象
	 * @apiParam {PageParam} pageParam 分页对象
	 * @apiVersion 1.0.0
	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *   "success": true,
	 *   "data": {
	 *      "pageSize": 5,
	 *      "pageNo": 1,
	 *      "totalCount": 1,
	 *      "totalPage": 1,
	 *       "list": [
	 *           {
	 *              "file1": "值1",
	 *              "file2": "值2"
	 *          }
	 *      ]
	 *  },
	 *  "code": "0",	//0：成功，非0：失败，见错误码
	 *  "message": "ok"
	 * }
	 */
    @RequestMapping(value="/findEventInvestPageByParam",method=RequestMethod.POST)
    public JsonResult<PageResult<EventInvest>> findEventInvestPageByParam(@RequestBody EventInvest eventInvest, PageParam pageParam) {
        JsonResult<PageResult<EventInvest>> jr = new JsonResult<PageResult<EventInvest>>();
        try {
            PageResult<EventInvest> page = this.eventInvestService.findEventInvestPageByParam(eventInvest, pageParam);
			jr = buildJsonResult(page);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }

    /**
	 * @api {post} /eventInvest/deleteEventInvestById 删除eventInvest(物理删除)
	 * @apiGroup eventInvest管理
	 * @apiName 删除eventInvest记录(物理删除)
	 * @apiDescription 根据主键id删除eventInvest记录(物理删除)
	 * @apiParam {String} id eventInvest主键id
	 * @apiVersion 1.0.0

	 * @apiSuccessExample {json} Response 200 Example
	 * HTTP/1.1 200 OK
	 * {
	 *     "success": true,
	 *     "data": {
	 *     },
	 *     "code": "0",	//0：成功，非0：失败，见错误码
	 *     "message": "ok"
	 * }
	 */
    @RequestMapping(value="/deleteEventInvestById",method=RequestMethod.DELETE)
    public JsonResult deleteEventInvestById(@PathVariable String id) {
        JsonResult<Object> jr = new JsonResult<Object>();
        try {
            this.eventInvestService.deleteEventInvestById(id);
			jr = buildJsonResult(null);
        } catch (Exception e) {
            logger.error("error", e);
            jr = buildFailJsonResult(false, ErrCodeEWeb.NONE);
        }

        return jr;
    }


}
