package cn.com.chinaventure.trans.datacenter.mapper;

import cn.com.chinaventure.trans.datacenter.entity.EventMergeSeller;
import cn.com.chinaventure.trans.datacenter.entity.EventMergeSellerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface EventMergeSellerMapper {
    int countByExample(EventMergeSellerExample example);

    int deleteByExample(EventMergeSellerExample example);

    int deleteByPrimaryKey(String id);

    int insert(EventMergeSeller record);

    int insertSelective(EventMergeSeller record);

    List<EventMergeSeller> selectByExample(EventMergeSellerExample example);

    EventMergeSeller selectByPrimaryKey(String id);

    int updateByExampleSelective(@Param("record") EventMergeSeller record, @Param("example") EventMergeSellerExample example);

    int updateByExample(@Param("record") EventMergeSeller record, @Param("example") EventMergeSellerExample example);

    int updateByPrimaryKeySelective(EventMergeSeller record);

    int updateByPrimaryKey(EventMergeSeller record);
}