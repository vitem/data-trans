package cn.com.chinaventure.trans.datacenter.entity;

import java.io.Serializable;
import java.util.Date;

public class CompanyCopyright implements Serializable {
    private String id;

    private String companyId;

    /**
     * 作品名称
     */
    private String copyrightName;

    /**
     * 登记号
     */
    private String applyNumber;

    /**
     * 版权又称著作权，根据不同的标准可以分为如下种类：?
            （1）根据版权的权利可以为为：发表权，署名权，修改权，保护作品完整权，复制权，发行权，出租权，展览权，表演权，放映权，广播权，信息网络传播权，摄制权，改编权，翻译权，汇编权，应当由著作权人享有的其他权利。?
            （2）根据版权的表现形式可以分为：软件著作权和作品著作权。作品著作权又分为：文字作品；口述作品；音乐、戏剧、曲艺、舞蹈作品；美术、摄影作品；电影、电视、录像作品；工程设计、产品设计图纸及其说明；地图、示意图等图形作品；法律、行政法规规定的其他作品。
     */
    private String category;

    /**
     * 创作完成日期
     */
    private Date endDate;

    /**
     * 登记日期
     */
    private Date enregisterDate;

    /**
     * 首次发布日期
     */
    private Date startPublishDate;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;

    private static final long serialVersionUID = 1L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId == null ? null : companyId.trim();
    }

    public String getCopyrightName() {
        return copyrightName;
    }

    public void setCopyrightName(String copyrightName) {
        this.copyrightName = copyrightName == null ? null : copyrightName.trim();
    }

    public String getApplyNumber() {
        return applyNumber;
    }

    public void setApplyNumber(String applyNumber) {
        this.applyNumber = applyNumber == null ? null : applyNumber.trim();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category == null ? null : category.trim();
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getEnregisterDate() {
        return enregisterDate;
    }

    public void setEnregisterDate(Date enregisterDate) {
        this.enregisterDate = enregisterDate;
    }

    public Date getStartPublishDate() {
        return startPublishDate;
    }

    public void setStartPublishDate(Date startPublishDate) {
        this.startPublishDate = startPublishDate;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", companyId=").append(companyId);
        sb.append(", copyrightName=").append(copyrightName);
        sb.append(", applyNumber=").append(applyNumber);
        sb.append(", category=").append(category);
        sb.append(", endDate=").append(endDate);
        sb.append(", enregisterDate=").append(enregisterDate);
        sb.append(", startPublishDate=").append(startPublishDate);
        sb.append(", ext1=").append(ext1);
        sb.append(", ext2=").append(ext2);
        sb.append(", ext3=").append(ext3);
        sb.append(", ext4=").append(ext4);
        sb.append(", ext5=").append(ext5);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}