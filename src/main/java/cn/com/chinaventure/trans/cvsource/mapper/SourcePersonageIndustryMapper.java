package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageIndustry;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageIndustryExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePersonageIndustryMapper {
    int countByExample(SourcePersonageIndustryExample example);

    int deleteByExample(SourcePersonageIndustryExample example);

    int deleteByPrimaryKey(Integer personageindustryId);

    int insert(SourcePersonageIndustry record);

    int insertSelective(SourcePersonageIndustry record);

    List<SourcePersonageIndustry> selectByExample(SourcePersonageIndustryExample example);

    SourcePersonageIndustry selectByPrimaryKey(Integer personageindustryId);

    int updateByExampleSelective(@Param("record") SourcePersonageIndustry record, @Param("example") SourcePersonageIndustryExample example);

    int updateByExample(@Param("record") SourcePersonageIndustry record, @Param("example") SourcePersonageIndustryExample example);

    int updateByPrimaryKeySelective(SourcePersonageIndustry record);

    int updateByPrimaryKey(SourcePersonageIndustry record);
}