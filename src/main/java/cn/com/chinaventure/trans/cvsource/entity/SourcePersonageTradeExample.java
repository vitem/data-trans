package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.List;

public class SourcePersonageTradeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePersonageTradeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPidIsNull() {
            addCriterion("pid is null");
            return (Criteria) this;
        }

        public Criteria andPidIsNotNull() {
            addCriterion("pid is not null");
            return (Criteria) this;
        }

        public Criteria andPidEqualTo(Long value) {
            addCriterion("pid =", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotEqualTo(Long value) {
            addCriterion("pid <>", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThan(Long value) {
            addCriterion("pid >", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidGreaterThanOrEqualTo(Long value) {
            addCriterion("pid >=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThan(Long value) {
            addCriterion("pid <", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidLessThanOrEqualTo(Long value) {
            addCriterion("pid <=", value, "pid");
            return (Criteria) this;
        }

        public Criteria andPidIn(List<Long> values) {
            addCriterion("pid in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotIn(List<Long> values) {
            addCriterion("pid not in", values, "pid");
            return (Criteria) this;
        }

        public Criteria andPidBetween(Long value1, Long value2) {
            addCriterion("pid between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andPidNotBetween(Long value1, Long value2) {
            addCriterion("pid not between", value1, value2, "pid");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIsNull() {
            addCriterion("category_id is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIsNotNull() {
            addCriterion("category_id is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryIdEqualTo(Byte value) {
            addCriterion("category_id =", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotEqualTo(Byte value) {
            addCriterion("category_id <>", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThan(Byte value) {
            addCriterion("category_id >", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("category_id >=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThan(Byte value) {
            addCriterion("category_id <", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdLessThanOrEqualTo(Byte value) {
            addCriterion("category_id <=", value, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdIn(List<Byte> values) {
            addCriterion("category_id in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotIn(List<Byte> values) {
            addCriterion("category_id not in", values, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdBetween(Byte value1, Byte value2) {
            addCriterion("category_id between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andCategoryIdNotBetween(Byte value1, Byte value2) {
            addCriterion("category_id not between", value1, value2, "categoryId");
            return (Criteria) this;
        }

        public Criteria andFullIdIsNull() {
            addCriterion("full_id is null");
            return (Criteria) this;
        }

        public Criteria andFullIdIsNotNull() {
            addCriterion("full_id is not null");
            return (Criteria) this;
        }

        public Criteria andFullIdEqualTo(String value) {
            addCriterion("full_id =", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdNotEqualTo(String value) {
            addCriterion("full_id <>", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdGreaterThan(String value) {
            addCriterion("full_id >", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdGreaterThanOrEqualTo(String value) {
            addCriterion("full_id >=", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdLessThan(String value) {
            addCriterion("full_id <", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdLessThanOrEqualTo(String value) {
            addCriterion("full_id <=", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdLike(String value) {
            addCriterion("full_id like", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdNotLike(String value) {
            addCriterion("full_id not like", value, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdIn(List<String> values) {
            addCriterion("full_id in", values, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdNotIn(List<String> values) {
            addCriterion("full_id not in", values, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdBetween(String value1, String value2) {
            addCriterion("full_id between", value1, value2, "fullId");
            return (Criteria) this;
        }

        public Criteria andFullIdNotBetween(String value1, String value2) {
            addCriterion("full_id not between", value1, value2, "fullId");
            return (Criteria) this;
        }

        public Criteria andTradeOneIsNull() {
            addCriterion("trade_one is null");
            return (Criteria) this;
        }

        public Criteria andTradeOneIsNotNull() {
            addCriterion("trade_one is not null");
            return (Criteria) this;
        }

        public Criteria andTradeOneEqualTo(Long value) {
            addCriterion("trade_one =", value, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneNotEqualTo(Long value) {
            addCriterion("trade_one <>", value, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneGreaterThan(Long value) {
            addCriterion("trade_one >", value, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_one >=", value, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneLessThan(Long value) {
            addCriterion("trade_one <", value, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneLessThanOrEqualTo(Long value) {
            addCriterion("trade_one <=", value, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneIn(List<Long> values) {
            addCriterion("trade_one in", values, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneNotIn(List<Long> values) {
            addCriterion("trade_one not in", values, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneBetween(Long value1, Long value2) {
            addCriterion("trade_one between", value1, value2, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeOneNotBetween(Long value1, Long value2) {
            addCriterion("trade_one not between", value1, value2, "tradeOne");
            return (Criteria) this;
        }

        public Criteria andTradeTwoIsNull() {
            addCriterion("trade_two is null");
            return (Criteria) this;
        }

        public Criteria andTradeTwoIsNotNull() {
            addCriterion("trade_two is not null");
            return (Criteria) this;
        }

        public Criteria andTradeTwoEqualTo(Long value) {
            addCriterion("trade_two =", value, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoNotEqualTo(Long value) {
            addCriterion("trade_two <>", value, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoGreaterThan(Long value) {
            addCriterion("trade_two >", value, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_two >=", value, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoLessThan(Long value) {
            addCriterion("trade_two <", value, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoLessThanOrEqualTo(Long value) {
            addCriterion("trade_two <=", value, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoIn(List<Long> values) {
            addCriterion("trade_two in", values, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoNotIn(List<Long> values) {
            addCriterion("trade_two not in", values, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoBetween(Long value1, Long value2) {
            addCriterion("trade_two between", value1, value2, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeTwoNotBetween(Long value1, Long value2) {
            addCriterion("trade_two not between", value1, value2, "tradeTwo");
            return (Criteria) this;
        }

        public Criteria andTradeThreeIsNull() {
            addCriterion("trade_three is null");
            return (Criteria) this;
        }

        public Criteria andTradeThreeIsNotNull() {
            addCriterion("trade_three is not null");
            return (Criteria) this;
        }

        public Criteria andTradeThreeEqualTo(Long value) {
            addCriterion("trade_three =", value, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeNotEqualTo(Long value) {
            addCriterion("trade_three <>", value, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeGreaterThan(Long value) {
            addCriterion("trade_three >", value, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_three >=", value, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeLessThan(Long value) {
            addCriterion("trade_three <", value, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeLessThanOrEqualTo(Long value) {
            addCriterion("trade_three <=", value, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeIn(List<Long> values) {
            addCriterion("trade_three in", values, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeNotIn(List<Long> values) {
            addCriterion("trade_three not in", values, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeBetween(Long value1, Long value2) {
            addCriterion("trade_three between", value1, value2, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeThreeNotBetween(Long value1, Long value2) {
            addCriterion("trade_three not between", value1, value2, "tradeThree");
            return (Criteria) this;
        }

        public Criteria andTradeFourIsNull() {
            addCriterion("trade_four is null");
            return (Criteria) this;
        }

        public Criteria andTradeFourIsNotNull() {
            addCriterion("trade_four is not null");
            return (Criteria) this;
        }

        public Criteria andTradeFourEqualTo(Long value) {
            addCriterion("trade_four =", value, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourNotEqualTo(Long value) {
            addCriterion("trade_four <>", value, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourGreaterThan(Long value) {
            addCriterion("trade_four >", value, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourGreaterThanOrEqualTo(Long value) {
            addCriterion("trade_four >=", value, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourLessThan(Long value) {
            addCriterion("trade_four <", value, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourLessThanOrEqualTo(Long value) {
            addCriterion("trade_four <=", value, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourIn(List<Long> values) {
            addCriterion("trade_four in", values, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourNotIn(List<Long> values) {
            addCriterion("trade_four not in", values, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourBetween(Long value1, Long value2) {
            addCriterion("trade_four between", value1, value2, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andTradeFourNotBetween(Long value1, Long value2) {
            addCriterion("trade_four not between", value1, value2, "tradeFour");
            return (Criteria) this;
        }

        public Criteria andFullCnNameIsNull() {
            addCriterion("full_cn_name is null");
            return (Criteria) this;
        }

        public Criteria andFullCnNameIsNotNull() {
            addCriterion("full_cn_name is not null");
            return (Criteria) this;
        }

        public Criteria andFullCnNameEqualTo(String value) {
            addCriterion("full_cn_name =", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameNotEqualTo(String value) {
            addCriterion("full_cn_name <>", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameGreaterThan(String value) {
            addCriterion("full_cn_name >", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("full_cn_name >=", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameLessThan(String value) {
            addCriterion("full_cn_name <", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameLessThanOrEqualTo(String value) {
            addCriterion("full_cn_name <=", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameLike(String value) {
            addCriterion("full_cn_name like", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameNotLike(String value) {
            addCriterion("full_cn_name not like", value, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameIn(List<String> values) {
            addCriterion("full_cn_name in", values, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameNotIn(List<String> values) {
            addCriterion("full_cn_name not in", values, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameBetween(String value1, String value2) {
            addCriterion("full_cn_name between", value1, value2, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andFullCnNameNotBetween(String value1, String value2) {
            addCriterion("full_cn_name not between", value1, value2, "fullCnName");
            return (Criteria) this;
        }

        public Criteria andLevelIdIsNull() {
            addCriterion("level_id is null");
            return (Criteria) this;
        }

        public Criteria andLevelIdIsNotNull() {
            addCriterion("level_id is not null");
            return (Criteria) this;
        }

        public Criteria andLevelIdEqualTo(Byte value) {
            addCriterion("level_id =", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdNotEqualTo(Byte value) {
            addCriterion("level_id <>", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdGreaterThan(Byte value) {
            addCriterion("level_id >", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("level_id >=", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdLessThan(Byte value) {
            addCriterion("level_id <", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdLessThanOrEqualTo(Byte value) {
            addCriterion("level_id <=", value, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdIn(List<Byte> values) {
            addCriterion("level_id in", values, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdNotIn(List<Byte> values) {
            addCriterion("level_id not in", values, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdBetween(Byte value1, Byte value2) {
            addCriterion("level_id between", value1, value2, "levelId");
            return (Criteria) this;
        }

        public Criteria andLevelIdNotBetween(Byte value1, Byte value2) {
            addCriterion("level_id not between", value1, value2, "levelId");
            return (Criteria) this;
        }

        public Criteria andCnNameIsNull() {
            addCriterion("CN_NAME is null");
            return (Criteria) this;
        }

        public Criteria andCnNameIsNotNull() {
            addCriterion("CN_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andCnNameEqualTo(String value) {
            addCriterion("CN_NAME =", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameNotEqualTo(String value) {
            addCriterion("CN_NAME <>", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameGreaterThan(String value) {
            addCriterion("CN_NAME >", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("CN_NAME >=", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameLessThan(String value) {
            addCriterion("CN_NAME <", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameLessThanOrEqualTo(String value) {
            addCriterion("CN_NAME <=", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameLike(String value) {
            addCriterion("CN_NAME like", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameNotLike(String value) {
            addCriterion("CN_NAME not like", value, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameIn(List<String> values) {
            addCriterion("CN_NAME in", values, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameNotIn(List<String> values) {
            addCriterion("CN_NAME not in", values, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameBetween(String value1, String value2) {
            addCriterion("CN_NAME between", value1, value2, "cnName");
            return (Criteria) this;
        }

        public Criteria andCnNameNotBetween(String value1, String value2) {
            addCriterion("CN_NAME not between", value1, value2, "cnName");
            return (Criteria) this;
        }

        public Criteria andEnNameIsNull() {
            addCriterion("EN_NAME is null");
            return (Criteria) this;
        }

        public Criteria andEnNameIsNotNull() {
            addCriterion("EN_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andEnNameEqualTo(String value) {
            addCriterion("EN_NAME =", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameNotEqualTo(String value) {
            addCriterion("EN_NAME <>", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameGreaterThan(String value) {
            addCriterion("EN_NAME >", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameGreaterThanOrEqualTo(String value) {
            addCriterion("EN_NAME >=", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameLessThan(String value) {
            addCriterion("EN_NAME <", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameLessThanOrEqualTo(String value) {
            addCriterion("EN_NAME <=", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameLike(String value) {
            addCriterion("EN_NAME like", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameNotLike(String value) {
            addCriterion("EN_NAME not like", value, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameIn(List<String> values) {
            addCriterion("EN_NAME in", values, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameNotIn(List<String> values) {
            addCriterion("EN_NAME not in", values, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameBetween(String value1, String value2) {
            addCriterion("EN_NAME between", value1, value2, "enName");
            return (Criteria) this;
        }

        public Criteria andEnNameNotBetween(String value1, String value2) {
            addCriterion("EN_NAME not between", value1, value2, "enName");
            return (Criteria) this;
        }

        public Criteria andDomainIdIsNull() {
            addCriterion("domain_id is null");
            return (Criteria) this;
        }

        public Criteria andDomainIdIsNotNull() {
            addCriterion("domain_id is not null");
            return (Criteria) this;
        }

        public Criteria andDomainIdEqualTo(Long value) {
            addCriterion("domain_id =", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotEqualTo(Long value) {
            addCriterion("domain_id <>", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdGreaterThan(Long value) {
            addCriterion("domain_id >", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdGreaterThanOrEqualTo(Long value) {
            addCriterion("domain_id >=", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdLessThan(Long value) {
            addCriterion("domain_id <", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdLessThanOrEqualTo(Long value) {
            addCriterion("domain_id <=", value, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdIn(List<Long> values) {
            addCriterion("domain_id in", values, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotIn(List<Long> values) {
            addCriterion("domain_id not in", values, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdBetween(Long value1, Long value2) {
            addCriterion("domain_id between", value1, value2, "domainId");
            return (Criteria) this;
        }

        public Criteria andDomainIdNotBetween(Long value1, Long value2) {
            addCriterion("domain_id not between", value1, value2, "domainId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}