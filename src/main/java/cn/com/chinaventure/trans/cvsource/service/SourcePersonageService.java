package cn.com.chinaventure.trans.cvsource.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonage;

import java.util.List;

/**
 * @author 
 *
 */
public interface SourcePersonageService {
	
	/**
     * @api saveSourcePersonage 新增sourcePersonage
     * @apiGroup sourcePersonage管理
     * @apiName  新增sourcePersonage记录
     * @apiDescription 全量插入sourcePersonage记录
     * @apiParam {SourcePersonage} sourcePersonage sourcePersonage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePersonage(SourcePersonage sourcePersonage);
	
	/**
     * @api updateSourcePersonage 修改sourcePersonage
     * @apiGroup sourcePersonage管理
     * @apiName  修改sourcePersonage记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePersonage} sourcePersonage sourcePersonage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePersonage(SourcePersonage sourcePersonage);
	
	/**
     * @api getSourcePersonageById 根据sourcePersonageid查询详情
     * @apiGroup sourcePersonage管理
     * @apiName  查询sourcePersonage详情
     * @apiDescription 根据主键id查询sourcePersonage详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePersonage sourcePersonage实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePersonage getSourcePersonageById(Long id);
	
	/**
     * @api getSourcePersonageList 根据sourcePersonage条件查询列表
     * @apiGroup sourcePersonage管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePersonage} sourcePersonage  实体条件
     * @apiSuccess List<SourcePersonage> sourcePersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonage> getSourcePersonageList(SourcePersonage sourcePersonage);
	

	/**
     * @api getSourcePersonageListByParam 根据sourcePersonage条件查询列表（含排序）
     * @apiGroup sourcePersonage管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePersonage} sourcePersonage  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePersonage> sourcePersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonage> getSourcePersonageListByParam(SourcePersonage sourcePersonage, String orderByStr);
	
	
	/**
     * @api findSourcePersonagePageByParam 根据sourcePersonage条件查询列表（分页）
     * @apiGroup sourcePersonage管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePersonage} sourcePersonage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePersonage> sourcePersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonage> findSourcePersonagePageByParam(SourcePersonage sourcePersonage, PageParam pageParam); 
	
	/**
     * @api findSourcePersonagePageByParam 根据sourcePersonage条件查询列表（分页，含排序）
     * @apiGroup sourcePersonage管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePersonage} sourcePersonage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePersonage> sourcePersonage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonage> findSourcePersonagePageByParam(SourcePersonage sourcePersonage, PageParam pageParam, String orderByStr);
	
	

}

