package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonage;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePersonageMapper;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("sourcePersonageService")
public class SourcePersonageServiceImpl implements SourcePersonageService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePersonageServiceImpl.class);

	@Resource
	private SourcePersonageMapper sourcePersonageMapper;


	@Override
	public void saveSourcePersonage(SourcePersonage sourcePersonage) {
		if (null == sourcePersonage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageMapper.insertSelective(sourcePersonage);
	}

	@Override
	public void updateSourcePersonage(SourcePersonage sourcePersonage) {
		if (null == sourcePersonage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageMapper.updateByPrimaryKeySelective(sourcePersonage);
	}

	@Override
	public SourcePersonage getSourcePersonageById(Long id) {
		return sourcePersonageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePersonage> getSourcePersonageList(SourcePersonage sourcePersonage) {
		return this.getSourcePersonageListByParam(sourcePersonage, null);
	}

	@Override
	public List<SourcePersonage> getSourcePersonageListByParam(SourcePersonage sourcePersonage, String orderByStr) {
		SourcePersonageExample example = new SourcePersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePersonage> sourcePersonageList = this.sourcePersonageMapper.selectByExample(example);
		return sourcePersonageList;
	}

	@Override
	public PageResult<SourcePersonage> findSourcePersonagePageByParam(SourcePersonage sourcePersonage, PageParam pageParam) {
		return this.findSourcePersonagePageByParam(sourcePersonage, pageParam, null);
	}

	@Override
	public PageResult<SourcePersonage> findSourcePersonagePageByParam(SourcePersonage sourcePersonage, PageParam pageParam, String orderByStr) {
		PageResult<SourcePersonage> pageResult = new PageResult<SourcePersonage>();

		SourcePersonageExample example = new SourcePersonageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePersonage> list = null;// sourcePersonageMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
