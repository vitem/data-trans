package cn.com.chinaventure.trans.cvsource.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePosition;

import java.util.List;

/**
 * @author 
 *
 */
public interface SourcePositionService {
	
	/**
     * @api saveSourcePosition 新增sourcePosition
     * @apiGroup sourcePosition管理
     * @apiName  新增sourcePosition记录
     * @apiDescription 全量插入sourcePosition记录
     * @apiParam {SourcePosition} sourcePosition sourcePosition对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePosition(SourcePosition sourcePosition);
	
	/**
     * @api updateSourcePosition 修改sourcePosition
     * @apiGroup sourcePosition管理
     * @apiName  修改sourcePosition记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePosition} sourcePosition sourcePosition对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePosition(SourcePosition sourcePosition);
	
	/**
     * @api getSourcePositionById 根据sourcePositionid查询详情
     * @apiGroup sourcePosition管理
     * @apiName  查询sourcePosition详情
     * @apiDescription 根据主键id查询sourcePosition详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePosition sourcePosition实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePosition getSourcePositionById(Long id);
	
	/**
     * @api getSourcePositionList 根据sourcePosition条件查询列表
     * @apiGroup sourcePosition管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePosition} sourcePosition  实体条件
     * @apiSuccess List<SourcePosition> sourcePosition实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePosition> getSourcePositionList(SourcePosition sourcePosition);
	

	/**
     * @api getSourcePositionListByParam 根据sourcePosition条件查询列表（含排序）
     * @apiGroup sourcePosition管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePosition} sourcePosition  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePosition> sourcePosition实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePosition> getSourcePositionListByParam(SourcePosition sourcePosition, String orderByStr);
	
	
	/**
     * @api findSourcePositionPageByParam 根据sourcePosition条件查询列表（分页）
     * @apiGroup sourcePosition管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePosition} sourcePosition  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePosition> sourcePosition实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePosition> findSourcePositionPageByParam(SourcePosition sourcePosition, PageParam pageParam); 
	
	/**
     * @api findSourcePositionPageByParam 根据sourcePosition条件查询列表（分页，含排序）
     * @apiGroup sourcePosition管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePosition} sourcePosition  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePosition> sourcePosition实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePosition> findSourcePositionPageByParam(SourcePosition sourcePosition, PageParam pageParam, String orderByStr);
	
	

}

