package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;

public class SourcePersonageInternate implements Serializable {
    private Integer personageinternateId;

    private Byte epIndustryOneid;

    private Integer epIndustryTwoid;

    private Integer epIndustryThreeid;

    private Integer epIndustryFourid;

    private Byte epIndustryType;

    private Integer personageId;

    private static final long serialVersionUID = 1L;

    public Integer getPersonageinternateId() {
        return personageinternateId;
    }

    public void setPersonageinternateId(Integer personageinternateId) {
        this.personageinternateId = personageinternateId;
    }

    public Byte getEpIndustryOneid() {
        return epIndustryOneid;
    }

    public void setEpIndustryOneid(Byte epIndustryOneid) {
        this.epIndustryOneid = epIndustryOneid;
    }

    public Integer getEpIndustryTwoid() {
        return epIndustryTwoid;
    }

    public void setEpIndustryTwoid(Integer epIndustryTwoid) {
        this.epIndustryTwoid = epIndustryTwoid;
    }

    public Integer getEpIndustryThreeid() {
        return epIndustryThreeid;
    }

    public void setEpIndustryThreeid(Integer epIndustryThreeid) {
        this.epIndustryThreeid = epIndustryThreeid;
    }

    public Integer getEpIndustryFourid() {
        return epIndustryFourid;
    }

    public void setEpIndustryFourid(Integer epIndustryFourid) {
        this.epIndustryFourid = epIndustryFourid;
    }

    public Byte getEpIndustryType() {
        return epIndustryType;
    }

    public void setEpIndustryType(Byte epIndustryType) {
        this.epIndustryType = epIndustryType;
    }

    public Integer getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Integer personageId) {
        this.personageId = personageId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", personageinternateId=").append(personageinternateId);
        sb.append(", epIndustryOneid=").append(epIndustryOneid);
        sb.append(", epIndustryTwoid=").append(epIndustryTwoid);
        sb.append(", epIndustryThreeid=").append(epIndustryThreeid);
        sb.append(", epIndustryFourid=").append(epIndustryFourid);
        sb.append(", epIndustryType=").append(epIndustryType);
        sb.append(", personageId=").append(personageId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}