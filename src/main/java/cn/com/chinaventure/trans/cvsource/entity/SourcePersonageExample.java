package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SourcePersonageExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePersonageExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("Personage_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("Personage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(Long value) {
            addCriterion("Personage_ID =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(Long value) {
            addCriterion("Personage_ID <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(Long value) {
            addCriterion("Personage_ID >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Personage_ID >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(Long value) {
            addCriterion("Personage_ID <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(Long value) {
            addCriterion("Personage_ID <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<Long> values) {
            addCriterion("Personage_ID in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<Long> values) {
            addCriterion("Personage_ID not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(Long value1, Long value2) {
            addCriterion("Personage_ID between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(Long value1, Long value2) {
            addCriterion("Personage_ID not between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameIsNull() {
            addCriterion("Personage_CN_Name is null");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameIsNotNull() {
            addCriterion("Personage_CN_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameEqualTo(String value) {
            addCriterion("Personage_CN_Name =", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameNotEqualTo(String value) {
            addCriterion("Personage_CN_Name <>", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameGreaterThan(String value) {
            addCriterion("Personage_CN_Name >", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_CN_Name >=", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameLessThan(String value) {
            addCriterion("Personage_CN_Name <", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameLessThanOrEqualTo(String value) {
            addCriterion("Personage_CN_Name <=", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameLike(String value) {
            addCriterion("Personage_CN_Name like", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameNotLike(String value) {
            addCriterion("Personage_CN_Name not like", value, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameIn(List<String> values) {
            addCriterion("Personage_CN_Name in", values, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameNotIn(List<String> values) {
            addCriterion("Personage_CN_Name not in", values, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameBetween(String value1, String value2) {
            addCriterion("Personage_CN_Name between", value1, value2, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageCnNameNotBetween(String value1, String value2) {
            addCriterion("Personage_CN_Name not between", value1, value2, "personageCnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameIsNull() {
            addCriterion("Personage_EN_Name is null");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameIsNotNull() {
            addCriterion("Personage_EN_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameEqualTo(String value) {
            addCriterion("Personage_EN_Name =", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameNotEqualTo(String value) {
            addCriterion("Personage_EN_Name <>", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameGreaterThan(String value) {
            addCriterion("Personage_EN_Name >", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_EN_Name >=", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameLessThan(String value) {
            addCriterion("Personage_EN_Name <", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameLessThanOrEqualTo(String value) {
            addCriterion("Personage_EN_Name <=", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameLike(String value) {
            addCriterion("Personage_EN_Name like", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameNotLike(String value) {
            addCriterion("Personage_EN_Name not like", value, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameIn(List<String> values) {
            addCriterion("Personage_EN_Name in", values, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameNotIn(List<String> values) {
            addCriterion("Personage_EN_Name not in", values, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameBetween(String value1, String value2) {
            addCriterion("Personage_EN_Name between", value1, value2, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageEnNameNotBetween(String value1, String value2) {
            addCriterion("Personage_EN_Name not between", value1, value2, "personageEnName");
            return (Criteria) this;
        }

        public Criteria andPersonageSexIsNull() {
            addCriterion("Personage_Sex is null");
            return (Criteria) this;
        }

        public Criteria andPersonageSexIsNotNull() {
            addCriterion("Personage_Sex is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageSexEqualTo(Long value) {
            addCriterion("Personage_Sex =", value, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexNotEqualTo(Long value) {
            addCriterion("Personage_Sex <>", value, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexGreaterThan(Long value) {
            addCriterion("Personage_Sex >", value, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexGreaterThanOrEqualTo(Long value) {
            addCriterion("Personage_Sex >=", value, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexLessThan(Long value) {
            addCriterion("Personage_Sex <", value, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexLessThanOrEqualTo(Long value) {
            addCriterion("Personage_Sex <=", value, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexIn(List<Long> values) {
            addCriterion("Personage_Sex in", values, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexNotIn(List<Long> values) {
            addCriterion("Personage_Sex not in", values, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexBetween(Long value1, Long value2) {
            addCriterion("Personage_Sex between", value1, value2, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageSexNotBetween(Long value1, Long value2) {
            addCriterion("Personage_Sex not between", value1, value2, "personageSex");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneIsNull() {
            addCriterion("Personage_Telephone is null");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneIsNotNull() {
            addCriterion("Personage_Telephone is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneEqualTo(String value) {
            addCriterion("Personage_Telephone =", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneNotEqualTo(String value) {
            addCriterion("Personage_Telephone <>", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneGreaterThan(String value) {
            addCriterion("Personage_Telephone >", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Telephone >=", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneLessThan(String value) {
            addCriterion("Personage_Telephone <", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneLessThanOrEqualTo(String value) {
            addCriterion("Personage_Telephone <=", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneLike(String value) {
            addCriterion("Personage_Telephone like", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneNotLike(String value) {
            addCriterion("Personage_Telephone not like", value, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneIn(List<String> values) {
            addCriterion("Personage_Telephone in", values, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneNotIn(List<String> values) {
            addCriterion("Personage_Telephone not in", values, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneBetween(String value1, String value2) {
            addCriterion("Personage_Telephone between", value1, value2, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageTelephoneNotBetween(String value1, String value2) {
            addCriterion("Personage_Telephone not between", value1, value2, "personageTelephone");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileIsNull() {
            addCriterion("Personage_Mobile is null");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileIsNotNull() {
            addCriterion("Personage_Mobile is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileEqualTo(String value) {
            addCriterion("Personage_Mobile =", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileNotEqualTo(String value) {
            addCriterion("Personage_Mobile <>", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileGreaterThan(String value) {
            addCriterion("Personage_Mobile >", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Mobile >=", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileLessThan(String value) {
            addCriterion("Personage_Mobile <", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileLessThanOrEqualTo(String value) {
            addCriterion("Personage_Mobile <=", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileLike(String value) {
            addCriterion("Personage_Mobile like", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileNotLike(String value) {
            addCriterion("Personage_Mobile not like", value, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileIn(List<String> values) {
            addCriterion("Personage_Mobile in", values, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileNotIn(List<String> values) {
            addCriterion("Personage_Mobile not in", values, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileBetween(String value1, String value2) {
            addCriterion("Personage_Mobile between", value1, value2, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMobileNotBetween(String value1, String value2) {
            addCriterion("Personage_Mobile not between", value1, value2, "personageMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageMailIsNull() {
            addCriterion("Personage_Mail is null");
            return (Criteria) this;
        }

        public Criteria andPersonageMailIsNotNull() {
            addCriterion("Personage_Mail is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageMailEqualTo(String value) {
            addCriterion("Personage_Mail =", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailNotEqualTo(String value) {
            addCriterion("Personage_Mail <>", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailGreaterThan(String value) {
            addCriterion("Personage_Mail >", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Mail >=", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailLessThan(String value) {
            addCriterion("Personage_Mail <", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailLessThanOrEqualTo(String value) {
            addCriterion("Personage_Mail <=", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailLike(String value) {
            addCriterion("Personage_Mail like", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailNotLike(String value) {
            addCriterion("Personage_Mail not like", value, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailIn(List<String> values) {
            addCriterion("Personage_Mail in", values, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailNotIn(List<String> values) {
            addCriterion("Personage_Mail not in", values, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailBetween(String value1, String value2) {
            addCriterion("Personage_Mail between", value1, value2, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonageMailNotBetween(String value1, String value2) {
            addCriterion("Personage_Mail not between", value1, value2, "personageMail");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographIsNull() {
            addCriterion("Personage_Photograph is null");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographIsNotNull() {
            addCriterion("Personage_Photograph is not null");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographEqualTo(String value) {
            addCriterion("Personage_Photograph =", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographNotEqualTo(String value) {
            addCriterion("Personage_Photograph <>", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographGreaterThan(String value) {
            addCriterion("Personage_Photograph >", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Photograph >=", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographLessThan(String value) {
            addCriterion("Personage_Photograph <", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographLessThanOrEqualTo(String value) {
            addCriterion("Personage_Photograph <=", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographLike(String value) {
            addCriterion("Personage_Photograph like", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographNotLike(String value) {
            addCriterion("Personage_Photograph not like", value, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographIn(List<String> values) {
            addCriterion("Personage_Photograph in", values, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographNotIn(List<String> values) {
            addCriterion("Personage_Photograph not in", values, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographBetween(String value1, String value2) {
            addCriterion("Personage_Photograph between", value1, value2, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonagePhotographNotBetween(String value1, String value2) {
            addCriterion("Personage_Photograph not between", value1, value2, "personagePhotograph");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescIsNull() {
            addCriterion("Personage_CN_DESC is null");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescIsNotNull() {
            addCriterion("Personage_CN_DESC is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescEqualTo(String value) {
            addCriterion("Personage_CN_DESC =", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescNotEqualTo(String value) {
            addCriterion("Personage_CN_DESC <>", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescGreaterThan(String value) {
            addCriterion("Personage_CN_DESC >", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_CN_DESC >=", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescLessThan(String value) {
            addCriterion("Personage_CN_DESC <", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescLessThanOrEqualTo(String value) {
            addCriterion("Personage_CN_DESC <=", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescLike(String value) {
            addCriterion("Personage_CN_DESC like", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescNotLike(String value) {
            addCriterion("Personage_CN_DESC not like", value, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescIn(List<String> values) {
            addCriterion("Personage_CN_DESC in", values, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescNotIn(List<String> values) {
            addCriterion("Personage_CN_DESC not in", values, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescBetween(String value1, String value2) {
            addCriterion("Personage_CN_DESC between", value1, value2, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageCnDescNotBetween(String value1, String value2) {
            addCriterion("Personage_CN_DESC not between", value1, value2, "personageCnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescIsNull() {
            addCriterion("Personage_EN_DESC is null");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescIsNotNull() {
            addCriterion("Personage_EN_DESC is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescEqualTo(String value) {
            addCriterion("Personage_EN_DESC =", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescNotEqualTo(String value) {
            addCriterion("Personage_EN_DESC <>", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescGreaterThan(String value) {
            addCriterion("Personage_EN_DESC >", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_EN_DESC >=", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescLessThan(String value) {
            addCriterion("Personage_EN_DESC <", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescLessThanOrEqualTo(String value) {
            addCriterion("Personage_EN_DESC <=", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescLike(String value) {
            addCriterion("Personage_EN_DESC like", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescNotLike(String value) {
            addCriterion("Personage_EN_DESC not like", value, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescIn(List<String> values) {
            addCriterion("Personage_EN_DESC in", values, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescNotIn(List<String> values) {
            addCriterion("Personage_EN_DESC not in", values, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescBetween(String value1, String value2) {
            addCriterion("Personage_EN_DESC between", value1, value2, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageEnDescNotBetween(String value1, String value2) {
            addCriterion("Personage_EN_DESC not between", value1, value2, "personageEnDesc");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusIsNull() {
            addCriterion("Personage_Status is null");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusIsNotNull() {
            addCriterion("Personage_Status is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusEqualTo(Long value) {
            addCriterion("Personage_Status =", value, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusNotEqualTo(Long value) {
            addCriterion("Personage_Status <>", value, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusGreaterThan(Long value) {
            addCriterion("Personage_Status >", value, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusGreaterThanOrEqualTo(Long value) {
            addCriterion("Personage_Status >=", value, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusLessThan(Long value) {
            addCriterion("Personage_Status <", value, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusLessThanOrEqualTo(Long value) {
            addCriterion("Personage_Status <=", value, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusIn(List<Long> values) {
            addCriterion("Personage_Status in", values, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusNotIn(List<Long> values) {
            addCriterion("Personage_Status not in", values, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusBetween(Long value1, Long value2) {
            addCriterion("Personage_Status between", value1, value2, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andPersonageStatusNotBetween(Long value1, Long value2) {
            addCriterion("Personage_Status not between", value1, value2, "personageStatus");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("Content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("Content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("Content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("Content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("Content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("Content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("Content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("Content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("Content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("Content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("Content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("Content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("Content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("Content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("Update_Time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("Update_Time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("Update_Time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("Update_Time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("Update_Time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Update_Time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("Update_Time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Update_Time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("Update_Time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("Update_Time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("Update_Time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Update_Time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNull() {
            addCriterion("Behavior is null");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNotNull() {
            addCriterion("Behavior is not null");
            return (Criteria) this;
        }

        public Criteria andBehaviorEqualTo(String value) {
            addCriterion("Behavior =", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotEqualTo(String value) {
            addCriterion("Behavior <>", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThan(String value) {
            addCriterion("Behavior >", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThanOrEqualTo(String value) {
            addCriterion("Behavior >=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThan(String value) {
            addCriterion("Behavior <", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThanOrEqualTo(String value) {
            addCriterion("Behavior <=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLike(String value) {
            addCriterion("Behavior like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotLike(String value) {
            addCriterion("Behavior not like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorIn(List<String> values) {
            addCriterion("Behavior in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotIn(List<String> values) {
            addCriterion("Behavior not in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorBetween(String value1, String value2) {
            addCriterion("Behavior between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotBetween(String value1, String value2) {
            addCriterion("Behavior not between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIsNull() {
            addCriterion("Verify_Time is null");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIsNotNull() {
            addCriterion("Verify_Time is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeEqualTo(Date value) {
            addCriterion("Verify_Time =", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotEqualTo(Date value) {
            addCriterion("Verify_Time <>", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeGreaterThan(Date value) {
            addCriterion("Verify_Time >", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Verify_Time >=", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeLessThan(Date value) {
            addCriterion("Verify_Time <", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("Verify_Time <=", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIn(List<Date> values) {
            addCriterion("Verify_Time in", values, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotIn(List<Date> values) {
            addCriterion("Verify_Time not in", values, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeBetween(Date value1, Date value2) {
            addCriterion("Verify_Time between", value1, value2, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("Verify_Time not between", value1, value2, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileIsNull() {
            addCriterion("Personage_IS_Mobile is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileIsNotNull() {
            addCriterion("Personage_IS_Mobile is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileEqualTo(Long value) {
            addCriterion("Personage_IS_Mobile =", value, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileNotEqualTo(Long value) {
            addCriterion("Personage_IS_Mobile <>", value, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileGreaterThan(Long value) {
            addCriterion("Personage_IS_Mobile >", value, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileGreaterThanOrEqualTo(Long value) {
            addCriterion("Personage_IS_Mobile >=", value, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileLessThan(Long value) {
            addCriterion("Personage_IS_Mobile <", value, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileLessThanOrEqualTo(Long value) {
            addCriterion("Personage_IS_Mobile <=", value, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileIn(List<Long> values) {
            addCriterion("Personage_IS_Mobile in", values, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileNotIn(List<Long> values) {
            addCriterion("Personage_IS_Mobile not in", values, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileBetween(Long value1, Long value2) {
            addCriterion("Personage_IS_Mobile between", value1, value2, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andPersonageIsMobileNotBetween(Long value1, Long value2) {
            addCriterion("Personage_IS_Mobile not between", value1, value2, "personageIsMobile");
            return (Criteria) this;
        }

        public Criteria andVerifyUserIsNull() {
            addCriterion("Verify_User is null");
            return (Criteria) this;
        }

        public Criteria andVerifyUserIsNotNull() {
            addCriterion("Verify_User is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyUserEqualTo(String value) {
            addCriterion("Verify_User =", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotEqualTo(String value) {
            addCriterion("Verify_User <>", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserGreaterThan(String value) {
            addCriterion("Verify_User >", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserGreaterThanOrEqualTo(String value) {
            addCriterion("Verify_User >=", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserLessThan(String value) {
            addCriterion("Verify_User <", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserLessThanOrEqualTo(String value) {
            addCriterion("Verify_User <=", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserLike(String value) {
            addCriterion("Verify_User like", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotLike(String value) {
            addCriterion("Verify_User not like", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserIn(List<String> values) {
            addCriterion("Verify_User in", values, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotIn(List<String> values) {
            addCriterion("Verify_User not in", values, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserBetween(String value1, String value2) {
            addCriterion("Verify_User between", value1, value2, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotBetween(String value1, String value2) {
            addCriterion("Verify_User not between", value1, value2, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdIsNull() {
            addCriterion("Organization_MT_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdIsNotNull() {
            addCriterion("Organization_MT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdEqualTo(Long value) {
            addCriterion("Organization_MT_ID =", value, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdNotEqualTo(Long value) {
            addCriterion("Organization_MT_ID <>", value, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdGreaterThan(Long value) {
            addCriterion("Organization_MT_ID >", value, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Organization_MT_ID >=", value, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdLessThan(Long value) {
            addCriterion("Organization_MT_ID <", value, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdLessThanOrEqualTo(Long value) {
            addCriterion("Organization_MT_ID <=", value, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdIn(List<Long> values) {
            addCriterion("Organization_MT_ID in", values, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdNotIn(List<Long> values) {
            addCriterion("Organization_MT_ID not in", values, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdBetween(Long value1, Long value2) {
            addCriterion("Organization_MT_ID between", value1, value2, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationMtIdNotBetween(Long value1, Long value2) {
            addCriterion("Organization_MT_ID not between", value1, value2, "organizationMtId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNull() {
            addCriterion("Organization_ID is null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIsNotNull() {
            addCriterion("Organization_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdEqualTo(Long value) {
            addCriterion("Organization_ID =", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotEqualTo(Long value) {
            addCriterion("Organization_ID <>", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThan(Long value) {
            addCriterion("Organization_ID >", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Organization_ID >=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThan(Long value) {
            addCriterion("Organization_ID <", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdLessThanOrEqualTo(Long value) {
            addCriterion("Organization_ID <=", value, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdIn(List<Long> values) {
            addCriterion("Organization_ID in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotIn(List<Long> values) {
            addCriterion("Organization_ID not in", values, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdBetween(Long value1, Long value2) {
            addCriterion("Organization_ID between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andOrganizationIdNotBetween(Long value1, Long value2) {
            addCriterion("Organization_ID not between", value1, value2, "organizationId");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameIsNull() {
            addCriterion("Position_Cn_Name is null");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameIsNotNull() {
            addCriterion("Position_Cn_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameEqualTo(String value) {
            addCriterion("Position_Cn_Name =", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotEqualTo(String value) {
            addCriterion("Position_Cn_Name <>", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameGreaterThan(String value) {
            addCriterion("Position_Cn_Name >", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Position_Cn_Name >=", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameLessThan(String value) {
            addCriterion("Position_Cn_Name <", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameLessThanOrEqualTo(String value) {
            addCriterion("Position_Cn_Name <=", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameLike(String value) {
            addCriterion("Position_Cn_Name like", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotLike(String value) {
            addCriterion("Position_Cn_Name not like", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameIn(List<String> values) {
            addCriterion("Position_Cn_Name in", values, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotIn(List<String> values) {
            addCriterion("Position_Cn_Name not in", values, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameBetween(String value1, String value2) {
            addCriterion("Position_Cn_Name between", value1, value2, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotBetween(String value1, String value2) {
            addCriterion("Position_Cn_Name not between", value1, value2, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameIsNull() {
            addCriterion("Position_En_Name is null");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameIsNotNull() {
            addCriterion("Position_En_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameEqualTo(String value) {
            addCriterion("Position_En_Name =", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotEqualTo(String value) {
            addCriterion("Position_En_Name <>", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameGreaterThan(String value) {
            addCriterion("Position_En_Name >", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Position_En_Name >=", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameLessThan(String value) {
            addCriterion("Position_En_Name <", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameLessThanOrEqualTo(String value) {
            addCriterion("Position_En_Name <=", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameLike(String value) {
            addCriterion("Position_En_Name like", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotLike(String value) {
            addCriterion("Position_En_Name not like", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameIn(List<String> values) {
            addCriterion("Position_En_Name in", values, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotIn(List<String> values) {
            addCriterion("Position_En_Name not in", values, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameBetween(String value1, String value2) {
            addCriterion("Position_En_Name between", value1, value2, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotBetween(String value1, String value2) {
            addCriterion("Position_En_Name not between", value1, value2, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNull() {
            addCriterion("Module_ID is null");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNotNull() {
            addCriterion("Module_ID is not null");
            return (Criteria) this;
        }

        public Criteria andModuleIdEqualTo(Long value) {
            addCriterion("Module_ID =", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotEqualTo(Long value) {
            addCriterion("Module_ID <>", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThan(Long value) {
            addCriterion("Module_ID >", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Module_ID >=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThan(Long value) {
            addCriterion("Module_ID <", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThanOrEqualTo(Long value) {
            addCriterion("Module_ID <=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdIn(List<Long> values) {
            addCriterion("Module_ID in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotIn(List<Long> values) {
            addCriterion("Module_ID not in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdBetween(Long value1, Long value2) {
            addCriterion("Module_ID between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotBetween(Long value1, Long value2) {
            addCriterion("Module_ID not between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("Role is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("Role is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("Role =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("Role <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("Role >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("Role >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("Role <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("Role <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("Role like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("Role not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("Role in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("Role not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("Role between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("Role not between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeIsNull() {
            addCriterion("Personage_Type is null");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeIsNotNull() {
            addCriterion("Personage_Type is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeEqualTo(String value) {
            addCriterion("Personage_Type =", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNotEqualTo(String value) {
            addCriterion("Personage_Type <>", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeGreaterThan(String value) {
            addCriterion("Personage_Type >", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Type >=", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeLessThan(String value) {
            addCriterion("Personage_Type <", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeLessThanOrEqualTo(String value) {
            addCriterion("Personage_Type <=", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeLike(String value) {
            addCriterion("Personage_Type like", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNotLike(String value) {
            addCriterion("Personage_Type not like", value, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeIn(List<String> values) {
            addCriterion("Personage_Type in", values, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNotIn(List<String> values) {
            addCriterion("Personage_Type not in", values, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeBetween(String value1, String value2) {
            addCriterion("Personage_Type between", value1, value2, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNotBetween(String value1, String value2) {
            addCriterion("Personage_Type not between", value1, value2, "personageType");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameIsNull() {
            addCriterion("Personage_Type_Name is null");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameIsNotNull() {
            addCriterion("Personage_Type_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameEqualTo(String value) {
            addCriterion("Personage_Type_Name =", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameNotEqualTo(String value) {
            addCriterion("Personage_Type_Name <>", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameGreaterThan(String value) {
            addCriterion("Personage_Type_Name >", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Type_Name >=", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameLessThan(String value) {
            addCriterion("Personage_Type_Name <", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameLessThanOrEqualTo(String value) {
            addCriterion("Personage_Type_Name <=", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameLike(String value) {
            addCriterion("Personage_Type_Name like", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameNotLike(String value) {
            addCriterion("Personage_Type_Name not like", value, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameIn(List<String> values) {
            addCriterion("Personage_Type_Name in", values, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameNotIn(List<String> values) {
            addCriterion("Personage_Type_Name not in", values, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameBetween(String value1, String value2) {
            addCriterion("Personage_Type_Name between", value1, value2, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageTypeNameNotBetween(String value1, String value2) {
            addCriterion("Personage_Type_Name not between", value1, value2, "personageTypeName");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateIsNull() {
            addCriterion("Personage_Birthdate is null");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateIsNotNull() {
            addCriterion("Personage_Birthdate is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateEqualTo(Date value) {
            addCriterionForJDBCDate("Personage_Birthdate =", value, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("Personage_Birthdate <>", value, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateGreaterThan(Date value) {
            addCriterionForJDBCDate("Personage_Birthdate >", value, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Personage_Birthdate >=", value, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateLessThan(Date value) {
            addCriterionForJDBCDate("Personage_Birthdate <", value, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Personage_Birthdate <=", value, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateIn(List<Date> values) {
            addCriterionForJDBCDate("Personage_Birthdate in", values, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("Personage_Birthdate not in", values, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Personage_Birthdate between", value1, value2, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageBirthdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Personage_Birthdate not between", value1, value2, "personageBirthdate");
            return (Criteria) this;
        }

        public Criteria andPersonageImIsNull() {
            addCriterion("Personage_IM is null");
            return (Criteria) this;
        }

        public Criteria andPersonageImIsNotNull() {
            addCriterion("Personage_IM is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageImEqualTo(String value) {
            addCriterion("Personage_IM =", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImNotEqualTo(String value) {
            addCriterion("Personage_IM <>", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImGreaterThan(String value) {
            addCriterion("Personage_IM >", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_IM >=", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImLessThan(String value) {
            addCriterion("Personage_IM <", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImLessThanOrEqualTo(String value) {
            addCriterion("Personage_IM <=", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImLike(String value) {
            addCriterion("Personage_IM like", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImNotLike(String value) {
            addCriterion("Personage_IM not like", value, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImIn(List<String> values) {
            addCriterion("Personage_IM in", values, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImNotIn(List<String> values) {
            addCriterion("Personage_IM not in", values, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImBetween(String value1, String value2) {
            addCriterion("Personage_IM between", value1, value2, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageImNotBetween(String value1, String value2) {
            addCriterion("Personage_IM not between", value1, value2, "personageIm");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryIsNull() {
            addCriterion("Personage_Area_Country is null");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryIsNotNull() {
            addCriterion("Personage_Area_Country is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryEqualTo(Integer value) {
            addCriterion("Personage_Area_Country =", value, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryNotEqualTo(Integer value) {
            addCriterion("Personage_Area_Country <>", value, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryGreaterThan(Integer value) {
            addCriterion("Personage_Area_Country >", value, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_Area_Country >=", value, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryLessThan(Integer value) {
            addCriterion("Personage_Area_Country <", value, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_Area_Country <=", value, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryIn(List<Integer> values) {
            addCriterion("Personage_Area_Country in", values, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryNotIn(List<Integer> values) {
            addCriterion("Personage_Area_Country not in", values, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryBetween(Integer value1, Integer value2) {
            addCriterion("Personage_Area_Country between", value1, value2, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCountryNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_Area_Country not between", value1, value2, "personageAreaCountry");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityIsNull() {
            addCriterion("Personage_Area_City is null");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityIsNotNull() {
            addCriterion("Personage_Area_City is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityEqualTo(Integer value) {
            addCriterion("Personage_Area_City =", value, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityNotEqualTo(Integer value) {
            addCriterion("Personage_Area_City <>", value, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityGreaterThan(Integer value) {
            addCriterion("Personage_Area_City >", value, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_Area_City >=", value, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityLessThan(Integer value) {
            addCriterion("Personage_Area_City <", value, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_Area_City <=", value, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityIn(List<Integer> values) {
            addCriterion("Personage_Area_City in", values, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityNotIn(List<Integer> values) {
            addCriterion("Personage_Area_City not in", values, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityBetween(Integer value1, Integer value2) {
            addCriterion("Personage_Area_City between", value1, value2, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaCityNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_Area_City not between", value1, value2, "personageAreaCity");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownIsNull() {
            addCriterion("Personage_Area_Town is null");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownIsNotNull() {
            addCriterion("Personage_Area_Town is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownEqualTo(Integer value) {
            addCriterion("Personage_Area_Town =", value, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownNotEqualTo(Integer value) {
            addCriterion("Personage_Area_Town <>", value, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownGreaterThan(Integer value) {
            addCriterion("Personage_Area_Town >", value, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_Area_Town >=", value, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownLessThan(Integer value) {
            addCriterion("Personage_Area_Town <", value, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_Area_Town <=", value, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownIn(List<Integer> values) {
            addCriterion("Personage_Area_Town in", values, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownNotIn(List<Integer> values) {
            addCriterion("Personage_Area_Town not in", values, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownBetween(Integer value1, Integer value2) {
            addCriterion("Personage_Area_Town between", value1, value2, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageAreaTownNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_Area_Town not between", value1, value2, "personageAreaTown");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryIsNull() {
            addCriterion("Personage_Glory is null");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryIsNotNull() {
            addCriterion("Personage_Glory is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryEqualTo(String value) {
            addCriterion("Personage_Glory =", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryNotEqualTo(String value) {
            addCriterion("Personage_Glory <>", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryGreaterThan(String value) {
            addCriterion("Personage_Glory >", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Glory >=", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryLessThan(String value) {
            addCriterion("Personage_Glory <", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryLessThanOrEqualTo(String value) {
            addCriterion("Personage_Glory <=", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryLike(String value) {
            addCriterion("Personage_Glory like", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryNotLike(String value) {
            addCriterion("Personage_Glory not like", value, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryIn(List<String> values) {
            addCriterion("Personage_Glory in", values, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryNotIn(List<String> values) {
            addCriterion("Personage_Glory not in", values, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryBetween(String value1, String value2) {
            addCriterion("Personage_Glory between", value1, value2, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageGloryNotBetween(String value1, String value2) {
            addCriterion("Personage_Glory not between", value1, value2, "personageGlory");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateIsNull() {
            addCriterion("Personage_Certificate is null");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateIsNotNull() {
            addCriterion("Personage_Certificate is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateEqualTo(String value) {
            addCriterion("Personage_Certificate =", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateNotEqualTo(String value) {
            addCriterion("Personage_Certificate <>", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateGreaterThan(String value) {
            addCriterion("Personage_Certificate >", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Certificate >=", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateLessThan(String value) {
            addCriterion("Personage_Certificate <", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateLessThanOrEqualTo(String value) {
            addCriterion("Personage_Certificate <=", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateLike(String value) {
            addCriterion("Personage_Certificate like", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateNotLike(String value) {
            addCriterion("Personage_Certificate not like", value, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateIn(List<String> values) {
            addCriterion("Personage_Certificate in", values, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateNotIn(List<String> values) {
            addCriterion("Personage_Certificate not in", values, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateBetween(String value1, String value2) {
            addCriterion("Personage_Certificate between", value1, value2, "personageCertificate");
            return (Criteria) this;
        }

        public Criteria andPersonageCertificateNotBetween(String value1, String value2) {
            addCriterion("Personage_Certificate not between", value1, value2, "personageCertificate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}