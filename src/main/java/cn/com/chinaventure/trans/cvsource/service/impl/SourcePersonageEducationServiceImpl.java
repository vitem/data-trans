package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageEducationService;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageEducation;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageEducationExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageEducationExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePersonageEducationMapper;


@Service("sourcePersonageEducationService")
public class SourcePersonageEducationServiceImpl implements SourcePersonageEducationService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePersonageEducationServiceImpl.class);

	@Resource
	private SourcePersonageEducationMapper sourcePersonageEducationMapper;


	@Override
	public void saveSourcePersonageEducation(SourcePersonageEducation sourcePersonageEducation) {
		if (null == sourcePersonageEducation) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageEducationMapper.insertSelective(sourcePersonageEducation);
	}

	@Override
	public void updateSourcePersonageEducation(SourcePersonageEducation sourcePersonageEducation) {
		if (null == sourcePersonageEducation) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageEducationMapper.updateByPrimaryKeySelective(sourcePersonageEducation);
	}

	@Override
	public SourcePersonageEducation getSourcePersonageEducationById(Integer id) {
		return sourcePersonageEducationMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePersonageEducation> getSourcePersonageEducationList(SourcePersonageEducation sourcePersonageEducation) {
		return this.getSourcePersonageEducationListByParam(sourcePersonageEducation, null);
	}

	@Override
	public List<SourcePersonageEducation> getSourcePersonageEducationListByParam(SourcePersonageEducation sourcePersonageEducation, String orderByStr) {
		SourcePersonageEducationExample example = new SourcePersonageEducationExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePersonageEducation> sourcePersonageEducationList = this.sourcePersonageEducationMapper.selectByExample(example);
		return sourcePersonageEducationList;
	}

	@Override
	public PageResult<SourcePersonageEducation> findSourcePersonageEducationPageByParam(SourcePersonageEducation sourcePersonageEducation, PageParam pageParam) {
		return this.findSourcePersonageEducationPageByParam(sourcePersonageEducation, pageParam, null);
	}

	@Override
	public PageResult<SourcePersonageEducation> findSourcePersonageEducationPageByParam(SourcePersonageEducation sourcePersonageEducation, PageParam pageParam, String orderByStr) {
		PageResult<SourcePersonageEducation> pageResult = new PageResult<SourcePersonageEducation>();

		SourcePersonageEducationExample example = new SourcePersonageEducationExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePersonageEducation> list = null;// sourcePersonageEducationMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
