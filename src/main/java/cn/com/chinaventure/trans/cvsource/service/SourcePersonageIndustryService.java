package cn.com.chinaventure.trans.cvsource.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageIndustry;

/**
 * @author 
 *
 */
public interface SourcePersonageIndustryService {
	
	/**
     * @api saveSourcePersonageIndustry 新增sourcePersonageIndustry
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  新增sourcePersonageIndustry记录
     * @apiDescription 全量插入sourcePersonageIndustry记录
     * @apiParam {SourcePersonageIndustry} sourcePersonageIndustry sourcePersonageIndustry对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePersonageIndustry(SourcePersonageIndustry sourcePersonageIndustry);
	
	/**
     * @api updateSourcePersonageIndustry 修改sourcePersonageIndustry
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  修改sourcePersonageIndustry记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePersonageIndustry} sourcePersonageIndustry sourcePersonageIndustry对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePersonageIndustry(SourcePersonageIndustry sourcePersonageIndustry);
	
	/**
     * @api getSourcePersonageIndustryById 根据sourcePersonageIndustryid查询详情
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  查询sourcePersonageIndustry详情
     * @apiDescription 根据主键id查询sourcePersonageIndustry详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePersonageIndustry sourcePersonageIndustry实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePersonageIndustry getSourcePersonageIndustryById(Integer id);
	
	/**
     * @api getSourcePersonageIndustryList 根据sourcePersonageIndustry条件查询列表
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePersonageIndustry} sourcePersonageIndustry  实体条件
     * @apiSuccess List<SourcePersonageIndustry> sourcePersonageIndustry实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageIndustry> getSourcePersonageIndustryList(SourcePersonageIndustry sourcePersonageIndustry);
	

	/**
     * @api getSourcePersonageIndustryListByParam 根据sourcePersonageIndustry条件查询列表（含排序）
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePersonageIndustry} sourcePersonageIndustry  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePersonageIndustry> sourcePersonageIndustry实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageIndustry> getSourcePersonageIndustryListByParam(SourcePersonageIndustry sourcePersonageIndustry, String orderByStr);
	
	
	/**
     * @api findSourcePersonageIndustryPageByParam 根据sourcePersonageIndustry条件查询列表（分页）
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePersonageIndustry} sourcePersonageIndustry  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePersonageIndustry> sourcePersonageIndustry实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageIndustry> findSourcePersonageIndustryPageByParam(SourcePersonageIndustry sourcePersonageIndustry, PageParam pageParam); 
	
	/**
     * @api findSourcePersonageIndustryPageByParam 根据sourcePersonageIndustry条件查询列表（分页，含排序）
     * @apiGroup sourcePersonageIndustry管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePersonageIndustry} sourcePersonageIndustry  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePersonageIndustry> sourcePersonageIndustry实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageIndustry> findSourcePersonageIndustryPageByParam(SourcePersonageIndustry sourcePersonageIndustry, PageParam pageParam, String orderByStr);
	
	

}

