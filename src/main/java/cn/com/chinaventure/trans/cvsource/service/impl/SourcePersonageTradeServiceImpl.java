package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageTradeService;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageTrade;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageTradeExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageTradeExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePersonageTradeMapper;


@Service("sourcePersonageTradeService")
public class SourcePersonageTradeServiceImpl implements SourcePersonageTradeService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePersonageTradeServiceImpl.class);

	@Resource
	private SourcePersonageTradeMapper sourcePersonageTradeMapper;


	@Override
	public void saveSourcePersonageTrade(SourcePersonageTrade sourcePersonageTrade) {
		if (null == sourcePersonageTrade) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageTradeMapper.insertSelective(sourcePersonageTrade);
	}

	@Override
	public void updateSourcePersonageTrade(SourcePersonageTrade sourcePersonageTrade) {
		if (null == sourcePersonageTrade) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageTradeMapper.updateByPrimaryKeySelective(sourcePersonageTrade);
	}

	@Override
	public SourcePersonageTrade getSourcePersonageTradeById(Long id) {
		return sourcePersonageTradeMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePersonageTrade> getSourcePersonageTradeList(SourcePersonageTrade sourcePersonageTrade) {
		return this.getSourcePersonageTradeListByParam(sourcePersonageTrade, null);
	}

	@Override
	public List<SourcePersonageTrade> getSourcePersonageTradeListByParam(SourcePersonageTrade sourcePersonageTrade, String orderByStr) {
		SourcePersonageTradeExample example = new SourcePersonageTradeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePersonageTrade> sourcePersonageTradeList = this.sourcePersonageTradeMapper.selectByExample(example);
		return sourcePersonageTradeList;
	}

	@Override
	public PageResult<SourcePersonageTrade> findSourcePersonageTradePageByParam(SourcePersonageTrade sourcePersonageTrade, PageParam pageParam) {
		return this.findSourcePersonageTradePageByParam(sourcePersonageTrade, pageParam, null);
	}

	@Override
	public PageResult<SourcePersonageTrade> findSourcePersonageTradePageByParam(SourcePersonageTrade sourcePersonageTrade, PageParam pageParam, String orderByStr) {
		PageResult<SourcePersonageTrade> pageResult = new PageResult<SourcePersonageTrade>();

		SourcePersonageTradeExample example = new SourcePersonageTradeExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePersonageTrade> list = null;// sourcePersonageTradeMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
