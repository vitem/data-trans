package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;

public class SourcePersonageIndustry implements Serializable {
    /**
     * 人物CV行业ID
     */
    private Integer personageindustryId;

    /**
     * CV一级行业ID
     */
    private Integer industryOneid;

    /**
     * CV二级行业ID
     */
    private Integer industryTwoid;

    /**
     * CV三级行业ID
     */
    private Integer industryThreeid;

    /**
     * 人物专家ID
     */
    private Integer personageId;

    private static final long serialVersionUID = 1L;

    public Integer getPersonageindustryId() {
        return personageindustryId;
    }

    public void setPersonageindustryId(Integer personageindustryId) {
        this.personageindustryId = personageindustryId;
    }

    public Integer getIndustryOneid() {
        return industryOneid;
    }

    public void setIndustryOneid(Integer industryOneid) {
        this.industryOneid = industryOneid;
    }

    public Integer getIndustryTwoid() {
        return industryTwoid;
    }

    public void setIndustryTwoid(Integer industryTwoid) {
        this.industryTwoid = industryTwoid;
    }

    public Integer getIndustryThreeid() {
        return industryThreeid;
    }

    public void setIndustryThreeid(Integer industryThreeid) {
        this.industryThreeid = industryThreeid;
    }

    public Integer getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Integer personageId) {
        this.personageId = personageId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", personageindustryId=").append(personageindustryId);
        sb.append(", industryOneid=").append(industryOneid);
        sb.append(", industryTwoid=").append(industryTwoid);
        sb.append(", industryThreeid=").append(industryThreeid);
        sb.append(", personageId=").append(personageId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}