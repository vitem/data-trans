package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageEducation;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageEducationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePersonageEducationMapper {
    int countByExample(SourcePersonageEducationExample example);

    int deleteByExample(SourcePersonageEducationExample example);

    int deleteByPrimaryKey(Integer educationId);

    int insert(SourcePersonageEducation record);

    int insertSelective(SourcePersonageEducation record);

    List<SourcePersonageEducation> selectByExample(SourcePersonageEducationExample example);

    SourcePersonageEducation selectByPrimaryKey(Integer educationId);

    int updateByExampleSelective(@Param("record") SourcePersonageEducation record, @Param("example") SourcePersonageEducationExample example);

    int updateByExample(@Param("record") SourcePersonageEducation record, @Param("example") SourcePersonageEducationExample example);

    int updateByPrimaryKeySelective(SourcePersonageEducation record);

    int updateByPrimaryKey(SourcePersonageEducation record);
}