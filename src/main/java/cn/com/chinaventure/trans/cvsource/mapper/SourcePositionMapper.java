package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePosition;
import cn.com.chinaventure.trans.cvsource.entity.SourcePositionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePositionMapper {
    int countByExample(SourcePositionExample example);

    int deleteByExample(SourcePositionExample example);

    int deleteByPrimaryKey(Long positionId);

    int insert(SourcePosition record);

    int insertSelective(SourcePosition record);

    List<SourcePosition> selectByExample(SourcePositionExample example);

    SourcePosition selectByPrimaryKey(Long positionId);

    int updateByExampleSelective(@Param("record") SourcePosition record, @Param("example") SourcePositionExample example);

    int updateByExample(@Param("record") SourcePosition record, @Param("example") SourcePositionExample example);

    int updateByPrimaryKeySelective(SourcePosition record);

    int updateByPrimaryKey(SourcePosition record);
}