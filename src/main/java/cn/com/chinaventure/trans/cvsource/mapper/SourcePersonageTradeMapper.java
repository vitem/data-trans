package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageTrade;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageTradeExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePersonageTradeMapper {
    int countByExample(SourcePersonageTradeExample example);

    int deleteByExample(SourcePersonageTradeExample example);

    int deleteByPrimaryKey(Long id);

    int insert(SourcePersonageTrade record);

    int insertSelective(SourcePersonageTrade record);

    List<SourcePersonageTrade> selectByExample(SourcePersonageTradeExample example);

    SourcePersonageTrade selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") SourcePersonageTrade record, @Param("example") SourcePersonageTradeExample example);

    int updateByExample(@Param("record") SourcePersonageTrade record, @Param("example") SourcePersonageTradeExample example);

    int updateByPrimaryKeySelective(SourcePersonageTrade record);

    int updateByPrimaryKey(SourcePersonageTrade record);
}