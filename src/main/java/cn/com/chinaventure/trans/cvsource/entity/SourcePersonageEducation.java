package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;
import java.util.Date;

public class SourcePersonageEducation implements Serializable {
    /**
     * 人物教育背景主键ID
     */
    private Integer educationId;

    /**
     * 人物专家ID
     */
    private Integer personageId;

    /**
     * 毕业院校ID：引用行业组织ID
     */
    private Integer graduateschoolId;

    /**
     * 学位ID
     */
    private Integer degreeId;

    /**
     * 在校起始时间
     */
    private Date startDate;

    /**
     * 在校截止时间
     */
    private Date endDate;

    private Date createTime;

    private Date updateTime;

    private String behavior;

    private Integer ordered;

    private static final long serialVersionUID = 1L;

    public Integer getEducationId() {
        return educationId;
    }

    public void setEducationId(Integer educationId) {
        this.educationId = educationId;
    }

    public Integer getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Integer personageId) {
        this.personageId = personageId;
    }

    public Integer getGraduateschoolId() {
        return graduateschoolId;
    }

    public void setGraduateschoolId(Integer graduateschoolId) {
        this.graduateschoolId = graduateschoolId;
    }

    public Integer getDegreeId() {
        return degreeId;
    }

    public void setDegreeId(Integer degreeId) {
        this.degreeId = degreeId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior == null ? null : behavior.trim();
    }

    public Integer getOrdered() {
        return ordered;
    }

    public void setOrdered(Integer ordered) {
        this.ordered = ordered;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", educationId=").append(educationId);
        sb.append(", personageId=").append(personageId);
        sb.append(", graduateschoolId=").append(graduateschoolId);
        sb.append(", degreeId=").append(degreeId);
        sb.append(", startDate=").append(startDate);
        sb.append(", endDate=").append(endDate);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", behavior=").append(behavior);
        sb.append(", ordered=").append(ordered);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}