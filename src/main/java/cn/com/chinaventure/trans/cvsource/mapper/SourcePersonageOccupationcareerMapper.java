package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageOccupationcareer;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageOccupationcareerExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePersonageOccupationcareerMapper {
    int countByExample(SourcePersonageOccupationcareerExample example);

    int deleteByExample(SourcePersonageOccupationcareerExample example);

    int deleteByPrimaryKey(Long occupationcareerId);

    int insert(SourcePersonageOccupationcareer record);

    int insertSelective(SourcePersonageOccupationcareer record);

    List<SourcePersonageOccupationcareer> selectByExample(SourcePersonageOccupationcareerExample example);

    SourcePersonageOccupationcareer selectByPrimaryKey(Long occupationcareerId);

    int updateByExampleSelective(@Param("record") SourcePersonageOccupationcareer record, @Param("example") SourcePersonageOccupationcareerExample example);

    int updateByExample(@Param("record") SourcePersonageOccupationcareer record, @Param("example") SourcePersonageOccupationcareerExample example);

    int updateByPrimaryKeySelective(SourcePersonageOccupationcareer record);

    int updateByPrimaryKey(SourcePersonageOccupationcareer record);
}