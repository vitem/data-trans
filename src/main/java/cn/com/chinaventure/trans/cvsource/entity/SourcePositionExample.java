package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SourcePositionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePositionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPositionIdIsNull() {
            addCriterion("Position_ID is null");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNotNull() {
            addCriterion("Position_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPositionIdEqualTo(Long value) {
            addCriterion("Position_ID =", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotEqualTo(Long value) {
            addCriterion("Position_ID <>", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThan(Long value) {
            addCriterion("Position_ID >", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Position_ID >=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThan(Long value) {
            addCriterion("Position_ID <", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThanOrEqualTo(Long value) {
            addCriterion("Position_ID <=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIn(List<Long> values) {
            addCriterion("Position_ID in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotIn(List<Long> values) {
            addCriterion("Position_ID not in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdBetween(Long value1, Long value2) {
            addCriterion("Position_ID between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotBetween(Long value1, Long value2) {
            addCriterion("Position_ID not between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameIsNull() {
            addCriterion("Position_CN_Name is null");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameIsNotNull() {
            addCriterion("Position_CN_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameEqualTo(String value) {
            addCriterion("Position_CN_Name =", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotEqualTo(String value) {
            addCriterion("Position_CN_Name <>", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameGreaterThan(String value) {
            addCriterion("Position_CN_Name >", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Position_CN_Name >=", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameLessThan(String value) {
            addCriterion("Position_CN_Name <", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameLessThanOrEqualTo(String value) {
            addCriterion("Position_CN_Name <=", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameLike(String value) {
            addCriterion("Position_CN_Name like", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotLike(String value) {
            addCriterion("Position_CN_Name not like", value, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameIn(List<String> values) {
            addCriterion("Position_CN_Name in", values, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotIn(List<String> values) {
            addCriterion("Position_CN_Name not in", values, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameBetween(String value1, String value2) {
            addCriterion("Position_CN_Name between", value1, value2, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionCnNameNotBetween(String value1, String value2) {
            addCriterion("Position_CN_Name not between", value1, value2, "positionCnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameIsNull() {
            addCriterion("Position_EN_Name is null");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameIsNotNull() {
            addCriterion("Position_EN_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameEqualTo(String value) {
            addCriterion("Position_EN_Name =", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotEqualTo(String value) {
            addCriterion("Position_EN_Name <>", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameGreaterThan(String value) {
            addCriterion("Position_EN_Name >", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Position_EN_Name >=", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameLessThan(String value) {
            addCriterion("Position_EN_Name <", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameLessThanOrEqualTo(String value) {
            addCriterion("Position_EN_Name <=", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameLike(String value) {
            addCriterion("Position_EN_Name like", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotLike(String value) {
            addCriterion("Position_EN_Name not like", value, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameIn(List<String> values) {
            addCriterion("Position_EN_Name in", values, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotIn(List<String> values) {
            addCriterion("Position_EN_Name not in", values, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameBetween(String value1, String value2) {
            addCriterion("Position_EN_Name between", value1, value2, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andPositionEnNameNotBetween(String value1, String value2) {
            addCriterion("Position_EN_Name not between", value1, value2, "positionEnName");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("Update_Time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("Update_Time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("Update_Time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("Update_Time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("Update_Time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Update_Time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("Update_Time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Update_Time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("Update_Time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("Update_Time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("Update_Time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Update_Time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNull() {
            addCriterion("Behavior is null");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNotNull() {
            addCriterion("Behavior is not null");
            return (Criteria) this;
        }

        public Criteria andBehaviorEqualTo(String value) {
            addCriterion("Behavior =", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotEqualTo(String value) {
            addCriterion("Behavior <>", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThan(String value) {
            addCriterion("Behavior >", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThanOrEqualTo(String value) {
            addCriterion("Behavior >=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThan(String value) {
            addCriterion("Behavior <", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThanOrEqualTo(String value) {
            addCriterion("Behavior <=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLike(String value) {
            addCriterion("Behavior like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotLike(String value) {
            addCriterion("Behavior not like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorIn(List<String> values) {
            addCriterion("Behavior in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotIn(List<String> values) {
            addCriterion("Behavior not in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorBetween(String value1, String value2) {
            addCriterion("Behavior between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotBetween(String value1, String value2) {
            addCriterion("Behavior not between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNull() {
            addCriterion("Ordered is null");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNotNull() {
            addCriterion("Ordered is not null");
            return (Criteria) this;
        }

        public Criteria andOrderedEqualTo(Long value) {
            addCriterion("Ordered =", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotEqualTo(Long value) {
            addCriterion("Ordered <>", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThan(Long value) {
            addCriterion("Ordered >", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThanOrEqualTo(Long value) {
            addCriterion("Ordered >=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThan(Long value) {
            addCriterion("Ordered <", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThanOrEqualTo(Long value) {
            addCriterion("Ordered <=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedIn(List<Long> values) {
            addCriterion("Ordered in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotIn(List<Long> values) {
            addCriterion("Ordered not in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedBetween(Long value1, Long value2) {
            addCriterion("Ordered between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotBetween(Long value1, Long value2) {
            addCriterion("Ordered not between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNull() {
            addCriterion("Module_ID is null");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNotNull() {
            addCriterion("Module_ID is not null");
            return (Criteria) this;
        }

        public Criteria andModuleIdEqualTo(Long value) {
            addCriterion("Module_ID =", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotEqualTo(Long value) {
            addCriterion("Module_ID <>", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThan(Long value) {
            addCriterion("Module_ID >", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Module_ID >=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThan(Long value) {
            addCriterion("Module_ID <", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThanOrEqualTo(Long value) {
            addCriterion("Module_ID <=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdIn(List<Long> values) {
            addCriterion("Module_ID in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotIn(List<Long> values) {
            addCriterion("Module_ID not in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdBetween(Long value1, Long value2) {
            addCriterion("Module_ID between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotBetween(Long value1, Long value2) {
            addCriterion("Module_ID not between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("Role is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("Role is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("Role =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("Role <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("Role >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("Role >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("Role <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("Role <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("Role like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("Role not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("Role in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("Role not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("Role between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("Role not between", value1, value2, "role");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}