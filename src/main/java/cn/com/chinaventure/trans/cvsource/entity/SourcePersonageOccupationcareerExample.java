package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SourcePersonageOccupationcareerExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePersonageOccupationcareerExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andOccupationcareerIdIsNull() {
            addCriterion("OccupationCareer_ID is null");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdIsNotNull() {
            addCriterion("OccupationCareer_ID is not null");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdEqualTo(Long value) {
            addCriterion("OccupationCareer_ID =", value, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdNotEqualTo(Long value) {
            addCriterion("OccupationCareer_ID <>", value, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdGreaterThan(Long value) {
            addCriterion("OccupationCareer_ID >", value, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdGreaterThanOrEqualTo(Long value) {
            addCriterion("OccupationCareer_ID >=", value, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdLessThan(Long value) {
            addCriterion("OccupationCareer_ID <", value, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdLessThanOrEqualTo(Long value) {
            addCriterion("OccupationCareer_ID <=", value, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdIn(List<Long> values) {
            addCriterion("OccupationCareer_ID in", values, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdNotIn(List<Long> values) {
            addCriterion("OccupationCareer_ID not in", values, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdBetween(Long value1, Long value2) {
            addCriterion("OccupationCareer_ID between", value1, value2, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andOccupationcareerIdNotBetween(Long value1, Long value2) {
            addCriterion("OccupationCareer_ID not between", value1, value2, "occupationcareerId");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("Name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("Name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("Name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("Name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("Name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("Name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("Name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("Name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("Name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("Name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("Name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("Name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("Name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("Name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNull() {
            addCriterion("Source_ID is null");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNotNull() {
            addCriterion("Source_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSourceIdEqualTo(Long value) {
            addCriterion("Source_ID =", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotEqualTo(Long value) {
            addCriterion("Source_ID <>", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThan(Long value) {
            addCriterion("Source_ID >", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Source_ID >=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThan(Long value) {
            addCriterion("Source_ID <", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThanOrEqualTo(Long value) {
            addCriterion("Source_ID <=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIn(List<Long> values) {
            addCriterion("Source_ID in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotIn(List<Long> values) {
            addCriterion("Source_ID not in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdBetween(Long value1, Long value2) {
            addCriterion("Source_ID between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotBetween(Long value1, Long value2) {
            addCriterion("Source_ID not between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNull() {
            addCriterion("Position_ID is null");
            return (Criteria) this;
        }

        public Criteria andPositionIdIsNotNull() {
            addCriterion("Position_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPositionIdEqualTo(String value) {
            addCriterion("Position_ID =", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotEqualTo(String value) {
            addCriterion("Position_ID <>", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThan(String value) {
            addCriterion("Position_ID >", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdGreaterThanOrEqualTo(String value) {
            addCriterion("Position_ID >=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThan(String value) {
            addCriterion("Position_ID <", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLessThanOrEqualTo(String value) {
            addCriterion("Position_ID <=", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdLike(String value) {
            addCriterion("Position_ID like", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotLike(String value) {
            addCriterion("Position_ID not like", value, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdIn(List<String> values) {
            addCriterion("Position_ID in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotIn(List<String> values) {
            addCriterion("Position_ID not in", values, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdBetween(String value1, String value2) {
            addCriterion("Position_ID between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionIdNotBetween(String value1, String value2) {
            addCriterion("Position_ID not between", value1, value2, "positionId");
            return (Criteria) this;
        }

        public Criteria andPositionNameIsNull() {
            addCriterion("Position_Name is null");
            return (Criteria) this;
        }

        public Criteria andPositionNameIsNotNull() {
            addCriterion("Position_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPositionNameEqualTo(String value) {
            addCriterion("Position_Name =", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotEqualTo(String value) {
            addCriterion("Position_Name <>", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameGreaterThan(String value) {
            addCriterion("Position_Name >", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameGreaterThanOrEqualTo(String value) {
            addCriterion("Position_Name >=", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameLessThan(String value) {
            addCriterion("Position_Name <", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameLessThanOrEqualTo(String value) {
            addCriterion("Position_Name <=", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameLike(String value) {
            addCriterion("Position_Name like", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotLike(String value) {
            addCriterion("Position_Name not like", value, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameIn(List<String> values) {
            addCriterion("Position_Name in", values, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotIn(List<String> values) {
            addCriterion("Position_Name not in", values, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameBetween(String value1, String value2) {
            addCriterion("Position_Name between", value1, value2, "positionName");
            return (Criteria) this;
        }

        public Criteria andPositionNameNotBetween(String value1, String value2) {
            addCriterion("Position_Name not between", value1, value2, "positionName");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateIsNull() {
            addCriterion("EnterdutyDate is null");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateIsNotNull() {
            addCriterion("EnterdutyDate is not null");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateEqualTo(Date value) {
            addCriterionForJDBCDate("EnterdutyDate =", value, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateNotEqualTo(Date value) {
            addCriterionForJDBCDate("EnterdutyDate <>", value, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateGreaterThan(Date value) {
            addCriterionForJDBCDate("EnterdutyDate >", value, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("EnterdutyDate >=", value, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateLessThan(Date value) {
            addCriterionForJDBCDate("EnterdutyDate <", value, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("EnterdutyDate <=", value, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateIn(List<Date> values) {
            addCriterionForJDBCDate("EnterdutyDate in", values, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateNotIn(List<Date> values) {
            addCriterionForJDBCDate("EnterdutyDate not in", values, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("EnterdutyDate between", value1, value2, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterdutydateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("EnterdutyDate not between", value1, value2, "enterdutydate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateIsNull() {
            addCriterion("LeaveofficeDate is null");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateIsNotNull() {
            addCriterion("LeaveofficeDate is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateEqualTo(Date value) {
            addCriterionForJDBCDate("LeaveofficeDate =", value, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateNotEqualTo(Date value) {
            addCriterionForJDBCDate("LeaveofficeDate <>", value, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateGreaterThan(Date value) {
            addCriterionForJDBCDate("LeaveofficeDate >", value, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LeaveofficeDate >=", value, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateLessThan(Date value) {
            addCriterionForJDBCDate("LeaveofficeDate <", value, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LeaveofficeDate <=", value, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateIn(List<Date> values) {
            addCriterionForJDBCDate("LeaveofficeDate in", values, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateNotIn(List<Date> values) {
            addCriterionForJDBCDate("LeaveofficeDate not in", values, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LeaveofficeDate between", value1, value2, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficedateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LeaveofficeDate not between", value1, value2, "leaveofficedate");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeIsNull() {
            addCriterion("Leaveoffice is null");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeIsNotNull() {
            addCriterion("Leaveoffice is not null");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeEqualTo(Long value) {
            addCriterion("Leaveoffice =", value, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeNotEqualTo(Long value) {
            addCriterion("Leaveoffice <>", value, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeGreaterThan(Long value) {
            addCriterion("Leaveoffice >", value, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeGreaterThanOrEqualTo(Long value) {
            addCriterion("Leaveoffice >=", value, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeLessThan(Long value) {
            addCriterion("Leaveoffice <", value, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeLessThanOrEqualTo(Long value) {
            addCriterion("Leaveoffice <=", value, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeIn(List<Long> values) {
            addCriterion("Leaveoffice in", values, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeNotIn(List<Long> values) {
            addCriterion("Leaveoffice not in", values, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeBetween(Long value1, Long value2) {
            addCriterion("Leaveoffice between", value1, value2, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andLeaveofficeNotBetween(Long value1, Long value2) {
            addCriterion("Leaveoffice not between", value1, value2, "leaveoffice");
            return (Criteria) this;
        }

        public Criteria andSourceTypeIsNull() {
            addCriterion("Source_Type is null");
            return (Criteria) this;
        }

        public Criteria andSourceTypeIsNotNull() {
            addCriterion("Source_Type is not null");
            return (Criteria) this;
        }

        public Criteria andSourceTypeEqualTo(Long value) {
            addCriterion("Source_Type =", value, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeNotEqualTo(Long value) {
            addCriterion("Source_Type <>", value, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeGreaterThan(Long value) {
            addCriterion("Source_Type >", value, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeGreaterThanOrEqualTo(Long value) {
            addCriterion("Source_Type >=", value, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeLessThan(Long value) {
            addCriterion("Source_Type <", value, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeLessThanOrEqualTo(Long value) {
            addCriterion("Source_Type <=", value, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeIn(List<Long> values) {
            addCriterion("Source_Type in", values, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeNotIn(List<Long> values) {
            addCriterion("Source_Type not in", values, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeBetween(Long value1, Long value2) {
            addCriterion("Source_Type between", value1, value2, "sourceType");
            return (Criteria) this;
        }

        public Criteria andSourceTypeNotBetween(Long value1, Long value2) {
            addCriterion("Source_Type not between", value1, value2, "sourceType");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("Personage_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("Personage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(Long value) {
            addCriterion("Personage_ID =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(Long value) {
            addCriterion("Personage_ID <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(Long value) {
            addCriterion("Personage_ID >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Personage_ID >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(Long value) {
            addCriterion("Personage_ID <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(Long value) {
            addCriterion("Personage_ID <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<Long> values) {
            addCriterion("Personage_ID in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<Long> values) {
            addCriterion("Personage_ID not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(Long value1, Long value2) {
            addCriterion("Personage_ID between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(Long value1, Long value2) {
            addCriterion("Personage_ID not between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageNameIsNull() {
            addCriterion("Personage_Name is null");
            return (Criteria) this;
        }

        public Criteria andPersonageNameIsNotNull() {
            addCriterion("Personage_Name is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageNameEqualTo(String value) {
            addCriterion("Personage_Name =", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameNotEqualTo(String value) {
            addCriterion("Personage_Name <>", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameGreaterThan(String value) {
            addCriterion("Personage_Name >", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_Name >=", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameLessThan(String value) {
            addCriterion("Personage_Name <", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameLessThanOrEqualTo(String value) {
            addCriterion("Personage_Name <=", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameLike(String value) {
            addCriterion("Personage_Name like", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameNotLike(String value) {
            addCriterion("Personage_Name not like", value, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameIn(List<String> values) {
            addCriterion("Personage_Name in", values, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameNotIn(List<String> values) {
            addCriterion("Personage_Name not in", values, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameBetween(String value1, String value2) {
            addCriterion("Personage_Name between", value1, value2, "personageName");
            return (Criteria) this;
        }

        public Criteria andPersonageNameNotBetween(String value1, String value2) {
            addCriterion("Personage_Name not between", value1, value2, "personageName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventIsNull() {
            addCriterion("Enterprise_Is_Event is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventIsNotNull() {
            addCriterion("Enterprise_Is_Event is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventEqualTo(Long value) {
            addCriterion("Enterprise_Is_Event =", value, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventNotEqualTo(Long value) {
            addCriterion("Enterprise_Is_Event <>", value, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventGreaterThan(Long value) {
            addCriterion("Enterprise_Is_Event >", value, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventGreaterThanOrEqualTo(Long value) {
            addCriterion("Enterprise_Is_Event >=", value, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventLessThan(Long value) {
            addCriterion("Enterprise_Is_Event <", value, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventLessThanOrEqualTo(Long value) {
            addCriterion("Enterprise_Is_Event <=", value, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventIn(List<Long> values) {
            addCriterion("Enterprise_Is_Event in", values, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventNotIn(List<Long> values) {
            addCriterion("Enterprise_Is_Event not in", values, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventBetween(Long value1, Long value2) {
            addCriterion("Enterprise_Is_Event between", value1, value2, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIsEventNotBetween(Long value1, Long value2) {
            addCriterion("Enterprise_Is_Event not between", value1, value2, "enterpriseIsEvent");
            return (Criteria) this;
        }

        public Criteria andEnameIsNull() {
            addCriterion("EName is null");
            return (Criteria) this;
        }

        public Criteria andEnameIsNotNull() {
            addCriterion("EName is not null");
            return (Criteria) this;
        }

        public Criteria andEnameEqualTo(String value) {
            addCriterion("EName =", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameNotEqualTo(String value) {
            addCriterion("EName <>", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameGreaterThan(String value) {
            addCriterion("EName >", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameGreaterThanOrEqualTo(String value) {
            addCriterion("EName >=", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameLessThan(String value) {
            addCriterion("EName <", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameLessThanOrEqualTo(String value) {
            addCriterion("EName <=", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameLike(String value) {
            addCriterion("EName like", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameNotLike(String value) {
            addCriterion("EName not like", value, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameIn(List<String> values) {
            addCriterion("EName in", values, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameNotIn(List<String> values) {
            addCriterion("EName not in", values, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameBetween(String value1, String value2) {
            addCriterion("EName between", value1, value2, "ename");
            return (Criteria) this;
        }

        public Criteria andEnameNotBetween(String value1, String value2) {
            addCriterion("EName not between", value1, value2, "ename");
            return (Criteria) this;
        }

        public Criteria andPositionEnameIsNull() {
            addCriterion("Position_EName is null");
            return (Criteria) this;
        }

        public Criteria andPositionEnameIsNotNull() {
            addCriterion("Position_EName is not null");
            return (Criteria) this;
        }

        public Criteria andPositionEnameEqualTo(String value) {
            addCriterion("Position_EName =", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameNotEqualTo(String value) {
            addCriterion("Position_EName <>", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameGreaterThan(String value) {
            addCriterion("Position_EName >", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameGreaterThanOrEqualTo(String value) {
            addCriterion("Position_EName >=", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameLessThan(String value) {
            addCriterion("Position_EName <", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameLessThanOrEqualTo(String value) {
            addCriterion("Position_EName <=", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameLike(String value) {
            addCriterion("Position_EName like", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameNotLike(String value) {
            addCriterion("Position_EName not like", value, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameIn(List<String> values) {
            addCriterion("Position_EName in", values, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameNotIn(List<String> values) {
            addCriterion("Position_EName not in", values, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameBetween(String value1, String value2) {
            addCriterion("Position_EName between", value1, value2, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPositionEnameNotBetween(String value1, String value2) {
            addCriterion("Position_EName not between", value1, value2, "positionEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameIsNull() {
            addCriterion("Personage_EName is null");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameIsNotNull() {
            addCriterion("Personage_EName is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameEqualTo(String value) {
            addCriterion("Personage_EName =", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameNotEqualTo(String value) {
            addCriterion("Personage_EName <>", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameGreaterThan(String value) {
            addCriterion("Personage_EName >", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameGreaterThanOrEqualTo(String value) {
            addCriterion("Personage_EName >=", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameLessThan(String value) {
            addCriterion("Personage_EName <", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameLessThanOrEqualTo(String value) {
            addCriterion("Personage_EName <=", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameLike(String value) {
            addCriterion("Personage_EName like", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameNotLike(String value) {
            addCriterion("Personage_EName not like", value, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameIn(List<String> values) {
            addCriterion("Personage_EName in", values, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameNotIn(List<String> values) {
            addCriterion("Personage_EName not in", values, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameBetween(String value1, String value2) {
            addCriterion("Personage_EName between", value1, value2, "personageEname");
            return (Criteria) this;
        }

        public Criteria andPersonageEnameNotBetween(String value1, String value2) {
            addCriterion("Personage_EName not between", value1, value2, "personageEname");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}