package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageOccupationcareerService;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageOccupationcareer;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageOccupationcareerExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageOccupationcareerExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePersonageOccupationcareerMapper;


@Service("sourcePersonageOccupationcareerService")
public class SourcePersonageOccupationcareerServiceImpl implements SourcePersonageOccupationcareerService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePersonageOccupationcareerServiceImpl.class);

	@Resource
	private SourcePersonageOccupationcareerMapper sourcePersonageOccupationcareerMapper;


	@Override
	public void saveSourcePersonageOccupationcareer(SourcePersonageOccupationcareer sourcePersonageOccupationcareer) {
		if (null == sourcePersonageOccupationcareer) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageOccupationcareerMapper.insertSelective(sourcePersonageOccupationcareer);
	}

	@Override
	public void updateSourcePersonageOccupationcareer(SourcePersonageOccupationcareer sourcePersonageOccupationcareer) {
		if (null == sourcePersonageOccupationcareer) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageOccupationcareerMapper.updateByPrimaryKeySelective(sourcePersonageOccupationcareer);
	}

	@Override
	public SourcePersonageOccupationcareer getSourcePersonageOccupationcareerById(Long id) {
		return sourcePersonageOccupationcareerMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePersonageOccupationcareer> getSourcePersonageOccupationcareerList(SourcePersonageOccupationcareer sourcePersonageOccupationcareer) {
		return this.getSourcePersonageOccupationcareerListByParam(sourcePersonageOccupationcareer, null);
	}

	@Override
	public List<SourcePersonageOccupationcareer> getSourcePersonageOccupationcareerListByParam(SourcePersonageOccupationcareer sourcePersonageOccupationcareer, String orderByStr) {
		SourcePersonageOccupationcareerExample example = new SourcePersonageOccupationcareerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePersonageOccupationcareer> sourcePersonageOccupationcareerList = this.sourcePersonageOccupationcareerMapper.selectByExample(example);
		return sourcePersonageOccupationcareerList;
	}

	@Override
	public PageResult<SourcePersonageOccupationcareer> findSourcePersonageOccupationcareerPageByParam(SourcePersonageOccupationcareer sourcePersonageOccupationcareer, PageParam pageParam) {
		return this.findSourcePersonageOccupationcareerPageByParam(sourcePersonageOccupationcareer, pageParam, null);
	}

	@Override
	public PageResult<SourcePersonageOccupationcareer> findSourcePersonageOccupationcareerPageByParam(SourcePersonageOccupationcareer sourcePersonageOccupationcareer, PageParam pageParam, String orderByStr) {
		PageResult<SourcePersonageOccupationcareer> pageResult = new PageResult<SourcePersonageOccupationcareer>();

		SourcePersonageOccupationcareerExample example = new SourcePersonageOccupationcareerExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePersonageOccupationcareer> list = null;// sourcePersonageOccupationcareerMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
