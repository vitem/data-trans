package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;
import java.util.Date;

public class SourcePosition implements Serializable {
    private Long positionId;

    private String positionCnName;

    private String positionEnName;

    private Date createTime;

    private Date updateTime;

    private String behavior;

    private Long ordered;

    private Long moduleId;

    private String role;

    private static final long serialVersionUID = 1L;

    public Long getPositionId() {
        return positionId;
    }

    public void setPositionId(Long positionId) {
        this.positionId = positionId;
    }

    public String getPositionCnName() {
        return positionCnName;
    }

    public void setPositionCnName(String positionCnName) {
        this.positionCnName = positionCnName == null ? null : positionCnName.trim();
    }

    public String getPositionEnName() {
        return positionEnName;
    }

    public void setPositionEnName(String positionEnName) {
        this.positionEnName = positionEnName == null ? null : positionEnName.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior == null ? null : behavior.trim();
    }

    public Long getOrdered() {
        return ordered;
    }

    public void setOrdered(Long ordered) {
        this.ordered = ordered;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", positionId=").append(positionId);
        sb.append(", positionCnName=").append(positionCnName);
        sb.append(", positionEnName=").append(positionEnName);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", behavior=").append(behavior);
        sb.append(", ordered=").append(ordered);
        sb.append(", moduleId=").append(moduleId);
        sb.append(", role=").append(role);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}