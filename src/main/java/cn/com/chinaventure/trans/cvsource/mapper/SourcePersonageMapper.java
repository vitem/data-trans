package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePersonage;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePersonageMapper {
    int countByExample(SourcePersonageExample example);

    int deleteByExample(SourcePersonageExample example);

    int deleteByPrimaryKey(Long personageId);

    int insert(SourcePersonage record);

    int insertSelective(SourcePersonage record);

    List<SourcePersonage> selectByExample(SourcePersonageExample example);

    SourcePersonage selectByPrimaryKey(Long personageId);

    int updateByExampleSelective(@Param("record") SourcePersonage record, @Param("example") SourcePersonageExample example);

    int updateByExample(@Param("record") SourcePersonage record, @Param("example") SourcePersonageExample example);

    int updateByPrimaryKeySelective(SourcePersonage record);

    int updateByPrimaryKey(SourcePersonage record);
}