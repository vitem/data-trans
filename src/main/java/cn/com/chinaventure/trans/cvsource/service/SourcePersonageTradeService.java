package cn.com.chinaventure.trans.cvsource.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageTrade;

import java.util.List;

/**
 * @author 
 *
 */
public interface SourcePersonageTradeService {
	
	/**
     * @api saveSourcePersonageTrade 新增sourcePersonageTrade
     * @apiGroup sourcePersonageTrade管理
     * @apiName  新增sourcePersonageTrade记录
     * @apiDescription 全量插入sourcePersonageTrade记录
     * @apiParam {SourcePersonageTrade} sourcePersonageTrade sourcePersonageTrade对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePersonageTrade(SourcePersonageTrade sourcePersonageTrade);
	
	/**
     * @api updateSourcePersonageTrade 修改sourcePersonageTrade
     * @apiGroup sourcePersonageTrade管理
     * @apiName  修改sourcePersonageTrade记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePersonageTrade} sourcePersonageTrade sourcePersonageTrade对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePersonageTrade(SourcePersonageTrade sourcePersonageTrade);
	
	/**
     * @api getSourcePersonageTradeById 根据sourcePersonageTradeid查询详情
     * @apiGroup sourcePersonageTrade管理
     * @apiName  查询sourcePersonageTrade详情
     * @apiDescription 根据主键id查询sourcePersonageTrade详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePersonageTrade sourcePersonageTrade实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePersonageTrade getSourcePersonageTradeById(Long id);
	
	/**
     * @api getSourcePersonageTradeList 根据sourcePersonageTrade条件查询列表
     * @apiGroup sourcePersonageTrade管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePersonageTrade} sourcePersonageTrade  实体条件
     * @apiSuccess List<SourcePersonageTrade> sourcePersonageTrade实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageTrade> getSourcePersonageTradeList(SourcePersonageTrade sourcePersonageTrade);
	

	/**
     * @api getSourcePersonageTradeListByParam 根据sourcePersonageTrade条件查询列表（含排序）
     * @apiGroup sourcePersonageTrade管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePersonageTrade} sourcePersonageTrade  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePersonageTrade> sourcePersonageTrade实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageTrade> getSourcePersonageTradeListByParam(SourcePersonageTrade sourcePersonageTrade, String orderByStr);
	
	
	/**
     * @api findSourcePersonageTradePageByParam 根据sourcePersonageTrade条件查询列表（分页）
     * @apiGroup sourcePersonageTrade管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePersonageTrade} sourcePersonageTrade  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePersonageTrade> sourcePersonageTrade实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageTrade> findSourcePersonageTradePageByParam(SourcePersonageTrade sourcePersonageTrade, PageParam pageParam); 
	
	/**
     * @api findSourcePersonageTradePageByParam 根据sourcePersonageTrade条件查询列表（分页，含排序）
     * @apiGroup sourcePersonageTrade管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePersonageTrade} sourcePersonageTrade  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePersonageTrade> sourcePersonageTrade实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageTrade> findSourcePersonageTradePageByParam(SourcePersonageTrade sourcePersonageTrade, PageParam pageParam, String orderByStr);
	
	

}

