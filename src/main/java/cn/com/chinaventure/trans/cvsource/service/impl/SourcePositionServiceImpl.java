package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvsource.entity.SourcePosition;
import cn.com.chinaventure.trans.cvsource.entity.SourcePositionExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePositionExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePositionMapper;
import cn.com.chinaventure.trans.cvsource.service.SourcePositionService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("sourcePositionService")
public class SourcePositionServiceImpl implements SourcePositionService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePositionServiceImpl.class);

	@Resource
	private SourcePositionMapper sourcePositionMapper;


	@Override
	public void saveSourcePosition(SourcePosition sourcePosition) {
		if (null == sourcePosition) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePositionMapper.insertSelective(sourcePosition);
	}

	@Override
	public void updateSourcePosition(SourcePosition sourcePosition) {
		if (null == sourcePosition) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePositionMapper.updateByPrimaryKeySelective(sourcePosition);
	}

	@Override
	public SourcePosition getSourcePositionById(Long id) {
		return sourcePositionMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePosition> getSourcePositionList(SourcePosition sourcePosition) {
		return this.getSourcePositionListByParam(sourcePosition, null);
	}

	@Override
	public List<SourcePosition> getSourcePositionListByParam(SourcePosition sourcePosition, String orderByStr) {
		SourcePositionExample example = new SourcePositionExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePosition> sourcePositionList = this.sourcePositionMapper.selectByExample(example);
		return sourcePositionList;
	}

	@Override
	public PageResult<SourcePosition> findSourcePositionPageByParam(SourcePosition sourcePosition, PageParam pageParam) {
		return this.findSourcePositionPageByParam(sourcePosition, pageParam, null);
	}

	@Override
	public PageResult<SourcePosition> findSourcePositionPageByParam(SourcePosition sourcePosition, PageParam pageParam, String orderByStr) {
		PageResult<SourcePosition> pageResult = new PageResult<SourcePosition>();

		SourcePositionExample example = new SourcePositionExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePosition> list = null;// sourcePositionMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
