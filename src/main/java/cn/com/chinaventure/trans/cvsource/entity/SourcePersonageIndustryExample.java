package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.List;

public class SourcePersonageIndustryExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePersonageIndustryExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPersonageindustryIdIsNull() {
            addCriterion("Personageindustry_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdIsNotNull() {
            addCriterion("Personageindustry_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdEqualTo(Integer value) {
            addCriterion("Personageindustry_ID =", value, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdNotEqualTo(Integer value) {
            addCriterion("Personageindustry_ID <>", value, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdGreaterThan(Integer value) {
            addCriterion("Personageindustry_ID >", value, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personageindustry_ID >=", value, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdLessThan(Integer value) {
            addCriterion("Personageindustry_ID <", value, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdLessThanOrEqualTo(Integer value) {
            addCriterion("Personageindustry_ID <=", value, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdIn(List<Integer> values) {
            addCriterion("Personageindustry_ID in", values, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdNotIn(List<Integer> values) {
            addCriterion("Personageindustry_ID not in", values, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdBetween(Integer value1, Integer value2) {
            addCriterion("Personageindustry_ID between", value1, value2, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andPersonageindustryIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Personageindustry_ID not between", value1, value2, "personageindustryId");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidIsNull() {
            addCriterion("Industry_OneID is null");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidIsNotNull() {
            addCriterion("Industry_OneID is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidEqualTo(Integer value) {
            addCriterion("Industry_OneID =", value, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidNotEqualTo(Integer value) {
            addCriterion("Industry_OneID <>", value, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidGreaterThan(Integer value) {
            addCriterion("Industry_OneID >", value, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidGreaterThanOrEqualTo(Integer value) {
            addCriterion("Industry_OneID >=", value, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidLessThan(Integer value) {
            addCriterion("Industry_OneID <", value, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidLessThanOrEqualTo(Integer value) {
            addCriterion("Industry_OneID <=", value, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidIn(List<Integer> values) {
            addCriterion("Industry_OneID in", values, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidNotIn(List<Integer> values) {
            addCriterion("Industry_OneID not in", values, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidBetween(Integer value1, Integer value2) {
            addCriterion("Industry_OneID between", value1, value2, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryOneidNotBetween(Integer value1, Integer value2) {
            addCriterion("Industry_OneID not between", value1, value2, "industryOneid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidIsNull() {
            addCriterion("Industry_TwoID is null");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidIsNotNull() {
            addCriterion("Industry_TwoID is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidEqualTo(Integer value) {
            addCriterion("Industry_TwoID =", value, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidNotEqualTo(Integer value) {
            addCriterion("Industry_TwoID <>", value, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidGreaterThan(Integer value) {
            addCriterion("Industry_TwoID >", value, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidGreaterThanOrEqualTo(Integer value) {
            addCriterion("Industry_TwoID >=", value, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidLessThan(Integer value) {
            addCriterion("Industry_TwoID <", value, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidLessThanOrEqualTo(Integer value) {
            addCriterion("Industry_TwoID <=", value, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidIn(List<Integer> values) {
            addCriterion("Industry_TwoID in", values, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidNotIn(List<Integer> values) {
            addCriterion("Industry_TwoID not in", values, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidBetween(Integer value1, Integer value2) {
            addCriterion("Industry_TwoID between", value1, value2, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryTwoidNotBetween(Integer value1, Integer value2) {
            addCriterion("Industry_TwoID not between", value1, value2, "industryTwoid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidIsNull() {
            addCriterion("Industry_ThreeID is null");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidIsNotNull() {
            addCriterion("Industry_ThreeID is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidEqualTo(Integer value) {
            addCriterion("Industry_ThreeID =", value, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidNotEqualTo(Integer value) {
            addCriterion("Industry_ThreeID <>", value, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidGreaterThan(Integer value) {
            addCriterion("Industry_ThreeID >", value, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidGreaterThanOrEqualTo(Integer value) {
            addCriterion("Industry_ThreeID >=", value, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidLessThan(Integer value) {
            addCriterion("Industry_ThreeID <", value, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidLessThanOrEqualTo(Integer value) {
            addCriterion("Industry_ThreeID <=", value, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidIn(List<Integer> values) {
            addCriterion("Industry_ThreeID in", values, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidNotIn(List<Integer> values) {
            addCriterion("Industry_ThreeID not in", values, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidBetween(Integer value1, Integer value2) {
            addCriterion("Industry_ThreeID between", value1, value2, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andIndustryThreeidNotBetween(Integer value1, Integer value2) {
            addCriterion("Industry_ThreeID not between", value1, value2, "industryThreeid");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("Personage_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("Personage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(Integer value) {
            addCriterion("Personage_ID =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(Integer value) {
            addCriterion("Personage_ID <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(Integer value) {
            addCriterion("Personage_ID >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(Integer value) {
            addCriterion("Personage_ID <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<Integer> values) {
            addCriterion("Personage_ID in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<Integer> values) {
            addCriterion("Personage_ID not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID not between", value1, value2, "personageId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}