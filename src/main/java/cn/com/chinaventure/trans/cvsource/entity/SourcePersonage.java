package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;
import java.util.Date;

public class SourcePersonage implements Serializable {
    private Long personageId;

    private String personageCnName;

    private String personageEnName;

    private Long personageSex;

    private String personageTelephone;

    private String personageMobile;

    private String personageMail;

    private String personagePhotograph;

    private String personageCnDesc;

    private String personageEnDesc;

    private Long personageStatus;

    private String content;

    private Date createTime;

    private Date updateTime;

    private String behavior;

    private Date verifyTime;

    private Long personageIsMobile;

    private String verifyUser;

    private Long organizationMtId;

    private Long organizationId;

    private String positionCnName;

    private String positionEnName;

    private Long moduleId;

    private String role;

    /**
     * 人物类型：
     */
    private String personageType;

    /**
     * 投资专家/企业高管/分析师/学术专家/协会专家/政策智囊
     */
    private String personageTypeName;

    /**
     * 出生日期
     */
    private Date personageBirthdate;

    /**
     * IM
     */
    private String personageIm;

    /**
     * 地区-国家
     */
    private Integer personageAreaCountry;

    /**
     * 地区-省
     */
    private Integer personageAreaCity;

    /**
     * 地区-市
     */
    private Integer personageAreaTown;

    /**
     * 所获荣誉
     */
    private String personageGlory;

    /**
     * 执业证书编号（人物类型为“分析师”时特有）
     */
    private String personageCertificate;

    private static final long serialVersionUID = 1L;

    public Long getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Long personageId) {
        this.personageId = personageId;
    }

    public String getPersonageCnName() {
        return personageCnName;
    }

    public void setPersonageCnName(String personageCnName) {
        this.personageCnName = personageCnName == null ? null : personageCnName.trim();
    }

    public String getPersonageEnName() {
        return personageEnName;
    }

    public void setPersonageEnName(String personageEnName) {
        this.personageEnName = personageEnName == null ? null : personageEnName.trim();
    }

    public Long getPersonageSex() {
        return personageSex;
    }

    public void setPersonageSex(Long personageSex) {
        this.personageSex = personageSex;
    }

    public String getPersonageTelephone() {
        return personageTelephone;
    }

    public void setPersonageTelephone(String personageTelephone) {
        this.personageTelephone = personageTelephone == null ? null : personageTelephone.trim();
    }

    public String getPersonageMobile() {
        return personageMobile;
    }

    public void setPersonageMobile(String personageMobile) {
        this.personageMobile = personageMobile == null ? null : personageMobile.trim();
    }

    public String getPersonageMail() {
        return personageMail;
    }

    public void setPersonageMail(String personageMail) {
        this.personageMail = personageMail == null ? null : personageMail.trim();
    }

    public String getPersonagePhotograph() {
        return personagePhotograph;
    }

    public void setPersonagePhotograph(String personagePhotograph) {
        this.personagePhotograph = personagePhotograph == null ? null : personagePhotograph.trim();
    }

    public String getPersonageCnDesc() {
        return personageCnDesc;
    }

    public void setPersonageCnDesc(String personageCnDesc) {
        this.personageCnDesc = personageCnDesc == null ? null : personageCnDesc.trim();
    }

    public String getPersonageEnDesc() {
        return personageEnDesc;
    }

    public void setPersonageEnDesc(String personageEnDesc) {
        this.personageEnDesc = personageEnDesc == null ? null : personageEnDesc.trim();
    }

    public Long getPersonageStatus() {
        return personageStatus;
    }

    public void setPersonageStatus(Long personageStatus) {
        this.personageStatus = personageStatus;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior == null ? null : behavior.trim();
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public Long getPersonageIsMobile() {
        return personageIsMobile;
    }

    public void setPersonageIsMobile(Long personageIsMobile) {
        this.personageIsMobile = personageIsMobile;
    }

    public String getVerifyUser() {
        return verifyUser;
    }

    public void setVerifyUser(String verifyUser) {
        this.verifyUser = verifyUser == null ? null : verifyUser.trim();
    }

    public Long getOrganizationMtId() {
        return organizationMtId;
    }

    public void setOrganizationMtId(Long organizationMtId) {
        this.organizationMtId = organizationMtId;
    }

    public Long getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(Long organizationId) {
        this.organizationId = organizationId;
    }

    public String getPositionCnName() {
        return positionCnName;
    }

    public void setPositionCnName(String positionCnName) {
        this.positionCnName = positionCnName == null ? null : positionCnName.trim();
    }

    public String getPositionEnName() {
        return positionEnName;
    }

    public void setPositionEnName(String positionEnName) {
        this.positionEnName = positionEnName == null ? null : positionEnName.trim();
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public String getPersonageType() {
        return personageType;
    }

    public void setPersonageType(String personageType) {
        this.personageType = personageType == null ? null : personageType.trim();
    }

    public String getPersonageTypeName() {
        return personageTypeName;
    }

    public void setPersonageTypeName(String personageTypeName) {
        this.personageTypeName = personageTypeName == null ? null : personageTypeName.trim();
    }

    public Date getPersonageBirthdate() {
        return personageBirthdate;
    }

    public void setPersonageBirthdate(Date personageBirthdate) {
        this.personageBirthdate = personageBirthdate;
    }

    public String getPersonageIm() {
        return personageIm;
    }

    public void setPersonageIm(String personageIm) {
        this.personageIm = personageIm == null ? null : personageIm.trim();
    }

    public Integer getPersonageAreaCountry() {
        return personageAreaCountry;
    }

    public void setPersonageAreaCountry(Integer personageAreaCountry) {
        this.personageAreaCountry = personageAreaCountry;
    }

    public Integer getPersonageAreaCity() {
        return personageAreaCity;
    }

    public void setPersonageAreaCity(Integer personageAreaCity) {
        this.personageAreaCity = personageAreaCity;
    }

    public Integer getPersonageAreaTown() {
        return personageAreaTown;
    }

    public void setPersonageAreaTown(Integer personageAreaTown) {
        this.personageAreaTown = personageAreaTown;
    }

    public String getPersonageGlory() {
        return personageGlory;
    }

    public void setPersonageGlory(String personageGlory) {
        this.personageGlory = personageGlory == null ? null : personageGlory.trim();
    }

    public String getPersonageCertificate() {
        return personageCertificate;
    }

    public void setPersonageCertificate(String personageCertificate) {
        this.personageCertificate = personageCertificate == null ? null : personageCertificate.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", personageId=").append(personageId);
        sb.append(", personageCnName=").append(personageCnName);
        sb.append(", personageEnName=").append(personageEnName);
        sb.append(", personageSex=").append(personageSex);
        sb.append(", personageTelephone=").append(personageTelephone);
        sb.append(", personageMobile=").append(personageMobile);
        sb.append(", personageMail=").append(personageMail);
        sb.append(", personagePhotograph=").append(personagePhotograph);
        sb.append(", personageCnDesc=").append(personageCnDesc);
        sb.append(", personageEnDesc=").append(personageEnDesc);
        sb.append(", personageStatus=").append(personageStatus);
        sb.append(", content=").append(content);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", behavior=").append(behavior);
        sb.append(", verifyTime=").append(verifyTime);
        sb.append(", personageIsMobile=").append(personageIsMobile);
        sb.append(", verifyUser=").append(verifyUser);
        sb.append(", organizationMtId=").append(organizationMtId);
        sb.append(", organizationId=").append(organizationId);
        sb.append(", positionCnName=").append(positionCnName);
        sb.append(", positionEnName=").append(positionEnName);
        sb.append(", moduleId=").append(moduleId);
        sb.append(", role=").append(role);
        sb.append(", personageType=").append(personageType);
        sb.append(", personageTypeName=").append(personageTypeName);
        sb.append(", personageBirthdate=").append(personageBirthdate);
        sb.append(", personageIm=").append(personageIm);
        sb.append(", personageAreaCountry=").append(personageAreaCountry);
        sb.append(", personageAreaCity=").append(personageAreaCity);
        sb.append(", personageAreaTown=").append(personageAreaTown);
        sb.append(", personageGlory=").append(personageGlory);
        sb.append(", personageCertificate=").append(personageCertificate);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}