package cn.com.chinaventure.trans.cvsource.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageInternate;

import java.util.List;

/**
 * @author 
 *
 */
public interface SourcePersonageInternateService {
	
	/**
     * @api saveSourcePersonageInternate 新增sourcePersonageInternate
     * @apiGroup sourcePersonageInternate管理
     * @apiName  新增sourcePersonageInternate记录
     * @apiDescription 全量插入sourcePersonageInternate记录
     * @apiParam {SourcePersonageInternate} sourcePersonageInternate sourcePersonageInternate对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePersonageInternate(SourcePersonageInternate sourcePersonageInternate);
	
	/**
     * @api updateSourcePersonageInternate 修改sourcePersonageInternate
     * @apiGroup sourcePersonageInternate管理
     * @apiName  修改sourcePersonageInternate记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePersonageInternate} sourcePersonageInternate sourcePersonageInternate对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePersonageInternate(SourcePersonageInternate sourcePersonageInternate);
	
	/**
     * @api getSourcePersonageInternateById 根据sourcePersonageInternateid查询详情
     * @apiGroup sourcePersonageInternate管理
     * @apiName  查询sourcePersonageInternate详情
     * @apiDescription 根据主键id查询sourcePersonageInternate详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePersonageInternate sourcePersonageInternate实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePersonageInternate getSourcePersonageInternateById(Integer id);
	
	/**
     * @api getSourcePersonageInternateList 根据sourcePersonageInternate条件查询列表
     * @apiGroup sourcePersonageInternate管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePersonageInternate} sourcePersonageInternate  实体条件
     * @apiSuccess List<SourcePersonageInternate> sourcePersonageInternate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageInternate> getSourcePersonageInternateList(SourcePersonageInternate sourcePersonageInternate);
	

	/**
     * @api getSourcePersonageInternateListByParam 根据sourcePersonageInternate条件查询列表（含排序）
     * @apiGroup sourcePersonageInternate管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePersonageInternate} sourcePersonageInternate  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePersonageInternate> sourcePersonageInternate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageInternate> getSourcePersonageInternateListByParam(SourcePersonageInternate sourcePersonageInternate, String orderByStr);
	
	
	/**
     * @api findSourcePersonageInternatePageByParam 根据sourcePersonageInternate条件查询列表（分页）
     * @apiGroup sourcePersonageInternate管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePersonageInternate} sourcePersonageInternate  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePersonageInternate> sourcePersonageInternate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageInternate> findSourcePersonageInternatePageByParam(SourcePersonageInternate sourcePersonageInternate, PageParam pageParam); 
	
	/**
     * @api findSourcePersonageInternatePageByParam 根据sourcePersonageInternate条件查询列表（分页，含排序）
     * @apiGroup sourcePersonageInternate管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePersonageInternate} sourcePersonageInternate  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePersonageInternate> sourcePersonageInternate实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageInternate> findSourcePersonageInternatePageByParam(SourcePersonageInternate sourcePersonageInternate, PageParam pageParam, String orderByStr);
	
	

}

