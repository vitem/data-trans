package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageInternateService;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageInternate;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageInternateExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageInternateExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePersonageInternateMapper;


@Service("sourcePersonageInternateService")
public class SourcePersonageInternateServiceImpl implements SourcePersonageInternateService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePersonageInternateServiceImpl.class);

	@Resource
	private SourcePersonageInternateMapper sourcePersonageInternateMapper;


	@Override
	public void saveSourcePersonageInternate(SourcePersonageInternate sourcePersonageInternate) {
		if (null == sourcePersonageInternate) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageInternateMapper.insertSelective(sourcePersonageInternate);
	}

	@Override
	public void updateSourcePersonageInternate(SourcePersonageInternate sourcePersonageInternate) {
		if (null == sourcePersonageInternate) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageInternateMapper.updateByPrimaryKeySelective(sourcePersonageInternate);
	}

	@Override
	public SourcePersonageInternate getSourcePersonageInternateById(Integer id) {
		return sourcePersonageInternateMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePersonageInternate> getSourcePersonageInternateList(SourcePersonageInternate sourcePersonageInternate) {
		return this.getSourcePersonageInternateListByParam(sourcePersonageInternate, null);
	}

	@Override
	public List<SourcePersonageInternate> getSourcePersonageInternateListByParam(SourcePersonageInternate sourcePersonageInternate, String orderByStr) {
		SourcePersonageInternateExample example = new SourcePersonageInternateExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePersonageInternate> sourcePersonageInternateList = this.sourcePersonageInternateMapper.selectByExample(example);
		return sourcePersonageInternateList;
	}

	@Override
	public PageResult<SourcePersonageInternate> findSourcePersonageInternatePageByParam(SourcePersonageInternate sourcePersonageInternate, PageParam pageParam) {
		return this.findSourcePersonageInternatePageByParam(sourcePersonageInternate, pageParam, null);
	}

	@Override
	public PageResult<SourcePersonageInternate> findSourcePersonageInternatePageByParam(SourcePersonageInternate sourcePersonageInternate, PageParam pageParam, String orderByStr) {
		PageResult<SourcePersonageInternate> pageResult = new PageResult<SourcePersonageInternate>();

		SourcePersonageInternateExample example = new SourcePersonageInternateExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePersonageInternate> list = null;// sourcePersonageInternateMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
