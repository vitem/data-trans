package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;

public class SourcePersonageTrade implements Serializable {
    private Long id;

    private Long pid;

    private Byte categoryId;

    private String fullId;

    private Long tradeOne;

    private Long tradeTwo;

    private Long tradeThree;

    private Long tradeFour;

    private String fullCnName;

    private Byte levelId;

    private String cnName;

    private String enName;

    private Long domainId;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Byte getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Byte categoryId) {
        this.categoryId = categoryId;
    }

    public String getFullId() {
        return fullId;
    }

    public void setFullId(String fullId) {
        this.fullId = fullId == null ? null : fullId.trim();
    }

    public Long getTradeOne() {
        return tradeOne;
    }

    public void setTradeOne(Long tradeOne) {
        this.tradeOne = tradeOne;
    }

    public Long getTradeTwo() {
        return tradeTwo;
    }

    public void setTradeTwo(Long tradeTwo) {
        this.tradeTwo = tradeTwo;
    }

    public Long getTradeThree() {
        return tradeThree;
    }

    public void setTradeThree(Long tradeThree) {
        this.tradeThree = tradeThree;
    }

    public Long getTradeFour() {
        return tradeFour;
    }

    public void setTradeFour(Long tradeFour) {
        this.tradeFour = tradeFour;
    }

    public String getFullCnName() {
        return fullCnName;
    }

    public void setFullCnName(String fullCnName) {
        this.fullCnName = fullCnName == null ? null : fullCnName.trim();
    }

    public Byte getLevelId() {
        return levelId;
    }

    public void setLevelId(Byte levelId) {
        this.levelId = levelId;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName == null ? null : cnName.trim();
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName == null ? null : enName.trim();
    }

    public Long getDomainId() {
        return domainId;
    }

    public void setDomainId(Long domainId) {
        this.domainId = domainId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", pid=").append(pid);
        sb.append(", categoryId=").append(categoryId);
        sb.append(", fullId=").append(fullId);
        sb.append(", tradeOne=").append(tradeOne);
        sb.append(", tradeTwo=").append(tradeTwo);
        sb.append(", tradeThree=").append(tradeThree);
        sb.append(", tradeFour=").append(tradeFour);
        sb.append(", fullCnName=").append(fullCnName);
        sb.append(", levelId=").append(levelId);
        sb.append(", cnName=").append(cnName);
        sb.append(", enName=").append(enName);
        sb.append(", domainId=").append(domainId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}