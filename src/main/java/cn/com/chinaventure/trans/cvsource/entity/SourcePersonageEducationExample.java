package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class SourcePersonageEducationExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePersonageEducationExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andEducationIdIsNull() {
            addCriterion("Education_ID is null");
            return (Criteria) this;
        }

        public Criteria andEducationIdIsNotNull() {
            addCriterion("Education_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEducationIdEqualTo(Integer value) {
            addCriterion("Education_ID =", value, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdNotEqualTo(Integer value) {
            addCriterion("Education_ID <>", value, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdGreaterThan(Integer value) {
            addCriterion("Education_ID >", value, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Education_ID >=", value, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdLessThan(Integer value) {
            addCriterion("Education_ID <", value, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdLessThanOrEqualTo(Integer value) {
            addCriterion("Education_ID <=", value, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdIn(List<Integer> values) {
            addCriterion("Education_ID in", values, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdNotIn(List<Integer> values) {
            addCriterion("Education_ID not in", values, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdBetween(Integer value1, Integer value2) {
            addCriterion("Education_ID between", value1, value2, "educationId");
            return (Criteria) this;
        }

        public Criteria andEducationIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Education_ID not between", value1, value2, "educationId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("Personage_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("Personage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(Integer value) {
            addCriterion("Personage_ID =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(Integer value) {
            addCriterion("Personage_ID <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(Integer value) {
            addCriterion("Personage_ID >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(Integer value) {
            addCriterion("Personage_ID <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<Integer> values) {
            addCriterion("Personage_ID in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<Integer> values) {
            addCriterion("Personage_ID not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID not between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdIsNull() {
            addCriterion("Graduateschool_ID is null");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdIsNotNull() {
            addCriterion("Graduateschool_ID is not null");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdEqualTo(Integer value) {
            addCriterion("Graduateschool_ID =", value, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdNotEqualTo(Integer value) {
            addCriterion("Graduateschool_ID <>", value, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdGreaterThan(Integer value) {
            addCriterion("Graduateschool_ID >", value, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Graduateschool_ID >=", value, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdLessThan(Integer value) {
            addCriterion("Graduateschool_ID <", value, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdLessThanOrEqualTo(Integer value) {
            addCriterion("Graduateschool_ID <=", value, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdIn(List<Integer> values) {
            addCriterion("Graduateschool_ID in", values, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdNotIn(List<Integer> values) {
            addCriterion("Graduateschool_ID not in", values, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdBetween(Integer value1, Integer value2) {
            addCriterion("Graduateschool_ID between", value1, value2, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andGraduateschoolIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Graduateschool_ID not between", value1, value2, "graduateschoolId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdIsNull() {
            addCriterion("Degree_ID is null");
            return (Criteria) this;
        }

        public Criteria andDegreeIdIsNotNull() {
            addCriterion("Degree_ID is not null");
            return (Criteria) this;
        }

        public Criteria andDegreeIdEqualTo(Integer value) {
            addCriterion("Degree_ID =", value, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdNotEqualTo(Integer value) {
            addCriterion("Degree_ID <>", value, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdGreaterThan(Integer value) {
            addCriterion("Degree_ID >", value, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Degree_ID >=", value, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdLessThan(Integer value) {
            addCriterion("Degree_ID <", value, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdLessThanOrEqualTo(Integer value) {
            addCriterion("Degree_ID <=", value, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdIn(List<Integer> values) {
            addCriterion("Degree_ID in", values, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdNotIn(List<Integer> values) {
            addCriterion("Degree_ID not in", values, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdBetween(Integer value1, Integer value2) {
            addCriterion("Degree_ID between", value1, value2, "degreeId");
            return (Criteria) this;
        }

        public Criteria andDegreeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Degree_ID not between", value1, value2, "degreeId");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNull() {
            addCriterion("Start_Date is null");
            return (Criteria) this;
        }

        public Criteria andStartDateIsNotNull() {
            addCriterion("Start_Date is not null");
            return (Criteria) this;
        }

        public Criteria andStartDateEqualTo(Date value) {
            addCriterionForJDBCDate("Start_Date =", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("Start_Date <>", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThan(Date value) {
            addCriterionForJDBCDate("Start_Date >", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Start_Date >=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThan(Date value) {
            addCriterionForJDBCDate("Start_Date <", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Start_Date <=", value, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateIn(List<Date> values) {
            addCriterionForJDBCDate("Start_Date in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("Start_Date not in", values, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Start_Date between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andStartDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Start_Date not between", value1, value2, "startDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNull() {
            addCriterion("End_Date is null");
            return (Criteria) this;
        }

        public Criteria andEndDateIsNotNull() {
            addCriterion("End_Date is not null");
            return (Criteria) this;
        }

        public Criteria andEndDateEqualTo(Date value) {
            addCriterionForJDBCDate("End_Date =", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("End_Date <>", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThan(Date value) {
            addCriterionForJDBCDate("End_Date >", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("End_Date >=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThan(Date value) {
            addCriterionForJDBCDate("End_Date <", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("End_Date <=", value, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateIn(List<Date> values) {
            addCriterionForJDBCDate("End_Date in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("End_Date not in", values, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("End_Date between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andEndDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("End_Date not between", value1, value2, "endDate");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("Update_Time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("Update_Time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("Update_Time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("Update_Time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("Update_Time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Update_Time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("Update_Time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Update_Time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("Update_Time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("Update_Time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("Update_Time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Update_Time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNull() {
            addCriterion("Behavior is null");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNotNull() {
            addCriterion("Behavior is not null");
            return (Criteria) this;
        }

        public Criteria andBehaviorEqualTo(String value) {
            addCriterion("Behavior =", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotEqualTo(String value) {
            addCriterion("Behavior <>", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThan(String value) {
            addCriterion("Behavior >", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThanOrEqualTo(String value) {
            addCriterion("Behavior >=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThan(String value) {
            addCriterion("Behavior <", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThanOrEqualTo(String value) {
            addCriterion("Behavior <=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLike(String value) {
            addCriterion("Behavior like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotLike(String value) {
            addCriterion("Behavior not like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorIn(List<String> values) {
            addCriterion("Behavior in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotIn(List<String> values) {
            addCriterion("Behavior not in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorBetween(String value1, String value2) {
            addCriterion("Behavior between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotBetween(String value1, String value2) {
            addCriterion("Behavior not between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNull() {
            addCriterion("Ordered is null");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNotNull() {
            addCriterion("Ordered is not null");
            return (Criteria) this;
        }

        public Criteria andOrderedEqualTo(Integer value) {
            addCriterion("Ordered =", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotEqualTo(Integer value) {
            addCriterion("Ordered <>", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThan(Integer value) {
            addCriterion("Ordered >", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThanOrEqualTo(Integer value) {
            addCriterion("Ordered >=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThan(Integer value) {
            addCriterion("Ordered <", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThanOrEqualTo(Integer value) {
            addCriterion("Ordered <=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedIn(List<Integer> values) {
            addCriterion("Ordered in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotIn(List<Integer> values) {
            addCriterion("Ordered not in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedBetween(Integer value1, Integer value2) {
            addCriterion("Ordered between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotBetween(Integer value1, Integer value2) {
            addCriterion("Ordered not between", value1, value2, "ordered");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}