package cn.com.chinaventure.trans.cvsource.entity;

import java.util.ArrayList;
import java.util.List;

public class SourcePersonageInternateExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public SourcePersonageInternateExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPersonageinternateIdIsNull() {
            addCriterion("Personageinternate_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdIsNotNull() {
            addCriterion("Personageinternate_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdEqualTo(Integer value) {
            addCriterion("Personageinternate_ID =", value, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdNotEqualTo(Integer value) {
            addCriterion("Personageinternate_ID <>", value, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdGreaterThan(Integer value) {
            addCriterion("Personageinternate_ID >", value, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personageinternate_ID >=", value, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdLessThan(Integer value) {
            addCriterion("Personageinternate_ID <", value, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdLessThanOrEqualTo(Integer value) {
            addCriterion("Personageinternate_ID <=", value, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdIn(List<Integer> values) {
            addCriterion("Personageinternate_ID in", values, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdNotIn(List<Integer> values) {
            addCriterion("Personageinternate_ID not in", values, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdBetween(Integer value1, Integer value2) {
            addCriterion("Personageinternate_ID between", value1, value2, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andPersonageinternateIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Personageinternate_ID not between", value1, value2, "personageinternateId");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidIsNull() {
            addCriterion("EP_Industry_OneID is null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidIsNotNull() {
            addCriterion("EP_Industry_OneID is not null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidEqualTo(Byte value) {
            addCriterion("EP_Industry_OneID =", value, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidNotEqualTo(Byte value) {
            addCriterion("EP_Industry_OneID <>", value, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidGreaterThan(Byte value) {
            addCriterion("EP_Industry_OneID >", value, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidGreaterThanOrEqualTo(Byte value) {
            addCriterion("EP_Industry_OneID >=", value, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidLessThan(Byte value) {
            addCriterion("EP_Industry_OneID <", value, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidLessThanOrEqualTo(Byte value) {
            addCriterion("EP_Industry_OneID <=", value, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidIn(List<Byte> values) {
            addCriterion("EP_Industry_OneID in", values, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidNotIn(List<Byte> values) {
            addCriterion("EP_Industry_OneID not in", values, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidBetween(Byte value1, Byte value2) {
            addCriterion("EP_Industry_OneID between", value1, value2, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryOneidNotBetween(Byte value1, Byte value2) {
            addCriterion("EP_Industry_OneID not between", value1, value2, "epIndustryOneid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidIsNull() {
            addCriterion("EP_Industry_TwoID is null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidIsNotNull() {
            addCriterion("EP_Industry_TwoID is not null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidEqualTo(Integer value) {
            addCriterion("EP_Industry_TwoID =", value, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidNotEqualTo(Integer value) {
            addCriterion("EP_Industry_TwoID <>", value, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidGreaterThan(Integer value) {
            addCriterion("EP_Industry_TwoID >", value, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidGreaterThanOrEqualTo(Integer value) {
            addCriterion("EP_Industry_TwoID >=", value, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidLessThan(Integer value) {
            addCriterion("EP_Industry_TwoID <", value, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidLessThanOrEqualTo(Integer value) {
            addCriterion("EP_Industry_TwoID <=", value, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidIn(List<Integer> values) {
            addCriterion("EP_Industry_TwoID in", values, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidNotIn(List<Integer> values) {
            addCriterion("EP_Industry_TwoID not in", values, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidBetween(Integer value1, Integer value2) {
            addCriterion("EP_Industry_TwoID between", value1, value2, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTwoidNotBetween(Integer value1, Integer value2) {
            addCriterion("EP_Industry_TwoID not between", value1, value2, "epIndustryTwoid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidIsNull() {
            addCriterion("EP_Industry_ThreeID is null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidIsNotNull() {
            addCriterion("EP_Industry_ThreeID is not null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidEqualTo(Integer value) {
            addCriterion("EP_Industry_ThreeID =", value, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidNotEqualTo(Integer value) {
            addCriterion("EP_Industry_ThreeID <>", value, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidGreaterThan(Integer value) {
            addCriterion("EP_Industry_ThreeID >", value, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidGreaterThanOrEqualTo(Integer value) {
            addCriterion("EP_Industry_ThreeID >=", value, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidLessThan(Integer value) {
            addCriterion("EP_Industry_ThreeID <", value, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidLessThanOrEqualTo(Integer value) {
            addCriterion("EP_Industry_ThreeID <=", value, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidIn(List<Integer> values) {
            addCriterion("EP_Industry_ThreeID in", values, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidNotIn(List<Integer> values) {
            addCriterion("EP_Industry_ThreeID not in", values, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidBetween(Integer value1, Integer value2) {
            addCriterion("EP_Industry_ThreeID between", value1, value2, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryThreeidNotBetween(Integer value1, Integer value2) {
            addCriterion("EP_Industry_ThreeID not between", value1, value2, "epIndustryThreeid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridIsNull() {
            addCriterion("EP_Industry_FourID is null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridIsNotNull() {
            addCriterion("EP_Industry_FourID is not null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridEqualTo(Integer value) {
            addCriterion("EP_Industry_FourID =", value, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridNotEqualTo(Integer value) {
            addCriterion("EP_Industry_FourID <>", value, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridGreaterThan(Integer value) {
            addCriterion("EP_Industry_FourID >", value, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridGreaterThanOrEqualTo(Integer value) {
            addCriterion("EP_Industry_FourID >=", value, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridLessThan(Integer value) {
            addCriterion("EP_Industry_FourID <", value, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridLessThanOrEqualTo(Integer value) {
            addCriterion("EP_Industry_FourID <=", value, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridIn(List<Integer> values) {
            addCriterion("EP_Industry_FourID in", values, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridNotIn(List<Integer> values) {
            addCriterion("EP_Industry_FourID not in", values, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridBetween(Integer value1, Integer value2) {
            addCriterion("EP_Industry_FourID between", value1, value2, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryFouridNotBetween(Integer value1, Integer value2) {
            addCriterion("EP_Industry_FourID not between", value1, value2, "epIndustryFourid");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeIsNull() {
            addCriterion("EP_Industry_Type is null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeIsNotNull() {
            addCriterion("EP_Industry_Type is not null");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeEqualTo(Byte value) {
            addCriterion("EP_Industry_Type =", value, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeNotEqualTo(Byte value) {
            addCriterion("EP_Industry_Type <>", value, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeGreaterThan(Byte value) {
            addCriterion("EP_Industry_Type >", value, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("EP_Industry_Type >=", value, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeLessThan(Byte value) {
            addCriterion("EP_Industry_Type <", value, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeLessThanOrEqualTo(Byte value) {
            addCriterion("EP_Industry_Type <=", value, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeIn(List<Byte> values) {
            addCriterion("EP_Industry_Type in", values, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeNotIn(List<Byte> values) {
            addCriterion("EP_Industry_Type not in", values, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeBetween(Byte value1, Byte value2) {
            addCriterion("EP_Industry_Type between", value1, value2, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andEpIndustryTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("EP_Industry_Type not between", value1, value2, "epIndustryType");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("Personage_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("Personage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(Integer value) {
            addCriterion("Personage_ID =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(Integer value) {
            addCriterion("Personage_ID <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(Integer value) {
            addCriterion("Personage_ID >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(Integer value) {
            addCriterion("Personage_ID <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<Integer> values) {
            addCriterion("Personage_ID in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<Integer> values) {
            addCriterion("Personage_ID not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID not between", value1, value2, "personageId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}