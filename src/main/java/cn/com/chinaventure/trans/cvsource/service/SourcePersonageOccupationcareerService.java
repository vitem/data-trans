package cn.com.chinaventure.trans.cvsource.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageOccupationcareer;

import java.util.List;

/**
 * @author 
 *
 */
public interface SourcePersonageOccupationcareerService {
	
	/**
     * @api saveSourcePersonageOccupationcareer 新增sourcePersonageOccupationcareer
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  新增sourcePersonageOccupationcareer记录
     * @apiDescription 全量插入sourcePersonageOccupationcareer记录
     * @apiParam {SourcePersonageOccupationcareer} sourcePersonageOccupationcareer sourcePersonageOccupationcareer对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePersonageOccupationcareer(SourcePersonageOccupationcareer sourcePersonageOccupationcareer);
	
	/**
     * @api updateSourcePersonageOccupationcareer 修改sourcePersonageOccupationcareer
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  修改sourcePersonageOccupationcareer记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePersonageOccupationcareer} sourcePersonageOccupationcareer sourcePersonageOccupationcareer对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePersonageOccupationcareer(SourcePersonageOccupationcareer sourcePersonageOccupationcareer);
	
	/**
     * @api getSourcePersonageOccupationcareerById 根据sourcePersonageOccupationcareerid查询详情
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  查询sourcePersonageOccupationcareer详情
     * @apiDescription 根据主键id查询sourcePersonageOccupationcareer详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePersonageOccupationcareer sourcePersonageOccupationcareer实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePersonageOccupationcareer getSourcePersonageOccupationcareerById(Long id);
	
	/**
     * @api getSourcePersonageOccupationcareerList 根据sourcePersonageOccupationcareer条件查询列表
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePersonageOccupationcareer} sourcePersonageOccupationcareer  实体条件
     * @apiSuccess List<SourcePersonageOccupationcareer> sourcePersonageOccupationcareer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageOccupationcareer> getSourcePersonageOccupationcareerList(SourcePersonageOccupationcareer sourcePersonageOccupationcareer);
	

	/**
     * @api getSourcePersonageOccupationcareerListByParam 根据sourcePersonageOccupationcareer条件查询列表（含排序）
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePersonageOccupationcareer} sourcePersonageOccupationcareer  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePersonageOccupationcareer> sourcePersonageOccupationcareer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageOccupationcareer> getSourcePersonageOccupationcareerListByParam(SourcePersonageOccupationcareer sourcePersonageOccupationcareer, String orderByStr);
	
	
	/**
     * @api findSourcePersonageOccupationcareerPageByParam 根据sourcePersonageOccupationcareer条件查询列表（分页）
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePersonageOccupationcareer} sourcePersonageOccupationcareer  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePersonageOccupationcareer> sourcePersonageOccupationcareer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageOccupationcareer> findSourcePersonageOccupationcareerPageByParam(SourcePersonageOccupationcareer sourcePersonageOccupationcareer, PageParam pageParam); 
	
	/**
     * @api findSourcePersonageOccupationcareerPageByParam 根据sourcePersonageOccupationcareer条件查询列表（分页，含排序）
     * @apiGroup sourcePersonageOccupationcareer管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePersonageOccupationcareer} sourcePersonageOccupationcareer  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePersonageOccupationcareer> sourcePersonageOccupationcareer实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageOccupationcareer> findSourcePersonageOccupationcareerPageByParam(SourcePersonageOccupationcareer sourcePersonageOccupationcareer, PageParam pageParam, String orderByStr);
	
	

}

