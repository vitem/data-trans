package cn.com.chinaventure.trans.cvsource.entity;

import java.io.Serializable;
import java.util.Date;

public class SourcePersonageOccupationcareer implements Serializable {
    private Long occupationcareerId;

    private String name;

    private Long sourceId;

    private String positionId;

    private String positionName;

    private Date enterdutydate;

    private Date leaveofficedate;

    private Long leaveoffice;

    private Long sourceType;

    private Long personageId;

    private String personageName;

    private Long enterpriseIsEvent;

    private String ename;

    private String positionEname;

    private String personageEname;

    private static final long serialVersionUID = 1L;

    public Long getOccupationcareerId() {
        return occupationcareerId;
    }

    public void setOccupationcareerId(Long occupationcareerId) {
        this.occupationcareerId = occupationcareerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Long getSourceId() {
        return sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getPositionId() {
        return positionId;
    }

    public void setPositionId(String positionId) {
        this.positionId = positionId == null ? null : positionId.trim();
    }

    public String getPositionName() {
        return positionName;
    }

    public void setPositionName(String positionName) {
        this.positionName = positionName == null ? null : positionName.trim();
    }

    public Date getEnterdutydate() {
        return enterdutydate;
    }

    public void setEnterdutydate(Date enterdutydate) {
        this.enterdutydate = enterdutydate;
    }

    public Date getLeaveofficedate() {
        return leaveofficedate;
    }

    public void setLeaveofficedate(Date leaveofficedate) {
        this.leaveofficedate = leaveofficedate;
    }

    public Long getLeaveoffice() {
        return leaveoffice;
    }

    public void setLeaveoffice(Long leaveoffice) {
        this.leaveoffice = leaveoffice;
    }

    public Long getSourceType() {
        return sourceType;
    }

    public void setSourceType(Long sourceType) {
        this.sourceType = sourceType;
    }

    public Long getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Long personageId) {
        this.personageId = personageId;
    }

    public String getPersonageName() {
        return personageName;
    }

    public void setPersonageName(String personageName) {
        this.personageName = personageName == null ? null : personageName.trim();
    }

    public Long getEnterpriseIsEvent() {
        return enterpriseIsEvent;
    }

    public void setEnterpriseIsEvent(Long enterpriseIsEvent) {
        this.enterpriseIsEvent = enterpriseIsEvent;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename == null ? null : ename.trim();
    }

    public String getPositionEname() {
        return positionEname;
    }

    public void setPositionEname(String positionEname) {
        this.positionEname = positionEname == null ? null : positionEname.trim();
    }

    public String getPersonageEname() {
        return personageEname;
    }

    public void setPersonageEname(String personageEname) {
        this.personageEname = personageEname == null ? null : personageEname.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", occupationcareerId=").append(occupationcareerId);
        sb.append(", name=").append(name);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", positionId=").append(positionId);
        sb.append(", positionName=").append(positionName);
        sb.append(", enterdutydate=").append(enterdutydate);
        sb.append(", leaveofficedate=").append(leaveofficedate);
        sb.append(", leaveoffice=").append(leaveoffice);
        sb.append(", sourceType=").append(sourceType);
        sb.append(", personageId=").append(personageId);
        sb.append(", personageName=").append(personageName);
        sb.append(", enterpriseIsEvent=").append(enterpriseIsEvent);
        sb.append(", ename=").append(ename);
        sb.append(", positionEname=").append(positionEname);
        sb.append(", personageEname=").append(personageEname);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}