package cn.com.chinaventure.trans.cvsource.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvsource.service.SourcePersonageIndustryService;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageIndustry;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageIndustryExample;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageIndustryExample.Criteria;
import cn.com.chinaventure.trans.cvsource.mapper.SourcePersonageIndustryMapper;


@Service("sourcePersonageIndustryService")
public class SourcePersonageIndustryServiceImpl implements SourcePersonageIndustryService {

	private final static Logger logger = LoggerFactory.getLogger(SourcePersonageIndustryServiceImpl.class);

	@Resource
	private SourcePersonageIndustryMapper sourcePersonageIndustryMapper;


	@Override
	public void saveSourcePersonageIndustry(SourcePersonageIndustry sourcePersonageIndustry) {
		if (null == sourcePersonageIndustry) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageIndustryMapper.insertSelective(sourcePersonageIndustry);
	}

	@Override
	public void updateSourcePersonageIndustry(SourcePersonageIndustry sourcePersonageIndustry) {
		if (null == sourcePersonageIndustry) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.sourcePersonageIndustryMapper.updateByPrimaryKeySelective(sourcePersonageIndustry);
	}

	@Override
	public SourcePersonageIndustry getSourcePersonageIndustryById(Integer id) {
		return sourcePersonageIndustryMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SourcePersonageIndustry> getSourcePersonageIndustryList(SourcePersonageIndustry sourcePersonageIndustry) {
		return this.getSourcePersonageIndustryListByParam(sourcePersonageIndustry, null);
	}

	@Override
	public List<SourcePersonageIndustry> getSourcePersonageIndustryListByParam(SourcePersonageIndustry sourcePersonageIndustry, String orderByStr) {
		SourcePersonageIndustryExample example = new SourcePersonageIndustryExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<SourcePersonageIndustry> sourcePersonageIndustryList = this.sourcePersonageIndustryMapper.selectByExample(example);
		return sourcePersonageIndustryList;
	}

	@Override
	public PageResult<SourcePersonageIndustry> findSourcePersonageIndustryPageByParam(SourcePersonageIndustry sourcePersonageIndustry, PageParam pageParam) {
		return this.findSourcePersonageIndustryPageByParam(sourcePersonageIndustry, pageParam, null);
	}

	@Override
	public PageResult<SourcePersonageIndustry> findSourcePersonageIndustryPageByParam(SourcePersonageIndustry sourcePersonageIndustry, PageParam pageParam, String orderByStr) {
		PageResult<SourcePersonageIndustry> pageResult = new PageResult<SourcePersonageIndustry>();

		SourcePersonageIndustryExample example = new SourcePersonageIndustryExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<SourcePersonageIndustry> list = null;// sourcePersonageIndustryMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


}
