package cn.com.chinaventure.trans.cvsource.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageEducation;

/**
 * @author 
 *
 */
public interface SourcePersonageEducationService {
	
	/**
     * @api saveSourcePersonageEducation 新增sourcePersonageEducation
     * @apiGroup sourcePersonageEducation管理
     * @apiName  新增sourcePersonageEducation记录
     * @apiDescription 全量插入sourcePersonageEducation记录
     * @apiParam {SourcePersonageEducation} sourcePersonageEducation sourcePersonageEducation对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveSourcePersonageEducation(SourcePersonageEducation sourcePersonageEducation);
	
	/**
     * @api updateSourcePersonageEducation 修改sourcePersonageEducation
     * @apiGroup sourcePersonageEducation管理
     * @apiName  修改sourcePersonageEducation记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {SourcePersonageEducation} sourcePersonageEducation sourcePersonageEducation对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateSourcePersonageEducation(SourcePersonageEducation sourcePersonageEducation);
	
	/**
     * @api getSourcePersonageEducationById 根据sourcePersonageEducationid查询详情
     * @apiGroup sourcePersonageEducation管理
     * @apiName  查询sourcePersonageEducation详情
     * @apiDescription 根据主键id查询sourcePersonageEducation详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess SourcePersonageEducation sourcePersonageEducation实体对象
     * @apiVersion 1.0.0
     * 
     */
	public SourcePersonageEducation getSourcePersonageEducationById(Integer id);
	
	/**
     * @api getSourcePersonageEducationList 根据sourcePersonageEducation条件查询列表
     * @apiGroup sourcePersonageEducation管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {SourcePersonageEducation} sourcePersonageEducation  实体条件
     * @apiSuccess List<SourcePersonageEducation> sourcePersonageEducation实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageEducation> getSourcePersonageEducationList(SourcePersonageEducation sourcePersonageEducation);
	

	/**
     * @api getSourcePersonageEducationListByParam 根据sourcePersonageEducation条件查询列表（含排序）
     * @apiGroup sourcePersonageEducation管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {SourcePersonageEducation} sourcePersonageEducation  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<SourcePersonageEducation> sourcePersonageEducation实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<SourcePersonageEducation> getSourcePersonageEducationListByParam(SourcePersonageEducation sourcePersonageEducation, String orderByStr);
	
	
	/**
     * @api findSourcePersonageEducationPageByParam 根据sourcePersonageEducation条件查询列表（分页）
     * @apiGroup sourcePersonageEducation管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {SourcePersonageEducation} sourcePersonageEducation  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<SourcePersonageEducation> sourcePersonageEducation实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageEducation> findSourcePersonageEducationPageByParam(SourcePersonageEducation sourcePersonageEducation, PageParam pageParam); 
	
	/**
     * @api findSourcePersonageEducationPageByParam 根据sourcePersonageEducation条件查询列表（分页，含排序）
     * @apiGroup sourcePersonageEducation管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {SourcePersonageEducation} sourcePersonageEducation  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<SourcePersonageEducation> sourcePersonageEducation实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<SourcePersonageEducation> findSourcePersonageEducationPageByParam(SourcePersonageEducation sourcePersonageEducation, PageParam pageParam, String orderByStr);
	
	

}

