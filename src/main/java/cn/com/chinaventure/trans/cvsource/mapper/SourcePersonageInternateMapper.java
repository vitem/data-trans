package cn.com.chinaventure.trans.cvsource.mapper;

import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageInternate;
import cn.com.chinaventure.trans.cvsource.entity.SourcePersonageInternateExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface SourcePersonageInternateMapper {
    int countByExample(SourcePersonageInternateExample example);

    int deleteByExample(SourcePersonageInternateExample example);

    int deleteByPrimaryKey(Integer personageinternateId);

    int insert(SourcePersonageInternate record);

    int insertSelective(SourcePersonageInternate record);

    List<SourcePersonageInternate> selectByExample(SourcePersonageInternateExample example);

    SourcePersonageInternate selectByPrimaryKey(Integer personageinternateId);

    int updateByExampleSelective(@Param("record") SourcePersonageInternate record, @Param("example") SourcePersonageInternateExample example);

    int updateByExample(@Param("record") SourcePersonageInternate record, @Param("example") SourcePersonageInternateExample example);

    int updateByPrimaryKeySelective(SourcePersonageInternate record);

    int updateByPrimaryKey(SourcePersonageInternate record);
}