package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseAppinfo;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseAppinfoExample;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseAppinfoExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.EnterpriseAppinfoMapper;
import cn.com.chinaventure.trans.cvent.service.EnterpriseAppinfoService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("enterpriseAppinfoService")
public class EnterpriseAppinfoServiceImpl implements EnterpriseAppinfoService {

	private final static Logger logger = LoggerFactory.getLogger(EnterpriseAppinfoServiceImpl.class);

	@Resource
	private EnterpriseAppinfoMapper enterpriseAppinfoMapper;


	@Override
	public void saveEnterpriseAppinfo(EnterpriseAppinfo enterpriseAppinfo) {
		if (null == enterpriseAppinfo) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseAppinfoMapper.insertSelective(enterpriseAppinfo);
	}

	@Override
	public void updateEnterpriseAppinfo(EnterpriseAppinfo enterpriseAppinfo) {
		if (null == enterpriseAppinfo) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseAppinfoMapper.updateByPrimaryKeySelective(enterpriseAppinfo);
	}

	@Override
	public EnterpriseAppinfo getEnterpriseAppinfoById(Integer id) {
		return enterpriseAppinfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EnterpriseAppinfo> getEnterpriseAppinfoList(EnterpriseAppinfo enterpriseAppinfo) {
		return this.getEnterpriseAppinfoListByParam(enterpriseAppinfo, null);
	}

	@Override
	public List<EnterpriseAppinfo> getEnterpriseAppinfoListByParam(EnterpriseAppinfo enterpriseAppinfo, String orderByStr) {
		EnterpriseAppinfoExample example = new EnterpriseAppinfoExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EnterpriseAppinfo> enterpriseAppinfoList = this.enterpriseAppinfoMapper.selectByExample(example);
		return enterpriseAppinfoList;
	}

	@Override
	public PageResult<EnterpriseAppinfo> findEnterpriseAppinfoPageByParam(EnterpriseAppinfo enterpriseAppinfo, PageParam pageParam) {
		return this.findEnterpriseAppinfoPageByParam(enterpriseAppinfo, pageParam, null);
	}

	@Override
	public PageResult<EnterpriseAppinfo> findEnterpriseAppinfoPageByParam(EnterpriseAppinfo enterpriseAppinfo, PageParam pageParam, String orderByStr) {
		PageResult<EnterpriseAppinfo> pageResult = new PageResult<EnterpriseAppinfo>();

		EnterpriseAppinfoExample example = new EnterpriseAppinfoExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<EnterpriseAppinfo> list = null;// enterpriseAppinfoMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterpriseAppinfoById(Integer id) {
		this.enterpriseAppinfoMapper.deleteByPrimaryKey(id);
	}

}
