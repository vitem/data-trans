package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCV;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCVExample;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCVExample.Criteria;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCVWithBLOBs;
import cn.com.chinaventure.trans.cvent.mapper.EnterpriseCVMapper;
import cn.com.chinaventure.trans.cvent.service.EnterpriseCVService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("enterpriseCVService")
public class EnterpriseCVServiceImpl implements EnterpriseCVService {

	private final static Logger logger = LoggerFactory.getLogger(EnterpriseCVServiceImpl.class);

	@Resource
	private EnterpriseCVMapper enterpriseCVMapper;


	@Override
	public void saveEnterpriseCV(EnterpriseCV enterpriseCV) {
		if (null == enterpriseCV) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseCVMapper.insertSelective(enterpriseCV);
	}

	@Override
	public void updateEnterpriseCV(EnterpriseCVWithBLOBs enterpriseCV) {
		if (null == enterpriseCV) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterpriseCVMapper.updateByPrimaryKeySelective(enterpriseCV);
	}

	@Override
	public EnterpriseCVWithBLOBs getEnterpriseCVById(Integer id) {
		return enterpriseCVMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<EnterpriseCV> getEnterpriseCVList(EnterpriseCV enterpriseCV) {
		return this.getEnterpriseCVListByParam(enterpriseCV, null);
	}

	@Override
	public List<EnterpriseCV> getEnterpriseCVListByParam(EnterpriseCV enterpriseCV, String orderByStr) {
		EnterpriseCVExample example = new EnterpriseCVExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<EnterpriseCV> enterpriseCVList = this.enterpriseCVMapper.selectByExample(example);
		return enterpriseCVList;
	}

	@Override
	public PageResult<EnterpriseCV> findEnterpriseCVPageByParam(EnterpriseCV enterpriseCV, PageParam pageParam) {
		return this.findEnterpriseCVPageByParam(enterpriseCV, pageParam, null);
	}

	@Override
	public PageResult<EnterpriseCV> findEnterpriseCVPageByParam(EnterpriseCV enterpriseCV, PageParam pageParam, String orderByStr) {
		PageResult<EnterpriseCV> pageResult = new PageResult<EnterpriseCV>();

		EnterpriseCVExample example = new EnterpriseCVExample();
		if (StringUtil.isNotBlank(orderByStr)) {
        	example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		example.setPageParam(pageParam);
		List<EnterpriseCV> list = enterpriseCVMapper.selectByExampleWithRowbounds(example);
		pageResult = PageResult.toPage(list,pageParam);
		return pageResult;
	}

	@Override
	public void deleteEnterpriseCVById(Integer id) {
		this.enterpriseCVMapper.deleteByPrimaryKey(id);
	}
	@Override
	public Integer countEnterpriseCV(){
		return enterpriseCVMapper.countByExample(new EnterpriseCVExample());
	}

}
