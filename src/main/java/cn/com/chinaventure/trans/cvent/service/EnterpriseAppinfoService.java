package cn.com.chinaventure.trans.cvent.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseAppinfo;

/**
 * @author 
 *
 */
public interface EnterpriseAppinfoService {
	
	/**
     * @api saveEnterpriseAppinfo 新增enterpriseAppinfo
     * @apiGroup enterpriseAppinfo管理
     * @apiName  新增enterpriseAppinfo记录
     * @apiDescription 全量插入enterpriseAppinfo记录
     * @apiParam {EnterpriseAppinfo} enterpriseAppinfo enterpriseAppinfo对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterpriseAppinfo(EnterpriseAppinfo enterpriseAppinfo);
	
	/**
     * @api updateEnterpriseAppinfo 修改enterpriseAppinfo
     * @apiGroup enterpriseAppinfo管理
     * @apiName  修改enterpriseAppinfo记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EnterpriseAppinfo} enterpriseAppinfo enterpriseAppinfo对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterpriseAppinfo(EnterpriseAppinfo enterpriseAppinfo);
	
	/**
     * @api getEnterpriseAppinfoById 根据enterpriseAppinfoid查询详情
     * @apiGroup enterpriseAppinfo管理
     * @apiName  查询enterpriseAppinfo详情
     * @apiDescription 根据主键id查询enterpriseAppinfo详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess EnterpriseAppinfo enterpriseAppinfo实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EnterpriseAppinfo getEnterpriseAppinfoById(Integer id);
	
	/**
     * @api getEnterpriseAppinfoList 根据enterpriseAppinfo条件查询列表
     * @apiGroup enterpriseAppinfo管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EnterpriseAppinfo} enterpriseAppinfo  实体条件
     * @apiSuccess List<EnterpriseAppinfo> enterpriseAppinfo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EnterpriseAppinfo> getEnterpriseAppinfoList(EnterpriseAppinfo enterpriseAppinfo);
	

	/**
     * @api getEnterpriseAppinfoListByParam 根据enterpriseAppinfo条件查询列表（含排序）
     * @apiGroup enterpriseAppinfo管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EnterpriseAppinfo} enterpriseAppinfo  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EnterpriseAppinfo> enterpriseAppinfo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EnterpriseAppinfo> getEnterpriseAppinfoListByParam(EnterpriseAppinfo enterpriseAppinfo, String orderByStr);
	
	
	/**
     * @api findEnterpriseAppinfoPageByParam 根据enterpriseAppinfo条件查询列表（分页）
     * @apiGroup enterpriseAppinfo管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EnterpriseAppinfo} enterpriseAppinfo  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EnterpriseAppinfo> enterpriseAppinfo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EnterpriseAppinfo> findEnterpriseAppinfoPageByParam(EnterpriseAppinfo enterpriseAppinfo, PageParam pageParam); 
	
	/**
     * @api findEnterpriseAppinfoPageByParam 根据enterpriseAppinfo条件查询列表（分页，含排序）
     * @apiGroup enterpriseAppinfo管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EnterpriseAppinfo} enterpriseAppinfo  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EnterpriseAppinfo> enterpriseAppinfo实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EnterpriseAppinfo> findEnterpriseAppinfoPageByParam(EnterpriseAppinfo enterpriseAppinfo, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterpriseAppinfoById 根据enterpriseAppinfo的id删除(物理删除)
     * @apiGroup enterpriseAppinfo管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterpriseAppinfoById(Integer id);

}

