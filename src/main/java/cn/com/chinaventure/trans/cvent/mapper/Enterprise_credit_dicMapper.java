package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_dic;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_dicExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_credit_dicMapper {
    int countByExample(Enterprise_credit_dicExample example);

    int deleteByExample(Enterprise_credit_dicExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Enterprise_credit_dic record);

    int insertSelective(Enterprise_credit_dic record);

    List<Enterprise_credit_dic> selectByExample(Enterprise_credit_dicExample example);

    Enterprise_credit_dic selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Enterprise_credit_dic record, @Param("example") Enterprise_credit_dicExample example);

    int updateByExample(@Param("record") Enterprise_credit_dic record, @Param("example") Enterprise_credit_dicExample example);

    int updateByPrimaryKeySelective(Enterprise_credit_dic record);

    int updateByPrimaryKey(Enterprise_credit_dic record);
}