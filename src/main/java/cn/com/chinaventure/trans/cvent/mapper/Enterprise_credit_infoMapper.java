package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_info;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_infoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_credit_infoMapper {
    int countByExample(Enterprise_credit_infoExample example);

    int deleteByExample(Enterprise_credit_infoExample example);

    int deleteByPrimaryKey(Long creditId);

    int insert(Enterprise_credit_info record);

    int insertSelective(Enterprise_credit_info record);

    List<Enterprise_credit_info> selectByExampleWithBLOBs(Enterprise_credit_infoExample example);

    List<Enterprise_credit_info> selectByExample(Enterprise_credit_infoExample example);

    Enterprise_credit_info selectByPrimaryKey(Long creditId);

    int updateByExampleSelective(@Param("record") Enterprise_credit_info record, @Param("example") Enterprise_credit_infoExample example);

    int updateByExampleWithBLOBs(@Param("record") Enterprise_credit_info record, @Param("example") Enterprise_credit_infoExample example);

    int updateByExample(@Param("record") Enterprise_credit_info record, @Param("example") Enterprise_credit_infoExample example);

    int updateByPrimaryKeySelective(Enterprise_credit_info record);

    int updateByPrimaryKeyWithBLOBs(Enterprise_credit_info record);

    int updateByPrimaryKey(Enterprise_credit_info record);
}