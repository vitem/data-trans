package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_position;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_positionExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_positionMapper {
    int countByExample(Enterprise_positionExample example);

    int deleteByExample(Enterprise_positionExample example);

    int deleteByPrimaryKey(Integer enterprisePositionId);

    int insert(Enterprise_position record);

    int insertSelective(Enterprise_position record);

    List<Enterprise_position> selectByExample(Enterprise_positionExample example);

    Enterprise_position selectByPrimaryKey(Integer enterprisePositionId);

    int updateByExampleSelective(@Param("record") Enterprise_position record, @Param("example") Enterprise_positionExample example);

    int updateByExample(@Param("record") Enterprise_position record, @Param("example") Enterprise_positionExample example);

    int updateByPrimaryKeySelective(Enterprise_position record);

    int updateByPrimaryKey(Enterprise_position record);
}