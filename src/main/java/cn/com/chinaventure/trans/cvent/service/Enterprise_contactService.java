package cn.com.chinaventure.trans.cvent.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_contact;

import java.util.List;

/**
 * @author 
 *
 */
public interface Enterprise_contactService {
	
	/**
     * @api saveEnterprise_contact 新增enterprise_contact
     * @apiGroup enterprise_contact管理
     * @apiName  新增enterprise_contact记录
     * @apiDescription 全量插入enterprise_contact记录
     * @apiParam {Enterprise_contact} enterprise_contact enterprise_contact对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise_contact(Enterprise_contact enterprise_contact);
	
	/**
     * @api updateEnterprise_contact 修改enterprise_contact
     * @apiGroup enterprise_contact管理
     * @apiName  修改enterprise_contact记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise_contact} enterprise_contact enterprise_contact对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise_contact(Enterprise_contact enterprise_contact);
	
	/**
     * @api getEnterprise_contactById 根据enterprise_contactid查询详情
     * @apiGroup enterprise_contact管理
     * @apiName  查询enterprise_contact详情
     * @apiDescription 根据主键id查询enterprise_contact详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Enterprise_contact enterprise_contact实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise_contact getEnterprise_contactById(Long id);
	
	/**
     * @api getEnterprise_contactList 根据enterprise_contact条件查询列表
     * @apiGroup enterprise_contact管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise_contact} enterprise_contact  实体条件
     * @apiSuccess List<Enterprise_contact> enterprise_contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_contact> getEnterprise_contactList(Enterprise_contact enterprise_contact);
	

	/**
     * @api getEnterprise_contactListByParam 根据enterprise_contact条件查询列表（含排序）
     * @apiGroup enterprise_contact管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise_contact} enterprise_contact  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise_contact> enterprise_contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_contact> getEnterprise_contactListByParam(Enterprise_contact enterprise_contact, String orderByStr);
	
	
	/**
     * @api findEnterprise_contactPageByParam 根据enterprise_contact条件查询列表（分页）
     * @apiGroup enterprise_contact管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise_contact} enterprise_contact  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise_contact> enterprise_contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_contact> findEnterprise_contactPageByParam(Enterprise_contact enterprise_contact, PageParam pageParam); 
	
	/**
     * @api findEnterprise_contactPageByParam 根据enterprise_contact条件查询列表（分页，含排序）
     * @apiGroup enterprise_contact管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise_contact} enterprise_contact  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise_contact> enterprise_contact实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_contact> findEnterprise_contactPageByParam(Enterprise_contact enterprise_contact, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterprise_contactById 根据enterprise_contact的id删除(物理删除)
     * @apiGroup enterprise_contact管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterprise_contactById(Long id);


	public Enterprise_contact getEnterpriseContactByEnpId(Integer enterPriseId);

}

