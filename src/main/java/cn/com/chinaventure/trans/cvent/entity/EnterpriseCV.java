package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class EnterpriseCV implements Serializable {
    private Integer enterpriseId;

    private String enterpriseCnName;

    private String enterpriseCnShort;

    private String enterpriseCnOnce;

    private String enterpriseEnName;

    private String enterpriseEnShort;

    private String enterpriseEnOnce;

    private String enterpriseKeywords;

    private String enterpriseWeb;

    private String enterpriseLogo;

    private Short enterpriseCountry;

    private Short enterpriseCity;

    private Short enterpriseTown;

    private Date enterpriseSetuptime;

    private Date enterpriseStoptime;

    private Byte commercestatusId;

    private Byte enterprisestageId;

    private Byte enterprisestageStatus;

    private Date createTime;

    private Date updateTime;

    private String behavior;

    private Date verifyTime;

    private String verifyUser;

    private String content;

    private Integer financingEventId;

    private Integer appearmarketId;

    private Integer buytogethereventId;

    private Short financingCount;

    private Short financeCount;

    private Short financingintelligenceCount;

    private Byte financingeventtypeid;

    private Byte financingevententityid;

    private Double financingrealitymoneyus;

    private Date financinghappendate;

    private Double appearmarketrealitymoneyus;

    private Date appearmarkethappendate;

    private Double buytogetherrealitymoneyus;

    private Date buytogetherhappendate;

    private String industryCnname;

    private String financingeventtypeCnname;

    private String exchangeCnname;

    private String buytogetherenterpriseCnname;

    private String buytogetherenterpriseId;

    private Double financingrealitymoney;

    private Byte financingrealitymoneyCurrency;

    private Double appearmarketrealitymoney;

    private Byte appearmarketrealitymoneyCurrency;

    private Double buytogetherrealitymoney;

    private Byte buytogetherrealitymoneyCurrency;

    private Short corprankCount;

    private String lastCorprankCnname;

    private String industryId;

    private String industryEnname;

    private Integer lastCorprankId;

    private Byte isEvent;

    private String exchangeEnname;

    private String buytogetherenterpriseEnname;

    private String financingeventtypeEnname;

    private String pdfPath;

    private String organizeCode;

    private String stockCode;

    private Byte characterId;

    private Byte sourceId;

    private String qincode;

    private Short internateOne;

    private Short internateTwo;

    private Short internateThree;

    private Short internateFour;

    private String internateId;

    private String internateCnname;

    private String internateEnname;

    private Double employeeNumber;

    private Double incomeOperations;

    private Double totalAssets;

    private String exchangeId;

    private Double totalEquity;

    private Double totalProfit;

    private Double roe;

    private Double sellGrossProfit;

    private String enterprisetype;

    private Double sellAverageRate;

    private Double net;

    private Byte searchtype;

    private Double newincomeOperations;

    private Date newincomeYear;

    private String legalPerson;

    private String mainProductsOne;

    private String mainProductsTwo;

    private String mainProductsThree;

    private Byte moduleId;

    private String role;

    private String trade;

    private Byte characterBackground;

    private Byte characterNature;

    private Integer parentId;

    private String parentOrganizeCode;

    private Byte level;

    private String mainProductOneNew;

    private String mainProductTwoNew;

    private String mainProductThreeNew;

    private String enterpriseCnDesc;

    private String enterpriseEnDesc;

    /**
     * 1,新三板
     */
    private Integer exchangeType;

    /**
     * 挂牌市场
     */
    private Integer listMarketNew;

    /**
     * 是否场外交易  1-无场外交易;2-新三板;3-产权股权交易
     */
    private Short isOtc;

    /**
     * 企业工商注册号
     */
    private String businessRegistrationId;

    /**
     * 企业控股情况 对应cvsource.tbl_dic_shareholding.ID
     */
    private Short shareholdingId;

    /**
     * 企业注册类型一级 cvsource.tbl_dic_character.id
     */
    private Short characterOne;

    /**
     * 企业注册类型二级 cvsource.tbl_dic_character.id
     */
    private Short characterTwo;

    /**
     * 企业注册类型三级 cvsource.tbl_dic_character.id
     */
    private Short characterThree;

    private static final long serialVersionUID = 1L;

    public Integer getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Integer enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getEnterpriseCnName() {
        return enterpriseCnName;
    }

    public void setEnterpriseCnName(String enterpriseCnName) {
        this.enterpriseCnName = enterpriseCnName == null ? null : enterpriseCnName.trim();
    }

    public String getEnterpriseCnShort() {
        return enterpriseCnShort;
    }

    public void setEnterpriseCnShort(String enterpriseCnShort) {
        this.enterpriseCnShort = enterpriseCnShort == null ? null : enterpriseCnShort.trim();
    }

    public String getEnterpriseCnOnce() {
        return enterpriseCnOnce;
    }

    public void setEnterpriseCnOnce(String enterpriseCnOnce) {
        this.enterpriseCnOnce = enterpriseCnOnce == null ? null : enterpriseCnOnce.trim();
    }

    public String getEnterpriseEnName() {
        return enterpriseEnName;
    }

    public void setEnterpriseEnName(String enterpriseEnName) {
        this.enterpriseEnName = enterpriseEnName == null ? null : enterpriseEnName.trim();
    }

    public String getEnterpriseEnShort() {
        return enterpriseEnShort;
    }

    public void setEnterpriseEnShort(String enterpriseEnShort) {
        this.enterpriseEnShort = enterpriseEnShort == null ? null : enterpriseEnShort.trim();
    }

    public String getEnterpriseEnOnce() {
        return enterpriseEnOnce;
    }

    public void setEnterpriseEnOnce(String enterpriseEnOnce) {
        this.enterpriseEnOnce = enterpriseEnOnce == null ? null : enterpriseEnOnce.trim();
    }

    public String getEnterpriseKeywords() {
        return enterpriseKeywords;
    }

    public void setEnterpriseKeywords(String enterpriseKeywords) {
        this.enterpriseKeywords = enterpriseKeywords == null ? null : enterpriseKeywords.trim();
    }

    public String getEnterpriseWeb() {
        return enterpriseWeb;
    }

    public void setEnterpriseWeb(String enterpriseWeb) {
        this.enterpriseWeb = enterpriseWeb == null ? null : enterpriseWeb.trim();
    }

    public String getEnterpriseLogo() {
        return enterpriseLogo;
    }

    public void setEnterpriseLogo(String enterpriseLogo) {
        this.enterpriseLogo = enterpriseLogo == null ? null : enterpriseLogo.trim();
    }

    public Short getEnterpriseCountry() {
        return enterpriseCountry;
    }

    public void setEnterpriseCountry(Short enterpriseCountry) {
        this.enterpriseCountry = enterpriseCountry;
    }

    public Short getEnterpriseCity() {
        return enterpriseCity;
    }

    public void setEnterpriseCity(Short enterpriseCity) {
        this.enterpriseCity = enterpriseCity;
    }

    public Short getEnterpriseTown() {
        return enterpriseTown;
    }

    public void setEnterpriseTown(Short enterpriseTown) {
        this.enterpriseTown = enterpriseTown;
    }

    public Date getEnterpriseSetuptime() {
        return enterpriseSetuptime;
    }

    public void setEnterpriseSetuptime(Date enterpriseSetuptime) {
        this.enterpriseSetuptime = enterpriseSetuptime;
    }

    public Date getEnterpriseStoptime() {
        return enterpriseStoptime;
    }

    public void setEnterpriseStoptime(Date enterpriseStoptime) {
        this.enterpriseStoptime = enterpriseStoptime;
    }

    public Byte getCommercestatusId() {
        return commercestatusId;
    }

    public void setCommercestatusId(Byte commercestatusId) {
        this.commercestatusId = commercestatusId;
    }

    public Byte getEnterprisestageId() {
        return enterprisestageId;
    }

    public void setEnterprisestageId(Byte enterprisestageId) {
        this.enterprisestageId = enterprisestageId;
    }

    public Byte getEnterprisestageStatus() {
        return enterprisestageStatus;
    }

    public void setEnterprisestageStatus(Byte enterprisestageStatus) {
        this.enterprisestageStatus = enterprisestageStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior == null ? null : behavior.trim();
    }

    public Date getVerifyTime() {
        return verifyTime;
    }

    public void setVerifyTime(Date verifyTime) {
        this.verifyTime = verifyTime;
    }

    public String getVerifyUser() {
        return verifyUser;
    }

    public void setVerifyUser(String verifyUser) {
        this.verifyUser = verifyUser == null ? null : verifyUser.trim();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getFinancingEventId() {
        return financingEventId;
    }

    public void setFinancingEventId(Integer financingEventId) {
        this.financingEventId = financingEventId;
    }

    public Integer getAppearmarketId() {
        return appearmarketId;
    }

    public void setAppearmarketId(Integer appearmarketId) {
        this.appearmarketId = appearmarketId;
    }

    public Integer getBuytogethereventId() {
        return buytogethereventId;
    }

    public void setBuytogethereventId(Integer buytogethereventId) {
        this.buytogethereventId = buytogethereventId;
    }

    public Short getFinancingCount() {
        return financingCount;
    }

    public void setFinancingCount(Short financingCount) {
        this.financingCount = financingCount;
    }

    public Short getFinanceCount() {
        return financeCount;
    }

    public void setFinanceCount(Short financeCount) {
        this.financeCount = financeCount;
    }

    public Short getFinancingintelligenceCount() {
        return financingintelligenceCount;
    }

    public void setFinancingintelligenceCount(Short financingintelligenceCount) {
        this.financingintelligenceCount = financingintelligenceCount;
    }

    public Byte getFinancingeventtypeid() {
        return financingeventtypeid;
    }

    public void setFinancingeventtypeid(Byte financingeventtypeid) {
        this.financingeventtypeid = financingeventtypeid;
    }

    public Byte getFinancingevententityid() {
        return financingevententityid;
    }

    public void setFinancingevententityid(Byte financingevententityid) {
        this.financingevententityid = financingevententityid;
    }

    public Double getFinancingrealitymoneyus() {
        return financingrealitymoneyus;
    }

    public void setFinancingrealitymoneyus(Double financingrealitymoneyus) {
        this.financingrealitymoneyus = financingrealitymoneyus;
    }

    public Date getFinancinghappendate() {
        return financinghappendate;
    }

    public void setFinancinghappendate(Date financinghappendate) {
        this.financinghappendate = financinghappendate;
    }

    public Double getAppearmarketrealitymoneyus() {
        return appearmarketrealitymoneyus;
    }

    public void setAppearmarketrealitymoneyus(Double appearmarketrealitymoneyus) {
        this.appearmarketrealitymoneyus = appearmarketrealitymoneyus;
    }

    public Date getAppearmarkethappendate() {
        return appearmarkethappendate;
    }

    public void setAppearmarkethappendate(Date appearmarkethappendate) {
        this.appearmarkethappendate = appearmarkethappendate;
    }

    public Double getBuytogetherrealitymoneyus() {
        return buytogetherrealitymoneyus;
    }

    public void setBuytogetherrealitymoneyus(Double buytogetherrealitymoneyus) {
        this.buytogetherrealitymoneyus = buytogetherrealitymoneyus;
    }

    public Date getBuytogetherhappendate() {
        return buytogetherhappendate;
    }

    public void setBuytogetherhappendate(Date buytogetherhappendate) {
        this.buytogetherhappendate = buytogetherhappendate;
    }

    public String getIndustryCnname() {
        return industryCnname;
    }

    public void setIndustryCnname(String industryCnname) {
        this.industryCnname = industryCnname == null ? null : industryCnname.trim();
    }

    public String getFinancingeventtypeCnname() {
        return financingeventtypeCnname;
    }

    public void setFinancingeventtypeCnname(String financingeventtypeCnname) {
        this.financingeventtypeCnname = financingeventtypeCnname == null ? null : financingeventtypeCnname.trim();
    }

    public String getExchangeCnname() {
        return exchangeCnname;
    }

    public void setExchangeCnname(String exchangeCnname) {
        this.exchangeCnname = exchangeCnname == null ? null : exchangeCnname.trim();
    }

    public String getBuytogetherenterpriseCnname() {
        return buytogetherenterpriseCnname;
    }

    public void setBuytogetherenterpriseCnname(String buytogetherenterpriseCnname) {
        this.buytogetherenterpriseCnname = buytogetherenterpriseCnname == null ? null : buytogetherenterpriseCnname.trim();
    }

    public String getBuytogetherenterpriseId() {
        return buytogetherenterpriseId;
    }

    public void setBuytogetherenterpriseId(String buytogetherenterpriseId) {
        this.buytogetherenterpriseId = buytogetherenterpriseId == null ? null : buytogetherenterpriseId.trim();
    }

    public Double getFinancingrealitymoney() {
        return financingrealitymoney;
    }

    public void setFinancingrealitymoney(Double financingrealitymoney) {
        this.financingrealitymoney = financingrealitymoney;
    }

    public Byte getFinancingrealitymoneyCurrency() {
        return financingrealitymoneyCurrency;
    }

    public void setFinancingrealitymoneyCurrency(Byte financingrealitymoneyCurrency) {
        this.financingrealitymoneyCurrency = financingrealitymoneyCurrency;
    }

    public Double getAppearmarketrealitymoney() {
        return appearmarketrealitymoney;
    }

    public void setAppearmarketrealitymoney(Double appearmarketrealitymoney) {
        this.appearmarketrealitymoney = appearmarketrealitymoney;
    }

    public Byte getAppearmarketrealitymoneyCurrency() {
        return appearmarketrealitymoneyCurrency;
    }

    public void setAppearmarketrealitymoneyCurrency(Byte appearmarketrealitymoneyCurrency) {
        this.appearmarketrealitymoneyCurrency = appearmarketrealitymoneyCurrency;
    }

    public Double getBuytogetherrealitymoney() {
        return buytogetherrealitymoney;
    }

    public void setBuytogetherrealitymoney(Double buytogetherrealitymoney) {
        this.buytogetherrealitymoney = buytogetherrealitymoney;
    }

    public Byte getBuytogetherrealitymoneyCurrency() {
        return buytogetherrealitymoneyCurrency;
    }

    public void setBuytogetherrealitymoneyCurrency(Byte buytogetherrealitymoneyCurrency) {
        this.buytogetherrealitymoneyCurrency = buytogetherrealitymoneyCurrency;
    }

    public Short getCorprankCount() {
        return corprankCount;
    }

    public void setCorprankCount(Short corprankCount) {
        this.corprankCount = corprankCount;
    }

    public String getLastCorprankCnname() {
        return lastCorprankCnname;
    }

    public void setLastCorprankCnname(String lastCorprankCnname) {
        this.lastCorprankCnname = lastCorprankCnname == null ? null : lastCorprankCnname.trim();
    }

    public String getIndustryId() {
        return industryId;
    }

    public void setIndustryId(String industryId) {
        this.industryId = industryId == null ? null : industryId.trim();
    }

    public String getIndustryEnname() {
        return industryEnname;
    }

    public void setIndustryEnname(String industryEnname) {
        this.industryEnname = industryEnname == null ? null : industryEnname.trim();
    }

    public Integer getLastCorprankId() {
        return lastCorprankId;
    }

    public void setLastCorprankId(Integer lastCorprankId) {
        this.lastCorprankId = lastCorprankId;
    }

    public Byte getIsEvent() {
        return isEvent;
    }

    public void setIsEvent(Byte isEvent) {
        this.isEvent = isEvent;
    }

    public String getExchangeEnname() {
        return exchangeEnname;
    }

    public void setExchangeEnname(String exchangeEnname) {
        this.exchangeEnname = exchangeEnname == null ? null : exchangeEnname.trim();
    }

    public String getBuytogetherenterpriseEnname() {
        return buytogetherenterpriseEnname;
    }

    public void setBuytogetherenterpriseEnname(String buytogetherenterpriseEnname) {
        this.buytogetherenterpriseEnname = buytogetherenterpriseEnname == null ? null : buytogetherenterpriseEnname.trim();
    }

    public String getFinancingeventtypeEnname() {
        return financingeventtypeEnname;
    }

    public void setFinancingeventtypeEnname(String financingeventtypeEnname) {
        this.financingeventtypeEnname = financingeventtypeEnname == null ? null : financingeventtypeEnname.trim();
    }

    public String getPdfPath() {
        return pdfPath;
    }

    public void setPdfPath(String pdfPath) {
        this.pdfPath = pdfPath == null ? null : pdfPath.trim();
    }

    public String getOrganizeCode() {
        return organizeCode;
    }

    public void setOrganizeCode(String organizeCode) {
        this.organizeCode = organizeCode == null ? null : organizeCode.trim();
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode == null ? null : stockCode.trim();
    }

    public Byte getCharacterId() {
        return characterId;
    }

    public void setCharacterId(Byte characterId) {
        this.characterId = characterId;
    }

    public Byte getSourceId() {
        return sourceId;
    }

    public void setSourceId(Byte sourceId) {
        this.sourceId = sourceId;
    }

    public String getQincode() {
        return qincode;
    }

    public void setQincode(String qincode) {
        this.qincode = qincode == null ? null : qincode.trim();
    }

    public Short getInternateOne() {
        return internateOne;
    }

    public void setInternateOne(Short internateOne) {
        this.internateOne = internateOne;
    }

    public Short getInternateTwo() {
        return internateTwo;
    }

    public void setInternateTwo(Short internateTwo) {
        this.internateTwo = internateTwo;
    }

    public Short getInternateThree() {
        return internateThree;
    }

    public void setInternateThree(Short internateThree) {
        this.internateThree = internateThree;
    }

    public Short getInternateFour() {
        return internateFour;
    }

    public void setInternateFour(Short internateFour) {
        this.internateFour = internateFour;
    }

    public String getInternateId() {
        return internateId;
    }

    public void setInternateId(String internateId) {
        this.internateId = internateId == null ? null : internateId.trim();
    }

    public String getInternateCnname() {
        return internateCnname;
    }

    public void setInternateCnname(String internateCnname) {
        this.internateCnname = internateCnname == null ? null : internateCnname.trim();
    }

    public String getInternateEnname() {
        return internateEnname;
    }

    public void setInternateEnname(String internateEnname) {
        this.internateEnname = internateEnname == null ? null : internateEnname.trim();
    }

    public Double getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(Double employeeNumber) {
        this.employeeNumber = employeeNumber;
    }

    public Double getIncomeOperations() {
        return incomeOperations;
    }

    public void setIncomeOperations(Double incomeOperations) {
        this.incomeOperations = incomeOperations;
    }

    public Double getTotalAssets() {
        return totalAssets;
    }

    public void setTotalAssets(Double totalAssets) {
        this.totalAssets = totalAssets;
    }

    public String getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(String exchangeId) {
        this.exchangeId = exchangeId == null ? null : exchangeId.trim();
    }

    public Double getTotalEquity() {
        return totalEquity;
    }

    public void setTotalEquity(Double totalEquity) {
        this.totalEquity = totalEquity;
    }

    public Double getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(Double totalProfit) {
        this.totalProfit = totalProfit;
    }

    public Double getRoe() {
        return roe;
    }

    public void setRoe(Double roe) {
        this.roe = roe;
    }

    public Double getSellGrossProfit() {
        return sellGrossProfit;
    }

    public void setSellGrossProfit(Double sellGrossProfit) {
        this.sellGrossProfit = sellGrossProfit;
    }

    public String getEnterprisetype() {
        return enterprisetype;
    }

    public void setEnterprisetype(String enterprisetype) {
        this.enterprisetype = enterprisetype == null ? null : enterprisetype.trim();
    }

    public Double getSellAverageRate() {
        return sellAverageRate;
    }

    public void setSellAverageRate(Double sellAverageRate) {
        this.sellAverageRate = sellAverageRate;
    }

    public Double getNet() {
        return net;
    }

    public void setNet(Double net) {
        this.net = net;
    }

    public Byte getSearchtype() {
        return searchtype;
    }

    public void setSearchtype(Byte searchtype) {
        this.searchtype = searchtype;
    }

    public Double getNewincomeOperations() {
        return newincomeOperations;
    }

    public void setNewincomeOperations(Double newincomeOperations) {
        this.newincomeOperations = newincomeOperations;
    }

    public Date getNewincomeYear() {
        return newincomeYear;
    }

    public void setNewincomeYear(Date newincomeYear) {
        this.newincomeYear = newincomeYear;
    }

    public String getLegalPerson() {
        return legalPerson;
    }

    public void setLegalPerson(String legalPerson) {
        this.legalPerson = legalPerson == null ? null : legalPerson.trim();
    }

    public String getMainProductsOne() {
        return mainProductsOne;
    }

    public void setMainProductsOne(String mainProductsOne) {
        this.mainProductsOne = mainProductsOne == null ? null : mainProductsOne.trim();
    }

    public String getMainProductsTwo() {
        return mainProductsTwo;
    }

    public void setMainProductsTwo(String mainProductsTwo) {
        this.mainProductsTwo = mainProductsTwo == null ? null : mainProductsTwo.trim();
    }

    public String getMainProductsThree() {
        return mainProductsThree;
    }

    public void setMainProductsThree(String mainProductsThree) {
        this.mainProductsThree = mainProductsThree == null ? null : mainProductsThree.trim();
    }

    public Byte getModuleId() {
        return moduleId;
    }

    public void setModuleId(Byte moduleId) {
        this.moduleId = moduleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade == null ? null : trade.trim();
    }

    public Byte getCharacterBackground() {
        return characterBackground;
    }

    public void setCharacterBackground(Byte characterBackground) {
        this.characterBackground = characterBackground;
    }

    public Byte getCharacterNature() {
        return characterNature;
    }

    public void setCharacterNature(Byte characterNature) {
        this.characterNature = characterNature;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getParentOrganizeCode() {
        return parentOrganizeCode;
    }

    public void setParentOrganizeCode(String parentOrganizeCode) {
        this.parentOrganizeCode = parentOrganizeCode == null ? null : parentOrganizeCode.trim();
    }

    public Byte getLevel() {
        return level;
    }

    public void setLevel(Byte level) {
        this.level = level;
    }

    public String getMainProductOneNew() {
        return mainProductOneNew;
    }

    public void setMainProductOneNew(String mainProductOneNew) {
        this.mainProductOneNew = mainProductOneNew == null ? null : mainProductOneNew.trim();
    }

    public String getMainProductTwoNew() {
        return mainProductTwoNew;
    }

    public void setMainProductTwoNew(String mainProductTwoNew) {
        this.mainProductTwoNew = mainProductTwoNew == null ? null : mainProductTwoNew.trim();
    }

    public String getMainProductThreeNew() {
        return mainProductThreeNew;
    }

    public void setMainProductThreeNew(String mainProductThreeNew) {
        this.mainProductThreeNew = mainProductThreeNew == null ? null : mainProductThreeNew.trim();
    }

    public Integer getExchangeType() {
        return exchangeType;
    }

    public void setExchangeType(Integer exchangeType) {
        this.exchangeType = exchangeType;
    }

    public Integer getListMarketNew() {
        return listMarketNew;
    }

    public void setListMarketNew(Integer listMarketNew) {
        this.listMarketNew = listMarketNew;
    }

    public Short getIsOtc() {
        return isOtc;
    }

    public void setIsOtc(Short isOtc) {
        this.isOtc = isOtc;
    }

    public String getBusinessRegistrationId() {
        return businessRegistrationId;
    }

    public void setBusinessRegistrationId(String businessRegistrationId) {
        this.businessRegistrationId = businessRegistrationId == null ? null : businessRegistrationId.trim();
    }

    public Short getShareholdingId() {
        return shareholdingId;
    }

    public void setShareholdingId(Short shareholdingId) {
        this.shareholdingId = shareholdingId;
    }

    public Short getCharacterOne() {
        return characterOne;
    }

    public void setCharacterOne(Short characterOne) {
        this.characterOne = characterOne;
    }

    public Short getCharacterTwo() {
        return characterTwo;
    }

    public void setCharacterTwo(Short characterTwo) {
        this.characterTwo = characterTwo;
    }

    public Short getCharacterThree() {
        return characterThree;
    }

    public void setCharacterThree(Short characterThree) {
        this.characterThree = characterThree;
    }

    public String getEnterpriseCnDesc() {
        return enterpriseCnDesc;
    }

    public void setEnterpriseCnDesc(String enterpriseCnDesc) {
        this.enterpriseCnDesc = enterpriseCnDesc == null ? null : enterpriseCnDesc.trim();
    }

    public String getEnterpriseEnDesc() {
        return enterpriseEnDesc;
    }

    public void setEnterpriseEnDesc(String enterpriseEnDesc) {
        this.enterpriseEnDesc = enterpriseEnDesc == null ? null : enterpriseEnDesc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", enterpriseId=").append(enterpriseId);
        sb.append(", enterpriseCnName=").append(enterpriseCnName);
        sb.append(", enterpriseCnShort=").append(enterpriseCnShort);
        sb.append(", enterpriseCnOnce=").append(enterpriseCnOnce);
        sb.append(", enterpriseEnName=").append(enterpriseEnName);
        sb.append(", enterpriseEnShort=").append(enterpriseEnShort);
        sb.append(", enterpriseEnOnce=").append(enterpriseEnOnce);
        sb.append(", enterpriseKeywords=").append(enterpriseKeywords);
        sb.append(", enterpriseWeb=").append(enterpriseWeb);
        sb.append(", enterpriseLogo=").append(enterpriseLogo);
        sb.append(", enterpriseCountry=").append(enterpriseCountry);
        sb.append(", enterpriseCity=").append(enterpriseCity);
        sb.append(", enterpriseTown=").append(enterpriseTown);
        sb.append(", enterpriseSetuptime=").append(enterpriseSetuptime);
        sb.append(", enterpriseStoptime=").append(enterpriseStoptime);
        sb.append(", commercestatusId=").append(commercestatusId);
        sb.append(", enterprisestageId=").append(enterprisestageId);
        sb.append(", enterprisestageStatus=").append(enterprisestageStatus);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", behavior=").append(behavior);
        sb.append(", verifyTime=").append(verifyTime);
        sb.append(", verifyUser=").append(verifyUser);
        sb.append(", content=").append(content);
        sb.append(", financingEventId=").append(financingEventId);
        sb.append(", appearmarketId=").append(appearmarketId);
        sb.append(", buytogethereventId=").append(buytogethereventId);
        sb.append(", financingCount=").append(financingCount);
        sb.append(", financeCount=").append(financeCount);
        sb.append(", financingintelligenceCount=").append(financingintelligenceCount);
        sb.append(", financingeventtypeid=").append(financingeventtypeid);
        sb.append(", financingevententityid=").append(financingevententityid);
        sb.append(", financingrealitymoneyus=").append(financingrealitymoneyus);
        sb.append(", financinghappendate=").append(financinghappendate);
        sb.append(", appearmarketrealitymoneyus=").append(appearmarketrealitymoneyus);
        sb.append(", appearmarkethappendate=").append(appearmarkethappendate);
        sb.append(", buytogetherrealitymoneyus=").append(buytogetherrealitymoneyus);
        sb.append(", buytogetherhappendate=").append(buytogetherhappendate);
        sb.append(", industryCnname=").append(industryCnname);
        sb.append(", financingeventtypeCnname=").append(financingeventtypeCnname);
        sb.append(", exchangeCnname=").append(exchangeCnname);
        sb.append(", buytogetherenterpriseCnname=").append(buytogetherenterpriseCnname);
        sb.append(", buytogetherenterpriseId=").append(buytogetherenterpriseId);
        sb.append(", financingrealitymoney=").append(financingrealitymoney);
        sb.append(", financingrealitymoneyCurrency=").append(financingrealitymoneyCurrency);
        sb.append(", appearmarketrealitymoney=").append(appearmarketrealitymoney);
        sb.append(", appearmarketrealitymoneyCurrency=").append(appearmarketrealitymoneyCurrency);
        sb.append(", buytogetherrealitymoney=").append(buytogetherrealitymoney);
        sb.append(", buytogetherrealitymoneyCurrency=").append(buytogetherrealitymoneyCurrency);
        sb.append(", corprankCount=").append(corprankCount);
        sb.append(", lastCorprankCnname=").append(lastCorprankCnname);
        sb.append(", industryId=").append(industryId);
        sb.append(", industryEnname=").append(industryEnname);
        sb.append(", lastCorprankId=").append(lastCorprankId);
        sb.append(", isEvent=").append(isEvent);
        sb.append(", exchangeEnname=").append(exchangeEnname);
        sb.append(", buytogetherenterpriseEnname=").append(buytogetherenterpriseEnname);
        sb.append(", financingeventtypeEnname=").append(financingeventtypeEnname);
        sb.append(", pdfPath=").append(pdfPath);
        sb.append(", organizeCode=").append(organizeCode);
        sb.append(", stockCode=").append(stockCode);
        sb.append(", characterId=").append(characterId);
        sb.append(", sourceId=").append(sourceId);
        sb.append(", qincode=").append(qincode);
        sb.append(", internateOne=").append(internateOne);
        sb.append(", internateTwo=").append(internateTwo);
        sb.append(", internateThree=").append(internateThree);
        sb.append(", internateFour=").append(internateFour);
        sb.append(", internateId=").append(internateId);
        sb.append(", internateCnname=").append(internateCnname);
        sb.append(", internateEnname=").append(internateEnname);
        sb.append(", employeeNumber=").append(employeeNumber);
        sb.append(", incomeOperations=").append(incomeOperations);
        sb.append(", totalAssets=").append(totalAssets);
        sb.append(", exchangeId=").append(exchangeId);
        sb.append(", totalEquity=").append(totalEquity);
        sb.append(", totalProfit=").append(totalProfit);
        sb.append(", roe=").append(roe);
        sb.append(", sellGrossProfit=").append(sellGrossProfit);
        sb.append(", enterprisetype=").append(enterprisetype);
        sb.append(", sellAverageRate=").append(sellAverageRate);
        sb.append(", net=").append(net);
        sb.append(", searchtype=").append(searchtype);
        sb.append(", newincomeOperations=").append(newincomeOperations);
        sb.append(", newincomeYear=").append(newincomeYear);
        sb.append(", legalPerson=").append(legalPerson);
        sb.append(", mainProductsOne=").append(mainProductsOne);
        sb.append(", mainProductsTwo=").append(mainProductsTwo);
        sb.append(", mainProductsThree=").append(mainProductsThree);
        sb.append(", moduleId=").append(moduleId);
        sb.append(", role=").append(role);
        sb.append(", trade=").append(trade);
        sb.append(", characterBackground=").append(characterBackground);
        sb.append(", characterNature=").append(characterNature);
        sb.append(", parentId=").append(parentId);
        sb.append(", parentOrganizeCode=").append(parentOrganizeCode);
        sb.append(", level=").append(level);
        sb.append(", mainProductOneNew=").append(mainProductOneNew);
        sb.append(", mainProductTwoNew=").append(mainProductTwoNew);
        sb.append(", mainProductThreeNew=").append(mainProductThreeNew);
        sb.append(", exchangeType=").append(exchangeType);
        sb.append(", listMarketNew=").append(listMarketNew);
        sb.append(", isOtc=").append(isOtc);
        sb.append(", businessRegistrationId=").append(businessRegistrationId);
        sb.append(", shareholdingId=").append(shareholdingId);
        sb.append(", characterOne=").append(characterOne);
        sb.append(", characterTwo=").append(characterTwo);
        sb.append(", characterThree=").append(characterThree);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}