package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteam;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteamExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_manageteamMapper {
    int countByExample(Enterprise_manageteamExample example);

    int deleteByExample(Enterprise_manageteamExample example);

    int deleteByPrimaryKey(Long enterpriseMtId);

    int insert(Enterprise_manageteam record);

    int insertSelective(Enterprise_manageteam record);

    List<Enterprise_manageteam> selectByExample(Enterprise_manageteamExample example);

    Enterprise_manageteam selectByPrimaryKey(Long enterpriseMtId);

    int updateByExampleSelective(@Param("record") Enterprise_manageteam record, @Param("example") Enterprise_manageteamExample example);

    int updateByExample(@Param("record") Enterprise_manageteam record, @Param("example") Enterprise_manageteamExample example);

    int updateByPrimaryKeySelective(Enterprise_manageteam record);

    int updateByPrimaryKey(Enterprise_manageteam record);
}