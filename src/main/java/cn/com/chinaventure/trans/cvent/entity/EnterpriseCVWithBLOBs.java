package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;

public class EnterpriseCVWithBLOBs extends EnterpriseCV implements Serializable {
    private String enterpriseCnDesc;

    private String enterpriseEnDesc;

    private static final long serialVersionUID = 1L;

    public String getEnterpriseCnDesc() {
        return enterpriseCnDesc;
    }

    public void setEnterpriseCnDesc(String enterpriseCnDesc) {
        this.enterpriseCnDesc = enterpriseCnDesc == null ? null : enterpriseCnDesc.trim();
    }

    public String getEnterpriseEnDesc() {
        return enterpriseEnDesc;
    }

    public void setEnterpriseEnDesc(String enterpriseEnDesc) {
        this.enterpriseEnDesc = enterpriseEnDesc == null ? null : enterpriseEnDesc.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", enterpriseCnDesc=").append(enterpriseCnDesc);
        sb.append(", enterpriseEnDesc=").append(enterpriseEnDesc);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}