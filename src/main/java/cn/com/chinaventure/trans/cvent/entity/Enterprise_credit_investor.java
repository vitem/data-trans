package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class Enterprise_credit_investor implements Serializable {
    private Long id;

    /**
     * 企业登记信息ID
     */
    private Long creditId;

    /**
     * 投资人名称
     */
    private String investorname;

    /**
     * 投资人类型
     */
    private String investortype;

    /**
     * 其他
     */
    private String othernum;

    /**
     * 出资方式
     */
    private String investmentway;

    /**
     * 认缴出资额
     */
    private Double rjamount;

    /**
     * 认缴出资额币种
     */
    private String rjamountUnit;

    /**
     * 认缴出资额单位
     */
    private String rjamountCurrency;

    private String rjamountinfo;

    /**
     * 实缴出资额
     */
    private Double sjamount;

    /**
     * 实缴出资额币种
     */
    private String sjamountUnit;

    /**
     * 实缴出资额单位
     */
    private String sjamountCurrency;

    private String sjamountinfo;

    /**
     * 实缴时间
     */
    private Date sjdate;

    private String sjdateinfo;

    /**
     * 入库时间
     */
    private Date createTime;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreditId() {
        return creditId;
    }

    public void setCreditId(Long creditId) {
        this.creditId = creditId;
    }

    public String getInvestorname() {
        return investorname;
    }

    public void setInvestorname(String investorname) {
        this.investorname = investorname == null ? null : investorname.trim();
    }

    public String getInvestortype() {
        return investortype;
    }

    public void setInvestortype(String investortype) {
        this.investortype = investortype == null ? null : investortype.trim();
    }

    public String getOthernum() {
        return othernum;
    }

    public void setOthernum(String othernum) {
        this.othernum = othernum == null ? null : othernum.trim();
    }

    public String getInvestmentway() {
        return investmentway;
    }

    public void setInvestmentway(String investmentway) {
        this.investmentway = investmentway == null ? null : investmentway.trim();
    }

    public Double getRjamount() {
        return rjamount;
    }

    public void setRjamount(Double rjamount) {
        this.rjamount = rjamount;
    }

    public String getRjamountUnit() {
        return rjamountUnit;
    }

    public void setRjamountUnit(String rjamountUnit) {
        this.rjamountUnit = rjamountUnit == null ? null : rjamountUnit.trim();
    }

    public String getRjamountCurrency() {
        return rjamountCurrency;
    }

    public void setRjamountCurrency(String rjamountCurrency) {
        this.rjamountCurrency = rjamountCurrency == null ? null : rjamountCurrency.trim();
    }

    public String getRjamountinfo() {
        return rjamountinfo;
    }

    public void setRjamountinfo(String rjamountinfo) {
        this.rjamountinfo = rjamountinfo == null ? null : rjamountinfo.trim();
    }

    public Double getSjamount() {
        return sjamount;
    }

    public void setSjamount(Double sjamount) {
        this.sjamount = sjamount;
    }

    public String getSjamountUnit() {
        return sjamountUnit;
    }

    public void setSjamountUnit(String sjamountUnit) {
        this.sjamountUnit = sjamountUnit == null ? null : sjamountUnit.trim();
    }

    public String getSjamountCurrency() {
        return sjamountCurrency;
    }

    public void setSjamountCurrency(String sjamountCurrency) {
        this.sjamountCurrency = sjamountCurrency == null ? null : sjamountCurrency.trim();
    }

    public String getSjamountinfo() {
        return sjamountinfo;
    }

    public void setSjamountinfo(String sjamountinfo) {
        this.sjamountinfo = sjamountinfo == null ? null : sjamountinfo.trim();
    }

    public Date getSjdate() {
        return sjdate;
    }

    public void setSjdate(Date sjdate) {
        this.sjdate = sjdate;
    }

    public String getSjdateinfo() {
        return sjdateinfo;
    }

    public void setSjdateinfo(String sjdateinfo) {
        this.sjdateinfo = sjdateinfo == null ? null : sjdateinfo.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", creditId=").append(creditId);
        sb.append(", investorname=").append(investorname);
        sb.append(", investortype=").append(investortype);
        sb.append(", othernum=").append(othernum);
        sb.append(", investmentway=").append(investmentway);
        sb.append(", rjamount=").append(rjamount);
        sb.append(", rjamountUnit=").append(rjamountUnit);
        sb.append(", rjamountCurrency=").append(rjamountCurrency);
        sb.append(", rjamountinfo=").append(rjamountinfo);
        sb.append(", sjamount=").append(sjamount);
        sb.append(", sjamountUnit=").append(sjamountUnit);
        sb.append(", sjamountCurrency=").append(sjamountCurrency);
        sb.append(", sjamountinfo=").append(sjamountinfo);
        sb.append(", sjdate=").append(sjdate);
        sb.append(", sjdateinfo=").append(sjdateinfo);
        sb.append(", createTime=").append(createTime);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}