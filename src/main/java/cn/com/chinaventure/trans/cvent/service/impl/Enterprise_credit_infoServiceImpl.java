package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.constant.CommonStatic;
import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_info;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_infoExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_infoExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_credit_infoMapper;
import cn.com.chinaventure.trans.cvent.service.Enterprise_credit_infoService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("enterprise_credit_infoService")
public class Enterprise_credit_infoServiceImpl implements Enterprise_credit_infoService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_credit_infoServiceImpl.class);

	@Resource
	private Enterprise_credit_infoMapper enterprise_credit_infoMapper;


	@Override
	public void saveEnterprise_credit_info(Enterprise_credit_info enterprise_credit_info) {
		if (null == enterprise_credit_info) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_infoMapper.insertSelective(enterprise_credit_info);
	}

	@Override
	public void updateEnterprise_credit_info(Enterprise_credit_info enterprise_credit_info) {
		if (null == enterprise_credit_info) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_infoMapper.updateByPrimaryKeySelective(enterprise_credit_info);
	}

	@Override
	public Enterprise_credit_info getEnterprise_credit_infoById(Long id) {
		return enterprise_credit_infoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_credit_info> getEnterprise_credit_infoList(Enterprise_credit_info enterprise_credit_info) {
		return this.getEnterprise_credit_infoListByParam(enterprise_credit_info, null);
	}

	@Override
	public List<Enterprise_credit_info> getEnterprise_credit_infoListByParam(Enterprise_credit_info enterprise_credit_info, String orderByStr) {
		Enterprise_credit_infoExample example = new Enterprise_credit_infoExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 	  example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		if(StringUtil.isNotEmpty(enterprise_credit_info.getEnterpriseId())){
			criteria.andEnterpriseIdNotEqualTo(enterprise_credit_info.getEnterpriseId());
		}

		List<Enterprise_credit_info> enterprise_credit_infoList = this.enterprise_credit_infoMapper.selectByExample(example);
		return enterprise_credit_infoList;
	}

	@Override
	public PageResult<Enterprise_credit_info> findEnterprise_credit_infoPageByParam(Enterprise_credit_info enterprise_credit_info, PageParam pageParam) {
		return this.findEnterprise_credit_infoPageByParam(enterprise_credit_info, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_credit_info> findEnterprise_credit_infoPageByParam(Enterprise_credit_info enterprise_credit_info, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_credit_info> pageResult = new PageResult<Enterprise_credit_info>();

		Enterprise_credit_infoExample example = new Enterprise_credit_infoExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_credit_info> list = null;// enterprise_credit_infoMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_credit_infoById(Long id) {
		this.enterprise_credit_infoMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Enterprise_credit_info getByEnterpriseId(Long enterId){
		if(CommonStatic.CREDIT_MAP.size()==0){
			Enterprise_credit_info enterprise_credit_info = new Enterprise_credit_info();
			enterprise_credit_info.setEnterpriseId(0L);
			//String orderby = " OpenEnd_Date desc , Credit_id desc  ";
			List<Enterprise_credit_info> list =  getEnterprise_credit_infoList( enterprise_credit_info);
			for (Enterprise_credit_info creditInfo : list) {
				Long enterpriseId = creditInfo.getEnterpriseId();
				if(CommonStatic.CREDIT_MAP.containsKey(enterpriseId)){
					if(null!=CommonStatic.CREDIT_MAP.get(enterpriseId).getOpenendDate() && null !=creditInfo.getOpenendDate()){
						CommonStatic.CREDIT_MAP.put(enterpriseId,creditInfo);
					}
					else if(CommonStatic.CREDIT_MAP.get(enterpriseId).getCreditId() - creditInfo.getCreditId() < 0 ){
						CommonStatic.CREDIT_MAP.put(enterpriseId,creditInfo);
					}
				}else{
					CommonStatic.CREDIT_MAP.put(enterpriseId,creditInfo);
				}
			}
		}
		return CommonStatic.CREDIT_MAP.get(enterId);
	}

}
