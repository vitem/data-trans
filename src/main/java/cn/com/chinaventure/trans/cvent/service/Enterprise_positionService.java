package cn.com.chinaventure.trans.cvent.service;

import java.util.List;
import java.util.Map;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_position;

/**
 * @author 
 *
 */
public interface Enterprise_positionService {
	
	/**
     * @api saveEnterprise_position 新增enterprise_position
     * @apiGroup enterprise_position管理
     * @apiName  新增enterprise_position记录
     * @apiDescription 全量插入enterprise_position记录
     * @apiParam {Enterprise_position} enterprise_position enterprise_position对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise_position(Enterprise_position enterprise_position);
	
	/**
     * @api updateEnterprise_position 修改enterprise_position
     * @apiGroup enterprise_position管理
     * @apiName  修改enterprise_position记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise_position} enterprise_position enterprise_position对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise_position(Enterprise_position enterprise_position);
	
	/**
     * @api getEnterprise_positionById 根据enterprise_positionid查询详情
     * @apiGroup enterprise_position管理
     * @apiName  查询enterprise_position详情
     * @apiDescription 根据主键id查询enterprise_position详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Enterprise_position enterprise_position实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise_position getEnterprise_positionById(Integer id);
	
	/**
     * @api getEnterprise_positionList 根据enterprise_position条件查询列表
     * @apiGroup enterprise_position管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise_position} enterprise_position  实体条件
     * @apiSuccess List<Enterprise_position> enterprise_position实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_position> getEnterprise_positionList(Enterprise_position enterprise_position);
	

	/**
     * @api getEnterprise_positionListByParam 根据enterprise_position条件查询列表（含排序）
     * @apiGroup enterprise_position管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise_position} enterprise_position  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise_position> enterprise_position实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_position> getEnterprise_positionListByParam(Enterprise_position enterprise_position, String orderByStr);
	
	
	/**
     * @api findEnterprise_positionPageByParam 根据enterprise_position条件查询列表（分页）
     * @apiGroup enterprise_position管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise_position} enterprise_position  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise_position> enterprise_position实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_position> findEnterprise_positionPageByParam(Enterprise_position enterprise_position, PageParam pageParam); 
	
	/**
     * @api findEnterprise_positionPageByParam 根据enterprise_position条件查询列表（分页，含排序）
     * @apiGroup enterprise_position管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise_position} enterprise_position  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise_position> enterprise_position实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_position> findEnterprise_positionPageByParam(Enterprise_position enterprise_position, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterprise_positionById 根据enterprise_position的id删除(物理删除)
     * @apiGroup enterprise_position管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterprise_positionById(Integer id);

}

