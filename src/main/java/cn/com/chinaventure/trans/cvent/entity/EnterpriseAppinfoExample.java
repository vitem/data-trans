package cn.com.chinaventure.trans.cvent.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class EnterpriseAppinfoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public EnterpriseAppinfoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("enterprise_id is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("enterprise_id is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(Integer value) {
            addCriterion("enterprise_id =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(Integer value) {
            addCriterion("enterprise_id <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(Integer value) {
            addCriterion("enterprise_id >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("enterprise_id >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(Integer value) {
            addCriterion("enterprise_id <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(Integer value) {
            addCriterion("enterprise_id <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<Integer> values) {
            addCriterion("enterprise_id in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<Integer> values) {
            addCriterion("enterprise_id not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(Integer value1, Integer value2) {
            addCriterion("enterprise_id between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("enterprise_id not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNull() {
            addCriterion("stock_code is null");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNotNull() {
            addCriterion("stock_code is not null");
            return (Criteria) this;
        }

        public Criteria andStockCodeEqualTo(String value) {
            addCriterion("stock_code =", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotEqualTo(String value) {
            addCriterion("stock_code <>", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThan(String value) {
            addCriterion("stock_code >", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThanOrEqualTo(String value) {
            addCriterion("stock_code >=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThan(String value) {
            addCriterion("stock_code <", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThanOrEqualTo(String value) {
            addCriterion("stock_code <=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLike(String value) {
            addCriterion("stock_code like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotLike(String value) {
            addCriterion("stock_code not like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIn(List<String> values) {
            addCriterion("stock_code in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotIn(List<String> values) {
            addCriterion("stock_code not in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeBetween(String value1, String value2) {
            addCriterion("stock_code between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotBetween(String value1, String value2) {
            addCriterion("stock_code not between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIsNull() {
            addCriterion("exchange_id is null");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIsNotNull() {
            addCriterion("exchange_id is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeIdEqualTo(Byte value) {
            addCriterion("exchange_id =", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotEqualTo(Byte value) {
            addCriterion("exchange_id <>", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdGreaterThan(Byte value) {
            addCriterion("exchange_id >", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("exchange_id >=", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLessThan(Byte value) {
            addCriterion("exchange_id <", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLessThanOrEqualTo(Byte value) {
            addCriterion("exchange_id <=", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIn(List<Byte> values) {
            addCriterion("exchange_id in", values, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotIn(List<Byte> values) {
            addCriterion("exchange_id not in", values, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdBetween(Byte value1, Byte value2) {
            addCriterion("exchange_id between", value1, value2, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotBetween(Byte value1, Byte value2) {
            addCriterion("exchange_id not between", value1, value2, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesIsNull() {
            addCriterion("exchange_CnNames is null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesIsNotNull() {
            addCriterion("exchange_CnNames is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesEqualTo(String value) {
            addCriterion("exchange_CnNames =", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesNotEqualTo(String value) {
            addCriterion("exchange_CnNames <>", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesGreaterThan(String value) {
            addCriterion("exchange_CnNames >", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_CnNames >=", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesLessThan(String value) {
            addCriterion("exchange_CnNames <", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesLessThanOrEqualTo(String value) {
            addCriterion("exchange_CnNames <=", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesLike(String value) {
            addCriterion("exchange_CnNames like", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesNotLike(String value) {
            addCriterion("exchange_CnNames not like", value, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesIn(List<String> values) {
            addCriterion("exchange_CnNames in", values, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesNotIn(List<String> values) {
            addCriterion("exchange_CnNames not in", values, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesBetween(String value1, String value2) {
            addCriterion("exchange_CnNames between", value1, value2, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnamesNotBetween(String value1, String value2) {
            addCriterion("exchange_CnNames not between", value1, value2, "exchangeCnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesIsNull() {
            addCriterion("exchange_EnNames is null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesIsNotNull() {
            addCriterion("exchange_EnNames is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesEqualTo(String value) {
            addCriterion("exchange_EnNames =", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesNotEqualTo(String value) {
            addCriterion("exchange_EnNames <>", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesGreaterThan(String value) {
            addCriterion("exchange_EnNames >", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_EnNames >=", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesLessThan(String value) {
            addCriterion("exchange_EnNames <", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesLessThanOrEqualTo(String value) {
            addCriterion("exchange_EnNames <=", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesLike(String value) {
            addCriterion("exchange_EnNames like", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesNotLike(String value) {
            addCriterion("exchange_EnNames not like", value, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesIn(List<String> values) {
            addCriterion("exchange_EnNames in", values, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesNotIn(List<String> values) {
            addCriterion("exchange_EnNames not in", values, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesBetween(String value1, String value2) {
            addCriterion("exchange_EnNames between", value1, value2, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnamesNotBetween(String value1, String value2) {
            addCriterion("exchange_EnNames not between", value1, value2, "exchangeEnnames");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameIsNull() {
            addCriterion("exchange_cn_name is null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameIsNotNull() {
            addCriterion("exchange_cn_name is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameEqualTo(String value) {
            addCriterion("exchange_cn_name =", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameNotEqualTo(String value) {
            addCriterion("exchange_cn_name <>", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameGreaterThan(String value) {
            addCriterion("exchange_cn_name >", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_cn_name >=", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameLessThan(String value) {
            addCriterion("exchange_cn_name <", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameLessThanOrEqualTo(String value) {
            addCriterion("exchange_cn_name <=", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameLike(String value) {
            addCriterion("exchange_cn_name like", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameNotLike(String value) {
            addCriterion("exchange_cn_name not like", value, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameIn(List<String> values) {
            addCriterion("exchange_cn_name in", values, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameNotIn(List<String> values) {
            addCriterion("exchange_cn_name not in", values, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameBetween(String value1, String value2) {
            addCriterion("exchange_cn_name between", value1, value2, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnNameNotBetween(String value1, String value2) {
            addCriterion("exchange_cn_name not between", value1, value2, "exchangeCnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameIsNull() {
            addCriterion("exchange_en_name is null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameIsNotNull() {
            addCriterion("exchange_en_name is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameEqualTo(String value) {
            addCriterion("exchange_en_name =", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameNotEqualTo(String value) {
            addCriterion("exchange_en_name <>", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameGreaterThan(String value) {
            addCriterion("exchange_en_name >", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_en_name >=", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameLessThan(String value) {
            addCriterion("exchange_en_name <", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameLessThanOrEqualTo(String value) {
            addCriterion("exchange_en_name <=", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameLike(String value) {
            addCriterion("exchange_en_name like", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameNotLike(String value) {
            addCriterion("exchange_en_name not like", value, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameIn(List<String> values) {
            addCriterion("exchange_en_name in", values, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameNotIn(List<String> values) {
            addCriterion("exchange_en_name not in", values, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameBetween(String value1, String value2) {
            addCriterion("exchange_en_name between", value1, value2, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeEnNameNotBetween(String value1, String value2) {
            addCriterion("exchange_en_name not between", value1, value2, "exchangeEnName");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortIsNull() {
            addCriterion("exchange_cn_short is null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortIsNotNull() {
            addCriterion("exchange_cn_short is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortEqualTo(String value) {
            addCriterion("exchange_cn_short =", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortNotEqualTo(String value) {
            addCriterion("exchange_cn_short <>", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortGreaterThan(String value) {
            addCriterion("exchange_cn_short >", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_cn_short >=", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortLessThan(String value) {
            addCriterion("exchange_cn_short <", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortLessThanOrEqualTo(String value) {
            addCriterion("exchange_cn_short <=", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortLike(String value) {
            addCriterion("exchange_cn_short like", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortNotLike(String value) {
            addCriterion("exchange_cn_short not like", value, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortIn(List<String> values) {
            addCriterion("exchange_cn_short in", values, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortNotIn(List<String> values) {
            addCriterion("exchange_cn_short not in", values, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortBetween(String value1, String value2) {
            addCriterion("exchange_cn_short between", value1, value2, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeCnShortNotBetween(String value1, String value2) {
            addCriterion("exchange_cn_short not between", value1, value2, "exchangeCnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortIsNull() {
            addCriterion("exchange_en_short is null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortIsNotNull() {
            addCriterion("exchange_en_short is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortEqualTo(String value) {
            addCriterion("exchange_en_short =", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortNotEqualTo(String value) {
            addCriterion("exchange_en_short <>", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortGreaterThan(String value) {
            addCriterion("exchange_en_short >", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortGreaterThanOrEqualTo(String value) {
            addCriterion("exchange_en_short >=", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortLessThan(String value) {
            addCriterion("exchange_en_short <", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortLessThanOrEqualTo(String value) {
            addCriterion("exchange_en_short <=", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortLike(String value) {
            addCriterion("exchange_en_short like", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortNotLike(String value) {
            addCriterion("exchange_en_short not like", value, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortIn(List<String> values) {
            addCriterion("exchange_en_short in", values, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortNotIn(List<String> values) {
            addCriterion("exchange_en_short not in", values, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortBetween(String value1, String value2) {
            addCriterion("exchange_en_short between", value1, value2, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andExchangeEnShortNotBetween(String value1, String value2) {
            addCriterion("exchange_en_short not between", value1, value2, "exchangeEnShort");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateIsNull() {
            addCriterion("appearmarket_date is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateIsNotNull() {
            addCriterion("appearmarket_date is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateEqualTo(Date value) {
            addCriterionForJDBCDate("appearmarket_date =", value, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("appearmarket_date <>", value, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateGreaterThan(Date value) {
            addCriterionForJDBCDate("appearmarket_date >", value, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("appearmarket_date >=", value, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateLessThan(Date value) {
            addCriterionForJDBCDate("appearmarket_date <", value, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("appearmarket_date <=", value, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateIn(List<Date> values) {
            addCriterionForJDBCDate("appearmarket_date in", values, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("appearmarket_date not in", values, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("appearmarket_date between", value1, value2, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("appearmarket_date not between", value1, value2, "appearmarketDate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeIsNull() {
            addCriterion("appearmarket_type is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeIsNotNull() {
            addCriterion("appearmarket_type is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeEqualTo(Byte value) {
            addCriterion("appearmarket_type =", value, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeNotEqualTo(Byte value) {
            addCriterion("appearmarket_type <>", value, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeGreaterThan(Byte value) {
            addCriterion("appearmarket_type >", value, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("appearmarket_type >=", value, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeLessThan(Byte value) {
            addCriterion("appearmarket_type <", value, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeLessThanOrEqualTo(Byte value) {
            addCriterion("appearmarket_type <=", value, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeIn(List<Byte> values) {
            addCriterion("appearmarket_type in", values, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeNotIn(List<Byte> values) {
            addCriterion("appearmarket_type not in", values, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeBetween(Byte value1, Byte value2) {
            addCriterion("appearmarket_type between", value1, value2, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andAppearmarketTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("appearmarket_type not between", value1, value2, "appearmarketType");
            return (Criteria) this;
        }

        public Criteria andInfoStatusIsNull() {
            addCriterion("info_status is null");
            return (Criteria) this;
        }

        public Criteria andInfoStatusIsNotNull() {
            addCriterion("info_status is not null");
            return (Criteria) this;
        }

        public Criteria andInfoStatusEqualTo(Byte value) {
            addCriterion("info_status =", value, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusNotEqualTo(Byte value) {
            addCriterion("info_status <>", value, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusGreaterThan(Byte value) {
            addCriterion("info_status >", value, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("info_status >=", value, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusLessThan(Byte value) {
            addCriterion("info_status <", value, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusLessThanOrEqualTo(Byte value) {
            addCriterion("info_status <=", value, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusIn(List<Byte> values) {
            addCriterion("info_status in", values, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusNotIn(List<Byte> values) {
            addCriterion("info_status not in", values, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusBetween(Byte value1, Byte value2) {
            addCriterion("info_status between", value1, value2, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andInfoStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("info_status not between", value1, value2, "infoStatus");
            return (Criteria) this;
        }

        public Criteria andStockTypeIsNull() {
            addCriterion("stock_type is null");
            return (Criteria) this;
        }

        public Criteria andStockTypeIsNotNull() {
            addCriterion("stock_type is not null");
            return (Criteria) this;
        }

        public Criteria andStockTypeEqualTo(Byte value) {
            addCriterion("stock_type =", value, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeNotEqualTo(Byte value) {
            addCriterion("stock_type <>", value, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeGreaterThan(Byte value) {
            addCriterion("stock_type >", value, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("stock_type >=", value, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeLessThan(Byte value) {
            addCriterion("stock_type <", value, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeLessThanOrEqualTo(Byte value) {
            addCriterion("stock_type <=", value, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeIn(List<Byte> values) {
            addCriterion("stock_type in", values, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeNotIn(List<Byte> values) {
            addCriterion("stock_type not in", values, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeBetween(Byte value1, Byte value2) {
            addCriterion("stock_type between", value1, value2, "stockType");
            return (Criteria) this;
        }

        public Criteria andStockTypeNotBetween(Byte value1, Byte value2) {
            addCriterion("stock_type not between", value1, value2, "stockType");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}