package cn.com.chinaventure.trans.cvent.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_personage;

import java.util.List;

/**
 * @author 
 *
 */
public interface Enterprise_credit_personageService {
	
	/**
     * @api saveEnterprise_credit_personage 新增enterprise_credit_personage
     * @apiGroup enterprise_credit_personage管理
     * @apiName  新增enterprise_credit_personage记录
     * @apiDescription 全量插入enterprise_credit_personage记录
     * @apiParam {Enterprise_credit_personage} enterprise_credit_personage enterprise_credit_personage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise_credit_personage(Enterprise_credit_personage enterprise_credit_personage);
	
	/**
     * @api updateEnterprise_credit_personage 修改enterprise_credit_personage
     * @apiGroup enterprise_credit_personage管理
     * @apiName  修改enterprise_credit_personage记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise_credit_personage} enterprise_credit_personage enterprise_credit_personage对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise_credit_personage(Enterprise_credit_personage enterprise_credit_personage);
	
	/**
     * @api getEnterprise_credit_personageById 根据enterprise_credit_personageid查询详情
     * @apiGroup enterprise_credit_personage管理
     * @apiName  查询enterprise_credit_personage详情
     * @apiDescription 根据主键id查询enterprise_credit_personage详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Enterprise_credit_personage enterprise_credit_personage实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise_credit_personage getEnterprise_credit_personageById(Long id);
	
	/**
     * @api getEnterprise_credit_personageList 根据enterprise_credit_personage条件查询列表
     * @apiGroup enterprise_credit_personage管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise_credit_personage} enterprise_credit_personage  实体条件
     * @apiSuccess List<Enterprise_credit_personage> enterprise_credit_personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_credit_personage> getEnterprise_credit_personageList(Enterprise_credit_personage enterprise_credit_personage);
	

	/**
     * @api getEnterprise_credit_personageListByParam 根据enterprise_credit_personage条件查询列表（含排序）
     * @apiGroup enterprise_credit_personage管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise_credit_personage} enterprise_credit_personage  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise_credit_personage> enterprise_credit_personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_credit_personage> getEnterprise_credit_personageListByParam(Enterprise_credit_personage enterprise_credit_personage, String orderByStr);
	
	
	/**
     * @api findEnterprise_credit_personagePageByParam 根据enterprise_credit_personage条件查询列表（分页）
     * @apiGroup enterprise_credit_personage管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise_credit_personage} enterprise_credit_personage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise_credit_personage> enterprise_credit_personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_credit_personage> findEnterprise_credit_personagePageByParam(Enterprise_credit_personage enterprise_credit_personage, PageParam pageParam); 
	
	/**
     * @api findEnterprise_credit_personagePageByParam 根据enterprise_credit_personage条件查询列表（分页，含排序）
     * @apiGroup enterprise_credit_personage管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise_credit_personage} enterprise_credit_personage  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise_credit_personage> enterprise_credit_personage实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_credit_personage> findEnterprise_credit_personagePageByParam(Enterprise_credit_personage enterprise_credit_personage, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterprise_credit_personageById 根据enterprise_credit_personage的id删除(物理删除)
     * @apiGroup enterprise_credit_personage管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterprise_credit_personageById(Long id);

}

