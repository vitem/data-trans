package cn.com.chinaventure.trans.cvent.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Enterprise_manageteamExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public Enterprise_manageteamExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andEnterpriseMtIdIsNull() {
            addCriterion("Enterprise_MT_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdIsNotNull() {
            addCriterion("Enterprise_MT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdEqualTo(Long value) {
            addCriterion("Enterprise_MT_ID =", value, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdNotEqualTo(Long value) {
            addCriterion("Enterprise_MT_ID <>", value, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdGreaterThan(Long value) {
            addCriterion("Enterprise_MT_ID >", value, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Enterprise_MT_ID >=", value, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdLessThan(Long value) {
            addCriterion("Enterprise_MT_ID <", value, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdLessThanOrEqualTo(Long value) {
            addCriterion("Enterprise_MT_ID <=", value, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdIn(List<Long> values) {
            addCriterion("Enterprise_MT_ID in", values, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdNotIn(List<Long> values) {
            addCriterion("Enterprise_MT_ID not in", values, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdBetween(Long value1, Long value2) {
            addCriterion("Enterprise_MT_ID between", value1, value2, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtIdNotBetween(Long value1, Long value2) {
            addCriterion("Enterprise_MT_ID not between", value1, value2, "enterpriseMtId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidIsNull() {
            addCriterion("Enterprise_MT_PersonageId is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidIsNotNull() {
            addCriterion("Enterprise_MT_PersonageId is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidEqualTo(Long value) {
            addCriterion("Enterprise_MT_PersonageId =", value, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidNotEqualTo(Long value) {
            addCriterion("Enterprise_MT_PersonageId <>", value, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidGreaterThan(Long value) {
            addCriterion("Enterprise_MT_PersonageId >", value, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidGreaterThanOrEqualTo(Long value) {
            addCriterion("Enterprise_MT_PersonageId >=", value, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidLessThan(Long value) {
            addCriterion("Enterprise_MT_PersonageId <", value, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidLessThanOrEqualTo(Long value) {
            addCriterion("Enterprise_MT_PersonageId <=", value, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidIn(List<Long> values) {
            addCriterion("Enterprise_MT_PersonageId in", values, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidNotIn(List<Long> values) {
            addCriterion("Enterprise_MT_PersonageId not in", values, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidBetween(Long value1, Long value2) {
            addCriterion("Enterprise_MT_PersonageId between", value1, value2, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonageidNotBetween(Long value1, Long value2) {
            addCriterion("Enterprise_MT_PersonageId not between", value1, value2, "enterpriseMtPersonageid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameIsNull() {
            addCriterion("Enterprise_MT_PersonageName is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameIsNotNull() {
            addCriterion("Enterprise_MT_PersonageName is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameEqualTo(String value) {
            addCriterion("Enterprise_MT_PersonageName =", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameNotEqualTo(String value) {
            addCriterion("Enterprise_MT_PersonageName <>", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameGreaterThan(String value) {
            addCriterion("Enterprise_MT_PersonageName >", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_MT_PersonageName >=", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameLessThan(String value) {
            addCriterion("Enterprise_MT_PersonageName <", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_MT_PersonageName <=", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameLike(String value) {
            addCriterion("Enterprise_MT_PersonageName like", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameNotLike(String value) {
            addCriterion("Enterprise_MT_PersonageName not like", value, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameIn(List<String> values) {
            addCriterion("Enterprise_MT_PersonageName in", values, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameNotIn(List<String> values) {
            addCriterion("Enterprise_MT_PersonageName not in", values, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameBetween(String value1, String value2) {
            addCriterion("Enterprise_MT_PersonageName between", value1, value2, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPersonagenameNotBetween(String value1, String value2) {
            addCriterion("Enterprise_MT_PersonageName not between", value1, value2, "enterpriseMtPersonagename");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeIsNull() {
            addCriterion("Enterprise_MT_Leaveoffice is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeIsNotNull() {
            addCriterion("Enterprise_MT_Leaveoffice is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeEqualTo(Long value) {
            addCriterion("Enterprise_MT_Leaveoffice =", value, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeNotEqualTo(Long value) {
            addCriterion("Enterprise_MT_Leaveoffice <>", value, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeGreaterThan(Long value) {
            addCriterion("Enterprise_MT_Leaveoffice >", value, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeGreaterThanOrEqualTo(Long value) {
            addCriterion("Enterprise_MT_Leaveoffice >=", value, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeLessThan(Long value) {
            addCriterion("Enterprise_MT_Leaveoffice <", value, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeLessThanOrEqualTo(Long value) {
            addCriterion("Enterprise_MT_Leaveoffice <=", value, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeIn(List<Long> values) {
            addCriterion("Enterprise_MT_Leaveoffice in", values, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeNotIn(List<Long> values) {
            addCriterion("Enterprise_MT_Leaveoffice not in", values, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeBetween(Long value1, Long value2) {
            addCriterion("Enterprise_MT_Leaveoffice between", value1, value2, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficeNotBetween(Long value1, Long value2) {
            addCriterion("Enterprise_MT_Leaveoffice not between", value1, value2, "enterpriseMtLeaveoffice");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateIsNull() {
            addCriterion("Enterprise_MT_EnterdutyDate is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateIsNotNull() {
            addCriterion("Enterprise_MT_EnterdutyDate is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate =", value, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateNotEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate <>", value, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateGreaterThan(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate >", value, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate >=", value, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateLessThan(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate <", value, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate <=", value, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate in", values, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateNotIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate not in", values, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate between", value1, value2, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtEnterdutydateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_MT_EnterdutyDate not between", value1, value2, "enterpriseMtEnterdutydate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateIsNull() {
            addCriterion("Enterprise_MT_LeaveofficeDate is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateIsNotNull() {
            addCriterion("Enterprise_MT_LeaveofficeDate is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate =", value, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateNotEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate <>", value, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateGreaterThan(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate >", value, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate >=", value, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateLessThan(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate <", value, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate <=", value, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate in", values, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateNotIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate not in", values, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate between", value1, value2, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtLeaveofficedateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_MT_LeaveofficeDate not between", value1, value2, "enterpriseMtLeaveofficedate");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("Update_Time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("Update_Time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("Update_Time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("Update_Time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("Update_Time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Update_Time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("Update_Time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Update_Time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("Update_Time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("Update_Time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("Update_Time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Update_Time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNull() {
            addCriterion("Behavior is null");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNotNull() {
            addCriterion("Behavior is not null");
            return (Criteria) this;
        }

        public Criteria andBehaviorEqualTo(String value) {
            addCriterion("Behavior =", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotEqualTo(String value) {
            addCriterion("Behavior <>", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThan(String value) {
            addCriterion("Behavior >", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThanOrEqualTo(String value) {
            addCriterion("Behavior >=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThan(String value) {
            addCriterion("Behavior <", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThanOrEqualTo(String value) {
            addCriterion("Behavior <=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLike(String value) {
            addCriterion("Behavior like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotLike(String value) {
            addCriterion("Behavior not like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorIn(List<String> values) {
            addCriterion("Behavior in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotIn(List<String> values) {
            addCriterion("Behavior not in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorBetween(String value1, String value2) {
            addCriterion("Behavior between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotBetween(String value1, String value2) {
            addCriterion("Behavior not between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("Enterprise_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("Enterprise_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(Long value) {
            addCriterion("Enterprise_ID =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(Long value) {
            addCriterion("Enterprise_ID <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(Long value) {
            addCriterion("Enterprise_ID >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Enterprise_ID >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(Long value) {
            addCriterion("Enterprise_ID <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(Long value) {
            addCriterion("Enterprise_ID <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<Long> values) {
            addCriterion("Enterprise_ID in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<Long> values) {
            addCriterion("Enterprise_ID not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(Long value1, Long value2) {
            addCriterion("Enterprise_ID between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(Long value1, Long value2) {
            addCriterion("Enterprise_ID not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionIsNull() {
            addCriterion("Enterprise_MT_Position is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionIsNotNull() {
            addCriterion("Enterprise_MT_Position is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionEqualTo(String value) {
            addCriterion("Enterprise_MT_Position =", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionNotEqualTo(String value) {
            addCriterion("Enterprise_MT_Position <>", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionGreaterThan(String value) {
            addCriterion("Enterprise_MT_Position >", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_MT_Position >=", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionLessThan(String value) {
            addCriterion("Enterprise_MT_Position <", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_MT_Position <=", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionLike(String value) {
            addCriterion("Enterprise_MT_Position like", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionNotLike(String value) {
            addCriterion("Enterprise_MT_Position not like", value, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionIn(List<String> values) {
            addCriterion("Enterprise_MT_Position in", values, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionNotIn(List<String> values) {
            addCriterion("Enterprise_MT_Position not in", values, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionBetween(String value1, String value2) {
            addCriterion("Enterprise_MT_Position between", value1, value2, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andEnterpriseMtPositionNotBetween(String value1, String value2) {
            addCriterion("Enterprise_MT_Position not between", value1, value2, "enterpriseMtPosition");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNull() {
            addCriterion("ordered is null");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNotNull() {
            addCriterion("ordered is not null");
            return (Criteria) this;
        }

        public Criteria andOrderedEqualTo(Long value) {
            addCriterion("ordered =", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotEqualTo(Long value) {
            addCriterion("ordered <>", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThan(Long value) {
            addCriterion("ordered >", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThanOrEqualTo(Long value) {
            addCriterion("ordered >=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThan(Long value) {
            addCriterion("ordered <", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThanOrEqualTo(Long value) {
            addCriterion("ordered <=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedIn(List<Long> values) {
            addCriterion("ordered in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotIn(List<Long> values) {
            addCriterion("ordered not in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedBetween(Long value1, Long value2) {
            addCriterion("ordered between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotBetween(Long value1, Long value2) {
            addCriterion("ordered not between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNull() {
            addCriterion("Module_ID is null");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNotNull() {
            addCriterion("Module_ID is not null");
            return (Criteria) this;
        }

        public Criteria andModuleIdEqualTo(Long value) {
            addCriterion("Module_ID =", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotEqualTo(Long value) {
            addCriterion("Module_ID <>", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThan(Long value) {
            addCriterion("Module_ID >", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Module_ID >=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThan(Long value) {
            addCriterion("Module_ID <", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThanOrEqualTo(Long value) {
            addCriterion("Module_ID <=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdIn(List<Long> values) {
            addCriterion("Module_ID in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotIn(List<Long> values) {
            addCriterion("Module_ID not in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdBetween(Long value1, Long value2) {
            addCriterion("Module_ID between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotBetween(Long value1, Long value2) {
            addCriterion("Module_ID not between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("Role is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("Role is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("Role =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("Role <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("Role >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("Role >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("Role <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("Role <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("Role like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("Role not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("Role in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("Role not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("Role between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("Role not between", value1, value2, "role");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}