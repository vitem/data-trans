package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.EnterpriseAppinfo;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseAppinfoExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnterpriseAppinfoMapper {
    int countByExample(EnterpriseAppinfoExample example);

    int deleteByExample(EnterpriseAppinfoExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(EnterpriseAppinfo record);

    int insertSelective(EnterpriseAppinfo record);

    List<EnterpriseAppinfo> selectByExample(EnterpriseAppinfoExample example);

    EnterpriseAppinfo selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") EnterpriseAppinfo record, @Param("example") EnterpriseAppinfoExample example);

    int updateByExample(@Param("record") EnterpriseAppinfo record, @Param("example") EnterpriseAppinfoExample example);

    int updateByPrimaryKeySelective(EnterpriseAppinfo record);

    int updateByPrimaryKey(EnterpriseAppinfo record);
}