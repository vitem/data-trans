package cn.com.chinaventure.trans.cvent.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Enterprise_credit_infoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public Enterprise_credit_infoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andCreditIdIsNull() {
            addCriterion("Credit_ID is null");
            return (Criteria) this;
        }

        public Criteria andCreditIdIsNotNull() {
            addCriterion("Credit_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCreditIdEqualTo(Long value) {
            addCriterion("Credit_ID =", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdNotEqualTo(Long value) {
            addCriterion("Credit_ID <>", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdGreaterThan(Long value) {
            addCriterion("Credit_ID >", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Credit_ID >=", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdLessThan(Long value) {
            addCriterion("Credit_ID <", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdLessThanOrEqualTo(Long value) {
            addCriterion("Credit_ID <=", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdIn(List<Long> values) {
            addCriterion("Credit_ID in", values, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdNotIn(List<Long> values) {
            addCriterion("Credit_ID not in", values, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdBetween(Long value1, Long value2) {
            addCriterion("Credit_ID between", value1, value2, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdNotBetween(Long value1, Long value2) {
            addCriterion("Credit_ID not between", value1, value2, "creditId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("Enterprise_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("Enterprise_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(Long value) {
            addCriterion("Enterprise_ID =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(Long value) {
            addCriterion("Enterprise_ID <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(Long value) {
            addCriterion("Enterprise_ID >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Enterprise_ID >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(Long value) {
            addCriterion("Enterprise_ID <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(Long value) {
            addCriterion("Enterprise_ID <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<Long> values) {
            addCriterion("Enterprise_ID in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<Long> values) {
            addCriterion("Enterprise_ID not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(Long value1, Long value2) {
            addCriterion("Enterprise_ID between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(Long value1, Long value2) {
            addCriterion("Enterprise_ID not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNull() {
            addCriterion("Province is null");
            return (Criteria) this;
        }

        public Criteria andProvinceIsNotNull() {
            addCriterion("Province is not null");
            return (Criteria) this;
        }

        public Criteria andProvinceEqualTo(String value) {
            addCriterion("Province =", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotEqualTo(String value) {
            addCriterion("Province <>", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThan(String value) {
            addCriterion("Province >", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceGreaterThanOrEqualTo(String value) {
            addCriterion("Province >=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThan(String value) {
            addCriterion("Province <", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLessThanOrEqualTo(String value) {
            addCriterion("Province <=", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceLike(String value) {
            addCriterion("Province like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotLike(String value) {
            addCriterion("Province not like", value, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceIn(List<String> values) {
            addCriterion("Province in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotIn(List<String> values) {
            addCriterion("Province not in", values, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceBetween(String value1, String value2) {
            addCriterion("Province between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andProvinceNotBetween(String value1, String value2) {
            addCriterion("Province not between", value1, value2, "province");
            return (Criteria) this;
        }

        public Criteria andRegisterNumIsNull() {
            addCriterion("Register_Num is null");
            return (Criteria) this;
        }

        public Criteria andRegisterNumIsNotNull() {
            addCriterion("Register_Num is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterNumEqualTo(String value) {
            addCriterion("Register_Num =", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotEqualTo(String value) {
            addCriterion("Register_Num <>", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumGreaterThan(String value) {
            addCriterion("Register_Num >", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumGreaterThanOrEqualTo(String value) {
            addCriterion("Register_Num >=", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumLessThan(String value) {
            addCriterion("Register_Num <", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumLessThanOrEqualTo(String value) {
            addCriterion("Register_Num <=", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumLike(String value) {
            addCriterion("Register_Num like", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotLike(String value) {
            addCriterion("Register_Num not like", value, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumIn(List<String> values) {
            addCriterion("Register_Num in", values, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotIn(List<String> values) {
            addCriterion("Register_Num not in", values, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumBetween(String value1, String value2) {
            addCriterion("Register_Num between", value1, value2, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNumNotBetween(String value1, String value2) {
            addCriterion("Register_Num not between", value1, value2, "registerNum");
            return (Criteria) this;
        }

        public Criteria andRegisterNameIsNull() {
            addCriterion("Register_Name is null");
            return (Criteria) this;
        }

        public Criteria andRegisterNameIsNotNull() {
            addCriterion("Register_Name is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterNameEqualTo(String value) {
            addCriterion("Register_Name =", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameNotEqualTo(String value) {
            addCriterion("Register_Name <>", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameGreaterThan(String value) {
            addCriterion("Register_Name >", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameGreaterThanOrEqualTo(String value) {
            addCriterion("Register_Name >=", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameLessThan(String value) {
            addCriterion("Register_Name <", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameLessThanOrEqualTo(String value) {
            addCriterion("Register_Name <=", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameLike(String value) {
            addCriterion("Register_Name like", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameNotLike(String value) {
            addCriterion("Register_Name not like", value, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameIn(List<String> values) {
            addCriterion("Register_Name in", values, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameNotIn(List<String> values) {
            addCriterion("Register_Name not in", values, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameBetween(String value1, String value2) {
            addCriterion("Register_Name between", value1, value2, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterNameNotBetween(String value1, String value2) {
            addCriterion("Register_Name not between", value1, value2, "registerName");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIsNull() {
            addCriterion("Register_Type is null");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIsNotNull() {
            addCriterion("Register_Type is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeEqualTo(String value) {
            addCriterion("Register_Type =", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotEqualTo(String value) {
            addCriterion("Register_Type <>", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeGreaterThan(String value) {
            addCriterion("Register_Type >", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeGreaterThanOrEqualTo(String value) {
            addCriterion("Register_Type >=", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLessThan(String value) {
            addCriterion("Register_Type <", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLessThanOrEqualTo(String value) {
            addCriterion("Register_Type <=", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeLike(String value) {
            addCriterion("Register_Type like", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotLike(String value) {
            addCriterion("Register_Type not like", value, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeIn(List<String> values) {
            addCriterion("Register_Type in", values, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotIn(List<String> values) {
            addCriterion("Register_Type not in", values, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeBetween(String value1, String value2) {
            addCriterion("Register_Type between", value1, value2, "registerType");
            return (Criteria) this;
        }

        public Criteria andRegisterTypeNotBetween(String value1, String value2) {
            addCriterion("Register_Type not between", value1, value2, "registerType");
            return (Criteria) this;
        }

        public Criteria andSetupdateIsNull() {
            addCriterion("SetUpDate is null");
            return (Criteria) this;
        }

        public Criteria andSetupdateIsNotNull() {
            addCriterion("SetUpDate is not null");
            return (Criteria) this;
        }

        public Criteria andSetupdateEqualTo(Date value) {
            addCriterionForJDBCDate("SetUpDate =", value, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("SetUpDate <>", value, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateGreaterThan(Date value) {
            addCriterionForJDBCDate("SetUpDate >", value, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("SetUpDate >=", value, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateLessThan(Date value) {
            addCriterionForJDBCDate("SetUpDate <", value, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("SetUpDate <=", value, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateIn(List<Date> values) {
            addCriterionForJDBCDate("SetUpDate in", values, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("SetUpDate not in", values, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("SetUpDate between", value1, value2, "setupdate");
            return (Criteria) this;
        }

        public Criteria andSetupdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("SetUpDate not between", value1, value2, "setupdate");
            return (Criteria) this;
        }

        public Criteria andLegalpersonIsNull() {
            addCriterion("LegalPerson is null");
            return (Criteria) this;
        }

        public Criteria andLegalpersonIsNotNull() {
            addCriterion("LegalPerson is not null");
            return (Criteria) this;
        }

        public Criteria andLegalpersonEqualTo(String value) {
            addCriterion("LegalPerson =", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonNotEqualTo(String value) {
            addCriterion("LegalPerson <>", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonGreaterThan(String value) {
            addCriterion("LegalPerson >", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonGreaterThanOrEqualTo(String value) {
            addCriterion("LegalPerson >=", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonLessThan(String value) {
            addCriterion("LegalPerson <", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonLessThanOrEqualTo(String value) {
            addCriterion("LegalPerson <=", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonLike(String value) {
            addCriterion("LegalPerson like", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonNotLike(String value) {
            addCriterion("LegalPerson not like", value, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonIn(List<String> values) {
            addCriterion("LegalPerson in", values, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonNotIn(List<String> values) {
            addCriterion("LegalPerson not in", values, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonBetween(String value1, String value2) {
            addCriterion("LegalPerson between", value1, value2, "legalperson");
            return (Criteria) this;
        }

        public Criteria andLegalpersonNotBetween(String value1, String value2) {
            addCriterion("LegalPerson not between", value1, value2, "legalperson");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyIsNull() {
            addCriterion("RegisterMoney is null");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyIsNotNull() {
            addCriterion("RegisterMoney is not null");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyEqualTo(Double value) {
            addCriterion("RegisterMoney =", value, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyNotEqualTo(Double value) {
            addCriterion("RegisterMoney <>", value, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyGreaterThan(Double value) {
            addCriterion("RegisterMoney >", value, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("RegisterMoney >=", value, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyLessThan(Double value) {
            addCriterion("RegisterMoney <", value, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyLessThanOrEqualTo(Double value) {
            addCriterion("RegisterMoney <=", value, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyIn(List<Double> values) {
            addCriterion("RegisterMoney in", values, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyNotIn(List<Double> values) {
            addCriterion("RegisterMoney not in", values, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyBetween(Double value1, Double value2) {
            addCriterion("RegisterMoney between", value1, value2, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyNotBetween(Double value1, Double value2) {
            addCriterion("RegisterMoney not between", value1, value2, "registermoney");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitIsNull() {
            addCriterion("RegisterMoney_Unit is null");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitIsNotNull() {
            addCriterion("RegisterMoney_Unit is not null");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitEqualTo(String value) {
            addCriterion("RegisterMoney_Unit =", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitNotEqualTo(String value) {
            addCriterion("RegisterMoney_Unit <>", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitGreaterThan(String value) {
            addCriterion("RegisterMoney_Unit >", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitGreaterThanOrEqualTo(String value) {
            addCriterion("RegisterMoney_Unit >=", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitLessThan(String value) {
            addCriterion("RegisterMoney_Unit <", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitLessThanOrEqualTo(String value) {
            addCriterion("RegisterMoney_Unit <=", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitLike(String value) {
            addCriterion("RegisterMoney_Unit like", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitNotLike(String value) {
            addCriterion("RegisterMoney_Unit not like", value, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitIn(List<String> values) {
            addCriterion("RegisterMoney_Unit in", values, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitNotIn(List<String> values) {
            addCriterion("RegisterMoney_Unit not in", values, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitBetween(String value1, String value2) {
            addCriterion("RegisterMoney_Unit between", value1, value2, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyUnitNotBetween(String value1, String value2) {
            addCriterion("RegisterMoney_Unit not between", value1, value2, "registermoneyUnit");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyIsNull() {
            addCriterion("RegisterMoney_Currency is null");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyIsNotNull() {
            addCriterion("RegisterMoney_Currency is not null");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyEqualTo(String value) {
            addCriterion("RegisterMoney_Currency =", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyNotEqualTo(String value) {
            addCriterion("RegisterMoney_Currency <>", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyGreaterThan(String value) {
            addCriterion("RegisterMoney_Currency >", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("RegisterMoney_Currency >=", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyLessThan(String value) {
            addCriterion("RegisterMoney_Currency <", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyLessThanOrEqualTo(String value) {
            addCriterion("RegisterMoney_Currency <=", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyLike(String value) {
            addCriterion("RegisterMoney_Currency like", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyNotLike(String value) {
            addCriterion("RegisterMoney_Currency not like", value, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyIn(List<String> values) {
            addCriterion("RegisterMoney_Currency in", values, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyNotIn(List<String> values) {
            addCriterion("RegisterMoney_Currency not in", values, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyBetween(String value1, String value2) {
            addCriterion("RegisterMoney_Currency between", value1, value2, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegistermoneyCurrencyNotBetween(String value1, String value2) {
            addCriterion("RegisterMoney_Currency not between", value1, value2, "registermoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIsNull() {
            addCriterion("Register_Address is null");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIsNotNull() {
            addCriterion("Register_Address is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressEqualTo(String value) {
            addCriterion("Register_Address =", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotEqualTo(String value) {
            addCriterion("Register_Address <>", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressGreaterThan(String value) {
            addCriterion("Register_Address >", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressGreaterThanOrEqualTo(String value) {
            addCriterion("Register_Address >=", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLessThan(String value) {
            addCriterion("Register_Address <", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLessThanOrEqualTo(String value) {
            addCriterion("Register_Address <=", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressLike(String value) {
            addCriterion("Register_Address like", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotLike(String value) {
            addCriterion("Register_Address not like", value, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressIn(List<String> values) {
            addCriterion("Register_Address in", values, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotIn(List<String> values) {
            addCriterion("Register_Address not in", values, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressBetween(String value1, String value2) {
            addCriterion("Register_Address between", value1, value2, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andRegisterAddressNotBetween(String value1, String value2) {
            addCriterion("Register_Address not between", value1, value2, "registerAddress");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateIsNull() {
            addCriterion("OpenStart_Date is null");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateIsNotNull() {
            addCriterion("OpenStart_Date is not null");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateEqualTo(Date value) {
            addCriterionForJDBCDate("OpenStart_Date =", value, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("OpenStart_Date <>", value, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateGreaterThan(Date value) {
            addCriterionForJDBCDate("OpenStart_Date >", value, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("OpenStart_Date >=", value, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateLessThan(Date value) {
            addCriterionForJDBCDate("OpenStart_Date <", value, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("OpenStart_Date <=", value, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateIn(List<Date> values) {
            addCriterionForJDBCDate("OpenStart_Date in", values, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("OpenStart_Date not in", values, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("OpenStart_Date between", value1, value2, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenstartDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("OpenStart_Date not between", value1, value2, "openstartDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateIsNull() {
            addCriterion("OpenEnd_Date is null");
            return (Criteria) this;
        }

        public Criteria andOpenendDateIsNotNull() {
            addCriterion("OpenEnd_Date is not null");
            return (Criteria) this;
        }

        public Criteria andOpenendDateEqualTo(Date value) {
            addCriterionForJDBCDate("OpenEnd_Date =", value, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("OpenEnd_Date <>", value, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateGreaterThan(Date value) {
            addCriterionForJDBCDate("OpenEnd_Date >", value, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("OpenEnd_Date >=", value, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateLessThan(Date value) {
            addCriterionForJDBCDate("OpenEnd_Date <", value, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("OpenEnd_Date <=", value, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateIn(List<Date> values) {
            addCriterionForJDBCDate("OpenEnd_Date in", values, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("OpenEnd_Date not in", values, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("OpenEnd_Date between", value1, value2, "openendDate");
            return (Criteria) this;
        }

        public Criteria andOpenendDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("OpenEnd_Date not between", value1, value2, "openendDate");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganIsNull() {
            addCriterion("Register_Organ is null");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganIsNotNull() {
            addCriterion("Register_Organ is not null");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganEqualTo(String value) {
            addCriterion("Register_Organ =", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganNotEqualTo(String value) {
            addCriterion("Register_Organ <>", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganGreaterThan(String value) {
            addCriterion("Register_Organ >", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganGreaterThanOrEqualTo(String value) {
            addCriterion("Register_Organ >=", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganLessThan(String value) {
            addCriterion("Register_Organ <", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganLessThanOrEqualTo(String value) {
            addCriterion("Register_Organ <=", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganLike(String value) {
            addCriterion("Register_Organ like", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganNotLike(String value) {
            addCriterion("Register_Organ not like", value, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganIn(List<String> values) {
            addCriterion("Register_Organ in", values, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganNotIn(List<String> values) {
            addCriterion("Register_Organ not in", values, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganBetween(String value1, String value2) {
            addCriterion("Register_Organ between", value1, value2, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andRegisterOrganNotBetween(String value1, String value2) {
            addCriterion("Register_Organ not between", value1, value2, "registerOrgan");
            return (Criteria) this;
        }

        public Criteria andLicensedateIsNull() {
            addCriterion("LicenseDate is null");
            return (Criteria) this;
        }

        public Criteria andLicensedateIsNotNull() {
            addCriterion("LicenseDate is not null");
            return (Criteria) this;
        }

        public Criteria andLicensedateEqualTo(Date value) {
            addCriterionForJDBCDate("LicenseDate =", value, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateNotEqualTo(Date value) {
            addCriterionForJDBCDate("LicenseDate <>", value, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateGreaterThan(Date value) {
            addCriterionForJDBCDate("LicenseDate >", value, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LicenseDate >=", value, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateLessThan(Date value) {
            addCriterionForJDBCDate("LicenseDate <", value, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("LicenseDate <=", value, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateIn(List<Date> values) {
            addCriterionForJDBCDate("LicenseDate in", values, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateNotIn(List<Date> values) {
            addCriterionForJDBCDate("LicenseDate not in", values, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LicenseDate between", value1, value2, "licensedate");
            return (Criteria) this;
        }

        public Criteria andLicensedateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("LicenseDate not between", value1, value2, "licensedate");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIsNull() {
            addCriterion("Business_State is null");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIsNotNull() {
            addCriterion("Business_State is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessStateEqualTo(String value) {
            addCriterion("Business_State =", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotEqualTo(String value) {
            addCriterion("Business_State <>", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateGreaterThan(String value) {
            addCriterion("Business_State >", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateGreaterThanOrEqualTo(String value) {
            addCriterion("Business_State >=", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLessThan(String value) {
            addCriterion("Business_State <", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLessThanOrEqualTo(String value) {
            addCriterion("Business_State <=", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateLike(String value) {
            addCriterion("Business_State like", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotLike(String value) {
            addCriterion("Business_State not like", value, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateIn(List<String> values) {
            addCriterion("Business_State in", values, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotIn(List<String> values) {
            addCriterion("Business_State not in", values, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateBetween(String value1, String value2) {
            addCriterion("Business_State between", value1, value2, "businessState");
            return (Criteria) this;
        }

        public Criteria andBusinessStateNotBetween(String value1, String value2) {
            addCriterion("Business_State not between", value1, value2, "businessState");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andQuerykeyIsNull() {
            addCriterion("QueryKey is null");
            return (Criteria) this;
        }

        public Criteria andQuerykeyIsNotNull() {
            addCriterion("QueryKey is not null");
            return (Criteria) this;
        }

        public Criteria andQuerykeyEqualTo(String value) {
            addCriterion("QueryKey =", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyNotEqualTo(String value) {
            addCriterion("QueryKey <>", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyGreaterThan(String value) {
            addCriterion("QueryKey >", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyGreaterThanOrEqualTo(String value) {
            addCriterion("QueryKey >=", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyLessThan(String value) {
            addCriterion("QueryKey <", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyLessThanOrEqualTo(String value) {
            addCriterion("QueryKey <=", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyLike(String value) {
            addCriterion("QueryKey like", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyNotLike(String value) {
            addCriterion("QueryKey not like", value, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyIn(List<String> values) {
            addCriterion("QueryKey in", values, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyNotIn(List<String> values) {
            addCriterion("QueryKey not in", values, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyBetween(String value1, String value2) {
            addCriterion("QueryKey between", value1, value2, "querykey");
            return (Criteria) this;
        }

        public Criteria andQuerykeyNotBetween(String value1, String value2) {
            addCriterion("QueryKey not between", value1, value2, "querykey");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}