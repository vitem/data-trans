package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteam;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteamExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteamExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_manageteamMapper;
import cn.com.chinaventure.trans.cvent.service.Enterprise_manageteamService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("enterprise_manageteamService")
public class Enterprise_manageteamServiceImpl implements Enterprise_manageteamService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_manageteamServiceImpl.class);

	@Resource
	private Enterprise_manageteamMapper enterprise_manageteamMapper;


	@Override
	public void saveEnterprise_manageteam(Enterprise_manageteam enterprise_manageteam) {
		if (null == enterprise_manageteam) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_manageteamMapper.insertSelective(enterprise_manageteam);
	}

	@Override
	public void updateEnterprise_manageteam(Enterprise_manageteam enterprise_manageteam) {
		if (null == enterprise_manageteam) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_manageteamMapper.updateByPrimaryKeySelective(enterprise_manageteam);
	}

	@Override
	public Enterprise_manageteam getEnterprise_manageteamById(Long id) {
		return enterprise_manageteamMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_manageteam> getEnterprise_manageteamList(Enterprise_manageteam enterprise_manageteam) {
		return this.getEnterprise_manageteamListByParam(enterprise_manageteam, null);
	}

	@Override
	public List<Enterprise_manageteam> getEnterprise_manageteamListByParam(Enterprise_manageteam enterprise_manageteam, String orderByStr) {
		Enterprise_manageteamExample example = new Enterprise_manageteamExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		if(enterprise_manageteam.getEnterpriseId()!=null){
			criteria.andEnterpriseIdEqualTo(enterprise_manageteam.getEnterpriseId());
		}

		List<Enterprise_manageteam> enterprise_manageteamList = this.enterprise_manageteamMapper.selectByExample(example);
		return enterprise_manageteamList;
	}

	@Override
	public PageResult<Enterprise_manageteam> findEnterprise_manageteamPageByParam(Enterprise_manageteam enterprise_manageteam, PageParam pageParam) {
		return this.findEnterprise_manageteamPageByParam(enterprise_manageteam, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_manageteam> findEnterprise_manageteamPageByParam(Enterprise_manageteam enterprise_manageteam, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_manageteam> pageResult = new PageResult<Enterprise_manageteam>();

		Enterprise_manageteamExample example = new Enterprise_manageteamExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_manageteam> list = null;// enterprise_manageteamMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}


	@Override
	public Enterprise_manageteam getEnterprise_manageteamByEnpId(Integer cvEnterpriseId) {
		Enterprise_manageteam enterpriseManageteam = new Enterprise_manageteam();
		enterpriseManageteam.setEnterpriseId((long)cvEnterpriseId);
		List<Enterprise_manageteam> list = getEnterprise_manageteamList(enterpriseManageteam);
		return list.size()>0?list.get(0):null;
	}

}
