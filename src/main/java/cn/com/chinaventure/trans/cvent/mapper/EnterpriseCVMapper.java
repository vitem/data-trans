package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.EnterpriseCV;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCVExample;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCVWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface EnterpriseCVMapper {
    int countByExample(EnterpriseCVExample example);

    int deleteByExample(EnterpriseCVExample example);

    int deleteByPrimaryKey(Integer enterpriseId);

    int insert(EnterpriseCVWithBLOBs record);

    int insertSelective(EnterpriseCV record);

    List<EnterpriseCVWithBLOBs> selectByExampleWithBLOBs(EnterpriseCVExample example);

    List<EnterpriseCV> selectByExample(EnterpriseCVExample example);

    EnterpriseCVWithBLOBs selectByPrimaryKey(Integer enterpriseId);

    int updateByExampleSelective(@Param("record") EnterpriseCVWithBLOBs record, @Param("example") EnterpriseCVExample example);

    int updateByExampleWithBLOBs(@Param("record") EnterpriseCVWithBLOBs record, @Param("example") EnterpriseCVExample example);

    int updateByExample(@Param("record") EnterpriseCV record, @Param("example") EnterpriseCVExample example);

    int updateByPrimaryKeySelective(EnterpriseCVWithBLOBs record);

    int updateByPrimaryKeyWithBLOBs(EnterpriseCVWithBLOBs record);

    int updateByPrimaryKey(EnterpriseCV record);

    List<EnterpriseCV> selectByExampleWithRowbounds(EnterpriseCVExample example);
}