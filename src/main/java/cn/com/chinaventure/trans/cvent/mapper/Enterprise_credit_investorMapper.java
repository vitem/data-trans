package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_investor;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_investorExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_credit_investorMapper {
    int countByExample(Enterprise_credit_investorExample example);

    int deleteByExample(Enterprise_credit_investorExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Enterprise_credit_investor record);

    int insertSelective(Enterprise_credit_investor record);

    List<Enterprise_credit_investor> selectByExample(Enterprise_credit_investorExample example);

    Enterprise_credit_investor selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Enterprise_credit_investor record, @Param("example") Enterprise_credit_investorExample example);

    int updateByExample(@Param("record") Enterprise_credit_investor record, @Param("example") Enterprise_credit_investorExample example);

    int updateByPrimaryKeySelective(Enterprise_credit_investor record);

    int updateByPrimaryKey(Enterprise_credit_investor record);
}