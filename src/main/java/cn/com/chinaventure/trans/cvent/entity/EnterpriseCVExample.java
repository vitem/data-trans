package cn.com.chinaventure.trans.cvent.entity;

import cn.com.chinaventure.common.page.PageParam;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


public class EnterpriseCVExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public PageParam pageParam;

    public EnterpriseCVExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public PageParam getPageParam(){
        return this.pageParam;
    }

    public void setPageParam(PageParam pageParam){
        this.pageParam = pageParam;
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("Enterprise_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("Enterprise_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(Integer value) {
            addCriterion("Enterprise_ID =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(Integer value) {
            addCriterion("Enterprise_ID <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(Integer value) {
            addCriterion("Enterprise_ID >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Enterprise_ID >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(Integer value) {
            addCriterion("Enterprise_ID <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(Integer value) {
            addCriterion("Enterprise_ID <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<Integer> values) {
            addCriterion("Enterprise_ID in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<Integer> values) {
            addCriterion("Enterprise_ID not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(Integer value1, Integer value2) {
            addCriterion("Enterprise_ID between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Enterprise_ID not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameIsNull() {
            addCriterion("Enterprise_CN_Name is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameIsNotNull() {
            addCriterion("Enterprise_CN_Name is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameEqualTo(String value) {
            addCriterion("Enterprise_CN_Name =", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameNotEqualTo(String value) {
            addCriterion("Enterprise_CN_Name <>", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameGreaterThan(String value) {
            addCriterion("Enterprise_CN_Name >", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_CN_Name >=", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameLessThan(String value) {
            addCriterion("Enterprise_CN_Name <", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_CN_Name <=", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameLike(String value) {
            addCriterion("Enterprise_CN_Name like", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameNotLike(String value) {
            addCriterion("Enterprise_CN_Name not like", value, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameIn(List<String> values) {
            addCriterion("Enterprise_CN_Name in", values, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameNotIn(List<String> values) {
            addCriterion("Enterprise_CN_Name not in", values, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameBetween(String value1, String value2) {
            addCriterion("Enterprise_CN_Name between", value1, value2, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnNameNotBetween(String value1, String value2) {
            addCriterion("Enterprise_CN_Name not between", value1, value2, "enterpriseCnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortIsNull() {
            addCriterion("Enterprise_CN_Short is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortIsNotNull() {
            addCriterion("Enterprise_CN_Short is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortEqualTo(String value) {
            addCriterion("Enterprise_CN_Short =", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortNotEqualTo(String value) {
            addCriterion("Enterprise_CN_Short <>", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortGreaterThan(String value) {
            addCriterion("Enterprise_CN_Short >", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_CN_Short >=", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortLessThan(String value) {
            addCriterion("Enterprise_CN_Short <", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_CN_Short <=", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortLike(String value) {
            addCriterion("Enterprise_CN_Short like", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortNotLike(String value) {
            addCriterion("Enterprise_CN_Short not like", value, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortIn(List<String> values) {
            addCriterion("Enterprise_CN_Short in", values, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortNotIn(List<String> values) {
            addCriterion("Enterprise_CN_Short not in", values, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortBetween(String value1, String value2) {
            addCriterion("Enterprise_CN_Short between", value1, value2, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnShortNotBetween(String value1, String value2) {
            addCriterion("Enterprise_CN_Short not between", value1, value2, "enterpriseCnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceIsNull() {
            addCriterion("Enterprise_CN_Once is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceIsNotNull() {
            addCriterion("Enterprise_CN_Once is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceEqualTo(String value) {
            addCriterion("Enterprise_CN_Once =", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceNotEqualTo(String value) {
            addCriterion("Enterprise_CN_Once <>", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceGreaterThan(String value) {
            addCriterion("Enterprise_CN_Once >", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_CN_Once >=", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceLessThan(String value) {
            addCriterion("Enterprise_CN_Once <", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_CN_Once <=", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceLike(String value) {
            addCriterion("Enterprise_CN_Once like", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceNotLike(String value) {
            addCriterion("Enterprise_CN_Once not like", value, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceIn(List<String> values) {
            addCriterion("Enterprise_CN_Once in", values, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceNotIn(List<String> values) {
            addCriterion("Enterprise_CN_Once not in", values, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceBetween(String value1, String value2) {
            addCriterion("Enterprise_CN_Once between", value1, value2, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCnOnceNotBetween(String value1, String value2) {
            addCriterion("Enterprise_CN_Once not between", value1, value2, "enterpriseCnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameIsNull() {
            addCriterion("Enterprise_EN_Name is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameIsNotNull() {
            addCriterion("Enterprise_EN_Name is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameEqualTo(String value) {
            addCriterion("Enterprise_EN_Name =", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameNotEqualTo(String value) {
            addCriterion("Enterprise_EN_Name <>", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameGreaterThan(String value) {
            addCriterion("Enterprise_EN_Name >", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_EN_Name >=", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameLessThan(String value) {
            addCriterion("Enterprise_EN_Name <", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_EN_Name <=", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameLike(String value) {
            addCriterion("Enterprise_EN_Name like", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameNotLike(String value) {
            addCriterion("Enterprise_EN_Name not like", value, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameIn(List<String> values) {
            addCriterion("Enterprise_EN_Name in", values, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameNotIn(List<String> values) {
            addCriterion("Enterprise_EN_Name not in", values, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameBetween(String value1, String value2) {
            addCriterion("Enterprise_EN_Name between", value1, value2, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnNameNotBetween(String value1, String value2) {
            addCriterion("Enterprise_EN_Name not between", value1, value2, "enterpriseEnName");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortIsNull() {
            addCriterion("Enterprise_EN_Short is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortIsNotNull() {
            addCriterion("Enterprise_EN_Short is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortEqualTo(String value) {
            addCriterion("Enterprise_EN_Short =", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortNotEqualTo(String value) {
            addCriterion("Enterprise_EN_Short <>", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortGreaterThan(String value) {
            addCriterion("Enterprise_EN_Short >", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_EN_Short >=", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortLessThan(String value) {
            addCriterion("Enterprise_EN_Short <", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_EN_Short <=", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortLike(String value) {
            addCriterion("Enterprise_EN_Short like", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortNotLike(String value) {
            addCriterion("Enterprise_EN_Short not like", value, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortIn(List<String> values) {
            addCriterion("Enterprise_EN_Short in", values, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortNotIn(List<String> values) {
            addCriterion("Enterprise_EN_Short not in", values, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortBetween(String value1, String value2) {
            addCriterion("Enterprise_EN_Short between", value1, value2, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnShortNotBetween(String value1, String value2) {
            addCriterion("Enterprise_EN_Short not between", value1, value2, "enterpriseEnShort");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceIsNull() {
            addCriterion("Enterprise_EN_Once is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceIsNotNull() {
            addCriterion("Enterprise_EN_Once is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceEqualTo(String value) {
            addCriterion("Enterprise_EN_Once =", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceNotEqualTo(String value) {
            addCriterion("Enterprise_EN_Once <>", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceGreaterThan(String value) {
            addCriterion("Enterprise_EN_Once >", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_EN_Once >=", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceLessThan(String value) {
            addCriterion("Enterprise_EN_Once <", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_EN_Once <=", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceLike(String value) {
            addCriterion("Enterprise_EN_Once like", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceNotLike(String value) {
            addCriterion("Enterprise_EN_Once not like", value, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceIn(List<String> values) {
            addCriterion("Enterprise_EN_Once in", values, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceNotIn(List<String> values) {
            addCriterion("Enterprise_EN_Once not in", values, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceBetween(String value1, String value2) {
            addCriterion("Enterprise_EN_Once between", value1, value2, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseEnOnceNotBetween(String value1, String value2) {
            addCriterion("Enterprise_EN_Once not between", value1, value2, "enterpriseEnOnce");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsIsNull() {
            addCriterion("Enterprise_Keywords is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsIsNotNull() {
            addCriterion("Enterprise_Keywords is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsEqualTo(String value) {
            addCriterion("Enterprise_Keywords =", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsNotEqualTo(String value) {
            addCriterion("Enterprise_Keywords <>", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsGreaterThan(String value) {
            addCriterion("Enterprise_Keywords >", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_Keywords >=", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsLessThan(String value) {
            addCriterion("Enterprise_Keywords <", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_Keywords <=", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsLike(String value) {
            addCriterion("Enterprise_Keywords like", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsNotLike(String value) {
            addCriterion("Enterprise_Keywords not like", value, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsIn(List<String> values) {
            addCriterion("Enterprise_Keywords in", values, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsNotIn(List<String> values) {
            addCriterion("Enterprise_Keywords not in", values, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsBetween(String value1, String value2) {
            addCriterion("Enterprise_Keywords between", value1, value2, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseKeywordsNotBetween(String value1, String value2) {
            addCriterion("Enterprise_Keywords not between", value1, value2, "enterpriseKeywords");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebIsNull() {
            addCriterion("Enterprise_Web is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebIsNotNull() {
            addCriterion("Enterprise_Web is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebEqualTo(String value) {
            addCriterion("Enterprise_Web =", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebNotEqualTo(String value) {
            addCriterion("Enterprise_Web <>", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebGreaterThan(String value) {
            addCriterion("Enterprise_Web >", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_Web >=", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebLessThan(String value) {
            addCriterion("Enterprise_Web <", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_Web <=", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebLike(String value) {
            addCriterion("Enterprise_Web like", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebNotLike(String value) {
            addCriterion("Enterprise_Web not like", value, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebIn(List<String> values) {
            addCriterion("Enterprise_Web in", values, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebNotIn(List<String> values) {
            addCriterion("Enterprise_Web not in", values, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebBetween(String value1, String value2) {
            addCriterion("Enterprise_Web between", value1, value2, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseWebNotBetween(String value1, String value2) {
            addCriterion("Enterprise_Web not between", value1, value2, "enterpriseWeb");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoIsNull() {
            addCriterion("Enterprise_Logo is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoIsNotNull() {
            addCriterion("Enterprise_Logo is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoEqualTo(String value) {
            addCriterion("Enterprise_Logo =", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoNotEqualTo(String value) {
            addCriterion("Enterprise_Logo <>", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoGreaterThan(String value) {
            addCriterion("Enterprise_Logo >", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoGreaterThanOrEqualTo(String value) {
            addCriterion("Enterprise_Logo >=", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoLessThan(String value) {
            addCriterion("Enterprise_Logo <", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoLessThanOrEqualTo(String value) {
            addCriterion("Enterprise_Logo <=", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoLike(String value) {
            addCriterion("Enterprise_Logo like", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoNotLike(String value) {
            addCriterion("Enterprise_Logo not like", value, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoIn(List<String> values) {
            addCriterion("Enterprise_Logo in", values, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoNotIn(List<String> values) {
            addCriterion("Enterprise_Logo not in", values, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoBetween(String value1, String value2) {
            addCriterion("Enterprise_Logo between", value1, value2, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseLogoNotBetween(String value1, String value2) {
            addCriterion("Enterprise_Logo not between", value1, value2, "enterpriseLogo");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryIsNull() {
            addCriterion("Enterprise_country is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryIsNotNull() {
            addCriterion("Enterprise_country is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryEqualTo(Short value) {
            addCriterion("Enterprise_country =", value, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryNotEqualTo(Short value) {
            addCriterion("Enterprise_country <>", value, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryGreaterThan(Short value) {
            addCriterion("Enterprise_country >", value, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryGreaterThanOrEqualTo(Short value) {
            addCriterion("Enterprise_country >=", value, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryLessThan(Short value) {
            addCriterion("Enterprise_country <", value, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryLessThanOrEqualTo(Short value) {
            addCriterion("Enterprise_country <=", value, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryIn(List<Short> values) {
            addCriterion("Enterprise_country in", values, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryNotIn(List<Short> values) {
            addCriterion("Enterprise_country not in", values, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryBetween(Short value1, Short value2) {
            addCriterion("Enterprise_country between", value1, value2, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCountryNotBetween(Short value1, Short value2) {
            addCriterion("Enterprise_country not between", value1, value2, "enterpriseCountry");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityIsNull() {
            addCriterion("Enterprise_city is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityIsNotNull() {
            addCriterion("Enterprise_city is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityEqualTo(Short value) {
            addCriterion("Enterprise_city =", value, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityNotEqualTo(Short value) {
            addCriterion("Enterprise_city <>", value, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityGreaterThan(Short value) {
            addCriterion("Enterprise_city >", value, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityGreaterThanOrEqualTo(Short value) {
            addCriterion("Enterprise_city >=", value, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityLessThan(Short value) {
            addCriterion("Enterprise_city <", value, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityLessThanOrEqualTo(Short value) {
            addCriterion("Enterprise_city <=", value, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityIn(List<Short> values) {
            addCriterion("Enterprise_city in", values, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityNotIn(List<Short> values) {
            addCriterion("Enterprise_city not in", values, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityBetween(Short value1, Short value2) {
            addCriterion("Enterprise_city between", value1, value2, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseCityNotBetween(Short value1, Short value2) {
            addCriterion("Enterprise_city not between", value1, value2, "enterpriseCity");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownIsNull() {
            addCriterion("Enterprise_town is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownIsNotNull() {
            addCriterion("Enterprise_town is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownEqualTo(Short value) {
            addCriterion("Enterprise_town =", value, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownNotEqualTo(Short value) {
            addCriterion("Enterprise_town <>", value, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownGreaterThan(Short value) {
            addCriterion("Enterprise_town >", value, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownGreaterThanOrEqualTo(Short value) {
            addCriterion("Enterprise_town >=", value, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownLessThan(Short value) {
            addCriterion("Enterprise_town <", value, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownLessThanOrEqualTo(Short value) {
            addCriterion("Enterprise_town <=", value, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownIn(List<Short> values) {
            addCriterion("Enterprise_town in", values, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownNotIn(List<Short> values) {
            addCriterion("Enterprise_town not in", values, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownBetween(Short value1, Short value2) {
            addCriterion("Enterprise_town between", value1, value2, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseTownNotBetween(Short value1, Short value2) {
            addCriterion("Enterprise_town not between", value1, value2, "enterpriseTown");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeIsNull() {
            addCriterion("Enterprise_SetUpTime is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeIsNotNull() {
            addCriterion("Enterprise_SetUpTime is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_SetUpTime =", value, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_SetUpTime <>", value, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeGreaterThan(Date value) {
            addCriterionForJDBCDate("Enterprise_SetUpTime >", value, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_SetUpTime >=", value, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeLessThan(Date value) {
            addCriterionForJDBCDate("Enterprise_SetUpTime <", value, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_SetUpTime <=", value, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_SetUpTime in", values, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_SetUpTime not in", values, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_SetUpTime between", value1, value2, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseSetuptimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_SetUpTime not between", value1, value2, "enterpriseSetuptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeIsNull() {
            addCriterion("Enterprise_StopTime is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeIsNotNull() {
            addCriterion("Enterprise_StopTime is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_StopTime =", value, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeNotEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_StopTime <>", value, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeGreaterThan(Date value) {
            addCriterionForJDBCDate("Enterprise_StopTime >", value, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_StopTime >=", value, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeLessThan(Date value) {
            addCriterionForJDBCDate("Enterprise_StopTime <", value, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("Enterprise_StopTime <=", value, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_StopTime in", values, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeNotIn(List<Date> values) {
            addCriterionForJDBCDate("Enterprise_StopTime not in", values, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_StopTime between", value1, value2, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andEnterpriseStoptimeNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("Enterprise_StopTime not between", value1, value2, "enterpriseStoptime");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdIsNull() {
            addCriterion("CommerceStatus_ID is null");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdIsNotNull() {
            addCriterion("CommerceStatus_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdEqualTo(Byte value) {
            addCriterion("CommerceStatus_ID =", value, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdNotEqualTo(Byte value) {
            addCriterion("CommerceStatus_ID <>", value, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdGreaterThan(Byte value) {
            addCriterion("CommerceStatus_ID >", value, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("CommerceStatus_ID >=", value, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdLessThan(Byte value) {
            addCriterion("CommerceStatus_ID <", value, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdLessThanOrEqualTo(Byte value) {
            addCriterion("CommerceStatus_ID <=", value, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdIn(List<Byte> values) {
            addCriterion("CommerceStatus_ID in", values, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdNotIn(List<Byte> values) {
            addCriterion("CommerceStatus_ID not in", values, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdBetween(Byte value1, Byte value2) {
            addCriterion("CommerceStatus_ID between", value1, value2, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andCommercestatusIdNotBetween(Byte value1, Byte value2) {
            addCriterion("CommerceStatus_ID not between", value1, value2, "commercestatusId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdIsNull() {
            addCriterion("EnterpriseStage_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdIsNotNull() {
            addCriterion("EnterpriseStage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdEqualTo(Byte value) {
            addCriterion("EnterpriseStage_ID =", value, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdNotEqualTo(Byte value) {
            addCriterion("EnterpriseStage_ID <>", value, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdGreaterThan(Byte value) {
            addCriterion("EnterpriseStage_ID >", value, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("EnterpriseStage_ID >=", value, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdLessThan(Byte value) {
            addCriterion("EnterpriseStage_ID <", value, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdLessThanOrEqualTo(Byte value) {
            addCriterion("EnterpriseStage_ID <=", value, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdIn(List<Byte> values) {
            addCriterion("EnterpriseStage_ID in", values, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdNotIn(List<Byte> values) {
            addCriterion("EnterpriseStage_ID not in", values, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdBetween(Byte value1, Byte value2) {
            addCriterion("EnterpriseStage_ID between", value1, value2, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageIdNotBetween(Byte value1, Byte value2) {
            addCriterion("EnterpriseStage_ID not between", value1, value2, "enterprisestageId");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusIsNull() {
            addCriterion("EnterpriseStage_Status is null");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusIsNotNull() {
            addCriterion("EnterpriseStage_Status is not null");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusEqualTo(Byte value) {
            addCriterion("EnterpriseStage_Status =", value, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusNotEqualTo(Byte value) {
            addCriterion("EnterpriseStage_Status <>", value, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusGreaterThan(Byte value) {
            addCriterion("EnterpriseStage_Status >", value, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusGreaterThanOrEqualTo(Byte value) {
            addCriterion("EnterpriseStage_Status >=", value, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusLessThan(Byte value) {
            addCriterion("EnterpriseStage_Status <", value, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusLessThanOrEqualTo(Byte value) {
            addCriterion("EnterpriseStage_Status <=", value, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusIn(List<Byte> values) {
            addCriterion("EnterpriseStage_Status in", values, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusNotIn(List<Byte> values) {
            addCriterion("EnterpriseStage_Status not in", values, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusBetween(Byte value1, Byte value2) {
            addCriterion("EnterpriseStage_Status between", value1, value2, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andEnterprisestageStatusNotBetween(Byte value1, Byte value2) {
            addCriterion("EnterpriseStage_Status not between", value1, value2, "enterprisestageStatus");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("Update_Time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("Update_Time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("Update_Time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("Update_Time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("Update_Time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Update_Time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("Update_Time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Update_Time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("Update_Time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("Update_Time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("Update_Time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Update_Time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNull() {
            addCriterion("Behavior is null");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNotNull() {
            addCriterion("Behavior is not null");
            return (Criteria) this;
        }

        public Criteria andBehaviorEqualTo(String value) {
            addCriterion("Behavior =", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotEqualTo(String value) {
            addCriterion("Behavior <>", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThan(String value) {
            addCriterion("Behavior >", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThanOrEqualTo(String value) {
            addCriterion("Behavior >=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThan(String value) {
            addCriterion("Behavior <", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThanOrEqualTo(String value) {
            addCriterion("Behavior <=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLike(String value) {
            addCriterion("Behavior like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotLike(String value) {
            addCriterion("Behavior not like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorIn(List<String> values) {
            addCriterion("Behavior in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotIn(List<String> values) {
            addCriterion("Behavior not in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorBetween(String value1, String value2) {
            addCriterion("Behavior between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotBetween(String value1, String value2) {
            addCriterion("Behavior not between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIsNull() {
            addCriterion("Verify_Time is null");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIsNotNull() {
            addCriterion("Verify_Time is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeEqualTo(Date value) {
            addCriterion("Verify_Time =", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotEqualTo(Date value) {
            addCriterion("Verify_Time <>", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeGreaterThan(Date value) {
            addCriterion("Verify_Time >", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Verify_Time >=", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeLessThan(Date value) {
            addCriterion("Verify_Time <", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("Verify_Time <=", value, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeIn(List<Date> values) {
            addCriterion("Verify_Time in", values, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotIn(List<Date> values) {
            addCriterion("Verify_Time not in", values, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeBetween(Date value1, Date value2) {
            addCriterion("Verify_Time between", value1, value2, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("Verify_Time not between", value1, value2, "verifyTime");
            return (Criteria) this;
        }

        public Criteria andVerifyUserIsNull() {
            addCriterion("Verify_User is null");
            return (Criteria) this;
        }

        public Criteria andVerifyUserIsNotNull() {
            addCriterion("Verify_User is not null");
            return (Criteria) this;
        }

        public Criteria andVerifyUserEqualTo(String value) {
            addCriterion("Verify_User =", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotEqualTo(String value) {
            addCriterion("Verify_User <>", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserGreaterThan(String value) {
            addCriterion("Verify_User >", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserGreaterThanOrEqualTo(String value) {
            addCriterion("Verify_User >=", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserLessThan(String value) {
            addCriterion("Verify_User <", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserLessThanOrEqualTo(String value) {
            addCriterion("Verify_User <=", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserLike(String value) {
            addCriterion("Verify_User like", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotLike(String value) {
            addCriterion("Verify_User not like", value, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserIn(List<String> values) {
            addCriterion("Verify_User in", values, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotIn(List<String> values) {
            addCriterion("Verify_User not in", values, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserBetween(String value1, String value2) {
            addCriterion("Verify_User between", value1, value2, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andVerifyUserNotBetween(String value1, String value2) {
            addCriterion("Verify_User not between", value1, value2, "verifyUser");
            return (Criteria) this;
        }

        public Criteria andContentIsNull() {
            addCriterion("Content is null");
            return (Criteria) this;
        }

        public Criteria andContentIsNotNull() {
            addCriterion("Content is not null");
            return (Criteria) this;
        }

        public Criteria andContentEqualTo(String value) {
            addCriterion("Content =", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotEqualTo(String value) {
            addCriterion("Content <>", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThan(String value) {
            addCriterion("Content >", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentGreaterThanOrEqualTo(String value) {
            addCriterion("Content >=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThan(String value) {
            addCriterion("Content <", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLessThanOrEqualTo(String value) {
            addCriterion("Content <=", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentLike(String value) {
            addCriterion("Content like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotLike(String value) {
            addCriterion("Content not like", value, "content");
            return (Criteria) this;
        }

        public Criteria andContentIn(List<String> values) {
            addCriterion("Content in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotIn(List<String> values) {
            addCriterion("Content not in", values, "content");
            return (Criteria) this;
        }

        public Criteria andContentBetween(String value1, String value2) {
            addCriterion("Content between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andContentNotBetween(String value1, String value2) {
            addCriterion("Content not between", value1, value2, "content");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdIsNull() {
            addCriterion("Financing_Event_ID is null");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdIsNotNull() {
            addCriterion("Financing_Event_ID is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdEqualTo(Integer value) {
            addCriterion("Financing_Event_ID =", value, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdNotEqualTo(Integer value) {
            addCriterion("Financing_Event_ID <>", value, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdGreaterThan(Integer value) {
            addCriterion("Financing_Event_ID >", value, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Financing_Event_ID >=", value, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdLessThan(Integer value) {
            addCriterion("Financing_Event_ID <", value, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdLessThanOrEqualTo(Integer value) {
            addCriterion("Financing_Event_ID <=", value, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdIn(List<Integer> values) {
            addCriterion("Financing_Event_ID in", values, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdNotIn(List<Integer> values) {
            addCriterion("Financing_Event_ID not in", values, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdBetween(Integer value1, Integer value2) {
            addCriterion("Financing_Event_ID between", value1, value2, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andFinancingEventIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Financing_Event_ID not between", value1, value2, "financingEventId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdIsNull() {
            addCriterion("Appearmarket_ID is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdIsNotNull() {
            addCriterion("Appearmarket_ID is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdEqualTo(Integer value) {
            addCriterion("Appearmarket_ID =", value, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdNotEqualTo(Integer value) {
            addCriterion("Appearmarket_ID <>", value, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdGreaterThan(Integer value) {
            addCriterion("Appearmarket_ID >", value, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Appearmarket_ID >=", value, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdLessThan(Integer value) {
            addCriterion("Appearmarket_ID <", value, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdLessThanOrEqualTo(Integer value) {
            addCriterion("Appearmarket_ID <=", value, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdIn(List<Integer> values) {
            addCriterion("Appearmarket_ID in", values, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdNotIn(List<Integer> values) {
            addCriterion("Appearmarket_ID not in", values, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdBetween(Integer value1, Integer value2) {
            addCriterion("Appearmarket_ID between", value1, value2, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andAppearmarketIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Appearmarket_ID not between", value1, value2, "appearmarketId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdIsNull() {
            addCriterion("Buytogetherevent_ID is null");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdIsNotNull() {
            addCriterion("Buytogetherevent_ID is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdEqualTo(Integer value) {
            addCriterion("Buytogetherevent_ID =", value, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdNotEqualTo(Integer value) {
            addCriterion("Buytogetherevent_ID <>", value, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdGreaterThan(Integer value) {
            addCriterion("Buytogetherevent_ID >", value, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Buytogetherevent_ID >=", value, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdLessThan(Integer value) {
            addCriterion("Buytogetherevent_ID <", value, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdLessThanOrEqualTo(Integer value) {
            addCriterion("Buytogetherevent_ID <=", value, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdIn(List<Integer> values) {
            addCriterion("Buytogetherevent_ID in", values, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdNotIn(List<Integer> values) {
            addCriterion("Buytogetherevent_ID not in", values, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdBetween(Integer value1, Integer value2) {
            addCriterion("Buytogetherevent_ID between", value1, value2, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andBuytogethereventIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Buytogetherevent_ID not between", value1, value2, "buytogethereventId");
            return (Criteria) this;
        }

        public Criteria andFinancingCountIsNull() {
            addCriterion("Financing_Count is null");
            return (Criteria) this;
        }

        public Criteria andFinancingCountIsNotNull() {
            addCriterion("Financing_Count is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingCountEqualTo(Short value) {
            addCriterion("Financing_Count =", value, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountNotEqualTo(Short value) {
            addCriterion("Financing_Count <>", value, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountGreaterThan(Short value) {
            addCriterion("Financing_Count >", value, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountGreaterThanOrEqualTo(Short value) {
            addCriterion("Financing_Count >=", value, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountLessThan(Short value) {
            addCriterion("Financing_Count <", value, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountLessThanOrEqualTo(Short value) {
            addCriterion("Financing_Count <=", value, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountIn(List<Short> values) {
            addCriterion("Financing_Count in", values, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountNotIn(List<Short> values) {
            addCriterion("Financing_Count not in", values, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountBetween(Short value1, Short value2) {
            addCriterion("Financing_Count between", value1, value2, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinancingCountNotBetween(Short value1, Short value2) {
            addCriterion("Financing_Count not between", value1, value2, "financingCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountIsNull() {
            addCriterion("Finance_Count is null");
            return (Criteria) this;
        }

        public Criteria andFinanceCountIsNotNull() {
            addCriterion("Finance_Count is not null");
            return (Criteria) this;
        }

        public Criteria andFinanceCountEqualTo(Short value) {
            addCriterion("Finance_Count =", value, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountNotEqualTo(Short value) {
            addCriterion("Finance_Count <>", value, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountGreaterThan(Short value) {
            addCriterion("Finance_Count >", value, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountGreaterThanOrEqualTo(Short value) {
            addCriterion("Finance_Count >=", value, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountLessThan(Short value) {
            addCriterion("Finance_Count <", value, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountLessThanOrEqualTo(Short value) {
            addCriterion("Finance_Count <=", value, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountIn(List<Short> values) {
            addCriterion("Finance_Count in", values, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountNotIn(List<Short> values) {
            addCriterion("Finance_Count not in", values, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountBetween(Short value1, Short value2) {
            addCriterion("Finance_Count between", value1, value2, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinanceCountNotBetween(Short value1, Short value2) {
            addCriterion("Finance_Count not between", value1, value2, "financeCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountIsNull() {
            addCriterion("Financingintelligence_Count is null");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountIsNotNull() {
            addCriterion("Financingintelligence_Count is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountEqualTo(Short value) {
            addCriterion("Financingintelligence_Count =", value, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountNotEqualTo(Short value) {
            addCriterion("Financingintelligence_Count <>", value, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountGreaterThan(Short value) {
            addCriterion("Financingintelligence_Count >", value, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountGreaterThanOrEqualTo(Short value) {
            addCriterion("Financingintelligence_Count >=", value, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountLessThan(Short value) {
            addCriterion("Financingintelligence_Count <", value, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountLessThanOrEqualTo(Short value) {
            addCriterion("Financingintelligence_Count <=", value, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountIn(List<Short> values) {
            addCriterion("Financingintelligence_Count in", values, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountNotIn(List<Short> values) {
            addCriterion("Financingintelligence_Count not in", values, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountBetween(Short value1, Short value2) {
            addCriterion("Financingintelligence_Count between", value1, value2, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingintelligenceCountNotBetween(Short value1, Short value2) {
            addCriterion("Financingintelligence_Count not between", value1, value2, "financingintelligenceCount");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidIsNull() {
            addCriterion("FinancingeventtypeId is null");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidIsNotNull() {
            addCriterion("FinancingeventtypeId is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidEqualTo(Byte value) {
            addCriterion("FinancingeventtypeId =", value, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidNotEqualTo(Byte value) {
            addCriterion("FinancingeventtypeId <>", value, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidGreaterThan(Byte value) {
            addCriterion("FinancingeventtypeId >", value, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidGreaterThanOrEqualTo(Byte value) {
            addCriterion("FinancingeventtypeId >=", value, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidLessThan(Byte value) {
            addCriterion("FinancingeventtypeId <", value, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidLessThanOrEqualTo(Byte value) {
            addCriterion("FinancingeventtypeId <=", value, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidIn(List<Byte> values) {
            addCriterion("FinancingeventtypeId in", values, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidNotIn(List<Byte> values) {
            addCriterion("FinancingeventtypeId not in", values, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidBetween(Byte value1, Byte value2) {
            addCriterion("FinancingeventtypeId between", value1, value2, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeidNotBetween(Byte value1, Byte value2) {
            addCriterion("FinancingeventtypeId not between", value1, value2, "financingeventtypeid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidIsNull() {
            addCriterion("FinancingevententityId is null");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidIsNotNull() {
            addCriterion("FinancingevententityId is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidEqualTo(Byte value) {
            addCriterion("FinancingevententityId =", value, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidNotEqualTo(Byte value) {
            addCriterion("FinancingevententityId <>", value, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidGreaterThan(Byte value) {
            addCriterion("FinancingevententityId >", value, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidGreaterThanOrEqualTo(Byte value) {
            addCriterion("FinancingevententityId >=", value, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidLessThan(Byte value) {
            addCriterion("FinancingevententityId <", value, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidLessThanOrEqualTo(Byte value) {
            addCriterion("FinancingevententityId <=", value, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidIn(List<Byte> values) {
            addCriterion("FinancingevententityId in", values, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidNotIn(List<Byte> values) {
            addCriterion("FinancingevententityId not in", values, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidBetween(Byte value1, Byte value2) {
            addCriterion("FinancingevententityId between", value1, value2, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingevententityidNotBetween(Byte value1, Byte value2) {
            addCriterion("FinancingevententityId not between", value1, value2, "financingevententityid");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusIsNull() {
            addCriterion("FinancingRealityMoneyUs is null");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusIsNotNull() {
            addCriterion("FinancingRealityMoneyUs is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusEqualTo(Double value) {
            addCriterion("FinancingRealityMoneyUs =", value, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusNotEqualTo(Double value) {
            addCriterion("FinancingRealityMoneyUs <>", value, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusGreaterThan(Double value) {
            addCriterion("FinancingRealityMoneyUs >", value, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusGreaterThanOrEqualTo(Double value) {
            addCriterion("FinancingRealityMoneyUs >=", value, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusLessThan(Double value) {
            addCriterion("FinancingRealityMoneyUs <", value, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusLessThanOrEqualTo(Double value) {
            addCriterion("FinancingRealityMoneyUs <=", value, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusIn(List<Double> values) {
            addCriterion("FinancingRealityMoneyUs in", values, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusNotIn(List<Double> values) {
            addCriterion("FinancingRealityMoneyUs not in", values, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusBetween(Double value1, Double value2) {
            addCriterion("FinancingRealityMoneyUs between", value1, value2, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyusNotBetween(Double value1, Double value2) {
            addCriterion("FinancingRealityMoneyUs not between", value1, value2, "financingrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateIsNull() {
            addCriterion("FinancingHappenDate is null");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateIsNotNull() {
            addCriterion("FinancingHappenDate is not null");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateEqualTo(Date value) {
            addCriterionForJDBCDate("FinancingHappenDate =", value, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateNotEqualTo(Date value) {
            addCriterionForJDBCDate("FinancingHappenDate <>", value, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateGreaterThan(Date value) {
            addCriterionForJDBCDate("FinancingHappenDate >", value, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("FinancingHappenDate >=", value, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateLessThan(Date value) {
            addCriterionForJDBCDate("FinancingHappenDate <", value, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("FinancingHappenDate <=", value, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateIn(List<Date> values) {
            addCriterionForJDBCDate("FinancingHappenDate in", values, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateNotIn(List<Date> values) {
            addCriterionForJDBCDate("FinancingHappenDate not in", values, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("FinancingHappenDate between", value1, value2, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andFinancinghappendateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("FinancingHappenDate not between", value1, value2, "financinghappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusIsNull() {
            addCriterion("AppearmarketRealityMoneyUs is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusIsNotNull() {
            addCriterion("AppearmarketRealityMoneyUs is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoneyUs =", value, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusNotEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoneyUs <>", value, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusGreaterThan(Double value) {
            addCriterion("AppearmarketRealityMoneyUs >", value, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusGreaterThanOrEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoneyUs >=", value, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusLessThan(Double value) {
            addCriterion("AppearmarketRealityMoneyUs <", value, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusLessThanOrEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoneyUs <=", value, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusIn(List<Double> values) {
            addCriterion("AppearmarketRealityMoneyUs in", values, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusNotIn(List<Double> values) {
            addCriterion("AppearmarketRealityMoneyUs not in", values, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusBetween(Double value1, Double value2) {
            addCriterion("AppearmarketRealityMoneyUs between", value1, value2, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyusNotBetween(Double value1, Double value2) {
            addCriterion("AppearmarketRealityMoneyUs not between", value1, value2, "appearmarketrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateIsNull() {
            addCriterion("AppearmarketHappenDate is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateIsNotNull() {
            addCriterion("AppearmarketHappenDate is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateEqualTo(Date value) {
            addCriterionForJDBCDate("AppearmarketHappenDate =", value, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateNotEqualTo(Date value) {
            addCriterionForJDBCDate("AppearmarketHappenDate <>", value, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateGreaterThan(Date value) {
            addCriterionForJDBCDate("AppearmarketHappenDate >", value, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("AppearmarketHappenDate >=", value, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateLessThan(Date value) {
            addCriterionForJDBCDate("AppearmarketHappenDate <", value, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("AppearmarketHappenDate <=", value, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateIn(List<Date> values) {
            addCriterionForJDBCDate("AppearmarketHappenDate in", values, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateNotIn(List<Date> values) {
            addCriterionForJDBCDate("AppearmarketHappenDate not in", values, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("AppearmarketHappenDate between", value1, value2, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andAppearmarkethappendateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("AppearmarketHappenDate not between", value1, value2, "appearmarkethappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusIsNull() {
            addCriterion("BuytogetherRealityMoneyUs is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusIsNotNull() {
            addCriterion("BuytogetherRealityMoneyUs is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoneyUs =", value, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusNotEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoneyUs <>", value, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusGreaterThan(Double value) {
            addCriterion("BuytogetherRealityMoneyUs >", value, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusGreaterThanOrEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoneyUs >=", value, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusLessThan(Double value) {
            addCriterion("BuytogetherRealityMoneyUs <", value, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusLessThanOrEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoneyUs <=", value, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusIn(List<Double> values) {
            addCriterion("BuytogetherRealityMoneyUs in", values, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusNotIn(List<Double> values) {
            addCriterion("BuytogetherRealityMoneyUs not in", values, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusBetween(Double value1, Double value2) {
            addCriterion("BuytogetherRealityMoneyUs between", value1, value2, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyusNotBetween(Double value1, Double value2) {
            addCriterion("BuytogetherRealityMoneyUs not between", value1, value2, "buytogetherrealitymoneyus");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateIsNull() {
            addCriterion("BuytogetherHappenDate is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateIsNotNull() {
            addCriterion("BuytogetherHappenDate is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateEqualTo(Date value) {
            addCriterionForJDBCDate("BuytogetherHappenDate =", value, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateNotEqualTo(Date value) {
            addCriterionForJDBCDate("BuytogetherHappenDate <>", value, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateGreaterThan(Date value) {
            addCriterionForJDBCDate("BuytogetherHappenDate >", value, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("BuytogetherHappenDate >=", value, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateLessThan(Date value) {
            addCriterionForJDBCDate("BuytogetherHappenDate <", value, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("BuytogetherHappenDate <=", value, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateIn(List<Date> values) {
            addCriterionForJDBCDate("BuytogetherHappenDate in", values, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateNotIn(List<Date> values) {
            addCriterionForJDBCDate("BuytogetherHappenDate not in", values, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("BuytogetherHappenDate between", value1, value2, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andBuytogetherhappendateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("BuytogetherHappenDate not between", value1, value2, "buytogetherhappendate");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameIsNull() {
            addCriterion("Industry_CnName is null");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameIsNotNull() {
            addCriterion("Industry_CnName is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameEqualTo(String value) {
            addCriterion("Industry_CnName =", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameNotEqualTo(String value) {
            addCriterion("Industry_CnName <>", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameGreaterThan(String value) {
            addCriterion("Industry_CnName >", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Industry_CnName >=", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameLessThan(String value) {
            addCriterion("Industry_CnName <", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameLessThanOrEqualTo(String value) {
            addCriterion("Industry_CnName <=", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameLike(String value) {
            addCriterion("Industry_CnName like", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameNotLike(String value) {
            addCriterion("Industry_CnName not like", value, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameIn(List<String> values) {
            addCriterion("Industry_CnName in", values, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameNotIn(List<String> values) {
            addCriterion("Industry_CnName not in", values, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameBetween(String value1, String value2) {
            addCriterion("Industry_CnName between", value1, value2, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryCnnameNotBetween(String value1, String value2) {
            addCriterion("Industry_CnName not between", value1, value2, "industryCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameIsNull() {
            addCriterion("FinancingEventType_CnName is null");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameIsNotNull() {
            addCriterion("FinancingEventType_CnName is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameEqualTo(String value) {
            addCriterion("FinancingEventType_CnName =", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameNotEqualTo(String value) {
            addCriterion("FinancingEventType_CnName <>", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameGreaterThan(String value) {
            addCriterion("FinancingEventType_CnName >", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameGreaterThanOrEqualTo(String value) {
            addCriterion("FinancingEventType_CnName >=", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameLessThan(String value) {
            addCriterion("FinancingEventType_CnName <", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameLessThanOrEqualTo(String value) {
            addCriterion("FinancingEventType_CnName <=", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameLike(String value) {
            addCriterion("FinancingEventType_CnName like", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameNotLike(String value) {
            addCriterion("FinancingEventType_CnName not like", value, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameIn(List<String> values) {
            addCriterion("FinancingEventType_CnName in", values, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameNotIn(List<String> values) {
            addCriterion("FinancingEventType_CnName not in", values, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameBetween(String value1, String value2) {
            addCriterion("FinancingEventType_CnName between", value1, value2, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeCnnameNotBetween(String value1, String value2) {
            addCriterion("FinancingEventType_CnName not between", value1, value2, "financingeventtypeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameIsNull() {
            addCriterion("Exchange_CnName is null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameIsNotNull() {
            addCriterion("Exchange_CnName is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameEqualTo(String value) {
            addCriterion("Exchange_CnName =", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameNotEqualTo(String value) {
            addCriterion("Exchange_CnName <>", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameGreaterThan(String value) {
            addCriterion("Exchange_CnName >", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Exchange_CnName >=", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameLessThan(String value) {
            addCriterion("Exchange_CnName <", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameLessThanOrEqualTo(String value) {
            addCriterion("Exchange_CnName <=", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameLike(String value) {
            addCriterion("Exchange_CnName like", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameNotLike(String value) {
            addCriterion("Exchange_CnName not like", value, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameIn(List<String> values) {
            addCriterion("Exchange_CnName in", values, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameNotIn(List<String> values) {
            addCriterion("Exchange_CnName not in", values, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameBetween(String value1, String value2) {
            addCriterion("Exchange_CnName between", value1, value2, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andExchangeCnnameNotBetween(String value1, String value2) {
            addCriterion("Exchange_CnName not between", value1, value2, "exchangeCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameIsNull() {
            addCriterion("BuytogetherEnterprise_CnName is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameIsNotNull() {
            addCriterion("BuytogetherEnterprise_CnName is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_CnName =", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameNotEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_CnName <>", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameGreaterThan(String value) {
            addCriterion("BuytogetherEnterprise_CnName >", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameGreaterThanOrEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_CnName >=", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameLessThan(String value) {
            addCriterion("BuytogetherEnterprise_CnName <", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameLessThanOrEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_CnName <=", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameLike(String value) {
            addCriterion("BuytogetherEnterprise_CnName like", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameNotLike(String value) {
            addCriterion("BuytogetherEnterprise_CnName not like", value, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameIn(List<String> values) {
            addCriterion("BuytogetherEnterprise_CnName in", values, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameNotIn(List<String> values) {
            addCriterion("BuytogetherEnterprise_CnName not in", values, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameBetween(String value1, String value2) {
            addCriterion("BuytogetherEnterprise_CnName between", value1, value2, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseCnnameNotBetween(String value1, String value2) {
            addCriterion("BuytogetherEnterprise_CnName not between", value1, value2, "buytogetherenterpriseCnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdIsNull() {
            addCriterion("BuytogetherEnterprise_Id is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdIsNotNull() {
            addCriterion("BuytogetherEnterprise_Id is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_Id =", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdNotEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_Id <>", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdGreaterThan(String value) {
            addCriterion("BuytogetherEnterprise_Id >", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdGreaterThanOrEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_Id >=", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdLessThan(String value) {
            addCriterion("BuytogetherEnterprise_Id <", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdLessThanOrEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_Id <=", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdLike(String value) {
            addCriterion("BuytogetherEnterprise_Id like", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdNotLike(String value) {
            addCriterion("BuytogetherEnterprise_Id not like", value, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdIn(List<String> values) {
            addCriterion("BuytogetherEnterprise_Id in", values, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdNotIn(List<String> values) {
            addCriterion("BuytogetherEnterprise_Id not in", values, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdBetween(String value1, String value2) {
            addCriterion("BuytogetherEnterprise_Id between", value1, value2, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseIdNotBetween(String value1, String value2) {
            addCriterion("BuytogetherEnterprise_Id not between", value1, value2, "buytogetherenterpriseId");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyIsNull() {
            addCriterion("FinancingRealityMoney is null");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyIsNotNull() {
            addCriterion("FinancingRealityMoney is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyEqualTo(Double value) {
            addCriterion("FinancingRealityMoney =", value, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyNotEqualTo(Double value) {
            addCriterion("FinancingRealityMoney <>", value, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyGreaterThan(Double value) {
            addCriterion("FinancingRealityMoney >", value, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("FinancingRealityMoney >=", value, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyLessThan(Double value) {
            addCriterion("FinancingRealityMoney <", value, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyLessThanOrEqualTo(Double value) {
            addCriterion("FinancingRealityMoney <=", value, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyIn(List<Double> values) {
            addCriterion("FinancingRealityMoney in", values, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyNotIn(List<Double> values) {
            addCriterion("FinancingRealityMoney not in", values, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyBetween(Double value1, Double value2) {
            addCriterion("FinancingRealityMoney between", value1, value2, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyNotBetween(Double value1, Double value2) {
            addCriterion("FinancingRealityMoney not between", value1, value2, "financingrealitymoney");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyIsNull() {
            addCriterion("FinancingRealityMoney_Currency is null");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyIsNotNull() {
            addCriterion("FinancingRealityMoney_Currency is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyEqualTo(Byte value) {
            addCriterion("FinancingRealityMoney_Currency =", value, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyNotEqualTo(Byte value) {
            addCriterion("FinancingRealityMoney_Currency <>", value, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyGreaterThan(Byte value) {
            addCriterion("FinancingRealityMoney_Currency >", value, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyGreaterThanOrEqualTo(Byte value) {
            addCriterion("FinancingRealityMoney_Currency >=", value, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyLessThan(Byte value) {
            addCriterion("FinancingRealityMoney_Currency <", value, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyLessThanOrEqualTo(Byte value) {
            addCriterion("FinancingRealityMoney_Currency <=", value, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyIn(List<Byte> values) {
            addCriterion("FinancingRealityMoney_Currency in", values, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyNotIn(List<Byte> values) {
            addCriterion("FinancingRealityMoney_Currency not in", values, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyBetween(Byte value1, Byte value2) {
            addCriterion("FinancingRealityMoney_Currency between", value1, value2, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andFinancingrealitymoneyCurrencyNotBetween(Byte value1, Byte value2) {
            addCriterion("FinancingRealityMoney_Currency not between", value1, value2, "financingrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyIsNull() {
            addCriterion("AppearmarketRealityMoney is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyIsNotNull() {
            addCriterion("AppearmarketRealityMoney is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoney =", value, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyNotEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoney <>", value, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyGreaterThan(Double value) {
            addCriterion("AppearmarketRealityMoney >", value, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoney >=", value, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyLessThan(Double value) {
            addCriterion("AppearmarketRealityMoney <", value, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyLessThanOrEqualTo(Double value) {
            addCriterion("AppearmarketRealityMoney <=", value, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyIn(List<Double> values) {
            addCriterion("AppearmarketRealityMoney in", values, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyNotIn(List<Double> values) {
            addCriterion("AppearmarketRealityMoney not in", values, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyBetween(Double value1, Double value2) {
            addCriterion("AppearmarketRealityMoney between", value1, value2, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyNotBetween(Double value1, Double value2) {
            addCriterion("AppearmarketRealityMoney not between", value1, value2, "appearmarketrealitymoney");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyIsNull() {
            addCriterion("AppearmarketRealityMoney_Currency is null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyIsNotNull() {
            addCriterion("AppearmarketRealityMoney_Currency is not null");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyEqualTo(Byte value) {
            addCriterion("AppearmarketRealityMoney_Currency =", value, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyNotEqualTo(Byte value) {
            addCriterion("AppearmarketRealityMoney_Currency <>", value, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyGreaterThan(Byte value) {
            addCriterion("AppearmarketRealityMoney_Currency >", value, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyGreaterThanOrEqualTo(Byte value) {
            addCriterion("AppearmarketRealityMoney_Currency >=", value, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyLessThan(Byte value) {
            addCriterion("AppearmarketRealityMoney_Currency <", value, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyLessThanOrEqualTo(Byte value) {
            addCriterion("AppearmarketRealityMoney_Currency <=", value, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyIn(List<Byte> values) {
            addCriterion("AppearmarketRealityMoney_Currency in", values, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyNotIn(List<Byte> values) {
            addCriterion("AppearmarketRealityMoney_Currency not in", values, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyBetween(Byte value1, Byte value2) {
            addCriterion("AppearmarketRealityMoney_Currency between", value1, value2, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andAppearmarketrealitymoneyCurrencyNotBetween(Byte value1, Byte value2) {
            addCriterion("AppearmarketRealityMoney_Currency not between", value1, value2, "appearmarketrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyIsNull() {
            addCriterion("BuytogetherRealityMoney is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyIsNotNull() {
            addCriterion("BuytogetherRealityMoney is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoney =", value, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyNotEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoney <>", value, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyGreaterThan(Double value) {
            addCriterion("BuytogetherRealityMoney >", value, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyGreaterThanOrEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoney >=", value, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyLessThan(Double value) {
            addCriterion("BuytogetherRealityMoney <", value, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyLessThanOrEqualTo(Double value) {
            addCriterion("BuytogetherRealityMoney <=", value, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyIn(List<Double> values) {
            addCriterion("BuytogetherRealityMoney in", values, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyNotIn(List<Double> values) {
            addCriterion("BuytogetherRealityMoney not in", values, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyBetween(Double value1, Double value2) {
            addCriterion("BuytogetherRealityMoney between", value1, value2, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyNotBetween(Double value1, Double value2) {
            addCriterion("BuytogetherRealityMoney not between", value1, value2, "buytogetherrealitymoney");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyIsNull() {
            addCriterion("BuytogetherRealityMoney_Currency is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyIsNotNull() {
            addCriterion("BuytogetherRealityMoney_Currency is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyEqualTo(Byte value) {
            addCriterion("BuytogetherRealityMoney_Currency =", value, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyNotEqualTo(Byte value) {
            addCriterion("BuytogetherRealityMoney_Currency <>", value, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyGreaterThan(Byte value) {
            addCriterion("BuytogetherRealityMoney_Currency >", value, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyGreaterThanOrEqualTo(Byte value) {
            addCriterion("BuytogetherRealityMoney_Currency >=", value, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyLessThan(Byte value) {
            addCriterion("BuytogetherRealityMoney_Currency <", value, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyLessThanOrEqualTo(Byte value) {
            addCriterion("BuytogetherRealityMoney_Currency <=", value, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyIn(List<Byte> values) {
            addCriterion("BuytogetherRealityMoney_Currency in", values, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyNotIn(List<Byte> values) {
            addCriterion("BuytogetherRealityMoney_Currency not in", values, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyBetween(Byte value1, Byte value2) {
            addCriterion("BuytogetherRealityMoney_Currency between", value1, value2, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andBuytogetherrealitymoneyCurrencyNotBetween(Byte value1, Byte value2) {
            addCriterion("BuytogetherRealityMoney_Currency not between", value1, value2, "buytogetherrealitymoneyCurrency");
            return (Criteria) this;
        }

        public Criteria andCorprankCountIsNull() {
            addCriterion("Corprank_Count is null");
            return (Criteria) this;
        }

        public Criteria andCorprankCountIsNotNull() {
            addCriterion("Corprank_Count is not null");
            return (Criteria) this;
        }

        public Criteria andCorprankCountEqualTo(Short value) {
            addCriterion("Corprank_Count =", value, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountNotEqualTo(Short value) {
            addCriterion("Corprank_Count <>", value, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountGreaterThan(Short value) {
            addCriterion("Corprank_Count >", value, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountGreaterThanOrEqualTo(Short value) {
            addCriterion("Corprank_Count >=", value, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountLessThan(Short value) {
            addCriterion("Corprank_Count <", value, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountLessThanOrEqualTo(Short value) {
            addCriterion("Corprank_Count <=", value, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountIn(List<Short> values) {
            addCriterion("Corprank_Count in", values, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountNotIn(List<Short> values) {
            addCriterion("Corprank_Count not in", values, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountBetween(Short value1, Short value2) {
            addCriterion("Corprank_Count between", value1, value2, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andCorprankCountNotBetween(Short value1, Short value2) {
            addCriterion("Corprank_Count not between", value1, value2, "corprankCount");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameIsNull() {
            addCriterion("Last_Corprank_CnName is null");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameIsNotNull() {
            addCriterion("Last_Corprank_CnName is not null");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameEqualTo(String value) {
            addCriterion("Last_Corprank_CnName =", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameNotEqualTo(String value) {
            addCriterion("Last_Corprank_CnName <>", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameGreaterThan(String value) {
            addCriterion("Last_Corprank_CnName >", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Last_Corprank_CnName >=", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameLessThan(String value) {
            addCriterion("Last_Corprank_CnName <", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameLessThanOrEqualTo(String value) {
            addCriterion("Last_Corprank_CnName <=", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameLike(String value) {
            addCriterion("Last_Corprank_CnName like", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameNotLike(String value) {
            addCriterion("Last_Corprank_CnName not like", value, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameIn(List<String> values) {
            addCriterion("Last_Corprank_CnName in", values, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameNotIn(List<String> values) {
            addCriterion("Last_Corprank_CnName not in", values, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameBetween(String value1, String value2) {
            addCriterion("Last_Corprank_CnName between", value1, value2, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankCnnameNotBetween(String value1, String value2) {
            addCriterion("Last_Corprank_CnName not between", value1, value2, "lastCorprankCnname");
            return (Criteria) this;
        }

        public Criteria andIndustryIdIsNull() {
            addCriterion("Industry_Id is null");
            return (Criteria) this;
        }

        public Criteria andIndustryIdIsNotNull() {
            addCriterion("Industry_Id is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryIdEqualTo(String value) {
            addCriterion("Industry_Id =", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdNotEqualTo(String value) {
            addCriterion("Industry_Id <>", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdGreaterThan(String value) {
            addCriterion("Industry_Id >", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdGreaterThanOrEqualTo(String value) {
            addCriterion("Industry_Id >=", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdLessThan(String value) {
            addCriterion("Industry_Id <", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdLessThanOrEqualTo(String value) {
            addCriterion("Industry_Id <=", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdLike(String value) {
            addCriterion("Industry_Id like", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdNotLike(String value) {
            addCriterion("Industry_Id not like", value, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdIn(List<String> values) {
            addCriterion("Industry_Id in", values, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdNotIn(List<String> values) {
            addCriterion("Industry_Id not in", values, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdBetween(String value1, String value2) {
            addCriterion("Industry_Id between", value1, value2, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryIdNotBetween(String value1, String value2) {
            addCriterion("Industry_Id not between", value1, value2, "industryId");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameIsNull() {
            addCriterion("Industry_EnName is null");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameIsNotNull() {
            addCriterion("Industry_EnName is not null");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameEqualTo(String value) {
            addCriterion("Industry_EnName =", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameNotEqualTo(String value) {
            addCriterion("Industry_EnName <>", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameGreaterThan(String value) {
            addCriterion("Industry_EnName >", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Industry_EnName >=", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameLessThan(String value) {
            addCriterion("Industry_EnName <", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameLessThanOrEqualTo(String value) {
            addCriterion("Industry_EnName <=", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameLike(String value) {
            addCriterion("Industry_EnName like", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameNotLike(String value) {
            addCriterion("Industry_EnName not like", value, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameIn(List<String> values) {
            addCriterion("Industry_EnName in", values, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameNotIn(List<String> values) {
            addCriterion("Industry_EnName not in", values, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameBetween(String value1, String value2) {
            addCriterion("Industry_EnName between", value1, value2, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andIndustryEnnameNotBetween(String value1, String value2) {
            addCriterion("Industry_EnName not between", value1, value2, "industryEnname");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdIsNull() {
            addCriterion("Last_Corprank_Id is null");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdIsNotNull() {
            addCriterion("Last_Corprank_Id is not null");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdEqualTo(Integer value) {
            addCriterion("Last_Corprank_Id =", value, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdNotEqualTo(Integer value) {
            addCriterion("Last_Corprank_Id <>", value, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdGreaterThan(Integer value) {
            addCriterion("Last_Corprank_Id >", value, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Last_Corprank_Id >=", value, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdLessThan(Integer value) {
            addCriterion("Last_Corprank_Id <", value, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdLessThanOrEqualTo(Integer value) {
            addCriterion("Last_Corprank_Id <=", value, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdIn(List<Integer> values) {
            addCriterion("Last_Corprank_Id in", values, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdNotIn(List<Integer> values) {
            addCriterion("Last_Corprank_Id not in", values, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdBetween(Integer value1, Integer value2) {
            addCriterion("Last_Corprank_Id between", value1, value2, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andLastCorprankIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Last_Corprank_Id not between", value1, value2, "lastCorprankId");
            return (Criteria) this;
        }

        public Criteria andIsEventIsNull() {
            addCriterion("Is_Event is null");
            return (Criteria) this;
        }

        public Criteria andIsEventIsNotNull() {
            addCriterion("Is_Event is not null");
            return (Criteria) this;
        }

        public Criteria andIsEventEqualTo(Byte value) {
            addCriterion("Is_Event =", value, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventNotEqualTo(Byte value) {
            addCriterion("Is_Event <>", value, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventGreaterThan(Byte value) {
            addCriterion("Is_Event >", value, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventGreaterThanOrEqualTo(Byte value) {
            addCriterion("Is_Event >=", value, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventLessThan(Byte value) {
            addCriterion("Is_Event <", value, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventLessThanOrEqualTo(Byte value) {
            addCriterion("Is_Event <=", value, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventIn(List<Byte> values) {
            addCriterion("Is_Event in", values, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventNotIn(List<Byte> values) {
            addCriterion("Is_Event not in", values, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventBetween(Byte value1, Byte value2) {
            addCriterion("Is_Event between", value1, value2, "isEvent");
            return (Criteria) this;
        }

        public Criteria andIsEventNotBetween(Byte value1, Byte value2) {
            addCriterion("Is_Event not between", value1, value2, "isEvent");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameIsNull() {
            addCriterion("Exchange_EnName is null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameIsNotNull() {
            addCriterion("Exchange_EnName is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameEqualTo(String value) {
            addCriterion("Exchange_EnName =", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameNotEqualTo(String value) {
            addCriterion("Exchange_EnName <>", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameGreaterThan(String value) {
            addCriterion("Exchange_EnName >", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Exchange_EnName >=", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameLessThan(String value) {
            addCriterion("Exchange_EnName <", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameLessThanOrEqualTo(String value) {
            addCriterion("Exchange_EnName <=", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameLike(String value) {
            addCriterion("Exchange_EnName like", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameNotLike(String value) {
            addCriterion("Exchange_EnName not like", value, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameIn(List<String> values) {
            addCriterion("Exchange_EnName in", values, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameNotIn(List<String> values) {
            addCriterion("Exchange_EnName not in", values, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameBetween(String value1, String value2) {
            addCriterion("Exchange_EnName between", value1, value2, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andExchangeEnnameNotBetween(String value1, String value2) {
            addCriterion("Exchange_EnName not between", value1, value2, "exchangeEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameIsNull() {
            addCriterion("BuytogetherEnterprise_EnName is null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameIsNotNull() {
            addCriterion("BuytogetherEnterprise_EnName is not null");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_EnName =", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameNotEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_EnName <>", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameGreaterThan(String value) {
            addCriterion("BuytogetherEnterprise_EnName >", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameGreaterThanOrEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_EnName >=", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameLessThan(String value) {
            addCriterion("BuytogetherEnterprise_EnName <", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameLessThanOrEqualTo(String value) {
            addCriterion("BuytogetherEnterprise_EnName <=", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameLike(String value) {
            addCriterion("BuytogetherEnterprise_EnName like", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameNotLike(String value) {
            addCriterion("BuytogetherEnterprise_EnName not like", value, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameIn(List<String> values) {
            addCriterion("BuytogetherEnterprise_EnName in", values, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameNotIn(List<String> values) {
            addCriterion("BuytogetherEnterprise_EnName not in", values, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameBetween(String value1, String value2) {
            addCriterion("BuytogetherEnterprise_EnName between", value1, value2, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andBuytogetherenterpriseEnnameNotBetween(String value1, String value2) {
            addCriterion("BuytogetherEnterprise_EnName not between", value1, value2, "buytogetherenterpriseEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameIsNull() {
            addCriterion("FinancingEventType_EnName is null");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameIsNotNull() {
            addCriterion("FinancingEventType_EnName is not null");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameEqualTo(String value) {
            addCriterion("FinancingEventType_EnName =", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameNotEqualTo(String value) {
            addCriterion("FinancingEventType_EnName <>", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameGreaterThan(String value) {
            addCriterion("FinancingEventType_EnName >", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameGreaterThanOrEqualTo(String value) {
            addCriterion("FinancingEventType_EnName >=", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameLessThan(String value) {
            addCriterion("FinancingEventType_EnName <", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameLessThanOrEqualTo(String value) {
            addCriterion("FinancingEventType_EnName <=", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameLike(String value) {
            addCriterion("FinancingEventType_EnName like", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameNotLike(String value) {
            addCriterion("FinancingEventType_EnName not like", value, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameIn(List<String> values) {
            addCriterion("FinancingEventType_EnName in", values, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameNotIn(List<String> values) {
            addCriterion("FinancingEventType_EnName not in", values, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameBetween(String value1, String value2) {
            addCriterion("FinancingEventType_EnName between", value1, value2, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andFinancingeventtypeEnnameNotBetween(String value1, String value2) {
            addCriterion("FinancingEventType_EnName not between", value1, value2, "financingeventtypeEnname");
            return (Criteria) this;
        }

        public Criteria andPdfPathIsNull() {
            addCriterion("PDF_Path is null");
            return (Criteria) this;
        }

        public Criteria andPdfPathIsNotNull() {
            addCriterion("PDF_Path is not null");
            return (Criteria) this;
        }

        public Criteria andPdfPathEqualTo(String value) {
            addCriterion("PDF_Path =", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathNotEqualTo(String value) {
            addCriterion("PDF_Path <>", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathGreaterThan(String value) {
            addCriterion("PDF_Path >", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathGreaterThanOrEqualTo(String value) {
            addCriterion("PDF_Path >=", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathLessThan(String value) {
            addCriterion("PDF_Path <", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathLessThanOrEqualTo(String value) {
            addCriterion("PDF_Path <=", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathLike(String value) {
            addCriterion("PDF_Path like", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathNotLike(String value) {
            addCriterion("PDF_Path not like", value, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathIn(List<String> values) {
            addCriterion("PDF_Path in", values, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathNotIn(List<String> values) {
            addCriterion("PDF_Path not in", values, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathBetween(String value1, String value2) {
            addCriterion("PDF_Path between", value1, value2, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andPdfPathNotBetween(String value1, String value2) {
            addCriterion("PDF_Path not between", value1, value2, "pdfPath");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeIsNull() {
            addCriterion("Organize_Code is null");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeIsNotNull() {
            addCriterion("Organize_Code is not null");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeEqualTo(String value) {
            addCriterion("Organize_Code =", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeNotEqualTo(String value) {
            addCriterion("Organize_Code <>", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeGreaterThan(String value) {
            addCriterion("Organize_Code >", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("Organize_Code >=", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeLessThan(String value) {
            addCriterion("Organize_Code <", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeLessThanOrEqualTo(String value) {
            addCriterion("Organize_Code <=", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeLike(String value) {
            addCriterion("Organize_Code like", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeNotLike(String value) {
            addCriterion("Organize_Code not like", value, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeIn(List<String> values) {
            addCriterion("Organize_Code in", values, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeNotIn(List<String> values) {
            addCriterion("Organize_Code not in", values, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeBetween(String value1, String value2) {
            addCriterion("Organize_Code between", value1, value2, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andOrganizeCodeNotBetween(String value1, String value2) {
            addCriterion("Organize_Code not between", value1, value2, "organizeCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNull() {
            addCriterion("Stock_Code is null");
            return (Criteria) this;
        }

        public Criteria andStockCodeIsNotNull() {
            addCriterion("Stock_Code is not null");
            return (Criteria) this;
        }

        public Criteria andStockCodeEqualTo(String value) {
            addCriterion("Stock_Code =", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotEqualTo(String value) {
            addCriterion("Stock_Code <>", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThan(String value) {
            addCriterion("Stock_Code >", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeGreaterThanOrEqualTo(String value) {
            addCriterion("Stock_Code >=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThan(String value) {
            addCriterion("Stock_Code <", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLessThanOrEqualTo(String value) {
            addCriterion("Stock_Code <=", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeLike(String value) {
            addCriterion("Stock_Code like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotLike(String value) {
            addCriterion("Stock_Code not like", value, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeIn(List<String> values) {
            addCriterion("Stock_Code in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotIn(List<String> values) {
            addCriterion("Stock_Code not in", values, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeBetween(String value1, String value2) {
            addCriterion("Stock_Code between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andStockCodeNotBetween(String value1, String value2) {
            addCriterion("Stock_Code not between", value1, value2, "stockCode");
            return (Criteria) this;
        }

        public Criteria andCharacterIdIsNull() {
            addCriterion("Character_ID is null");
            return (Criteria) this;
        }

        public Criteria andCharacterIdIsNotNull() {
            addCriterion("Character_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterIdEqualTo(Byte value) {
            addCriterion("Character_ID =", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdNotEqualTo(Byte value) {
            addCriterion("Character_ID <>", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdGreaterThan(Byte value) {
            addCriterion("Character_ID >", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("Character_ID >=", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdLessThan(Byte value) {
            addCriterion("Character_ID <", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdLessThanOrEqualTo(Byte value) {
            addCriterion("Character_ID <=", value, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdIn(List<Byte> values) {
            addCriterion("Character_ID in", values, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdNotIn(List<Byte> values) {
            addCriterion("Character_ID not in", values, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdBetween(Byte value1, Byte value2) {
            addCriterion("Character_ID between", value1, value2, "characterId");
            return (Criteria) this;
        }

        public Criteria andCharacterIdNotBetween(Byte value1, Byte value2) {
            addCriterion("Character_ID not between", value1, value2, "characterId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNull() {
            addCriterion("Source_ID is null");
            return (Criteria) this;
        }

        public Criteria andSourceIdIsNotNull() {
            addCriterion("Source_ID is not null");
            return (Criteria) this;
        }

        public Criteria andSourceIdEqualTo(Byte value) {
            addCriterion("Source_ID =", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotEqualTo(Byte value) {
            addCriterion("Source_ID <>", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThan(Byte value) {
            addCriterion("Source_ID >", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("Source_ID >=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThan(Byte value) {
            addCriterion("Source_ID <", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdLessThanOrEqualTo(Byte value) {
            addCriterion("Source_ID <=", value, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdIn(List<Byte> values) {
            addCriterion("Source_ID in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotIn(List<Byte> values) {
            addCriterion("Source_ID not in", values, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdBetween(Byte value1, Byte value2) {
            addCriterion("Source_ID between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andSourceIdNotBetween(Byte value1, Byte value2) {
            addCriterion("Source_ID not between", value1, value2, "sourceId");
            return (Criteria) this;
        }

        public Criteria andQincodeIsNull() {
            addCriterion("QinCode is null");
            return (Criteria) this;
        }

        public Criteria andQincodeIsNotNull() {
            addCriterion("QinCode is not null");
            return (Criteria) this;
        }

        public Criteria andQincodeEqualTo(String value) {
            addCriterion("QinCode =", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeNotEqualTo(String value) {
            addCriterion("QinCode <>", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeGreaterThan(String value) {
            addCriterion("QinCode >", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeGreaterThanOrEqualTo(String value) {
            addCriterion("QinCode >=", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeLessThan(String value) {
            addCriterion("QinCode <", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeLessThanOrEqualTo(String value) {
            addCriterion("QinCode <=", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeLike(String value) {
            addCriterion("QinCode like", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeNotLike(String value) {
            addCriterion("QinCode not like", value, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeIn(List<String> values) {
            addCriterion("QinCode in", values, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeNotIn(List<String> values) {
            addCriterion("QinCode not in", values, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeBetween(String value1, String value2) {
            addCriterion("QinCode between", value1, value2, "qincode");
            return (Criteria) this;
        }

        public Criteria andQincodeNotBetween(String value1, String value2) {
            addCriterion("QinCode not between", value1, value2, "qincode");
            return (Criteria) this;
        }

        public Criteria andInternateOneIsNull() {
            addCriterion("Internate_One is null");
            return (Criteria) this;
        }

        public Criteria andInternateOneIsNotNull() {
            addCriterion("Internate_One is not null");
            return (Criteria) this;
        }

        public Criteria andInternateOneEqualTo(Short value) {
            addCriterion("Internate_One =", value, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneNotEqualTo(Short value) {
            addCriterion("Internate_One <>", value, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneGreaterThan(Short value) {
            addCriterion("Internate_One >", value, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneGreaterThanOrEqualTo(Short value) {
            addCriterion("Internate_One >=", value, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneLessThan(Short value) {
            addCriterion("Internate_One <", value, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneLessThanOrEqualTo(Short value) {
            addCriterion("Internate_One <=", value, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneIn(List<Short> values) {
            addCriterion("Internate_One in", values, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneNotIn(List<Short> values) {
            addCriterion("Internate_One not in", values, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneBetween(Short value1, Short value2) {
            addCriterion("Internate_One between", value1, value2, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateOneNotBetween(Short value1, Short value2) {
            addCriterion("Internate_One not between", value1, value2, "internateOne");
            return (Criteria) this;
        }

        public Criteria andInternateTwoIsNull() {
            addCriterion("Internate_Two is null");
            return (Criteria) this;
        }

        public Criteria andInternateTwoIsNotNull() {
            addCriterion("Internate_Two is not null");
            return (Criteria) this;
        }

        public Criteria andInternateTwoEqualTo(Short value) {
            addCriterion("Internate_Two =", value, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoNotEqualTo(Short value) {
            addCriterion("Internate_Two <>", value, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoGreaterThan(Short value) {
            addCriterion("Internate_Two >", value, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoGreaterThanOrEqualTo(Short value) {
            addCriterion("Internate_Two >=", value, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoLessThan(Short value) {
            addCriterion("Internate_Two <", value, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoLessThanOrEqualTo(Short value) {
            addCriterion("Internate_Two <=", value, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoIn(List<Short> values) {
            addCriterion("Internate_Two in", values, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoNotIn(List<Short> values) {
            addCriterion("Internate_Two not in", values, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoBetween(Short value1, Short value2) {
            addCriterion("Internate_Two between", value1, value2, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateTwoNotBetween(Short value1, Short value2) {
            addCriterion("Internate_Two not between", value1, value2, "internateTwo");
            return (Criteria) this;
        }

        public Criteria andInternateThreeIsNull() {
            addCriterion("Internate_Three is null");
            return (Criteria) this;
        }

        public Criteria andInternateThreeIsNotNull() {
            addCriterion("Internate_Three is not null");
            return (Criteria) this;
        }

        public Criteria andInternateThreeEqualTo(Short value) {
            addCriterion("Internate_Three =", value, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeNotEqualTo(Short value) {
            addCriterion("Internate_Three <>", value, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeGreaterThan(Short value) {
            addCriterion("Internate_Three >", value, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeGreaterThanOrEqualTo(Short value) {
            addCriterion("Internate_Three >=", value, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeLessThan(Short value) {
            addCriterion("Internate_Three <", value, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeLessThanOrEqualTo(Short value) {
            addCriterion("Internate_Three <=", value, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeIn(List<Short> values) {
            addCriterion("Internate_Three in", values, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeNotIn(List<Short> values) {
            addCriterion("Internate_Three not in", values, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeBetween(Short value1, Short value2) {
            addCriterion("Internate_Three between", value1, value2, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateThreeNotBetween(Short value1, Short value2) {
            addCriterion("Internate_Three not between", value1, value2, "internateThree");
            return (Criteria) this;
        }

        public Criteria andInternateFourIsNull() {
            addCriterion("Internate_Four is null");
            return (Criteria) this;
        }

        public Criteria andInternateFourIsNotNull() {
            addCriterion("Internate_Four is not null");
            return (Criteria) this;
        }

        public Criteria andInternateFourEqualTo(Short value) {
            addCriterion("Internate_Four =", value, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourNotEqualTo(Short value) {
            addCriterion("Internate_Four <>", value, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourGreaterThan(Short value) {
            addCriterion("Internate_Four >", value, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourGreaterThanOrEqualTo(Short value) {
            addCriterion("Internate_Four >=", value, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourLessThan(Short value) {
            addCriterion("Internate_Four <", value, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourLessThanOrEqualTo(Short value) {
            addCriterion("Internate_Four <=", value, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourIn(List<Short> values) {
            addCriterion("Internate_Four in", values, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourNotIn(List<Short> values) {
            addCriterion("Internate_Four not in", values, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourBetween(Short value1, Short value2) {
            addCriterion("Internate_Four between", value1, value2, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateFourNotBetween(Short value1, Short value2) {
            addCriterion("Internate_Four not between", value1, value2, "internateFour");
            return (Criteria) this;
        }

        public Criteria andInternateIdIsNull() {
            addCriterion("Internate_Id is null");
            return (Criteria) this;
        }

        public Criteria andInternateIdIsNotNull() {
            addCriterion("Internate_Id is not null");
            return (Criteria) this;
        }

        public Criteria andInternateIdEqualTo(String value) {
            addCriterion("Internate_Id =", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdNotEqualTo(String value) {
            addCriterion("Internate_Id <>", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdGreaterThan(String value) {
            addCriterion("Internate_Id >", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdGreaterThanOrEqualTo(String value) {
            addCriterion("Internate_Id >=", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdLessThan(String value) {
            addCriterion("Internate_Id <", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdLessThanOrEqualTo(String value) {
            addCriterion("Internate_Id <=", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdLike(String value) {
            addCriterion("Internate_Id like", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdNotLike(String value) {
            addCriterion("Internate_Id not like", value, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdIn(List<String> values) {
            addCriterion("Internate_Id in", values, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdNotIn(List<String> values) {
            addCriterion("Internate_Id not in", values, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdBetween(String value1, String value2) {
            addCriterion("Internate_Id between", value1, value2, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateIdNotBetween(String value1, String value2) {
            addCriterion("Internate_Id not between", value1, value2, "internateId");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameIsNull() {
            addCriterion("Internate_CnName is null");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameIsNotNull() {
            addCriterion("Internate_CnName is not null");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameEqualTo(String value) {
            addCriterion("Internate_CnName =", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameNotEqualTo(String value) {
            addCriterion("Internate_CnName <>", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameGreaterThan(String value) {
            addCriterion("Internate_CnName >", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Internate_CnName >=", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameLessThan(String value) {
            addCriterion("Internate_CnName <", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameLessThanOrEqualTo(String value) {
            addCriterion("Internate_CnName <=", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameLike(String value) {
            addCriterion("Internate_CnName like", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameNotLike(String value) {
            addCriterion("Internate_CnName not like", value, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameIn(List<String> values) {
            addCriterion("Internate_CnName in", values, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameNotIn(List<String> values) {
            addCriterion("Internate_CnName not in", values, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameBetween(String value1, String value2) {
            addCriterion("Internate_CnName between", value1, value2, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateCnnameNotBetween(String value1, String value2) {
            addCriterion("Internate_CnName not between", value1, value2, "internateCnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameIsNull() {
            addCriterion("Internate_EnName is null");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameIsNotNull() {
            addCriterion("Internate_EnName is not null");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameEqualTo(String value) {
            addCriterion("Internate_EnName =", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameNotEqualTo(String value) {
            addCriterion("Internate_EnName <>", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameGreaterThan(String value) {
            addCriterion("Internate_EnName >", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameGreaterThanOrEqualTo(String value) {
            addCriterion("Internate_EnName >=", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameLessThan(String value) {
            addCriterion("Internate_EnName <", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameLessThanOrEqualTo(String value) {
            addCriterion("Internate_EnName <=", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameLike(String value) {
            addCriterion("Internate_EnName like", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameNotLike(String value) {
            addCriterion("Internate_EnName not like", value, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameIn(List<String> values) {
            addCriterion("Internate_EnName in", values, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameNotIn(List<String> values) {
            addCriterion("Internate_EnName not in", values, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameBetween(String value1, String value2) {
            addCriterion("Internate_EnName between", value1, value2, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andInternateEnnameNotBetween(String value1, String value2) {
            addCriterion("Internate_EnName not between", value1, value2, "internateEnname");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNull() {
            addCriterion("Employee_Number is null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIsNotNull() {
            addCriterion("Employee_Number is not null");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberEqualTo(Double value) {
            addCriterion("Employee_Number =", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotEqualTo(Double value) {
            addCriterion("Employee_Number <>", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThan(Double value) {
            addCriterion("Employee_Number >", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberGreaterThanOrEqualTo(Double value) {
            addCriterion("Employee_Number >=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThan(Double value) {
            addCriterion("Employee_Number <", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberLessThanOrEqualTo(Double value) {
            addCriterion("Employee_Number <=", value, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberIn(List<Double> values) {
            addCriterion("Employee_Number in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotIn(List<Double> values) {
            addCriterion("Employee_Number not in", values, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberBetween(Double value1, Double value2) {
            addCriterion("Employee_Number between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andEmployeeNumberNotBetween(Double value1, Double value2) {
            addCriterion("Employee_Number not between", value1, value2, "employeeNumber");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsIsNull() {
            addCriterion("Income_operations is null");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsIsNotNull() {
            addCriterion("Income_operations is not null");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsEqualTo(Double value) {
            addCriterion("Income_operations =", value, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsNotEqualTo(Double value) {
            addCriterion("Income_operations <>", value, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsGreaterThan(Double value) {
            addCriterion("Income_operations >", value, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsGreaterThanOrEqualTo(Double value) {
            addCriterion("Income_operations >=", value, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsLessThan(Double value) {
            addCriterion("Income_operations <", value, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsLessThanOrEqualTo(Double value) {
            addCriterion("Income_operations <=", value, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsIn(List<Double> values) {
            addCriterion("Income_operations in", values, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsNotIn(List<Double> values) {
            addCriterion("Income_operations not in", values, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsBetween(Double value1, Double value2) {
            addCriterion("Income_operations between", value1, value2, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andIncomeOperationsNotBetween(Double value1, Double value2) {
            addCriterion("Income_operations not between", value1, value2, "incomeOperations");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNull() {
            addCriterion("Total_Assets is null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIsNotNull() {
            addCriterion("Total_Assets is not null");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsEqualTo(Double value) {
            addCriterion("Total_Assets =", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotEqualTo(Double value) {
            addCriterion("Total_Assets <>", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThan(Double value) {
            addCriterion("Total_Assets >", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsGreaterThanOrEqualTo(Double value) {
            addCriterion("Total_Assets >=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThan(Double value) {
            addCriterion("Total_Assets <", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsLessThanOrEqualTo(Double value) {
            addCriterion("Total_Assets <=", value, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsIn(List<Double> values) {
            addCriterion("Total_Assets in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotIn(List<Double> values) {
            addCriterion("Total_Assets not in", values, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsBetween(Double value1, Double value2) {
            addCriterion("Total_Assets between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andTotalAssetsNotBetween(Double value1, Double value2) {
            addCriterion("Total_Assets not between", value1, value2, "totalAssets");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIsNull() {
            addCriterion("Exchange_Id is null");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIsNotNull() {
            addCriterion("Exchange_Id is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeIdEqualTo(String value) {
            addCriterion("Exchange_Id =", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotEqualTo(String value) {
            addCriterion("Exchange_Id <>", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdGreaterThan(String value) {
            addCriterion("Exchange_Id >", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdGreaterThanOrEqualTo(String value) {
            addCriterion("Exchange_Id >=", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLessThan(String value) {
            addCriterion("Exchange_Id <", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLessThanOrEqualTo(String value) {
            addCriterion("Exchange_Id <=", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdLike(String value) {
            addCriterion("Exchange_Id like", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotLike(String value) {
            addCriterion("Exchange_Id not like", value, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdIn(List<String> values) {
            addCriterion("Exchange_Id in", values, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotIn(List<String> values) {
            addCriterion("Exchange_Id not in", values, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdBetween(String value1, String value2) {
            addCriterion("Exchange_Id between", value1, value2, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andExchangeIdNotBetween(String value1, String value2) {
            addCriterion("Exchange_Id not between", value1, value2, "exchangeId");
            return (Criteria) this;
        }

        public Criteria andTotalEquityIsNull() {
            addCriterion("Total_Equity is null");
            return (Criteria) this;
        }

        public Criteria andTotalEquityIsNotNull() {
            addCriterion("Total_Equity is not null");
            return (Criteria) this;
        }

        public Criteria andTotalEquityEqualTo(Double value) {
            addCriterion("Total_Equity =", value, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityNotEqualTo(Double value) {
            addCriterion("Total_Equity <>", value, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityGreaterThan(Double value) {
            addCriterion("Total_Equity >", value, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityGreaterThanOrEqualTo(Double value) {
            addCriterion("Total_Equity >=", value, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityLessThan(Double value) {
            addCriterion("Total_Equity <", value, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityLessThanOrEqualTo(Double value) {
            addCriterion("Total_Equity <=", value, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityIn(List<Double> values) {
            addCriterion("Total_Equity in", values, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityNotIn(List<Double> values) {
            addCriterion("Total_Equity not in", values, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityBetween(Double value1, Double value2) {
            addCriterion("Total_Equity between", value1, value2, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalEquityNotBetween(Double value1, Double value2) {
            addCriterion("Total_Equity not between", value1, value2, "totalEquity");
            return (Criteria) this;
        }

        public Criteria andTotalProfitIsNull() {
            addCriterion("Total_Profit is null");
            return (Criteria) this;
        }

        public Criteria andTotalProfitIsNotNull() {
            addCriterion("Total_Profit is not null");
            return (Criteria) this;
        }

        public Criteria andTotalProfitEqualTo(Double value) {
            addCriterion("Total_Profit =", value, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitNotEqualTo(Double value) {
            addCriterion("Total_Profit <>", value, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitGreaterThan(Double value) {
            addCriterion("Total_Profit >", value, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitGreaterThanOrEqualTo(Double value) {
            addCriterion("Total_Profit >=", value, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitLessThan(Double value) {
            addCriterion("Total_Profit <", value, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitLessThanOrEqualTo(Double value) {
            addCriterion("Total_Profit <=", value, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitIn(List<Double> values) {
            addCriterion("Total_Profit in", values, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitNotIn(List<Double> values) {
            addCriterion("Total_Profit not in", values, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitBetween(Double value1, Double value2) {
            addCriterion("Total_Profit between", value1, value2, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andTotalProfitNotBetween(Double value1, Double value2) {
            addCriterion("Total_Profit not between", value1, value2, "totalProfit");
            return (Criteria) this;
        }

        public Criteria andRoeIsNull() {
            addCriterion("ROE is null");
            return (Criteria) this;
        }

        public Criteria andRoeIsNotNull() {
            addCriterion("ROE is not null");
            return (Criteria) this;
        }

        public Criteria andRoeEqualTo(Double value) {
            addCriterion("ROE =", value, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeNotEqualTo(Double value) {
            addCriterion("ROE <>", value, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeGreaterThan(Double value) {
            addCriterion("ROE >", value, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeGreaterThanOrEqualTo(Double value) {
            addCriterion("ROE >=", value, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeLessThan(Double value) {
            addCriterion("ROE <", value, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeLessThanOrEqualTo(Double value) {
            addCriterion("ROE <=", value, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeIn(List<Double> values) {
            addCriterion("ROE in", values, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeNotIn(List<Double> values) {
            addCriterion("ROE not in", values, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeBetween(Double value1, Double value2) {
            addCriterion("ROE between", value1, value2, "roe");
            return (Criteria) this;
        }

        public Criteria andRoeNotBetween(Double value1, Double value2) {
            addCriterion("ROE not between", value1, value2, "roe");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitIsNull() {
            addCriterion("Sell_Gross_Profit is null");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitIsNotNull() {
            addCriterion("Sell_Gross_Profit is not null");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitEqualTo(Double value) {
            addCriterion("Sell_Gross_Profit =", value, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitNotEqualTo(Double value) {
            addCriterion("Sell_Gross_Profit <>", value, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitGreaterThan(Double value) {
            addCriterion("Sell_Gross_Profit >", value, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitGreaterThanOrEqualTo(Double value) {
            addCriterion("Sell_Gross_Profit >=", value, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitLessThan(Double value) {
            addCriterion("Sell_Gross_Profit <", value, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitLessThanOrEqualTo(Double value) {
            addCriterion("Sell_Gross_Profit <=", value, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitIn(List<Double> values) {
            addCriterion("Sell_Gross_Profit in", values, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitNotIn(List<Double> values) {
            addCriterion("Sell_Gross_Profit not in", values, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitBetween(Double value1, Double value2) {
            addCriterion("Sell_Gross_Profit between", value1, value2, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andSellGrossProfitNotBetween(Double value1, Double value2) {
            addCriterion("Sell_Gross_Profit not between", value1, value2, "sellGrossProfit");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeIsNull() {
            addCriterion("enterpriseType is null");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeIsNotNull() {
            addCriterion("enterpriseType is not null");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeEqualTo(String value) {
            addCriterion("enterpriseType =", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeNotEqualTo(String value) {
            addCriterion("enterpriseType <>", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeGreaterThan(String value) {
            addCriterion("enterpriseType >", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeGreaterThanOrEqualTo(String value) {
            addCriterion("enterpriseType >=", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeLessThan(String value) {
            addCriterion("enterpriseType <", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeLessThanOrEqualTo(String value) {
            addCriterion("enterpriseType <=", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeLike(String value) {
            addCriterion("enterpriseType like", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeNotLike(String value) {
            addCriterion("enterpriseType not like", value, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeIn(List<String> values) {
            addCriterion("enterpriseType in", values, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeNotIn(List<String> values) {
            addCriterion("enterpriseType not in", values, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeBetween(String value1, String value2) {
            addCriterion("enterpriseType between", value1, value2, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andEnterprisetypeNotBetween(String value1, String value2) {
            addCriterion("enterpriseType not between", value1, value2, "enterprisetype");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateIsNull() {
            addCriterion("Sell_Average_Rate is null");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateIsNotNull() {
            addCriterion("Sell_Average_Rate is not null");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateEqualTo(Double value) {
            addCriterion("Sell_Average_Rate =", value, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateNotEqualTo(Double value) {
            addCriterion("Sell_Average_Rate <>", value, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateGreaterThan(Double value) {
            addCriterion("Sell_Average_Rate >", value, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateGreaterThanOrEqualTo(Double value) {
            addCriterion("Sell_Average_Rate >=", value, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateLessThan(Double value) {
            addCriterion("Sell_Average_Rate <", value, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateLessThanOrEqualTo(Double value) {
            addCriterion("Sell_Average_Rate <=", value, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateIn(List<Double> values) {
            addCriterion("Sell_Average_Rate in", values, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateNotIn(List<Double> values) {
            addCriterion("Sell_Average_Rate not in", values, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateBetween(Double value1, Double value2) {
            addCriterion("Sell_Average_Rate between", value1, value2, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andSellAverageRateNotBetween(Double value1, Double value2) {
            addCriterion("Sell_Average_Rate not between", value1, value2, "sellAverageRate");
            return (Criteria) this;
        }

        public Criteria andNetIsNull() {
            addCriterion("net is null");
            return (Criteria) this;
        }

        public Criteria andNetIsNotNull() {
            addCriterion("net is not null");
            return (Criteria) this;
        }

        public Criteria andNetEqualTo(Double value) {
            addCriterion("net =", value, "net");
            return (Criteria) this;
        }

        public Criteria andNetNotEqualTo(Double value) {
            addCriterion("net <>", value, "net");
            return (Criteria) this;
        }

        public Criteria andNetGreaterThan(Double value) {
            addCriterion("net >", value, "net");
            return (Criteria) this;
        }

        public Criteria andNetGreaterThanOrEqualTo(Double value) {
            addCriterion("net >=", value, "net");
            return (Criteria) this;
        }

        public Criteria andNetLessThan(Double value) {
            addCriterion("net <", value, "net");
            return (Criteria) this;
        }

        public Criteria andNetLessThanOrEqualTo(Double value) {
            addCriterion("net <=", value, "net");
            return (Criteria) this;
        }

        public Criteria andNetIn(List<Double> values) {
            addCriterion("net in", values, "net");
            return (Criteria) this;
        }

        public Criteria andNetNotIn(List<Double> values) {
            addCriterion("net not in", values, "net");
            return (Criteria) this;
        }

        public Criteria andNetBetween(Double value1, Double value2) {
            addCriterion("net between", value1, value2, "net");
            return (Criteria) this;
        }

        public Criteria andNetNotBetween(Double value1, Double value2) {
            addCriterion("net not between", value1, value2, "net");
            return (Criteria) this;
        }

        public Criteria andSearchtypeIsNull() {
            addCriterion("SearchType is null");
            return (Criteria) this;
        }

        public Criteria andSearchtypeIsNotNull() {
            addCriterion("SearchType is not null");
            return (Criteria) this;
        }

        public Criteria andSearchtypeEqualTo(Byte value) {
            addCriterion("SearchType =", value, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeNotEqualTo(Byte value) {
            addCriterion("SearchType <>", value, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeGreaterThan(Byte value) {
            addCriterion("SearchType >", value, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeGreaterThanOrEqualTo(Byte value) {
            addCriterion("SearchType >=", value, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeLessThan(Byte value) {
            addCriterion("SearchType <", value, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeLessThanOrEqualTo(Byte value) {
            addCriterion("SearchType <=", value, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeIn(List<Byte> values) {
            addCriterion("SearchType in", values, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeNotIn(List<Byte> values) {
            addCriterion("SearchType not in", values, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeBetween(Byte value1, Byte value2) {
            addCriterion("SearchType between", value1, value2, "searchtype");
            return (Criteria) this;
        }

        public Criteria andSearchtypeNotBetween(Byte value1, Byte value2) {
            addCriterion("SearchType not between", value1, value2, "searchtype");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsIsNull() {
            addCriterion("newIncome_operations is null");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsIsNotNull() {
            addCriterion("newIncome_operations is not null");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsEqualTo(Double value) {
            addCriterion("newIncome_operations =", value, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsNotEqualTo(Double value) {
            addCriterion("newIncome_operations <>", value, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsGreaterThan(Double value) {
            addCriterion("newIncome_operations >", value, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsGreaterThanOrEqualTo(Double value) {
            addCriterion("newIncome_operations >=", value, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsLessThan(Double value) {
            addCriterion("newIncome_operations <", value, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsLessThanOrEqualTo(Double value) {
            addCriterion("newIncome_operations <=", value, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsIn(List<Double> values) {
            addCriterion("newIncome_operations in", values, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsNotIn(List<Double> values) {
            addCriterion("newIncome_operations not in", values, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsBetween(Double value1, Double value2) {
            addCriterion("newIncome_operations between", value1, value2, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeOperationsNotBetween(Double value1, Double value2) {
            addCriterion("newIncome_operations not between", value1, value2, "newincomeOperations");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearIsNull() {
            addCriterion("newincome_year is null");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearIsNotNull() {
            addCriterion("newincome_year is not null");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearEqualTo(Date value) {
            addCriterionForJDBCDate("newincome_year =", value, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearNotEqualTo(Date value) {
            addCriterionForJDBCDate("newincome_year <>", value, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearGreaterThan(Date value) {
            addCriterionForJDBCDate("newincome_year >", value, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("newincome_year >=", value, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearLessThan(Date value) {
            addCriterionForJDBCDate("newincome_year <", value, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("newincome_year <=", value, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearIn(List<Date> values) {
            addCriterionForJDBCDate("newincome_year in", values, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearNotIn(List<Date> values) {
            addCriterionForJDBCDate("newincome_year not in", values, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("newincome_year between", value1, value2, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andNewincomeYearNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("newincome_year not between", value1, value2, "newincomeYear");
            return (Criteria) this;
        }

        public Criteria andLegalPersonIsNull() {
            addCriterion("legal_person is null");
            return (Criteria) this;
        }

        public Criteria andLegalPersonIsNotNull() {
            addCriterion("legal_person is not null");
            return (Criteria) this;
        }

        public Criteria andLegalPersonEqualTo(String value) {
            addCriterion("legal_person =", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonNotEqualTo(String value) {
            addCriterion("legal_person <>", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonGreaterThan(String value) {
            addCriterion("legal_person >", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonGreaterThanOrEqualTo(String value) {
            addCriterion("legal_person >=", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonLessThan(String value) {
            addCriterion("legal_person <", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonLessThanOrEqualTo(String value) {
            addCriterion("legal_person <=", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonLike(String value) {
            addCriterion("legal_person like", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonNotLike(String value) {
            addCriterion("legal_person not like", value, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonIn(List<String> values) {
            addCriterion("legal_person in", values, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonNotIn(List<String> values) {
            addCriterion("legal_person not in", values, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonBetween(String value1, String value2) {
            addCriterion("legal_person between", value1, value2, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andLegalPersonNotBetween(String value1, String value2) {
            addCriterion("legal_person not between", value1, value2, "legalPerson");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneIsNull() {
            addCriterion("main_products_one is null");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneIsNotNull() {
            addCriterion("main_products_one is not null");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneEqualTo(String value) {
            addCriterion("main_products_one =", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneNotEqualTo(String value) {
            addCriterion("main_products_one <>", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneGreaterThan(String value) {
            addCriterion("main_products_one >", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneGreaterThanOrEqualTo(String value) {
            addCriterion("main_products_one >=", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneLessThan(String value) {
            addCriterion("main_products_one <", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneLessThanOrEqualTo(String value) {
            addCriterion("main_products_one <=", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneLike(String value) {
            addCriterion("main_products_one like", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneNotLike(String value) {
            addCriterion("main_products_one not like", value, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneIn(List<String> values) {
            addCriterion("main_products_one in", values, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneNotIn(List<String> values) {
            addCriterion("main_products_one not in", values, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneBetween(String value1, String value2) {
            addCriterion("main_products_one between", value1, value2, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsOneNotBetween(String value1, String value2) {
            addCriterion("main_products_one not between", value1, value2, "mainProductsOne");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoIsNull() {
            addCriterion("main_products_two is null");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoIsNotNull() {
            addCriterion("main_products_two is not null");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoEqualTo(String value) {
            addCriterion("main_products_two =", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoNotEqualTo(String value) {
            addCriterion("main_products_two <>", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoGreaterThan(String value) {
            addCriterion("main_products_two >", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoGreaterThanOrEqualTo(String value) {
            addCriterion("main_products_two >=", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoLessThan(String value) {
            addCriterion("main_products_two <", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoLessThanOrEqualTo(String value) {
            addCriterion("main_products_two <=", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoLike(String value) {
            addCriterion("main_products_two like", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoNotLike(String value) {
            addCriterion("main_products_two not like", value, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoIn(List<String> values) {
            addCriterion("main_products_two in", values, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoNotIn(List<String> values) {
            addCriterion("main_products_two not in", values, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoBetween(String value1, String value2) {
            addCriterion("main_products_two between", value1, value2, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsTwoNotBetween(String value1, String value2) {
            addCriterion("main_products_two not between", value1, value2, "mainProductsTwo");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeIsNull() {
            addCriterion("main_products_three is null");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeIsNotNull() {
            addCriterion("main_products_three is not null");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeEqualTo(String value) {
            addCriterion("main_products_three =", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeNotEqualTo(String value) {
            addCriterion("main_products_three <>", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeGreaterThan(String value) {
            addCriterion("main_products_three >", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeGreaterThanOrEqualTo(String value) {
            addCriterion("main_products_three >=", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeLessThan(String value) {
            addCriterion("main_products_three <", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeLessThanOrEqualTo(String value) {
            addCriterion("main_products_three <=", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeLike(String value) {
            addCriterion("main_products_three like", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeNotLike(String value) {
            addCriterion("main_products_three not like", value, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeIn(List<String> values) {
            addCriterion("main_products_three in", values, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeNotIn(List<String> values) {
            addCriterion("main_products_three not in", values, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeBetween(String value1, String value2) {
            addCriterion("main_products_three between", value1, value2, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andMainProductsThreeNotBetween(String value1, String value2) {
            addCriterion("main_products_three not between", value1, value2, "mainProductsThree");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNull() {
            addCriterion("Module_ID is null");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNotNull() {
            addCriterion("Module_ID is not null");
            return (Criteria) this;
        }

        public Criteria andModuleIdEqualTo(Byte value) {
            addCriterion("Module_ID =", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotEqualTo(Byte value) {
            addCriterion("Module_ID <>", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThan(Byte value) {
            addCriterion("Module_ID >", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThanOrEqualTo(Byte value) {
            addCriterion("Module_ID >=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThan(Byte value) {
            addCriterion("Module_ID <", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThanOrEqualTo(Byte value) {
            addCriterion("Module_ID <=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdIn(List<Byte> values) {
            addCriterion("Module_ID in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotIn(List<Byte> values) {
            addCriterion("Module_ID not in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdBetween(Byte value1, Byte value2) {
            addCriterion("Module_ID between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotBetween(Byte value1, Byte value2) {
            addCriterion("Module_ID not between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("Role is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("Role is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("Role =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("Role <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("Role >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("Role >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("Role <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("Role <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("Role like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("Role not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("Role in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("Role not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("Role between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("Role not between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andTradeIsNull() {
            addCriterion("trade is null");
            return (Criteria) this;
        }

        public Criteria andTradeIsNotNull() {
            addCriterion("trade is not null");
            return (Criteria) this;
        }

        public Criteria andTradeEqualTo(String value) {
            addCriterion("trade =", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeNotEqualTo(String value) {
            addCriterion("trade <>", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeGreaterThan(String value) {
            addCriterion("trade >", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeGreaterThanOrEqualTo(String value) {
            addCriterion("trade >=", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeLessThan(String value) {
            addCriterion("trade <", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeLessThanOrEqualTo(String value) {
            addCriterion("trade <=", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeLike(String value) {
            addCriterion("trade like", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeNotLike(String value) {
            addCriterion("trade not like", value, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeIn(List<String> values) {
            addCriterion("trade in", values, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeNotIn(List<String> values) {
            addCriterion("trade not in", values, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeBetween(String value1, String value2) {
            addCriterion("trade between", value1, value2, "trade");
            return (Criteria) this;
        }

        public Criteria andTradeNotBetween(String value1, String value2) {
            addCriterion("trade not between", value1, value2, "trade");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundIsNull() {
            addCriterion("character_background is null");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundIsNotNull() {
            addCriterion("character_background is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundEqualTo(Byte value) {
            addCriterion("character_background =", value, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundNotEqualTo(Byte value) {
            addCriterion("character_background <>", value, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundGreaterThan(Byte value) {
            addCriterion("character_background >", value, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundGreaterThanOrEqualTo(Byte value) {
            addCriterion("character_background >=", value, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundLessThan(Byte value) {
            addCriterion("character_background <", value, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundLessThanOrEqualTo(Byte value) {
            addCriterion("character_background <=", value, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundIn(List<Byte> values) {
            addCriterion("character_background in", values, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundNotIn(List<Byte> values) {
            addCriterion("character_background not in", values, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundBetween(Byte value1, Byte value2) {
            addCriterion("character_background between", value1, value2, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterBackgroundNotBetween(Byte value1, Byte value2) {
            addCriterion("character_background not between", value1, value2, "characterBackground");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureIsNull() {
            addCriterion("character_nature is null");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureIsNotNull() {
            addCriterion("character_nature is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureEqualTo(Byte value) {
            addCriterion("character_nature =", value, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureNotEqualTo(Byte value) {
            addCriterion("character_nature <>", value, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureGreaterThan(Byte value) {
            addCriterion("character_nature >", value, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureGreaterThanOrEqualTo(Byte value) {
            addCriterion("character_nature >=", value, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureLessThan(Byte value) {
            addCriterion("character_nature <", value, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureLessThanOrEqualTo(Byte value) {
            addCriterion("character_nature <=", value, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureIn(List<Byte> values) {
            addCriterion("character_nature in", values, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureNotIn(List<Byte> values) {
            addCriterion("character_nature not in", values, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureBetween(Byte value1, Byte value2) {
            addCriterion("character_nature between", value1, value2, "characterNature");
            return (Criteria) this;
        }

        public Criteria andCharacterNatureNotBetween(Byte value1, Byte value2) {
            addCriterion("character_nature not between", value1, value2, "characterNature");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Integer value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Integer value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Integer value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Integer value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Integer value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Integer> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Integer> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Integer value1, Integer value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Integer value1, Integer value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeIsNull() {
            addCriterion("parent_organize_code is null");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeIsNotNull() {
            addCriterion("parent_organize_code is not null");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeEqualTo(String value) {
            addCriterion("parent_organize_code =", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeNotEqualTo(String value) {
            addCriterion("parent_organize_code <>", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeGreaterThan(String value) {
            addCriterion("parent_organize_code >", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("parent_organize_code >=", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeLessThan(String value) {
            addCriterion("parent_organize_code <", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeLessThanOrEqualTo(String value) {
            addCriterion("parent_organize_code <=", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeLike(String value) {
            addCriterion("parent_organize_code like", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeNotLike(String value) {
            addCriterion("parent_organize_code not like", value, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeIn(List<String> values) {
            addCriterion("parent_organize_code in", values, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeNotIn(List<String> values) {
            addCriterion("parent_organize_code not in", values, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeBetween(String value1, String value2) {
            addCriterion("parent_organize_code between", value1, value2, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andParentOrganizeCodeNotBetween(String value1, String value2) {
            addCriterion("parent_organize_code not between", value1, value2, "parentOrganizeCode");
            return (Criteria) this;
        }

        public Criteria andLevelIsNull() {
            addCriterion("level is null");
            return (Criteria) this;
        }

        public Criteria andLevelIsNotNull() {
            addCriterion("level is not null");
            return (Criteria) this;
        }

        public Criteria andLevelEqualTo(Byte value) {
            addCriterion("level =", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotEqualTo(Byte value) {
            addCriterion("level <>", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThan(Byte value) {
            addCriterion("level >", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelGreaterThanOrEqualTo(Byte value) {
            addCriterion("level >=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThan(Byte value) {
            addCriterion("level <", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelLessThanOrEqualTo(Byte value) {
            addCriterion("level <=", value, "level");
            return (Criteria) this;
        }

        public Criteria andLevelIn(List<Byte> values) {
            addCriterion("level in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotIn(List<Byte> values) {
            addCriterion("level not in", values, "level");
            return (Criteria) this;
        }

        public Criteria andLevelBetween(Byte value1, Byte value2) {
            addCriterion("level between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andLevelNotBetween(Byte value1, Byte value2) {
            addCriterion("level not between", value1, value2, "level");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewIsNull() {
            addCriterion("main_product_one_new is null");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewIsNotNull() {
            addCriterion("main_product_one_new is not null");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewEqualTo(String value) {
            addCriterion("main_product_one_new =", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewNotEqualTo(String value) {
            addCriterion("main_product_one_new <>", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewGreaterThan(String value) {
            addCriterion("main_product_one_new >", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewGreaterThanOrEqualTo(String value) {
            addCriterion("main_product_one_new >=", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewLessThan(String value) {
            addCriterion("main_product_one_new <", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewLessThanOrEqualTo(String value) {
            addCriterion("main_product_one_new <=", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewLike(String value) {
            addCriterion("main_product_one_new like", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewNotLike(String value) {
            addCriterion("main_product_one_new not like", value, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewIn(List<String> values) {
            addCriterion("main_product_one_new in", values, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewNotIn(List<String> values) {
            addCriterion("main_product_one_new not in", values, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewBetween(String value1, String value2) {
            addCriterion("main_product_one_new between", value1, value2, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductOneNewNotBetween(String value1, String value2) {
            addCriterion("main_product_one_new not between", value1, value2, "mainProductOneNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewIsNull() {
            addCriterion("main_product_two_new is null");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewIsNotNull() {
            addCriterion("main_product_two_new is not null");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewEqualTo(String value) {
            addCriterion("main_product_two_new =", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewNotEqualTo(String value) {
            addCriterion("main_product_two_new <>", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewGreaterThan(String value) {
            addCriterion("main_product_two_new >", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewGreaterThanOrEqualTo(String value) {
            addCriterion("main_product_two_new >=", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewLessThan(String value) {
            addCriterion("main_product_two_new <", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewLessThanOrEqualTo(String value) {
            addCriterion("main_product_two_new <=", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewLike(String value) {
            addCriterion("main_product_two_new like", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewNotLike(String value) {
            addCriterion("main_product_two_new not like", value, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewIn(List<String> values) {
            addCriterion("main_product_two_new in", values, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewNotIn(List<String> values) {
            addCriterion("main_product_two_new not in", values, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewBetween(String value1, String value2) {
            addCriterion("main_product_two_new between", value1, value2, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductTwoNewNotBetween(String value1, String value2) {
            addCriterion("main_product_two_new not between", value1, value2, "mainProductTwoNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewIsNull() {
            addCriterion("main_product_three_new is null");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewIsNotNull() {
            addCriterion("main_product_three_new is not null");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewEqualTo(String value) {
            addCriterion("main_product_three_new =", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewNotEqualTo(String value) {
            addCriterion("main_product_three_new <>", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewGreaterThan(String value) {
            addCriterion("main_product_three_new >", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewGreaterThanOrEqualTo(String value) {
            addCriterion("main_product_three_new >=", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewLessThan(String value) {
            addCriterion("main_product_three_new <", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewLessThanOrEqualTo(String value) {
            addCriterion("main_product_three_new <=", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewLike(String value) {
            addCriterion("main_product_three_new like", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewNotLike(String value) {
            addCriterion("main_product_three_new not like", value, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewIn(List<String> values) {
            addCriterion("main_product_three_new in", values, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewNotIn(List<String> values) {
            addCriterion("main_product_three_new not in", values, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewBetween(String value1, String value2) {
            addCriterion("main_product_three_new between", value1, value2, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andMainProductThreeNewNotBetween(String value1, String value2) {
            addCriterion("main_product_three_new not between", value1, value2, "mainProductThreeNew");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeIsNull() {
            addCriterion("exchange_type is null");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeIsNotNull() {
            addCriterion("exchange_type is not null");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeEqualTo(Integer value) {
            addCriterion("exchange_type =", value, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeNotEqualTo(Integer value) {
            addCriterion("exchange_type <>", value, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeGreaterThan(Integer value) {
            addCriterion("exchange_type >", value, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeGreaterThanOrEqualTo(Integer value) {
            addCriterion("exchange_type >=", value, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeLessThan(Integer value) {
            addCriterion("exchange_type <", value, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeLessThanOrEqualTo(Integer value) {
            addCriterion("exchange_type <=", value, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeIn(List<Integer> values) {
            addCriterion("exchange_type in", values, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeNotIn(List<Integer> values) {
            addCriterion("exchange_type not in", values, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeBetween(Integer value1, Integer value2) {
            addCriterion("exchange_type between", value1, value2, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andExchangeTypeNotBetween(Integer value1, Integer value2) {
            addCriterion("exchange_type not between", value1, value2, "exchangeType");
            return (Criteria) this;
        }

        public Criteria andListMarketNewIsNull() {
            addCriterion("list_market_new is null");
            return (Criteria) this;
        }

        public Criteria andListMarketNewIsNotNull() {
            addCriterion("list_market_new is not null");
            return (Criteria) this;
        }

        public Criteria andListMarketNewEqualTo(Integer value) {
            addCriterion("list_market_new =", value, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewNotEqualTo(Integer value) {
            addCriterion("list_market_new <>", value, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewGreaterThan(Integer value) {
            addCriterion("list_market_new >", value, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewGreaterThanOrEqualTo(Integer value) {
            addCriterion("list_market_new >=", value, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewLessThan(Integer value) {
            addCriterion("list_market_new <", value, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewLessThanOrEqualTo(Integer value) {
            addCriterion("list_market_new <=", value, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewIn(List<Integer> values) {
            addCriterion("list_market_new in", values, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewNotIn(List<Integer> values) {
            addCriterion("list_market_new not in", values, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewBetween(Integer value1, Integer value2) {
            addCriterion("list_market_new between", value1, value2, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andListMarketNewNotBetween(Integer value1, Integer value2) {
            addCriterion("list_market_new not between", value1, value2, "listMarketNew");
            return (Criteria) this;
        }

        public Criteria andIsOtcIsNull() {
            addCriterion("is_OTC is null");
            return (Criteria) this;
        }

        public Criteria andIsOtcIsNotNull() {
            addCriterion("is_OTC is not null");
            return (Criteria) this;
        }

        public Criteria andIsOtcEqualTo(Short value) {
            addCriterion("is_OTC =", value, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcNotEqualTo(Short value) {
            addCriterion("is_OTC <>", value, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcGreaterThan(Short value) {
            addCriterion("is_OTC >", value, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcGreaterThanOrEqualTo(Short value) {
            addCriterion("is_OTC >=", value, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcLessThan(Short value) {
            addCriterion("is_OTC <", value, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcLessThanOrEqualTo(Short value) {
            addCriterion("is_OTC <=", value, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcIn(List<Short> values) {
            addCriterion("is_OTC in", values, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcNotIn(List<Short> values) {
            addCriterion("is_OTC not in", values, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcBetween(Short value1, Short value2) {
            addCriterion("is_OTC between", value1, value2, "isOtc");
            return (Criteria) this;
        }

        public Criteria andIsOtcNotBetween(Short value1, Short value2) {
            addCriterion("is_OTC not between", value1, value2, "isOtc");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdIsNull() {
            addCriterion("business_registration_ID is null");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdIsNotNull() {
            addCriterion("business_registration_ID is not null");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdEqualTo(String value) {
            addCriterion("business_registration_ID =", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdNotEqualTo(String value) {
            addCriterion("business_registration_ID <>", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdGreaterThan(String value) {
            addCriterion("business_registration_ID >", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdGreaterThanOrEqualTo(String value) {
            addCriterion("business_registration_ID >=", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdLessThan(String value) {
            addCriterion("business_registration_ID <", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdLessThanOrEqualTo(String value) {
            addCriterion("business_registration_ID <=", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdLike(String value) {
            addCriterion("business_registration_ID like", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdNotLike(String value) {
            addCriterion("business_registration_ID not like", value, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdIn(List<String> values) {
            addCriterion("business_registration_ID in", values, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdNotIn(List<String> values) {
            addCriterion("business_registration_ID not in", values, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdBetween(String value1, String value2) {
            addCriterion("business_registration_ID between", value1, value2, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andBusinessRegistrationIdNotBetween(String value1, String value2) {
            addCriterion("business_registration_ID not between", value1, value2, "businessRegistrationId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdIsNull() {
            addCriterion("shareholding_ID is null");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdIsNotNull() {
            addCriterion("shareholding_ID is not null");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdEqualTo(Short value) {
            addCriterion("shareholding_ID =", value, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdNotEqualTo(Short value) {
            addCriterion("shareholding_ID <>", value, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdGreaterThan(Short value) {
            addCriterion("shareholding_ID >", value, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdGreaterThanOrEqualTo(Short value) {
            addCriterion("shareholding_ID >=", value, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdLessThan(Short value) {
            addCriterion("shareholding_ID <", value, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdLessThanOrEqualTo(Short value) {
            addCriterion("shareholding_ID <=", value, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdIn(List<Short> values) {
            addCriterion("shareholding_ID in", values, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdNotIn(List<Short> values) {
            addCriterion("shareholding_ID not in", values, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdBetween(Short value1, Short value2) {
            addCriterion("shareholding_ID between", value1, value2, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andShareholdingIdNotBetween(Short value1, Short value2) {
            addCriterion("shareholding_ID not between", value1, value2, "shareholdingId");
            return (Criteria) this;
        }

        public Criteria andCharacterOneIsNull() {
            addCriterion("character_one is null");
            return (Criteria) this;
        }

        public Criteria andCharacterOneIsNotNull() {
            addCriterion("character_one is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterOneEqualTo(Short value) {
            addCriterion("character_one =", value, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneNotEqualTo(Short value) {
            addCriterion("character_one <>", value, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneGreaterThan(Short value) {
            addCriterion("character_one >", value, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneGreaterThanOrEqualTo(Short value) {
            addCriterion("character_one >=", value, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneLessThan(Short value) {
            addCriterion("character_one <", value, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneLessThanOrEqualTo(Short value) {
            addCriterion("character_one <=", value, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneIn(List<Short> values) {
            addCriterion("character_one in", values, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneNotIn(List<Short> values) {
            addCriterion("character_one not in", values, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneBetween(Short value1, Short value2) {
            addCriterion("character_one between", value1, value2, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterOneNotBetween(Short value1, Short value2) {
            addCriterion("character_one not between", value1, value2, "characterOne");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoIsNull() {
            addCriterion("character_two is null");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoIsNotNull() {
            addCriterion("character_two is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoEqualTo(Short value) {
            addCriterion("character_two =", value, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoNotEqualTo(Short value) {
            addCriterion("character_two <>", value, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoGreaterThan(Short value) {
            addCriterion("character_two >", value, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoGreaterThanOrEqualTo(Short value) {
            addCriterion("character_two >=", value, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoLessThan(Short value) {
            addCriterion("character_two <", value, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoLessThanOrEqualTo(Short value) {
            addCriterion("character_two <=", value, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoIn(List<Short> values) {
            addCriterion("character_two in", values, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoNotIn(List<Short> values) {
            addCriterion("character_two not in", values, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoBetween(Short value1, Short value2) {
            addCriterion("character_two between", value1, value2, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterTwoNotBetween(Short value1, Short value2) {
            addCriterion("character_two not between", value1, value2, "characterTwo");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeIsNull() {
            addCriterion("character_three is null");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeIsNotNull() {
            addCriterion("character_three is not null");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeEqualTo(Short value) {
            addCriterion("character_three =", value, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeNotEqualTo(Short value) {
            addCriterion("character_three <>", value, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeGreaterThan(Short value) {
            addCriterion("character_three >", value, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeGreaterThanOrEqualTo(Short value) {
            addCriterion("character_three >=", value, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeLessThan(Short value) {
            addCriterion("character_three <", value, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeLessThanOrEqualTo(Short value) {
            addCriterion("character_three <=", value, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeIn(List<Short> values) {
            addCriterion("character_three in", values, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeNotIn(List<Short> values) {
            addCriterion("character_three not in", values, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeBetween(Short value1, Short value2) {
            addCriterion("character_three between", value1, value2, "characterThree");
            return (Criteria) this;
        }

        public Criteria andCharacterThreeNotBetween(Short value1, Short value2) {
            addCriterion("character_three not between", value1, value2, "characterThree");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}