package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_contact;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_contactExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface Enterprise_contactMapper {
    int countByExample(Enterprise_contactExample example);

    int deleteByExample(Enterprise_contactExample example);

    int deleteByPrimaryKey(Long epContactId);

    int insert(Enterprise_contact record);

    int insertSelective(Enterprise_contact record);

    List<Enterprise_contact> selectByExample(Enterprise_contactExample example);

    Enterprise_contact selectByPrimaryKey(Long epContactId);

    int updateByExampleSelective(@Param("record") Enterprise_contact record, @Param("example") Enterprise_contactExample example);

    int updateByExample(@Param("record") Enterprise_contact record, @Param("example") Enterprise_contactExample example);

    int updateByPrimaryKeySelective(Enterprise_contact record);

    int updateByPrimaryKey(Enterprise_contact record);
}