package cn.com.chinaventure.trans.cvent.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Enterprise_contactExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public Enterprise_contactExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEpContactIdIsNull() {
            addCriterion("EP_Contact_ID is null");
            return (Criteria) this;
        }

        public Criteria andEpContactIdIsNotNull() {
            addCriterion("EP_Contact_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEpContactIdEqualTo(Long value) {
            addCriterion("EP_Contact_ID =", value, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdNotEqualTo(Long value) {
            addCriterion("EP_Contact_ID <>", value, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdGreaterThan(Long value) {
            addCriterion("EP_Contact_ID >", value, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdGreaterThanOrEqualTo(Long value) {
            addCriterion("EP_Contact_ID >=", value, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdLessThan(Long value) {
            addCriterion("EP_Contact_ID <", value, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdLessThanOrEqualTo(Long value) {
            addCriterion("EP_Contact_ID <=", value, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdIn(List<Long> values) {
            addCriterion("EP_Contact_ID in", values, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdNotIn(List<Long> values) {
            addCriterion("EP_Contact_ID not in", values, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdBetween(Long value1, Long value2) {
            addCriterion("EP_Contact_ID between", value1, value2, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpContactIdNotBetween(Long value1, Long value2) {
            addCriterion("EP_Contact_ID not between", value1, value2, "epContactId");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressIsNull() {
            addCriterion("EP_CN_Address is null");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressIsNotNull() {
            addCriterion("EP_CN_Address is not null");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressEqualTo(String value) {
            addCriterion("EP_CN_Address =", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressNotEqualTo(String value) {
            addCriterion("EP_CN_Address <>", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressGreaterThan(String value) {
            addCriterion("EP_CN_Address >", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressGreaterThanOrEqualTo(String value) {
            addCriterion("EP_CN_Address >=", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressLessThan(String value) {
            addCriterion("EP_CN_Address <", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressLessThanOrEqualTo(String value) {
            addCriterion("EP_CN_Address <=", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressLike(String value) {
            addCriterion("EP_CN_Address like", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressNotLike(String value) {
            addCriterion("EP_CN_Address not like", value, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressIn(List<String> values) {
            addCriterion("EP_CN_Address in", values, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressNotIn(List<String> values) {
            addCriterion("EP_CN_Address not in", values, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressBetween(String value1, String value2) {
            addCriterion("EP_CN_Address between", value1, value2, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnAddressNotBetween(String value1, String value2) {
            addCriterion("EP_CN_Address not between", value1, value2, "epCnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressIsNull() {
            addCriterion("EP_EN_Address is null");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressIsNotNull() {
            addCriterion("EP_EN_Address is not null");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressEqualTo(String value) {
            addCriterion("EP_EN_Address =", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressNotEqualTo(String value) {
            addCriterion("EP_EN_Address <>", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressGreaterThan(String value) {
            addCriterion("EP_EN_Address >", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressGreaterThanOrEqualTo(String value) {
            addCriterion("EP_EN_Address >=", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressLessThan(String value) {
            addCriterion("EP_EN_Address <", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressLessThanOrEqualTo(String value) {
            addCriterion("EP_EN_Address <=", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressLike(String value) {
            addCriterion("EP_EN_Address like", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressNotLike(String value) {
            addCriterion("EP_EN_Address not like", value, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressIn(List<String> values) {
            addCriterion("EP_EN_Address in", values, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressNotIn(List<String> values) {
            addCriterion("EP_EN_Address not in", values, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressBetween(String value1, String value2) {
            addCriterion("EP_EN_Address between", value1, value2, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpEnAddressNotBetween(String value1, String value2) {
            addCriterion("EP_EN_Address not between", value1, value2, "epEnAddress");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonIsNull() {
            addCriterion("EP_CN_Person is null");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonIsNotNull() {
            addCriterion("EP_CN_Person is not null");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonEqualTo(String value) {
            addCriterion("EP_CN_Person =", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonNotEqualTo(String value) {
            addCriterion("EP_CN_Person <>", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonGreaterThan(String value) {
            addCriterion("EP_CN_Person >", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonGreaterThanOrEqualTo(String value) {
            addCriterion("EP_CN_Person >=", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonLessThan(String value) {
            addCriterion("EP_CN_Person <", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonLessThanOrEqualTo(String value) {
            addCriterion("EP_CN_Person <=", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonLike(String value) {
            addCriterion("EP_CN_Person like", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonNotLike(String value) {
            addCriterion("EP_CN_Person not like", value, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonIn(List<String> values) {
            addCriterion("EP_CN_Person in", values, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonNotIn(List<String> values) {
            addCriterion("EP_CN_Person not in", values, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonBetween(String value1, String value2) {
            addCriterion("EP_CN_Person between", value1, value2, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpCnPersonNotBetween(String value1, String value2) {
            addCriterion("EP_CN_Person not between", value1, value2, "epCnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonIsNull() {
            addCriterion("EP_EN_Person is null");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonIsNotNull() {
            addCriterion("EP_EN_Person is not null");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonEqualTo(String value) {
            addCriterion("EP_EN_Person =", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonNotEqualTo(String value) {
            addCriterion("EP_EN_Person <>", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonGreaterThan(String value) {
            addCriterion("EP_EN_Person >", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonGreaterThanOrEqualTo(String value) {
            addCriterion("EP_EN_Person >=", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonLessThan(String value) {
            addCriterion("EP_EN_Person <", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonLessThanOrEqualTo(String value) {
            addCriterion("EP_EN_Person <=", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonLike(String value) {
            addCriterion("EP_EN_Person like", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonNotLike(String value) {
            addCriterion("EP_EN_Person not like", value, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonIn(List<String> values) {
            addCriterion("EP_EN_Person in", values, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonNotIn(List<String> values) {
            addCriterion("EP_EN_Person not in", values, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonBetween(String value1, String value2) {
            addCriterion("EP_EN_Person between", value1, value2, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpEnPersonNotBetween(String value1, String value2) {
            addCriterion("EP_EN_Person not between", value1, value2, "epEnPerson");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeIsNull() {
            addCriterion("EP_PostCode is null");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeIsNotNull() {
            addCriterion("EP_PostCode is not null");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeEqualTo(String value) {
            addCriterion("EP_PostCode =", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeNotEqualTo(String value) {
            addCriterion("EP_PostCode <>", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeGreaterThan(String value) {
            addCriterion("EP_PostCode >", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeGreaterThanOrEqualTo(String value) {
            addCriterion("EP_PostCode >=", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeLessThan(String value) {
            addCriterion("EP_PostCode <", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeLessThanOrEqualTo(String value) {
            addCriterion("EP_PostCode <=", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeLike(String value) {
            addCriterion("EP_PostCode like", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeNotLike(String value) {
            addCriterion("EP_PostCode not like", value, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeIn(List<String> values) {
            addCriterion("EP_PostCode in", values, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeNotIn(List<String> values) {
            addCriterion("EP_PostCode not in", values, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeBetween(String value1, String value2) {
            addCriterion("EP_PostCode between", value1, value2, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpPostcodeNotBetween(String value1, String value2) {
            addCriterion("EP_PostCode not between", value1, value2, "epPostcode");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneIsNull() {
            addCriterion("EP_Telephone is null");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneIsNotNull() {
            addCriterion("EP_Telephone is not null");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneEqualTo(String value) {
            addCriterion("EP_Telephone =", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneNotEqualTo(String value) {
            addCriterion("EP_Telephone <>", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneGreaterThan(String value) {
            addCriterion("EP_Telephone >", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneGreaterThanOrEqualTo(String value) {
            addCriterion("EP_Telephone >=", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneLessThan(String value) {
            addCriterion("EP_Telephone <", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneLessThanOrEqualTo(String value) {
            addCriterion("EP_Telephone <=", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneLike(String value) {
            addCriterion("EP_Telephone like", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneNotLike(String value) {
            addCriterion("EP_Telephone not like", value, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneIn(List<String> values) {
            addCriterion("EP_Telephone in", values, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneNotIn(List<String> values) {
            addCriterion("EP_Telephone not in", values, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneBetween(String value1, String value2) {
            addCriterion("EP_Telephone between", value1, value2, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpTelephoneNotBetween(String value1, String value2) {
            addCriterion("EP_Telephone not between", value1, value2, "epTelephone");
            return (Criteria) this;
        }

        public Criteria andEpFaxIsNull() {
            addCriterion("EP_Fax is null");
            return (Criteria) this;
        }

        public Criteria andEpFaxIsNotNull() {
            addCriterion("EP_Fax is not null");
            return (Criteria) this;
        }

        public Criteria andEpFaxEqualTo(String value) {
            addCriterion("EP_Fax =", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxNotEqualTo(String value) {
            addCriterion("EP_Fax <>", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxGreaterThan(String value) {
            addCriterion("EP_Fax >", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxGreaterThanOrEqualTo(String value) {
            addCriterion("EP_Fax >=", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxLessThan(String value) {
            addCriterion("EP_Fax <", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxLessThanOrEqualTo(String value) {
            addCriterion("EP_Fax <=", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxLike(String value) {
            addCriterion("EP_Fax like", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxNotLike(String value) {
            addCriterion("EP_Fax not like", value, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxIn(List<String> values) {
            addCriterion("EP_Fax in", values, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxNotIn(List<String> values) {
            addCriterion("EP_Fax not in", values, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxBetween(String value1, String value2) {
            addCriterion("EP_Fax between", value1, value2, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpFaxNotBetween(String value1, String value2) {
            addCriterion("EP_Fax not between", value1, value2, "epFax");
            return (Criteria) this;
        }

        public Criteria andEpMailIsNull() {
            addCriterion("EP_Mail is null");
            return (Criteria) this;
        }

        public Criteria andEpMailIsNotNull() {
            addCriterion("EP_Mail is not null");
            return (Criteria) this;
        }

        public Criteria andEpMailEqualTo(String value) {
            addCriterion("EP_Mail =", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailNotEqualTo(String value) {
            addCriterion("EP_Mail <>", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailGreaterThan(String value) {
            addCriterion("EP_Mail >", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailGreaterThanOrEqualTo(String value) {
            addCriterion("EP_Mail >=", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailLessThan(String value) {
            addCriterion("EP_Mail <", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailLessThanOrEqualTo(String value) {
            addCriterion("EP_Mail <=", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailLike(String value) {
            addCriterion("EP_Mail like", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailNotLike(String value) {
            addCriterion("EP_Mail not like", value, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailIn(List<String> values) {
            addCriterion("EP_Mail in", values, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailNotIn(List<String> values) {
            addCriterion("EP_Mail not in", values, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailBetween(String value1, String value2) {
            addCriterion("EP_Mail between", value1, value2, "epMail");
            return (Criteria) this;
        }

        public Criteria andEpMailNotBetween(String value1, String value2) {
            addCriterion("EP_Mail not between", value1, value2, "epMail");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNull() {
            addCriterion("Update_Time is null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIsNotNull() {
            addCriterion("Update_Time is not null");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeEqualTo(Date value) {
            addCriterion("Update_Time =", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotEqualTo(Date value) {
            addCriterion("Update_Time <>", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThan(Date value) {
            addCriterion("Update_Time >", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Update_Time >=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThan(Date value) {
            addCriterion("Update_Time <", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Update_Time <=", value, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeIn(List<Date> values) {
            addCriterion("Update_Time in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotIn(List<Date> values) {
            addCriterion("Update_Time not in", values, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeBetween(Date value1, Date value2) {
            addCriterion("Update_Time between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andUpdateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Update_Time not between", value1, value2, "updateTime");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNull() {
            addCriterion("Behavior is null");
            return (Criteria) this;
        }

        public Criteria andBehaviorIsNotNull() {
            addCriterion("Behavior is not null");
            return (Criteria) this;
        }

        public Criteria andBehaviorEqualTo(String value) {
            addCriterion("Behavior =", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotEqualTo(String value) {
            addCriterion("Behavior <>", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThan(String value) {
            addCriterion("Behavior >", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorGreaterThanOrEqualTo(String value) {
            addCriterion("Behavior >=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThan(String value) {
            addCriterion("Behavior <", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLessThanOrEqualTo(String value) {
            addCriterion("Behavior <=", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorLike(String value) {
            addCriterion("Behavior like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotLike(String value) {
            addCriterion("Behavior not like", value, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorIn(List<String> values) {
            addCriterion("Behavior in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotIn(List<String> values) {
            addCriterion("Behavior not in", values, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorBetween(String value1, String value2) {
            addCriterion("Behavior between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andBehaviorNotBetween(String value1, String value2) {
            addCriterion("Behavior not between", value1, value2, "behavior");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNull() {
            addCriterion("Ordered is null");
            return (Criteria) this;
        }

        public Criteria andOrderedIsNotNull() {
            addCriterion("Ordered is not null");
            return (Criteria) this;
        }

        public Criteria andOrderedEqualTo(Long value) {
            addCriterion("Ordered =", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotEqualTo(Long value) {
            addCriterion("Ordered <>", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThan(Long value) {
            addCriterion("Ordered >", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedGreaterThanOrEqualTo(Long value) {
            addCriterion("Ordered >=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThan(Long value) {
            addCriterion("Ordered <", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedLessThanOrEqualTo(Long value) {
            addCriterion("Ordered <=", value, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedIn(List<Long> values) {
            addCriterion("Ordered in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotIn(List<Long> values) {
            addCriterion("Ordered not in", values, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedBetween(Long value1, Long value2) {
            addCriterion("Ordered between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andOrderedNotBetween(Long value1, Long value2) {
            addCriterion("Ordered not between", value1, value2, "ordered");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidIsNull() {
            addCriterion("EnterpriseId is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidIsNotNull() {
            addCriterion("EnterpriseId is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidEqualTo(Long value) {
            addCriterion("EnterpriseId =", value, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidNotEqualTo(Long value) {
            addCriterion("EnterpriseId <>", value, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidGreaterThan(Long value) {
            addCriterion("EnterpriseId >", value, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidGreaterThanOrEqualTo(Long value) {
            addCriterion("EnterpriseId >=", value, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidLessThan(Long value) {
            addCriterion("EnterpriseId <", value, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidLessThanOrEqualTo(Long value) {
            addCriterion("EnterpriseId <=", value, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidIn(List<Long> values) {
            addCriterion("EnterpriseId in", values, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidNotIn(List<Long> values) {
            addCriterion("EnterpriseId not in", values, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidBetween(Long value1, Long value2) {
            addCriterion("EnterpriseId between", value1, value2, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterpriseidNotBetween(Long value1, Long value2) {
            addCriterion("EnterpriseId not between", value1, value2, "enterpriseid");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameIsNull() {
            addCriterion("EnterpriseName is null");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameIsNotNull() {
            addCriterion("EnterpriseName is not null");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameEqualTo(String value) {
            addCriterion("EnterpriseName =", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameNotEqualTo(String value) {
            addCriterion("EnterpriseName <>", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameGreaterThan(String value) {
            addCriterion("EnterpriseName >", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameGreaterThanOrEqualTo(String value) {
            addCriterion("EnterpriseName >=", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameLessThan(String value) {
            addCriterion("EnterpriseName <", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameLessThanOrEqualTo(String value) {
            addCriterion("EnterpriseName <=", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameLike(String value) {
            addCriterion("EnterpriseName like", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameNotLike(String value) {
            addCriterion("EnterpriseName not like", value, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameIn(List<String> values) {
            addCriterion("EnterpriseName in", values, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameNotIn(List<String> values) {
            addCriterion("EnterpriseName not in", values, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameBetween(String value1, String value2) {
            addCriterion("EnterpriseName between", value1, value2, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andEnterprisenameNotBetween(String value1, String value2) {
            addCriterion("EnterpriseName not between", value1, value2, "enterprisename");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNull() {
            addCriterion("Module_ID is null");
            return (Criteria) this;
        }

        public Criteria andModuleIdIsNotNull() {
            addCriterion("Module_ID is not null");
            return (Criteria) this;
        }

        public Criteria andModuleIdEqualTo(Long value) {
            addCriterion("Module_ID =", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotEqualTo(Long value) {
            addCriterion("Module_ID <>", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThan(Long value) {
            addCriterion("Module_ID >", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Module_ID >=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThan(Long value) {
            addCriterion("Module_ID <", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdLessThanOrEqualTo(Long value) {
            addCriterion("Module_ID <=", value, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdIn(List<Long> values) {
            addCriterion("Module_ID in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotIn(List<Long> values) {
            addCriterion("Module_ID not in", values, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdBetween(Long value1, Long value2) {
            addCriterion("Module_ID between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andModuleIdNotBetween(Long value1, Long value2) {
            addCriterion("Module_ID not between", value1, value2, "moduleId");
            return (Criteria) this;
        }

        public Criteria andRoleIsNull() {
            addCriterion("Role is null");
            return (Criteria) this;
        }

        public Criteria andRoleIsNotNull() {
            addCriterion("Role is not null");
            return (Criteria) this;
        }

        public Criteria andRoleEqualTo(String value) {
            addCriterion("Role =", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotEqualTo(String value) {
            addCriterion("Role <>", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThan(String value) {
            addCriterion("Role >", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleGreaterThanOrEqualTo(String value) {
            addCriterion("Role >=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThan(String value) {
            addCriterion("Role <", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLessThanOrEqualTo(String value) {
            addCriterion("Role <=", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleLike(String value) {
            addCriterion("Role like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotLike(String value) {
            addCriterion("Role not like", value, "role");
            return (Criteria) this;
        }

        public Criteria andRoleIn(List<String> values) {
            addCriterion("Role in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotIn(List<String> values) {
            addCriterion("Role not in", values, "role");
            return (Criteria) this;
        }

        public Criteria andRoleBetween(String value1, String value2) {
            addCriterion("Role between", value1, value2, "role");
            return (Criteria) this;
        }

        public Criteria andRoleNotBetween(String value1, String value2) {
            addCriterion("Role not between", value1, value2, "role");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}