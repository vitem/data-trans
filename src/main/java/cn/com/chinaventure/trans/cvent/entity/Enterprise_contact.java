package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class Enterprise_contact implements Serializable {
    private Long epContactId;

    private String epCnAddress;

    private String epEnAddress;

    private String epCnPerson;

    private String epEnPerson;

    private String epPostcode;

    private String epTelephone;

    private String epFax;

    private String epMail;

    private Date createTime;

    private Date updateTime;

    private String behavior;

    private Long ordered;

    private Long enterpriseid;

    private String enterprisename;

    private Long moduleId;

    private String role;

    private static final long serialVersionUID = 1L;

    public Long getEpContactId() {
        return epContactId;
    }

    public void setEpContactId(Long epContactId) {
        this.epContactId = epContactId;
    }

    public String getEpCnAddress() {
        return epCnAddress;
    }

    public void setEpCnAddress(String epCnAddress) {
        this.epCnAddress = epCnAddress == null ? null : epCnAddress.trim();
    }

    public String getEpEnAddress() {
        return epEnAddress;
    }

    public void setEpEnAddress(String epEnAddress) {
        this.epEnAddress = epEnAddress == null ? null : epEnAddress.trim();
    }

    public String getEpCnPerson() {
        return epCnPerson;
    }

    public void setEpCnPerson(String epCnPerson) {
        this.epCnPerson = epCnPerson == null ? null : epCnPerson.trim();
    }

    public String getEpEnPerson() {
        return epEnPerson;
    }

    public void setEpEnPerson(String epEnPerson) {
        this.epEnPerson = epEnPerson == null ? null : epEnPerson.trim();
    }

    public String getEpPostcode() {
        return epPostcode;
    }

    public void setEpPostcode(String epPostcode) {
        this.epPostcode = epPostcode == null ? null : epPostcode.trim();
    }

    public String getEpTelephone() {
        return epTelephone;
    }

    public void setEpTelephone(String epTelephone) {
        this.epTelephone = epTelephone == null ? null : epTelephone.trim();
    }

    public String getEpFax() {
        return epFax;
    }

    public void setEpFax(String epFax) {
        this.epFax = epFax == null ? null : epFax.trim();
    }

    public String getEpMail() {
        return epMail;
    }

    public void setEpMail(String epMail) {
        this.epMail = epMail == null ? null : epMail.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior == null ? null : behavior.trim();
    }

    public Long getOrdered() {
        return ordered;
    }

    public void setOrdered(Long ordered) {
        this.ordered = ordered;
    }

    public Long getEnterpriseid() {
        return enterpriseid;
    }

    public void setEnterpriseid(Long enterpriseid) {
        this.enterpriseid = enterpriseid;
    }

    public String getEnterprisename() {
        return enterprisename;
    }

    public void setEnterprisename(String enterprisename) {
        this.enterprisename = enterprisename == null ? null : enterprisename.trim();
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", epContactId=").append(epContactId);
        sb.append(", epCnAddress=").append(epCnAddress);
        sb.append(", epEnAddress=").append(epEnAddress);
        sb.append(", epCnPerson=").append(epCnPerson);
        sb.append(", epEnPerson=").append(epEnPerson);
        sb.append(", epPostcode=").append(epPostcode);
        sb.append(", epTelephone=").append(epTelephone);
        sb.append(", epFax=").append(epFax);
        sb.append(", epMail=").append(epMail);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", behavior=").append(behavior);
        sb.append(", ordered=").append(ordered);
        sb.append(", enterpriseid=").append(enterpriseid);
        sb.append(", enterprisename=").append(enterprisename);
        sb.append(", moduleId=").append(moduleId);
        sb.append(", role=").append(role);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}