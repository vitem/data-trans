package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvent.service.Enterprise_credit_organizationService;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_organization;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_organizationExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_organizationExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_credit_organizationMapper;


@Service("enterprise_credit_organizationService")
public class Enterprise_credit_organizationServiceImpl implements Enterprise_credit_organizationService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_credit_organizationServiceImpl.class);

	@Resource
	private Enterprise_credit_organizationMapper enterprise_credit_organizationMapper;


	@Override
	public void saveEnterprise_credit_organization(Enterprise_credit_organization enterprise_credit_organization) {
		if (null == enterprise_credit_organization) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_organizationMapper.insertSelective(enterprise_credit_organization);
	}

	@Override
	public void updateEnterprise_credit_organization(Enterprise_credit_organization enterprise_credit_organization) {
		if (null == enterprise_credit_organization) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_organizationMapper.updateByPrimaryKeySelective(enterprise_credit_organization);
	}

	@Override
	public Enterprise_credit_organization getEnterprise_credit_organizationById(Long id) {
		return enterprise_credit_organizationMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_credit_organization> getEnterprise_credit_organizationList(Enterprise_credit_organization enterprise_credit_organization) {
		return this.getEnterprise_credit_organizationListByParam(enterprise_credit_organization, null);
	}

	@Override
	public List<Enterprise_credit_organization> getEnterprise_credit_organizationListByParam(Enterprise_credit_organization enterprise_credit_organization, String orderByStr) {
		Enterprise_credit_organizationExample example = new Enterprise_credit_organizationExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise_credit_organization> enterprise_credit_organizationList = this.enterprise_credit_organizationMapper.selectByExample(example);
		return enterprise_credit_organizationList;
	}

	@Override
	public PageResult<Enterprise_credit_organization> findEnterprise_credit_organizationPageByParam(Enterprise_credit_organization enterprise_credit_organization, PageParam pageParam) {
		return this.findEnterprise_credit_organizationPageByParam(enterprise_credit_organization, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_credit_organization> findEnterprise_credit_organizationPageByParam(Enterprise_credit_organization enterprise_credit_organization, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_credit_organization> pageResult = new PageResult<Enterprise_credit_organization>();

		Enterprise_credit_organizationExample example = new Enterprise_credit_organizationExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_credit_organization> list = null;// enterprise_credit_organizationMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_credit_organizationById(Long id) {
		this.enterprise_credit_organizationMapper.deleteByPrimaryKey(id);
	}

}
