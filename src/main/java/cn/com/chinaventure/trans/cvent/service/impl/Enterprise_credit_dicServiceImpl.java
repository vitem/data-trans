package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_dic;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_dicExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_dicExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_credit_dicMapper;
import cn.com.chinaventure.trans.cvent.service.Enterprise_credit_dicService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("enterprise_credit_dicService")
public class Enterprise_credit_dicServiceImpl implements Enterprise_credit_dicService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_credit_dicServiceImpl.class);

	@Resource
	private Enterprise_credit_dicMapper enterprise_credit_dicMapper;


	@Override
	public void saveEnterprise_credit_dic(Enterprise_credit_dic enterprise_credit_dic) {
		if (null == enterprise_credit_dic) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_dicMapper.insertSelective(enterprise_credit_dic);
	}

	@Override
	public void updateEnterprise_credit_dic(Enterprise_credit_dic enterprise_credit_dic) {
		if (null == enterprise_credit_dic) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_dicMapper.updateByPrimaryKeySelective(enterprise_credit_dic);
	}

	@Override
	public Enterprise_credit_dic getEnterprise_credit_dicById(Long id) {
		return enterprise_credit_dicMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_credit_dic> getEnterprise_credit_dicList(Enterprise_credit_dic enterprise_credit_dic) {
		return this.getEnterprise_credit_dicListByParam(enterprise_credit_dic, null);
	}

	@Override
	public List<Enterprise_credit_dic> getEnterprise_credit_dicListByParam(Enterprise_credit_dic enterprise_credit_dic, String orderByStr) {
		Enterprise_credit_dicExample example = new Enterprise_credit_dicExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise_credit_dic> enterprise_credit_dicList = this.enterprise_credit_dicMapper.selectByExample(example);
		return enterprise_credit_dicList;
	}

	@Override
	public PageResult<Enterprise_credit_dic> findEnterprise_credit_dicPageByParam(Enterprise_credit_dic enterprise_credit_dic, PageParam pageParam) {
		return this.findEnterprise_credit_dicPageByParam(enterprise_credit_dic, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_credit_dic> findEnterprise_credit_dicPageByParam(Enterprise_credit_dic enterprise_credit_dic, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_credit_dic> pageResult = new PageResult<Enterprise_credit_dic>();

		Enterprise_credit_dicExample example = new Enterprise_credit_dicExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_credit_dic> list = null;// enterprise_credit_dicMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_credit_dicById(Long id) {
		this.enterprise_credit_dicMapper.deleteByPrimaryKey(id);
	}

}
