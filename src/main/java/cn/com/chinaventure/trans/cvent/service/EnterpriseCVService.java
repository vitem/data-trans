package cn.com.chinaventure.trans.cvent.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCV;
import cn.com.chinaventure.trans.cvent.entity.EnterpriseCVWithBLOBs;

import java.util.List;

/**
 * @author 
 *
 */
public interface EnterpriseCVService {
	
	/**
     * @api saveEnterpriseCV 新增enterpriseCV
     * @apiGroup enterpriseCV管理
     * @apiName  新增enterpriseCV记录
     * @apiDescription 全量插入enterpriseCV记录
     * @apiParam {EnterpriseCV} enterpriseCV enterpriseCV对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterpriseCV(EnterpriseCV enterpriseCV);
	
	/**
     * @api updateEnterpriseCV 修改enterpriseCV
     * @apiGroup enterpriseCV管理
     * @apiName  修改enterpriseCV记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {EnterpriseCV} enterpriseCV enterpriseCV对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterpriseCV(EnterpriseCVWithBLOBs enterpriseCV);
	
	/**
     * @api getEnterpriseCVById 根据enterpriseCVid查询详情
     * @apiGroup enterpriseCV管理
     * @apiName  查询enterpriseCV详情
     * @apiDescription 根据主键id查询enterpriseCV详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess EnterpriseCV enterpriseCV实体对象
     * @apiVersion 1.0.0
     * 
     */
	public EnterpriseCVWithBLOBs getEnterpriseCVById(Integer id);
	
	/**
     * @api getEnterpriseCVList 根据enterpriseCV条件查询列表
     * @apiGroup enterpriseCV管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {EnterpriseCV} enterpriseCV  实体条件
     * @apiSuccess List<EnterpriseCV> enterpriseCV实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EnterpriseCV> getEnterpriseCVList(EnterpriseCV enterpriseCV);
	

	/**
     * @api getEnterpriseCVListByParam 根据enterpriseCV条件查询列表（含排序）
     * @apiGroup enterpriseCV管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {EnterpriseCV} enterpriseCV  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<EnterpriseCV> enterpriseCV实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<EnterpriseCV> getEnterpriseCVListByParam(EnterpriseCV enterpriseCV, String orderByStr);
	
	
	/**
     * @api findEnterpriseCVPageByParam 根据enterpriseCV条件查询列表（分页）
     * @apiGroup enterpriseCV管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {EnterpriseCV} enterpriseCV  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<EnterpriseCV> enterpriseCV实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EnterpriseCV> findEnterpriseCVPageByParam(EnterpriseCV enterpriseCV, PageParam pageParam);
	
	/**
     * @api findEnterpriseCVPageByParam 根据enterpriseCV条件查询列表（分页，含排序）
     * @apiGroup enterpriseCV管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {EnterpriseCV} enterpriseCV  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<EnterpriseCV> enterpriseCV实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<EnterpriseCV> findEnterpriseCVPageByParam(EnterpriseCV enterpriseCV, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterpriseCVById 根据enterpriseCV的id删除(物理删除)
     * @apiGroup enterpriseCV管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterpriseCVById(Integer id);

	public Integer countEnterpriseCV();

}

