package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvent.service.Enterprise_credit_personageService;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_personage;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_personageExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_personageExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_credit_personageMapper;


@Service("enterprise_credit_personageService")
public class Enterprise_credit_personageServiceImpl implements Enterprise_credit_personageService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_credit_personageServiceImpl.class);

	@Resource
	private Enterprise_credit_personageMapper enterprise_credit_personageMapper;


	@Override
	public void saveEnterprise_credit_personage(Enterprise_credit_personage enterprise_credit_personage) {
		if (null == enterprise_credit_personage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_personageMapper.insertSelective(enterprise_credit_personage);
	}

	@Override
	public void updateEnterprise_credit_personage(Enterprise_credit_personage enterprise_credit_personage) {
		if (null == enterprise_credit_personage) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_personageMapper.updateByPrimaryKeySelective(enterprise_credit_personage);
	}

	@Override
	public Enterprise_credit_personage getEnterprise_credit_personageById(Long id) {
		return enterprise_credit_personageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_credit_personage> getEnterprise_credit_personageList(Enterprise_credit_personage enterprise_credit_personage) {
		return this.getEnterprise_credit_personageListByParam(enterprise_credit_personage, null);
	}

	@Override
	public List<Enterprise_credit_personage> getEnterprise_credit_personageListByParam(Enterprise_credit_personage enterprise_credit_personage, String orderByStr) {
		Enterprise_credit_personageExample example = new Enterprise_credit_personageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise_credit_personage> enterprise_credit_personageList = this.enterprise_credit_personageMapper.selectByExample(example);
		return enterprise_credit_personageList;
	}

	@Override
	public PageResult<Enterprise_credit_personage> findEnterprise_credit_personagePageByParam(Enterprise_credit_personage enterprise_credit_personage, PageParam pageParam) {
		return this.findEnterprise_credit_personagePageByParam(enterprise_credit_personage, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_credit_personage> findEnterprise_credit_personagePageByParam(Enterprise_credit_personage enterprise_credit_personage, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_credit_personage> pageResult = new PageResult<Enterprise_credit_personage>();

		Enterprise_credit_personageExample example = new Enterprise_credit_personageExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_credit_personage> list = null;// enterprise_credit_personageMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_credit_personageById(Long id) {
		this.enterprise_credit_personageMapper.deleteByPrimaryKey(id);
	}

}
