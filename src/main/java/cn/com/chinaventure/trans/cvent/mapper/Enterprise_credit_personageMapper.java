package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_personage;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_personageExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_credit_personageMapper {
    int countByExample(Enterprise_credit_personageExample example);

    int deleteByExample(Enterprise_credit_personageExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Enterprise_credit_personage record);

    int insertSelective(Enterprise_credit_personage record);

    List<Enterprise_credit_personage> selectByExample(Enterprise_credit_personageExample example);

    Enterprise_credit_personage selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Enterprise_credit_personage record, @Param("example") Enterprise_credit_personageExample example);

    int updateByExample(@Param("record") Enterprise_credit_personage record, @Param("example") Enterprise_credit_personageExample example);

    int updateByPrimaryKeySelective(Enterprise_credit_personage record);

    int updateByPrimaryKey(Enterprise_credit_personage record);
}