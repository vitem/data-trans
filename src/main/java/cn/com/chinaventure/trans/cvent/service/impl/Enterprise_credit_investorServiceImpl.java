package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvent.service.Enterprise_credit_investorService;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_investor;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_investorExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_investorExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_credit_investorMapper;


@Service("enterprise_credit_investorService")
public class Enterprise_credit_investorServiceImpl implements Enterprise_credit_investorService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_credit_investorServiceImpl.class);

	@Resource
	private Enterprise_credit_investorMapper enterprise_credit_investorMapper;


	@Override
	public void saveEnterprise_credit_investor(Enterprise_credit_investor enterprise_credit_investor) {
		if (null == enterprise_credit_investor) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_investorMapper.insertSelective(enterprise_credit_investor);
	}

	@Override
	public void updateEnterprise_credit_investor(Enterprise_credit_investor enterprise_credit_investor) {
		if (null == enterprise_credit_investor) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_investorMapper.updateByPrimaryKeySelective(enterprise_credit_investor);
	}

	@Override
	public Enterprise_credit_investor getEnterprise_credit_investorById(Long id) {
		return enterprise_credit_investorMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_credit_investor> getEnterprise_credit_investorList(Enterprise_credit_investor enterprise_credit_investor) {
		return this.getEnterprise_credit_investorListByParam(enterprise_credit_investor, null);
	}

	@Override
	public List<Enterprise_credit_investor> getEnterprise_credit_investorListByParam(Enterprise_credit_investor enterprise_credit_investor, String orderByStr) {
		Enterprise_credit_investorExample example = new Enterprise_credit_investorExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise_credit_investor> enterprise_credit_investorList = this.enterprise_credit_investorMapper.selectByExample(example);
		return enterprise_credit_investorList;
	}

	@Override
	public PageResult<Enterprise_credit_investor> findEnterprise_credit_investorPageByParam(Enterprise_credit_investor enterprise_credit_investor, PageParam pageParam) {
		return this.findEnterprise_credit_investorPageByParam(enterprise_credit_investor, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_credit_investor> findEnterprise_credit_investorPageByParam(Enterprise_credit_investor enterprise_credit_investor, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_credit_investor> pageResult = new PageResult<Enterprise_credit_investor>();

		Enterprise_credit_investorExample example = new Enterprise_credit_investorExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_credit_investor> list = null;// enterprise_credit_investorMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_credit_investorById(Long id) {
		this.enterprise_credit_investorMapper.deleteByPrimaryKey(id);
	}

}
