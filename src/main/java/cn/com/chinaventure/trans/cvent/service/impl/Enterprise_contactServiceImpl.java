package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_contact;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_contactExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_contactExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_contactMapper;
import cn.com.chinaventure.trans.cvent.service.Enterprise_contactService;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;


@Service("enterprise_contactService")
public class Enterprise_contactServiceImpl implements Enterprise_contactService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_contactServiceImpl.class);

	@Resource
	private Enterprise_contactMapper enterprise_contactMapper;


	@Override
	public void saveEnterprise_contact(Enterprise_contact enterprise_contact) {
		if (null == enterprise_contact) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_contactMapper.insertSelective(enterprise_contact);
	}

	@Override
	public void updateEnterprise_contact(Enterprise_contact enterprise_contact) {
		if (null == enterprise_contact) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_contactMapper.updateByPrimaryKeySelective(enterprise_contact);
	}

	@Override
	public Enterprise_contact getEnterprise_contactById(Long id) {
		return enterprise_contactMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_contact> getEnterprise_contactList(Enterprise_contact enterprise_contact) {
		return this.getEnterprise_contactListByParam(enterprise_contact, null);
	}

	@Override
	public List<Enterprise_contact> getEnterprise_contactListByParam(Enterprise_contact enterprise_contact, String orderByStr) {
		Enterprise_contactExample example = new Enterprise_contactExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		if(enterprise_contact.getEnterpriseid()!=null){
			criteria.andEnterpriseidEqualTo(enterprise_contact.getEnterpriseid());
		}

		List<Enterprise_contact> enterprise_contactList = this.enterprise_contactMapper.selectByExample(example);
		return enterprise_contactList;
	}

	@Override
	public PageResult<Enterprise_contact> findEnterprise_contactPageByParam(Enterprise_contact enterprise_contact, PageParam pageParam) {
		return this.findEnterprise_contactPageByParam(enterprise_contact, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_contact> findEnterprise_contactPageByParam(Enterprise_contact enterprise_contact, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_contact> pageResult = new PageResult<Enterprise_contact>();

		Enterprise_contactExample example = new Enterprise_contactExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_contact> list = null;// enterprise_contactMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_contactById(Long id) {
		this.enterprise_contactMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Enterprise_contact getEnterpriseContactByEnpId(Integer enterPriseId){
		Enterprise_contact enterprise_contact = new Enterprise_contact();
		enterprise_contact.setEnterpriseid((long)enterPriseId);
		List<Enterprise_contact> list =  getEnterprise_contactList( enterprise_contact);
		return list.size()>0?list.get(0):null;
	}

}
