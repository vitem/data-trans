package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_empty;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_emptyExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_credit_emptyMapper {
    int countByExample(Enterprise_credit_emptyExample example);

    int deleteByExample(Enterprise_credit_emptyExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Enterprise_credit_empty record);

    int insertSelective(Enterprise_credit_empty record);

    List<Enterprise_credit_empty> selectByExample(Enterprise_credit_emptyExample example);

    Enterprise_credit_empty selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Enterprise_credit_empty record, @Param("example") Enterprise_credit_emptyExample example);

    int updateByExample(@Param("record") Enterprise_credit_empty record, @Param("example") Enterprise_credit_emptyExample example);

    int updateByPrimaryKeySelective(Enterprise_credit_empty record);

    int updateByPrimaryKey(Enterprise_credit_empty record);
}