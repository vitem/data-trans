package cn.com.chinaventure.trans.cvent.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_dic;

import java.util.List;

/**
 * @author 
 *
 */
public interface Enterprise_credit_dicService {
	
	/**
     * @api saveEnterprise_credit_dic 新增enterprise_credit_dic
     * @apiGroup enterprise_credit_dic管理
     * @apiName  新增enterprise_credit_dic记录
     * @apiDescription 全量插入enterprise_credit_dic记录
     * @apiParam {Enterprise_credit_dic} enterprise_credit_dic enterprise_credit_dic对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise_credit_dic(Enterprise_credit_dic enterprise_credit_dic);
	
	/**
     * @api updateEnterprise_credit_dic 修改enterprise_credit_dic
     * @apiGroup enterprise_credit_dic管理
     * @apiName  修改enterprise_credit_dic记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise_credit_dic} enterprise_credit_dic enterprise_credit_dic对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise_credit_dic(Enterprise_credit_dic enterprise_credit_dic);
	
	/**
     * @api getEnterprise_credit_dicById 根据enterprise_credit_dicid查询详情
     * @apiGroup enterprise_credit_dic管理
     * @apiName  查询enterprise_credit_dic详情
     * @apiDescription 根据主键id查询enterprise_credit_dic详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Enterprise_credit_dic enterprise_credit_dic实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise_credit_dic getEnterprise_credit_dicById(Long id);
	
	/**
     * @api getEnterprise_credit_dicList 根据enterprise_credit_dic条件查询列表
     * @apiGroup enterprise_credit_dic管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise_credit_dic} enterprise_credit_dic  实体条件
     * @apiSuccess List<Enterprise_credit_dic> enterprise_credit_dic实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_credit_dic> getEnterprise_credit_dicList(Enterprise_credit_dic enterprise_credit_dic);
	

	/**
     * @api getEnterprise_credit_dicListByParam 根据enterprise_credit_dic条件查询列表（含排序）
     * @apiGroup enterprise_credit_dic管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise_credit_dic} enterprise_credit_dic  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise_credit_dic> enterprise_credit_dic实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_credit_dic> getEnterprise_credit_dicListByParam(Enterprise_credit_dic enterprise_credit_dic, String orderByStr);
	
	
	/**
     * @api findEnterprise_credit_dicPageByParam 根据enterprise_credit_dic条件查询列表（分页）
     * @apiGroup enterprise_credit_dic管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise_credit_dic} enterprise_credit_dic  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise_credit_dic> enterprise_credit_dic实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_credit_dic> findEnterprise_credit_dicPageByParam(Enterprise_credit_dic enterprise_credit_dic, PageParam pageParam); 
	
	/**
     * @api findEnterprise_credit_dicPageByParam 根据enterprise_credit_dic条件查询列表（分页，含排序）
     * @apiGroup enterprise_credit_dic管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise_credit_dic} enterprise_credit_dic  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise_credit_dic> enterprise_credit_dic实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_credit_dic> findEnterprise_credit_dicPageByParam(Enterprise_credit_dic enterprise_credit_dic, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterprise_credit_dicById 根据enterprise_credit_dic的id删除(物理删除)
     * @apiGroup enterprise_credit_dic管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterprise_credit_dicById(Long id);

}

