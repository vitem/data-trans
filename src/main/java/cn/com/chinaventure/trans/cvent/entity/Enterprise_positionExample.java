package cn.com.chinaventure.trans.cvent.entity;

import java.util.ArrayList;
import java.util.List;

public class Enterprise_positionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public Enterprise_positionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEnterprisePositionIdIsNull() {
            addCriterion("Enterprise_Position_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdIsNotNull() {
            addCriterion("Enterprise_Position_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdEqualTo(Integer value) {
            addCriterion("Enterprise_Position_ID =", value, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdNotEqualTo(Integer value) {
            addCriterion("Enterprise_Position_ID <>", value, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdGreaterThan(Integer value) {
            addCriterion("Enterprise_Position_ID >", value, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Enterprise_Position_ID >=", value, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdLessThan(Integer value) {
            addCriterion("Enterprise_Position_ID <", value, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdLessThanOrEqualTo(Integer value) {
            addCriterion("Enterprise_Position_ID <=", value, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdIn(List<Integer> values) {
            addCriterion("Enterprise_Position_ID in", values, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdNotIn(List<Integer> values) {
            addCriterion("Enterprise_Position_ID not in", values, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdBetween(Integer value1, Integer value2) {
            addCriterion("Enterprise_Position_ID between", value1, value2, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andEnterprisePositionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Enterprise_Position_ID not between", value1, value2, "enterprisePositionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdIsNull() {
            addCriterion("Postion_ID is null");
            return (Criteria) this;
        }

        public Criteria andPostionIdIsNotNull() {
            addCriterion("Postion_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPostionIdEqualTo(Integer value) {
            addCriterion("Postion_ID =", value, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdNotEqualTo(Integer value) {
            addCriterion("Postion_ID <>", value, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdGreaterThan(Integer value) {
            addCriterion("Postion_ID >", value, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Postion_ID >=", value, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdLessThan(Integer value) {
            addCriterion("Postion_ID <", value, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdLessThanOrEqualTo(Integer value) {
            addCriterion("Postion_ID <=", value, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdIn(List<Integer> values) {
            addCriterion("Postion_ID in", values, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdNotIn(List<Integer> values) {
            addCriterion("Postion_ID not in", values, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdBetween(Integer value1, Integer value2) {
            addCriterion("Postion_ID between", value1, value2, "postionId");
            return (Criteria) this;
        }

        public Criteria andPostionIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Postion_ID not between", value1, value2, "postionId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNull() {
            addCriterion("Enterprise_ID is null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIsNotNull() {
            addCriterion("Enterprise_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdEqualTo(Integer value) {
            addCriterion("Enterprise_ID =", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotEqualTo(Integer value) {
            addCriterion("Enterprise_ID <>", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThan(Integer value) {
            addCriterion("Enterprise_ID >", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Enterprise_ID >=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThan(Integer value) {
            addCriterion("Enterprise_ID <", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdLessThanOrEqualTo(Integer value) {
            addCriterion("Enterprise_ID <=", value, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdIn(List<Integer> values) {
            addCriterion("Enterprise_ID in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotIn(List<Integer> values) {
            addCriterion("Enterprise_ID not in", values, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdBetween(Integer value1, Integer value2) {
            addCriterion("Enterprise_ID between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andEnterpriseIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Enterprise_ID not between", value1, value2, "enterpriseId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNull() {
            addCriterion("Personage_ID is null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIsNotNull() {
            addCriterion("Personage_ID is not null");
            return (Criteria) this;
        }

        public Criteria andPersonageIdEqualTo(Integer value) {
            addCriterion("Personage_ID =", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotEqualTo(Integer value) {
            addCriterion("Personage_ID <>", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThan(Integer value) {
            addCriterion("Personage_ID >", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID >=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThan(Integer value) {
            addCriterion("Personage_ID <", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdLessThanOrEqualTo(Integer value) {
            addCriterion("Personage_ID <=", value, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdIn(List<Integer> values) {
            addCriterion("Personage_ID in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotIn(List<Integer> values) {
            addCriterion("Personage_ID not in", values, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andPersonageIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Personage_ID not between", value1, value2, "personageId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdIsNull() {
            addCriterion("Manageteam_ID is null");
            return (Criteria) this;
        }

        public Criteria andManageteamIdIsNotNull() {
            addCriterion("Manageteam_ID is not null");
            return (Criteria) this;
        }

        public Criteria andManageteamIdEqualTo(Integer value) {
            addCriterion("Manageteam_ID =", value, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdNotEqualTo(Integer value) {
            addCriterion("Manageteam_ID <>", value, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdGreaterThan(Integer value) {
            addCriterion("Manageteam_ID >", value, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("Manageteam_ID >=", value, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdLessThan(Integer value) {
            addCriterion("Manageteam_ID <", value, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdLessThanOrEqualTo(Integer value) {
            addCriterion("Manageteam_ID <=", value, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdIn(List<Integer> values) {
            addCriterion("Manageteam_ID in", values, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdNotIn(List<Integer> values) {
            addCriterion("Manageteam_ID not in", values, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdBetween(Integer value1, Integer value2) {
            addCriterion("Manageteam_ID between", value1, value2, "manageteamId");
            return (Criteria) this;
        }

        public Criteria andManageteamIdNotBetween(Integer value1, Integer value2) {
            addCriterion("Manageteam_ID not between", value1, value2, "manageteamId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}