package cn.com.chinaventure.trans.cvent.mapper;

import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_organization;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_organizationExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface Enterprise_credit_organizationMapper {
    int countByExample(Enterprise_credit_organizationExample example);

    int deleteByExample(Enterprise_credit_organizationExample example);

    int deleteByPrimaryKey(Long id);

    int insert(Enterprise_credit_organization record);

    int insertSelective(Enterprise_credit_organization record);

    List<Enterprise_credit_organization> selectByExample(Enterprise_credit_organizationExample example);

    Enterprise_credit_organization selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") Enterprise_credit_organization record, @Param("example") Enterprise_credit_organizationExample example);

    int updateByExample(@Param("record") Enterprise_credit_organization record, @Param("example") Enterprise_credit_organizationExample example);

    int updateByPrimaryKeySelective(Enterprise_credit_organization record);

    int updateByPrimaryKey(Enterprise_credit_organization record);
}