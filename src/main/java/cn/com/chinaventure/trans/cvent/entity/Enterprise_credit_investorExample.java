package cn.com.chinaventure.trans.cvent.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class Enterprise_credit_investorExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    protected String fields;

    public Enterprise_credit_investorExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setFields(String fields) {
        this.fields=fields;
    }

    public String getFields() {
        return fields;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("Id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("Id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("Id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("Id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("Id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("Id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("Id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("Id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("Id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("Id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("Id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andCreditIdIsNull() {
            addCriterion("Credit_ID is null");
            return (Criteria) this;
        }

        public Criteria andCreditIdIsNotNull() {
            addCriterion("Credit_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCreditIdEqualTo(Long value) {
            addCriterion("Credit_ID =", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdNotEqualTo(Long value) {
            addCriterion("Credit_ID <>", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdGreaterThan(Long value) {
            addCriterion("Credit_ID >", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdGreaterThanOrEqualTo(Long value) {
            addCriterion("Credit_ID >=", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdLessThan(Long value) {
            addCriterion("Credit_ID <", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdLessThanOrEqualTo(Long value) {
            addCriterion("Credit_ID <=", value, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdIn(List<Long> values) {
            addCriterion("Credit_ID in", values, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdNotIn(List<Long> values) {
            addCriterion("Credit_ID not in", values, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdBetween(Long value1, Long value2) {
            addCriterion("Credit_ID between", value1, value2, "creditId");
            return (Criteria) this;
        }

        public Criteria andCreditIdNotBetween(Long value1, Long value2) {
            addCriterion("Credit_ID not between", value1, value2, "creditId");
            return (Criteria) this;
        }

        public Criteria andInvestornameIsNull() {
            addCriterion("InvestorName is null");
            return (Criteria) this;
        }

        public Criteria andInvestornameIsNotNull() {
            addCriterion("InvestorName is not null");
            return (Criteria) this;
        }

        public Criteria andInvestornameEqualTo(String value) {
            addCriterion("InvestorName =", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameNotEqualTo(String value) {
            addCriterion("InvestorName <>", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameGreaterThan(String value) {
            addCriterion("InvestorName >", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameGreaterThanOrEqualTo(String value) {
            addCriterion("InvestorName >=", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameLessThan(String value) {
            addCriterion("InvestorName <", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameLessThanOrEqualTo(String value) {
            addCriterion("InvestorName <=", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameLike(String value) {
            addCriterion("InvestorName like", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameNotLike(String value) {
            addCriterion("InvestorName not like", value, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameIn(List<String> values) {
            addCriterion("InvestorName in", values, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameNotIn(List<String> values) {
            addCriterion("InvestorName not in", values, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameBetween(String value1, String value2) {
            addCriterion("InvestorName between", value1, value2, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestornameNotBetween(String value1, String value2) {
            addCriterion("InvestorName not between", value1, value2, "investorname");
            return (Criteria) this;
        }

        public Criteria andInvestortypeIsNull() {
            addCriterion("InvestorType is null");
            return (Criteria) this;
        }

        public Criteria andInvestortypeIsNotNull() {
            addCriterion("InvestorType is not null");
            return (Criteria) this;
        }

        public Criteria andInvestortypeEqualTo(String value) {
            addCriterion("InvestorType =", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeNotEqualTo(String value) {
            addCriterion("InvestorType <>", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeGreaterThan(String value) {
            addCriterion("InvestorType >", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeGreaterThanOrEqualTo(String value) {
            addCriterion("InvestorType >=", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeLessThan(String value) {
            addCriterion("InvestorType <", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeLessThanOrEqualTo(String value) {
            addCriterion("InvestorType <=", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeLike(String value) {
            addCriterion("InvestorType like", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeNotLike(String value) {
            addCriterion("InvestorType not like", value, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeIn(List<String> values) {
            addCriterion("InvestorType in", values, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeNotIn(List<String> values) {
            addCriterion("InvestorType not in", values, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeBetween(String value1, String value2) {
            addCriterion("InvestorType between", value1, value2, "investortype");
            return (Criteria) this;
        }

        public Criteria andInvestortypeNotBetween(String value1, String value2) {
            addCriterion("InvestorType not between", value1, value2, "investortype");
            return (Criteria) this;
        }

        public Criteria andOthernumIsNull() {
            addCriterion("OtherNum is null");
            return (Criteria) this;
        }

        public Criteria andOthernumIsNotNull() {
            addCriterion("OtherNum is not null");
            return (Criteria) this;
        }

        public Criteria andOthernumEqualTo(String value) {
            addCriterion("OtherNum =", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumNotEqualTo(String value) {
            addCriterion("OtherNum <>", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumGreaterThan(String value) {
            addCriterion("OtherNum >", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumGreaterThanOrEqualTo(String value) {
            addCriterion("OtherNum >=", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumLessThan(String value) {
            addCriterion("OtherNum <", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumLessThanOrEqualTo(String value) {
            addCriterion("OtherNum <=", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumLike(String value) {
            addCriterion("OtherNum like", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumNotLike(String value) {
            addCriterion("OtherNum not like", value, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumIn(List<String> values) {
            addCriterion("OtherNum in", values, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumNotIn(List<String> values) {
            addCriterion("OtherNum not in", values, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumBetween(String value1, String value2) {
            addCriterion("OtherNum between", value1, value2, "othernum");
            return (Criteria) this;
        }

        public Criteria andOthernumNotBetween(String value1, String value2) {
            addCriterion("OtherNum not between", value1, value2, "othernum");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayIsNull() {
            addCriterion("InvestmentWay is null");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayIsNotNull() {
            addCriterion("InvestmentWay is not null");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayEqualTo(String value) {
            addCriterion("InvestmentWay =", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayNotEqualTo(String value) {
            addCriterion("InvestmentWay <>", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayGreaterThan(String value) {
            addCriterion("InvestmentWay >", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayGreaterThanOrEqualTo(String value) {
            addCriterion("InvestmentWay >=", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayLessThan(String value) {
            addCriterion("InvestmentWay <", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayLessThanOrEqualTo(String value) {
            addCriterion("InvestmentWay <=", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayLike(String value) {
            addCriterion("InvestmentWay like", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayNotLike(String value) {
            addCriterion("InvestmentWay not like", value, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayIn(List<String> values) {
            addCriterion("InvestmentWay in", values, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayNotIn(List<String> values) {
            addCriterion("InvestmentWay not in", values, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayBetween(String value1, String value2) {
            addCriterion("InvestmentWay between", value1, value2, "investmentway");
            return (Criteria) this;
        }

        public Criteria andInvestmentwayNotBetween(String value1, String value2) {
            addCriterion("InvestmentWay not between", value1, value2, "investmentway");
            return (Criteria) this;
        }

        public Criteria andRjamountIsNull() {
            addCriterion("RJAmount is null");
            return (Criteria) this;
        }

        public Criteria andRjamountIsNotNull() {
            addCriterion("RJAmount is not null");
            return (Criteria) this;
        }

        public Criteria andRjamountEqualTo(Double value) {
            addCriterion("RJAmount =", value, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountNotEqualTo(Double value) {
            addCriterion("RJAmount <>", value, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountGreaterThan(Double value) {
            addCriterion("RJAmount >", value, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountGreaterThanOrEqualTo(Double value) {
            addCriterion("RJAmount >=", value, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountLessThan(Double value) {
            addCriterion("RJAmount <", value, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountLessThanOrEqualTo(Double value) {
            addCriterion("RJAmount <=", value, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountIn(List<Double> values) {
            addCriterion("RJAmount in", values, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountNotIn(List<Double> values) {
            addCriterion("RJAmount not in", values, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountBetween(Double value1, Double value2) {
            addCriterion("RJAmount between", value1, value2, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountNotBetween(Double value1, Double value2) {
            addCriterion("RJAmount not between", value1, value2, "rjamount");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitIsNull() {
            addCriterion("RJAmount_Unit is null");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitIsNotNull() {
            addCriterion("RJAmount_Unit is not null");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitEqualTo(String value) {
            addCriterion("RJAmount_Unit =", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitNotEqualTo(String value) {
            addCriterion("RJAmount_Unit <>", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitGreaterThan(String value) {
            addCriterion("RJAmount_Unit >", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitGreaterThanOrEqualTo(String value) {
            addCriterion("RJAmount_Unit >=", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitLessThan(String value) {
            addCriterion("RJAmount_Unit <", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitLessThanOrEqualTo(String value) {
            addCriterion("RJAmount_Unit <=", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitLike(String value) {
            addCriterion("RJAmount_Unit like", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitNotLike(String value) {
            addCriterion("RJAmount_Unit not like", value, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitIn(List<String> values) {
            addCriterion("RJAmount_Unit in", values, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitNotIn(List<String> values) {
            addCriterion("RJAmount_Unit not in", values, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitBetween(String value1, String value2) {
            addCriterion("RJAmount_Unit between", value1, value2, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountUnitNotBetween(String value1, String value2) {
            addCriterion("RJAmount_Unit not between", value1, value2, "rjamountUnit");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyIsNull() {
            addCriterion("RJAmount_Currency is null");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyIsNotNull() {
            addCriterion("RJAmount_Currency is not null");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyEqualTo(String value) {
            addCriterion("RJAmount_Currency =", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyNotEqualTo(String value) {
            addCriterion("RJAmount_Currency <>", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyGreaterThan(String value) {
            addCriterion("RJAmount_Currency >", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("RJAmount_Currency >=", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyLessThan(String value) {
            addCriterion("RJAmount_Currency <", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyLessThanOrEqualTo(String value) {
            addCriterion("RJAmount_Currency <=", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyLike(String value) {
            addCriterion("RJAmount_Currency like", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyNotLike(String value) {
            addCriterion("RJAmount_Currency not like", value, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyIn(List<String> values) {
            addCriterion("RJAmount_Currency in", values, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyNotIn(List<String> values) {
            addCriterion("RJAmount_Currency not in", values, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyBetween(String value1, String value2) {
            addCriterion("RJAmount_Currency between", value1, value2, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountCurrencyNotBetween(String value1, String value2) {
            addCriterion("RJAmount_Currency not between", value1, value2, "rjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoIsNull() {
            addCriterion("RJAmountInfo is null");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoIsNotNull() {
            addCriterion("RJAmountInfo is not null");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoEqualTo(String value) {
            addCriterion("RJAmountInfo =", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoNotEqualTo(String value) {
            addCriterion("RJAmountInfo <>", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoGreaterThan(String value) {
            addCriterion("RJAmountInfo >", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoGreaterThanOrEqualTo(String value) {
            addCriterion("RJAmountInfo >=", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoLessThan(String value) {
            addCriterion("RJAmountInfo <", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoLessThanOrEqualTo(String value) {
            addCriterion("RJAmountInfo <=", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoLike(String value) {
            addCriterion("RJAmountInfo like", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoNotLike(String value) {
            addCriterion("RJAmountInfo not like", value, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoIn(List<String> values) {
            addCriterion("RJAmountInfo in", values, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoNotIn(List<String> values) {
            addCriterion("RJAmountInfo not in", values, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoBetween(String value1, String value2) {
            addCriterion("RJAmountInfo between", value1, value2, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andRjamountinfoNotBetween(String value1, String value2) {
            addCriterion("RJAmountInfo not between", value1, value2, "rjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountIsNull() {
            addCriterion("SJAmount is null");
            return (Criteria) this;
        }

        public Criteria andSjamountIsNotNull() {
            addCriterion("SJAmount is not null");
            return (Criteria) this;
        }

        public Criteria andSjamountEqualTo(Double value) {
            addCriterion("SJAmount =", value, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountNotEqualTo(Double value) {
            addCriterion("SJAmount <>", value, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountGreaterThan(Double value) {
            addCriterion("SJAmount >", value, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountGreaterThanOrEqualTo(Double value) {
            addCriterion("SJAmount >=", value, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountLessThan(Double value) {
            addCriterion("SJAmount <", value, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountLessThanOrEqualTo(Double value) {
            addCriterion("SJAmount <=", value, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountIn(List<Double> values) {
            addCriterion("SJAmount in", values, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountNotIn(List<Double> values) {
            addCriterion("SJAmount not in", values, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountBetween(Double value1, Double value2) {
            addCriterion("SJAmount between", value1, value2, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountNotBetween(Double value1, Double value2) {
            addCriterion("SJAmount not between", value1, value2, "sjamount");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitIsNull() {
            addCriterion("SJAmount_Unit is null");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitIsNotNull() {
            addCriterion("SJAmount_Unit is not null");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitEqualTo(String value) {
            addCriterion("SJAmount_Unit =", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitNotEqualTo(String value) {
            addCriterion("SJAmount_Unit <>", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitGreaterThan(String value) {
            addCriterion("SJAmount_Unit >", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitGreaterThanOrEqualTo(String value) {
            addCriterion("SJAmount_Unit >=", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitLessThan(String value) {
            addCriterion("SJAmount_Unit <", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitLessThanOrEqualTo(String value) {
            addCriterion("SJAmount_Unit <=", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitLike(String value) {
            addCriterion("SJAmount_Unit like", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitNotLike(String value) {
            addCriterion("SJAmount_Unit not like", value, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitIn(List<String> values) {
            addCriterion("SJAmount_Unit in", values, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitNotIn(List<String> values) {
            addCriterion("SJAmount_Unit not in", values, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitBetween(String value1, String value2) {
            addCriterion("SJAmount_Unit between", value1, value2, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountUnitNotBetween(String value1, String value2) {
            addCriterion("SJAmount_Unit not between", value1, value2, "sjamountUnit");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyIsNull() {
            addCriterion("SJAmount_Currency is null");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyIsNotNull() {
            addCriterion("SJAmount_Currency is not null");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyEqualTo(String value) {
            addCriterion("SJAmount_Currency =", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyNotEqualTo(String value) {
            addCriterion("SJAmount_Currency <>", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyGreaterThan(String value) {
            addCriterion("SJAmount_Currency >", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("SJAmount_Currency >=", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyLessThan(String value) {
            addCriterion("SJAmount_Currency <", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyLessThanOrEqualTo(String value) {
            addCriterion("SJAmount_Currency <=", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyLike(String value) {
            addCriterion("SJAmount_Currency like", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyNotLike(String value) {
            addCriterion("SJAmount_Currency not like", value, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyIn(List<String> values) {
            addCriterion("SJAmount_Currency in", values, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyNotIn(List<String> values) {
            addCriterion("SJAmount_Currency not in", values, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyBetween(String value1, String value2) {
            addCriterion("SJAmount_Currency between", value1, value2, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountCurrencyNotBetween(String value1, String value2) {
            addCriterion("SJAmount_Currency not between", value1, value2, "sjamountCurrency");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoIsNull() {
            addCriterion("SJAmountInfo is null");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoIsNotNull() {
            addCriterion("SJAmountInfo is not null");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoEqualTo(String value) {
            addCriterion("SJAmountInfo =", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoNotEqualTo(String value) {
            addCriterion("SJAmountInfo <>", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoGreaterThan(String value) {
            addCriterion("SJAmountInfo >", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoGreaterThanOrEqualTo(String value) {
            addCriterion("SJAmountInfo >=", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoLessThan(String value) {
            addCriterion("SJAmountInfo <", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoLessThanOrEqualTo(String value) {
            addCriterion("SJAmountInfo <=", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoLike(String value) {
            addCriterion("SJAmountInfo like", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoNotLike(String value) {
            addCriterion("SJAmountInfo not like", value, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoIn(List<String> values) {
            addCriterion("SJAmountInfo in", values, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoNotIn(List<String> values) {
            addCriterion("SJAmountInfo not in", values, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoBetween(String value1, String value2) {
            addCriterion("SJAmountInfo between", value1, value2, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjamountinfoNotBetween(String value1, String value2) {
            addCriterion("SJAmountInfo not between", value1, value2, "sjamountinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateIsNull() {
            addCriterion("SJDate is null");
            return (Criteria) this;
        }

        public Criteria andSjdateIsNotNull() {
            addCriterion("SJDate is not null");
            return (Criteria) this;
        }

        public Criteria andSjdateEqualTo(Date value) {
            addCriterionForJDBCDate("SJDate =", value, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateNotEqualTo(Date value) {
            addCriterionForJDBCDate("SJDate <>", value, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateGreaterThan(Date value) {
            addCriterionForJDBCDate("SJDate >", value, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("SJDate >=", value, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateLessThan(Date value) {
            addCriterionForJDBCDate("SJDate <", value, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("SJDate <=", value, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateIn(List<Date> values) {
            addCriterionForJDBCDate("SJDate in", values, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateNotIn(List<Date> values) {
            addCriterionForJDBCDate("SJDate not in", values, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("SJDate between", value1, value2, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("SJDate not between", value1, value2, "sjdate");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoIsNull() {
            addCriterion("SJDateInfo is null");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoIsNotNull() {
            addCriterion("SJDateInfo is not null");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoEqualTo(String value) {
            addCriterion("SJDateInfo =", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoNotEqualTo(String value) {
            addCriterion("SJDateInfo <>", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoGreaterThan(String value) {
            addCriterion("SJDateInfo >", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoGreaterThanOrEqualTo(String value) {
            addCriterion("SJDateInfo >=", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoLessThan(String value) {
            addCriterion("SJDateInfo <", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoLessThanOrEqualTo(String value) {
            addCriterion("SJDateInfo <=", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoLike(String value) {
            addCriterion("SJDateInfo like", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoNotLike(String value) {
            addCriterion("SJDateInfo not like", value, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoIn(List<String> values) {
            addCriterion("SJDateInfo in", values, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoNotIn(List<String> values) {
            addCriterion("SJDateInfo not in", values, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoBetween(String value1, String value2) {
            addCriterion("SJDateInfo between", value1, value2, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andSjdateinfoNotBetween(String value1, String value2) {
            addCriterion("SJDateInfo not between", value1, value2, "sjdateinfo");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("Create_Time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("Create_Time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("Create_Time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("Create_Time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("Create_Time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("Create_Time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("Create_Time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("Create_Time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("Create_Time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("Create_Time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("Create_Time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("Create_Time not between", value1, value2, "createTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}