package cn.com.chinaventure.trans.cvent.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_empty;

import java.util.List;

/**
 * @author 
 *
 */
public interface Enterprise_credit_emptyService {
	
	/**
     * @api saveEnterprise_credit_empty 新增enterprise_credit_empty
     * @apiGroup enterprise_credit_empty管理
     * @apiName  新增enterprise_credit_empty记录
     * @apiDescription 全量插入enterprise_credit_empty记录
     * @apiParam {Enterprise_credit_empty} enterprise_credit_empty enterprise_credit_empty对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise_credit_empty(Enterprise_credit_empty enterprise_credit_empty);
	
	/**
     * @api updateEnterprise_credit_empty 修改enterprise_credit_empty
     * @apiGroup enterprise_credit_empty管理
     * @apiName  修改enterprise_credit_empty记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise_credit_empty} enterprise_credit_empty enterprise_credit_empty对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise_credit_empty(Enterprise_credit_empty enterprise_credit_empty);
	
	/**
     * @api getEnterprise_credit_emptyById 根据enterprise_credit_emptyid查询详情
     * @apiGroup enterprise_credit_empty管理
     * @apiName  查询enterprise_credit_empty详情
     * @apiDescription 根据主键id查询enterprise_credit_empty详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Enterprise_credit_empty enterprise_credit_empty实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise_credit_empty getEnterprise_credit_emptyById(Long id);
	
	/**
     * @api getEnterprise_credit_emptyList 根据enterprise_credit_empty条件查询列表
     * @apiGroup enterprise_credit_empty管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise_credit_empty} enterprise_credit_empty  实体条件
     * @apiSuccess List<Enterprise_credit_empty> enterprise_credit_empty实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_credit_empty> getEnterprise_credit_emptyList(Enterprise_credit_empty enterprise_credit_empty);
	

	/**
     * @api getEnterprise_credit_emptyListByParam 根据enterprise_credit_empty条件查询列表（含排序）
     * @apiGroup enterprise_credit_empty管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise_credit_empty} enterprise_credit_empty  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise_credit_empty> enterprise_credit_empty实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_credit_empty> getEnterprise_credit_emptyListByParam(Enterprise_credit_empty enterprise_credit_empty, String orderByStr);
	
	
	/**
     * @api findEnterprise_credit_emptyPageByParam 根据enterprise_credit_empty条件查询列表（分页）
     * @apiGroup enterprise_credit_empty管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise_credit_empty} enterprise_credit_empty  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise_credit_empty> enterprise_credit_empty实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_credit_empty> findEnterprise_credit_emptyPageByParam(Enterprise_credit_empty enterprise_credit_empty, PageParam pageParam); 
	
	/**
     * @api findEnterprise_credit_emptyPageByParam 根据enterprise_credit_empty条件查询列表（分页，含排序）
     * @apiGroup enterprise_credit_empty管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise_credit_empty} enterprise_credit_empty  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise_credit_empty> enterprise_credit_empty实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_credit_empty> findEnterprise_credit_emptyPageByParam(Enterprise_credit_empty enterprise_credit_empty, PageParam pageParam, String orderByStr);
	
	
	/**
     * @api deleteEnterprise_credit_emptyById 根据enterprise_credit_empty的id删除(物理删除)
     * @apiGroup enterprise_credit_empty管理
     * @apiName  根据id删除(物理删除)
     * @apiDescription 根据主键id删除对象(物理删除)
     * @apiParam {Integer} id  主键id
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void deleteEnterprise_credit_emptyById(Long id);

}

