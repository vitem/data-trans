package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class Enterprise_credit_dic implements Serializable {
    private Long id;

    private String city;

    private String citycode;

    private String citynum;

    private Long startnum;

    private Date updatetime;

    /**
     * 0-不需要抓取；1-表示需要抓取
     */
    private Integer isEnable;

    private static final long serialVersionUID = 1L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city == null ? null : city.trim();
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode == null ? null : citycode.trim();
    }

    public String getCitynum() {
        return citynum;
    }

    public void setCitynum(String citynum) {
        this.citynum = citynum == null ? null : citynum.trim();
    }

    public Long getStartnum() {
        return startnum;
    }

    public void setStartnum(Long startnum) {
        this.startnum = startnum;
    }

    public Date getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(Date updatetime) {
        this.updatetime = updatetime;
    }

    public Integer getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Integer isEnable) {
        this.isEnable = isEnable;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", city=").append(city);
        sb.append(", citycode=").append(citycode);
        sb.append(", citynum=").append(citynum);
        sb.append(", startnum=").append(startnum);
        sb.append(", updatetime=").append(updatetime);
        sb.append(", isEnable=").append(isEnable);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}