package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class Enterprise_manageteam implements Serializable {
    private Long enterpriseMtId;

    private Long enterpriseMtPersonageid;

    private String enterpriseMtPersonagename;

    private Long enterpriseMtLeaveoffice;

    private Date enterpriseMtEnterdutydate;

    private Date enterpriseMtLeaveofficedate;

    private Date createTime;

    private Date updateTime;

    private String behavior;

    private Long enterpriseId;

    private String enterpriseMtPosition;

    private Long ordered;

    private Long moduleId;

    private String role;

    private static final long serialVersionUID = 1L;

    public Long getEnterpriseMtId() {
        return enterpriseMtId;
    }

    public void setEnterpriseMtId(Long enterpriseMtId) {
        this.enterpriseMtId = enterpriseMtId;
    }

    public Long getEnterpriseMtPersonageid() {
        return enterpriseMtPersonageid;
    }

    public void setEnterpriseMtPersonageid(Long enterpriseMtPersonageid) {
        this.enterpriseMtPersonageid = enterpriseMtPersonageid;
    }

    public String getEnterpriseMtPersonagename() {
        return enterpriseMtPersonagename;
    }

    public void setEnterpriseMtPersonagename(String enterpriseMtPersonagename) {
        this.enterpriseMtPersonagename = enterpriseMtPersonagename == null ? null : enterpriseMtPersonagename.trim();
    }

    public Long getEnterpriseMtLeaveoffice() {
        return enterpriseMtLeaveoffice;
    }

    public void setEnterpriseMtLeaveoffice(Long enterpriseMtLeaveoffice) {
        this.enterpriseMtLeaveoffice = enterpriseMtLeaveoffice;
    }

    public Date getEnterpriseMtEnterdutydate() {
        return enterpriseMtEnterdutydate;
    }

    public void setEnterpriseMtEnterdutydate(Date enterpriseMtEnterdutydate) {
        this.enterpriseMtEnterdutydate = enterpriseMtEnterdutydate;
    }

    public Date getEnterpriseMtLeaveofficedate() {
        return enterpriseMtLeaveofficedate;
    }

    public void setEnterpriseMtLeaveofficedate(Date enterpriseMtLeaveofficedate) {
        this.enterpriseMtLeaveofficedate = enterpriseMtLeaveofficedate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getBehavior() {
        return behavior;
    }

    public void setBehavior(String behavior) {
        this.behavior = behavior == null ? null : behavior.trim();
    }

    public Long getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getEnterpriseMtPosition() {
        return enterpriseMtPosition;
    }

    public void setEnterpriseMtPosition(String enterpriseMtPosition) {
        this.enterpriseMtPosition = enterpriseMtPosition == null ? null : enterpriseMtPosition.trim();
    }

    public Long getOrdered() {
        return ordered;
    }

    public void setOrdered(Long ordered) {
        this.ordered = ordered;
    }

    public Long getModuleId() {
        return moduleId;
    }

    public void setModuleId(Long moduleId) {
        this.moduleId = moduleId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", enterpriseMtId=").append(enterpriseMtId);
        sb.append(", enterpriseMtPersonageid=").append(enterpriseMtPersonageid);
        sb.append(", enterpriseMtPersonagename=").append(enterpriseMtPersonagename);
        sb.append(", enterpriseMtLeaveoffice=").append(enterpriseMtLeaveoffice);
        sb.append(", enterpriseMtEnterdutydate=").append(enterpriseMtEnterdutydate);
        sb.append(", enterpriseMtLeaveofficedate=").append(enterpriseMtLeaveofficedate);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", behavior=").append(behavior);
        sb.append(", enterpriseId=").append(enterpriseId);
        sb.append(", enterpriseMtPosition=").append(enterpriseMtPosition);
        sb.append(", ordered=").append(ordered);
        sb.append(", moduleId=").append(moduleId);
        sb.append(", role=").append(role);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}