package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvent.service.Enterprise_positionService;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_position;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_positionExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_positionExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_positionMapper;


@Service("enterprise_positionService")
public class Enterprise_positionServiceImpl implements Enterprise_positionService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_positionServiceImpl.class);

	@Resource
	private Enterprise_positionMapper enterprise_positionMapper;


	@Override
	public void saveEnterprise_position(Enterprise_position enterprise_position) {
		if (null == enterprise_position) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_positionMapper.insertSelective(enterprise_position);
	}

	@Override
	public void updateEnterprise_position(Enterprise_position enterprise_position) {
		if (null == enterprise_position) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_positionMapper.updateByPrimaryKeySelective(enterprise_position);
	}

	@Override
	public Enterprise_position getEnterprise_positionById(Integer id) {
		return enterprise_positionMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_position> getEnterprise_positionList(Enterprise_position enterprise_position) {
		return this.getEnterprise_positionListByParam(enterprise_position, null);
	}

	@Override
	public List<Enterprise_position> getEnterprise_positionListByParam(Enterprise_position enterprise_position, String orderByStr) {
		Enterprise_positionExample example = new Enterprise_positionExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise_position> enterprise_positionList = this.enterprise_positionMapper.selectByExample(example);
		return enterprise_positionList;
	}

	@Override
	public PageResult<Enterprise_position> findEnterprise_positionPageByParam(Enterprise_position enterprise_position, PageParam pageParam) {
		return this.findEnterprise_positionPageByParam(enterprise_position, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_position> findEnterprise_positionPageByParam(Enterprise_position enterprise_position, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_position> pageResult = new PageResult<Enterprise_position>();

		Enterprise_positionExample example = new Enterprise_positionExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_position> list = null;// enterprise_positionMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_positionById(Integer id) {
		this.enterprise_positionMapper.deleteByPrimaryKey(id);
	}

}
