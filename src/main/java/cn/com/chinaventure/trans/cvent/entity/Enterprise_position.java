package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;

public class Enterprise_position implements Serializable {
    private Integer enterprisePositionId;

    private Integer postionId;

    private Integer enterpriseId;

    private Integer personageId;

    private Integer manageteamId;

    private static final long serialVersionUID = 1L;

    public Integer getEnterprisePositionId() {
        return enterprisePositionId;
    }

    public void setEnterprisePositionId(Integer enterprisePositionId) {
        this.enterprisePositionId = enterprisePositionId;
    }

    public Integer getPostionId() {
        return postionId;
    }

    public void setPostionId(Integer postionId) {
        this.postionId = postionId;
    }

    public Integer getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Integer enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public Integer getPersonageId() {
        return personageId;
    }

    public void setPersonageId(Integer personageId) {
        this.personageId = personageId;
    }

    public Integer getManageteamId() {
        return manageteamId;
    }

    public void setManageteamId(Integer manageteamId) {
        this.manageteamId = manageteamId;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", enterprisePositionId=").append(enterprisePositionId);
        sb.append(", postionId=").append(postionId);
        sb.append(", enterpriseId=").append(enterpriseId);
        sb.append(", personageId=").append(personageId);
        sb.append(", manageteamId=").append(manageteamId);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}