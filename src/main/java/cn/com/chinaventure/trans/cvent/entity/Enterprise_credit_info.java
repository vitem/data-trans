package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class Enterprise_credit_info implements Serializable {
    private Long creditId;

    /**
     * 企业ID
     */
    private Long enterpriseId;

    /**
     * 所属省份
     */
    private String province;

    /**
     * 注册号
     */
    private String registerNum;

    /**
     * 注册名称
     */
    private String registerName;

    /**
     * 类型
     */
    private String registerType;

    /**
     * 成立日期
     */
    private Date setupdate;

    /**
     * 法定代表人
     */
    private String legalperson;

    /**
     * 注册资本
     */
    private Double registermoney;

    /**
     * 注册资本单位
     */
    private String registermoneyUnit;

    /**
     * 注册资本币种
     */
    private String registermoneyCurrency;

    /**
     * 住所
     */
    private String registerAddress;

    /**
     * 营业开始日期
     */
    private Date openstartDate;

    /**
     * 营业结束日期
     */
    private Date openendDate;

    /**
     * 登记机关
     */
    private String registerOrgan;

    /**
     * 法照日期
     */
    private Date licensedate;

    /**
     * 经营状态
     */
    private String businessState;

    /**
     * 入库时间
     */
    private Date createTime;

    /**
     * 查询关键字
     */
    private String querykey;

    /**
     * 经营范围
     */
    private String mainBusiness;

    private static final long serialVersionUID = 1L;

    public Long getCreditId() {
        return creditId;
    }

    public void setCreditId(Long creditId) {
        this.creditId = creditId;
    }

    public Long getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Long enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province == null ? null : province.trim();
    }

    public String getRegisterNum() {
        return registerNum;
    }

    public void setRegisterNum(String registerNum) {
        this.registerNum = registerNum == null ? null : registerNum.trim();
    }

    public String getRegisterName() {
        return registerName;
    }

    public void setRegisterName(String registerName) {
        this.registerName = registerName == null ? null : registerName.trim();
    }

    public String getRegisterType() {
        return registerType;
    }

    public void setRegisterType(String registerType) {
        this.registerType = registerType == null ? null : registerType.trim();
    }

    public Date getSetupdate() {
        return setupdate;
    }

    public void setSetupdate(Date setupdate) {
        this.setupdate = setupdate;
    }

    public String getLegalperson() {
        return legalperson;
    }

    public void setLegalperson(String legalperson) {
        this.legalperson = legalperson == null ? null : legalperson.trim();
    }

    public Double getRegistermoney() {
        return registermoney;
    }

    public void setRegistermoney(Double registermoney) {
        this.registermoney = registermoney;
    }

    public String getRegistermoneyUnit() {
        return registermoneyUnit;
    }

    public void setRegistermoneyUnit(String registermoneyUnit) {
        this.registermoneyUnit = registermoneyUnit == null ? null : registermoneyUnit.trim();
    }

    public String getRegistermoneyCurrency() {
        return registermoneyCurrency;
    }

    public void setRegistermoneyCurrency(String registermoneyCurrency) {
        this.registermoneyCurrency = registermoneyCurrency == null ? null : registermoneyCurrency.trim();
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress == null ? null : registerAddress.trim();
    }

    public Date getOpenstartDate() {
        return openstartDate;
    }

    public void setOpenstartDate(Date openstartDate) {
        this.openstartDate = openstartDate;
    }

    public Date getOpenendDate() {
        return openendDate;
    }

    public void setOpenendDate(Date openendDate) {
        this.openendDate = openendDate;
    }

    public String getRegisterOrgan() {
        return registerOrgan;
    }

    public void setRegisterOrgan(String registerOrgan) {
        this.registerOrgan = registerOrgan == null ? null : registerOrgan.trim();
    }

    public Date getLicensedate() {
        return licensedate;
    }

    public void setLicensedate(Date licensedate) {
        this.licensedate = licensedate;
    }

    public String getBusinessState() {
        return businessState;
    }

    public void setBusinessState(String businessState) {
        this.businessState = businessState == null ? null : businessState.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getQuerykey() {
        return querykey;
    }

    public void setQuerykey(String querykey) {
        this.querykey = querykey == null ? null : querykey.trim();
    }

    public String getMainBusiness() {
        return mainBusiness;
    }

    public void setMainBusiness(String mainBusiness) {
        this.mainBusiness = mainBusiness == null ? null : mainBusiness.trim();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", creditId=").append(creditId);
        sb.append(", enterpriseId=").append(enterpriseId);
        sb.append(", province=").append(province);
        sb.append(", registerNum=").append(registerNum);
        sb.append(", registerName=").append(registerName);
        sb.append(", registerType=").append(registerType);
        sb.append(", setupdate=").append(setupdate);
        sb.append(", legalperson=").append(legalperson);
        sb.append(", registermoney=").append(registermoney);
        sb.append(", registermoneyUnit=").append(registermoneyUnit);
        sb.append(", registermoneyCurrency=").append(registermoneyCurrency);
        sb.append(", registerAddress=").append(registerAddress);
        sb.append(", openstartDate=").append(openstartDate);
        sb.append(", openendDate=").append(openendDate);
        sb.append(", registerOrgan=").append(registerOrgan);
        sb.append(", licensedate=").append(licensedate);
        sb.append(", businessState=").append(businessState);
        sb.append(", createTime=").append(createTime);
        sb.append(", querykey=").append(querykey);
        sb.append(", mainBusiness=").append(mainBusiness);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}