package cn.com.chinaventure.trans.cvent.service;

import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_manageteam;

import java.util.List;

/**
 * @author 
 *
 */
public interface Enterprise_manageteamService {
	
	/**
     * @api saveEnterprise_manageteam 新增enterprise_manageteam
     * @apiGroup enterprise_manageteam管理
     * @apiName  新增enterprise_manageteam记录
     * @apiDescription 全量插入enterprise_manageteam记录
     * @apiParam {Enterprise_manageteam} enterprise_manageteam enterprise_manageteam对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public  void saveEnterprise_manageteam(Enterprise_manageteam enterprise_manageteam);
	
	/**
     * @api updateEnterprise_manageteam 修改enterprise_manageteam
     * @apiGroup enterprise_manageteam管理
     * @apiName  修改enterprise_manageteam记录
     * @apiDescription 根据主键id修改记录，只修改非null属性
     * @apiParam {Enterprise_manageteam} enterprise_manageteam enterprise_manageteam对象
     * @apiSuccess void
     * @apiVersion 1.0.0
     * 
     */
	public void updateEnterprise_manageteam(Enterprise_manageteam enterprise_manageteam);
	
	/**
     * @api getEnterprise_manageteamById 根据enterprise_manageteamid查询详情
     * @apiGroup enterprise_manageteam管理
     * @apiName  查询enterprise_manageteam详情
     * @apiDescription 根据主键id查询enterprise_manageteam详情
     * @apiParam {Integer} id 主键id
     * @apiSuccess Enterprise_manageteam enterprise_manageteam实体对象
     * @apiVersion 1.0.0
     * 
     */
	public Enterprise_manageteam getEnterprise_manageteamById(Long id);
	
	/**
     * @api getEnterprise_manageteamList 根据enterprise_manageteam条件查询列表
     * @apiGroup enterprise_manageteam管理
     * @apiName  查询列表
     * @apiDescription 根据条件查询列表
     * @apiParam {Enterprise_manageteam} enterprise_manageteam  实体条件
     * @apiSuccess List<Enterprise_manageteam> enterprise_manageteam实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_manageteam> getEnterprise_manageteamList(Enterprise_manageteam enterprise_manageteam);
	

	/**
     * @api getEnterprise_manageteamListByParam 根据enterprise_manageteam条件查询列表（含排序）
     * @apiGroup enterprise_manageteam管理
     * @apiName  查询列表（含排序）
     * @apiDescription 根据条件查询列表，含排序
     * @apiParam {Enterprise_manageteam} enterprise_manageteam  实体条件
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess List<Enterprise_manageteam> enterprise_manageteam实体列表
     * @apiVersion 1.0.0
     * 
     */
	public List<Enterprise_manageteam> getEnterprise_manageteamListByParam(Enterprise_manageteam enterprise_manageteam, String orderByStr);
	
	
	/**
     * @api findEnterprise_manageteamPageByParam 根据enterprise_manageteam条件查询列表（分页）
     * @apiGroup enterprise_manageteam管理
     * @apiName  查询列表（分页）
     * @apiDescription 根据条件查询列表，分页
     * @apiParam {Enterprise_manageteam} enterprise_manageteam  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiSuccess PageResult<Enterprise_manageteam> enterprise_manageteam实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_manageteam> findEnterprise_manageteamPageByParam(Enterprise_manageteam enterprise_manageteam, PageParam pageParam); 
	
	/**
     * @api findEnterprise_manageteamPageByParam 根据enterprise_manageteam条件查询列表（分页，含排序）
     * @apiGroup enterprise_manageteam管理
     * @apiName  查询列表（分页，含排序）
     * @apiDescription 根据条件查询列表，分页，含排序
     * @apiParam {Enterprise_manageteam} enterprise_manageteam  实体条件
     * @apiParam {PageParam} pageParam  必需属性pageNo，pageSize
     * @apiParam {String} orderByStr  排序字段。举例：id asc
     * @apiSuccess PageResult<Enterprise_manageteam> enterprise_manageteam实体列表
     * @apiVersion 1.0.0
     * 
     */
	public PageResult<Enterprise_manageteam> findEnterprise_manageteamPageByParam(Enterprise_manageteam enterprise_manageteam, PageParam pageParam, String orderByStr);
	
	


    Enterprise_manageteam getEnterprise_manageteamByEnpId(Integer cvEnterpriseId);
}

