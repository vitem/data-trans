package cn.com.chinaventure.trans.cvent.entity;

import java.io.Serializable;
import java.util.Date;

public class EnterpriseAppinfo implements Serializable {
    private Integer id;

    private Integer enterpriseId;

    private String stockCode;

    private Byte exchangeId;

    private String exchangeCnnames;

    private String exchangeEnnames;

    private String exchangeCnName;

    private String exchangeEnName;

    private String exchangeCnShort;

    private String exchangeEnShort;

    private Date appearmarketDate;

    private Byte appearmarketType;

    /**
     * 1--有效；2--无效
     */
    private Byte infoStatus;

    /**
     * 1.A股,2.H股,3.美股,9.新三板;
     */
    private Byte stockType;

    private static final long serialVersionUID = 1L;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEnterpriseId() {
        return enterpriseId;
    }

    public void setEnterpriseId(Integer enterpriseId) {
        this.enterpriseId = enterpriseId;
    }

    public String getStockCode() {
        return stockCode;
    }

    public void setStockCode(String stockCode) {
        this.stockCode = stockCode == null ? null : stockCode.trim();
    }

    public Byte getExchangeId() {
        return exchangeId;
    }

    public void setExchangeId(Byte exchangeId) {
        this.exchangeId = exchangeId;
    }

    public String getExchangeCnnames() {
        return exchangeCnnames;
    }

    public void setExchangeCnnames(String exchangeCnnames) {
        this.exchangeCnnames = exchangeCnnames == null ? null : exchangeCnnames.trim();
    }

    public String getExchangeEnnames() {
        return exchangeEnnames;
    }

    public void setExchangeEnnames(String exchangeEnnames) {
        this.exchangeEnnames = exchangeEnnames == null ? null : exchangeEnnames.trim();
    }

    public String getExchangeCnName() {
        return exchangeCnName;
    }

    public void setExchangeCnName(String exchangeCnName) {
        this.exchangeCnName = exchangeCnName == null ? null : exchangeCnName.trim();
    }

    public String getExchangeEnName() {
        return exchangeEnName;
    }

    public void setExchangeEnName(String exchangeEnName) {
        this.exchangeEnName = exchangeEnName == null ? null : exchangeEnName.trim();
    }

    public String getExchangeCnShort() {
        return exchangeCnShort;
    }

    public void setExchangeCnShort(String exchangeCnShort) {
        this.exchangeCnShort = exchangeCnShort == null ? null : exchangeCnShort.trim();
    }

    public String getExchangeEnShort() {
        return exchangeEnShort;
    }

    public void setExchangeEnShort(String exchangeEnShort) {
        this.exchangeEnShort = exchangeEnShort == null ? null : exchangeEnShort.trim();
    }

    public Date getAppearmarketDate() {
        return appearmarketDate;
    }

    public void setAppearmarketDate(Date appearmarketDate) {
        this.appearmarketDate = appearmarketDate;
    }

    public Byte getAppearmarketType() {
        return appearmarketType;
    }

    public void setAppearmarketType(Byte appearmarketType) {
        this.appearmarketType = appearmarketType;
    }

    public Byte getInfoStatus() {
        return infoStatus;
    }

    public void setInfoStatus(Byte infoStatus) {
        this.infoStatus = infoStatus;
    }

    public Byte getStockType() {
        return stockType;
    }

    public void setStockType(Byte stockType) {
        this.stockType = stockType;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", enterpriseId=").append(enterpriseId);
        sb.append(", stockCode=").append(stockCode);
        sb.append(", exchangeId=").append(exchangeId);
        sb.append(", exchangeCnnames=").append(exchangeCnnames);
        sb.append(", exchangeEnnames=").append(exchangeEnnames);
        sb.append(", exchangeCnName=").append(exchangeCnName);
        sb.append(", exchangeEnName=").append(exchangeEnName);
        sb.append(", exchangeCnShort=").append(exchangeCnShort);
        sb.append(", exchangeEnShort=").append(exchangeEnShort);
        sb.append(", appearmarketDate=").append(appearmarketDate);
        sb.append(", appearmarketType=").append(appearmarketType);
        sb.append(", infoStatus=").append(infoStatus);
        sb.append(", stockType=").append(stockType);
        sb.append(", serialVersionUID=").append(serialVersionUID);
        sb.append("]");
        return sb.toString();
    }
}