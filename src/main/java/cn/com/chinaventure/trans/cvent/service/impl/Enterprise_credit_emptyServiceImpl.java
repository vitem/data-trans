package cn.com.chinaventure.trans.cvent.service.impl;

import cn.com.chinaventure.common.error.BusinessException;
import cn.com.chinaventure.common.error.ErrCodeE;
import cn.com.chinaventure.common.page.PageParam;
import cn.com.chinaventure.common.page.PageResult;
import cn.com.chinaventure.common.page.PageTools;
import cn.com.chinaventure.common.util.StringUtil;
import org.apache.ibatis.session.RowBounds;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import cn.com.chinaventure.trans.cvent.service.Enterprise_credit_emptyService;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_empty;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_emptyExample;
import cn.com.chinaventure.trans.cvent.entity.Enterprise_credit_emptyExample.Criteria;
import cn.com.chinaventure.trans.cvent.mapper.Enterprise_credit_emptyMapper;


@Service("enterprise_credit_emptyService")
public class Enterprise_credit_emptyServiceImpl implements Enterprise_credit_emptyService {

	private final static Logger logger = LoggerFactory.getLogger(Enterprise_credit_emptyServiceImpl.class);

	@Resource
	private Enterprise_credit_emptyMapper enterprise_credit_emptyMapper;


	@Override
	public void saveEnterprise_credit_empty(Enterprise_credit_empty enterprise_credit_empty) {
		if (null == enterprise_credit_empty) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_emptyMapper.insertSelective(enterprise_credit_empty);
	}

	@Override
	public void updateEnterprise_credit_empty(Enterprise_credit_empty enterprise_credit_empty) {
		if (null == enterprise_credit_empty) {
		 throw new BusinessException(ErrCodeE.PARAM_INVALID.getCode(), ErrCodeE.PARAM_INVALID.getName());
		}
		this.enterprise_credit_emptyMapper.updateByPrimaryKeySelective(enterprise_credit_empty);
	}

	@Override
	public Enterprise_credit_empty getEnterprise_credit_emptyById(Long id) {
		return enterprise_credit_emptyMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Enterprise_credit_empty> getEnterprise_credit_emptyList(Enterprise_credit_empty enterprise_credit_empty) {
		return this.getEnterprise_credit_emptyListByParam(enterprise_credit_empty, null);
	}

	@Override
	public List<Enterprise_credit_empty> getEnterprise_credit_emptyListByParam(Enterprise_credit_empty enterprise_credit_empty, String orderByStr) {
		Enterprise_credit_emptyExample example = new Enterprise_credit_emptyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();

		List<Enterprise_credit_empty> enterprise_credit_emptyList = this.enterprise_credit_emptyMapper.selectByExample(example);
		return enterprise_credit_emptyList;
	}

	@Override
	public PageResult<Enterprise_credit_empty> findEnterprise_credit_emptyPageByParam(Enterprise_credit_empty enterprise_credit_empty, PageParam pageParam) {
		return this.findEnterprise_credit_emptyPageByParam(enterprise_credit_empty, pageParam, null);
	}

	@Override
	public PageResult<Enterprise_credit_empty> findEnterprise_credit_emptyPageByParam(Enterprise_credit_empty enterprise_credit_empty, PageParam pageParam, String orderByStr) {
		PageResult<Enterprise_credit_empty> pageResult = new PageResult<Enterprise_credit_empty>();

		Enterprise_credit_emptyExample example = new Enterprise_credit_emptyExample();
		if (StringUtil.isNotBlank(orderByStr)) {
		 example.setOrderByClause(orderByStr);
		}
		Criteria criteria = example.createCriteria();
		//PageHelper.startPage(pageParam.getPageNo(), pageParam.getPageSize()); //true:进行count查询,默认true
		RowBounds rowBounds=  PageTools.createRowBounds(pageParam);
		List<Enterprise_credit_empty> list = null;// enterprise_credit_emptyMapper.selectByExampleWithRowbounds(example);
		//兼容分页和不分页
		pageResult = PageResult.toPage(list);
		return pageResult;
	}

	@Override
	public void deleteEnterprise_credit_emptyById(Long id) {
		this.enterprise_credit_emptyMapper.deleteByPrimaryKey(id);
	}

}
